$(document).ready(function () {
	let dataFormId = $("#hdnSessDataFormId").val();
	let fnaId = $("#hdnSessFnaId").val();
	getClientDetails(fnaId);
	getAllOtherPerDataById(fnaId);
	/*Nric search*/
	getAllCustomerData();
	window.addEventListener("beforeunload", function (e) {
		// saveData();
	});

	$("#expandcollape").trigger("click");

});
function intrPrePanelDisplay(display) {
	document.getElementById("intrPrePanelId").style.display = display;
}

$('.select2-single').select2();

// Select2 Single  with Placeholder
$('#selFldClntSave').select2({
	templateResult: formatState,
	placeholder: "Select a Client to Save",
	allowClear: true
});

function formatState(state) {
	if (!state.id) {
		return state.text;
	}

	let prin = state.element.value.toLowerCase();
	let icon = "";
	switch (prin) {

		case "aviva":
			icon = "img/aviva.png";
			break;

		case "ntuc":
			icon = "img/ntucincome.png";
			break;

		case "manulife":
			icon = "img/Manulife_logo.png";
			break;

		case "aia":
			icon = "img/aia.jpg";
			break;
		default:
			icon = "img/noimage.png";
	}
	let $state = $('<span><img src="' + icon + '" class="img-flag" style="width:100px;height:20px;" /> &nbsp; | &nbsp; <strong>' + state.text + '</strong></span>'
	);
	return $state;
}

// inter/Trust stepper js code start
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function () {
	if (animating) return false;
	animating = true;

	current_fs = $(this).parent();
	next_fs = $(this).parent().next();

	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

	//show the next fieldset
	next_fs.show();
	//hide the current fieldset with style
	current_fs.animate({ opacity: 0 }, {
		step: function (now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50) + "%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
				'transform': 'scale(' + scale + ')',
				'position': 'absolute'
			});
			next_fs.css({ 'left': left, 'opacity': opacity });
		},
		duration: 800,
		complete: function () {
			current_fs.hide();
			animating = false;
		},
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function () {
	if (animating) return false;
	animating = true;

	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();

	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

	//show the previous fieldset
	previous_fs.show();
	//hide the current fieldset with style
	current_fs.animate({ opacity: 0 }, {
		step: function (now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1 - now) * 50) + "%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({ 'left': left });
			previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity });
		},
		duration: 800,
		complete: function () {
			current_fs.hide();
			animating = false;
		},
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function () {
	return false;
});
// end##########

//Add new insured details
//Create personal details tab

//Get all self spouse data
/*getAllCustomerData();*/
//Search nric 

function getNricSearch() {

	let input, filter, ul, li, a, txtValue;
	input = document.getElementById("nricTypeText");
	filter = input.value.toUpperCase();
	ul = document.getElementById("searchListItems");

	ul.style.display = "block";

	if (input.value == "") {
		ul.style.display = "none";
	}

	li = ul.getElementsByTagName("li");
	for (let idx = 0; idx < li.length; idx++) {
		a = li[idx].getElementsByTagName("a")[0];

		txtValue = a.textContent || a.innerText;

		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			li[idx].style.display = "";
		} else {
			li[idx].style.display = "none";
		}
	}
}

function addInsured() {
	let selfName = "new";
	let dataFormId = "new";
	addNewInsured(dataFormId, selfName);
	let firstTab = 0;
	let secondTab = 1;
	$('#myTab a:eq(' + firstTab + ')').tab('show');
	$('#myTab a:eq(' + secondTab + ')').tab('show');
}

var custMap = new Map();

function addNewInsured(custId, custName) {

	$('.modal').modal('hide');

	let nextTab = $('#myTab li').length + 1;
	let toShowTab = $('#myTab li').length - 2;

	$('#myTab').find("a.nav-link").removeClass("active");
	$('#myTab').find(' > li:nth-last-child(2)').before('<li class="nav-item"> <a class="nav-link" id="' + custId + '" data-toggle="tab" href= "#tab' + nextTab + '" role="tab" aria-controls="client' + nextTab + '" aria-selected="false">' + custName + '&nbsp;<i class="fa fa-trash-o delBtnclr" aria-hidden="true" style="font-size: larger;" title="Delete Client" onclick=deleteTab();></a></i></li>');

	// create the tab content

	$('#myTabContent').find("div.tab-pane").removeClass("active");

	/*$('<div class="tab-pane fade show active" id="tab'+nextTab+'" role="tabpanel" aria-labelledby="client'+nextTab+'-tab" ><input type="text" id="newone'+nextTab+'"/></div>').appendTo('#myTabContent');*/

	let tabPane = $('<div class="tab-pane fade show active" id="client' + nextTab + '-tab" role="tabpanel" aria-labelledby="client' + nextTab + '-tab" > ' + $("#myTabContent").find("div.tab-pane").html() + '</div>');
	tabPane.appendTo('#myTabContent');

	setCurrentClientData(nextTab, toShowTab, custId);
	intrPrePanelDisplay("none");
	// make the new tab active
	//$('#myTab a:eq('+toShowTab+')').tab('show');
}


// add new insured details
function addNewInsuredClnt(obj) {

	let insurAddFlg = $("#AddInsured").is(':checked');
	if (insurAddFlg == true) {
		$("#selFldNRICNameError").removeClass("show").addClass("hide");
		//clear Search Insured Combo Box
		$('#selFldNRICName').val('');
		$('#selFldNRICName').select2({
			templateResult: formatClientOpt,
			placeholder: 'Select a Insured Name'
		});
		swal.fire({
			title: "Add New Client!",
			input: "text",
			inputLabel: "Client Name:",
			//html:'Insured Name: <input id="swal-input1" class="swal2-input">' ,
			showCancelButton: true,
			closeOnConfirm: false,
			inputValidator: function (value) {
				if (!value) {
					return 'Client Name Cannot be empty!'
				}
			}
		}).then((result) => {
			if (result.value) {
				let inputValue = result.value

				let newinsured = inputValue.trim();

				// check if already tab existed or not
				let tabArr = [], strTab = "";
				$('#myTab li a').each(function () {
					strTab = $(this).text();
					tabArr.push(strTab.trim().toLowerCase());
				});

				let clntTabNameExts = tabArr.includes(newinsured.trim().toLowerCase());

				if (!clntTabNameExts) {
					let nextTab = $('#myTab li').length + 1;

					$('#myTab').find("a.nav-link").removeClass("active");
					$('#myTab').find('li:nth-last-child(2)').before('<li class="nav-item" style="list-style-type:none;"><a class="nav-link dfsps active" id="client' + nextTab + '-tab" data-toggle="tab" href= "#tab' + nextTab + '" role="tab" aria-controls="client' + nextTab + '" aria-selected="false"> ' + newinsured + '&nbsp;<i class="fa fa-trash-o delBtnclr pl-3" aria-hidden="true" style="font-size: larger;" id="' + newinsured + '" title="Delete : ' + newinsured + '" onclick="deletInsured(this)" ></i></a> </li>');
					$('#myTabContent').find("div.tab-pane").removeClass("active");

					let dynamictab = $("#divSpouseDetsElems");
					$("#divSpouseDetsElems").removeClass("d-none");

					$('<div class="tab-pane fade show active" id="tab' + nextTab + '" role="tabpanel" aria-label="client' + nextTab + '-tab" ></div>').prepend(dynamictab).appendTo('#myTabContent');

					$('#myTab a:eq(client' + nextTab + '-tab)').tab('show');

					//set Spouse Name in dfSpsName Field
					$("#dfSpsName").val(newinsured);

					//set spousename Fld ReadOnly Property
					$("#dfSpsName").prop("readOnly", true);

					showSpouseInMenu(newinsured);

					if ($('#myTab li').length == 6) {
						$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor", "not-allowed");
						$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle", "");
						$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target", "");
					}

					$('#new2ModalScrollable').modal('hide');
					swal.close();
				} else {
					swal.fire("New Client Name already Exists ,Add different Client Name!", "", "error");
				}
			}
		});
	}
}

//set Search Insured radio btn values while click add Insured Button 
function setAddSrchRadioBtnFields() {
	$("#selFldNRICNameError").removeClass("show").addClass("hide");
	$("#SrchInsured").prop("checked", true);
	$('input[name="AddSrchInsured"]:checked').val("SrchInsured");
	$('#selFldNRICName').val('');
	$('#selFldNRICName').select2({
		templateResult: formatClientOpt,
		placeholder: 'Select a Insured Name'
	});
	$("#AddInsured").prop("checked", false);
}

//enable client(2)/ spouse name to edit
function enableEditSpsNameFld() {
	$("#dfSpsName").prop("readOnly", false);
}

//dfSpsName onchange --> set SpouseName to Insured Tab
function changeSpsTabName(obj) {
	let SpsName = $(obj).val();
	if (isEmptyFld(SpsName)) {
		$("#dfSpsNameError").removeClass("hide").addClass("show");
		return;
	} else {
		$("#dfSpsNameError").removeClass("show").addClass("hide");
		$('#myTab li a.active').text(SpsName.trim());

		//create Insured Delete Icon and Append to Spouse Tab
		let strInsDelIcon = '<i class="fa fa-trash-o checkdb delBtnclr pl-3"' +
			'aria-hidden="true" style="font-size: larger;" ' +
			'id="' + SpsName.trim() + '" title="Delete :' + SpsName.trim() + '" onclick="deletInsured(this)"></i>';
		$('#myTab li a.active').eq(0).append(strInsDelIcon);
		$("#dfSpsName").prop("readOnly", true);
		showSpouseInMenu(SpsName.trim());
	}
}

function setCurrentClientData(nextTab, toShowTab, custId) {
	if (custId.includes("CUST")) {
		getCustomerDetails(custId);
	} else {
		setEmptyClientDetails();
	}

	let firstTab = 0;
	let secondTab = toShowTab - 2;
	$('#myTab a:eq(' + firstTab + ')').tab('show');
	$('#myTab a:eq(' + secondTab + ')').tab('show');
}

function formatClientOpt(client) {

	if (!client.id) {
		return client.text;
	}

	if (client.id == "Select Client/Add New Client") {
		return client.text;
	}

	let image = "vendor/psgisubmit/img/client.png";
	let $clients = $(
		'<span><img style="width:25px" src="' + image + '" class="img-flag" /> ' + client.text + '</span>'
	);
	return $clients;
}

function setCustomerData(data) {
	setCustPersonalData(data);
	setCustAddressData(data);
	setCustContactData(data);
	setCustEmpAndFinData(data);
}

function setEmptyClientDetails() {
	setEmptyPersonalData();
	setEmptyAddressData();
	setEmptyContactData();
	setEmptyEmpandFinData();
	setEmptyEduData();
}

function setEmptySpouseDetails() {
	setEmptySpsPersonalData(selfSpouseDets);
	setEmptySpsAddrData(selfSpouseDets);
	setEmptySpsContactData(selfSpouseDets);
	setEmptySpsEmpandFinData(selfSpouseDets);
	setEmptySpsEduData(selfSpouseDets);
}

function setInsuredData(custId, custName, nric) {
	let name = String(custName);
	let id = String(custId);
	let nricValue = String(nric);
	//alert(nric);

	addNewInsured(id.substring(1, id.length - 1), name.substring(1, name.length - 1));

	$('#' + nricValue.substring(1, nricValue.length - 1)).remove();
}

//Get customer details
function getCustomerDetails(custId) {
	$.ajax({
		url: "CustomerDetails/getDataById/" + custId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			setTimeout(function () {
				setCustomerData(data);
			}, 500);
		},
		error: function (xhr, textStatus, errorThrown) {
			//alert("Error");
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
}

//Get all customer data
function getAllCustomerData() {
	let col0 = "", col1 = "", col2 = "", col3 = "";

	let selfName = $("#dfSelfName").val();
	$.ajax({
		url: "CustomerDetails/getAllData",
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			// setNricSearchListData(data[i]);
			//}
			for (let custIdx = 0; custIdx < data.length; custIdx++) {

				col0 = data[custIdx].custName;
				col1 = data[custIdx].dob;
				col2 = data[custIdx].resHandPhone;
				col3 = data[custIdx].custId;

				$("#selFldNRICName").find('optgroup#searchExistGrp').append('<option value="' + col3 + '" >' + col0 + '</option>');
			}
			//disable to choose current client name
			let selfName = $("#dfSelfName").val();
			$('#selFldNRICName option:contains("' + selfName + '")').prop("disabled", "disabled");
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function setNricSearchListData(selfSpouseData) {
	let selfNric = client.dfSelfNric;
	let spsNric = client.dfSpsNric;
	let custNric = selfSpouseData.nric
	if (custNric == null || custNric == undefined) {
		custNric = "";
	} if (spsNric == null || spsNric == undefined) {
		spsNric = "";
	}
	if (!selfNric.includes(custNric) && !spsNric.includes(custNric)) {
		let li = '<li><a href="#" id=' + selfSpouseData.nric + ' onclick="setInsuredData(/' + selfSpouseData.custId + '/,/' + selfSpouseData.custName + '/,/' + selfSpouseData.nric + '/)">' + selfSpouseData.nric + '</a></li>';
		$("#searchListItems").append(li);
	}
}

$('#selFldNRICName').select2({
	placeholder: "---Add insured Details---",
	templateResult: formatClientOpt,
});

function formatClientOpt(client) {

	if (!client.id) {
		return client.text;
	}

	if (client.id == "Select Client/Add New Client") {
		return client.text;
	}

	let image = "vendor/psgisubmit/img/client.png";
	let $clients = $(
		'<span><img style="width:25px" src="' + image + '" class="img-flag" /> ' + client.text + '</span>'
	);
	return $clients;
};

$('[data-toggle="popoverPersDetls"]').popover({
	placement: 'top',
	html: true,
	content: function () {
		return $('#popover-contentPg2PersonalDetlsCardNotes').html();
	}
});

function expandAllPage2Accordian() {
	/*  $('#collexpans').on('click', function () { */
	$('#accordion .panel-collapse').collapse('toggle');
	/* }); */
}

//Open active tab based on button clicked
/*   $('#testId').on('click', function() {
    var switchTab = $(this).data('tab');   
    activaTab(switchTab);
    function activaTab(switchTab) {
        $('.nav-tabs a[href="#' + switchTab + '"]').tab('show');
    };
  }); */

// Toggle New/Existing Customer
var custType = $('#customer-type'),
	newCust = $('.new-customer'),
	existCust = $('.existing-customer'),
	createAccBtn = $('.create-account'),
	verifyAccBtn = $('.verify-account');

custType.val($(this).is(':checked'))
	.change(function () {
		if ($(this).is(':checked')) {
			newCust.fadeToggle(400, function () { // Hide Full form when checked
				existCust.fadeToggle(500); //Display Small form when checked
				createAccBtn.toggleClass('hide');
				verifyAccBtn.toggleClass('hide');
			});
		} else {
			existCust.fadeToggle(400, function () { //Hide Small form when unchecked
				newCust.fadeToggle(500); //Display Full form when unchecked
				createAccBtn.toggleClass('hide');
				verifyAccBtn.toggleClass('hide');
			});
		}
	});

$("#addNewClient2Btn").on("click", function () {
	/*$('.modal').modal('hide');
	console.log( $(".nav-tabs").children().length );
	var nextTab = $('#myTab li').length+1;
		var toShowTab = $('#myTab li').length-2;
	
$('#myTab').find("a.nav-link").removeClass("active");
$('#myTab').find(' > li:nth-last-child(3)').before('<li class="nav-item"> <a class="nav-link active" id="client'+nextTab+'-tab" data-toggle="tab" href= "#tab'+nextTab+'" role="tab" aria-controls="client'+nextTab+'" aria-selected="false"> Salim'+nextTab+'</a> </li>');
	
	// create the tab content
	
$('#myTabContent').find("div.tab-pane").removeClass("active");
 
	 $('<div class="tab-pane fade show active" id="tab'+nextTab+'" role="tabpanel" aria-labelledby="client'+nextTab+'-tab" > '+$("#myTabContent").find("div.tab-pane").html()+'</div>').appendTo('#myTabContent');
	// make the new tab active
 
	$('#myTab a:eq('+toShowTab+')').tab('show');*/
	//newly added
	let srchFlag = $("#SrchInsured").is(":checked")
	let addFlag = $("#AddInsured").is(":checked")
	if (srchFlag == true) {
		if (!validateInsSrchDels()) { return; }
		createInsuredTab();
	}
	if (addFlag == true) {
		$("#selFldNRICNameError").removeClass("show").addClass("hide");
		addNewInsuredClnt($("#AddInsured"));

	}
});

function createInsuredTab() {
	let newinsured = $('#selFldNRICName option:selected').text();
	let newinusredcustid = $('#selFldNRICName option:selected').val();

	let nextTab = $('#myTab li').length + 1;

	$('#myTab').find("a.nav-link").removeClass("active");

	//$('#myTab').find('li:nth-last-child(2)').before('<li class="nav-item" style="list-style-type:none;"><a class="nav-link active" id="client'+nextTab+'-tab" data-toggle="tab" href= "#tab'+nextTab+'" role="tab" aria-controls="client'+nextTab+'" aria-selected="false"> '+newinsured+'</a> </li>');
	$('#myTab').find('li:nth-last-child(2)').before('<li class="nav-item" style="list-style-type:none;"><a class="nav-link active" id="client' + nextTab + '-tab" data-toggle="tab" href= "#tab' + nextTab + '" role="tab" aria-controls="client' + nextTab + '" aria-selected="false"> ' + newinsured + '&nbsp;<i class="fa fa-trash-o delBtnclr pl-3" aria-hidden="true" style="font-size: larger;" id="' + newinsured + '" title="Delete : ' + newinsured + '" onclick="deletInsured(this)" ></i></a> </li>');

	$('#myTabContent').find("div.tab-pane").removeClass("active");

	//		var dynamictab = $("#divSpouseDetsElems").html();
	let dynamictab = $("#divSpouseDetsElems");
	$("#divSpouseDetsElems").removeClass("d-none");

	//		$('<div class="tab-pane fade show active" id="tab'+nextTab+'" role="tabpanel" aria-labelledby="client'+nextTab+'-tab" > '+dynamictab+'</div>').appendTo('#myTabContent');
	$('<div class="tab-pane fade show active" id="tab' + nextTab + '" role="tabpanel" aria-label="client' + nextTab + '-tab" ></div>').prepend(dynamictab).appendTo('#myTabContent');

	if (newinusredcustid != "_addnew_" && !isEmpty(newinusredcustid)) {
		//getClientById(newinusredcustid);
		getCustomerDetails(newinusredcustid);
	}

	$("#dfSpsName").prop("readOnly", true);

	showSpouseInMenu(newinsured);

	$('.modal').modal('hide');
	$('#myTab a:eq(client' + nextTab + '-tab)').tab('show');

	if ($('#myTab li').length == 5) {
		$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor", "not-allowed");
		$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle", "");
		$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target", "");
	}
}

$('#myModalTest').on('shown.bs.modal', function () {
	// Load up a new modal...
	///init();
	//mode(true)
	//load();
});

$("#addNewClient2Btn").bind("change", function () {
	let thisval = $("#addNewClient2Btn").val();
	if (isEmpty(thisval) || thisval == "_addnew_") {

		$("#selFldNRICName").closest("form-group").addClass("err-fld");
	} else {
		$("#selFldNRICName").closest("form-group").removeClass("err-fld");
	}
});

//selfNRICName onchnge hide Error Mesage
$("#selFldNRICName").bind("change", function () {
	let newinusredcustid = $('#selFldNRICName option:selected').val();
	if (isEmptyFld(newinusredcustid)) {
		$("#selFldNRICNameError").removeClass("hide").addClass("show");
		return;
	} else {
		$("#selFldNRICNameError").removeClass("show").addClass("hide");
	}
});

function validateInsSrchDels() {
	let newinusredcustid = $('#selFldNRICName option:selected').val();
	if (isEmptyFld(newinusredcustid)) {
		$("#selFldNRICNameError").removeClass("hide").addClass("show");
		return;
	} else {
		$("#selFldNRICNameError").removeClass("show").addClass("hide");
	}
	return true;
}

function enableIntrprt(obj) {

	//$("#tempIntprtDiv").find(":input").prop("disabled",function(i, v) {return !v; });
	let flag = $(obj).is(":checked");

	//enable lanuguage use Option (English) if interpretor flsg is selected
	if (flag == true) {
		$("#htflaneng").trigger("click");
		$("#tempIntprtDiv").find(":input").prop("disabled", false);
		$("#tempIntprtDiv").find("input:text,select").val("");
	} else {
		$("#htflaneng").prop("checked", false);
		$("#htflaneng").val('');
		$("#cdLanguages").val('');
		$("#tempIntprtDiv").find(":input").prop("disabled", true);
		$("#tempIntprtDiv").find("input:text,select").val("");
	}
	// $("#tempIntprtDiv").find(":input").prop("disabled",function(i, v) { return !v; });
	//$("#tempIntprtDiv").find("input:text,select").val("");
}

function expandAll(thisobj) {

	let txt = $(thisobj).text();

	$(thisobj).html(function (i, text) {
		if (text.indexOf("Expand All") > 0)
			return '<i class="fa fa-compress" aria-hidden="true" style="color:#ff9800;"></i>&nbsp;Collapse All';
		if (text.indexOf("Collapse All") > 0)
			return '<i class="fa fa-expand" aria-hidden="true" style="color:#656767;"></i>&nbsp;Expand All';
	});

	//$('div.panel-collapse').toggleClass('show');
	// $('div.panel-heading').find('.panel-title').find('a').toggleClass('collapsed');

	if (txt.indexOf("Expand All") > 0) {
		$('div.panel-collapse').addClass('show');
		$('div.panel-heading').find('.panel-title').find('a').removeClass('collapsed');
	}
	if (txt.indexOf("Collapse All") > 0) {
		$('div.panel-collapse').removeClass('show');
		$('div.panel-heading').find('.panel-title').find('a').addClass('collapsed');
	}
}

//CRUD operation
//Get client details from particular field
/*Delete tab*/
function deleteTab() {
	let spouseName = client.dfSpsName;

	swal.fire({
		title: "Are you sure?",
		text: "Your spouse details will be removed !!!",
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No,Cancel",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then((result) => {
		if (result.isConfirmed) {
			//setEmptySpouseDetails();
			setEmptyClientDetails();
			$('a[href="#client1"]').click();

			//$( '#myTab' ).tabs('select', 0);
			swal.fire("Deleted!", "Client Name : (" + spouseName + ") has been deleted.", "success");

			$("#SPS").remove();
			//setEmptySpouseDetails();
			//updateAjaxCall(client);
		} else {
			swal.fire("Cancelled", "Client Name : (" + spouseName + ")  Product Details is safe :)", "error");
		}
	});
}

function deletInsured(obj) {

	let parentTabli = $(obj).closest("li")
	let parentTab = $(obj).closest("li").find("a.nav-link");
	let tabId = parentTab.prop("id");

	let strClient = $(obj).attr("id");
	//alert($(obj).closest(".ProdDetlsSec").children().eq(1).html())
	swal.fire({
		title: "Are you sure?",
		text: "Do you want to delete this Client : (" + strClient + ") details!",
		type: "warning",
		showCancelButton: true,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, delete it!",
		cancelButtonText: "No,Cancel",
		closeOnConfirm: false,
		closeOnCancel: false
	}).then((result) => {
		if (result.isConfirmed) {
			//clear all Spouse Details
			setEmptySpouseDetails();
			// date Picker Values Clear of Spouse
			$("#divSpouseDetsElems").find('#simple-date2 .input-group.date').datepicker("update", new Date());
			$("#divSpouseDetsElems").find("input,textarea,select").val("");
			$("#divSpouseDetsElems").find(':input').removeAttr("readOnly");
			$("#divSpouseDetsElems").find(':input').removeAttr("disabled");
			$("#divSpouseDetsElems").find('input:checkbox').prop('checked', false);
			$("#divSpouseDetsElems").find('input:radio').prop('checked', false);

			//End
			let spouseElemsDiv = $("#divSpouseDetsElems");
			$(spouseElemsDiv).addClass("d-none");
			$(spouseElemsDiv).appendTo("#divSpouseDetsElemsParent")

			hideSpouseInMenu();

			$("#myTabContent").find('div[aria-label=' + tabId + ']').remove();
			$(parentTabli).remove();

			$("#myTab").find("li:eq(0)>a").addClass("active");
			$("#myTabContent").find("div.tab-pane").addClass("active").addClass("show");

			$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor", "pointer");
			$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle", "modal");
			$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target", "#new2ModalScrollable");

			saveData($(this), 'kycHome', false, true);

			swal.fire("Deleted!", "Client Name : (" + strClient + ") has been deleted.", "success");
		} else {
			swal.fire("Cancelled", "Client Name : (" + strClient + ") detail is safe :)", "error");
		}
	});
}

//Set and get PersonalData
var arrPersonalDet = ["dfSelfName", "dfSelfNric", "dfSelfDob",
	"dfSelfMartsts", "dfSelfBirthCntry"];
var arrSpouseDet = ["dfSpsName", "dfSpsNric", "dfSpsDob",
	"dfSpsMartsts", "dfSpsBirthCntry"];

function getPersonalData(client) {
	for (let persIdx = 0; persIdx < arrPersonalDet.length; persIdx++) {
		client[arrPersonalDet[persIdx]] = document.getElementsByName(arrPersonalDet[persIdx])[0].value;
	}
	let nationality = $('input[name="dfSelfNationality"]:checked').val();
	if (nationality == "Others") {
		client.dfSelfNationality = nationality;
		client.dfSelfNatyDets = $("#dfSelfNatyDets").val();
	} else {
		client.dfSelfNationality = nationality;
	}

	let gender = $('input[name="dfSelfGender"]:checked').val();
	client.dfSelfGender = gender;
	//client.dfSelfDob=formatDbDate(client.dfSelfDob);
	return client;
}

function getSpousePersonalData(spouse) {
	for (let spsIdx = 0; spsIdx < arrSpouseDet.length; spsIdx++) {
		spouse[arrSpouseDet[spsIdx]] = document.getElementsByName(arrSpouseDet[spsIdx])[0].value;
	}

	//Nationality
	let gender = $('input[name="dfSpsGender"]:checked').val();
	spouse.dfSpsGender = gender;
	let nationality = $('input[name="dfSpsNationality"]:checked').val();
	if (nationality == "Others") {
		spouse.dfSpsNationality = nationality;
		spouse.dfSpsNatyDets = $("#dfSpsNatyDets").val();
	} else {
		spouse.dfSpsNationality = nationality;
	}
	return spouse;
}

function setPersonalData(clientData) {
	for (let perIdx = 0; perIdx < arrPersonalDet.length; perIdx++) {
		if (clientData[arrPersonalDet[perIdx]] != undefined) {
			document.getElementsByName(arrPersonalDet[perIdx])[0].value = clientData[arrPersonalDet[perIdx]];
		}
	}
	if (!isEmpty(clientData.dfSelfName)) {
		$("#dfSelfName").prop("readonly", true);
	} if (!isEmpty(clientData.dfSelfNric)) {
		$("#dfSelfNric").prop("readonly", true);
	}

	//Tab name as self name
	$("#hdnSessDataFormId").val(clientData.dataFormId);
	$("#client1-tab").prop('id', clientData.dataFormId);
	$("#" + clientData.dataFormId).html(clientData.dfSelfName);

	//Gender
	setGender(clientData.dfSelfGender, "dfSelfGender", "Self");
	//Dob
	setDob(clientData.dfSelfDob, "dfSelfDob", "simple-date1");
	if (!isEmpty(clientData.dfSelfDob)) {

		$('#dfSelfDob').prop("readonly", true);
	}

	//Nationality
	setNationality(clientData.dfSelfNationality, 'Self', clientData.dfSelfNatyDets);
}

function setSpousePersonalData(spouse) {
	for (let spouseIdx = 0; spouseIdx < arrSpouseDet.length; spouseIdx++) {
		document.getElementsByName(arrSpouseDet[spouseIdx])[0].value = spouse[arrSpouseDet[spouseIdx]];
	}

	setDob(spouse.dfSpsDob, "dfSpsDob", "simple-date2");
	setNationality(spouse.dfSpsNationality, 'Sps', spouse.dfSpsNatyDets);

	//Gender
	setGender(spouse.dfSpsGender, "dfSpsGender", 'Sps');
	//$("#SPS").text(spouse.dfSpsName);
}

function setGender(value, id, selfOrSps) {
	$("#radBtn" + selfOrSps + "male").prop("checked", false);
	$("#radBtn" + selfOrSps + "female").prop("checked", false);

	if (value == "F") {
		$("INPUT[name=" + id + "]").val([value]);
		$("#radBtn" + selfOrSps + "female").prop("checked", true);
		$("#radBtn" + selfOrSps + "male").prop("checked", false);
	} else if (value == "M") {
		$("INPUT[name=" + id + "]").val([value]);
		$("#radBtn" + selfOrSps + "male").prop("checked", true);
		$("#radBtn" + selfOrSps + "female").prop("checked", false);
	}
}

function setDob(value, id, selfOrSps) {
	//var dob=new Date(selfDob);
	//document.getElementsByName("dfSelfDob")[0].value=formatDate(dob);
	$("#" + id).val(value);
	// set updated date in datepicker calendar
	$('#' + selfOrSps + ' .input-group.date').datepicker("update", value);
}

function setNationality(nationality, spsOrSelf, nationalityOthers) {
	if (nationality != "" && nationality != null) {
		if (nationality != "SG" && nationality != "SG-PR") {

			$("input[name='df" + spsOrSelf + "Nationality'][value='Others']").prop('checked', true);
			// $("#df"+spsOrSelf+"NationalityOth").prop('checked',true);

			// document.getElementById("df"+spsOrSelf+"NatyDets").style.display = 'block';
			$("#df" + spsOrSelf + "NatyDets").removeClass("invisible").addClass("show");
			document.getElementById("df" + spsOrSelf + "NatyDets").value = nationalityOthers;
			// $("#selectNationOther").val(nationality);
		} else {
			$("input[name='df" + spsOrSelf + "Nationality'][value='" + nationality + "']").prop('checked', true);
			/*$("#df"+spsOrSelf+"NationalitySign").prop('checked',false);
			$("#df"+spsOrSelf+"NationalityOth").prop('checked',true);
			document.getElementById("df"+spsOrSelf+"NatyDets").value="";*/
			// document.getElementById("df"+spsOrSelf+"NatyDets").style.display = 'none';
			$("#df" + spsOrSelf + "NatyDets").removeClass("show").addClass("invisible");

			//$("#selectNationOther").val("");
			//$("input[name=dfSelfNationality][value=" + nationality + "]").attr('checked', true);
		}
	}
}

/*function fnaNaltyFlg(chkbox,Ele){
var chkd = chkbox.checked;
var val = $(chkbox).val();
	
	
if(chkd && val != "SG" && val != "SG-PR"){
	document.getElementById(Ele).style.display="block";
}else{
	document.getElementById(Ele).style.display="none";
//		$("#"+Ele+"").val('');
}
}*/

function fnaNaltyFlg(chkbox, Ele) {
	let chkd = chkbox.checked;
	let val = $(chkbox).val();

	if ((chkd && val == "SG") || (chkd && val == "SG-PR")) {
		$("#" + Ele).removeClass("show").addClass("invisible");
	} else {
		$("#" + Ele).removeClass("invisible").addClass("show");
		//		$("#"+Ele+"").val('');
	}
}

function setEmptyPersonalData() {
	for (let ePerIdx = 0; ePerIdx < arrPersonalDet.length; ePerIdx++) {
		document.getElementsByName(arrPersonalDet[ePerIdx])[0].value = "";
	}
	$('#exampleRadios11').prop('checked', true);
	document.getElementById("selectNationOther").style.display = 'none';
	document.getElementsByName("dataFormId")[0].value = "SPS";
	$("#new").prop('id', 'SPS');
}

function setEmptySpsPersonalData(client) {
	for (let eSpsPerIdx = 0; eSpsPerIdx < arrSpouseDet.length; eSpsPerIdx++) {
		client[arrSpouseDet[eSpsPerIdx]] = "";
	}
}

//Set and get customer details
function setCustPersonalData(customerData) {
	document.getElementsByName("dfSpsName")[0].value = customerData.custName;
	document.getElementsByName("dfSpsNric")[0].value = customerData.nric;
	document.getElementsByName("dfSpsMartsts")[0].value = customerData.maritalStatus;
	//document.getElementsByName("dataFormId")[0].value="SPS";
	//$("#"+customerData.custId).val("SPS");

	//$("#"+customerData.custId).prop('id','SPS');
	//Gender
	setGender(customerData.sex, "dfSpsGender", "Sps");
	//Dob
	setDob(customerData.dob, "dfSpsDob", "simple-date2");

	//Nationality
	if ($("#dfSpsNationalitySign").val() == "") {
		$("#dfSpsNationalitySign").val("SG");
	}
	if ($("#dfSpsNationalitySign-PR").val() == "") {
		$("#dfSpsNationalitySign-PR").val("SG-PR");
	}
	if ($("#dfSpsNationalityOth").val() == "") {
		$("#dfSpsNationalityOth").val("Others");
	}
	setNationality(customerData.nationality, 'Sps', customerData.nationality);
}
//End

//Set and get address details

var arrAddrDet = ["dfSelfMailAddr", "dfSelfHomeAddrr"];
var dfSelfMailAddrFlg;
var arrAddrData = ["dfSpsMailAddr", "dfSpsHomeAddrr", "dfSpsMailAddrFlg"];

function setEmptySpsAddrData(client) {
	for (let eSpsAddrIdx = 0; eSpsAddrIdx < arrAddrData.length; eSpsAddrIdx++) {
		client[arrAddrData[eSpsAddrIdx]] = "";
	}
}

function getMailAddressCheckBox() {
	$('#time-01').prop('checked', true);
	let address = document.getElementsByName("dfSelfHomeAddr")[0].value;
	document.getElementsByName("dfSelfMailAddr")[0].value = address;
	$('#time-02').prop('checked', false);
	dfSelfMailAddrFlg = "Y";
}

function getDiffAddrCheckBox() {
	$('#time-02').prop('checked', true);
	document.getElementsByName("dfSelfMailAddr")[0].value = "";
	$('#time-01').prop('checked', false);
	dfSelfMailAddrFlg = "N";
}

function getAddressData(client) {
	client.dfSelfMailAddr = document.getElementsByName("dfSelfMailAddr")[0].value;
	client.dfSelfHomeAddr = document.getElementsByName("dfSelfHomeAddr")[0].value;
	if (client.dfSelfMailAddr == client.dfSelfHomeAddr) {
		$("#chkSelfSameAddrFlg").prop('checked', true);
		$("#chkSelfNoSameAddrFlg").prop('checked', false);
	} else {
		$("#chkSelfSameAddrFlg").prop('checked', false);
		$("#chkSelfNoSameAddrFlg").prop('checked', true);
	}

	let sameAsRegAddr = $('input[name="dfSelfSameAsregadd"]:checked').val();
	if (sameAsRegAddr == "Y") {
		client.dfSelfMailAddrFlg = sameAsRegAddr;
	} else {
		client.dfSelfMailAddrFlg = "N";
	}

	return client;
}

function getSpouseAddressData(client) {
	client.dfSpsMailAddr = document.getElementsByName("dfSpsMailAddr")[0].value;
	client.dfSpsHomeAddr = document.getElementsByName("dfSpsHomeAddr")[0].value;
	let sameAsRegAddr;
	if (client.dfSpsMailAddr == client.dfSpsHomeAddr) {
		$("#chkSpsSameAddrFlg").prop('checked', true);
		$("#chkSpsNoSameAddrFlg").prop('checked', false);
		sameAsRegAddr = "Y";
	} else {
		$("#chkSpsSameAddrFlg").prop('checked', false);
		$("#chkSpsNoSameAddrFlg").prop('checked', true);
	}

	// var sameAsRegAddr = $('input[name="chkSpsSameAddrFlg"]:checked').val();
	if (sameAsRegAddr == "Y") {
		client.dfSpsMailAddrFlg = sameAsRegAddr;
	} else {
		client.dfSpsMailAddrFlg = "N";
	}
	return client;
}

function setSpouseAddressData(spouse) {

	let mailAddrFlg = spouse.dfSpsMailAddrFlg;

	document.getElementsByName("dfSpsMailAddr")[0].value = spouse.dfSpsMailAddr;
	document.getElementsByName("dfSpsHomeAddr")[0].value = spouse.dfSpsHomeAddr;
	if (mailAddrFlg == "Y") {
		$("#chkSpsSameAddrFlg").prop('checked', true);
		$("#chkSpsNoSameAddrFlg").prop('checked', false);
		$("#dfSpsMailAddr").prop('disabled', true);
	}
	else {
		$("#chkSpsNoSameAddrFlg").prop('checked', true);
		$("#chkSpsSameAddrFlg").prop('checked', false);
		$("#dfSpsMailAddr").prop('disabled', false);
	}
}

function setAddressData(clientData) {
	setAddress(clientData, "Self");
}

function setEmptyAddressData() {
	document.getElementsByName("dfSelfHomeAddr")[0].value = "";
	document.getElementsByName("dfSelfMailAddr")[0].value = "";
	$("#time-01").prop('checked', false);
	$("#time-02").prop('checked', false);
}

function setAddress(clientData, selfOrSps) {
	let address = clientData.resAddr1 + ',' + clientData.resAddr2
		+ ',' + clientData.resAddr3 + ',' + clientData.resCity
		+ ',' + clientData.resCountry + ',' + clientData.resPostalCode;

	let homeAddr = clientData.dfSelfHomeAddr;
	let mailAddrFlg = clientData.dfSelfMailAddrFlg;
	let offAddr = clientData.dfSelfOffAddr;
	if (offAddr != null && offAddr != "") {
		document.getElementsByName("dfSelfHomeAddr")[0].value = clientData.dfSelfOffAddr;
		$("#chkSelfSameAddrFlg").trigger("click");
	}

	else if (homeAddr != null && homeAddr != "") {
		/*for(var i=0;i<arrAddrDet.length;i++){
					 document.getElementsByName(arrAddrDet[i])[0].value=clientData[arrAddrDet[i]];
		}*/

		document.getElementsByName("df" + selfOrSps + "HomeAddr")[0].value = clientData.dfSelfHomeAddr;
		$("#chkSelfSameAddrFlg").trigger("click");
		//$("#chkSelfSameAddrFlg").prop('checked',true);
		//sameAsResiAddr(this,'chkSelfNoSameAddrFlg');
		// 
		if (!isEmpty(mailAddrFlg)) {
			if (mailAddrFlg == "Y") {
				$("#chkSelfSameAddrFlg").prop('checked', true);
				$("#chkSelfNoSameAddrFlg").prop('checked', false);
				$("#dfSelfMailAddr").prop('disabled', true);
			}
			else {
				$("#chkSelfNoSameAddrFlg").trigger("click");
				document.getElementsByName("df" + selfOrSps + "MailAddr")[0].value = clientData.dfSelfMailAddr;
				//$("#chkSelfNoSameAddrFlg").prop('checked',true);
				$("#chkSelfSameAddrFlg").prop('checked', false);
				$("#dfSelfMailAddr").prop('disabled', false);
			}
		}
	} else {
		document.getElementsByName("df" + selfOrSps + "HomeAddr")[0].value = address;
		document.getElementsByName("df" + selfOrSps + "MailAddr")[0].value = address;
	}
}

function enableSelfSpsSameAddrFlg(obj, client) {
	if (client == 'self') {
		let selfHomeaddr = $("#" + obj).val();
		let flg = $("#chkSelfSameAddrFlg").is(':checked');
		if ((selfHomeaddr.length >= 0) && (flg == true)) {
			$("#dfSelfMailAddr").val(selfHomeaddr);
			if (selfHomeaddr != "") {
				$("#dfSelfMailAddr").trigger("change");
			}
			$("#chkSelfSameAddrFlg").val("Y");
		}
	}
	if (client == 'spouse') {
		let spsHomeaddr = $("#" + obj).val();
		let flg = $("#chkSpsSameAddrFlg").is(':checked');
		if ((spsHomeaddr.length >= 0) && (flg == true)) {
			$("#dfSpsMailAddr").val(spsHomeaddr);
			if (spsHomeaddr != "") {
				$("#dfSpsMailAddr").trigger("change");
			}
			$("#chkSpsSameAddrFlg").val("Y");
		}
	}
}

//Set customer set and get details
function setCustAddressData(data) {
	setAddress(data, "Sps");
}
//End

//Set and get  contact data
var arrContactDet = ["dfSelfPersEmail", "dfSelfMobile", "dfSelfHome", "dfSelfOffice"];
var arrSpsContactData = ["dfSpsHp", "dfSpsPersEmail", "dfSpsHome", "dfSpsOffice"];

function setEmptySpsContactData(client) {
	for (let eSpsConIdx = 0; eSpsConIdx < arrSpsContactData.length; eSpsConIdx++) {
		client[arrSpsContactData[eSpsConIdx]] = "";
	}
}

function getContactData(client) {
	for (let arrConIdx = 0; arrConIdx < arrContactDet.length; arrConIdx++) {
		client[arrContactDet[arrConIdx]] = document.getElementsByName(arrContactDet[arrConIdx])[0].value;
	}
	return client;
}

function getSpouseContactData(client) {
	for (let spsConIdx = 0; spsConIdx < arrSpsContactData.length; spsConIdx++) {
		client[arrSpsContactData[spsConIdx]] = document.getElementsByName(arrSpsContactData[spsConIdx])[0].value;
	}
	return client;
}

function setContactData(clientData) {
	for (let conIdx = 0; conIdx < arrContactDet.length; conIdx++) {
		if (clientData[arrContactDet[conIdx]] != undefined) {
			document.getElementsByName(arrContactDet[conIdx])[0].value = clientData[arrContactDet[conIdx]];
		}
	}
}

function setSpouseContactData(client) {
	for (let spsContIdx = 0; spsContIdx < arrSpsContactData.length; spsContIdx++) {
		document.getElementsByName(arrSpsContactData[spsContIdx])[0].value = client[arrSpsContactData[spsContIdx]];
	}
}

function setEmptyContactData() {
	for (let eConIdx = 0; eConIdx < arrContactDet.length; eConIdx++) {
		document.getElementsByName(arrContactDet[eConIdx])[0].value = "";
	}
}

//Set and get customer details
function setCustContactData(custData) {
	document.getElementsByName("dfSpsPersEmail")[0].value = custData.emailId;
	document.getElementsByName("dfSpsHp")[0].value = custData.othHandPhone;
}
//End

//Set and get EmpFinDetails

var arrEmpAndFinDet = ["dfSelfCompname", "dfSelfOccpn", "dfSelfAnnlIncome",
	"dfSelfBusinatr", "dfSelfBusinatrDets", "dfSelfFundsrc", "dfSelfFundsrcDets"];

var arrSpsEmpAndFinDet = ["dfSpsCompname", "dfSpsOccpn", "dfSpsAnnlIncome",
	"dfSpsBusinatr", "dfSpsBusinatrDets", "dfSpsFundsrc", "dfSpsFundsrcDets"];

function setEmptySpsEmpandFinData(client) {
	for (let spsEmpIdx = 0; spsEmpIdx < arrSpsEmpAndFinDet.length; spsEmpIdx++) {
		client[arrSpsEmpAndFinDet[spsEmpIdx]] = "";
	}
}

function getEmpAndFinData(client) {
	for (let spsEmIdx = 0; spsEmIdx < arrEmpAndFinDet.length; spsEmIdx++) {
		client[arrEmpAndFinDet[spsEmIdx]] = document.getElementsByName(arrEmpAndFinDet[spsEmIdx])[0].value;
		if ($("#dfSelfBusinatr").val() == "Others") {
			$("#dfSelfBusNatOthSec").removeClass("invisible").addClass("show");
		}
	}
	return client;
}

//Get spouse data
function getSpouseEmpAndFinData(client) {
	for (let gSpsEmpIdx = 0; gSpsEmpIdx < arrSpsEmpAndFinDet.length; gSpsEmpIdx++) {
		client[arrSpsEmpAndFinDet[gSpsEmpIdx]] = document.getElementsByName(arrSpsEmpAndFinDet[gSpsEmpIdx])[0].value;
		if ($("#dfSpsBusinatr").val() == "Others") {
			$("#dfSpsBusNatOthSec").removeClass("invisible").addClass("show");
		}
	}
	return client;
}

function setEmpAndFinData(clientData) {
	for (let sEmpIdx = 0; sEmpIdx < arrEmpAndFinDet.length; sEmpIdx++) {
		if (clientData[arrEmpAndFinDet[sEmpIdx]] != undefined) {
			document.getElementsByName(arrEmpAndFinDet[sEmpIdx])[0].value = clientData[arrEmpAndFinDet[sEmpIdx]];
			if ($("#dfSelfBusinatr").val() == "Others") {
				$("#dfSelfBusNatOthSec").removeClass("invisible").addClass("show");
			}
		}
	}

	let srcFund = clientData.dfSelfFundsrc;

	if (srcFund != "" && srcFund != null && srcFund != undefined) {
		let srcFundData = JSON.parse(srcFund);
		setSrcFundData(srcFundData, 'Self');
	}
}

function setSrcFundData(srcFundData, selfOrSps) {
	$("#chkCD" + selfOrSps + "ErndIncm").prop('checked', false);
	$("#chkCD" + selfOrSps + "InvIncm").prop('checked', false);
	$("#chkCD" + selfOrSps + "PrsnlIncm").prop('checked', false);
	$("#chkCD" + selfOrSps + "CPFSvngs").prop('checked', false);
	$("#chkCD" + selfOrSps + "SrcIncmOthrs").prop('checked', false);

	if (srcFundData.CERN == "Y") {
		$("#chkCD" + selfOrSps + "ErndIncm").prop('checked', true);
	}
	if (srcFundData.CINV == "Y") {
		$("#chkCD" + selfOrSps + "InvIncm").prop('checked', true);
	}
	if (srcFundData.CPRS == "Y") {
		$("#chkCD" + selfOrSps + "PrsnlIncm").prop('checked', true);
	}
	if (srcFundData.CCPF == "Y") {
		$("#chkCD" + selfOrSps + "CPFSvngs").prop('checked', true);
	}
	if (srcFundData.COTH == "Y") {
		$("#chkCD" + selfOrSps + "SrcIncmOthrs").prop('checked', true);
		$("#df" + selfOrSps + "FundsrcDets").prop('disabled', false);
	}
}

function setSpouseEmpAndFinData(client) {
	for (let sSpsEmpIdx = 0; sSpsEmpIdx < arrSpsEmpAndFinDet.length; sSpsEmpIdx++) {
		document.getElementsByName(arrSpsEmpAndFinDet[sSpsEmpIdx])[0].value = client[arrSpsEmpAndFinDet[sSpsEmpIdx]];
		if ($("#dfSpsBusinatr").val() == "Others") {
			$("#dfSpsBusNatOthSec").removeClass("invisible").addClass("show");
		}
	}
	let srcFund = client.dfSpsFundsrc;

	if (srcFund != "" && srcFund != null && srcFund != undefined) {
		let srcFundData = JSON.parse(srcFund);
		setSrcFundData(srcFundData, 'Sps');
	}
}

var arrSrcFund = ["CERN", "CINV", "CPRS", "CCPF", "COTH"];

function setEmptyEmpandFinData() {
	for (let eEmpIdx = 0; eEmpIdx < arrEmpAndFinDet.length; eEmpIdx++) {
		document.getElementsByName(arrEmpAndFinDet[eEmpIdx])[0].value = "";
	}
	let srcFund = {};
	for (let srcIdx = 0; srcIdx < arrSrcFund.length; srcIdx++) {
		srcFund[arrSrcFund[srcIdx]] = "N";
	}
	let srcFundData = JSON.stringify(srcFund);
	setSrcFundData(JSON.parse(srcFundData));
}

//Set and get customer details
function setCustEmpAndFinData(custData) {
	document.getElementsByName("dfSpsCompname")[0].value = custData.companyName;
	document.getElementsByName("dfSpsOccpn")[0].value = custData.occpnDesc;
	document.getElementsByName("dfSpsAnnlIncome")[0].value = custData.income;
	document.getElementsByName("dfSpsBusinatr")[0].value = custData.businessNatr;
	if (custData.businessNatr == "Others") {
		$("#dfSpsBusNatOthSec").removeClass("invisible").addClass("show");
		$("#dfSpsBusinatrDets").val(custData.businessNatrDets);
	}
}
//End

//Set and get education details
var arrEduDet = ["dfSelfEduLevel", "dfSelfEngSpoken", "dfSelfEngWritten"];

var arrSpsEduDet = ["dfSpsEduLevel", "dfSpsEngSpoken", "dfSpsEngWritten"];
function setEmptySpsEduData(client) {
	for (let eSpsIdx = 0; eSpsIdx < arrSpsEduDet.length; eSpsIdx++) {
		client[arrSpsEduDet[eSpsIdx]] = "";
	}
}

function getEduData(client) {
	client.dfSelfEduLevel = $('input[name="dfSelfEduLevel"]:checked').val();
	client.dfSelfEngSpoken = $('input[name="dfSelfEngSpoken"]:checked').val();
	client.dfSelfEngWritten = $('input[name="dfSelfEngWritten"]:checked').val();
	return client;
}

//Get spouse data
function getSpouseEduData(client) {
	client.dfSpsEduLevel = $('input[name="dfSpsEduLevel"]:checked').val();
	client.dfSpsEngSpoken = $('input[name="dfSpsEngSpoken"]:checked').val();
	client.dfSpsEngWritten = $('input[name="dfSpsEngWritten"]:checked').val();
	return client;
}

function setEduData(clientData) {
	if (clientData.dfSelfEduLevel != "") {
		$("#chkClntH" + clientData.dfSelfEduLevel).prop('checked', true);
	}
	setLangData(clientData.dfSelfEngSpoken, clientData.dfSelfEngWritten, "Self");
}

function setSpouseEduData(clientData) {
	if (clientData.dfSpsEduLevel != "") {
		$("#chkSpsH" + clientData.dfSpsEduLevel).prop('checked', true);

	}
	setLangData(clientData.dfSpsEngSpoken, clientData.dfSpsEngWritten, "Sps");
}

//Set SPS/SELF lang data
function setLangData(engSpoken, engWritten, role) {
	$("#df" + role + "EngSpokenY").prop('checked', false);
	$("#df" + role + "EngSpokenN").prop('checked', false);

	$("#df" + role + "EngWrittenY").prop('checked', false);
	$("#df" + role + "EngWrittenN").prop('checked', false);

	if (engSpoken == "Y") {
		$("#df" + role + "EngSpokenY").prop('checked', true);
	} else if (engSpoken == "N") {
		$("#df" + role + "EngSpokenN").prop('checked', true);
	}
	if (engWritten == "Y") {
		$("#df" + role + "EngWrittenY").prop('checked', true);
	} else if (engWritten == "N") {
		$("#df" + role + "EngWrittenN").prop('checked', true);
	}
}

function setEmptyEduData() {
	document.getElementsByName("dfSelfEduLevel")[0].value = "";
	$("#inlineCheckbox1").prop('checked', false);
	$("#inlineCheckbox2").prop('checked', false);
}
//End Edu details

function getClientData(client) {
	client = getPersonalData(client);
	client = getAddressData(client);
	client = getContactData(client);
	client = getEmpAndFinData(client);
	client = getEduData(client);
	return client;
}

function getSelfSpouseData(client) {
	client = getSpousePersonalData(client);
	client = getSpouseAddressData(client);
	client = getSpouseContactData(client);
	client = getSpouseEmpAndFinData(client);
	client = getSpouseEduData(client);
	return client;
}

function setSelfSpouseData(client) {
	setSpousePersonalData(client);
	setSpouseAddressData(client);
	setSpouseContactData(client);
	setSpouseEmpAndFinData(client);
	setSpouseEduData(client);
}

var client = {};

function updateSelfAndSpouseData(clientDets) {
	let dfSpsName = document.getElementsByName("dfSpsName")[0].value;

	//Get Self dets
	clientDets = getClientData(clientDets);

	//Get Spouse dets
	if (dfSpsName != "" && dfSpsName != null) {
		clientDets = getSelfSpouseData(clientDets);
	}
	return clientDets;
}

function updateAjaxCall(client) {
	fnaDetails = getInterprtData(fnaDetails);
	let hdnFnaDetailsStr = $("#hdnFnaDetails").val();
	let hdnFnaDetails = JSON.parse(hdnFnaDetailsStr);
	fnaDetails.mgrEmailSentFlg = hdnFnaDetails.mgrEmailSentFlg;
	client.fnaDetails = fnaDetails;
	client.dataFormId = $("#hdnSessDataFormId").val();
	//alert(JSON.stringify(client));
	$.ajax({
		url: "FnaSelfSpouse/updateData",
		type: "PUT",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(client),
		async: true,
		success: function (data) {
			//alert("success");
		},
		error: function (data) {
			//alert("Error");
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
}

//Get client details from client info page
var fnaDetails = {};
var selfSpouseDets = {};
function getClientDetails(dataFormId) {
	pageLoaderShow();
	let clientData = {};
	$.ajax({
		url: "FnaSelfSpouse/getDataById/" + dataFormId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			selfSpouseDets = data;
			fnaDetails = data.fnaDetails;
			setClientData(data);
			/* if((spsactive =="Y") && (data.dfSpsName!=null && data.dfSpsName!="")){
				addNewInsured("SPS",data.dfSpsName);
				 setSelfSpouseData(data);
			 }else if((spsactive == "YN") && (data.dfSpsName!=null && data.dfSpsName!="")){
					 setSelfSpouseData(data);
			 }*/
			if (data.dfSpsName != null && data.dfSpsName != "") {
				$("#MenuSpsName").text(data.dfSpsName);
				createSpouseTab("dfSpsName", data.dfSpsName);
				setSelfSpouseData(data);
			}
			// pageLoaderHide();
		},
		error: function (xhr, textStatus, errorThrown) {
			//alert("Error");
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
	return clientData;
}

function createSpouseTab(key, value) {
	let nextTab = $('#myTab li').length + 1;
	$('#myTab').find('li:nth-last-child(2)').before('<li class="nav-item" style="list-style-type:none;"><a class="nav-link" id="client' + nextTab + '-tab" data-toggle="tab" href= "#tab' + nextTab + '" role="tab" aria-controls="client' + nextTab + '" aria-selected="false"> ' + value + '&nbsp;<i class="fa fa-trash-o delBtnclr pl-3" aria-hidden="true" style="font-size: larger;" id="' + value + '" title="Delete : ' + value + '" onclick="deletInsured(this)" ></i></a> </li>');

	let dynamictab = $("#divSpouseDetsElems");
	$("#divSpouseDetsElems").removeClass("d-none");

	$('<div class="tab-pane fade show" id="tab' + nextTab + '" role="tabpanel" aria-label="client' + nextTab + '-tab" ></div>').prepend(dynamictab).appendTo('#myTabContent');

	$("#" + key).val(value);
	$("#" + key).prop("readonly", true);
}

function pageLoaderShow() {
	$(".page-loader").show();
	$(".overlay").show();
}

function pageLoaderHide() {
	document.getElementById("pageLoader").style.display = "none";
	document.getElementById("overlayLoader").style.display = "none";
	/* $(".page-loader").hide();
	 $(".overlay").hide();*/
}

//Set client data to corresponding fields
function setClientData(clientData) {
	setPersonalData(clientData);
	setAddressData(clientData);
	setContactData(clientData);
	setEmpAndFinData(clientData);
	setEduData(clientData);
	setInterprtData(clientData.fnaDetails);
}

function formatDate(date) {
	let d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [year, month, day].join('-');
}

function getNationalityShow() {
	document.getElementById('selectNationOther').style.display = 'block';
}

function getNationalityHide() {
	document.getElementById('selectNationOther').style.display = 'none';
}

//Policy E-Submission
function submitPolicy() {
	let principalData = {};
	let principalNames = [];
	$.each($("input[name='principalName']:checked"), function () {
		principalNames.push($(this).val());
	});
	principalData.PrincipalNames = principalNames;
	principalData.fnaId = client.fnaDetails.fnaId;
	principalData.custId = client.fnaDetails.custDetails.custId;
	//	principalData.isubmitData=client;
	principalData.distributorId = client.fnaDetails.custDetails.distributorId;
	//	principalData.ntucjson=ntucjson;
	let ntucJson = {};
	principalData.ntucjson = getNtucJson(ntucJson, client);
	//alert(JSON.stringify(principalData));
	console.log(JSON.stringify(principalData));

	let form = document.createElement("form");
	form.setAttribute("method", "post");
	form.setAttribute("action", "http://localhost:7206/PSGPORTAL/api/v1/esubmit/insurer");

	form.setAttribute("target", "view");

	let hiddenField = document.createElement("input");
	hiddenField.setAttribute("type", "hidden");
	hiddenField.setAttribute("name", "jsonData");
	hiddenField.setAttribute("value", JSON.stringify(principalData));
	form.appendChild(hiddenField);
	document.body.appendChild(form);
	window.open('', 'view', "width=700,height=450,left=100,top=100,resizable=no,scrollbars=yes");

	form.submit();

	//	window.open("http://localhost:7206/PSGPORTAL/api/v1/esubmit/insurer/?jsonData= "+encodeURIComponent(JSON.stringify(principalData)));
	let frmdata = new FormData();
	//frmdata.append("jsonData", principalData);
	/*$.ajax({
	    url: "http://localhost:7206/PSGPORTAL/api/v1/esubmit/insurer",
        type: "POST",
        contentType: "application/json",
//        processData: false,
        // async:false,
        data: JSON.stringify(principalData),
        success: function (data1) {
			//alert(data.errMsg);
        	// alert(data1);
        },
        error: function (e) {
			// alert("Error message:"+e);
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
    });*/

	/*
	$.ajax({
	    url:"http://localhost:7206/PSGPORTAL/OAuthServlet",
	    type:"POST",
	    data:$.param(
	    		{CALLFOR: "OAUTHVALIDATE",
	    			KYCJSON : JSON.stringify(ntucjson),
	    			ADVSTFCODE:"586043",
	    			FNAID:"FNA00000000000000096",
	    			CASESTATUSDB:"In-Progress",
	    			CASEID:""
	    		}),
	      success:function(data) {
//	    	  loadMsg.style.display="none";
				var response = $.parseJSON(data);
				if (response["SESSION_EXPIRY"]) {
					// alert("session expired")
//					window.location = baseUrl + SESSION_EXP_JSP;
//					return;
				}
				$.each(response, function(count, arr) {

					$.each(arr,function(tab,data){

						$.each(data,function(key,val){

//							console.log(key +" >> " +val);

							if(key == "UPDATESTATUS_B4_CREATEMF2"){
//								alert(val);
							}
							if(key == "UPDATESTATUS_B4_CREATEMF2_MSG"){
								// alert(val);
//								loadMsg.style.display="none";
							}

							if(key == "OAUTH_UNAUTHORIZE"){
								// alert(val["error_description"])
//								loadMsg.style.display="none";
							}

							if(key == "OAUTH_STATUSBASED_FAIL"){
								// alert(val["EKYC_ERROR"])
//								loadMsg.style.display="none";
							}

//							if(key == "OAUTH_SUCCESS"){
//								callCreateMF2(jsonstring);
//							}

							if(key == "CREATE_POLICY_SUCCESS"){
								//alert(val["caseId"])

//								alert("DSF CaseId Generated success.\nCase ID :"+val["caseId"] +"\n");

								//lockimg.show();
//								$(lockimg).closest('tr').find("td:eq(3) select:eq(0)").val("In-Progress");

//								$(lockimg).closest('tr').find("td:eq(2) input:eq(8)").val(val["caseId"]);
//								$(lockimg).closest('tr').find('td:eq(3) input:eq(3)').val(val["caseId"]);

								caseid =val["caseId"];

								if(!isEmpty(caseid)){

//									$(lockimg).closest('tr').find("td:eq(0) img:eq(3)").show();
//									$(lockimg).closest('tr').find("td:eq(3) img:eq(2)").show();

//									var retarr = checkPolicyExist(caseid);
//									if(retarr.length == 0){
//										$(lockimg).closest('tr').find('td:eq(0) img:eq(3)').show();

//									}else{
//										$(lockimg).closest('tr').find('td:eq(3) input:eq(4)').val(retarr[0][0]);
//										$(lockimg).closest('tr').find('td:eq(3) input:eq(5)').val(retarr[0][1]);
//									}

//									$("#hTxtDsfStatusChk").val("Y");

								}

//								loadMsg.style.display="none";
							}

							if(key == "CREATE_POLICY_ERROR"){
								if(val["errorMsg"]){
									// alert(val["errorMsg"])
								}else{
									// alert(val)
								}
//								loadMsg.style.display="none";
//								row.find("td:eq(0) img:eq(2)").attr('src', orgfile);
//								loadMsg.style.display="none";
							}

							if(key == "IDP_RESPONSE_TO_DSF"){
//								alert(val)
							}
							if(key == "DSF_RESPONSE"){

							}

							if(key == "ENCODED_SAML"){var encodesaml=val;}
							if(key == "CASEID"){caseid =val;}
							if(key == "NTUC_DSF_URL"){
console.log(val)
//								dsf_url=val;
								creatSubmtDynaNewWindow(val);
//								creatSubmtDynaNewWindow(baseUrl+ "/testdsf.jsp");
//
//								row.find("td:eq(0) img:eq(2)").css('visibility', 'visible');
//								row.find("td:eq(0) img:eq(2)").attr('src', orgfile);
//								loadMsg.style.display="none";

							}
							if(key == "UPDATEDSFCASE"){
//								alert("Case Return From DSF ->"+val)
							}
						})
					});
				});
	      }
	});
	*/
}

function getNtucJson(personInfo, client) {
	let ntucJson = {};
	personInfo.KYCID = client.fnaDetails.fnaId;

	let agentIntroForm = {};
	agentIntroForm.AgentCode = "586043";
	agentIntroForm.LifeAdvisor = true;
	agentIntroForm.HealthAdvisor = true;
	agentIntroForm.ILPAdvisor = true;

	personInfo.AgentIntroForm = agentIntroForm;
	let PersonalGeneralForm = {};
	if (client.dfSelfDob != null) {
		PersonalGeneralForm.DateOfBirth = formatDateTemp(client.dfSelfDob);
	} else {
		PersonalGeneralForm.DateOfBirth = "";
	}

	PersonalGeneralForm.EducationLevel = chkValueExistInList(educationList, client.dfSelfEduLevel);// client.dfSelfEduLevel;
	let gen = client.dfSelfGender; gen = (gen == "Male" ? "M" : gen == "Female" ? "F" : "");
	PersonalGeneralForm.GenderCode = gen;
	if (client.dfSelfEngSpoken != null) {
		PersonalGeneralForm.LanguageSpoken = (isEmpty(client.dfSelfEngSpoken)) ? "" : (client.dfSelfEngSpoken == 'Y') ? '011' : '99';//client.dfSelfEngSpoken;
	} else {
		PersonalGeneralForm.LanguageSpoken = '99';
	}
	if (client.dfSelfEngWritten != null) {
		PersonalGeneralForm.LanguageWritten = (isEmpty(client.dfSelfEngWritten)) ? "" : (client.dfSelfEngWritten == 'Y') ? '011' : '99';//client.dfSelfEngWritten;
	} else {
		PersonalGeneralForm.LanguageWritten = '99';
	}
	PersonalGeneralForm.MaritalStatus = chkValueExistInList(maritalList, client.dfSelfMartsts);//client.dfSelfMartsts;
	PersonalGeneralForm.NRIC_Passport = client.dfSelfNric;
	PersonalGeneralForm.Name = client.dfSelfName;
	PersonalGeneralForm.NationalityCode = isEmpty(client.dfSelfNationality) ? "-1" :
		client.dfSelfNationality == 'OTH' ?
			chkValueExistInListOthr(nationalityList, (isEmpty(client.dfSelfNatyDets) ? "-1" : client.dfSelfNatyDets))
			: chkValueExistInList(nationalityList, client.dfSelfNationality);//client.dfSelfNationality;

	PersonalGeneralForm.Occupation = (isEmpty(client.dfSelfOccpn)) ? "D999" : chkValueExistInListOthr(occupationList, client.dfSelfOccpn);
	//client.dfSelfOccpn;
	let sm = client.dfSelfSmoking; sm = (sm == "Smoking" ? "Y" : "N");
	PersonalGeneralForm.Smoker = sm;
	PersonalGeneralForm.CPFNo = '';
	let contact = {};
	contact.Office = client.dfSelfOffice;
	contact.Mobile = client.dfSelfMobile;
	contact.Home = client.dfSelfHome;
	PersonalGeneralForm.ContatNo = contact;
	PersonalGeneralForm.Race = '';
	PersonalGeneralForm.Height = removeDecimalPoint(client.dfSelfHeight);	//client.dfSelfHeight;
	PersonalGeneralForm.NatureOfWork = (client.dfSelfBusinatr == '') ? '' : (client.dfSelfBusinatr == 'Others') ? client.dfSelfBusinatrDets : client.dfSelfBusinatr;//client.dfSelfBusinatr;
	PersonalGeneralForm.Weight = client.dfSelfWeight;
	PersonalGeneralForm.EmailAddress = client.dfSelfPersEmail;
	PersonalGeneralForm.NameOfCompanySchool = client.dfSelfCompname;
	let address = {};
	address.Address1 = client.resAddr1;
	address.Address2 = client.resAddr2;
	address.Address3 = client.resAddr3;
	address.Address4 = "";
	address.PostalCode = (client.resCountry.toUpperCase() == 'SINGAPORE') ? client.resPostalCode : '000000';//client.resPostalCode;
	address.AddressType = (client.resCountry.toUpperCase() == 'SINGAPORE') ? 'Singapore' : 'Non Singapore'//client.resCountry;
	address.Unit = '';
	PersonalGeneralForm.Address = address;
	personInfo.PersonalGeneralForm = PersonalGeneralForm;
	ntucJson.personInfo = personInfo
	ntucJson.faCode = "AVALLISFINANCIAL";
	ntucJson.faURL = "www.avallis.com";

	return ntucJson;
}

//Interpreter and Trusted individual Details
//BNF,TPP,PEP and RCA checkbox action
function openOthInptrSec(obj, val) {
	let fnaId = $("#hdnSessFnaId").val();
	if (val == "BENOWNER") {
		if ($(obj).is(":checked")) {
			$(obj).val("Y");
			$(obj).attr("data-toggle", "modal");
			$(obj).attr("data-target", "#txtAreaModal1");
			$(obj).attr("data-backdrop", "static");
			$(obj).attr("data-keyboard", "false");
		} else {
			$(obj).val("N");
			$(obj).removeAttr("data-toggle", "modal");
			$(obj).removeAttr("data-target", "#txtAreaModal1");
			$(obj).removeAttr("data-backdrop", "static");
			$(obj).removeAttr("data-keyboard", "false");
			Swal.fire({
				title: 'Are you sure?',
				text: "Want to Clear Benefical Owner Details?",
				icon: 'warning',
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, Clear it!'
			}).then((result) => {
				if (result.isConfirmed) {
					clearBenOwnerDetls();
					deleteOthPerData(fnaId, "BENF", obj);
					// saveData($(this), 'kycHome', false, true, true);
				} else {
					$(obj).prop("checked", true);
					$(obj).val("Y");
				}
			})
		}
	}

	if (val == "TPP") {
		if ($(obj).is(":checked")) {
			$(obj).val("Y");
			$(obj).attr("data-toggle", "modal");
			$(obj).attr("data-target", "#txtAreaModal2");
			$(obj).attr("data-backdrop", "static");
			$(obj).attr("data-keyboard", "false");
		} else {
			$(obj).val("N");
			$(obj).removeAttr("data-toggle", "modal");
			$(obj).removeAttr("data-target", "#txtAreaModal2");
			$(obj).removeAttr("data-backdrop", "static");
			$(obj).removeAttr("data-keyboard", "false");
			Swal.fire({
				title: 'Are you sure?',
				text: "Want to Clear Third Party Payer Details?",
				icon: 'warning',
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, Clear it!'
			}).then((result) => {
				if (result.isConfirmed) {
					clearThirdPartyDetls();
					deleteOthPerData(fnaId, "TPP", obj);
					// saveData($(this), 'kycHome', false, true, true);
				} else {
					$(obj).prop("checked", true);
					$(obj).val("Y");
				}
			})
		}
	}

	if (val == "PEP") {
		if ($(obj).is(":checked")) {
			$(obj).val("Y");
			$(obj).attr("data-toggle", "modal");
			$(obj).attr("data-target", "#txtAreaModal3");
			$(obj).attr("data-backdrop", "static");
			$(obj).attr("data-keyboard", "false");
		} else {
			$(obj).val("N");
			$(obj).removeAttr("data-toggle", "modal");
			$(obj).removeAttr("data-target", "#txtAreaModal3");
			$(obj).removeAttr("data-backdrop", "static");
			$(obj).removeAttr("data-keyboard", "false");
			Swal.fire({
				title: 'Are you sure?',
				text: "want to clear PEP and RCA Details",
				icon: 'warning',
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, Clear it!'
			}).then((result) => {
				if (result.isConfirmed) {
					clearPepRcaDetls();
					deleteOthPerData(fnaId, "PEP", obj);
					// saveData($(this), 'kycHome', false, true, true);
				} else {
					$(obj).prop("checked", true);
					$(obj).val("Y");
				}
			});
		}
	}
}

function deleteOthPerData(fnaId, categ, obj) {
	$.ajax({
		type: "DELETE",
		url: "FnaOtherPerDets/deleteOthPerData/" + fnaId + "/" + categ,
		success: function(isDeleted) {
			if (isDeleted == "true") {
				Swal.fire("Cleared!", "Your PEP and RCA Details.", "success");
				$(obj).val("N");
				if (categ == "BENF") {
					selfSpouseDets.fnaDetails.cdBenfownFlg = "N";
				} else if (categ == "TPP") {
					selfSpouseDets.fnaDetails.cdTppFlg = "N";
				} else if (categ == "PEP") {
					selfSpouseDets.fnaDetails.cdPepFlg = "N";
				}
			} else {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

// Clear all Benefial Owner Details
function clearBenOwnerDetls() {
	$("#txtAreaModal1").find(".modal-body").find("input[type=text],textarea,select").val("");
	$('input[name="othperWorkBen"]').prop('checked', false);
}

// Clear all Third Party Details
function clearThirdPartyDetls() {
	$("#txtAreaModal2").find(".modal-body").find("input[type=text], textarea,select").val("");
	$('input[name="othperWorkTpp"]').prop('checked', false);
	$('input[name="othperPaymentmodeTpp"]').prop('checked', false);
}

// Clear all PEP / RCA Details
function clearPepRcaDetls() {
	$("#txtAreaModal3").find(".modal-body").find("input[type=text], textarea,select").val("");
	$('input[name="othperWorkPep"]').prop('checked', false);
}

$('#txtAreaModal1').on('hidden.bs.modal', function () {
	let allempty = true;
	$("#txtAreaModal1").find(".modal-body").find("input.mandatory, textarea.mandatory, select.mandatory").each(function(){
		let thisval = $(this).val();
		if ($(this)[0].type == "radio" && !$(this)[0].checked) {
			thisval = "";
		}
		console.log($(this).prop("name") + "," + thisval)
		if(!isEmpty(thisval)){
			allempty=false;
			$("#cdBenfownflg").prop("checked",true);
			$("#cdBenfownflg").val("Y");
			selfSpouseDets.fnaDetails.cdBenfownFlg = "Y";
		}
	})
	
	if(allempty){
		$("#cdBenfownflg").prop("checked",false);
		$("#cdBenfownflg").val("N");
		selfSpouseDets.fnaDetails.cdBenfownFlg = "N";
	}
	
})

$('#txtAreaModal2').on('hidden.bs.modal', function () {
	let allempty = true;
	$("#txtAreaModal2").find(".modal-body").find("input.mandatory, textarea.mandatory, select.mandatory").each(function(){
		let thisval = $(this).val();
		if ($(this)[0].type == "radio" && !$(this)[0].checked) {
			thisval = "";
		}
		if(!isEmpty(thisval)){
			allempty=false;
			$("#cdTppflg").prop("checked",true);
			$("#cdTppflg").val("Y");
			selfSpouseDets.fnaDetails.cdTppFlg = "Y";
		}
	})
	
	if(allempty){
		$("#cdTppflg").prop("checked",false);
		$("#cdTppflg").val("N");
		selfSpouseDets.fnaDetails.cdTppFlg = "N";
	}
	
})

$('#txtAreaModal3').on('hidden.bs.modal', function () {
	let allempty = true;
	$("#txtAreaModal3").find(".modal-body").find("input.mandatory, textarea.mandatory,select.mandatory").each(function(){
		let thisval = $(this).val();
		if ($(this)[0].type == "radio" && !$(this)[0].checked) {
			thisval = "";
		}
		if(!isEmpty(thisval)){
			allempty=false;
			$("#cdPepflg").prop("checked",true);
			$("#cdPepflg").val("Y");
			selfSpouseDets.fnaDetails.cdPepFlg = "Y";
		}
	})
	
	if(allempty){
		$("#cdPepflg").prop("checked",false);
		$("#cdPepflg").val("N");
		selfSpouseDets.fnaDetails.cdPepFlg = "N";
	}
	
})

// Add json data for CDLanguages
function chkJsnOptions(chkbox, jsnFld) {
	let advAppTypeOpt = $("#" + jsnFld + "");
	let jsonvalues = JSON.parse(emptyOrUndefined(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	let chkd = chkbox.checked;
	let val = $(chkbox).attr("data");

	let newobj = jsonvalues;
	newobj[val] = (chkd == true ? "Y" : "N");
	advAppTypeOpt.val(JSON.stringify(newobj));
	document.getElementsByName(jsnFld)[0].value = JSON.stringify(newobj);
}

function sameAsResiAddr(obj, Elem) {
	let crntClnt = "";
	if (Elem == "chkSelfNoSameAddrFlg") {
		crntClnt = "Self"
	}
	if (Elem == "chkSpsNoSameAddrFlg") {
		crntClnt = "Sps"
	}

	if ($(obj).is(':checked')) {
		$("#df" + crntClnt + "MailAddr").val($("#df" + crntClnt + "HomeAddr").val());
		$("#df" + crntClnt + "MailAddr").prop("readOnly", true);
		$("#" + Elem).prop("checked", false);
		$(obj).val("Y");
	} else {
		$(obj).val("N");
	}
}

function NotsameAsResiAddr(obj, Elem) {
	let crntClnt = "";
	if (Elem == "chkSelfSameAddrFlg") {
		crntClnt = "Self"
	}
	if (Elem == "chkSpsSameAddrFlg") {
		crntClnt = "Sps"
	}

	if ($(obj).is(':checked')) {
		$("#df" + crntClnt + "MailAddr").prop("readOnly", false);
		$("#df" + crntClnt + "MailAddr").prop("disabled", false);
		$("#df" + crntClnt + "MailAddr").removeAttr("readOnly");
		$("#df" + crntClnt + "MailAddr").val("");
		$("#" + Elem).prop("checked", false);
		$(obj).val("Y");
	}
}

//Set and get Interpreter details
var arrIntPrt = ["intprtName", "intprtContact", "intprtRelat", "cdLanguages"];

function getInterprtData(interPrt) {
	let intrprtFlg = document.getElementById("cdIntrprtflg");

	if (intrprtFlg.checked == true) {
		interPrt.cdIntrprtFlg = "Y";
	}
	else {
		interPrt.cdIntrprtFlg = "N";
	}
	for (let intPrtIdx = 0; intPrtIdx < arrIntPrt.length; intPrtIdx++) {
		interPrt[arrIntPrt[intPrtIdx]] = document.getElementsByName(arrIntPrt[intPrtIdx])[0].value;
	}
	interPrt.cdLanguageOth = $("#cdLanguageOth").val();
	return interPrt;
}

function setInterprtData(interPrt) {
	for (let sIntPrtIdx = 0; sIntPrtIdx < arrIntPrt.length; sIntPrtIdx++) {
		if (interPrt[arrIntPrt[sIntPrtIdx]] != undefined) {
			document.getElementsByName(arrIntPrt[sIntPrtIdx])[0].value = interPrt[arrIntPrt[sIntPrtIdx]];
		}
		if (arrIntPrt[sIntPrtIdx] == "cdLanguages" && arrIntPrt[sIntPrtIdx] != null) {
			let cdLanguage = interPrt[arrIntPrt[sIntPrtIdx]];
			if (cdLanguage != "" && cdLanguage != null && cdLanguage != "undefined") {
				let cdLangData = JSON.parse(cdLanguage);
				setLanguageData(cdLangData, interPrt);
			}
		}
	}
	//Set interpreter flag
	if (interPrt.cdIntrprtFlg == "Y") {
		$("#cdIntrprtflg").prop('checked', true);//enableIntrprt();
		$("#tempIntprtDiv").find(":input").prop("disabled", false);
		let benFldval = interPrt.cdBenfownFlg;
		if (benFldval == "Y") {
			$("#cdBenfownflg").val(benFldval);
			$("#cdBenfownflg").prop('checked', true);
		} else {
			$("#cdBenfownflg").val(benFldval);
			$("#cdBenfownflg").prop('checked', false);
		}

		let tppFldval = interPrt.cdTppFlg;
		if (tppFldval == "Y") {
			$("#cdTppflg").val(tppFldval);
			$("#cdTppflg").prop('checked', true);
		} else {
			$("#cdTppflg").val(tppFldval);
			$("#cdTppflg").prop('checked', false);
		}

		let pepFldval = interPrt.cdPepFlg;
		if (pepFldval == "Y") {
			$("#cdPepflg").val(pepFldval);
			$("#cdPepflg").prop('checked', true);
		} else {
			$("#cdPepflg").val(pepFldval);
			$("#cdPepflg").prop('checked', false);
		}
	}
}

function setLanguageData(cdLangData, interPrt) {
	if (cdLangData.ENG == "Y") {
		$("#htflaneng").prop('checked', true);
	}
	if (cdLangData.MAN == "Y") {
		$("#htflanman").prop('checked', true);
	}
	if (cdLangData.MAL == "Y") {
		$("#htflanmalay").prop('checked', true);
	}
	if (cdLangData.TAM == "Y") {
		$("#htflantamil").prop('checked', true);
	}
	if (cdLangData.OTH == "Y") {
		$("#htflanoth").prop('checked', true);
		$("#cdLanguageOth").prop('disabled', false).val(interPrt.cdLanguageOth);
	}
}
//End

//Set and get BENF
var arrBenfData = ["othperIdBen", "othperNameBen", "othperNricBen", "othperIncorpnoBen",
	"othperWorkCountryBen", "othperRelationBen"];

//Set and get TPP
var arrTppData = ["othperIdTpp", "othperNameTpp", "othperNricTpp", "othperIncorpnoTpp", "othperWorkCountryTpp",
	"othperRelationTpp", "othperContactnoTpp", "othperBanknameTpp", "othperChqOrdernoTpp"];

//Set and get PEP&RCA dets
var arrPepRcaData = ["othperIdPep", "othperNamePep", "othperNricPep", "othperIncorpnoPep",
	"othperWorkCountryPep", "othperRelationPep"];

function getOtherPerDets(otherDets, arrData, addrFldId, categ, replaceStr, othperWork) {
	for (let othPerIdx = 0; othPerIdx < arrData.length; othPerIdx++) {
		otherDets[arrData[othPerIdx].replace(replaceStr, "")] = document.getElementsByName(arrData[othPerIdx])[0].value;
	}

	otherDets.othperRegaddr = $("#" + addrFldId).val();
	otherDets.othperCateg = categ;
	otherDets.fnaDetails = fnaDetails;
	otherDets.othperWork = $('input[name=' + othperWork + ']:checked').val();
	if (categ == "TPP") {
		otherDets.othperPaymentmode = $('input[name="othperPaymentmodeTpp"]:checked').val();
	}
	return otherDets;
}

function getAllOtherPerDataById(fnaId) {
	$.ajax({
		url: "FnaOtherPerDets/getAllDataById/" + fnaId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			for (let sOthPerIdx = 0; sOthPerIdx < data.length; sOthPerIdx++) {
				setOtherPerDets(data[sOthPerIdx]);
			}
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function setOtherPerDets(data) {
	if (data.othperCateg == "BENF") {
		setBenfTppRcaData(data, arrBenfData, "txtFldCDBenfAddr", "Ben");
		$("#BtnInptrBenOwnSwtch").prop('checked', true);
	}
	if (data.othperCateg == "TPP") {
		setBenfTppRcaData(data, arrTppData, "txtFldCDTppAddr", "Tpp");
		$("#BtnInptrTppSwtch").prop('checked', true);
	}
	if (data.othperCateg == "PEP") {
		setBenfTppRcaData(data, arrPepRcaData, "txtFldCDPepAddr", "Pep");
		$("#BtnInptrPepSwtch").prop('checked', true);
	}
}

function setBenfTppRcaData(data, arrData, addrFldId, replaceStr) {
	for (let sBenfTppIdx = 0; sBenfTppIdx < arrData.length; sBenfTppIdx++) {
		if (data[arrData[sBenfTppIdx].replace(replaceStr, "")] != undefined) {
			document.getElementsByName(arrData[sBenfTppIdx])[0].value = data[arrData[sBenfTppIdx].replace(replaceStr, "")];
		}
	}

	if (data.othperWork == "JOB") {
		$("#Intr" + replaceStr + "Job").prop('checked', true);
	} else if (data.othperWork == "POLITICAL") {
		$("#Intr" + replaceStr + "Pol").prop('checked', true);
	} else if (data.othperWork == "MILITARY") {
		$("#Intr" + replaceStr + "Mil").prop('checked', true);
	}

	if (replaceStr == "Tpp") {
		if (data.othperPaymentmode == "CHQ") {
			$("#radPayChq").prop('checked', true);
		} else if (data.othperPaymentmode == "CRDTCARD") {
			$("#radPayCrdCard").prop('checked', true);
		}
	}
	$("#" + addrFldId).val(data.othperRegaddr);
	/*otherDets.othperCateg=categ;
	otherDets.fnaDetails=fnaDetails;*/
}

function saveOtherPerDets() {

	let benfDets = {};
	let tppDets = {};
	let pepRceDets = {};
	let formData = new FormData();
	if ($("#cdBenfownflg").val() == "Y") {
		formData.append("benfDets", JSON.stringify(getOtherPerDets(benfDets, arrBenfData, "txtFldCDBenfAddr", "BENF"
			, "Ben", "othperWorkBen")));
	} else {
		formData.append("benfDets", JSON.stringify(benfDets));
	}
	if ($("#cdTppflg").val() == "Y") {
		formData.append("tppDets", JSON.stringify(getOtherPerDets(tppDets, arrTppData, "txtFldCDTppAddr"
			, "TPP", "Tpp", "othperWorkTpp")));
	} else {
		formData.append("tppDets", JSON.stringify(tppDets));
	}
	if ($("#cdPepflg").val() == "Y") {
		formData.append("pepRceDets", JSON.stringify(getOtherPerDets(pepRceDets, arrPepRcaData, "txtFldCDPepAddr"
			, "PEP", "Pep", "othperWorkPep")));
	} else {
		formData.append("pepRceDets", JSON.stringify(pepRceDets));
	}

	$.ajax({
		url: "FnaOtherPerDets/saveData",
		type: "POST",
		contentType: false,
		processData: false,
		data: formData,
		async: true,
		success: function (data) {
			// alert("success");
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}
//End

//Footer page next button action
function saveData(obj, nextscreen, navigateflg, infoflag, fromTrust) {

	//interpreter sec Validation
	let inptrFlag = $("#cdIntrprtflg").is(":checked");
	if (inptrFlag == true) {
		if (!validateInptrValidation()) { return; }
	}

	//interpreter popup sec validation
	let cdBenfownflg = $("#cdBenfownflg").is(":checked");
	if (cdBenfownflg == true) {
		//validateBenOwnerDetls
		if (!validateBenOwnerDetls() && !fromTrust) {
			$('#txtAreaModal1').modal('show');
			return;
		}
		$('#txtAreaModal1').modal('hide');
	}

	let cdTppflg = $("#cdTppflg").is(":checked");
	if (cdTppflg == true) {
		//validateTppDetls
		if (!validateTppDetls() && !fromTrust) {
			$('#txtAreaModal2').modal('show');
			return;
		}
		$('#txtAreaModal2').modal('hide');
	}

	let cdPepflg = $("#cdPepflg").is(":checked");
	if (cdPepflg == true) {
		//validatePepRcaDetls
		if (!validatePepRcaDetls() && !fromTrust) {
			$('#txtAreaModal3').modal('show');
			return;
		}
		$('#txtAreaModal3').modal('hide');
	}

	let hdnSessDataFormId = $("#hdnSessDataFormId").val();
	let clientDets = {};
	clientDets = updateSelfAndSpouseData(selfSpouseDets);
	updateAjaxCall(clientDets);
	//alert(JSON.stringify(client));
	saveOtherPerDets();
	if (navigateflg) {
		window.location.href = nextscreen;
	}
}

function validateInptrValidation() {
	let name, Contact, relation, numberOfChecked, Fld = "Error";
	name = $('#intprtName').val();
	Contact = $('#intprtContact').val();
	relation = $('#intprtRelat').val();
	numberOfChecked = $(".selfInptrLangSec").find('input:checkbox:checked').length;

	//Client Name
	if (isEmptyFld(name)) {
		$("#intprtName" + Fld).removeClass("hide").addClass("show");
		$("#intprtName").addClass('err-fld');
		$("#intprtName").focus();
		return;
	} else {
		$("#intprtName" + Fld).removeClass("show").addClass("hide");
		$("#intprtName").removeClass('err-fld');
	}
	//contact
	if (isEmptyFld(Contact)) {
		$("#intprtContact" + Fld).removeClass("hide").addClass("show");
		$("#intprtContact").addClass('err-fld');
		$("#intprtContact").focus();
		return;
	} else {
		$("#intprtContact" + Fld).removeClass("show").addClass("hide");
		$("#intprtContact").removeClass('err-fld');
	}
	//Relationship
	if (isEmptyFld(relation)) {
		$("#intprtRelat" + Fld).removeClass("hide").addClass("show");
		$("#intprtRelat").addClass('err-fld');
		$("#intprtRelat").focus();
		return;
	} else {
		$("#intprtRelat" + Fld).removeClass("show").addClass("hide");
		$("#intprtRelat").removeClass('err-fld');
	}

	if (isEmptyFld(numberOfChecked)) {
		$("#selfInptrLangSec" + Fld).removeClass("hide").addClass("show");
		$(".selfInptrLangSec").addClass('err-fld');
		//$("#intprtRelat").focus();
		return;
	} else {
		hideInptrLanSecError();
	}
	return true;
}

function creatSubmtDynaNewWindow(href) {

	let lex1 = href.split('?');
	let action = lex1[0];
	let qstr = lex1[1];
	let obj = " ";

	let formId = document.getElementById("hiddenForm");
	let divId = document.getElementById("dynamicFormDiv");

	if (formId) {
		if (divId)
			formId.removeChild(divId);
	}

	let newdiv = document.createElement('div');
	newdiv.id = "dynamicFormDiv";

	if (qstr != null) {
		let params = qstr.split('&');
		for (let parIdx = 0; parIdx < params.length; parIdx++) {
			let keyValue = params[parIdx].split('=');
			let name = keyValue[0];
			let value = keyValue[1];

			if (value.indexOf("\'") != -1)
				value = value.replace("\'", "\'");
			value = unescape(value);

			//obj += '<input type="hidden" name="'+name+'" value="'+value+'"/>';
			obj += "<input type='hidden' name='" + name + "' value='" + value + "'/>";
		}
	}
	newdiv.innerHTML = obj;

	if (document.getElementById("hiddenForm"))
		document.getElementById("hiddenForm").appendChild(newdiv);

	document.getElementById("hiddenForm").action = action;
	document.getElementById("hiddenForm").method = "POST";

	window.open('', 'thewindow', "channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
	document.getElementById("hiddenForm").submit();
}

function AddClient2TabData() {
	if ($('#myTab li').length == 5) {
		$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor", "not-allowed");
		$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle", "");
		$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target", "");
	} else {
		let signornot = $("#hTxtFldSignedOrNot").val();
		if (signornot != "Y") {
			setAddSrchRadioBtnFields();
			if ($('#myTab li').length == 5) {
				$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor", "not-allowed");
				$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle", "");
				$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target", "");
			} else if ($('#myTab li').length < 5) {
				$('#myTab').find("#btnAddNewInsur").children(".btn").css("cursor", "pointer");
				$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-toggle", "modal");
				$('#myTab').find("#btnAddNewInsur").children(".btn").attr("data-target", "#new2ModalScrollable");
			}
		} else {
			showConfrmAfterSignChng($("#btnNextFormUniq"), $("#applNextScreen").val(), false, false);
		}
	}
}

function formatDateTemp(date) {
	let d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;
	//    return [year, month, day].join('-');
	return [day, month, year].join('-');
}

var educationList = [{ name: "P", value: "Primary & below" },
{ name: "S", value: "Secondary" },
{ name: "O", value: "GCE O/N Level" },
{ name: "A", value: "Pre-U/JC" },
{ name: "D", value: "Diploma" },
{ name: "G", value: "Degree" },
{ name: "M", value: "Post Graduate" }];

var genderList = [{ name: "M", value: "Male" },
{ name: "F", value: "Female" }];

var languageList = [{ name: "011", value: "English" },
{ name: "211", value: "Mandarin" },
{ name: "264", value: "Malay" },
{ name: "201", value: "Tamil" },
{ name: "99", value: "Others" }];

var maritalList = [{ name: "1", value: "Married" },
{ name: "2", value: "Single" },
{ name: "3", value: "Divorced" },
{ name: "4", value: "Widowed" }];

var nationalityList = [{ name: "702", value: "Singaporean" },
{ name: "703", value: "Singaporean-PR" },
{ name: "458", value: "Malaysian" },
{ name: "-1", value: "Others" }];

var occupationList = [{ name: "D088", value: "Housewife" },
{ name: "D161", value: "Student" },
{ name: "D999", value: "Others" }];

var raceList = [{ name: "C", value: "Chinese" },
{ name: "I", value: "Indian" },
{ name: "M", value: "Malay" },
{ name: "OTH", value: "Others" }];

function chkValueExistInList(objList, val) {
	let rtnval = "";
	$.each(objList, function (i, key) {
		if (!isEmpty(key.value) && !isEmpty(val) && key.value.toUpperCase() == val.toUpperCase()) {
			rtnval = key.name;
		}
	});

	return rtnval;
}

function chkValueExistInListOthr(objList, val) {
	let rtnval = "";
	if (!isEmpty(val)) {
		$.each(objList, function (i, key) {
			if (!isEmpty(key.value) && !isEmpty(val) && key.value.toUpperCase() == val.toUpperCase()) {
				rtnval = key.name;
			}
		});
	}

	if (!isEmpty(val) && rtnval == "") {
		rtnval = objList[objList.length - 1].name;
	}

	return rtnval;
}

Number.prototype.countDecimals = function () {
	if (Math.floor(this.valueOf()) === this.valueOf()) return 0;
	return this.toString().split(".")[0].length || 0;
}

function removeDecimalPoint(val) {
	let precisonCnt = Number(val).countDecimals();
	let retrnval = "";

	if (isEmpty(val)) {

	} else if (precisonCnt == 3) {
		retrnval = parseFloat(Math.round(Number(val) * 1).toFixed(0));
	} else if (precisonCnt == 2) {
		retrnval = parseFloat(Math.round(Number(val) * 10).toFixed(0));
	} else if (precisonCnt == 1) {
		retrnval = parseFloat(Math.round(Number(val) * 100).toFixed(0));
	} else if (precisonCnt == 0) {
		retrnval = Number(val);
	}
	return retrnval;
}

//validate self and sps business natue details

//validate BusinessNature fld validation

function valiBusNatOthers(selObj) {
	let selFldId = $(selObj).attr("id");

	if (selFldId == "dfSelfBusinatr") {
		$("#dfSelfBusinatrDets").val("");
		let selFldVal = $(selObj).val();
		if (selFldVal == 'Others') {
			$("#dfSelfBusNatOthSec").removeClass("invisible").addClass("show");
			if (isEmpty($("#businessNatrDets").val())) {
				$("#dfSelfBusinatrDets").addClass("err-fld");
				$("#dfSelfBusinatrDets").focus();
				$("#dfSelfBusinatrDetsError").addClass("show").removeClass("invisible");
			} else {
				$("#dfSelfBusinatrDets").removeClass("err-fld");
				$("#dfSelfBusinatrDetsError").removeClass("show").addClass("invisible");
			}
		} else {
			$("#dfSelfBusNatOthSec").removeClass("show").addClass("invisible");
		}
	}
	if (selFldId == "dfSpsBusinatr") {
		$("#dfSpsBusinatrDets").val("");
		let selFldVal = $(selObj).val();
		if (selFldVal == 'Others') {
			$("#dfSpsBusNatOthSec").removeClass("invisible").addClass("show");
			if (isEmpty($("#businessNatrDets").val())) {
				$("#dfSpsBusinatrDets").addClass("err-fld");
				$("#dfSpsBusinatrDets").focus();
				$("#dfSpsBusinatrDetsError").addClass("show").removeClass("invisible");
			} else {
				$("#dfSpsBusinatrDets").removeClass("err-fld");
				$("#dfSpsBusinatrDetsError").removeClass("show").addClass("invisible");
			}
		} else {
			$("#dfSpsBusNatOthSec").removeClass("show").addClass("invisible");
		}
	}
}

function validateFld(obj) {
	// alert("hhuh")
	let thisFldId = $(obj).attr("id");
	let thisFldVal = $(obj).val();

	if (isEmptyFld(thisFldVal)) {
		$("#" + thisFldId + "Error").removeClass("hide").addClass("show");
		$("#" + thisFldId).addClass('err-fld');
		return;
	} else {
		$("#" + thisFldId + "Error").removeClass("show").addClass("hide");
		$("#" + thisFldId).removeClass('err-fld');
	}
	//})
}

function radioAndChkboxFld(obj) {
	let thisFldName = $(obj).attr("name");
	let thisFldVal = $("input[name=" + thisFldName + "]:checked").val();

	if (isEmptyFld(thisFldVal)) {
		$("#" + thisFldName + "Error").removeClass("hide").addClass("show");
		//$("#"+thisFldId).addClass('err-fld');
		return;
	} else {
		$("#" + thisFldName + "Error").removeClass("show").addClass("hide");
		// $("#"+thisFldId).removeClass('err-fld');
	}
}

function fnaAssgnFlg(obj, id) {
	if (id == "chk") {
		if (obj.checked) {
			hideInptrLanSecError();
			obj.value = "Y";
		} else if (!obj.checked) {
			obj.value = "";
		}
	} else {
		if (obj.checked) {
			document.getElementById(id).value = obj.value;
		}
	}
}

function hideInptrLanSecError() {
	$("#selfInptrLangSecError").removeClass("show").addClass("hide");
	$(".selfInptrLangSec").removeClass('err-fld');
}

function validateBenOwnerDetls() {
	let name, nric, incorNum, resiAddr, job, country, relationship, Fld = "Error";
	name = $('#othperNameBen').val();
	nric = $('#othperNricBen').val();
	incorNum = $('#othperIncorpnoBen').val();
	resiAddr = $('#txtFldCDBenfAddr').val();
	job = $('input[name="othperWorkBen"]:checked').val();
	country = $('#othperWorkCountryBen').val();
	relationship = $('#othperRelationBen').val();

	//numberOfChecked = $(".selfInptrLangSec").find('input:checkbox:checked').length;

	//Client Name
	if (isEmptyFld(name)) {
		$("#othperNameBen" + Fld).removeClass("hide").addClass("show");
		$("#othperNameBen").addClass('err-fld');
		$("#othperNameBen").focus();
		return;
	} else {
		$("#othperNameBen" + Fld).removeClass("show").addClass("hide");
		$("#othperNameBen").removeClass('err-fld');
	}

	//nric
	if (isEmptyFld(nric)) {
		$("#othperNricBen" + Fld).removeClass("hide").addClass("show");
		$("#othperNricBen").addClass('err-fld');
		$("#othperNricBen").focus();
		return;
	} else {
		$("#othperNricBen" + Fld).removeClass("show").addClass("hide");
		$("#othperNricBen").removeClass('err-fld');
	}

	//Entity Incor No
	if (isEmptyFld(incorNum)) {
		$("#othperIncorpnoBen" + Fld).removeClass("hide").addClass("show");
		$("#othperIncorpnoBen").addClass('err-fld');
		$("#othperIncorpnoBen").focus();
		return;
	} else {
		$("#othperIncorpnoBen" + Fld).removeClass("show").addClass("hide");
		$("#othperIncorpnoBen").removeClass('err-fld');
	}

	//Resi Addr
	if (isEmptyFld(resiAddr)) {
		$("#txtFldCDBenfAddr" + Fld).removeClass("hide").addClass("show");
		$("#txtFldCDBenfAddr").addClass('err-fld');
		$("#txtFldCDBenfAddr").focus();
		return;
	} else {
		$("#txtFldCDBenfAddr" + Fld).removeClass("show").addClass("hide");
		$("#txtFldCDBenfAddr").removeClass('err-fld');
	}

	//job
	if (isEmptyFld(job)) {
		$("#othperWorkBen" + Fld).removeClass("hide").addClass("show");
		//$("#othperWorkBen").addClass('err-fld');
		//$("#othperWorkBen").focus();
		return;
	} else {
		$("#othperWorkBen" + Fld).removeClass("show").addClass("hide");
		//$("#othperWorkBen").removeClass('err-fld');
	}

	//Country
	if (isEmptyFld(country)) {
		$("#othperWorkCountryBen" + Fld).removeClass("hide").addClass("show");
		$("#othperWorkCountryBen").addClass('err-fld');
		$("#othperWorkCountryBen").focus();
		return;
	} else {
		$("#othperWorkCountryBen" + Fld).removeClass("show").addClass("hide");
		$("#othperWorkCountryBen").removeClass('err-fld');
	}

	//Relationship
	if (isEmptyFld(relationship)) {
		$("#othperRelationBen" + Fld).removeClass("hide").addClass("show");
		$("#othperRelationBen").addClass('err-fld');
		$("#othperRelationBen").focus();
		return;
	} else {
		$("#othperRelationBen" + Fld).removeClass("show").addClass("hide");
		$("#othperRelationBen").removeClass('err-fld');
	}
	return true;
}

//Tpp sec mandotary fld  Validation 
function validateTppDetls() {
	let name, nric, incorNum, resiAddr, job, country, relationship, payMode, contact, Fld = "Error";
	name = $('#othperNameTpp').val();
	nric = $('#othperNricTpp').val();
	incorNum = $('#othperIncorpnoTpp').val();
	resiAddr = $('#txtFldCDTppAddr').val();
	job = $('input[name="othperWorkTpp"]:checked').val();
	country = $('#othperWorkCountryTpp').val();
	relationship = $('#othperRelationTpp').val();
	payMode = $('input[name="othperPaymentmodeTpp"]:checked').val();
	contact = $('#othperContactnoTpp').val();
	//numberOfChecked = $(".selfInptrLangSec").find('input:checkbox:checked').length;

	//Client Name
	if (isEmptyFld(name)) {
		$("#othperNameTpp" + Fld).removeClass("hide").addClass("show");
		$("#othperNameTpp").addClass('err-fld');
		$("#othperNameTpp").focus();
		return;
	} else {
		$("#othperNameTpp" + Fld).removeClass("show").addClass("hide");
		$("#othperNameTpp").removeClass('err-fld');
	}

	//nric
	if (isEmptyFld(nric)) {
		$("#othperNricTpp" + Fld).removeClass("hide").addClass("show");
		$("#othperNricTpp").addClass('err-fld');
		$("#othperNricTpp").focus();
		return;
	} else {
		$("#othperNricTpp" + Fld).removeClass("show").addClass("hide");
		$("#othperNricTpp").removeClass('err-fld');
	}

	//Entity Incor No
	if (isEmptyFld(incorNum)) {
		$("#othperIncorpnoTpp" + Fld).removeClass("hide").addClass("show");
		$("#othperIncorpnoTpp").addClass('err-fld');
		$("#othperIncorpnoTpp").focus();
		return;
	} else {
		$("#othperIncorpnoTpp" + Fld).removeClass("show").addClass("hide");
		$("#othperIncorpnoTpp").removeClass('err-fld');
	}

	//Resi Addr
	if (isEmptyFld(resiAddr)) {
		$("#txtFldCDTppAddr" + Fld).removeClass("hide").addClass("show");
		$("#txtFldCDTppAddr").addClass('err-fld');
		$("#txtFldCDTppAddr").focus();
		return;
	} else {
		$("#txtFldCDTppAddr" + Fld).removeClass("show").addClass("hide");
		$("#txtFldCDTppAddr").removeClass('err-fld');
	}

	//job
	if (isEmptyFld(job)) {
		$("#othperWorkTpp" + Fld).removeClass("hide").addClass("show");
		//$("#txtFldCDBenfAddr").addClass('err-fld');
		//$("#txtFldCDBenfAddr").focus();
		return;
	} else {
		$("#othperWorkTpp" + Fld).removeClass("show").addClass("hide");
		//$("#txtFldCDBenfAddr").removeClass('err-fld');
	}

	//Country
	if (isEmptyFld(country)) {
		$("#othperWorkCountryTpp" + Fld).removeClass("hide").addClass("show");
		$("#othperWorkCountryTpp").addClass('err-fld');
		$("#othperWorkCountryTpp").focus();
		return;
	} else {
		$("#othperWorkCountryTpp" + Fld).removeClass("show").addClass("hide");
		$("#othperWorkCountryTpp").removeClass('err-fld');
	}

	//Relationship
	if (isEmptyFld(relationship)) {
		$("#othperRelationTpp" + Fld).removeClass("hide").addClass("show");
		$("#othperRelationTpp").addClass('err-fld');
		$("#othperRelationTpp").focus();
		return;
	} else {
		$("#othperRelationTpp" + Fld).removeClass("show").addClass("hide");
		$("#othperRelationTpp").removeClass('err-fld');
	}

	//payMode
	if (isEmptyFld(payMode)) {
		$("#othperPaymentmodeTpp" + Fld).removeClass("hide").addClass("show");
		//$("#txtFldCDBenfAddr").addClass('err-fld');
		//$("#txtFldCDBenfAddr").focus();
		return;
	} else {
		$("#othperPaymentmodeTpp" + Fld).removeClass("show").addClass("hide");
		//$("#txtFldCDBenfAddr").removeClass('err-fld');
	}

	//Contact
	if (isEmptyFld(contact)) {
		$("#othperContactnoTpp" + Fld).removeClass("hide").addClass("show");
		$("#othperContactnoTpp").addClass('err-fld');
		$("#othperContactnoTpp").focus();
		return;
	} else {
		$("#othperContactnoTpp" + Fld).removeClass("show").addClass("hide");
		$("#othperContactnoTpp").removeClass('err-fld');
	}

	return true;
}

//Pep/Rca Mand Field Validation 
function validatePepRcaDetls() {
	let name, nric, incorNum, resiAddr, job, country, relationship, Fld = "Error";
	name = $('#othperNamePep').val();
	nric = $('#othperNricPep').val();
	incorNum = $('#othperIncorpnoPep').val();
	resiAddr = $('#txtFldCDPepAddr').val();
	job = $('input[name="othperWorkPep"]:checked').val();
	country = $('#othperWorkCountryPep').val();
	relationship = $('#othperRelationPep').val();

	//numberOfChecked = $(".selfInptrLangSec").find('input:checkbox:checked').length;

	//Client Name
	if (isEmptyFld(name)) {
		$("#othperNamePep" + Fld).removeClass("hide").addClass("show");
		$("#othperNamePep").addClass('err-fld');
		$("#othperNamePep").focus();
		return;
	} else {
		$("#othperNamePep" + Fld).removeClass("show").addClass("hide");
		$("#othperNamePep").removeClass('err-fld');
	}

	//nric
	if (isEmptyFld(nric)) {
		$("#othperNricPep" + Fld).removeClass("hide").addClass("show");
		$("#othperNricPep").addClass('err-fld');
		$("#othperNricPep").focus();
		return;
	} else {
		$("#othperNricPep" + Fld).removeClass("show").addClass("hide");
		$("#othperNricPep").removeClass('err-fld');
	}

	//Entity Incor No
	if (isEmptyFld(incorNum)) {
		$("#othperIncorpnoPep" + Fld).removeClass("hide").addClass("show");
		$("#othperIncorpnoPep").addClass('err-fld');
		$("#othperIncorpnoPep").focus();
		return;
	} else {
		$("#othperIncorpnoPep" + Fld).removeClass("show").addClass("hide");
		$("#othperIncorpnoPep").removeClass('err-fld');
	}

	//Resi Addr
	if (isEmptyFld(resiAddr)) {
		$("#txtFldCDPepAddr" + Fld).removeClass("hide").addClass("show");
		$("#txtFldCDPepAddr").addClass('err-fld');
		$("#txtFldCDPepAddr").focus();
		return;
	} else {
		$("#txtFldCDPepAddr" + Fld).removeClass("show").addClass("hide");
		$("#txtFldCDPepAddr").removeClass('err-fld');
	}

	//job
	if (isEmptyFld(job)) {
		$("#othperWorkPep" + Fld).removeClass("hide").addClass("show");

		return;
	} else {
		$("#othperWorkPep" + Fld).removeClass("show").addClass("hide");

	}

	//Country
	if (isEmptyFld(country)) {
		$("#othperWorkCountryPep" + Fld).removeClass("hide").addClass("show");
		$("#othperWorkCountryPep").addClass('err-fld');
		$("#othperWorkCountryPep").focus();
		return;
	} else {
		$("#othperWorkCountryPep" + Fld).removeClass("show").addClass("hide");
		$("#othperWorkCountryPep").removeClass('err-fld');
	}

	//Relationship
	if (isEmptyFld(relationship)) {
		$("#othperRelationPep" + Fld).removeClass("hide").addClass("show");
		$("#othperRelationPep").addClass('err-fld');
		$("#othperRelationPep").focus();
		return;
	} else {
		$("#othperRelationPep" + Fld).removeClass("show").addClass("hide");
		$("#othperRelationPep").removeClass('err-fld');
	}

	return true;
}

function validateFld(obj) {
	// alert("hhuh")
	let thisFldId = $(obj).attr("id");
	let thisFldVal = $(obj).val();

	if (isEmptyFld(thisFldVal)) {
		$("#" + thisFldId + "Error").removeClass("hide").addClass("show");
		$("#" + thisFldId).addClass('err-fld');
		return;
	} else {
		$("#" + thisFldId + "Error").removeClass("show").addClass("hide");
		$("#" + thisFldId).removeClass('err-fld');
	}
	//})
}

function radioAndChkboxFld(obj) {
	let thisFldName = $(obj).attr("name");
	let thisFldVal = $("input[name=" + thisFldName + "]:checked").val();

	if (isEmptyFld(thisFldVal)) {
		$("#" + thisFldName + "Error").removeClass("hide").addClass("show");
		//$("#"+thisFldId).addClass('err-fld');
		return;
	} else {
		$("#" + thisFldName + "Error").removeClass("show").addClass("hide");
		// $("#"+thisFldId).removeClass('err-fld');
	}
}

function showSpouseInMenu(newinsured) {
	$("#clnt2Text").addClass("show").removeClass("hide");
	$("#MenuSpsName").text(newinsured).addClass("show").removeClass("hide");
}

function hideSpouseInMenu() {
	$("#MenuSpsName").text("").addClass("hide").removeClass("show");
	$("#clnt2Text").addClass("hide").removeClass("show");
}

$("#dfSelfBusinatrDets").on("keyup", function () {
	if (isEmptyFld($("#dfSelfBusinatrDets").val())) {
		$("#dfSelfBusinatrDetsError").removeClass("invisible").addClass("show");
		$("#dfSelfBusinatrDets").addClass('err-fld');
		return;
	} else {
		$("#dfSelfBusinatrDetsError").removeClass("show").addClass("invisible");
		$("#dfSelfBusinatrDets").removeClass('err-fld');
	}
});

$("#dfSpsBusinatrDets").on("keyup", function () {
	if (isEmptyFld($("#dfSpsBusinatrDets").val())) {
		$("#dfSpsBusinatrDetsError").removeClass("invisible").addClass("show");
		$("#dfSpsBusinatrDets").addClass('err-fld');
		return;
	} else {
		$("#dfSpsBusinatrDetsError").removeClass("show").addClass("invisible");
		$("#dfSpsBusinatrDets").removeClass('err-fld');
	}
});