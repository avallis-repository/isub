/**
 * 
 */
$(function () {
	// document.cookie = "session=";
	document.cookie = "session=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
	document.cookie = "managerApprovalHide=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
	$("#lognError").removeClass('show').addClass('hide');
	let lognErr = $("#lognError").text();
	if (!isEmptyFld(lognErr)) {
		$("#lognError").removeClass('hide').addClass('show');
	} else {
		$("#lognError").removeClass('show').addClass('hide');
	}
	$("#txtFldUserName").focus();
});

function validateLogin() {

	document.cookie = "session=valid; path=/";
	let loginFrm = document.forms["frmLogin"];
	let userIdFld = loginFrm.txtFldUserName;
	let passwordFld = loginFrm.txtFldpassword;

	if (isEmptyFld(userIdFld.value)) {
		$("#txtFldUserNameError").removeClass('hide').addClass('show');
		return false;
	} else {
		$("#txtFldUserNameError").removeClass('show').addClass('hide');
	}

	if (isEmptyFld(passwordFld.value)) {
		$("#txtFldpasswordError").removeClass('hide').addClass('show');
		return false;
	} else {
		$("#txtFldpasswordError").removeClass('show').addClass('hide');
	}

	// loginFrm.action = "loginValidate";
	// loginFrm.submit();

	$.ajax({
		type: "POST",
		url: "loginValidate?email=" + userIdFld.value + "&password=" + passwordFld.value,
		dataType: "json",
		success: function (loginResponse) {
			// let loginResponse = JSON.parse(response);
			if (loginResponse && loginResponse.status) {
				if (loginResponse.status == "failed") {
					Swal.fire({
						icon: "error",
						text: loginResponse.reason,
						allowEscapeKey: false,
						allowOutsideClick: false
					});
				} else if (loginResponse.status == "success") {
					window.location.href = "index";
				}
			}
		},
		error: function (error) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}