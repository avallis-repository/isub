/*Qr code implementation*/
var signatureDoneArr = [];
/*$(document).ready(function () {
	var fnaId=$("#hdnSessFnaId").val();
	getAllSign(fnaId);
	//var sessspsname=$("#hdnSessSpsName").val();
	console.log("onload signatureDoneArr-->"+signatureDoneArr)
});*/

function getQrCode(value, id, loaderId, delFlag) {
	let needDelete = delFlag;
	if ($("#" + id).attr("src") == "") {
		document.getElementById(loaderId).style.display = "block";
	}
	$.ajax({
		url: 'FnaSignature/getQrCodeById/' + value,
		type: 'GET',
		// async:false,
		dataType: "text",
		data: { "isDelete": delFlag },
		success: function (byteArray) {
			$('#' + id).attr("src", 'data:image/png;base64,' + byteArray).css("margin", "auto");
			// console.log("SUCCESS : ", document.getElementById("qrCode").src);
			document.getElementById(loaderId).style.display = "none";

			if (needDelete == "true") {
				let fnaDetailsStr = $("#hdnFnaDetails").val();
				let fnaDetails = JSON.parse(fnaDetailsStr);
				fnaDetails.mgrEmailSentFlg = "N";
				$("#hdnFnaDetails").val(JSON.stringify(fnaDetails));
				saveData();

				setPageKeyInfoFail();
				setPageSignInfoFail();
				setPageManagerSts();
			}
			// startTimer();
			// startWebsocket();
		},
		error: function (e) {
			console.log("ERROR : ", e);
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

//Generate Qr code or Signpad
function genQrOrSign(personType, id) {

	let clientSignExist = false, sposeSignExist = false;
	console.log("inside genQrOrSign  return :" + signatureDoneArr)
	if (signatureDoneArr.includes("CLIENT") === true) {
		clientSignExist = true;
	}

	if (isEmpty(sessspsname)) {
		sposeSignExist = true;
	} else {
		if (signatureDoneArr.includes("SPS") === true) {
			sposeSignExist = true;
		}
	}

	if (personType == "CLIENT") {
		if (selfData != undefined) {
			hideQrCode(id);
			selfInit('selfDiagram', 'selfSavedDiagram'); setClientType('CLIENT');
			setSignData(selfData, 'selfSignId', selfDiagramObj, 'selfSavedDiagram');
			$("#spanClientSignWaitMsg").html('<i class="fa fa-check-circle" style="font-size:24px"></i>&nbsp;Client(1) signature captured success!');
		} else {
			hideSignPad(id);
			getQrCode('CLIENT', 'qrCodeClient1', 'signLoaderClient1', false);
			selfData = null
			$("#spanClientSignWaitMsg").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;<span class="">Checking to load Client(1) signature!</sapn>');
		}
	} else if (personType == "SPS") {
		if (spsData != undefined) {
			hideQrCode(id);
			spsInit('spsDiagram', 'spsSavedDiagram'); setClientType('SPOUSE');
			setSignData(spsData, 'spsSignId', spsDiagramObj, 'spsSavedDiagram');
			$("#spanSpsSignWaitMsg").html('<i class="fa fa-check-circle" style="font-size:24px"></i>&nbsp;<span class="">Client(2) signature captured success!</span>');
		} else {
			hideSignPad(id);
			getQrCode('SPS', 'qrCodeClient2', 'signLoaderClient2', false);
			spsData = null
			$("#spanSpsSignWaitMsg").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;<span class="">Checking to load Client(2) signature!</span>');
		}
	} else if (personType == "ADVISER") {

		if (clientSignExist && sposeSignExist) {
			if (advData != undefined) {
				hideQrCode(id);
				advInit('advDiagram', 'advSavedDiagram'); setClientType('ADVISER')
				setSignData(advData, 'advSignId', advDiagramObj, 'advSavedDiagram');
				$("#spanAdvSignWaitMsg").html('<i class="fa fa-check-circle" style="font-size:24px"></i>&nbsp;<span class="">Adviser signature captured success!</span>');
			} else {
				hideSignPad(id);
				getQrCode('ADVISER', 'qrCodeAdviser', 'signLoaderAdviser', false);
				advData = null;
				$("#spanAdvSignWaitMsg").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;<span class="">Checking to load Adviser signature!</span>');
			}
		} else {
			hideQrCode(id);
			hideSignPad(id);
			advData = null;
			$("#spanAdvSignWaitMsg").html('<i class="fa fa-remove" style="font-size:24px"></i>&nbsp;<span class="">Client(1)/Client(2) signature yet to capture!</span>');
		}
	} else if (personType == "MANAGER") {
		if (mgrData != undefined) {
			hideQrCode(id);
			mgrInit('mgrDiagram', 'mgrSavedDiagram'); setClientType('MANAGER'); mgrSignData()
			setSignData(mgrData, 'mgrSignId', mgrDiagramObj, 'mgrSavedDiagram');
			$("#spanAdvSignWaitMsg").html('<i class="fa fa-check-circle" style="font-size:24px"></i>&nbsp;<span class="">Manager signature captured success!</span>');
		} else {
			hideSignPad(id);
			getQrCode('MANAGER', 'qrCodeManager', 'signLoaderManager', false);
			mgrData = null;
			$("#spanAdvSignWaitMsg").html('<i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;<span class="">checking to load Manager signature!</span>');
		}
	}
}

function hideQrCode(id) {
	document.getElementById("signSec" + id).style.display = "block";
	document.getElementById("qrCode" + id).style.display = "none";
}

function hideSignPad(id) {
	document.getElementById("signSec" + id).style.display = "none";
	document.getElementById("qrCode" + id).style.display = "block";
}

/*Start web socket*/

/*var socketConn = new WebSocket('ws://192.168.43.25:8080/PSGISUBMIT/autoRefreshHandler');
function startWebsocket(){
	
	send("START");
}

//Send Message
function send(msg) {
		//var clientMsg = document.getElementById('clientMsg');
		if (msg) {
			socketConn.send(msg);
			msg = '';
		}
}

// Receive Message
socketConn.onmessage = function(event) {
		//var serverMsg = document.getElementById('serverMsg');
		//serverMsg.value = event.data;
		alert(event.data);
	}*/

/*End websocket*/
var timesRun = 0;
var timer;
function startTimer() {
	//timer = setInterval(getAllSignDatas, 30000);//30sec
	//var isScantoSignOpen = $('#ScanToSignModal').is(':visible');
	//alert(isScantoSignOpen)

	if ((isEmpty(sessspsname) && signatureDoneArr.length == 2) || (!isEmpty(sessspsname) && signatureDoneArr.length == 3)) {
		clearInterval(timer);
	} else {
		let isScantoSignOpen = $("#ScanToSignModal").is(':visible');
		if (isScantoSignOpen) {
			timer = setInterval(getAllSignDatas, 10000);//10sec
		} else {
			clearInterval(timer);
		}
	}

	// $('#ScanToSignModal').on('shown.bs.modal', function () {
	//	timer = setInterval(getAllSignDatas, 10000);
	// 	return true;
	// });
	// clearInterval(timer);

	/*if(isScantoSignOpen){
		timer = setInterval(getAllSignDatas, 10000);//10sec
	}else{
		clearInterval(timer);
	}*/
}

function getAllSignDatas() {

	timesRun++;

	let fnaId = $("#hdnSessFnaId").val();
	getAllSign(fnaId);

	// isEmailPopup();
	/*if(timesRun == 2){
		clearInterval(timer);
		alert("Time out:Signature is not present here, please refresh the page..");
	}*/
}

/*Signpad*/
/* Self */
function setClientType(type) {
	$("#perType").val(type);
}

var count = 0;
function selfInit(diagramId, savedDiagram) {
	let valid = document.getElementById("selfLoad").value;

	if (valid == "Y") {
		if (window.goSamples) goSamples();
		let $ = go.GraphObject.make;
		selfDiagramObj = $(go.Diagram, diagramId);
		selfDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
		selfDiagramObj.nodeTemplateMap.add("FreehandDrawing",
			$(go.Part,
				{ locationSpot: go.Spot.Center, isLayoutPositioned: false },
				new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
				{
					selectionAdorned: true, selectionObjectName: "SHAPE",
					selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
						$(go.Adornment, "Auto",
							$(go.Shape, { stroke: "dodgerblue", fill: null }),
							$(go.Placeholder, { margin: -1 }))
				},
				{ resizable: true, resizeObjectName: "SHAPE" },
				{ rotatable: true, rotateObjectName: "SHAPE" },
				{ reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
				$(go.Shape,
					{ name: "SHAPE", fill: null, strokeWidth: 1.5 },
					new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
					new go.Binding("angle").makeTwoWay(),
					new go.Binding("geometryString", "geo").makeTwoWay(),
					new go.Binding("fill"),
					new go.Binding("stroke"),
					new go.Binding("strokeWidth"))
			));
		// create drawing tool for myDiagram, defined in FreehandDrawingTool.js
		let tool = new FreehandDrawingTool();
		// provide the default JavaScript object for a new polygon in the model
		tool.archetypePartData =
			{ stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
		// allow the tool to start on top of an existing Part
		tool.isBackgroundOnly = false;
		// install as first mouse-move-tool
		selfDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);

		selfLoad(savedDiagram);  // load a simple diagram from the textarea
		document.getElementById("selfLoad").value = "N";
	}
}

function selfClearDiagram(savedDiagram) {
	clearDiagram(savedDiagram, selfDiagramObj);
}

function selfLoad(savedDiagram) {
	load(savedDiagram, selfDiagramObj);
}

//Sps
function spsInit(diagramId, savedDiagram) {
	let valid = document.getElementById("spsLoad").value;

	if (window.goSamples) goSamples();
	let $ = go.GraphObject.make;
	if (valid == "Y") {
		spsDiagramObj = $(go.Diagram, diagramId);
		spsDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
		spsDiagramObj.nodeTemplateMap.add("FreehandDrawing",
			$(go.Part,
				{ locationSpot: go.Spot.Center, isLayoutPositioned: false },
				new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
				{
					selectionAdorned: true, selectionObjectName: "SHAPE",
					selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
						$(go.Adornment, "Auto",
							$(go.Shape, { stroke: "dodgerblue", fill: null }),
							$(go.Placeholder, { margin: -1 }))
				},
				{ resizable: true, resizeObjectName: "SHAPE" },
				{ rotatable: true, rotateObjectName: "SHAPE" },
				{ reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
				$(go.Shape,
					{ name: "SHAPE", fill: null, strokeWidth: 1.5 },
					new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
					new go.Binding("angle").makeTwoWay(),
					new go.Binding("geometryString", "geo").makeTwoWay(),
					new go.Binding("fill"),
					new go.Binding("stroke"),
					new go.Binding("strokeWidth"))
			));
		// create drawing tool for myDiagram, defined in FreehandDrawingTool.js
		let tool = new FreehandDrawingTool();
		// provide the default JavaScript object for a new polygon in the model
		tool.archetypePartData =
			{ stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
		// allow the tool to start on top of an existing Part
		tool.isBackgroundOnly = false;
		// install as first mouse-move-tool
		spsDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
		spsLoad(savedDiagram);  // load a simple diagram from the textarea
		document.getElementById("spsLoad").value = "N";
	}
}

function spsClearDiagram(savedDiagram) {
	clearDiagram(savedDiagram, spsDiagramObj);
}

function spsLoad(savedDiagram) {
	load(savedDiagram, spsDiagramObj);
}
//End

//Advisor
function advInit(diagramId, savedDiagram) {
	let valid = document.getElementById("advLoad").value;
	if (valid == "Y") {
		if (window.goSamples) goSamples();
		let $ = go.GraphObject.make;

		advDiagramObj = $(go.Diagram, diagramId);
		advDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
		advDiagramObj.nodeTemplateMap.add("FreehandDrawing",
			$(go.Part,
				{ locationSpot: go.Spot.Center, isLayoutPositioned: false },
				new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
				{
					selectionAdorned: true, selectionObjectName: "SHAPE",
					selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
						$(go.Adornment, "Auto",
							$(go.Shape, { stroke: "dodgerblue", fill: null }),
							$(go.Placeholder, { margin: -1 }))
				},
				{ resizable: true, resizeObjectName: "SHAPE" },
				{ rotatable: true, rotateObjectName: "SHAPE" },
				{ reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
				$(go.Shape,
					{ name: "SHAPE", fill: null, strokeWidth: 1.5 },
					new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
					new go.Binding("angle").makeTwoWay(),
					new go.Binding("geometryString", "geo").makeTwoWay(),
					new go.Binding("fill"),
					new go.Binding("stroke"),
					new go.Binding("strokeWidth"))
			));
		// create drawing tool for myDiagram, defined in FreehandDrawingTool.js
		let tool = new FreehandDrawingTool();
		// provide the default JavaScript object for a new polygon in the model
		tool.archetypePartData =
			{ stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
		// allow the tool to start on top of an existing Part
		tool.isBackgroundOnly = true;
		// install as first mouse-move-tool
		advDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
		advLoad(savedDiagram);  // load a simple diagram from the textarea
		document.getElementById("advLoad").value = "N";
	}
}

function advClearDiagram(savedDiagram) {
	clearDiagram(savedDiagram, advDiagramObj);
}

function advLoad(savedDiagram) {
	load(savedDiagram, advDiagramObj);
}

function clearDiagram(savedDiagram, diagramObj) {
	let str1 = '{ "position": "-5 -5", "model": { "class": "GraphLinksModel", "nodeDataArray": [], "linkDataArray": []} }';
	document.getElementById(savedDiagram).value = str1;

	let str = document.getElementById(savedDiagram).value;
	try {
		let json = JSON.parse(str);

		diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
		diagramObj.model = go.Model.fromJson(json.model);
		diagramObj.model.undoManager.isEnabled = true;
	} catch (ex) {
		// alert(ex);
	}
}

function load(savedDiagram, diagramObj) {
	let str = document.getElementById(savedDiagram).value;
	try {
		let json = JSON.parse(str);
		diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
		diagramObj.model = go.Model.fromJson(json.model);
		diagramObj.model.undoManager.isEnabled = true;
	} catch (ex) {
		// alert(ex);
	}
}
//End

//Manager
function mgrInit(diagramId, savedDiagram) {
	let valid = document.getElementById("mgrLoad").value;
	if (valid == "Y") {
		if (window.goSamples) goSamples();
		let $ = go.GraphObject.make;
		mgrDiagramObj = $(go.Diagram, diagramId);
		mgrDiagramObj.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
		mgrDiagramObj.nodeTemplateMap.add("FreehandDrawing",
			$(go.Part,
				{ locationSpot: go.Spot.Center, isLayoutPositioned: false },
				new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
				{
					selectionAdorned: true, selectionObjectName: "SHAPE",
					selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
						$(go.Adornment, "Auto",
							$(go.Shape, { stroke: "dodgerblue", fill: null }),
							$(go.Placeholder, { margin: -1 }))
				},
				{ resizable: true, resizeObjectName: "SHAPE" },
				{ rotatable: true, rotateObjectName: "SHAPE" },
				{ reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
				$(go.Shape,
					{ name: "SHAPE", fill: null, strokeWidth: 1.5 },
					new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
					new go.Binding("angle").makeTwoWay(),
					new go.Binding("geometryString", "geo").makeTwoWay(),
					new go.Binding("fill"),
					new go.Binding("stroke"),
					new go.Binding("strokeWidth"))
			));
		// create drawing tool for myDiagram, defined in FreehandDrawingTool.js
		let tool = new FreehandDrawingTool();
		// provide the default JavaScript object for a new polygon in the model
		tool.archetypePartData =
			{ stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
		// allow the tool to start on top of an existing Part
		tool.isBackgroundOnly = false;
		// install as first mouse-move-tool
		mgrDiagramObj.toolManager.mouseMoveTools.insertAt(0, tool);
		mgrLoad(savedDiagram);  // load a simple diagram from the textarea
		document.getElementById("mgrLoad").value = "N";
	}
}

function mgrClearDiagram(savedDiagram) {
	clearDiagram(savedDiagram, mgrDiagramObj);
}

function mgrLoad(savedDiagram) {
	load(savedDiagram, mgrDiagramObj);
}

function mode(draw) {
	let tool = myDiagram.toolManager.findTool("FreehandDrawing");
	tool.isEnabled = draw;
}

function updateAllAdornments() {  // called after checkboxes change Diagram.allow...
	myDiagram.selection.each(function (p) { p.updateAdornments(); });
}

// save a model to and load a model from Json text, displayed below the Diagram
function selfSave() {
	makeBlob(selfDiagramObj);
}

function spsSave() {
	makeBlob(spsDiagramObj);
}

function advSave() {
	makeBlob(advDiagramObj);
}

function mgrSave() {
	makeBlob(mgrDiagramObj);
}

function makeBlob(selfDiagramObj) {
	let blob = selfDiagramObj.makeImageData({ background: "white", returnType: "blob", callback: sendSignData });
}

function sendSignData(blob) {
	mgrSign = blob;
	let url = URL.createObjectURL(blob);

	let signByte;
	let request = new XMLHttpRequest();
	request.open('GET', url, true);
	request.responseType = 'blob';
	request.onload = function () {
		let reader = new FileReader();
		reader.readAsDataURL(request.response);
		reader.onload = function (e) {
			signByte = e.target.result;
			getImage(signByte);

		};
	};
	request.send();
}

function getImage(signByte) {
	let perType = $('#perType').val();

	let str;
	let esignId;
	if (perType == "CLIENT") {
		str = '{ "position": "' + go.Point.stringify(selfDiagramObj.position) + '",\n  "model": ' + selfDiagramObj.model.toJson() + ' }';
		document.getElementById("selfSavedDiagram").value = str;
		esignId = document.getElementById('selfSignId').value;
		//alert(str);
	} else if (perType == "SPS") {
		str = '{ "position": "' + go.Point.stringify(spsDiagramObj.position) + '",\n  "model": ' + spsDiagramObj.model.toJson() + ' }';
		document.getElementById("spsSavedDiagram").value = str;
		esignId = document.getElementById('spsSignId').value;
		//alert(str);
	}
	else if (perType == "ADVISOR") {
		str = '{ "position": "' + go.Point.stringify(advDiagramObj.position) + '",\n  "model": ' + advDiagramObj.model.toJson() + ' }';
		document.getElementById("advSavedDiagram").value = str;
		esignId = document.getElementById('advSignId').value;
		//alert(str);
	} else if (perType == "MANAGER") {
		str = '{ "position": "' + go.Point.stringify(mgrDiagramObj.position) + '",\n  "model": ' + mgrDiagramObj.model.toJson() + ' }';
		document.getElementById("mgrSavedDiagram").value = str;
		esignId = document.getElementById('mgrSignId').value;
		//alert(str);
	}

	let signImgString = signByte.split(",")[1];

	let fnaId = $("#hdnSessFnaId").val();

	saveSignature(fnaId, str, signImgString, perType, esignId);
}

function saveSignature(fnaId, str, signImgString, perType, esignId) {
	let curPage = $("#curPage").val();

	$.ajax({
		url: "FnaSignature/saveFnaSignature",
		type: "POST",
		// async:false,
		data: {
			fnaId: fnaId,
			esign: str,
			signDoc: signImgString,
			perType: perType,
			esignId: esignId,
			curPage: curPage
		},
		success: function (data) {
			//After isertion set esign id -Avoid duplicate entries
			setSignPersonData(data);
			//isEmailPopup();
			// alert("success");
		},
		error: function (data) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function isEmailPopup() {
	let fnaId = $("#hdnSessFnaId").val();
	$.ajax({
		url: 'FnaSignature/checkPopupMail/' + fnaId,
		type: 'GET',
		// async:false,
		success: function (data) {
			let totTabCount = $("#v-pills-tab").find("a").length;

			if (totTabCount == data.length) {
				$("#mailModal").modal("show");
			}
		},
		error: function (jqXHR, textStatus, errorThrown, data) {
			console.log('Error: ' + textStatus + ' - ' + errorThrown);
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

//Get all data by fnaId
function editSignData() {
	let fnaId = $("#hdnSessFnaId").val();
	getAllSign(fnaId);
}

//Set signature data
function getAllSign(fnaId) {
	signatureDoneArr = [];
	$.ajax({
		url: 'FnaSignature/getAllDataById/' + fnaId,
		type: 'GET',
		// async:false,
		success: function (data) {
			if (data.length > 0) {
				setAllSign(data);
			}

			genQrOrSign('CLIENT', 'Client1');
			if (!isEmpty(sessspsname)) {
				genQrOrSign('SPS', 'Client2');
			}
			genQrOrSign('ADVISER', 'Adviser');

			enableProgressBar();
		},
		error: function (jqXHR, textStatus, errorThrown, data) {
			console.log('Error: ' + textStatus + ' - ' + errorThrown);
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function enableProgressBar() {
	let fnaDetailsStr = $("#hdnFnaDetails").val();
	let fnaDetails = JSON.parse(fnaDetailsStr);

	if (fnaDetails.mgrEmailSentFlg == "Y") {
		$("#hTxtFldSignedOrNot").val("Y");
	} else {
		if ((isEmpty(sessspsname) && signatureDoneArr.length < 2) || (!isEmpty(sessspsname) && signatureDoneArr.length < 3)) {
			// startTimer();
			//if(signatureDoneArr.length != 0){	
			$("#applValidMessage").html('<small><img src="vendor/psgisubmit/img/remove.png" />&nbsp;Signature not yet completed!</small>')
			$("#btntoolbarsign").addClass("btn-bs3-danger");
			$("#btntoolbarsign").removeClass("btn-bs3-prime");
			$("#btntoolbarsign").find("i").removeClass("fa-pencil");
			$("#btntoolbarsign").find("i").addClass("fa-exclamation-circle");
			$("#hTxtFldSignedOrNot").val("N");
		} else {
			$("#applValidMessage").html('<small><img src="vendor/psgisubmit/img/check.png" />&nbsp;All signatures are captured</small>');
			$("#btntoolbarsign").removeClass("btn-bs3-danger");
			$("#btntoolbarsign").addClass("btn-bs3-prime");
			$("#btntoolbarsign").find("i").addClass("fa-pencil");
			$("#btntoolbarsign").find("i").removeClass("fa-exclamation-circle");

			$("#btnAllPgeNoti").addClass("fa-check").removeClass("fa-remove");
			$("#btnAllPgeNoti").closest("button").removeAttr("disabled");
			$("#btnAllPgeNoti").closest("button").addClass("btn-success").removeClass("btn-secondary");

			$("#btnSignStatus").addClass("fa-check").removeClass("fa-remove");
			$("#btnSignStatus").closest("button").removeAttr("disabled");
			$("#btnSignStatus").closest("button").addClass("btn-success").removeClass("btn-secondary");

			//$(".maincontentdiv").addClass("disabledMngrSec");

			$("#hTxtFldSignedOrNot").val("Y");
		}
	}
}

var selfData;
var spsData;
var advData;
var mgrData;
function setAllSign(data) {
	fnaSignData = data;

	for (let signIdx = 0; signIdx < data.length; signIdx++) {
		if (signatureDoneArr.includes(data[signIdx].signPerson) === false) {
			signatureDoneArr.push(data[signIdx].signPerson);
		}
		setSignPersonData(data[signIdx]);
	}
}

function setSignPersonData(signData) {
	let signPerson = signData.signPerson;
	let fnaDetails = {};
	if (signPerson == "CLIENT") {
		selfData = signData;
		fnaDetails = selfData.fnaDetails;
		enableApproveProgress(fnaDetails);
		setViewSign(signData, "client1Qr", "selfSignDate");
		$("#signmodalclient1qrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
	} if (signPerson == "SPS") {
		spsData = signData;
		setViewSign(signData, "client2Qr", "spsSignDate");
		$("#signmodalclient2qrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
	} if (signPerson == "ADVISER") {
		advData = signData;
		setViewSign(signData, "advQr", "advSignDate");
		$("#signmodaladviserqrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
	} if (signPerson == "MANAGER") {

		mgrData = signData;
		setViewSign(signData, "mgrQr", "mgrSignDate");
		$("#signmodalmanagerqrsign").removeClass("fa-exclamation-triangle").addClass("fa-check-circle");
		let supreFlg = $('input[name="suprevMgrFlg"]:checked').val();
		if (supreFlg != undefined) {
			$("#mgrMailModal").modal("show");
		}
	}
}

function enableApproveProgress(fnaDetails) {
	/*alert(fnaDetails.adminApproveStatus);*/

	if (fnaDetails.mgrApproveStatus == "APPROVE") {
		$("#btnMngrApproval").addClass("fa-check").removeClass("fa-remove");
		$("#btnMngrApproval").closest("button").removeAttr("disabled");
		$("#btnMngrApproval").closest("button").addClass("btn-success").removeClass("btn-secondary");
	}
	if (fnaDetails.adminApproveStatus == "APPROVE") {
		$("#btnAdmnApproval").addClass("fa-check").removeClass("fa-remove");
		$("#btnAdmnApproval").closest("button").removeAttr("disabled");
		$("#btnAdmnApproval").closest("button").addClass("btn-success").removeClass("btn-secondary");
	}
	if (fnaDetails.compApproveStatus == "APPROVE") {
		$("#btnCompApproval").addClass("fa-check").removeClass("fa-remove");
		$("#btnCompApproval").closest("button").removeAttr("disabled");
		$("#btnCompApproval").closest("button").addClass("btn-success").removeClass("btn-secondary");
	}
}

function setViewSign(signData, roleQr, signDateId) {
	let byteArray = signData.signDocBlob;

	if ((byteArray != "") && (byteArray != null)) {
		$("." + roleQr + "SecNoData").removeClass("show").addClass("hide")
		$("." + roleQr + "Sec").removeClass("hide").addClass("show")
		$("." + roleQr).attr('src', 'data:image/png;base64,' + byteArray);
	} else {
		$("." + roleQr + "Sec").removeClass("show").addClass("hide")
		$("." + roleQr + "NoData").removeClass("hide").addClass("show")
	}

	$("#" + signDateId).text(formatDate(signData.signDate));

	if (approvalScreen != "true") {
		if ((isEmpty(sessspsname) && signatureDoneArr.length == 2) || (!isEmpty(sessspsname) && signatureDoneArr.length == 3)) {
			// setTimeout(function(){
			let fnaDetailsStr = $("#hdnFnaDetails").val();
			if (isEmpty(fnaDetailsStr)) {
				setPageKeyInfoSucs();
				setPageSignInfoSucs();
				clearInterval(timer);
				showManagerMailConfirm();
			} else {
				let fnaDetails = JSON.parse(fnaDetailsStr);
				if (fnaDetails && fnaDetails.mgrEmailSentFlg != "Y") {
					setPageKeyInfoSucs();
					setPageSignInfoSucs();
					clearInterval(timer);
					showManagerMailConfirm();
				} else {
					setPageManagerSts();
				}
			}
			// }, 200);
		}
	}
}

function selfSignData() {
	if (selfData != undefined) {
		setSignData(selfData, 'selfSignId', selfDiagramObj, 'selfSavedDiagram');
	}
}

function advSignData() {
	if (advData != undefined) {
		setSignData(advData, 'advSignId', advDiagramObj, 'advSavedDiagram');
	}
}

function mgrSignData() {
	if (mgrData != undefined) {
		setSignData(mgrData, 'mgrSignId', mgrDiagramObj, 'mgrSavedDiagram');
	}
}

function spsSignData() {
	if (spsData != undefined) {
		setSignData(spsData, 'spsSignId', spsDiagramObj, 'spsSavedDiagram');
	}
}

function setSignData(signData, signId, diagramObj, savedDiagram) {
	if (signData.esignId != undefined) {
		document.getElementById(signId).value = signData.esignId;
	}
	try {
		let json = signData.eSign;
		diagramObj.initialPosition = go.Point.parse(json.position || "-5 -5");
		diagramObj.model = go.Model.fromJson(json.model);
		diagramObj.model.undoManager.isEnabled = true;
		document.getElementById(savedDiagram).value = signData.eSign;
	} catch (ex) {
		// alert(ex);
	}
}

function generateQrCode(perType, id) {
	Swal.fire({
		title: "Clear Signature",
		text: "Are you sure to clear the signature?",
		type: "warning",
		showCancelButton: true,
		allowOutsideClick: false,
		allowEscapeKey: false,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "Yes, Clear it!",
		cancelButtonText: "No,Cancel",
		closeOnConfirm: false,
		//showLoaderOnConfirm: true
		closeOnCancel: false
	}).then((result) => {
		if (result.isConfirmed) {

			if (perType == "CLIENT") {
				selfData = null;
				$("#spanClientSignWaitMsg").html("");
				$("#signmodalclient1qrsign").removeClass("fa-check-circle").addClass("fa-exclamation-triangle");
			}

			if (perType == "SPS") {
				spsData = null;
				$("#spanSpsSignWaitMsg").html("");
				$("#signmodalclient2qrsign").removeClass("fa-check-circle").addClass("fa-exclamation-triangle");
			}

			if (perType == "ADVISER") {
				advData = null;
				$("#spanAdvSignWaitMsg").html("");
				$("#signmodaladviserqrsign").removeClass("fa-check-circle").addClass("fa-exclamation-triangle");
			}

			if (perType == "MANAGER") {
				mgrData = null;
				$("#spanMgrSignWaitMsg").html("");
			}

			swal.close();

			hideSignPad(id);
			getQrCode(perType, 'qrCode' + id, 'signLoader' + id, 'true');
			signatureDoneArr.splice(signatureDoneArr.indexOf(perType), 1);

		} else {
			swal("Cancelled", "", "error");
		}
	});
}

function showManagerMailConfirm() {
	$('#ScanToSignModal').modal('hide');
	if (!document.cookie.includes("managerApprovalHide=yes")) {
		Swal.fire({
			title: "Manager Approval",
			width: 800,
			allowOutsideClick: false,
			allowEscapeKey: false,
			showClass: {
				popup: 'animate__animated animate__fadeInDown'
			},
			hideClass: {
				popup: 'animate__animated animate__fadeOutUp'
			},
			text: "This FNA details can send to Manager Approval.",
			icon: "info",
			showDenyButton: true,
			showCancelButton: true,
			cancelButtonText: "No, Will Send Later",
			confirmButtonText: "Yes, Send Now",
			denyButtonText: "Upload Docs. + Mail",
			//showCloseButton: false,
		}).then((result) => {
			console.log(result, "result")
			if (result.isConfirmed) {
				// sendMailtoManger();
				$("#mailModal").modal("show");
				// clearInterval(timer);
			} else if (result.isDenied) {
				$("#btnAppUpload").trigger("click");
				setTimeout(function() {
					$("#btnUplodAndEmail").removeClass('d-none');
				}, 500);
				// $("#btnWithoutUplodAndEmail").removeClass('d-none');
				// $("#btnUplodCustAttchFile").addClass("d-none")
				// } else if(result.isDismissed){
				// 	var fnaId=$("#hdnSessFnaId").val();
				// 	Cookies.set(fnaId, "Y");
			} else if (result.isDismissed) {
				document.cookie = "managerApprovalHide=yes; path=/";
			}
		});
	}
}

/*$('#mailModal').on('shown.bs.modal', function (e) {
	clearInterval(timer);
});*/

function cancelScanToSign() {

	clearInterval(timer);

	/*if(approvalscreen == "true"){
		$("#suprevFollowReason").val("")
		$("#diManagerSection").find('input:radio').prop('checked',false);
		
		
		
		mgrClearDiagram('mgrSavedDiagram');
		
		mgrData=null;
		$("#spanMgrSignWaitMsg").html("")	;
		$("#btnMgrSignToggle").trigger("click");//temp
		
	}*/
	setPageManagerSts();
	$('#ScanToSignModal').modal('hide');
}

function closeScanToSign() {
	clearInterval(timer);

	/*if(approvalscreen == "true"){
		
		$('#cover-spin').show(0);

		setTimeout(function() { mgrSave() ;}, 0);	
		
	}else{*/

	if ($("#curPage").val() == "ADVISER") {
		$('#cover-spin').show(0);
		advSave();
	}

	if (approvalScreen != "true") {
		if ((isEmpty(sessspsname) && signatureDoneArr.length == 2) || (!isEmpty(sessspsname) && signatureDoneArr.length == 3)) {
			let fnaDetailsStr = $("#hdnFnaDetails").val();
			if (isEmpty(fnaDetailsStr)) {
				setPageKeyInfoSucs();
				setPageSignInfoSucs();
				showManagerMailConfirm();
			} else {
				let fnaDetails = JSON.parse(fnaDetailsStr);
				if (fnaDetails && fnaDetails.mgrEmailSentFlg != "Y") {
					setPageKeyInfoSucs();
					setPageSignInfoSucs();
					showManagerMailConfirm();
				} else {
					setPageManagerSts();
				}
			}
		}
	}
	// }

	$('#ScanToSignModal').modal("hide");
}
/*Qr code end*/