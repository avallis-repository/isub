 


$("#country").val("SINGAPORE");

//$("#tenantName").focus();

$("#parentName").prop("readOnly",true)
 let strcountry = $("#country").val();
 
 if (strcountry == "SINGAPORE"){
        $("#state").val("").prop("readOnly", true)
        $("#city").val("").prop("readOnly", true)
		
 }else {
        $("#state").prop("readOnly", false)
		$("#city").prop("readOnly", false)
		 
 }
 

var searchtabletblMasterTenantDis = $('#tblMasterTenantDis').DataTable({
	scrollX: false,
	scrollY: "500px",
	scroller: true,
	scrollCollapse: true,
	autoWidth: false
	/* paging: true,
	pagingType: "simple", */
/*	columnDefs: [
		{
			"targets": [7],
			"visible": false,
			"searchable": false
		}
	]*/
});
 
 
 
listMasterTenantDist();

function listMasterTenantDist(){

$.ajax({
	
	type:'GET',
	url:'MasterTenantAdviserDetails/masterTenantDistList',
	dataType: "json",
	contentType: "application/json",
	success:function(tenantDisList){
	//console.log(tenantDisList);
    //tenantDisList &&
				if ( tenantDisList.length > 0) {

					//var data = JSON.parse(tenantDisList);
					for (let tenantIdX = 0; tenantIdX < tenantDisList.length; tenantIdX++) {
						let jsonObj = (tenantDisList[tenantIdX]);

						col0 = jsonObj.tenantName;
						col1 = jsonObj.mnemonic;
						col2 = jsonObj.contactRef;
						col3 = jsonObj.registrationNo;
						col4 = jsonObj.emailId;
						/*col5 = jsonObj.parentName;*/
						col5 = '<a href="editMasterTenantDis?'+window.btoa(jsonObj.tenantId)+'">Edit</a>'+"&nbsp;&nbsp;"+'<a href="#" id="'+jsonObj.tenantId+'"  onclick="DeleteMasterTenantform(this);" >Delete</a>';      
						searchtabletblMasterTenantDis.row.add([col0, col1, col2, col3, col4, col5]).draw(false);
					}

					setTimeout(function () {
						searchtabletblMasterTenantDis.columns.adjust();
					}, 200);
				}
	
	},
	error: function (xhr, textStatus, errorThrown) {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}

});
/*<button id="deletetenant" value="'+jsonObj.tenantId+'" onclick="DeleteMasterTenantform(this);" >Delete</button>*/
//window.location.href = "editMasterTenantDis?" + window.btoa(tenantId);
}
 
 
 
function openTabSec(value) {
 
if (value == 'Prev') {

 window.location.href = "masterTenantDist";
 
	//	showMasterTenantListSec();
		
	}
	
	if (value == 'Next') {
		
		showMasterTenantSec();
		//$("#tenantName").focus();
		setTimeout(function () { $("#tenantName").focus(); }, 500);
	}
	 
}


function showMasterTenantSec() {

	$('#MasterTenantListSec').removeClass('js-active');
	 
	$('#MasterTenantSec').addClass('js-active');
	
	//$("#tenantName").focus();
	
	
}

function showMasterTenantListSec() {
	 
	 
	$('#MasterTenantSec').removeClass('js-active');
	$('#MasterTenantListSec').addClass('js-active');
}



	function isEmptyorundefined(value) {
		if(value){
			 value = value.trim();
		}
		if (value == "" || value == null || value == undefined){
			return true;
		}else{
			return false;
	    }
	}
	 
  
// Master Tenant Dist Detls mandotary Fld Validation
function validatemasterTenantDetls() {

		let tenantName, mnemonic,loginAppear,contactRef,emailId,registrationNo,address1,pinCode,country, Fld = "Error";
		 
		tenantName = $('#tenantName').val();
		mnemonic = $('#mnemonic').val();
		loginAppear= $('input[name="loginAppear"]:checked').val();
		contactRef = $('#contactRef').val();
		emailId = $('#emailId').val();
		registrationNo = $('#registrationNo').val();
		pinCode = $('#pinCode').val();
		country = $('#country').val();
		address1 = $('#address1').val();

//isEmptyFld
		if (isEmptyorundefined(tenantName)) {
			$("#tenantName" + Fld).removeClass("hide").addClass("show");
			$("#tenantName").addClass('err-fld');
			$("#tenantName").focus();
			return;
		} else {
			$("#tenantName" + Fld).removeClass("show").addClass("hide");
			$("#tenantName").removeClass('err-fld');
		}

		if (isEmptyorundefined(mnemonic)) {
			$("#mnemonic" + Fld).removeClass("hide").addClass("show");
			$("#mnemonic").addClass('err-fld');
			$("#mnemonic").focus();
			return;
		} else {
			$("#mnemonic" + Fld).removeClass("show").addClass("hide");
			$("#mnemonic").removeClass('err-fld');
		}
		
		if (isEmptyorundefined(loginAppear)) {
			$("#loginAppear" + Fld).removeClass("hide").addClass("show");
			$("#loginAppear").addClass('err-fld');
			$("#loginAppearY").focus();
			//$("#loginAppear").focus();
			
		//	$('#loginAppear input').focus(function() {
         //   $('input:radio[name=loginAppear]').attr('checked', false);
         //   });
  
			return;
		} else {
			$("#loginAppear" + Fld).removeClass("show").addClass("hide");
			$("#loginAppear").removeClass('err-fld');
		}
		
		if (isEmptyorundefined(contactRef)) {
			$("#contactRef" + Fld).removeClass("hide").addClass("show");
			$("#contactRef").addClass('err-fld');
			$("#contactRef").focus();
			return;
		} else {
			$("#contactRef" + Fld).removeClass("show").addClass("hide");
			$("#contactRef").removeClass('err-fld');
		}
	 
		if (isEmptyorundefined(emailId)) {
			$("#emailId" + Fld).removeClass("hide").addClass("show");
			$("#emailId").addClass('err-fld');
			$("#emailId").focus();
			return;
		} else {
			$("#emailId" + Fld).removeClass("show").addClass("hide");
			$("#emailId").removeClass('err-fld');
		}
	 
		//validate email format while ckick next btn
		if (!isEmptyorundefined(emailId)) {
			let regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (!regex.test(emailId)) {
				$("#emailId" + Fld).removeClass('hide').addClass('show');
				$("#emailId" + Fld).html("Invalid Email Id Format");
				$("#emailId").focus();
				return;
			} else {
				$("#emailId" + Fld).removeClass('show').addClass('hide');
			}
		}
	 
		 if (isEmptyorundefined(registrationNo)) {
				$("#registrationNo" + Fld).removeClass("hide").addClass("show");
				$("#registrationNo").addClass('err-fld');
				$("#registrationNo").focus();
				return;
			} else {
				$("#registrationNo" + Fld).removeClass("show").addClass("hide");
				$("#registrationNo").removeClass('err-fld');
			}
		
		if (isEmptyorundefined(address1)) {
			$("#address1" + Fld).removeClass("hide").addClass("show");
			$("#address1").addClass('err-fld');
			$("#address1").focus();
			return;
		} else {
			$("#address1" + Fld).removeClass("show").addClass("hide");
			$("#address1").removeClass('err-fld');
		}
		
		if (isEmptyorundefined(pinCode)) {
			$("#pinCode" + Fld).removeClass("hide").addClass("show");
			$("#pinCode").addClass('err-fld');
			$("#pinCode").focus();
			return;
		} else {
			$("#pinCode" + Fld).removeClass("show").addClass("hide");
			$("#pinCode").removeClass('err-fld');
		}
		
		if (isEmptyorundefined(country)) {
			$("#country" + Fld).removeClass("hide").addClass("show");
			$("#country").addClass('err-fld');
			$("#country").focus();
			return;
		} else {
			$("#country" + Fld).removeClass("show").addClass("hide");
			$("#country").removeClass('err-fld');
		}

	return true;
}
 

 
  
function setCountry(custvalue) {
	custvalue = custvalue.toUpperCase();

	if (custvalue == "SINGAPORE") {
		$("#country").val(custvalue);
		$("#city").val("").prop("readonly", true)
		$("#state").val("").prop("readonly", true)
	} else {
		$("#city").prop("readonly", false)
		$("#state").prop("readonly", false)
		$("#country").val(custvalue);
	}
}
  

function ValidateMasterTenantform() {
//showMasterTenantSec();
	if (!validatemasterTenantDetls()) {  return; }
	
    // $("#MasterTenantformDelete").addClass('d-none');
     //$("#UpdateTenantformsave").addClass('d-none');

	let masterTenantData = {};
	let frmElements = $('.masterTenantDistFrm :input').serializeObject();
	$.map(frmElements, function (n, element) {
		 masterTenantData[element] = n;
	});
	
	let loginAppear = $('input[name="loginAppear"]:checked').val();
	
	if(loginAppear === "Y"){
	masterTenantData["loginAppear"] = true;
	}else if(loginAppear === "N"){
	masterTenantData["loginAppear"] = false;
	}
	
	let parent = $('input[name="parent"]:checked').val();
	
	if(parent === "Y"){
	masterTenantData["parent"] = true;
	}else if(parent === "N"){
	masterTenantData["parent"] = false;
	}
   
	$.ajax({
	 
		url: "MasterTenantAdviserDetails/masterTenantsaveData",
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(masterTenantData),
		// async:false,
		beforeSend: function () {
			$('#cover-spin').show();
		},
		success: function (response) {
			 
			 /* Swal.fire({
		      icon: "success",
			  text: "Master Tenant Dist Save Success!",
			});*/
			 
			Swal.fire({
			  /*title: '',*/
			  icon: "success",
			  text: "Master Tenant Dist Save Success!",
			  showDenyButton: false,
			  showCancelButton: false,
			  confirmButtonText: `Ok`,
			  
			}).then((result) => {
			  /* Read more about isConfirmed, isDenied below */
			  if (result.isConfirmed) {
			  window.location.href = "masterTenantDist";
			  var table = $('#tblMasterTenantDis').DataTable();
                  table.clear().draw();
     
			   clearvalues();
		       listMasterTenantDist();
			  }  
			})
			
		},
		complete: function () {
			$('#cover-spin').hide();
		},
		error: function (xhr, textStatus, errorThrown) {
			//alert("Error while updating client!" );//"Error!"  - replace by constant value
			 
			$('#cover-spin').hide();
			 Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}



function UpdateMasterTenantform(){

if (!validatemasterTenantDetls()) { showMasterTenantSec(); return; }
	
	//var table = $('#tblMasterTenantDis').DataTable();
    // table.clear().draw();
    
     // $("#MasterTenantformDelete").addClass('d-none');
      $("#UpdateTenantformsave").addClass('d-none');
      $("#MasterTenantformsave").removeClass('d-none');

	let masterTenantData = {};
	let frmElements = $('.masterTenantDistFrm :input').serializeObject();
	$.map(frmElements, function (n, element) {
		 masterTenantData[element] = n;
	});
	
	let loginAppear = $('input[name="loginAppear"]:checked').val();
	
	if(loginAppear === "Y"){
		masterTenantData["loginAppear"] = true;
	}else if(loginAppear === "N"){
		masterTenantData["loginAppear"] = false;
	}
	
	let parent = $('input[name="parent"]:checked').val();
	
	if(parent === "Y"){
		masterTenantData["parent"] = true;
	}else if(parent === "N"){
		masterTenantData["parent"] = false;
	}
	
	let tenantData = $("#tenantJsonData").val();
	if(tenantData){
		let tendata = JSON.parse(tenantData);
	//console.log(" update tendata" + tendata);
		if(tendata){
			masterTenantData["tenantId"] = tendata.tenantId;
			masterTenantData["createdDate"] = tendata.createdDate;
			masterTenantData["createdBy"] = tendata.createdBy;
		}
	}
	
   let  tenantId = $("#tenantId").val();
	// alert("tenantId " + tenantId);
	$.ajax({
		url: "MasterTenantAdviserDetails/masterTenantupdateData",
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(masterTenantData),
		// async:false,
		beforeSend: function () {
			$('#cover-spin').show();
		},
		success: function (response) {
		//alert("success");
			
			/*  Swal.fire({
		      icon: "success",
			  text: "Master Tenant Dist Update Success!",
			});*/
			
			Swal.fire({
			  /*title: 'Master Tenant Dist Update Success!',*/
			  icon: "success",
			  text: "Master Tenant Dist Update Success!",
			  showDenyButton: false,
			  showCancelButton: false,
			  confirmButtonText: `Ok`,
			  
			}).then((result) => {
			  /* Read more about isConfirmed, isDenied below */
			  if (result.isConfirmed) {
			   window.location.href = "masterTenantDist";
			   searchtabletblMasterTenantDis.clear().draw();
			   clearvalues();
			   listMasterTenantDist();
			  
			   
			  }  
			})
			
		},
		complete: function () {
			$('#cover-spin').hide();
		},
		error: function (xhr, textStatus, errorThrown) {
			//alert("Error while updating client!" );//"Error!"  - replace by constant value
			 
			$('#cover-spin').hide();
			 Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});

}
 

/*$('#tblMasterTenantDis tbody').on('click', 'tr', function () {
	let tenantData = searchtabletblMasterTenantDis.row(this).data();
	let tenantId = tenantData[6];
	 
	window.location.href = "editMasterTenantDis?" + window.btoa(tenantId);
	
	//showMasterTenantSec()
	 
	
});*/

//Edit 
let tenantData = $("#tenantJsonData").val();
if (tenantData != null && tenantData != '') {
 
	setMasterTenantDetails(tenantData);
}


function setMasterTenantDetails(tenantData){
 
let tenantdata = JSON.parse(tenantData);
 
if(tenantdata){
		let loginAppear = tenantdata.loginAppear;
	
		if(loginAppear){
		
				  //$("INPUT[name='loginAppear']").val("Y");
				    $("#loginAppearY").val("Y");
					$("#loginAppearY").prop("checked", true);
			 
		}else{
				
				//$("INPUT[name='loginAppear']").val("N");
				    $("#loginAppearN").val("N");
				    $("#loginAppearN").prop("checked", true);
				 
				}
		
		  let parent = tenantdata.parent;
			if(parent != null && parent != undefined ){
					if(parent){
					 
					//$("INPUT[name='parent']").val("Y");
						$("#parentY").prop("checked", true);
						$("#parentY").val("Y");
				        $("#parentName").prop("readOnly",false);
				        
					}else{
					    //$("INPUT[name='parent']").val("N");
					    $("#parentN").val("N");
						$("#parentN").prop("checked", true);
						$("#parentName").prop("readOnly",true);
					 
					}
				}
			}
		
		$('#tenantName').val(tenantdata.tenantName);
		$('#mnemonic').val(tenantdata.mnemonic);
		$('#contactRef').val(tenantdata.contactRef);
		$('#emailId').val(tenantdata.emailId);
		$('#registrationNo').val(tenantdata.registrationNo);
		$('#address1').val(tenantdata.address1);
		$('#address2').val(tenantdata.address2);
		$('#address3').val(tenantdata.address3);
		$('#pinCode').val(tenantdata.pinCode);
		$('#country').val(tenantdata.country);
		$('#city').val(tenantdata.city);
		$('#state').val(tenantdata.state);
		$('#telephoneOff').val(tenantdata.telephoneOff);
		$('#parentName').val(tenantdata.parentName);
		$('#previousName').val(tenantdata.previousName);
		$('#tenantName').val(tenantdata.tenantName);
		$('#website').val(tenantdata.website);
		
		 let strCountry = $("#country").val();
 
			 if (strCountry == "SINGAPORE"){
			        $("#state").val("").prop("readOnly", true)
			        $("#city").val("").prop("readOnly", true)
					
			 }else {
			        $("#state").prop("readOnly", false)
					$("#city").prop("readOnly", false)
					 
			 }
		
		//$("#MasterTenantformDelete").removeClass('d-none');
		$("#UpdateTenantformsave").removeClass('d-none');
		$("#MasterTenantformsave").addClass('d-none');
		
		 showMasterTenantSec();
		 setTimeout(function () { $("#tenantName").focus() }, 500);
		 
}


function DeleteMasterTenantform(id){
   
   var tenantId = $(id).attr("id");

	//var tenantId = $(id).find('input:hidden[name=hdntenantId]').val();
	//alert(tenantId);
	
	Swal.fire({
	 // title: 'Do you want to delete this record?',
	  icon: "question",
	  text:'Are you sure you want to delete Tenant and Adviser record(s) for this Tenant?',
	  showDenyButton: true,
	  showCancelButton: false,
	  confirmButtonText: `Delete`,
	  denyButtonText: `Don't delete`,
	}).then((result) => {
	  
		  if (result.isConfirmed) {
		  
		  //var tenantId = $('#deletetenant').val();
		
				$.ajax({
					url: "MasterTenantAdviserDetails/deleteMasterTenantDis/" + tenantId,
					type: "DELETE",
					dataType: "json",
					//contentType: "application/json",
					//async: false,
					success: function (response) {
				//	console.log(data);
				//	console.log((data.status));
					// let response = JSON.parse(data);
				  if(response.status == "success"){
					//$("#MasterTenantformDelete").addClass('d-none');
					
					$("#UpdateTenantformsave").addClass('d-none');
					$("#MasterTenantformsave").removeClass('d-none');
					  
						//Swal.fire('Delete!', '', 'success')
					  
					   Swal.fire({
						  icon: "success",
						  text: "Delete!",
						  confirmButtonText: `Ok`,
						}).then((result) => {
						  if (result.isConfirmed) {
						  window.location.href = "masterTenantDist";
						  
						 //  var table = $('#tblMasterTenantDis').DataTable();
			               searchtabletblMasterTenantDis.clear().draw();
			               
			               clearvalues();
				           listMasterTenantDist();
			         
						  }  
						})
						
						}
						if(response.status == "failed"){
						 Swal.fire({
						icon: 'error',
						text: 'Unable to delete this Tenant?',
					});
						
						}
					},
				    complete: function () {
						 
					},
					
					error: function (xhr, textStatus, errorThrown) {
						Swal.fire({
							icon: 'error',
							text: 'Please Try again Later or else Contact your System Administrator',
						});
					}
				});
		    
		  } else if (result.isDenied) {
		    Swal.fire('Record are not delete', '', 'info')
		  }
	})
	 

}

 
function filtrCuntry(obj) {
	let thisVal = $(obj).find("option:selected").val();

	if (thisVal == "SINGAPORE") {
		$("#city").val("").prop("readonly", true)
		$("#state").val("").prop("readonly", true)
	} else {
		$("#city").prop("readonly", false)
		$("#state").prop("readonly", false)
	}
}
 
 
 //$("#parentName").val("").prop("readOnly",true)
 
$('input[name=parent]').click(function () {

	let strparent = $(this).val();
	
	if (strparent == "Y") {
	    $('#parentY').val("Y");
        $("#parentName").val();
        $("#parentName").prop("readOnly",false);
	
	}else if(strparent == "N"){
	  
	     $('#parentN').val("N");
	     $("#parentName").val("");
	     $("#parentName").prop("readOnly",true);
	}
});

$('input[name=loginAppear]').click(function () {
	$("#loginAppearError").removeClass("show").addClass("hide");
	$("#loginAppear").removeClass('err-fld');
});


function clearvalues(){

        $('#tenantName').val("");
		$('#mnemonic').val("");
		//$('input[name="loginAppear"]').val("");
	    $('input[name="loginAppear"]').prop('checked', false);
		$('#contactRef').val("");
		$('#emailId').val("");
		$('#registrationNo').val("");
		$('#address1').val("");
		$('#address2').val("");
		$('#address3').val("");
		$('#pinCode').val("");
		$('#country').val("");
		$('#city').val("");
		$('#state').val("");
		$('#telephoneOff').val("");
		//$('input[name="parent"]').val("");
		$('input[name="parent"]').prop('checked', false);
		$('#parentName').val("");
		$('#previousName').val("");
		$('#website').val("");
		$('#createdDate').val("");
		$('#createdBy').val("");
		
		$("#country").val("SINGAPORE");
}              

//onkeypress="return isNumber(event)"
/*function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}*/

		
//master tenant page Mandotary field Validation onchange input value hide error Messages
$(".checkMand").bind("change", function () {
	let thisFldId = $(this).attr("id");
	let thisFldVal = $(this).val();

	if (isEmptyorundefined(thisFldVal)) {
		$("#" + thisFldId + "Error").removeClass("hide").addClass("show");
		$("#" + thisFldId).addClass('err-fld');
		return;
	} else {
		$("#" + thisFldId + "Error").removeClass("show").addClass("hide");
		$("#" + thisFldId).removeClass('err-fld');
	}
})

/*$("#checkWebLink").click( function() {
    var url = "http://" + $("#website").val();
    window.open(url,'_blank');
});*/
 
 
 
//Nric validation

$('input[name=registrationNo]').change(function () {

	var strRegistrationNo = $(this).val();
 
	if (strRegistrationNo) {
 
		$.ajax({
			url: "MasterTenantAdviserDetails/getAllTenantRegistreNoList/" + strRegistrationNo,
			type: "GET",
			// async:false,
			dataType: "json",
			contentType: "application/json",
			success: function (data) {

				//console.log(data);
				 
				if (data.length > 0) {
				 
					Swal.fire({
						//		  title: 'Are you Not Proceed?',
						text: "Registration No Already Exist. You have to clear it!",
						icon: 'warning',
						allowOutsideClick: false,
						allowEscapeKey: false,
						showCancelButton: false,
						confirmButtonColor: '#3085d6',
						cancelButtonColor: '#d33',
						confirmButtonText: 'Clear it!'
					   }).then((result) => {
						if (result.isConfirmed) {
							
							swal.fire("Cleared!", "Your Registration No Cleared", "success");
							
							$("#registrationNo").val('');
							$("#registrationNo").focus();
					  	}  
					  }); 
					  }

			},
			error: function (xhr, textStatus, errorThrown) {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	}
});



     $("#srchTenant").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $("#tblMasterTenantDis tr").filter(function() {
                  $(this).toggle($(this).find("td").text().toLowerCase().indexOf(value) > -1)
                });
               
              });
 
