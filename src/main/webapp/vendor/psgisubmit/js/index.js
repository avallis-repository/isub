var clientAutoCompList = [];

$(document).ready(function () {
	$('#searchIcon').hide();
	$('#goPreviousArrow').hide();

	$('#srchClntModal').on('shown.bs.modal', function () {
		//GetAllData
		// getAllCustomerDetails();
		$("#txtFldCustName").val("").focus();
	});

	$("#radNewClient").on("click", function () {
		window.location.href = "addClientInfo";
	});

	// $("#txtFldCustName").typeahead({ source:clientAutoCompList});
});

var searchtable = $('#srchClntTable').DataTable({
	/*processing: true,
	language: {
		'loadingRecords': '&nbsp;',
		'processing': '<div class="spinner"></div>'
	} ,*/

	scrollX: false,
	scrollY: "500px",
	scroller: true,
	scrollCollapse: true,
	autoWidth: false,
	/*paging:true,
	pagingType: "simple",*/
	columnDefs: [
		{
			"targets": [6],
			"visible": false,
			"searchable": false
		}
	]

});

$('#srchClntTable tbody').on('click', 'tr', function () {
	//$(this).addClass("bg-success");
	//$(this).css('cursor', 'pointer');
	let data = searchtable.row(this).data();

	if (data) {
		let custId = data[6];
		window.location.href = "editClientInfo?" + window.btoa(custId);
	}

});

function getAllCustomerDetails() {

	let custName = '';
	let client = $("#txtFldCustName").val();
	if (client.length == 0) {
		custName = client;
	} else {
		custName = client;
	}

	// show search results table
	$("#srchClntModal").find(".modal-body").find("#srchClntTblSec").removeClass('hide').addClass('show');

	$("#srchClntModal").find(".modal-body").find("#noteInfo").text("Don't click anywhere in the window, until it is searching your results!");

	//set overlay class for loader
	$("#srchClntModal").find(".modal-body").find("#secOverlay").addClass("disabledMngrSec");

	let loggedAdvId = $("#hTxtFldLoggedAdvId").val();
	let tblId = document.getElementById("srchClntTable");
	let tbody = tblId.tBodies[0];

	searchtable.clear().draw();
	let col0 = "", col1 = "", col2 = "", col3 = "", col4 = "", col5 = "", col6 = "";

	$.ajax({
		url: "CustomerDetails/getAllDataBySrchStr",
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		data: {"clientName": custName},
		// async:false,
		success: function (data) {
			for (let custIndex = 0; custIndex < data.length; custIndex++) {

				col0 = data[custIndex].custName;

				clientAutoCompList.push(col0);

				col1 = data[custIndex].custInitials;
				col2 = data[custIndex].nric;
				col3 = data[custIndex].dob;
				col4 = data[custIndex].resHandPhone;
				col6 = data[custIndex].custCateg;
				col5 = data[custIndex].custId;
				searchtable.row.add([col0, col1, col2, col3, col4, col6, col5]).draw(false);

			}
			//alert("Success");
			$("#srchClntModal").find(".modal-body").find('#srchClntLoader').removeClass("d-block").addClass("hide");
			//remove overlay class for loader
			$("#srchClntModal").find(".modal-body").find("#secOverlay").removeClass("disabledMngrSec");
			$("#srchClntModal").find(".modal-body").find(".noteInfoMsg").removeClass('hide').addClass('show');
			$("#srchClntModal").find(".modal-body").find("#noteInfo").text("Click a row to view the details");
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}
function masterTenantDist(){		 
	window.location.href = "masterTenantDist";
}
function masterAdviserManagement(){	 
	window.location.href = "masterAdviserManagement";
}