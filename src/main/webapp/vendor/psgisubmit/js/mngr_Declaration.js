$(document).ready(function () {
	let fnaId = $("#hdnSessFnaId").val();
	getFnaDets(fnaId);
	window.addEventListener("beforeunload", function (e) {
		// saveData();
	});
});

//Set and get Manager declartion
var arrMgrData = ["suprevMgrFlg", "suprevFollowReason"];
function getFnaData(fnaDets) {
	fnaDets.suprevMgrFlg = $('input[name="suprevMgrFlg"]:checked').val();
	fnaDets.suprevFollowReason = $("#suprevFollowReason").val();
	/*for(var i=0;i<arrMgrData.length;i++){
		fnaDets[arrMgrData[i]]=$("#"+arrMgrData[i]).val();
		alert(arrMgrData[i]+'---'+$("#"+arrMgrData[i]).val());
	}*/
}

function setFnaData(fnaDets) {
	if (fnaDets.suprevMgrFlg == "Y") {
		$("#suprevMgrFlgY").prop('checked', true);
	} else if (fnaDets.suprevMgrFlg == undefined) {
		$("#suprevMgrFlgY").prop('checked', false);
		$("#suprevMgrFlgN").prop('checked', false);
	} else {
		$("#suprevMgrFlgN").prop('checked', true);
	}
	if (fnaDets.mgrEmailSentFlg == "Y") {
		document.getElementById("mailSent").style.display = "block";
		document.getElementById("mailSentMgr").style.display = "block";
		document.getElementById("mailNotSent").style.display = "none";
		document.getElementById("mailNotSentMgr").style.display = "none";
	}
	$("#suprevFollowReason").val(fnaDets.suprevFollowReason);
	/*for(var i=0;i<arrMgrData.length;i++){
		$("#"+arrMgrData[i]).val(fnaDets[arrMgrData[i]]);
	}*/
}

//Save Data
function saveData(obj, nextscreen, navigateflg, infoflag) {
	updateFnaDets(obj, nextscreen, navigateflg, infoflag);
}
