$(document).ready(function () {
	let sessionTotalTime = $("#hTxtFldSessionTimeout").val();
	let cookie = document.cookie;

	if (!cookie.includes("session=valid") && !location.pathname.includes("signInfo")) {
		window.location.href = 'login';
	}
	if (sessionTotalTime && sessionTotalTime > 0) {
		let sessionBeforeEndTime = sessionTotalTime - (Number(sessionTotalTime) * 16.67 / 100);

		let defaults = {
			message: "Your session is about to expire!",
			title: "Session Timeout",
			logoutUrl: "login",
			sessionUpdateUrl: "sessionUpdate",
			warningAfter: sessionBeforeEndTime * 1000
		};

		let dialogTimer;
		let logoutTimer;

		function updateSession(result) {
			$.ajax({
				type: "POST",
				url: defaults.sessionUpdateUrl,
				data: JSON.stringify(result),
				success: function (result) {
					// nothing to show
				},
				error: function (err) {
					console.log("error on update session: " + err);
					Swal.fire({
						icon: 'error',
						text: 'Please Try again Later or else Contact your System Administrator',
					});
				}
			});
		}

		function sesssionTimeoutAlert(attrResult) {
			Swal.fire({
				icon: "info",
				title: defaults.title,
				text: defaults.message,
				showDenyButton: true,
				confirmButtonText: "Stay Connected",
				denyButtonText: "Logout Now",
				allowEscapeKey: false,
				allowOutsideClick: false
			}).then((result) => {
				if (result.isConfirmed) {
					controlDialogTimer('stop');
					controlDialogTimer('start');
					controlLogoutTimer("stop");
					controlLogoutTimer("start");

					updateSession(JSON.parse(attrResult));
				} else if (result.isDenied) {
					window.location.href = defaults.logoutUrl;
				}
			});
		}

		function controlDialogTimer(action) {
			switch (action) {
				case 'start':
					dialogTimer = setTimeout(function () {
						defaults.message = "Your session is about to expire!";
						saveSessionAttributes();
					}, defaults.warningAfter);
					break;
				case 'stop':
					clearTimeout(dialogTimer);
					break;
			}
		}

		controlDialogTimer('start');
		controlLogoutTimer("start");

		function controlLogoutTimer(action) {
			switch (action) {
				case "start":
					logoutTimer = setTimeout(function () {
						defaults.message = "Your session is expired!";
						saveSessionAttributes();
					}, sessionTotalTime * 1000);
					break;
				case "stop":
					clearTimeout(logoutTimer);
					break;
			}
		}

		function saveSessionAttributes() {
			$.ajax({
				type: "POST",
				url: "saveSessionAttributes",
				success: function (result) {
					sesssionTimeoutAlert(result);
				},
				error: function (err) {
					console.log("error on save session: " + err);
					Swal.fire({
						icon: 'error',
						text: 'Please Try again Later or else Contact your System Administrator',
					});
				}
			});
		}
	}
});