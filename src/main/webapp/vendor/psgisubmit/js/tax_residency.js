$(document).ready(function () {
	console.log("ready!");

	$('[data-toggle="ReasonAPopup"]').popover({
		placement: 'top',
		html: true,
		content: function () {
			return $('#popover-ReasonAPopup').html();
		}
	});

	$('[data-toggle="ReasonBPopup"]').popover({
		placement: 'top',
		html: true,
		content: function () {
			return $('#popover-ReasonBPopup').html();
		}
	});

	$('[data-toggle="ReasonCPopup"]').popover({
		placement: 'top',
		html: true,
		content: function () {
			return $('#popover-ReasonCPopup').html();
		}
	});

	let fnaId = $("#hdnSessFnaId").val();
	let spouseData = getFnaSelfSpouseDets(fnaId);

	window.addEventListener("beforeunload", function (e) {
		// saveData();
	});

	//always set self Client (1) Tab Active in Tax Residency Screen
	$('#selfSpouseTabs').children().eq(0).find("a.nav-link").trigger("click");
});

function chngChktoRadFATCA(chkbox, clnttype) {
	if (clnttype == "client") {
		let chkboxname = $(chkbox).prop("name");
		console.log(chkboxname, "chkboxname");

		if (chkbox.checked == true) {
			//$("input[name='dfSelfUsnotifyflg'], input[name='dfSelfUspersonflg']").attr("checked",false);

			chkbox.checked = true;
			switch (chkboxname) {
				case "dfSelfUsnotifyflg":
					$("#dfSelfUsnotifyflg1").prop("checked", true).val("Y");
					$("#collapseSec1").removeClass("hide").addClass("show");

					$("#dfSelfUspersonflg1").prop("checked", false).val("N");
					$("#collapseSec2").removeClass("show").addClass("hide");

					break;
				case "dfSelfUspersonflg":
					$("#dfSelfUspersonflg1").prop("checked", true).val("Y");
					$("#collapseSec2").removeClass("hide").addClass("show");
					$("#dfSelfUstaxno").val('');
					$("#dfSelfUstaxno").focus();

					$("#dfSelfUsnotifyflg1").prop("checked", false).val("N");
					$("#collapseSec1").removeClass("show").addClass("hide");

					if (page_wise_err) {
						removeErrFld($("#dfSelfUsnotifyflg1"));
						$("#spanerrdfSelfUsnotifyflg1").remove();
						page_wise_err = false;
						saveData();
						setTimeout(() => {
							$("#btntoolbarsign").trigger("click");
						}, 1000);
					}
					break;
			}
		} else if (chkbox.checked == false) {
			let chkboxname = $(chkbox).prop("name");
			if (chkboxname == 'dfSelfUsnotifyflg') {
				$(chkbox).val("N");
				$("#collapseSec1").removeClass("show").addClass("hide");
			}
			if (chkboxname == 'dfSelfUspersonflg') {
				$(chkbox).val("N");
				$("#collapseSec2").removeClass("show").addClass("hide");
			}
		}
	}

	if (clnttype == "spouse") {
		if (chkbox.checked == true) {
			//$("input[name='dfSpsUsnotifyflg'], input[name='dfSpsUspersonflg']").attr("checked",false);
			chkbox.checked = true;
			let chkboxname = $(chkbox).prop("name");

			switch (chkboxname) {
				case "dfSpsUsnotifyflg":
					$("#dfSpsUsnotifyflg1").prop("checked", true).val("Y");
					$("#collapseSec3").removeClass("hide").addClass("show");

					$("#dfSpsUspersonflg1").prop("checked", false).val("N");
					$("#collapseSec4").removeClass("show").addClass("hide");
					break;
				case "dfSpsUspersonflg":
					$("#dfSpsUspersonflg1").prop("checked", true).val("Y");
					$("#collapseSec4").removeClass("hide").addClass("show");

					$("#dfSpsUstaxno").val('');
					$("#dfSpsUstaxno").focus();

					$("#dfSpsUsnotifyflg1").prop("checked", false).val("N");
					$("#collapseSec3").removeClass("show").addClass("hide");

					if (page_wise_err) {
						removeErrFld($("#dfSpsUsnotifyflg1"));
						$("#spanerrdfSpsUsnotifyflg1").remove();
						page_wise_err = false;
						saveData();
						setTimeout(() => {
							$("#btntoolbarsign").trigger("click");
						}, 1000);
					}
					break;
			}
		} else if (chkbox.checked == false) {
			let chkboxname = $(chkbox).prop("name");
			if (chkboxname == 'dfSpsUsnotifyflg') {
				// alert(chkboxname)
				$(chkbox).val("N");
				$("#collapseSec3").removeClass("show").addClass("hide");
			}
			if (chkboxname == 'dfSpsUspersonflg') {
				// alert(chkboxname)
				$(chkbox).val("N");
				$("#collapseSec4").removeClass("show").addClass("hide");
			}
		}
	}
	chkNextChange();
}

function enabOrDisbTxtFld(chkObj, txtFldID, lblFldID) {

	let txtFldObj = document.getElementById(txtFldID);
	let lblFldObj = document.getElementById(lblFldID);
	if (chkObj != null && lblFldObj != null && txtFldObj != null) {
		if (chkObj.checked) {
			lblFldObj.style.color = '#243665';
			txtFldObj.className = 'form-control checkdb';
			txtFldObj.readOnly = false;
			txtFldObj.focus();
		} else if (!(chkObj.checked)) {
			lblFldObj.style.color = 'black';

			txtFldObj.value = '';
			txtFldObj.className = 'form-control';
			txtFldObj.readOnly = true;
		}// end of if else
	}
}// end of enabOrDisbTxtFld

function chkJsnOptionsSingle(chkbox, jsnFld) {

	$(chkbox).closest("tr").find("input:checkbox").each(function () {
		let chkName = $(this).attr("name");

		if (!$(this).is($(chkbox))) {
			this.checked = false
		}
		if (chkName == "chkCDClntOth" && this.checked == false) {
			$("#dfSelfTaxresOth").removeClass("writeModeTextbox").addClass("form-control")
				.attr('readOnly', true).val("");
		}
		if (chkName == "chkCDSpsOth" && this.checked == false) {
			$("#dfSpsfTaxresOth").removeClass("writeModeTextbox").addClass("form-control")
				.attr('readOnly', true).val("");
		}
	});

	let advAppTypeOpt = $("#" + jsnFld + "");
	advAppTypeOpt.val("");
	let jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	let chkd = chkbox.checked;
	let val = $(chkbox).attr("data");

	let newobj = jsonvalues;
	newobj[val] = (chkd == true ? "Y" : "N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}

var selfSpouseDets = {};
function getFnaSelfSpouseDets(fnaId) {

	$.ajax({
		url: "FnaSelfSpouse/getDataById/" + fnaId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			selfSpouseDets = data;
			//alert(data.dfSelfUsnotifyflg)
			setUsTaxDets(selfSpouseDets);
			setCRSData(data);
			let fnaId = $("#hdnSessFnaId").val();
			getAllDataByFnaId(fnaId);
		},
		error: function (xhr, textStatus, errorThrown) {
			// alert("Error");
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
	return selfSpouseDets;
}

//Set and get US tax declaration
//Radio button 
var arrUsTaxRadioData = ["dfSelfUsnotifyflg", "dfSelfUspersonflg",
	"dfSpsUsnotifyflg", "dfSpsUspersonflg",
	"dfSelfTaxres", "dfSpsTaxres"];

//Textbox 
var arrUsTaxTextData = ["dfSelfUstaxno", "dfSpsUstaxno", "dfSelfTaxresOth"];
function getUsTaxDets(usTaxDets) {
	for (let usTaxRadIdx = 0; usTaxRadIdx < arrUsTaxRadioData.length; usTaxRadIdx++) {
		usTaxDets[arrUsTaxRadioData[usTaxRadIdx]] = $('input[name=' + arrUsTaxRadioData[usTaxRadIdx] + ']:checked').val();
	}
	for (let usTaxTxtIdx = 0; usTaxTxtIdx < arrUsTaxTextData.length; usTaxTxtIdx++) {
		usTaxDets[arrUsTaxTextData[usTaxTxtIdx]] = document.getElementsByName(arrUsTaxTextData[usTaxTxtIdx])[0].value;
	}
}

var ListId = "", radbtnName = ""; var value = "";
function setUsTaxDets(selfSpouseDets) {

	//get self and spouse nric values

	if (!isEmpty(selfSpouseDets.dfSelfNric)) {
		$("#hdnSelfNric").val(selfSpouseDets.dfSelfNric)
	} else {
		$("#hdnSelfNric").val('')
	}

	if (!isEmpty(selfSpouseDets.dfSpsNric)) {
		$("#hdnSpsNric").val(selfSpouseDets.dfSpsNric)
	} else {
		$("#hdnSpsNric").val('')
	}

	if (selfSpouseDets.dfSelfUsnotifyflg == "Y") {
		$("#dfSelfUsnotifyflg1").prop("checked", true).val("Y");
		$("#collapseSec1").removeClass("hide").addClass("show")
		$("#dfSelfUspersonflg1").prop("checked", false).val("N");
		$("#collapseSec2").removeClass("show").addClass("hide")
		$("#dfSelfUstaxno").val("");
	} else if (selfSpouseDets.dfSelfUsnotifyflg == "N") {
		$("#dfSelfUsnotifyflg1").prop("checked", false).val("N");
		$("#collapseSec1").removeClass("show").addClass("hide")
	}

	if (selfSpouseDets.dfSelfUspersonflg == "Y") {
		$("#dfSelfUspersonflg1").prop("checked", true).val("Y");
		$("#collapseSec2").removeClass("hide").addClass("show")
		$("#dfSelfUsnotifyflg1").prop("checked", false).val("N");
		$("#collapseSec1").removeClass("show").addClass("hide")
	} else if (selfSpouseDets.dfSelfUspersonflg == "N") {
		$("#dfSelfUspersonflg1").prop("checked", false).val("N");
		$("#collapseSec2").removeClass("show").addClass("hide")
	}

	if (selfSpouseDets.dfSpsUsnotifyflg == "Y") {
		$("#dfSpsUsnotifyflg1").prop("checked", true).val("Y");
		$("#collapseSec3").removeClass("hide").addClass("show")
		$("#dfSpsUspersonflg1").prop("checked", false).val("N");
		$("#collapseSec4").removeClass("show").addClass("hide")
		$("#dfSpsUstaxno").val("");
	} else if (selfSpouseDets.dfSpsUsnotifyflg == "N") {
		$("#dfSpsUsnotifyflg1").prop("checked", false).val("N");
		$("#collapseSec3").removeClass("show").addClass("hide")
	}

	if (selfSpouseDets.dfSpsUspersonflg == "Y") {
		$("#dfSpsUspersonflg1").prop("checked", true).val("Y");
		$("#collapseSec4").removeClass("hide").addClass("show")
		$("#dfSpsUsnotifyflg1").prop("checked", false).val("N");
		$("#collapseSec3").removeClass("show").addClass("hide")
	} else if (selfSpouseDets.dfSpsUspersonflg == "N") {
		$("#dfSpsUspersonflg1").prop("checked", false).val("N");
		$("#collapseSec4").removeClass("show").addClass("hide")
	}

	if (selfSpouseDets.dfSelfUstaxno) {
		$("#dfSelfUstaxno").val(selfSpouseDets.dfSelfUstaxno)
	}

	if (selfSpouseDets.dfSpsUstaxno) {
		$("#dfSpsUstaxno").val(selfSpouseDets.dfSpsUstaxno)
	}

	//CRS Section data for self and spouse

	//set values for Self CRI Section
	if (selfSpouseDets.dfSelfTaxres && selfSpouseDets.dfSelfTaxres.charAt(0) == 'C') {
		ListId = "ulLiStyle-01selfList";
		radbtnName = "dfSelfTaxres";
		let x = selfSpouseDets.dfSelfTaxres;
		setTimeout(function () {
			$("#ulLiStyle-01selfList").children().find(".form-check-input").each(function () {
				let thisFldRadBtnVal = $(this).val();
				//alert(thisFldRadBtnVal)
				if ((x == thisFldRadBtnVal)) {
					$(this).trigger("click");
				}

			})
		}, 1000);
	}

	//set values for Sps CRI Section
	if (selfSpouseDets.dfSpsTaxres && selfSpouseDets.dfSpsTaxres.charAt(0) == 'S') {
		ListId = "ulLiStyle-01spsList";
		radbtnName = "dfSpsTaxres";
		let y = selfSpouseDets.dfSpsTaxres;

		setTimeout(function () {
			$("#ulLiStyle-01spsList").children().find(".form-check-input").each(function () {
				let thisFldRadBtnVal = $(this).val();
				//alert(thisFldRadBtnVal)
				if ((y == thisFldRadBtnVal)) {
					$(this).trigger("click");
				}

			})
		}, 1000);
	}

	/*for(var i=0;i<arrUsTaxRadioData.length-2;i++){
		var collId=i+1;
		if(selfSpouseDets[arrUsTaxRadioData[i]] == "Y"){
			$("#"+arrUsTaxRadioData[i]+"1").prop('checked',true);
			$("#collapseSec"+collId).removeClass("hide").addClass("show");
    	
		}
		
	}
	for(var i=0;i<arrUsTaxTextData.length-1;i++){
		if(selfSpouseDets[arrUsTaxTextData[i]] != undefined){
			document.getElementsByName(arrUsTaxTextData[i])[0].value=selfSpouseDets[arrUsTaxTextData[i]];
		}
	}*/
}

function setClrTaxData(selfSpouseDets) {
	for (let sUsTaxRadIdx = 4; sUsTaxRadIdx < arrUsTaxRadioData.length; sUsTaxRadIdx++) {
		let value = selfSpouseDets[arrUsTaxRadioData[sUsTaxRadIdx]];

		if (value == "STU") {
			$("#clrStu" + arrUsTaxRadioData[sUsTaxRadIdx]).prop('checked', true);
		}
		else if (value == "CUL") {
			$("#clrCul" + arrUsTaxRadioData[sUsTaxRadIdx]).prop('checked', true);
		}
		else if (value == "HWD") {
			$("#clrHwd" + arrUsTaxRadioData[sUsTaxRadIdx]).prop('checked', true);
		}
		else if (value == "OTH") {
			$("#clrOth" + arrUsTaxRadioData[sUsTaxRadIdx]).prop('checked', true);
		}
	}
	//oth text area
	for (let sUsTaxTxtIdx = 2; sUsTaxTxtIdx < arrUsTaxTextData.length; sUsTaxTxtIdx++) {
		$("#" + arrUsTaxTextData[sUsTaxTxtIdx]).val(selfSpouseDets[arrUsTaxTextData[sUsTaxTxtIdx]]);
		if (selfSpouseDets[arrUsTaxTextData[sUsTaxTxtIdx]] != "") {
			enableTextArea('/' + arrUsTaxTextData[sUsTaxTxtIdx] + '/');
		}
	}
}

function setCRSData(data) {
	if (data.dfSelfName != null) {
		addTab("SELF", data.dfSelfName, "selfList", "dfSelfTaxres", "dfSelfTaxresOth", "C");
	}
	let spsTab = '';

	if (data.dfSpsName != null) {
		spsTab = data.dfSpsName;
		addTab("SPS", data.dfSpsName, "spsList", "dfSpsTaxres", "dfSpsTaxresOth", "S");
		arrUsTaxTextData.push("dfSpsTaxresOth");
	}
	setClrTaxData(selfSpouseDets);

	//check spsData is Empty or Not

	if (isEmpty(spsTab)) {
		$("#dfSpsUspersonflg1").prop("checked", false).val("N");//Us Person flag spouse
		$("#dfSpsUsnotifyflg1").prop("checked", false).val("N");//not US Person flag spouse
		$("#dfSpsUspersonflg1").prop("disabled", true)//disable self US flag
		$("#dfSpsUsnotifyflg1").prop("disabled", true)//disable sos not US flag
		$("#collapseSec3").removeClass("show").addClass("hide")
		$("#collapseSec4").removeClass("show").addClass("hide")
		$("#dfSpsUstaxno").val('')
	} else if (!isEmpty(spsTab)) {
		$("#dfSpsUspersonflg1").removeAttr("disabled")//Enable self US flag
		$("#dfSpsUsnotifyflg1").removeAttr("disabled")//Enable Sps Not US Flag
	}
}

//Get all data by Fna Id
function getAllDataByFnaId(fnaId) {

	$.ajax({
		url: "FnaFatcaTax/getAllDataById/" + fnaId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			//alert(data)
			for (let taxSelfIdx = 0; taxSelfIdx < data.length; taxSelfIdx++) {
				setTaxSelfOrSpouse(data[taxSelfIdx]);
			}
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

var s = 1, ss = 1;
function setTaxSelfOrSpouse(data) {

	if (data.taxFor == "SELF") {
		setTaxResiDets(data, s, "selfList");
		s++;
	}
	if (data.taxFor == "SPS") {
		setTaxResiDets(data, ss, "spsList");
		ss++;
	}
}

var arrTaxData = ["taxId", "taxCountry", "taxRefNo", "reasonToNoTax", "reasonbDetails", "taxFor"];
var taxDets = {};

function validateTaxDets() {
	let refNo = document.getElementsByName("taxRefNo")[0].value;
	let cntry = document.getElementsByName("taxCountry")[0].value;
	let reason = document.getElementsByName("reasonToNoTax")[0].value;
	let reasonDets = document.getElementsByName("reasonbDetails")[0].value;

	let Fld = "Error";

	if (isEmptyFld(cntry)) {
		$("#cntry" + Fld).removeClass("hide").addClass("show");
		$("#country").addClass('err-fld');
		return;
	} else {
		$("#cntry" + Fld).removeClass("show").addClass("hide");
		$("#country").removeClass('err-fld');
	}

	if (isEmptyFld(refNo)) {
		$("#taxRef" + Fld).removeClass("hide").addClass("show");
		$("#taxRefNo").addClass('err-fld');
		return;
	} else {
		$("#taxRef" + Fld).removeClass("show").addClass("hide");
		$("#taxRefNo").removeClass('err-fld');
	}

	if (isEmptyFld(reason)) {
		$("#reasonToNoTax" + Fld).removeClass("hide").addClass("show");
		$("#reasonToNoTax").addClass('err-fld');
		return;
	} else {
		$("#reasonToNoTax" + Fld).removeClass("show").addClass("hide");
		$("#reasonToNoTax").removeClass('err-fld');
	}

	if (!isEmpty(reason) && reason == "Reason B" && isEmpty(reasonDets)) {
		$("#reasonB" + Fld).removeClass("hide").addClass("show");
		$("#reasonbDetails").addClass('err-fld');
		return;
	} else {
		$("#reasonB" + Fld).removeClass("show").addClass("hide");
		$("#reasonbDetails").removeClass('err-fld');
	}
	return true;
}

function saveTaxDets() {
	// if(validateTaxDets()){
	for (let arrTaxIdx = 0; arrTaxIdx < arrTaxData.length; arrTaxIdx++) {
		taxDets[arrTaxData[arrTaxIdx]] = document.getElementsByName(arrTaxData[arrTaxIdx])[0].value;
	}
	taxDets.fnaDetails = selfSpouseDets.fnaDetails;

	//alert(JSON.stringify(taxDets));
	$.ajax({
		url: "FnaFatcaTax/saveData",
		type: "POST",
		data: JSON.stringify(taxDets),
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			$('#addModalFormDetls').modal('hide');
			let sessTaxId = $("#sessTaxId").val();
			if (sessTaxId != "") {
				$("#" + data.taxId).remove();
			}

			removeErrFld($("#span_dfSelfUstaxno"));
			$("#spanerrspan_dfSelfUstaxno").remove();

			setTaxSelfOrSpouse(data);
			clrTaxResData(data.taxFor);

			if (page_wise_err) {
				page_wise_err = false;
				$("#btntoolbarsign").trigger("click");
			}
		},
		error: function (data) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
	// }
}

//Clear Tax residency modal 
function clrTaxResData() {
	for (let clrTaxIdx = 0; clrTaxIdx < arrTaxData.length - 1; clrTaxIdx++) {
		document.getElementsByName(arrTaxData[clrTaxIdx])[0].value = "";
	}
}

//Delete tax residency details
function dltTaxData(obj) {

	let isSigned = $("#hTxtFldSignedOrNot").val();
	
	if(isSigned == "Y") {
		enableReSignIfRequiredforEdtDeltAction();
	} else {

		let taxId = $(obj).attr("id");

		let curntList = $(obj).parents(".list-group").attr("id");

		closeElement();

		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-success',
				cancelButton: 'btn btn-danger mr-3'
			},
			buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
			title: 'Are you sure?',
			text: "You will not be able to recover this TaxTesidency Details ?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			reverseButtons: true
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: "FnaFatcaTax/deleteData/" + taxId,
					type: "DELETE",
					dataType: "json",
					contentType: "application/json",
					// async:false,

					success: function (response) {
						if (curntList == "selfList") {

							let selfTaxList = $("#" + curntList).children().length;

							if (selfTaxList > 1) {
								$("#" + taxId).remove();
								hideTaxImgSelf(curntList);
							}

							if (selfTaxList == 1) {
								$("#" + taxId).remove();
								showTaxImgSelf(curntList);
							}
						}

						if (curntList == "spsList") {

							let spsTaxList = $("#" + curntList).children().length;

							if (spsTaxList > 1) {
								$("#" + taxId).remove();
								hideTaxImgSps(curntList);
							}
							if (spsTaxList == 1) {
								$("#" + taxId).remove();
								showTaxImgSps(curntList);
							}
						}

						swalWithBootstrapButtons.fire("Deleted!", "Your Tax Residential Details has been deleted.", "success");
						chkNextChange();
						//checkdb onchange function for delete operation
						// enableReSignIfRequiredforEdtDeltAction();
					},
					error: function (xhr, textStatus, errorThrown) {
						Swal.fire({
							icon: 'error',
							text: 'Please Try again Later or else Contact your System Administrator',
						});
					}
				});
			} else if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.cancel
			) {
				swalWithBootstrapButtons.fire(
					"Cancelled", "Your Tax Residential Details is safe :)", "error"
				)
			}
		})
	}
}

function setTaxResiDets(data, taxNo, listId) {

	//country field empty validation 
	if (isEmpty(data.taxCountry)) {
		data.taxCountry = "NIL";
	} else {
		data.taxCountry = data.taxCountry;
	}

	//TIN Number empty validation 
	if (isEmpty(data.taxRefNo)) {
		data.taxRefNo = "NIL";
	} else {
		data.taxRefNo = data.taxRefNo;
	}

	//Reason field empty validation 
	if (isEmpty(data.reasonToNoTax)) {
		data.reasonToNoTax = "NIL";
	} else {
		data.reasonToNoTax = data.reasonToNoTax;
	}

	//Remarks field empty validation 
	if (isEmpty(data.reasonbDetails)) {
		data.reasonbDetails = "NIL";
	} else {
		data.reasonbDetails = data.reasonbDetails;
	}

	let listGroup = '<a id=' + data.taxId + ' href="#" class="list-group-item list-group-item-action flex-column align-items-start ">'
		+ '<div class="d-flex w-100 justify-content-between">'
		+ '<h6 class="mb-1 text-primaryy font-sz-level6 bold">Tax#' + taxNo + '</h6>'
		+ '<span class="font-sz-level7 bold text-custom-color-gp"><i class="fa fa-pencil-square-o mr-2"" aria-hidden="true" style="color:blue;" title="Edit Tax Details" onclick="editTaxDets(/' + data.taxId + '/)"></i>&nbsp;&nbsp;<i class="fa fa-trash checkdb" aria-hidden="true" style="color: red;" title="Delete Tax Details"  id="' + data.taxId + '" onclick="dltTaxData(this)"></i></span>'
		+ '</div>'
		+ '<div class="row">'
		+ '<div class="col-md-6">'
		+ '<span class="font-sz-level7 bold text-custom-color-gp"><strong>Country&nbsp; :&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">' + data.taxCountry + '</span>'
		+ '</div>'
		+ '<div class="col-md-6">'
		+ '<span class="font-sz-level7 bold text-primary">TIN Number&nbsp;:&nbsp;' + data.taxRefNo + '</span>'
		+ '</div>'
		+ '</div>'

		+ '<div class="row">'
		+ '<div class="col-md-4">'
		+ '<span class="font-sz-level7 bold text-custom-color-gp"><strong>Reason&nbsp;:&nbsp;</strong></span><span class="font-sz-level7 font-normal">' + data.reasonToNoTax + '</span>'
		+ '</div>'
		+ '<div class="col-md-8" style="height: 10vh;overflow-y: auto;">'
		+ '<span class="font-sz-level7 bold text-custom-color-gp"><strong>Remarks&nbsp;:&nbsp;</strong></span><span class="font-sz-level7 font-normal">' + data.reasonbDetails + '</span>'
		+ '</div>'
		+ '</div>'

		+ '</a>';

	//hide self sps tax image while datalist > 0
	if (listId == "selfList") {
		hideTaxImgSelf(listId);
	}
	if (listId == "spsList") {
		hideTaxImgSps(listId);
	}
	//end

	$("#" + listId).append(listGroup);
}

function hideTaxImgSps(listId) {
	$("#Taxcontent").find("#tab2").find("#NoTaxDetlsRow" + listId).removeClass("show").addClass("hide");
}

function hideTaxImgSelf(listId) {
	$("#Taxcontent").find("#tab1").find("#NoTaxDetlsRow" + listId).removeClass("show").addClass("hide");
}


function showTaxImgSps(listId) {
	$("#Taxcontent").find("#tab2").find("#NoTaxDetlsRow" + listId).removeClass("hide").addClass("show");
}

function showTaxImgSelf(listId) {
	$("#Taxcontent").find("#tab1").find("#NoTaxDetlsRow" + listId).removeClass("hide").addClass("show");
}

function editTaxDets(taxId) {
	//checkdb onchange function for edit operation
	enableReSignIfRequiredforEdtDeltAction();
	$.ajax({
		url: "FnaFatcaTax/getDataById/" + taxId,
		type: "GET",
		dataType: "json",// async:false,
		contentType: "application/json",
		success: function (data) {
			setTaxResModalData(data);
		},
		error: function (xhr, textStatus, errorThrown) {
			// alert("Error");
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
}

function setTaxResModalData(data) {
	$('#addModalFormDetls').modal('show');
	for (let sArrTaxIdx = 0; sArrTaxIdx < arrTaxData.length; sArrTaxIdx++) {
		if (!isEmpty(data[arrTaxData[sArrTaxIdx]])) {
			document.getElementsByName(arrTaxData[sArrTaxIdx])[0].value = data[arrTaxData[sArrTaxIdx]];
		} else {
			document.getElementsByName(arrTaxData[sArrTaxIdx])[0].value = ""
		}
	}
}

function addModal(clientType) {

	//check checkdb onchange function for AddingTax Details
	enableReSignIfRequiredforEdtDeltAction();

	// set default country type singapore to self and spouse
	$("#country").val("Singapore");
	$("#reasonToNoTax").val("");
	$("#reasonbDetails").val("");
	$("#reasonbDetails").prop("disabled", true);

	//set Slef and Nric here
	if (clientType == '/SELF/') {
		$('#taxRefNo').val($("#hdnSelfNric").val())
		if (isEmpty($("#taxRefNo").val())) {
			$("#reasonToNoTax").prop("disabled", false);
		} else {
			$("#reasonToNoTax").prop("disabled", true);
		}
	}
	//set Spouse Nric here 
	else if (clientType == '/SPS/') {
		$('#taxRefNo').val($("#hdnSpsNric").val())
		if (isEmpty($("#taxRefNo").val())) {
			$("#reasonToNoTax").prop("disabled", false);
		} else {
			$("#reasonToNoTax").prop("disabled", true);
		}
	}

	let taxForStr = String(clientType)
	document.getElementsByName("taxFor")[0].value = taxForStr.substring(1, taxForStr.length - 1);
}

$("#taxRefNo").bind("change", function () {
	let taxVal = $(this).val();
	// alert(taxVal);
	if (isEmpty(taxVal)) {
		$("#reasonToNoTax").removeAttr("disabled");
		$("#reasonbDetails").prop("readOnly", false);
	} else {
		$("#reasonToNoTax").prop("disabled", true);
		$("#reasonToNoTax").val("");

		$("#reasonbDetails").prop("readOnly", true);
		$("#reasonbDetails").val("");
	}
})

function addTab(clientType, tabName, listId, optName, optOth, clntPrefix) {

	let nextTab = $('#selfSpouseTabs li').length + 1;

	// create the tab
	$('<li class="nav-item"> <a class="nav-link" id="tab' + nextTab + '" data-toggle="tab" href="#tab' + nextTab + '"  onclick="openThisTabContArea(this)" role="tab" aria-controls="client2" aria-selected="false">' + tabName + '</a> </li>').appendTo('#selfSpouseTabs');

	let tabShow = "fade";
	if (clientType == "SELF") {
		tabShow = "active show";
	}

	// create the tab content
	//$('<div class="tab-pane" id="tab'+nextTab+'">tab' +nextTab+' content</div>').appendTo('.tab-content');
	let taxDetls = '<div class="tab-pane ' + tabShow + '" id="tab' + nextTab + '" role="tabpanel" aria-labelledby="client2-tab">'
		+ '<input type="hidden" id="hdnClientType" name="taxFor" value=' + clientType + '>'
		+ '<input type="hidden" id="hdnClientType" name="tabName" value=' + tabName + '>'
		+ '<div class="card mb-1  p-1 m-1 " id="">'
		+ '<div class="card-body" style="border:1px solid #ddd;" id="">'
		+ '<div class="row">'
		+ '<div class="col-md-7">'
		+ '<div class="card">'
		+ '<div class="card-header" id="cardHeaderStyle">'
		+ '<div class="row">'
		+ '<div class="col-md-8">'
		+ '<div class="pt-1">Tax Residency Details</div>'
		+ '</div>'
		+ '<div class="col-md-4">'
		+ '<button class="btn btn-sm btn-bs3-prime addtaxdetsbtn" style="background: green; border: green; float:right;" data-toggle="modal" data-target="#addModalFormDetls" onclick="addModal(/' + clientType + '/);"><i class="fa fa-plus" aria-hidden="true"></i> Add Tax Resi. Detls</button>'
		+ '</div>'
		+ '</div>'
		+ '</div>'

		//+'<div class="card-body" style="min-height: 70vh;overflow-y: scroll;overflow-x: hidden;">'

		+ '<div class="card-body" style="height: 80vh;overflow-y: scroll;overflow-x: hidden;">'


		+ '<div class="row NoTaxDetlsRow' + listId + '" id="NoTaxDetlsRow' + listId + '">'
		+ '<div class="col-2"></div>'
		+ '<div class="col-8">'
		+ '<img src="vendor/psgisubmit/img/tax.gif" class="rounded mx-auto d-block img-fluid  prod-img" id="TaxImg" style="width: 100%;height: 45vh;border:1px solid grey;" >'
		+ '<p class="center noData mt-2" id="NoTaxInfo"><img src="vendor/psgisubmit/img/warning.png" class="mr-3">&nbsp;No Tax Details Available&nbsp;</p>'
		+ '</div>'

		+ '<div class="col-2"></div>'
		+ '</div>'

		+ '<div class="list-group" id=' + listId + '>'

		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '<div class="col-md-5">'
		+ '<div class="card h-100">'
		+ '<div class="card-header" id="cardHeaderStyle">Classification of Tax Residence Information</div>'
		+ '<div class="card-body">'
		+ '<div class="row pl-1  ">'
		+ '<div class="col- "><img src="vendor/psgisubmit/img/infor.png" class="mt-2"></div>'
		+ '<div class="col-11">'
		+ '<div class="col-">'
		+ '<span class="font-sz-level6 lh-20">If the country indicated in your mailing address/contact number is different from the country(ies) which you have disclosed as tax residence(s), please provide your explanation below. Otherwise, please disregard this section and proceed to Declaration. Please select <span class="text-primaryy bold txt-urline ">ONE</span> option only:</span>'
		+ '</div>'
		+ '</div>'
		+ '</div>'

		+ '<ul class="list-group mt-2" id="ulLiStyle-01' + listId + '" class="selfSpsCRISec">'
		+ '<li class="list-group-item">'
		+ '<div class="form-check checkdb">'
		+ '<label class="form-check-label checkdb">'
		+ '<input type="radio" class="form-check-input checkdb" id="clrSTU' + optName + '" name=' + optName + ' onclick="disableTextArea(/' + optOth + '/)"  value="' + clntPrefix + 'STU" maxlength="60">I am a student'
		+ '</label>'
		+ '</div>'

		+ '</li>'
		+ '<li class="list-group-item">'
		+ '<div class="form-check checkdb">'
		+ '<label class="form-check-label checkdb">'
		+ '<input type="radio" class="form-check-input checkdb" id="clrCUL' + optName + '" name=' + optName + ' value="' + clntPrefix + 'CUL" onclick="disableTextArea(/' + optOth + '/)" maxlength="60">I am on cultural/diplomatic purpose'
		+ '</label>'
		+ '</div>'

		+ '</li>'
		+ '<li class="list-group-item">'
		+ '<div class="form-check checkdb">'
		+ '<label class="form-check-label checkdb">'
		+ '<input type="radio" class="form-check-input checkdb" id="clrHWD' + optName + '" name=' + optName + ' value="' + clntPrefix + 'HWD" onclick="disableTextArea(/' + optOth + '/)" maxlength="60">I am a housewife/dependent'
		+ '</label>'
		+ '</div>'

		+ '</li>'
		+ '<li class="list-group-item">'
		+ '<div class="form-check checkdb">'
		+ '<label class="form-check-label checkdb">'
		+ '<input type="radio" class="form-check-input checkdb" id="clrOTH' + optName + '" name=' + optName + ' value="' + clntPrefix + 'OTH" onclick="enableTextArea(/' + optOth + '/);" maxlength="60">Others'
		+ '</label>'
		+ '</div>'
		+ '<textarea class="form-control txtarea-hrlines text-wrap checkdb mt-1 " rows="3" id=' + optOth + ' name=' + optOth + ' maxlength="300" onkeydown="textCounter(this,300);" onkeyup="textCounter(this,300);"disabled></textarea><div class="font-sz-level7 mt-2 text-primaryy char-count"></div>'
		+ '</li>'

		+ '</ul>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>';

	$(taxDetls).appendTo('#Taxcontent');

	$('#selfSpouseTabs a:first').tab('show');
}

function enableTextArea(optOth) {
	let optStr = String(optOth);
	let opt = optStr.substring(1, optStr.length - 1);
	$("#" + opt).prop("disabled", false);
}
function disableTextArea(optOth) {
	let optStr = String(optOth);
	let opt = optStr.substring(1, optStr.length - 1);
	$("#" + opt).prop("disabled", true);
	$("#" + opt).val("");
}
//Set and get tax residency

function updateSelfSpouse(selfSpouse, nextscreen, navigateflg) {
	getUsTaxDets(selfSpouse);
	let hdnFnaDetailsStr = $("#hdnFnaDetails").val();
	let hdnFnaDetails = JSON.parse(hdnFnaDetailsStr);
	selfSpouse.fnaDetails.mgrEmailSentFlg = hdnFnaDetails.mgrEmailSentFlg;
	$.ajax({
		url: "FnaSelfSpouse/updateData",
		type: "PUT",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(selfSpouse),
		// // async:false,
		success: function (data) {
			//alert(data);
			if (navigateflg) {
				window.location.href = nextscreen;
			}
		},
		error: function (data) {
			//alert(data);
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
}
function saveData(obj, nextscreen, navigateflg, infoflag) {
	//alert(JSON.stringify(selfSpouseDets));
	updateSelfSpouse(selfSpouseDets, nextscreen, navigateflg);
}

function enableReasonDets(obj) {

	if (obj.value == "Reason B") {
		$("#noteReasonB").trigger("mouseover");
		$("#noteReasonA").trigger("mouseout");
		$("#noteReasonC").trigger("mouseout");
		//obj.parentNode.parentNode.childNodes[2].childNodes[0].disabled = false;
		$("#reasonbDetails").prop("disabled", false);
	}
	if (obj.value == "Reason A") {
		$("#noteReasonA").trigger("mouseover");
		$("#noteReasonB").trigger("mouseout");
		$("#noteReasonC").trigger("mouseout");
		$("#reasonbDetails").prop("disabled", true);
		$("#reasonbDetails").val("");
	}

	if (obj.value == "Reason C") {
		$("#noteReasonC").trigger("mouseover");
		$("#noteReasonA").trigger("mouseout");
		$("#noteReasonB").trigger("mouseout");
		$("#reasonbDetails").prop("disabled", true);
		$("#reasonbDetails").val("");
	}
}

//click to show current tabContent only
function openThisTabContArea(obj) {
	let tabid = $(obj).attr("id");

	let tabcontAreaId = [];
	$('#Taxcontent div').each(function () {
		let tabContId = $(this).attr("id");
		if (tabid == tabContId) {
			$(this).addClass("active show");
			$('#Taxcontent div').not(this).removeClass("active show");
		}
	});
}

$("#dfSelfUstaxno, #dfSpsUstaxno").on("change", function() {
	if (page_wise_err) {
		page_wise_err = false;
		saveData();
		setTimeout(function() {
			$("#btntoolbarsign").trigger("click");
		}, 1000);
	}
});
