/*var fnaDets=$("#hdnFnaDetails").val();
alert(JSON.stringify(fnaDets));
setFnaData(fnaDets);*/
var fnaDets = {};
function getFnaDets(fnaId) {
	$.ajax({
		url: "fnadetails/getDataById/" + fnaId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			fnaDets = data;
			setFnaData(data);
		},
		error: function (data) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
	return fnaDets;
}

//Update FnaDetails
function updateFnaDets(obj, nextscreen, navigateflg, infoflag) {
	let fnaDetailsStr = $("#hdnFnaDetails").val();
	let fnaDetails = JSON.parse(fnaDetailsStr);
	getFnaData(fnaDetails);
	//alert(JSON.stringify(fnaDets));
	$.ajax({
		url: "fnadetails/update",
		type: "PUT",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(fnaDetails),
		// async:false,
		success: function (data) {
			//alert("Success");

			if (navigateflg && nextscreen != "FINISHED") {
				window.location.href = nextscreen;
			} else {
				if (nextscreen == "FINISHED" && infoflag) {
					var signornot = $("#hTxtFldSignedOrNot").val();
					var mgrkycSentStatus = $("#kycSentStatus").val();
					var mgrapproveStatus = $("#mgrApproveStatus").val();

					if (signornot == "Y") {
						if (fnaDetails && fnaDetails.mgrEmailSentFlg == "Y") {
							Swal.fire({
								text: "This FNA Form is already signed and sent for approval process",
								icon: 'info',
								showCancelButton: false,
							}).then((result) => {
								if (result.isConfirmed) {
									Swal.close()
								}
							})
						} else {
							closeScanToSign();
						}
					} else {
						Swal.fire({
							text: "Do you want to finish and proceed to key information verification?",
							icon: 'question',
							showCancelButton: true,
							//  confirmButtonColor: '#3085d6',
							// cancelButtonColor: '#d33',
							confirmButtonText: 'Yes, Verify!',
							showLoaderOnConfirm: true,
						}).then((result) => {
							if (result.isConfirmed) {
								$("#btntoolbarsign").trigger("click");
							}
						})
					}
				}
			}
		},
		error: function (data) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function emptyOrUndefined(advDecOpt) {
	if (isEmpty(advDecOpt) || advDecOpt == "undefined") {
		return true;
	}
	return false;
}

//For approval page
function getApprovalFnaDets(fnaId) {
	$.ajax({
		url: "fnadetails/getDataById/" + fnaId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			fnaDets = data;
			setApprovalFnaData(data);
		},
		error: function (data) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});

	return fnaDets;
}

//Update FnaDetails
function updateApprovalFnaDets() {
	getApprovalFnaData(fnaDets);
	//alert(JSON.stringify(fnaDets));
	$.ajax({
		url: "fnadetails/update",
		type: "PUT",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(fnaDets),
		// async:false,
		success: function (data) {
			//alert("Success");
		},
		error: function (data) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}
