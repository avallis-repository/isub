
$(document).ready(function () {
  console.log("ready Advice & Recomm Page!");
  $('[data-toggle="popover1"]').popover({

    placement: 'top',
    html: true,
    content: function () {
      return $('#popover-contentPg10CardNotes1').html();
    }
  });

  $('[data-toggle="popover2"]').popover({

    placement: 'top',
    html: true,
    content: function () {
      return $('#popover-contentPg10CardNotes2').html();
    }
  });

  $('[data-toggle="popover3"]').popover({
    html: true,
    content: function () {
      return $('#popover-contentPg10CardNotes3').html();
    }
  });

  $('[data-toggle="popover4"]').popover({
    html: true,
    content: function () {
      return $('#popover-contentPg10CardNotes4').html();
    }
  });
  let fnaId = $("#hdnSessFnaId").val();
  getFnaDets(fnaId);
  window.addEventListener("beforeunload", function (e) {
    // saveData();
  });
});

//Set and Get Advice & Basis Recommendation
var arrAdvBasData = ["advrecReason", "advrecReason1", "advrecReason2", "advrecReason3"];
function getFnaData(fnaDets) {
  for (let advIdx = 0; advIdx < arrAdvBasData.length; advIdx++) {
    fnaDets[arrAdvBasData[advIdx]] = $("#" + arrAdvBasData[advIdx]).val();
  }
}
function setFnaData(fnaDets) {
  for (let advBasIdx = 0; advBasIdx < arrAdvBasData.length; advBasIdx++) {
    $("#" + arrAdvBasData[advBasIdx]).val(fnaDets[arrAdvBasData[advBasIdx]]);
  }
}

//SaveData
function saveData(obj, nextscreen, navigateflg, infoflag) {
  updateFnaDets(obj, nextscreen, navigateflg, infoflag);
}

$(".viewRemarkRec").on("click", function () {
  $("#advrecReason_pop").val($("#advrecReason").val());
  $("#advrecReason1_pop").val($("#advrecReason1").val());
  $("#advrecReason2_pop").val($("#advrecReason2").val());
  $("#advrecReason3_pop").val($("#advrecReason3").val());
});

$(".viewAdvRec").on("click", function () {
  $("#advrecReason").val($("#advrecReason_pop").val());
  $("#advrecReason1").val($("#advrecReason1_pop").val());
  $("#advrecReason2").val($("#advrecReason2_pop").val());
  $("#advrecReason3").val($("#advrecReason3_pop").val());
});

