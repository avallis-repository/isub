//Common Script File
$('#cover-spin').hide();

function isEmptyObj(obj) {
	return Object.keys(obj).length === 0;
}

function isEmptyFld(value) {
	if (value == " " || value == null || value.length == 0 || value == undefined)
		return true;
	else
		return false;
}

function emptyOrUndefined(advDecOpt) {
	if (isEmpty(advDecOpt) || advDecOpt == "undefined") {
		return true;
	}
	return false;
}

function isNumber(event, errMsgId) {
	event = (event) ? event : window.event;
	let charCode = (event.which) ? event.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		//$("#depName").addClass("errorclz");
		// $("#numErrMsg").show();
		$("#" + errMsgId).show();
		return false;
	}
	$("#" + errMsgId).hide();
	return true;
}

function isJsonObjEmpty(obj) {
	if (Object.keys(obj).length === 0 && obj.constructor === Object)
		return true;
	else
		return false;
}
//income,sumAssured and Premium Field Number validation it allows Numbers and Decimal Points
$('.numberFldClass').keyup(function () {
	let val = $(this).val();
	if (isNaN(val)) {
		val = val.replace(/[^0-9\.]/g, '');
		if (val.split('.').length > 2)
			val = val.replace(/\.+$/, "");
	}
	$(this).val(val);
});
/*function isEmptyFld(obj,Elem){
	var value = $(this).val();
	if(value == " " || value == null || value.length == 0 || value == undefined)
		return true;
	else
		return false;
} */

function isEmpty(str) {
	if (str == null || str.length == 0 || str == undefined || str == 0)
		return true;
	else
		return false;
}

$('#txtFldPremium').keyup(function () {
	let val = $(this).val();
	if (isNaN(val)) {
		val = val.replace(/[^0-9\.]/g, '');
		if (val.split('.').length > 2)
			val = val.replace(/\.+$/, "");
	}
	$(this).val(val);
});

$('#txtFldSumAssured').keyup(function () {
	let val = $(this).val();
	if (isNaN(val)) {
		val = val.replace(/[^0-9\.]/g, '');
		if (val.split('.').length > 2)
			val = val.replace(/\.+$/, "");
	}
	$(this).val(val);
});

//Poovathi Add Common Email Id Validation on 10-11-2020
function validateEmail(obj, Elem) {
	let email = $(obj).val();
	if (isEmptyFld(email)) {
		$('#' + Elem + "Error").removeClass('hide').addClass('show');
		$('#' + Elem).addClass('err-fld');
		return;
	} else {
		let regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!regex.test(email)) {
			$('#' + Elem + "Error").removeClass('hide').addClass('show');
			$('#' + Elem + "Error").html("Invalid Email Id Format");
			$('#' + Elem).addClass('err-fld');
			$('#' + Elem).focus();
			return false;
		} else {
			$('#' + Elem + "Error").removeClass('show').addClass('hide');
			$('#' + Elem).removeClass('err-fld');
			return true;
		}
	}
}
//end of Email validation

function makeid(length) {
	let result = '';
	let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	let charactersLength = characters.length;
	for (let idx = 0; idx < length; idx++) {
		result += characters.charAt(Math.floor(Math.random() * charactersLength));
	}
	return ("PR_" + result).toUpperCase();
}

function formatDate(date) {
	let d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [day, month, year].join('-');
}

function formatDbDate(date) {
	let d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [year, month, day].join('-');
}

$.fn.serializeObject = function () {
	let o = {};
	let a = this.serializeArray();
	$.each(a, function () {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

//Bootstrap Date Picker
/*$('#simple-date1 .input-group.date').datepicker({
dateFormat: 'dd/mm/yyyy',
todayBtn: 'linked',
language: "it",
todayHighlight: true,
autoclose: true, 
});*/

//old KYC Reuse Common functions below

function fnaAssgnFlg(obj, id) {
	if (id == "chk") {
		if (obj.checked) {
			obj.value = "Y";
		} else if (!obj.checked) {
			obj.value = "";
		}

	} else {
		if (obj.checked) {
			document.getElementById(id).value = obj.value;
		}
	}
}

function PasteOnlyNum(obj, pre) {
	setTimeout(function () {
		//		console.log(obj.value);

		if (pre == 0) {
			let regNoPre = /^\d+$/;
			if (!regNoPre.test(obj.value.trim())) {
				obj.value = "";
				// alert("Numbers only allowed.")
			}
		} else {
			let reg = new RegExp("^[0-9]+(\.[0-9]{1," + pre + "})?$");
			if (!reg.test(obj.value.trim())) {
				obj.value = "";
				// alert("Numbers only allowed.")
			}

		}

		obj.value = obj.value.trim();

	}, 400);
}

function textCounter(field, maxlimit) {

	let len = field.value.length;
	if (len > maxlimit) {
		field.value = field.value.substring(0, maxlimit);
		donothing();
		return false;
	}

}//end textCounter

function donothing() {
	document.returnValue = true;
}//end donothing

function chkSelfSpsOthrValid(chkbox, Ele) {

	let chkd = chkbox.checked;
	if (chkd) {
		$("#" + Ele + "").removeClass("readOnlyText").addClass("writeModeTextbox")
			.attr('readOnly', false).prop('disabled', false).attr('disabled', false);
	} else {
		$("#" + Ele + "").removeClass("writeModeTextbox").addClass("readOnlyText")
			.attr('readOnly', true).val("");
	}
}

//Send email
function sendEmail() {
	// $("#ScanToSignModal").modal("hide");
	//var email="lalitha@tasproit";

	//document.getElementById("mailLoaderDiv").style.display = "block";
	let email = $("#mgrMailId").val();
	$.ajax({
		url: 'FnaSignature/sentMail/' + email,
		type: 'GET',
		dataType: "text",
		async: false,
		beforeSend: function () {
			$('#cover-spin').show();
		},
		success: function (data) {
			let result = JSON.parse(data);
			if (result) {
				if (result.status === 'failed') {
					Swal.fire({
						icon: 'error',
						text: result.reason
					});
				} else {
					$("#hdnFnaDetails").val(data);

					$('#cover-spin').hide();
					$('.circle-loader').addClass('load-complete');
					$('.checkmark').toggle();
					//$("#modalMsgLbl").html("Mail was sent !!!");
					Swal.fire({
						icon: 'success',
						title: 'Mail was sent !!!',
						showConfirmButton: false,
						timer: 1500
					});
					$("#mailModal").modal("hide");
					setTimeout(function () {
						location.reload();
					}, 1500);
					// document.cookie = "kycSentStatus=sent";
				}
			}
		},
		error: function (e) {
			// console.log("ERROR : ", e);
			$('#cover-spin').hide();
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator'
			});
		}
	});
}

function enableMgr() {
	document.getElementById("signManager").style.display = "block";
	document.getElementById("signManagerQr").style.display = "block";
}

function sendMgrEmail() {
	let email = document.getElementById("advMailId").value;

	document.getElementById("mailLoaderAdv").style.display = "block";
	let supreFlg = $('input[name="suprevMgrFlg"]:checked').val();
	let reason = $("#suprevFollowReason").val();
	$.ajax({
		url: 'FnaSignature/sentAdvMail',
		type: 'POST',
		async: false,
		data: {
			supreFlg: supreFlg,
			reason: reason,
			email: email
		},
		success: function (data) {

			$('.circle-loader').addClass('load-complete');
			$('.checkmark').toggle();
			$("#modalMsgLblAdv").html("Mail was sent !!!");
		},
		error: function (e) {
			// console.log("ERROR : ", e);
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
}

function textCounter(field, maxlimit) {
	let len = field.value.length;
	if (len > maxlimit) {
		field.value = field.value.substring(0, maxlimit);
		donothing();
		return false;
	} else if (len <= maxlimit) {
		$(field).next().html(maxlimit - len + " characters are left from " + '<span class="text-success">' + maxlimit + '</span>' + " Characters");
	}

	// trigger note content while keyup text area Field Start
	let thisFldId = $(field).attr("id");
	//enableNoteContent(thisFldId);
	//end of note content display trigger

}//end textCounter

//Menu navigation save
function menuNavSave() {
	window.addEventListener("beforeunload", function (e) {
		saveData();
		return (e || window.event).returnValue = "Your data is saved";
		/* var dependant={};
		 dependant=getDependantData(dependant);
		 
		 if(isEmpty(dependant)){
		   return (e || window.event).returnValue = "Your data may be lost";
		 }*/

	});
}

// All Page Mandotary Field Validation 
var page_wise_err = false;

function allPageValidation() {

	//saveCurrentPageData($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
	let ajaxTime = 0;
	// if (page_wise_err) {
	// 	page_wise_err = false;
	// 	saveData();
	// 	ajaxTime = 1000;
	// }

	setTimeout(function() {
		let fnaId = $("#hdnSessFnaId").val();
		ajaxCall(fnaId);
	}, ajaxTime);
	
}

var FNASSDET_TABLE_DATA = {}, FNA_TABLE_DATA = {}, FATCATAXDET_DATA = [], FATCATAXDETSPS_DATA = [], FNADEPNT_TABLE_DATA = [], ADVRECPRDTPLAN_DATA = [], FNAPROD_RECOM_DATA = [];

function fetchAllPageData() {

	FNA_TABLE_DATA = {}, FNASSDET_TABLE_DATA = {}, FATCATAXDET_DATA = [], FATCATAXDETSPS_DATA = [], FNADEPNT_TABLE_DATA = [], ADVRECPRDTPLAN_DATA = [], FNAPROD_RECOM_DATA = [];

	//var fnaId = $("#hdnSessFnaId").val();


	//var respText = ajaxCall(fnaId);

	FNA_TABLE_DATA = JSON.parse(allDataMap.get("FNA_DETAILS"));
	FNASSDET_TABLE_DATA = JSON.parse(allDataMap.get("FNA_SELF_SPOUSE_DATA"));
	FATCATAXDET_DATA = JSON.parse(allDataMap.get("FATCATAXDET_DATA"));
	FATCATAXDETSPS_DATA = JSON.parse(allDataMap.get("FATCATAXDETSPS_DATA"));
	FNADEPNT_TABLE_DATA = JSON.parse(allDataMap.get("FNADEPNT_TABLE_DATA"));
	FNAPROD_RECOM_DATA = JSON.parse(allDataMap.get("FNAPROD_RECOM_DATA"));

	//kycHome
	if (!validateFnaSelfSpsDets(true)) {
		return;
	}
	//Tax residency
	if (!validateFnaFatcaTaxDets(true)) {
		return;
	}
	if (!validateFnaDepentDets(false))
		return;

	if (!validateFnaWellnesReview(true))
		return;
	//Product Recommend
	if (!validAdvAndRecmChkboxes(true)) {
		return;
	}

	if (!validateFnaProdRecomDet()) {
		return;
	}

	/*change*/
	/*if (!validateFnaArtuPlantDet(false))
		return;*/

	if (!validateFnaSwrepPlantDet(false))
		return;

	//Product switch plan
	if (!validateReasonforRecomm(true)) {
		return;
	}

	//Client declartion
	if (!validateClntConsent(true)) {
		return;
	}
	//Manager declaration
	/*if (!advDeclrAndSupReview(true)){
		return;
	}*/

	/*toastr.clear($('.toast'));
	toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
	toastr["success"]("All Verifications are done!");	
	
	$("#btnAllPgeNoti").addClass("fa-check").removeClass("fa-remove");
	$("#btnAllPgeNoti").closest("button").removeAttr("disabled");
	$("#btnAllPgeNoti").closest("button").addClass("btn-success").removeClass("btn-secondary");
	
	setTimeout(function(){
	
		$("#ScanToSignModal").modal({
			backdrop: 'static',
			keyboard: false,
			show: true
		  })
	
	},1000);
	
	setTimeout(function(){
	
		startTimer();
	
	},1200);*/
	setPageKeyInfoSucs();
	
	// let signOrNot = $("#hTxtFldSignedOrNot").val();

	// if(signOrNot != "Y") {

		setTimeout(function () {
	
			closeElement();
	
			Swal.fire({
				// title: 'Are you sure?',
				html: "Key Information <strong>verification is completed</strong>. Do you want to <strong>proceed to signature?</strong>",
				icon: 'question',
				allowOutsideClick: false,
				allowEscapeKey: false,
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Yes, Proceed'
			}).then((result) => {
				if (result.isConfirmed) {
					$("#ScanToSignModal").modal({
						backdrop: 'static',
						keyboard: false,
						show: true
					});
	
					// var cfnaId=$("#hdnSessFnaId").val();
					//Cookies.remove(cfnaId, { path: '/' })
					//startTimer()
	
					$("#scanToSignClient1Btn").trigger("click");
	
					setTimeout(function () {
						startTimer();
					}, 1200);
				}
			})
		}, 1000);
	// }
	return true;
}

function addErrorMsgToChat(elemId, message, pagetonav, elemname, secId, isPrepend) {

	if (!isEmptyFld(elemId)) {

		let spanId = "spanerr" + elemId;

		focusToField(pagetonav, elemId);

		makeErrMsg(elemId, "errorclasss", message, pagetonav);

		let elem = '<li class="other" id="' + spanId + '" onclick=focusToField("' + pagetonav + '","' + elemId + '")>' + message + ' </li>'

		//console.log(elemId,message,pagetonav,spanId,elem,elemname,"***")
		if ($("#" + elemId).length) {

			$("#" + elemId).bind("change", function () {
				removeErrFld($(this));
				$("#" + spanId).remove();


				if ($('#ekycValidMessageList li').length == 0) {
					saveData();
					page_wise_err = false;
					setTimeout(function () {
						$("#btntoolbarsign").trigger("click");
					}, 1000);
				}

			});

		} else if ($("." + elemId).length) {

			$("." + elemId).bind("change", function () {
				removeErrFld($(this));
				$("#" + spanId).remove();

				if ($('#ekycValidMessageList li').length == 0) {
					saveData();
					page_wise_err = false;
					setTimeout(function () {
						$("#btntoolbarsign").trigger("click");
					}, 1000);
				}
			});
		}

		if (!isEmptyFld(elemname)) {

			let nodetype = "";
			if (($('input[name="' + elemname + '"]').length)) {
				nodetype = $('input[name="' + elemname + '"]').prop('type').toUpperCase()
			} else {
				nodetype = $('.' + elemname).prop('type').toUpperCase();
			}

			if (nodetype == "RADIO" || nodetype == "CHECKBOX") {
				$(':input[name="' + elemname + '"]').bind("click", function () {
					removeErrFld($(this));
					$("#" + spanId).remove();

					if ($('#ekycValidMessageList li').length == 0) {
						saveData();
						setTimeout(function () {
							$("#btntoolbarsign").trigger("click");
						}, 1000);
					}

				});

				$('.' + elemname).bind("click,", function () {
					removeErrFld($(this));
					$("#" + spanId).remove();

					if ($('#ekycValidMessageList li').length == 0) {
						saveData();
						page_wise_err = false;
						setTimeout(function () {
							$("#btntoolbarsign").trigger("click");
						}, 1000);
					}

				});

			} else {
				$(':input[name="' + elemname + '"]').bind("change", function () {
					removeErrFld($(this));
					$("#" + spanId).remove();

					if ($('#ekycValidMessageList li').length == 0) {
						saveData();
						page_wise_err = false;
						setTimeout(function () {
							$("#btntoolbarsign").trigger("click");
						}, 1000);
					}
				});

				$('.' + elemname).bind("change", function () {
					removeErrFld($(this));
					$("#" + spanId).remove();

					if ($('#ekycValidMessageList li').length == 0) {
						saveData();
						page_wise_err = false;
						setTimeout(function () {
							$("#btntoolbarsign").trigger("click");
						}, 1000);
					}
				});
			}
		}

		if ($("#ekycValidMessageList").find('li#' + spanId).length == 0) {
			$("#ekycValidMessageList").append(elem);

		}
	} else {
		let spanId = "span_" + message;
		let elem;
		if (secId && !isEmptyFld(secId)) {
			spanId = "span_" + elemname;
			makeErrMsg(secId, "errorclasss", message, pagetonav, isPrepend);
			spanId = spanId.replace(/[&\/\\,+()$~%.'":*?<>{}]/g, '_');
			elem = '<li class="other" id="' + spanId + '" onclick=focusToField("' + pagetonav + '","' + elemname + '")>' + message + ' </li>'

			let currentPage = window.location.pathname;
			currentPage = currentPage.substr(currentPage.lastIndexOf("/") + 1);
			if (currentPage == pagetonav) {
				elemname = "";
			}
			focusToField(pagetonav, elemname);
		} else {
			focusToField(pagetonav, elemname);
			makeErrMsg(elemId, "errorclasss", message, pagetonav);
			spanId = spanId.replace(/[&\/\\,+()$~%.'":*?<>{}]/g, '_');
			elem = '<li class="other" id="' + spanId + '">' + message + ' </li>'
		}

		if ($("#ekycValidMessageList").find('li#' + spanId).length == 0) {
			$("#ekycValidMessageList").append(elem);
		}
		if (!isEmptyFld(elemname)) {
			let nodetype = $('input[name="' + elemname + '"]').prop('type').toUpperCase();

			if (nodetype == "RADIO" || nodetype == "CHECKBOX") {
				$(':input[name="' + elemname + '"]').bind("click", function () {
					removeErrFld($(this));
					$("#" + spanId).remove();

					if ($('#ekycValidMessageList li').length == 0) {
						saveData();
						page_wise_err = false;
						setTimeout(function () {
							$("#btntoolbarsign").trigger("click");
						}, 1000);
					}

				});
			} else {
				$(':input[name="' + elemname + '"]').bind("change", function () {
					removeErrFld($(this));
					$("#" + spanId).remove();

					if ($('#ekycValidMessageList li').length == 0) {
						saveData();
						page_wise_err = false;
						setTimeout(function () {
							$("#btntoolbarsign").trigger("click");
						}, 1000);
					}
				});
			}
		}
	}

	page_wise_err = true;
	setPageKeyInfoFail();
	openElement();
}

function focusToField(page, field) {
	let currentPage = window.location.pathname;
	currentPage = currentPage.substr(currentPage.lastIndexOf("/") + 1);

	//	console.log(currentPage,page,"currpage and page",field)
	if (currentPage != page) {
		// creatSubmtDynaForm(page + "?param1=" + field);
		// creatSubmtDynaForm(page + "?" + window.btoa("param1=" + field));
		window.location.href = page + "?" + window.btoa("param1=" + field);
		//window.location.href=page;
	}
	if (!isEmptyFld(field) && field != "null") {

		let tag = ""; let nodetype = ""; let elemtof = null;
		if ($("#" + field).length) {

			tag = $("#" + field).prop("tagName").toUpperCase();
			if (tag != "SPAN") { nodetype = $('#' + field).prop('type').toUpperCase(); }
			elemtof = $('#' + field);
		} else if ($("." + field).length) {
			tag = $("." + field).prop("tagName").toUpperCase();
			if (tag != "SPAN") { nodetype = $('.' + field).prop('type').toUpperCase(); }
			elemtof = $('.' + field);
		}

		if (tag != "SPAN") {

			if (nodetype == "HIDDEN" || nodetype == "TEXT") {
				$(elemtof).closest(".card-body").find(":input:eq(0)").focus();
				$(elemtof).closest(".card-body").addClass("err-fld");
			} else if (nodetype == "RADIO" || nodetype == "CHECKBOX") {
				$(elemtof).closest(".card-body").find("#" + elemtof[0].id).focus();
				$(elemtof).closest(".card-body").addClass("err-fld");
			}
		}

		if ($("#" + field).length) {
			//			console.log("------> "+field +","+$("#"+field).closest("div.tab-pane").prop("id"))
			let tabid = $("#" + field).closest("div.tab-pane").prop("id");

			if (!isEmptyFld(tabid)) {
				let clz = field.indexOf("dfSelf") == 0 ? "dfself" : "dfsps";//temp solu
				$('a.' + clz).trigger("click");
				$("#" + field).focus();
			}

			$("#" + field).focus();

		}
	}
}

function makeErrMsg(Errsec, ErrCls, Err, ErrPge, isPrepend) {

	let inerr = ' <div class="invalid-feedbacks show inerrfld" >' + Err + '</div>';

	if (!isEmptyFld(Errsec) && $("#" + Errsec).length) {
		if ($("#" + Errsec).closest('div[class^="col-"], div[class*=" col-"]').find(".inerrfld").length == 0) {
			let nodetype = $("#" + Errsec).prop('nodeName').toUpperCase();
			if (nodetype != "SPAN") {
				$("#" + Errsec).addClass("err-fld");
			}

			if (isPrepend == "Y") {
				$("#" + Errsec).closest('div[class^="col-"], div[class*=" col-"]').prepend(inerr);
			} else {
				$("#" + Errsec).closest('div[class^="col-"], div[class*=" col-"]').append(inerr);
			}
		}
	} else if (!isEmptyFld(Errsec) && $("." + Errsec).length) {
		if ($("." + Errsec).closest('div[class^="col-"], div[class*=" col-"]').find(".inerrfld").length == 0) {
			let nodetype = $("." + Errsec).prop('nodeName').toUpperCase();
			if (nodetype != "SPAN") {
				$("." + Errsec).addClass("err-fld");
			}

			$("." + Errsec).closest('div[class^="col-"], div[class*=" col-"]').append(inerr);

		}
	}
}

function removeErrFld(obj) {

	$(obj).closest('div[class^="col-"], div[class*=" col-"]').find("div.inerrfld").remove();
	if ($(obj).prop("name") == "prodrecObjectives") {//exceptional
		$(obj).parents('div[class^="col-"], div[class*=" col-"]').find("div.inerrfld").remove();
	}

	console.log($(obj).prop("id"), "")

	if ($(obj).prop("id") == "span_dfSelfUstaxno") {//exceptional
		$(obj).parents('div[class^="col-"], div[class*=" col-"]').find("div.inerrfld").remove();
	}

	let classList = isEmptyFld($(obj).prop("class")) ? "" : $(obj).prop("class").split(" ")
	if (classList.length > 0) {

		for (let clsIdx = 0; clsIdx < classList.length; clsIdx++) {
			let classname = classList[clsIdx];
			if (classname.indexOf("grp") > 0) {
				$('.' + classname).each(function () {
					$(this).closest('div[class^="col-"], div[class*=" col-"]').find("div.inerrfld").remove();
					$(this).removeClass("err-fld")
				})
			}
		}
	}

	$(obj).removeClass("err-fld");
	$(obj).closest(".card-body").removeClass("err-fld")
}

function setPageKeyInfoSucs() {
	$("#btnAllPgeNoti").addClass("fa-check").removeClass("fa-remove");
	$("#btnAllPgeNoti").closest("button").removeAttr("disabled");
	$("#btnAllPgeNoti").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");
	$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;Key Information verification is completed.</small>');
	$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
}

function setPageKeyInfoFail() {
	$("#btnAllPgeNoti").closest("button").addClass("btn-danger").removeClass("btn-secondary").removeClass("btn-success");
	$("#btnAllPgeNoti").closest("button").removeAttr("disabled");
	$("#btnAllPgeNoti").addClass("fa-remove").removeClass("fa-check");
	$("#applValidMessage").html("")
	$("#applValidMessage").removeClass("alert-default").removeClass("alert-warning")
	$("#hTxtFldSignedOrNot").val("N");
}

function setPageSignInfoSucs() {
	$("#btnSignStatus").addClass("fa-check").removeClass("fa-remove");
	$("#btnSignStatus").closest("button").removeAttr("disabled");
	$("#btnSignStatus").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");
	$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;Signatures are captured. <br/><i class="fa fa-info-circle"></i>&nbsp;<a class="helptextlink" onclick="showManagerMailConfirm()">CLICK HERE</a> to send this FNA to manager approval!</small>');
	$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
	$("#hTxtFldSignedOrNot").val("Y");
}

function setPageSignInfoFail() {
	$("#btnSignStatus").removeClass("fa-check").addClass("fa-remove");
	$("#btnSignStatus").closest("button").attr("disabled", true);
	$("#btnSignStatus").closest("button").removeClass("btn-success").removeClass("btn-secondary").addClass("btn-danger");
	$("#applValidMessage").html('<small><i class="fa fa-exclamation-triangle"></i>&nbsp;Signatures are not yet captured.<br/><i class="fa fa-info-circle"></i>&nbsp;<a class="helptextlink" onclick="allPageValidation()">CLICK HERE</a> to verify and proceed to signature.</small>');
	$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")
	$("#hTxtFldSignedOrNot").val("N");
}

function nextErrPage(currentPage) {

	//kycHome

	if (currentPage == "kycHome") {
		//Tax residency
		if (!validateFnaFatcaTaxDets(true)) {
			return;
		}
		//Product Recommend
		if (!validAdvAndRecmChkboxes(true)) {
			return;
		}
		//Product switch plan
		if (!validateReasonforRecomm(true)) {
			return;
		}
		//Client declartion
		if (!validateClntConsent(true)) {
			return;
		}
		//Manager declaration
		if (!advDeclrAndSupReview(true)) {
			return;
		}
	}
	//if (!validateFnaDepentDets(false))
	//	return;

	//if(!validateFnaWellnesReview(false))
	//	return;

	if (currentPage == "taxResidency") {
		//Product Recommend
		if (!validAdvAndRecmChkboxes(true)) {
			return;
		}
		//Product switch plan
		if (!validateReasonforRecomm(true)) {
			return;
		}
		//Client declartion
		if (!validateClntConsent(true)) {
			return;
		}
		//Manager declaration
		if (!advDeclrAndSupReview(true)) {
			return;
		}
	}
	/*change*/
	/*if (!validateFnaArtuPlantDet(false))
		return;

	if (!validateFnaSwrepPlantDet(false))
		return;
    */
	if (currentPage == "productRecommend") {
		//Product switch plan
		if (!validateReasonforRecomm(true)) {
			return;
		}
		//Client declartion
		if (!validateClntConsent(true)) {
			return;
		}
		//Manager declaration
		if (!advDeclrAndSupReview(true)) {
			return;
		}
	}

	if (currentPage == "adviceBasisRecomm") {
		//Client declartion
		if (!validateClntConsent(true)) {
			return;
		}
		//Manager declaration
		if (!advDeclrAndSupReview(true)) {
			return;
		}
	}
	if (currentPage == "clientDeclaration") {
		//Manager declaration
		if (!advDeclrAndSupReview(true)) {
			return;
		}
	}
	toastr.clear($('.toast'));
	toastr.options = { timeOut: "5000", extendedTimeOut: "1000" };
	toastr["success"]("All Verifications are done!");

	$("#btnAllPgeNoti").addClass("fa-check").removeClass("fa-remove");
	$("#btnAllPgeNoti").closest("button").removeAttr("disabled");
	$("#btnAllPgeNoti").closest("button").addClass("btn-success").removeClass("btn-secondary");

	setTimeout(function () {

		$("#ScanToSignModal").modal({
			backdrop: 'static',
			keyboard: false,
			show: true
		})

		setTimeout(function () {
			startTimer();
		}, 1200);

	}, 1000);

	return true;
}

function creatSubmtDynaForm(href) {
	let lex1 = href.split('?');
	let action = lex1[0];
	let qstr = lex1[1];
	let obj = " ";

	let newdiv = document.createElement('div');

	if (qstr != null) {
		let params = qstr.split('&');
		for (let paramIdx = 0; paramIdx < params.length; paramIdx++) {
			let keyValue = params[paramIdx].split('=');
			let name = keyValue[0];
			let value = keyValue[1];
			obj += "<input type='hidden' name='" + name + "' value='" + value + "'/>";

		}
	}
	newdiv.innerHTML = obj;
	if (document.getElementById("validationForm"))
		document.getElementById("validationForm").appendChild(newdiv);

	document.getElementById("validationForm").action = action;
	document.getElementById("validationForm").method = "GET";
	document.getElementById("validationForm").submit();
}//end creatSubmtDynaForm

function validateFnaSelfSpsDets(flag) {

	let Page2 = "kycHome";

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfName)) {
		addErrorMsgToChat("dfSelfName", NAMEOFCLIENT, Page2, null);

	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfNric)) {
		addErrorMsgToChat("dfSelfNric", NRICOFCLIENT, Page2, null);
	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfGender)) {
		addErrorMsgToChat("dfSelfGendergrp", GENDEROFCLIENT, Page2, "dfSelfGendergrp");
	}


	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfDob)) {
		addErrorMsgToChat("dfSelfDob", DOBOFCLIENT, Page2, null);
	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfMartsts)) {
		addErrorMsgToChat("dfSelfMartsts", MARTIALSTATUSOFCLIENT, Page2, null);
	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfBirthCntry)) {
		addErrorMsgToChat("dfSelfBirthCntry", BIRTHCOUNTRYCLIENT, Page2, null);
	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfNationality)) {
		addErrorMsgToChat("dfSelfNationalitygrp", NATLYCLIENT, Page2, "dfSelfNationalitygrp");
	}


	if (!isEmptyFld(FNASSDET_TABLE_DATA.dfSelfNationality) && (FNASSDET_TABLE_DATA.dfSelfNationality != "SG" && FNASSDET_TABLE_DATA.dfSelfNationality != "SG-PR")) {
		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfNatyDets)) {
			addErrorMsgToChat("dfSelfNatyDets", NATLYCLIENT, Page2, null);
		}
	}

	//contact details sec changed
	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfMobile) && isEmptyFld(FNASSDET_TABLE_DATA.dfSelfHome) && isEmptyFld(FNASSDET_TABLE_DATA.dfSelfOffice)) {
		addErrorMsgToChat("dfSelfMobilegrp", CONTACTDETAILS1, Page2, "dfSelfMobilegrp");

	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfPersEmail) || !emailValidation(FNASSDET_TABLE_DATA.dfSelfPersEmail, 'dfSelfPersEmail')) {
		addErrorMsgToChat("dfSelfPersEmail", CONTACTDETAILS1, Page2);
	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfHomeAddr)) {
		addErrorMsgToChat("dfSelfHomeAddr", REGADDRESSCLIENT, Page2, null);
	}

	if (!isEmptyFld(FNASSDET_TABLE_DATA.dfSelfSameAsregadd) && FNASSDET_TABLE_DATA.dfSelfSameAsregadd == "Y" && isEmptyFld(FNASSDET_TABLE_DATA.dfSelfMailAddr)) {
		addErrorMsgToChat("dfSelfMailAddr", MAILINGADDRESSOTHERS, Page2, null);
	}

	//Employment &Financial Detls Sec Validation start

	//Employer Name
	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfCompname)) {
		addErrorMsgToChat("dfSelfCompname", EMPLOYEEROFCLIENT, Page2, null);
	}

	//Occupation
	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfOccpn)) {
		addErrorMsgToChat("dfSelfOccpn", OCCUPATIONOFCLIENT, Page2, null);
	}

	//Estd.AnnualIncome
	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfAnnlIncome)) {
		addErrorMsgToChat("dfSelfAnnlIncome", ANNUALINCOMEOFCLIENT, Page2, null);
	}

	//Nature of Business
	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfBusinatr)) {
		addErrorMsgToChat("dfSelfBusinatr", NATUREBUSINESSOFCLIENT, Page2, null);
	}
	//End

	if (FNASSDET_TABLE_DATA.dfSelfBusinatr == "Others" && isEmptyFld(FNASSDET_TABLE_DATA.dfSelfBusinatrDets)) {
		addErrorMsgToChat("dfSelfBusinatrDets", NATUREBUSINESSOTHERSOFCLIENT, Page2, null);
	}

	let srcFunds = isEmptyFld(FNASSDET_TABLE_DATA.dfSelfFundsrc) ? "{}" : JSON.parse(FNASSDET_TABLE_DATA.dfSelfFundsrc);
	if (!isEmptyFld(srcFunds)) {

		if ((srcFunds.CERN == "Y" || srcFunds.CINV == "Y" || srcFunds.CPRS == "Y" || srcFunds.CCPF == "Y" || srcFunds.COTH == "Y")) {

		} else {
			addErrorMsgToChat("chkCDClntFundSrcgrp", SOURCEOFFFUNDCLIENT, Page2, "chkCDClntFundSrcgrp");
		}


	}
	//	else if(!isEmpty(srcFunds) && srcFunds == "{}" ) {
	//		addErrorMsgToChat("chkCDClntErndIncm",SOURCEOFFFUNDCLIENT,Page2,"dfSelfFundsrc");
	//	}


	if (srcFunds.COTH == "Y" && isEmptyFld(FNASSDET_TABLE_DATA.dfSelfFundsrcDets)) {
		addErrorMsgToChat("dfSelfFundsrcDets", SRCFUNDOTHCLIENT, Page2, null);
	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfEduLevel)) {
		addErrorMsgToChat("dfSelfEduLevelGrp", EDULVLCLIENT, Page2, "dfSelfEduLevelGrp");
	}

	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfEngSpoken)) {
		addErrorMsgToChat("dfSelfEngSpokenY", LANGSPOKCLIENT, Page2, "dfSelfEngSpoken");
	}


	if (isEmptyFld(FNASSDET_TABLE_DATA.dfSelfEngWritten)) {
		addErrorMsgToChat("dfSelfEngWrittenY", LANGWRITECLIENT, Page2, "dfSelfEngWritten");
	}

	//---------------- Interpreter and Language

	//	alert("FNA_TABLE_DATA.cdIntrprtflg--->"+FNA_TABLE_DATA.cdIntrprtflg)
	let selfLanguage = isEmptyFld(FNA_TABLE_DATA.cdLanguages) ? "{}" : JSON.parse(FNA_TABLE_DATA.cdLanguages);

	if (!isEmptyFld(FNA_TABLE_DATA.cdIntrprtflg) && FNA_TABLE_DATA.cdIntrprtflg == 'Y') {

		if (selfLanguage.ENG == "Y" || selfLanguage.OTH == "Y" || selfLanguage.MAN == "Y" || selfLanguage.MAL == "Y" || selfLanguage.TAM == "Y") {
			//			no action		
		} else {
			addErrorMsgToChat("htflaneng", "Select Any 1 Language Use", Page2, null);
		}

		if (isEmptyFld(FNA_TABLE_DATA.intprtName)) {
			addErrorMsgToChat("intprtName", INTEPRE_NAME, Page2, null);
		}

		if (isEmptyFld(FNA_TABLE_DATA.intprtContact)) {
			addErrorMsgToChat("intprtContact", INTEPRE_CONTACT, Page2, null);
		}
		if (isEmptyFld(FNA_TABLE_DATA.intprtRelat)) {
			addErrorMsgToChat("intprtRelat", INTEPRE_SEL_RELSHIP, Page2, null);
		}
	}
	//----------------end of interpreter

	//-----------------------------Spouse Elements---------

	if (!isEmptyFld(FNASSDET_TABLE_DATA.dfSpsName)) {
		let spstabfocus = false;

		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsNric)) {
			addErrorMsgToChat("dfSpsNric", NRICOFSPOUSE, Page2, null);
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;

		}

		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsGender)) {
			addErrorMsgToChat("dfSpsGendergrp", NAMEOFSPOUSEGENDER, Page2, "dfSpsGendergrp");
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}


		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsDob)) {
			addErrorMsgToChat("dfSpsDob", DOBOFSPOUSE, Page2, null);
			//$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}

		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsMartsts)) {
			addErrorMsgToChat("dfSpsMartsts", MARTIALSTATUSOFSPOUSE, Page2, null);
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}
		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsBirthCntry)) {
			addErrorMsgToChat("dfSpsBirthCntry", BIRTHCOUNTRYSPOUSE, Page2, null);
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}

		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsNationality)) {
			addErrorMsgToChat("dfSpsNationalitygrp", NATLYSPOUSE, Page2, "dfSpsNationalitygrp");
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}

		if (!isEmptyFld(FNASSDET_TABLE_DATA.dfSpsNationality) && (FNASSDET_TABLE_DATA.dfSpsNationality != "SG" && FNASSDET_TABLE_DATA.dfSpsNationality != "SG-PR")) {
			if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsNatyDets)) {
				addErrorMsgToChat("dfSpsNatyDets", NATLYSPOUSEOTH, Page2, null);
				//				$('#myTab li:eq(1) a').tab('show');
				spstabfocus = true;
			}
		}

		//spouse contact detls change
		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsHp) && isEmptyFld(FNASSDET_TABLE_DATA.dfSpsHome) && isEmptyFld(FNASSDET_TABLE_DATA.dfSpsOffice)) {
			addErrorMsgToChat("dfSpsHpgrp", CONTACTDETAILSSPOUSE1, Page2, "dfSpsHpgrp");
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}


		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsPersEmail) || !emailValidation(FNASSDET_TABLE_DATA.dfSpsPersEmail, 'dfSpsPersEmail')) {
			addErrorMsgToChat("dfSpsPersEmail", CONTACTDETAILSSPOUSE1, Page2, null);
			spstabfocus = true;
		}

		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsHomeAddr)) {
			addErrorMsgToChat("dfSpsHomeAddr", REGADDRESSSPOUSE, Page2, null);
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}


		if (!isEmptyFld(FNASSDET_TABLE_DATA.dfSpsMailAddrFlg) && FNASSDET_TABLE_DATA.dfSpsMailAddrFlg == "Y" && isEmptyFld(FNASSDET_TABLE_DATA.dfSpsMailAddr)) {
			addErrorMsgToChat("dfSpsMailAddr", MAILINGADDRESSOTHERS, Page2, null);
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}

		//Employment &Financial Detls Sec Validation start spouse

		//Employer Name
		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsCompname)) {
			addErrorMsgToChat("dfSpsCompname", EMPLOYEEROFSPOUSE, Page2, null);
			spstabfocus = true;
		}

		//Occupation
		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsOccpn)) {
			addErrorMsgToChat("dfSpsOccpn", OCCUPATIONOFSPOUSE, Page2, null);
			spstabfocus = true;
		}

		//Estd.AnnualIncome
		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsAnnlIncome)) {
			addErrorMsgToChat("dfSpsAnnlIncome", ANNUALINCOMEOFSPOUSE, Page2, null);
			spstabfocus = true;
		}

		//Nature of Business
		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsBusinatr)) {
			addErrorMsgToChat("dfSpsBusinatr", NATUREBUSINESSOFSPOUSE, Page2, null);
			spstabfocus = true;
		}
		//End

		//Nature ofBuss Others validation spouse
		if (FNASSDET_TABLE_DATA.dfSpsBusinatr == "Others" && isEmptyFld(FNASSDET_TABLE_DATA.dfSpsBusinatrDets)) {
			addErrorMsgToChat("dfSpsBusinatrDets", NATUREBUSINESSOTHERSOFSPOUSE, Page2, null);
			spstabfocus = true;
		}
		//End

		let srcFundsSps = isEmptyFld(FNASSDET_TABLE_DATA.dfSpsFundsrc) ? "{}" : JSON.parse(FNASSDET_TABLE_DATA.dfSpsFundsrc);

		if (!isEmptyFld(srcFundsSps)) {
			if (srcFundsSps.CERN == "Y" || srcFundsSps.CINV == "Y" || srcFundsSps.CPRS == "Y" || srcFundsSps.CCPF == "Y" || srcFundsSps.COTH == "Y") {

			} else {
				addErrorMsgToChat("spsSrcFundgrp", SOURCEOFFFUNDSPOUSE, Page2, "spsSrcFundgrp");

				//				$('#myTab li:eq(1) a').tab('show');
				spstabfocus = true;
			}

		}

		if (srcFundsSps.COTH == "Y" && isEmptyFld(FNASSDET_TABLE_DATA.dfSpsFundsrcDets)) {
			addErrorMsgToChat("dfSpsFundsrcDets", SRCFUNDOTHSPOUSE, Page2, null);
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}


		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsEduLevel)) {
			addErrorMsgToChat("dfSpsEduLevelGrp", EDULVLSPOUSE, Page2, "dfSpsEduLevelGrp");
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}

		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsEngSpoken)) {
			addErrorMsgToChat("dfSpsEngSpokenY", LANGSPOKSPOUSE, Page2, "dfSpsEngSpoken");
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}

		if (isEmptyFld(FNASSDET_TABLE_DATA.dfSpsEngWritten)) {
			addErrorMsgToChat("dfSpsEngWrittenY", LANGWRITESPOUSE, Page2, "dfSpsEngWritten");
			//			$('#myTab li:eq(1) a').tab('show');
			spstabfocus = true;
		}

		if (spstabfocus) {
			$('#myTab li:eq(1) a').tab('show');
		}
	}
	//-----------------------------end of spouse elements

	/*	$("#chkSignPersDet").prop("checked",true);
		$("#validation1").removeClass("err-fld");
		*/
	//	console.log(page_wise_err,"page_wise_err")

	if (page_wise_err) return false;

	//closeElement()

	return true;
}

function validateFnaFatcaTaxDets(flag) {

	//clear all errmessages in keyInformation PopUp chat window
	removeAllErrorMsgInChat();

	let page4 = "taxResidency";
	let spsName = FNASSDET_TABLE_DATA.dfSpsName;//$("#dfSpsName").val();


	let clientFatcaFlag = FNASSDET_TABLE_DATA.dfSelfUsnotifyflg;// $("#dfSelfUsnotifyflg").prop("checked");
	let spouseFatcaFlag = FNASSDET_TABLE_DATA.dfSpsUsnotifyflg;

	let clientUsPersFlag = FNASSDET_TABLE_DATA.dfSelfUspersonflg;
	let spouseUsPersFlag = FNASSDET_TABLE_DATA.dfSpsUspersonflg;

	if (clientFatcaFlag != "Y" && clientUsPersFlag != "Y") {
		addErrorMsgToChat("dfSelfUsnotifyflg1", "Select any US Tax Declaration option for client(1)", page4, "dfSelfUsnotifyflggrp");
	}

	if (!isEmptyFld(spsName)) {
		if (spouseFatcaFlag != "Y" && spouseUsPersFlag != "Y") {
			addErrorMsgToChat("dfSpsUsnotifyflg1", "Select any US Tax Declaration option for client(2)", page4, "dfSpsUsnotifyflggrp");
		}
	}

	let selfTaxNo = FNASSDET_TABLE_DATA.dfSelfUstaxno;//document.getElementById('dfSelfUstaxno');
	let spouseTaxNo = FNASSDET_TABLE_DATA.dfSpsUstaxno;//document.getElementById('dfSpsUstaxno');

	if (clientUsPersFlag == "Y") {
		if (isEmptyFld(selfTaxNo)) {
			removeAllErrorMsgInChat();
			addErrorMsgToChat("dfSelfUstaxno", USTAX_PRSNCLNT_VAL_MSG, page4, null);
		}
	}// end of if

	if (spouseUsPersFlag == "Y" && !isEmptyFld(spsName)) {
		if (isEmptyFld(spouseTaxNo)) {
			removeAllErrorMsgInChat();
			addErrorMsgToChat("dfSpsUstaxno", USTAX_PRSNSPS_VAL_MSG, page4, null);
		}
	}// end of if

	if (clientFatcaFlag == "Y") {

		if (FATCATAXDET_DATA.length == 0) {
			removeAllErrorMsgInChat();
			addErrorMsgToChat("span_dfSelfUstaxno", FNA_FATCA_ATLEAST_1TAX + " for Client(1)", page4, null);
			// $('body').scrollTo('#span_dfSelfUstaxno');
			//	$(".addtaxdetsbtn").trigger("click");

		} else {

			removeErrFld($("#span_dfSelfUstaxno"));
			let fatcaData = FATCATAXDET_DATA;

			for (let tax in fatcaData) {

				let refNo = fatcaData[tax]["taxRefNo"];
				let cntry = fatcaData[tax]["taxCountry"];
				let reason = fatcaData[tax]["reasonToNoTax"];
				let reasonDets = fatcaData[tax]["reasonbDetails"];
				//				console.log(cntry)

				if (isEmptyFld(cntry)) {
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSelfUstaxno", FNA_FATCACONTRY_VALMSG + " for Client(1) ", page4, null);
				}

				if (isEmptyFld(refNo) && isEmptyFld(reason)) {
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSelfUstaxno", FNA_NOTAXREASON + " for Client(1) ", page4, null);
				}

				if (!isEmptyFld(reason) && reason == "Reason B" && isEmptyFld(reasonDets)) {
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSelfUstaxno", FNA_REASONDETAILS + " for Client(1) ", page4, null);
				}

			}
		}
	}

	if (spouseFatcaFlag == "Y" && !isEmptyFld(spsName)) {
		if (FATCATAXDETSPS_DATA.length == 0) {
			removeAllErrorMsgInChat();
			addErrorMsgToChat("span_dfSpsUstaxno", FNA_FATCA_ATLEAST_1TAX + " for Client(2)", page4, null);
		} else {

			let fatcaDataSps = FATCATAXDETSPS_DATA;

			for (let stax in fatcaDataSps) {

				let refNo = fatcaDataSps[stax]["taxRefNo"];
				let cntry = fatcaDataSps[stax]["taxCountry"];
				let reason = fatcaDataSps[stax]["reasonToNoTax"];
				let reasonDets = fatcaDataSps[stax]["reasonbDetails"];

				if (isEmptyFld(cntry)) {
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSpsUstaxno", FNA_FATCACONTRY_VALMSG + " for Client(2) ", page4, null);

				}

				if (isEmptyFld(refNo) && isEmptyFld(reason)) {
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSpsUstaxno", FNA_NOTAXREASON + " for Client(2) ", page4, null);
				}

				//				if(isEmpty(reasonDets)){
				//					addErrorMsgToChat("span_dfSpsUstaxno",FNA_REASONDETAILS + " for Client(2) ",page4,null);
				//				}


				if (!isEmptyFld(reason) && reason == "Reason B" && isEmptyFld(reasonDets)) {
					removeAllErrorMsgInChat();
					addErrorMsgToChat("span_dfSelfUstaxno", FNA_REASONDETAILS + " for Client(2) ", page4, null);
				}

			}

		}
	}

	/*	$("#TaxResi").prop("checked",true);
		$("#validation2").removeClass("err-fld");*/

	if (page_wise_err) return false;

	return true;
}

function validateFnaDepentDets(flag) {

	let page4 = "finanWellReview";

	let depData = FNADEPNT_TABLE_DATA;

	if (depData.length > 0) {
		for (let dep in depData) {
			let depvalue = depData[dep];
			if (isEmptyFld(depvalue["depnName"])) {
				addErrorMsgToChat("", FNA_DEPNTNAME_VALMSG + " for Client(1) ", page4, null);
			}
		}
	}

	$("#FinWell").prop("checked", true);
	$("#validation3").removeClass("err-fld");

	if (page_wise_err) return false;

	return true;
}

function validateFnaProdRecomDet() {

	let page4 = "productRecommend";

	let prodRecomData = FNAPROD_RECOM_DATA;

	if (prodRecomData.length == 0) {
		addErrorMsgToChat("", FNA_PROD_RECOM_VALMSG, page4, "NoProdDetlsRow", "NoProdDetlsRow", "Y");
	}

	$("#LifeILPNewPurchase").prop("checked", true);
	$("#validation4").removeClass("err-fld");

	if (page_wise_err) return false;

	return true;
}

function validateFnaArtuPlantDet(flag) {

	let page4 = "productRecommend";

	let advrecplanData = ADVRECPRDTPLAN_DATA;

	for (let advrec in advrecplanData) {

		let planOpt = advrecplanData[advrec]["txtFldRecomPpOpt"];
		let prodType = advrecplanData[advrec]["selFnaAdvRecPrdtType"]
		let prinName = advrecplanData[advrec]["selFnaAdvRecCompName"]
		let riskrate = advrecplanData[advrec]["selRecomProdRisk"];

		if (isEmptyFld(prodType)) {
			addErrorMsgToChat("", FNA_ARTUPLANPRDTTYPE_VALMSG + " for Client(1) ", page4, null);
		}

		if (isEmptyFld(prinName)) {
			addErrorMsgToChat("", FNA_ARTUPLAN_COMP_VALMSG + " for Client(1) ", page4, null);
		}

		if (prodType == "ILP") {

			if (isEmptyFld(riskrate)) {
				addErrorMsgToChat("", FNA_ARTUPLAN_RISKRATE_VALMSG + " for Client(1) ", page4, null);
			}

		}
	}

	$("#LifeILPNewPurchase").prop("checked", true);
	$("#validation4").removeClass("err-fld");

	if (page_wise_err) return false;

	return true;
}

function validAdvAndRecmChkboxes() {

	let page4 = "productRecommend";

	let prodObject = !isEmptyFld(FNA_TABLE_DATA.prodrecObjectives) && FNA_TABLE_DATA.prodrecObjectives != "undefined" ? JSON.parse(FNA_TABLE_DATA.prodrecObjectives) : {};

	//if(!isJsonObjEmpty(prodObject) && prodObject.ALLOBJ != "Y" && prodObject.PRO != "Y" &&  prodObject.INV != "Y" && prodObject.RET != "Y" && prodObject.HIN != "Y" && prodObject.SAV != "Y"){
	if (isJsonObjEmpty(prodObject) || (!isJsonObjEmpty(prodObject) && (prodObject.ALLOBJ != "Y" && prodObject.PRO != "Y" && prodObject.INV != "Y" && prodObject.RET != "Y" && prodObject.HIN != "Y" && prodObject.SAV != "Y"))) {
		//		showObjSecError();
		addErrorMsgToChat("prodrecObjectives", ADNT_INCMPROTECT_VAL_MSG, page4, "chkProdObject");
	}

	//	$("#LifeILPNewPurchase").prop("checked",true);
	//	$("#validation4").removeClass("err-fld");
	//	hideObjSecError();

	if (page_wise_err) return false;
	$("#divProdRecomFrm").find("div.inerrfld").remove();
	//hideObjSecError();
	$("#objSecError").html("");
	return true;
}// end of validAdvAndRecmChkboxes

/*
function validateFnaSwrepPlantDet(flag) {

	$("#LifeILPSwtch").prop("checked",true);
	$("#validation5").removeClass("err-fld");

	return true;

*/

function validateFnaSwrepPlantDet(flag) {

	let page4 = "productSwitchRecomm";

	let question1 = FNA_TABLE_DATA.swrepConflg;
	let question2 = FNA_TABLE_DATA.swrepAdvbyflg;
	let question3 = FNA_TABLE_DATA.swrepDisadvgflg;
	let question4 = FNA_TABLE_DATA.swrepProceedflg;

	//q1
	if (isEmptyFld(question1)) {
		addErrorMsgToChat("", SWREPCONFLGQ1, page4, "swrepConflgC", "swrepConflgC");
	}

	//q2
	if (isEmptyFld(question2)) { addErrorMsgToChat("", SWREPADVBYFLGQ2, page4, "swrepAdvbyflgC", "swrepAdvbyflgC"); }

	//q3
	if (isEmptyFld(question3)) { addErrorMsgToChat("", SWREPDISADVGFLGQ3, page4, "swrepDisadvgflgC", "swrepDisadvgflgC"); }

	//q4
	if (isEmptyFld(question4)) { addErrorMsgToChat("", SWREPPROCEEDFLGQ4, page4, "swrepProceedflgC", "swrepProceedflgC"); }

	if (page_wise_err) return false;
	$("#LifeILPSwtch").prop("checked", true);
	$("#validation5").removeClass("err-fld");

	return true;
}

function validateReasonforRecomm(flag) {

	let page4 = "adviceBasisRecomm";

	let resnForRecom = FNA_TABLE_DATA.advrecReason;//document.getElementById('advrecReason');
	let resnForRecom1 = FNA_TABLE_DATA.advrecReason1;//document.getElementById('advrecReason1');
	let resnForRecom2 = FNA_TABLE_DATA.advrecReason2;//document.getElementById('advrecReason2');
	//var  resnForRecom3= FNA_TABLE_DATA.advrecReason3;
	if (flag) {

		let emptyAdvReason = false;
		let emptyAdvReason1 = false;
		let emptyAdvReason2 = false;
		if (isEmptyFld(resnForRecom)) {
			addErrorMsgToChat("advrecReason", ADNT_RESNRECOM_VAL_MSG, page4, null);
			emptyAdvReason = true;
		}

		if (isEmptyFld(resnForRecom1)) {
			addErrorMsgToChat("advrecReason1", ADNT_RESNRECOM1_VAL_MSG, page4, null);
			emptyAdvReason1 = true;
		}

		if (isEmptyFld(resnForRecom2)) {
			addErrorMsgToChat("advrecReason2", ADNT_RESNRECOM2_VAL_MSG, page4, null);
			emptyAdvReason2 = true;
		}

		if (emptyAdvReason) {
			$("#advrecReason").focus();
		} else if (emptyAdvReason1) {
			$("#advrecReason1").focus();
		} else if (emptyAdvReason2) {
			$("#advrecReason2").focus();
		}

		/*if(isEmpty(resnForRecom3)){
					addErrorMsgToChat("advrecReason3",ADNT_RESNRECOM3_VAL_MSG ,page4,null);
		}*/
	}

	/*$("#AdvBscRecomm").prop("checked",true);
$("#validation6").removeClass("err-fld");*/

	if (page_wise_err) return false;

	return true;
}

function validateClntConsent() {

	let page4 = "clientDeclaration"

	let clientack = isEmptyFld(FNA_TABLE_DATA.cdackAgree) ? "" : (FNA_TABLE_DATA.cdackAgree)//.toUpperCase();
	let clientackremarks = FNA_TABLE_DATA.cdackAdvrecomm;
	let clientconsent = FNA_TABLE_DATA.cdConsentApprch;

	if (isEmptyFld(clientack)) {
		addErrorMsgToChat("radFldackAgree", CLNT_ACKN_VAL_MSG, page4, "cdackAgree");
		//		$('body').scrollTo('#radFldackAgree');
	}// end of if

	if (clientack == "partial" && isEmptyFld(clientackremarks)) {
		addErrorMsgToChat("cdackAdvrecomm", CLNT_CHOICE_REPRECOMM_VAL_MSG, page4, null);
	}
	if (isEmptyFld(clientconsent)) {
		addErrorMsgToChat("span_cdConsentApprch", CLNT_CONSENT_VAL_MSG, page4, "cdConsentApprch");
	} else {
		removeErrFld($("#span_cdConsentApprch"));
	}

	/*$("#ClntDecl").prop("checked",true);
	$("#validation7").removeClass("err-fld");*/

	if (page_wise_err) return false;

	return true;
}// end of validateClntConsent

function advDeclrAndSupReview(flag) {

	let page4 = "managerDeclaration";
	let servAdvId = "";//$("#txtFldClntServAdv").val();
	let servAdvMgrId = "";//$("#txtFldMgrId").val();

	//if (LOG_USER_MGRACS == "Y" && LOG_USER_ADVID == servAdvMgrId && flag) {

	let radSupReview = FNA_TABLE_DATA.suprevMgrFlg;

	if (isEmptyFld(radSupReview)) {
		addErrorMsgToChat("span_suprevMgrFlg", SUPR_REVIEW_VAL_MSG, page4, "suprevMgrFlg");
	}// end of if

	if (radSupReview == "N") {
		let suprRevFld = FNA_TABLE_DATA.suprevFollowReason;

		if (isEmptyFld(suprRevFld)) {
			addErrorMsgToChat("suprevFollowReason", SUPRREVW_RESN_VAL_MSG, page4, "suprevFollowReason");
		}
	}// end of if

	//}

	$("#AdvMngrDecl").prop("checked", true);
	$("#validation8").removeClass("err-fld");

	if (page_wise_err) return false;

	return true;
}// end of advDeclrAndSupReview


function validateFnaWellnesReview(flag) {

	let page4 = "finanWellReview";

	let depntSec = FNA_TABLE_DATA.fwrDepntFlg;//$("#fwrDepntflg").val();
	let finCommSec = FNA_TABLE_DATA.fwrFinAssetFlg; //$("#fwrFinassetflg").val();

	if (!isEmptyFld(depntSec) && depntSec == "Y" && flag) {
		if (FNADEPNT_TABLE_DATA.length == 0) {
			addErrorMsgToChat("", FNA_DEPENT_NODET, page4, "collapseDepend", "collapseDepend");
		}
	}

	$("#FinWell").prop("checked", true);
	$("#validation3").removeClass("err-fld");

	if (page_wise_err) return false;

	return true;
}

var allDataMap = new Map();
function ajaxCall(fnaId) {

	$.ajax({
		type: "GET",
		url: "Common/getAllPageData/" + fnaId,
		dataType: "json",
		contentType: "application/json",

		async: false,

		beforeSend: function () {
			$("#loadingLayer").show();
			$("#loadingMessage").show();
			//loaderBlock();
		},

		success: function (data) {

			$.each(data, function (key, value) {
				allDataMap.set(key, value);
			});
			fetchAllPageData();
		},
		complete: function () {
			// alert("Loaded Success...");
			console.log("loaded success!")
			$("#loadingMessage").hide();
			$('#cover-spin').hide();
			//setTimeout(function(){ l/*oaderHide(); */},200);
		},
		error: function () {
			// alert("Loading Error...");
			$("#loadingMessage").hide();
			//setTimeout(function(){/* loaderHide(); */},200);
			$('#cover-spin').hide();
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
	return allDataMap;
}

function chkforOthers(radbtn, txtfldid) {

	if ($(radbtn).is(":checked")) {
		$("#" + txtfldid).removeAttr('disabled');

	} else {
		$("#" + txtfldid).val('');
		$("#" + txtfldid).prop('disabled', 'true');
	}

	/*if (radbtn.value == value) {

		$("#" + txtfldid).focus();
		$("#" + txtfldid).attr('readonly', false);

		$("#" + txtfldid).attr('class', '');
		$("#" + txtfldid).removeClass('hidden');
		$("#" + txtfldid).addClass("writeModeTextbox");

	} else {
		$("#" + txtfldid).val("");
		$("#" + txtfldid).attr('readonly', true);

		$("#" + txtfldid).attr('class', '');

		$("#" + txtfldid).addClass("readOnlyText");
	}*/
}

function setQValue(obj) {
	if ($(obj).prop("checked")) { $(obj).val("Y") }
	else { $(obj).val("Y") }
}

// $("#btnPolicyESubmit").on("click", function () {
function policySubmission(principal) {
	console.log("Policy is submitted!");
	let fnaId = $("#hdnSessFnaId").val();
	console.log("FNA ID: " + fnaId);
	let principals = [];
	/*$("#PrinList").find('input[type="checkbox"]').each(function () {
		if ($(this).prop('checked') == true) {
			console.log($(this).val() + " is checked!");
			principals.push($(this).val());
		}
	});*/
	principals.push(principal);
	if (principals.length > 0) {
	
		saveData();
		
		setTimeout(() => {
		
			let signOrNot = $("#hTxtFldSignedOrNot").val();
			
			if (signOrNot != "Y") {
				$("#btntoolbarsign").trigger("click");
			} else {
			
				//preparing data....
			
				var jsonData = "";
				var accssTok = "";
				var afaPsgUrl = "",afaPsgIndexPath="";
				
				$.ajax({
					type: "GET",
					async:false,
					url: "Common/policySubmit?fnaId=" + fnaId + "&principals=" + principals,
					success: function (result) {
						let parsedResult = JSON.parse(result);
						
						if (parsedResult && parsedResult != "failed" && parsedResult.status == "success") {
							
							switch(principal) {
								case "ntuc_income":
									jsonData = parsedResult.afaJson;
									afaPsgUrl = parsedResult.afaPsgUrl;
									afaPsgIndexPath = parsedResult.afaPsgIndexPath;
									var clientcred = parsedResult.afaPsgMyCred;
									
									//window.open(afaPsgUrl + afaPsgIndexPath + "?afaClientInfo=" + jsonData + "&afaCred=" + clientcred, "policy");
									
									creatSubmtDynaIsubmit(afaPsgUrl + afaPsgIndexPath + "?afaClientInfo=" + jsonData + "&afaCred=" + clientcred);
									break;
									
								case "aviva":
									createAvivaPolicy(parsedResult.encryptedPayload);
									break;
									
								default:
									break;
							}
						}
					},
					complete: function () {
						$('#cover-spin').hide();
					},
					error: function (e) {
						$('#cover-spin').hide();
						Swal.fire({
							icon: 'error',
							text: 'Please Try again Later or else Contact your System Administrator',
						});
					}
				});
			}
		}, 1200);
		
		/*$.ajax({
						type: "GET",async:false,
						url: "Common/getAFAToekn",
						success: function (response) {
							
							var respbody = JSON.parse(response.body);
							if(response.status == "success"){
								
								accssTok = (respbody.access_token);
							}else{
								alert("error in getting access token")
							}
						},
						complete: function () {
							$('#cover-spin').hide();
						},
						error: function (e) {
							alert("error in getting access token")
						}
					});*/
					
					
					
					
					/*$.ajax({
									type: "POST",
									url: "http://localhost:7206/api/v1/esubmit/insurer",
									 headers: {
								        'Authorization':'Bearer '+accssTok
								    },
									data :{afaClientInfo : jsonData , afaToken :accssTok },
									success: function (s3) {
										var suc = (s3);
										window.open(suc)
									},
									complete: function () {
										$('#cover-spin').hide();
									},
									error: function (e) {
										alert("error in getting access token111111111")
									}
								});*/
	}
// });
}

function chkNextChange() {
	//	alert(strPageValidMsgFld)
	if (!isEmptyFld(strPageValidMsgFld)) {

		setTimeout(function () {
			$('#cover-spin').show();
		}, 100)

		setTimeout(function () {
			$("#btntoolbarsign").trigger("click");
		}, 1200)
	}
}

// open search screen wihle click seach icon in header
function openSrchScreen() {
	window.location.href = "index"
}

function setQ1Value(obj) {
	let id = $(obj).attr("id");
	if (id == 'swrepConflgY') {

		if (obj.checked == true) {
			$("#swrepConfDets").removeAttr('disabled');
			//$("#swrepConflg").val("");
			$("#swrepConflgN").val("");
			$("#swrepConflgN").prop("checked", false)
			$("#swrepConflg").val("Y");
			$("#swrepConflgY").val("Y");
			// $("#span_swrepConflgC").remove();
		}
		else if (obj.checked == false) {
			$("#swrepConflgY").prop("checked", false)
			$("#swrepConflg").val("");
			$("#swrepConflgN").val("");
			$("#swrepConflgY").val("");
		}
	}

	if (id == 'swrepConflgN') {

		if (obj.checked == true) {
			$("#swrepConfDets").val('');
			$("#swrepConfDets").prop('disabled', 'true');
			//$("#swrepConflg").val("N");
			$("#swrepConflgY").val("");
			$("#swrepConflgY").prop("checked", false)
			$("#swrepConflg").val("N");
			$("#swrepConflgN").val("N");
			// $("#span_swrepConflgC").remove();
		}
		else if (obj.checked == false) {
			$("#swrepConflgN").prop("checked", false)
			$("#swrepConflg").val("");
			$("#swrepConflgN").val("");
			$("#swrepConflgY").val("");
		}
	}
	// strPageValidMsgFld="retest";
	// chkNextChange();
	if (page_wise_err) {
		page_wise_err = false;
		saveData();
		setTimeout(function () {
			$("#btntoolbarsign").trigger("click");
		}, 1200);
	}
}

function setQ2Value(obj) {
	let id = $(obj).attr("id");
	if (id == 'swrepAdvbyflgY') {
		if (obj.checked == true) {
			//$("#swrepAdvbyflg").val("");
			$("#swrepAdvbyflgN").val("");
			$("#swrepAdvbyflgN").prop("checked", false)
			$("#swrepAdvbyflg").val("Y");
			$("#swrepAdvbyflgY").val("Y");
			// $("#span_swrepAdvbyflgC").remove();
		}
		else if (obj.checked == false) {
			$("#swrepAdvbyflgY").prop("checked", false)
			$("#swrepAdvbyflg").val("");
			$("#swrepAdvbyflgY").val("");
			$("#swrepAdvbyflgN").val("");
		}
	}

	if (id == 'swrepAdvbyflgN') {
		if (obj.checked == true) {
			//$("#swrepAdvbyflg").val("");
			$("#swrepAdvbyflgY").val("");
			$("#swrepAdvbyflgY").prop("checked", false)
			$("#swrepAdvbyflg").val("N");
			$("#swrepAdvbyflgN").val("N");
			// $("#span_swrepAdvbyflgC").remove();
		}
		else if (obj.checked == false) {
			$("#swrepAdvbyflgN").prop("checked", false)
			$("#swrepAdvbyflg").val("");
			$("#swrepAdvbyflgN").val("");
			$("#swrepAdvbyflgY").val("");
		}
	}
	// strPageValidMsgFld="retest";
	// chkNextChange();
	if (page_wise_err) {
		page_wise_err = false;
		saveData();
		setTimeout(function () {
			$("#btntoolbarsign").trigger("click");
		}, 1200);
	}
}

function setQ3Value(obj) {
	let id = $(obj).attr("id");
	if (id == 'swrepDisadvgflgY') {
		if (obj.checked == true) {
			//$("#swrepConflg").val("");
			$("#swrepDisadvgflgN").val("");
			$("#swrepDisadvgflgN").prop("checked", false)
			$("#swrepDisadvgflg").val("Y");
			$("#swrepDisadvgflgY").val("Y");
			// $("#span_swrepDisadvgflgC").remove();
		}
		else if (obj.checked == false) {
			$("#swrepDisadvgflgY").prop("checked", false)
			$("#swrepDisadvgflg").val("");
			$("#swrepDisadvgflgY").val("");
			$("#swrepDisadvgflgN").val("");
		}
	}

	if (id == 'swrepDisadvgflgN') {
		if (obj.checked == true) {
			//$("#swrepDisadvgflg").val("");
			$("#swrepDisadvgflgY").val("");
			$("#swrepDisadvgflgY").prop("checked", false)
			$("#swrepDisadvgflg").val("N");
			$("#swrepDisadvgflgN").val("N");
			// $("#span_swrepDisadvgflgC").remove();
		}
		else if (obj.checked == false) {
			$("#swrepDisadvgflgN").prop("checked", false)
			$("#swrepDisadvgflg").val("");
			$("#swrepDisadvgflgN").val("");
			$("#swrepDisadvgflgY").val("");
		}
	}
	// strPageValidMsgFld="retest";
	// chkNextChange();
	if (page_wise_err) {
		page_wise_err = false;
		saveData();
		setTimeout(function () {
			$("#btntoolbarsign").trigger("click");
		}, 1200);
	}
}

function setQ4Value(obj) {
	let id = $(obj).attr("id");
	if (id == 'swrepProceedflgY') {
		if (obj.checked == true) {
			$("#swrepProceedflg").val("");
			$("#swrepProceedflgN").val("");
			$("#swrepProceedflgN").prop("checked", false)
			$("#swrepProceedflg").val("Y");
			$("#swrepProceedflgY").val("Y");
			// $("#span_swrepProceedflgC").remove();
		}
		else if (obj.checked == false) {
			$("#swrepProceedflgY").prop("checked", false)
			$("#swrepProceedflg").val("");
			$("#swrepProceedflgY").val("");
		}
	}

	if (id == 'swrepProceedflgN') {
		if (obj.checked == true) {
			//$("#swrepProceedflg").val("");
			$("#swrepProceedflgY").val("");
			$("#swrepProceedflgY").prop("checked", false)
			$("#swrepProceedflg").val("N");
			$("#swrepProceedflgN").val("N");
			// $("#span_swrepProceedflgC").remove();
		}
		else if (obj.checked == false) {
			$("#swrepProceedflgN").prop("checked", false)
			$("#swrepProceedflg").val("");
			$("#swrepProceedflgN").val("");
			$("#swrepProceedflgY").val("");
		}
	}
	// strPageValidMsgFld="retest";
	// chkNextChange();
	if (page_wise_err) {
		page_wise_err = false;
		saveData();
		setTimeout(function () {
			$("#btntoolbarsign").trigger("click");
		}, 1200);
	}
}

// go Prev Page 
function goPrevPage(obj, prevScreen) {
	saveData(obj, prevScreen, true, false);
	//window.location.href= prevScreen;
}

function chkAgreeDisagreeFld(radBtn, txtFldId) {

	if ($(radBtn).is(":checked")) {
		let thisFldId = $(radBtn).attr("id");
		if ((thisFldId == 'radFldackAgree') || (thisFldId == 'suprevMgrFlgA')) {
			if (thisFldId == 'suprevMgrFlgA') {

				$(radBtn).val("Y")
				$("#mngrDisagrReason").html("");
				$("#mngrBtnErrorMsg").html("");

				$("#errorFld").css("border", "");

				$("#ScanToSignModal").modal({
					backdrop: 'static',
					keyboard: false,
					show: true
				});

				$("#signManagerQr").trigger("click");

				$('#ScanToSignModal').on('shown.bs.modal', function () {
					let fnaId = $("#hTxtFldCurrFNAId").val();
					getAllSign(fnaId);
					if (approvalscreen == "true") {
						$("#btnMgrSignToggle").trigger("click");
					}
				})

				$(".genqrbtn").hide()
			}
			//$("#"+txtFldId).val('');
		}

		if ((thisFldId == 'radFldackPartial') || (thisFldId == 'suprevMgrFlgD')) {
			if (thisFldId == 'suprevMgrFlgD') {
				$(radBtn).val("Y")
				$("#mngrDisagrReason").html("please state the Reasons and Follow-up Action for Disagree the option!");
				$("#mngrBtnErrorMsg").html("");
				$("#errorFld").css("border", "");
				$("#" + txtFldId).addClass("err-fld");
			}
			$("#" + txtFldId).removeAttr('disabled');
			$("#" + txtFldId).val('');
			$("#" + txtFldId).focus();
			$("#" + txtFldId).removeClass("err-fld");
		}

		if (txtFldId == "cdackAdvrecomm") {
			if (page_wise_err) {
				page_wise_err = false;
				saveData();
				setTimeout(function () {
					$("#btntoolbarsign").trigger("click");
				}, 1200);
			}
		}
	}
}

function chkOptionExists(selectid, value) {
	let exists = false;
	$('#' + selectid + '  option').each(function () {
		if (!isEmptyFld(value)) {
			if (this.value.toUpperCase() == value.toUpperCase()) {
				return exists = true;
			}
		}
	});
	return exists;
}

function removeOptionByVal(selectid, value) {

	let sourceSelect = document.getElementById(selectid);

	for (let opt = 0; opt < sourceSelect.options.length; opt++) {
		if (sourceSelect.options[opt].value.toUpperCase() == value.toUpperCase()) {
			sourceSelect.remove(opt);
		}
	}

	/*$('#'+selectid +' option[value='+value+']').each(function() {
	    $(this).remove();
	});*/
}

function showConfrmAfterSignChng(obj, nextscreen, navigateflg, infoflag) {
	Swal.fire({
		//  	  title: "Clear Signature",
		text: " By changing the data, you must capture sign again and redo the approval process!",
		icon: "info",
		showCancelButton: true,
		allowOutsideClick: false,
		allowEscapeKey: false,
		confirmButtonClass: "btn-danger",
		confirmButtonText: "OK",
		cancelButtonText: "No,Cancel",
		//  	  closeOnConfirm: false,
		//showLoaderOnConfirm: true
		//  	  closeOnCancel: false
	}).then((result) => {
		if (result.isConfirmed) {

			let fnaId = $("#hdnSessFnaId").val();

			// saveData($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);

			$.ajax({
				url: 'FnaSignature/clearByFnaId/' + fnaId,
				type: 'DELETE',
				// async:false,
				success: function (result) {

					$("#hdnFnaDetails").val(result);
					//						  doCurrentPageData(obj,nextscreen,navigateflg,infoflag);
					saveData($("#btnNextFormUniq"), $("#applNextScreen").val(), false, false);
					document.cookie = "managerApprovalHide=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
					

					setPageKeyInfoFail();
					setPageSignInfoFail();
					setPageManagerSts();
					/*setToolbarSignBtnFail();
				    
					setPageAdminSts();
					setPageComplSts();*/

					// return true;
					window.location.reload();
				},
				error: function (e) {
					Swal.fire({
						icon: 'error',
						//title: 'Something went wrong!',
						text: 'Please Try again Later or else Contact your System Administrator',
						//footer: '<a href>Please Contact Administrator?</a>'
					});
					return false;
				}
			});
		} else if (result.dismiss === Swal.DismissReason.cancel) {
			window.location.reload();
		}

	});

}

//clear all errmessages in keyInformation PopUp chat window
function removeAllErrorMsgInChat() {
	$("#ekycValidMessageList").children().remove();
}

function setPageManagerSts() {

	let fnaDetailsStr = $("#hdnFnaDetails").val();
	let fnaDetails = JSON.parse(fnaDetailsStr);

	let mgrkycSentStatus = $("#kycSentStatus").val();
	let mgrapproveStatus = $("#mgrApproveStatus").val();
	let mgrapprovestaff = $("#mgrapprstsByadvstf").val();
	let mgrapprovedate = $("#mgrApproveDate").val();

	let dynatext = " on <strong>" + mgrapprovedate + " </strong> by <strong> " + mgrapprovestaff + "</strong>";
	let dynatextSimple = " on " + mgrapprovedate + "   by   " + mgrapprovestaff;

	if (fnaDetails.mgrEmailSentFlg == "Y") {

		$("#btnAllPgeNoti").addClass("fa-check").removeClass("fa-remove");
		$("#btnAllPgeNoti").closest("button").addClass("btn-success").removeClass("btn-secondary");

		$("#btnSignStatus").addClass("fa-check").removeClass("fa-remove");
		$("#btnSignStatus").closest("button").addClass("btn-success").removeClass("btn-secondary");

		if (isEmptyFld(mgrapproveStatus)) {

			$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;This FNA already sent to manager and awaiting for approval.<br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
			$("#applValidMessage").removeClass("alert-default").addClass("alert-warning")

			$("#btnMngrApproval").addClass("fa-question").removeClass("fa-remove");
			$("#btnMngrApproval").closest("button").addClass("btn-info").removeClass("btn-danger").removeClass("btn-secondary");
			$("#hTxtFldSignedOrNot").val("Y");

		} else if (!isEmptyFld(mgrapproveStatus) && mgrapproveStatus == "APPROVE") {

			$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;This FNA approved by manager ' + dynatext + '</small>')
			$("#applValidMessage").removeClass("alert-default").addClass("alert-warning");

			$("#btnMngrApproval").addClass("fa-check").removeClass("fa-remove");
			$("#btnMngrApproval").closest("button").removeAttr("disabled");
			$("#btnMngrApproval").closest("button").addClass("btn-success").removeClass("btn-secondary").removeClass("btn-danger");
			$("#btnMngrApproval").attr("title", "Approved " + dynatextSimple);
			$("#hTxtFldSignedOrNot").val("Y");

		} else if (!isEmptyFld(mgrapproveStatus) && mgrapproveStatus == "REJECT") {

			$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;This FNA rejected by manager.<a class="helptextlink" onclick="showManagerMailConfirm()">CLICK HERE </a> to Resend Manager Approval!<br/><i class="fa fa-exclamation-triangle"></i>&nbsp;In case of any changes required in FNA Form, re-signature is required</small>')
			$("#applValidMessage").removeClass("alert-default").addClass("alert-warning");

			$("#btnMngrApproval").addClass("fa-remove").removeClass("fa-question").removeClass("fa-check");
			$("#btnMngrApproval").closest("button").removeAttr("disabled");
			$("#btnMngrApproval").closest("button").addClass("btn-danger").removeClass("btn-secondary").removeClass("btn-success");
			$("#btnMngrApproval").attr("title", "Rejected " + dynatextSimple);
			$("#hTxtFldSignedOrNot").val("Y");
		}
	} else {

		$("#btnMngrApproval").addClass("fa-remove").removeClass("fa-question");
		$("#btnMngrApproval").closest("button").removeClass("btn-success").addClass("btn-secondary").removeClass("btn-info");

		//$("#applValidMessage").html('<small><i class="fa fa-info-circle"></i>&nbsp;Signatures are captured. Adviser can send this FNA to <a style="cursor: pointer;color:blue" onclick="alert()">manager approval</a>!<br/></small>')
		//$("#applValidMessage").removeClass("alert-default").addClass("alert-secondary")
		$("#hTxtFldSignedOrNot").val("N");
	}
}

function addOption(selectid, optkey, optvalue) {
	$('#' + selectid)
		.append($("<option></option>")
			.attr("value", optkey)
			.text(optvalue));
}

$("#btnDelFnaDets").on("click", function () {
	Swal.fire({
		text: "Are you sure to delete the current FNA related details?",
		icon: "question",
		allowOutsideClick: false,
		allowEscapeKey: false,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, Delete'
	}).then((result) => {
		if (result.isConfirmed) {
			let fnaId = $("#hdnSessFnaId").val();
			$.ajax({
				type: "DELETE",
				url: "fnadetails/deleteAllDetailsByFnaId/" + fnaId,
				success: function (result) {
					if (result) {
						if (result == "true") {
							Swal.fire({
								icon: "success",
								text: "FNA related details are deleted!",
								allowOutsideClick: false,
								allowEscapeKey: false
							}).then((result) => {
								if (result.isConfirmed) {
									openSrchScreen();
								}
							});
						} else if (result == "false") {
							Swal.fire({
								icon: 'error',
								text: 'Please Try again Later or else Contact your System Administrator',
							});
						}
					}
				},
				error: function (err) {
					Swal.fire({
						icon: 'error',
						text: 'Please Try again Later or else Contact your System Administrator',
					});
				}
			});
		}
	});
});

function emailValidation(emailValue, field) {
	let validEmail = false;
	if (!isEmptyFld(emailValue)) {
		let regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		if (!regex.test(emailValue)) {
			$('#' + field + "Error").removeClass('hide').addClass('show');
			$('#' + field + "Error").html("Invalid Email Id Format");
			$('#' + field).addClass('err-fld');
		} else {
			$('#' + field + "Error").removeClass('show').addClass('hide');
			$('#' + field).removeClass('err-fld');
			validEmail = true;
		}
	}
	return validEmail;
}

var needValidation = false;
function validEmailId(obj, elem) {
	let email = $(obj).val();
	let validEmail = emailValidation(email, elem);
	if (!validEmail && page_wise_err) {
		if (email.length == 0) {
			$('#' + elem + "Error").removeClass('hide').addClass('show');
			$('#' + elem + "Error").html("Invalid Email Id Format");
			$('#' + elem).addClass('err-fld');
		}
		addErrorMsgToChat(elem, CONTACTDETAILS1, "kycHome");
		if (!needValidation) {
			needValidation = true;
			$(obj).blur();
		}
	}
}

function onNtucService(apiFor) {
	let fnaId = $("#hdnSessFnaId").val();
	let url = "Common/";
	let dsfStsName;
	
	switch(apiFor) {
		case "retrievePolicy":
			url += "ntucRetrievePolicy/";
			break;
		case "retrieveFileInfo":
			url += "ntucRetrieveFileInfo/";
			break;
		case "dsfUpdateSts":
			url += "ntucDsfUpdateSts/";
			dsfStsName = $("#selFldNtucSts").val();
			break;
	}
	
	url += fnaId;
	
	let fnaIdWithCaseId = $("#hdnCaseId").val();
	if (fnaIdWithCaseId && fnaIdWithCaseId !== "null" && fnaIdWithCaseId.indexOf(fnaId) > -1) {
		let caseId = fnaIdWithCaseId.split("_")[1];
		url += "?caseId=" + caseId;
	}
	
	if (dsfStsName && fnaIdWithCaseId && fnaIdWithCaseId !== "null") {
		url += "&dsfStsName=" + dsfStsName;
	} else if (dsfStsName && (!fnaIdWithCaseId || fnaIdWithCaseId === "null")) {
		url += "?dsfStsName=" + dsfStsName;
	}
	
	$.ajax({
		type: "GET",
		url: url,
		dataType: "json",
		success: function(result) {
			console.log(result);
			// alert(JSON.stringify(result));
			if (result) {
				if (result.status === "success" || result.status === "info") {
					if (result.message) {
						Swal.fire({
							icon: result.status,
							text: result.message
						});
					} else {
						if (result.docList && result.docList.length > 0) {
							$("#btnviewDoc").html('You have <span class="badge badge-warning badge-pill">' + result.docList.length + '</span> Doc(s). Click to view');
							$("#btnviewDoc").removeClass("hide").addClass("show");
							$('#noNtucDataMsg').removeClass("show").addClass("hide");
							$("#dynaAttachList").addClass("show").removeClass("hide");
		             
							let docList = result.docList;
							// let count = 1;
							// for (let docIndex = 0; docIndex < docList.length; docIndex++) {
							$.each(docList, function(i, doc){
								// downloadNtucDoc(docList[docIndex].document, docList[docIndex].doc_filetype, "file_" + count++);
								populatefnaNtucDocs(doc, "file_" + ++i);
							});
						} else {
							$("#btnviewDoc").removeClass("show").addClass("hide");
							$('#noNtucDataMsg').removeClass("hide").addClass("show");
							$("#dynaAttachList").addClass("hide").removeClass("show");
						}
					}
				} else if (result.status === "failed" && result.reason) {
					Swal.fire({
						icon: "error",
						text: result.reason
					});
				}
			}
		},
		error: function(errorRes) {
			console.log(errorRes);
			alert(JSON.stringify(errorRes));
			Swal.fire({
				icon: "error",
				text: 'Please Try again Later or else Contact your System Administrator'
			});
		}
	});
}

function populatefnaNtucDocs(doc) {

    let docId = doc.docId;
	let docTitle = doc.fileDesc;
	let docName = doc.fileName;
	let docType = doc.fileType;
	let docSize = doc.fileSize;
	let docContent = doc.fileContent;
    
    let strList = '<a href="#" class="list-group-item list-group-item-action p-1" id="' + docId + '">'+
            '<div class="row">' +
               '<div class="col-12"><small><i class="fa fa-download cursor-pointer right mr-2 mt-1" style="color: #007bff;" onclick=downloadNtucDoc(\"' + docContent + '\",\"' + docType + '\",\"' + docName + '\") title="Click to Download Doc."></i></small></div>' +
               '<div class="col-12 mt-2"><h6 class="mb-1 font-sz-level7 text-custom-color-gp bold"><span class="text-dark">Doc Title : </span> ' + docTitle + '</h6></div>' +
            '</div>' +
        
             '<div class="row">' +
                 '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Name : </span> ' + docName + '</small></div>' +
                 '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Size : </span> ' + docSize + '</small></div>' +
                 '<div class="col-12"><small class="font-sz-level8"><span class="text-dark bold">File Type : </span> ' + docType + '</small> </div>' +
             '</div>' +
      '</a>';
    
     $("#dynaAttachList").append(strList);
     $('#noNtucDataMsg').removeClass("show").addClass("hide");
    
}

function downloadNtucDoc(docData, fileType, fileName) {
	let arrrayBuffer = base64ToArrayBuffer(docData);
	let blob = new Blob([arrrayBuffer], { type: "application/" + fileType });
	let link = window.URL.createObjectURL(blob);

	let a = document.createElement("a");
	document.body.appendChild(a);
	a.style = "display: none";
	a.href = link;
	a.download = fileName + "." + fileType;
	// a.click();
}

function base64ToArrayBuffer(base64) {
	let binaryString = window.atob(base64);
	let binaryLen = binaryString.length;
	let bytes = new Uint8Array(binaryLen);
	for (let binaryIdx = 0; binaryIdx < binaryLen; binaryIdx++) {
		let ascii = binaryString.charCodeAt(binaryIdx);
		bytes[binaryIdx] = ascii;
	}
	return bytes;
}

function onCaseInfo() {
	let fnaId = $("#hdnSessFnaId").val();
	$.ajax({
		type: "GET",
		url: "Common/getCaseInfoByFnaId/" + fnaId,
		success: function(result) {
			console.log(result);
			// alert(result);
			if (result) {
				let parsedResult = JSON.parse(result);
				if (parsedResult) {
					if (parsedResult.status === "success" && parsedResult.result) {
						let caseResult = JSON.parse(parsedResult.result);
						if (caseResult) {
							if (caseResult.caseId) {
								$("#spanCaseId").text(caseResult.caseId);
							} else {
								$("#spanCaseId").text("--NIL--");
							}
	                
							if(caseResult.caseStatus) {
								let caseClass="";
								let caseStatus = caseResult.caseStatus;
								switch(caseResult.caseStatus){
									case "In Progress":
										caseClass="fa fa-question-circle-o text-primary"
										break;
										
									case "Completed":
										caseClass = "fa fa-check text-success"
										break;
										
									case "Submitted":
										caseClass = "fa fa-check-circle text-success"
										break;
										
									case "Approved":
										caseClass = "fa fa-check-square-o text-success"
										break;
										
									case "Rejected":
										caseClass = "fa fa-times-circle-o text-danger"
										break;
										
									case "Cancelled":
										caseClass = "fa fa-ban text-warning"
										break;
										
									default:
										caseClass = "";
										caseStatus = "--NIL--";
										break;
								}
								
								let strCase = '<i class="'+caseClass+' fa-2x" id="caseStsClass" aria-hidden="true"></i>&nbsp;-&nbsp;' + caseStatus + '';
								$("#spanCaseStatus").html(strCase);
								if (caseStatus !== "--NIL--") {
									$("#selFldNtucSts").val(caseStatus);
								}
							} else {
								$("#spanCaseStatus").text("--NIL--");
							}
						}
					} else if (parsedResult.status === "failed" && parsedResult.reason) {
						$("#spanCaseId").text("--NIL--");
						$("#spanCaseStatus").text("--NIL--");
						// alert(JSON.stringify(parsedResult));
					}
				}
			}
		},
		error: function(errorRes) {
			console.log(errorRes);
			alert(errorRes);
		}
	});
}

function createAvivaPolicy(payload) {

	// var payload = "7Baz6PxDmqHHNkqkVQk2MxeW3daw/zSdsHtgwqj+VewK5w5fbqFroFY3w1tAMgO1xXhYDzAjkNF1tz/XIuydrcVU8tCIQixFg+auARdeDqmN9RxiSGidabYukiY3DyUTBd9hIgnXmj734izlLCTKmvXG8+PXek1n6HU1iFVHBUCoT/Wyzx0E347R7tN9q8aFGFmF89rO5bEuxZWcmVmHGluqU7jjCUAwigOeHO4vtMF7fETs61HaV4RwXOFEl502BHdIcu2Xq6eu3h7uwvzbxaB/DrYWu3lSmfQ1fXDO46wqy/RYEOAgl7BtG5t1SdfhEBtoHY9V7ffaz6aN+kS21mLSRE16R2w8MyC8WSC2hp5EWW6/xM1zGoeMcHa8Dh4kfTGF44zQYVTp/dvjOOfXZ1fEKrITvNypJKwxW4OFEbLw0rH81ky8vHZ0AvrNekVO3d7c72GFAghNgysPil/p0KxofvJWZtUPLPKwQVBc9ODMel4MrWOVD37AnH7vNnButKENo5Y9d2+y90cU9WujnkuT113SuVnhAPp4dq5kERNHv5/ysiNMnkOhAZy/8tJMIEVBCrlnBd7sNZMh1jeyf8GGJo1ScFjJ+PyDhEnOx4YOfrPMh9kXDMTwoxs7RK/rN/7nPJTiSaNT4CV/Bbcp3QAOMXuDBMGTKRrgmPmVtH9x4AiAosO6CLKeOqHxoAtVnIJk++sz59o3CZty5/4cuZm9bJ0bcP/YhHTYuI2qSszBFse7+YYe5eT2FK2g4pSN";
	// var payload = "jjHKd5oPK6T4B1oY/h/qi8ksj/EjsaCRmot5nItqD6ZFds4ol6oSMXutiJ7gQXODyNw28EUx60ssCGQtR4n7jrmrxWZXEyKbuSrPiiLmD9IKMveaxF+CFgp5FLDXjXnSnzqgT0OD+xyieb5hflfht9lrGDglc7NUJnF2mG1zC8v25yryftHMTsuW0fOYV9asFXqMSLtoX503eiyQ+97VYDioK/ejVhzYQTWPv0Ui01M0JgMBkOQAWR0NCahL7B3Sxblb8uTklSKz4iGBbO/B1Z/lwUs42sFt6MI5KqUW2YiQjq2nMreofkYs+LYX43OARoftTQb3m5fbZEifbnnE8/E8BAknvXHyzTeUrkdLqmfr4v5cpspDrHEyrX6d3n4gb6xaYW/FJixl9ihWqypIICMmYt95pHr3BaoQM6lK2a5La3eOGSi34JoXeHAjUFwpTrVsKBqKrUHe7HQF0fA+GHgYWhqg7Ka0SBEKWpehEgmG1sFuxYM/Ub7xMeg5eA0jYcm0tlOg/9WySGRVSh0QHQBBAtBi8apT5OcUi96NcL7Yxs7biVS6bZfjfYKc6vXmrmLyZSzuPH1WRLWiPvYXBygNy5vxhJ93qiG9HAgz6JV0uNcEXeqOXu32Zd6Rg7lEQ295sHVuDQ3ctbRObm5ZjMk/DZ8WMgcmwcLzF2EhJ+BytceSQhvHbcR9NG6AkXDOcI0tH3WA53A13hEwKE5NoNcLBHkki/8AD0BxkBr9s22+ZucoxEO1gD1KMFCf2HGfhlMbv/XxCkijQCQ32nS2ECHQo6EaI5nxyRLB+WwsYK4hEX4Zr1j9il81LWW8LHAHjQ1R2NFx6lcChFKfMaZcLvcXBUbEOmI/KjUKFf6K1ZzJ4yCl0N0evYbGOswbH/8zJKufjIAJzrP0m1FSwX7NCLpzM1RQsTXYBpTonmW63OpoK194/BMayromqvwfFWSvtRzj2ksZZ5h7lxFDbWquHLT+K7MCNu70//I74uKtLEi+zzI0RDfd50o0xSXfmwRRIFybTYebQkBnFOug5z6TKA==";
	// ${urlScheme}://?moduleId=${moduleId}&fnaId=${fnaId}&selectType=${selectType}&json=${encrypted}
	// var url = "ezsub-uat://?moduleId=${QT}&fnaId=${FNA00000000000006436}&selectType=${selectType}&json=${"+payload+"}";
	// var url = "ezsub-uat://?moduleId=QT&fnaId=FNA00000000000006436&selectType=a&json="+payload;
	
	var url = "ezsub-uat://?json=" + payload;
	// alert(url)
	window.open(url,"_blank");
	/*$('#cover-spin').show();
	$.ajax({
		type: "GET",
		url: "Common/openSafari",
		complete: function() {
			$('#cover-spin').hide();
		},
		success: function(result) {
			console.log("opening safari is: " + result);
		},
		error: function(errorRes) {
			$('#cover-spin').hide();
			console.log(errorRes);
			alert("opening safari is " + errorRes);
		}
	});*/
}

// Testing start
function encrypt(key, data) {
	const iv = CryptoJS.lib.WordArray.random(16);
	
	const encrypted = CryptoJS.AES.encrypt(
		data,
		CryptoJS.enc.Base64.parse(key),
		{ iv }
	);
	
	const ivString = iv.toString(CryptoJS.enc.Base64);
	const encryptedString = encrypted.toString();
	
	return btoa(atob(ivString) + atob(encryptedString));
}

function decrypt(key, data) {
	const aes_key = CryptoJS.enc.Base64.parse(key);
	const raw = atob(data);
	
	const ivLength = 16;
	const iv = raw.slice(0, ivLength);
	const encrypted = raw.slice(ivLength);
	
	return CryptoJS.AES.decrypt(
		btoa(encrypted),
		aes_key,
		{
			iv: CryptoJS.enc.Base64.parse(
				btoa(iv)
			)
		}
	).toString(CryptoJS.enc.Utf8);
}
// Testing end