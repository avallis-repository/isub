var strGlblUsrPrvlg = "";

var strFnaMgrUnAppDets = "";
var STR_FNA_MGRAPPRSTS_LIST = "APPROVE^REJECT";
var strAdminFNADets = "";
var strComplianceFNADets = "";
var strLoggedUserEmailId = "";
var archiveflg = false;
var baseUrl;
var currDate = "";
var strMngrFlg = "", strAdminFlg = "", strCompFlg = "";
var strALLFNAIds = "";

var COMMON_SESS_VALS = "";
var LOGGED_DISTID = "";//,FNA_RPT_FILELOC="",FNA_PDFGENLOC="",FNA_FORMTYPE="";
var strLoggedAdvStfId = "";
var strLoggedUserId = "";
var ntuc_policy_access_flag = false, ntuc_sftp_acs_flg = false;
var isFromMenu = "";

var pendingdatatable, approveddatatable, rejecteddatatable;
var acsmode = ""
var jsnDataFnaDetails = "";
var mgrmailsentflag = false;

function callBodyInit() {

	for (let sess in COMMON_SESS_VALS) {

		let sessDets = COMMON_SESS_VALS[sess];
		for (let dets in sessDets) {
			if (sessDets.hasOwnProperty(dets)) {
				let sesskey = dets;
				let sessvalue = sessDets[dets];

				if (sesskey == "LOGGED_DISTID") { LOGGED_DISTID = sessvalue; }

				if (sesskey == "LOGGED_USER_EMAIL_ID") { strLoggedUserEmailId = sessvalue; }

				if (sesskey == "TODAY_DATE") { currDate = sessvalue; }

				if (sesskey == "MANAGER_ACCESSFLG") { strMngrFlg = sessvalue; }

				if (sesskey == "LOGGED_ADVSTFID") { strLoggedAdvStfId = sessvalue; }

				if (sesskey == "LOGGED_USERID") { strLoggedUserId = sessvalue; }//|| sessKey == "STR_LOGGEDUSER" || || sessKey == "LOGGED_USER"

				if (sesskey == "RPT_FILE_LOC") { FNA_RPT_FILELOC = sessvalue; }

				if (sesskey == "ADMIN_ACCESSFLG") { strAdminFlg = sessvalue; }

				if (sesskey == "COMPLIANCE_ACCESSFLG") { strCompFlg = sessvalue; }

				if (sesskey == "NTUC_POLICY_ACCESS") { ntuc_policy_access_flag = sessvalue; }

				if (sesskey == "NTUC_SFTP_ACCESS") { ntuc_sftp_acs_flg = sessvalue; }
			}
		}
	}

	strMngrFlg = "Y"
	openList('PENDING');

	pendingdatatable = $('#tblPending').DataTable(
		{
			scrollX: true,
			scrollY: "800px",
			scroller: true,
			scrollCollapse: true,
			autoWidth: false,
			"columnDefs": [{ "targets": [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], "visible": false, "searchable": false }]
		}
	);

	approveddatatable = $('#tblApproved').DataTable(
		{
			scrollX: true,
			scrollY: "800px",
			scroller: true,
			scrollCollapse: true,
			autoWidth: false,
			"columnDefs": [{ "targets": [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], "visible": false, "searchable": false }]
		}
	);

	rejecteddatatable = $('#tblRejected').DataTable(
		{
			scrollX: true,
			scrollY: "800px",
			scroller: true,
			scrollCollapse: true,
			autoWidth: false,
			"columnDefs": [{ "targets": [6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21], "visible": false, "searchable": false }]
		}
	);

	if (isFromMenu == true) {
		$("#landingpage").find("li:eq(0) a").trigger("click");
	} else {
		$("#landingpage").find("li:eq(1) a").trigger("click");
		// $('#cover-spin').hide(0);
	}

	if (strALLFNAIds != null) {
		loadALLFNAId();
	}

	if (strMngrFlg != "Y") {
		$("#appr-mgr-tab").hide();
		$("#appr-mgr-tab-cont").hide();
	}

	if (strAdminFlg != "Y") {
		$("#appr-admin-tab").hide();
		$("#appr-admin-tab-cont").hide();
	}

	if (strCompFlg != "Y") {
		$("#appr-comp-tab").hide();
		$("#appr-comp-tab-cont").hide();
	}

	$("#spanCrtdUser").text(strLoggedUserId);

	if (acsmode != "MANAGER") {
		$("#btnQRCodesignMgr").hide();
	}

	createPie(".pieID.legend", ".pieID.pie");

	if (!isEmpty(jsnDataFnaDetails) && !isJsonObjEmpty(jsnDataFnaDetails)) {
		for (let obj in jsnDataFnaDetails) {
			if (jsnDataFnaDetails.hasOwnProperty(obj)) {
				let objValue = jsnDataFnaDetails[obj];
				//				console.log(obj,objValue,"--------------")
				switch (obj) {
					case "adviser_name":
						$(".adviserNameTab").text(objValue);
						break;

					case "mgrEmailSentFlg":
						if (!isEmpty(objValue) && objValue == "Y" && strMngrFlg == "Y") {
							mgrmailsentflag = true;
						} else {
							mgrmailsentflag = false;
						}
						break;

					case "suprevMgrFlg":
						if (objValue == "agree") {
							$("#suprevMgrFlgA").prop("checked", true);
						} else if (objValue == "disagree") {
							$("#suprevMgrFlgD").prop("checked", true);
						} else {
							$("#suprevMgrFlgA").prop("checked", false);
							$("#suprevMgrFlgD").prop("checked", false);
						}

						if (objValue == "agree") {
							$("#diManagerSection").find(":input").prop("disabled", true);
							$("#diManagerSection").addClass("disabledsec");
						}

					case "mgrName":
						//						No action
						break;

					default:
						$("#" + obj).val(objValue);
				}
			}
		}
	}

	let fnaId = $("#hdnSessfnaId").val();
	getAllSign(fnaId);

	if (!mgrmailsentflag) {
		//		alert("")

		Swal.fire({
			width: "650px",
			title: "eKYC Notification",
			html: "This FNA is not yet ready for approval. Please contact the adviser for more details",
			icon: "info",
			//	  	  showCancelButton: true,
			allowOutsideClick: false,
			allowEscapeKey: false,
			confirmButtonClass: "btn-danger",
			confirmButtonText: "OK",
			//	  	  cancelButtonText: "No,Cancel",
			//	  	  closeOnConfirm: false,
			//showLoaderOnConfirm: true
			//	  	  closeOnCancel: false
		}).then((result) => {
			if (result.isConfirmed) {
				Swal.close();
				$("#landingpage").find("li:eq(0) a").trigger("click");
			}
		});
	}
}

function loaderBlock() {
	/*var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");
	
	loadMsg.style.display="block";
	//alert(loadMsg.style.display)
	loadingLayer.style.display="block"; */
}

$("#imgPolicyDocs").on("click", function () {
	downloadAndOpenFile(_selectedFnaid, true);
});

function downLoadAllFile(fnaId, msgflag) {
	downloadAndOpenFile(fnaId);
}

function downloadAndOpenFile(hrefobj, msgflag) {

	let fnaid = hrefobj;

	$.ajax({
		type: "POST",
		url: "KycUtilsServlet",
		data: { "dbcall": "FETCHNTUCDOCUMENT", "strFnaId": fnaid },
		dataType: "json",
		async: false,
		beforeSend: function () {
			$("#loadingLayer").show();
		},
		success: function (data) {
			let jsnResponse = data;

			if (jsnResponse[0].SESSION_EXPIRY == 'SESSION_EXPIRY') {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}

			if (!jsnResponse[0].NO_RECORDS) {
				// poovathi add on 17-03-2020
				let strNoRecords = "No Document found.";
				$("#dynaAttachNTUCList").append(strNoRecords);
				if (msgflag) {
					alert("No Document found.")
				}
				return;
			}

			/*ModalPopups.polAttachmentUWPopup("POL_UW_ATTACH", {width : 900,height : 490,onOk : "closeAttachDialog()"});
			$("#POL_UW_ATTACH_okButton").val("CLOSE");
			 */
			$.each(jsnResponse[0].CLIENT_DOCUMENT, function (i, obj) {
				loadDocJsnDataNew(obj, fnaid);
			});
		},
		complete: function () {
			$("#loadingMessage").hide();
		},
		error: function () {
			$("#loadingMessage").hide();
		}
	});
}

//added by kavi 21/09/2018
function ModalPopupsCancelKycDoc(selObj) {

	let loadMsg = document.getElementById("loadingMessage");
	let loadingLayer = document.getElementById("loadingLayer");
	loadMsg.style.display = "none";

	selObj.value = "";
	ModalPopups.Cancel("KycApprovalDoc");
}

function kycCompApproveDocBinary() {

	let parameter = "dbcall=KYCDOCTOBINARYDATA";

	let form = $('#hdnFormKycApprDoc')[0];
	let data = new FormData(form);

	$.ajax({
		url: baseUrl + "/KycUtilsServlet?" + parameter,
		type: "POST",
		data: data,
		cache: false,
		enctype: 'multipart/form-data',
		contentType: false,
		processData: false
	}).done(function (response) { //
		let saveResp = $.parseJSON(response);

		if (saveResp[0].SESSION_EXPIRY == 'SESSION_EXPIRY') {
			window.location = baseUrl + SESSION_EXP_JSP;
			return;
		}

		if (saveResp[0].STATUS == 'SUCCESS') {
			alert(saveResp[0].BINARYDATA);
			ModalPopupsCancelKycDoc();
		}

		if (saveResp[0].STATUS == 'FAIL') {
			alert("Failed to convert to base64.");
		}
	});

	ModalPopupsCancelKycDoc();
}

function closeAttachDialog() {
	document.getElementById("uwattachjsppage").appendChild(document.getElementById("polAttachmentJspUW"));
	$("#KycAttachTbl tbody tr").remove();
	ModalPopups.Cancel("POL_UW_ATTACH");
	return true;
}

function downLoadNtucAttach(ntid) {
	let url = 'DownloadNtucFile?i=' + ntid;
	window.open(url, "_blank");
}

function DownLoadAllNtucFile() {
	let url = 'DownloadNtucZipFile.action?txtFldFnaPolId=' + $("[name=hdnTxtFldNtucFnaId]").val();
	window.open(url, "_blank");
	//	window.location.href =url;
}

function reorderTblById(id) {
	let count = 1;
	$("#" + id + " tbody tr").each(function () {
		$(this).find("td:first input:first").val(count);
		count++;
	});
}

function ajaxCall(parameter) {

	let jsnResponse = "";

	$.ajax({
		type: "POST",
		url: baseUrl + "/KYCServlet",
		data: parameter,
		dataType: "json",

		async: false,

		beforeSend: function () {
		},

		success: function (data) {
			jsnResponse = data;
		},
		complete: function () {
		},
		error: function () {
		}
	});

	return jsnResponse;
}

function openEKYCNew(dataset) {

	let kycformname = "SIMPLIFIED";
	let lobArrtoKYC = "ALL";
	let strLoggedUserDesig = "";
	let strLoggedDistrbutor = "";
	//var emailId = dataset[0]["txtFldAdvStfEmailId"]; 
	let formType = "SIMPLIFIED";

	let fnaId = dataset[1];
	$("#txtFnaId").text(fnaId)
	$("#txtFldCurFnaId").val(fnaId)

	getLatestFnaIdDetls(fnaId)

	let custname = dataset[2].toUpperCase();
	//$("#txtFnaId").text(fnaId)

	$("#optgrpPending").append('<option value="' + fnaId + '">' + custname + '</option>');
	$('#dropDownQuickLinkFNAHist').val(custname);
	$('#dropDownQuickLinkFNAHist').trigger('change.select2');

	let custid = dataset[6];
	let advId = dataset[7];
	let emailId = dataset[8];

	let machine = "";
	machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC //
		+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaId + "&__format=pdf"
		+ "&P_PRDTTYPE=" + lobArrtoKYC
		+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + kycformname
		+ "&P_DISTID=" + LOGGED_DISTID;
	setTimeout(function () {
		$('#cover-spin').show(0);
		$("#dynaFrame").attr("src", machine);
	}, 0);

	let iframe = document.getElementById('dynaFrame');
	iframe.onload = function () { $('#cover-spin').hide(0); };

	//setTimeout(function(){$('#cover-spin').hide(0);	}, 1500);

}

function formatDataSelect2(data) {
	if (!data.id) {
		return data.text;
	}
	let $format = $('<span>' + data.text + '</span>');
	return $format;
}

function setManagerStatus(currentObj) {
	let thisval = $(currentObj).val();

	let agree_m = "", disagree_m = "", reason_m = "", status_m = "";
	agreeordis = thisval;
	reason_m = $("#txtFldMgrApproveRemarks").val();

	status_m = thisval;//agree_m ? "APPROVE" : (disagree_m ? "REJECT" : "");

	if (isEmpty(status_m)) {
		alert(FNA_APPR_MGR_AGREE);
		$(currentObj).focus();
		//$("#tr_mgr_agreesec").css({"border":"1px solid red"});
		//$("input[name='suprevMgrFlg']").closest("td").css({"background-color":"yellow"});
		return false;
	}

	if (!isEmpty(status_m) && status_m == "REJECT") {
		if (isEmpty(reason_m)) {
			alert(MANAGER_KEYIN_REMARKS);
			$(currobj).val("")
			return false;
		}
	}
	//if(flag){

	if (!isEmpty(status_m) && status_m == "APPROVE") {
		let conf = window.confirm(CONFIRM_APPROVE_MANAGER);
		if (!conf) {
			//	var loadMsg = document.getElementById("loadingMessage");
			//var loadingLayer = document.getElementById("loadingLayer");
			//loadMsg.style.display="none";
			//					selObj.value = "";

			//agreefld.prop("checked",false);
			//	disagreefld.prop("checked",false);
			$(currentObj).val("");

			return false;
		} else {
			//agreefld.prop("checked",true).prop("disabled",true);
			//disagreefld.prop("checked",false).prop("disabled",true);

		}
	}

	if (!isEmpty(status_m) && status_m == "REJECT") {
		let conf = window.confirm(CONFIRM_REJECT_MANAGER);
		if (!conf) {
			//var loadMsg = document.getElementById("loadingMessage");
			//var loadingLayer = document.getElementById("loadingLayer");
			//loadMsg.style.display="none";
			//agreefld.prop("checked",false);
			//disagreefld.prop("checked",false);
			//					selObj.value = "";
			$(currentObj).val("");
			return false;
		}
	}

	//}

	let mangrEmailId = "";//$("#txtFldAdvMgrEmailId").val()
	let mangrId = "";// $("#txtFldMgrId").val()
	let hNTUCPolicyId = "";//$("#ntucCaseId").val();
	let polNo = "";

	let advstfEmailId = "";//$("#txtFldAdvEmailId").val();			            	  					
	let advName = "";// $("#txtFldAdvName").val();
	let custName = "";//$("#dfSelfName").val();

	let approverAdvStfId = "";
	let approverUserId = "";//$("#hTxtFldFnaLoggedUserId").val();
	let strLoggedUserEmailId = "";
	let fnaPrin = "";
	let fnaId = $("#txtFldFnaId").val();

	let ajaxParam = "dbcall=MANAGER_APPROVE_STATUS"
		+ "&txtFldFnaId=" + fnaId

		+ "&txtFldMgrAppStatus=" + status_m
		+ "&txtFldManagerEmailId=" + mangrEmailId
		+ "&txtFldManagerId=" + mangrId
		+ "&txtFldManagerRemarks=" + reason_m

		+ "&hNTUCPolicyId=" + hNTUCPolicyId
		+ "&txtFldPolNum=" + polNo

		+ "&txtFldAdvEmailId=" + advstfEmailId
		+ "&txtFldAdviserName=" + advName
		+ "&txtFldCustName=" + custName

		+ "&txtFldApprAdvId=" + approverAdvStfId
		+ "&txtFldApprUserId=" + approverUserId
		+ "&txtFldApprUserEmailId=" + strLoggedUserEmailId
		+ "&txtFldFnaPrin=" + fnaPrin
		;

	setTimeout(function () {
		window.parent.document.getElementById('cover-spin').style.display = "block";
	}, 0);
	setTimeout(function () {
		leletjaxResponse = callAjax(ajaxParam, false);
		window.parent.document.getElementById('cover-spin').style.display = "block";
		retval = jsonResponse = eval('(' + ajaxResponse + ')');

		for (let val in retval) {

			let tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for (let tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					let value = tabdets[tab];

					if (tab == "TAB_MANAGER_APPROVE_DETAILS") {

						//alert(value[0].MANAGER_POLICY_STATUS);

						Swal.fire({
							//						  	  title: "Clear Signature",
							html: value[0].MANAGER_POLICY_STATUS,
							icon: "info",
							//						  	  showCancelButton: true,
							allowOutsideClick: false,
							allowEscapeKey: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "OK",
							//						  	  cancelButtonText: "No,Cancel",
							//						  	  closeOnConfirm: false,
							//showLoaderOnConfirm: true
							//						  	  closeOnCancel: false
						}).then((result) => {
							if (result.isConfirmed) {
								parent.location.reload(true);
							}
						});
						//							 location.reload(true);
					}
				}
			}
		}
	}, 1000);
}

function setAdminStatus(currobj) {

	let thisval = $(currobj).val().toUpperCase();

	if (!isEmpty(thisval)) {
		let fnaId = $("#txtFldFnaId").val();
		let mangrEmailId = "";//strLoggedUserEmailId;
		let remarks = $("#txtFldAdminApproveRemarks").val();
		let hNTUCPolicyId = $("#txtFldNTUCCaseId").val();
		let polNo = $("#txtFldNTUCPolNo").val();
		let advName = $("#txtFldAdvStfName").val();
		let custName = $("#txtFldCustName").val();
		let custId = $("#txtFldCustId").val();
		let advstfEmailId = $("#txtFldAdvEmailId").val();

		let mgrApproveSts = $("#txtFldMgrApproveStatus").val();

		if (mgrApproveSts != "APPROVE") {

			Swal.fire({
				width: "650px",
				title: "eKYC Notification",
				html: "Manager is not yet approved this FNA Details!. Once manager approved then admin can proceed to approval process!",
				icon: "error",
				//			  	  showCancelButton: true,
				allowOutsideClick: false,
				allowEscapeKey: false,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "OK",
				//			  	  cancelButtonText: "No,Cancel",
				//			  	  closeOnConfirm: false,
				//showLoaderOnConfirm: true
				//			  	  closeOnCancel: false
			}).then((result) => {
				if (result.isConfirmed) {
					Swal.close();
				}
			});
			$(currobj).val("");
			return false;
		}

		let approverAdvStfId = ""
		let approverUserId = ""

		let fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";

		if (thisval == "REJECT" && isEmpty(remarks)) {

			alert(ADMIN_KEYIN_REMARKS);

			$("#txtFldAdminApproveRemarks").prop("readOnly", false);
			$("#txtFldAdminApproveRemarks").focus();
			$(currobj).val("")
			return false;
		}

		if (thisval == "REJECT") {
			let conf = window.confirm(ADMIN_REJECTSTATUS_CONFIRM);
			if (!conf) {
				$(currobj).val("")
				return false;
			}
		} else if (thisval == "APPROVE") {
			let conf = window.confirm(ADMIN_APPROVESTATUS_CONFIRM);
			if (!conf) {
				$(currobj).val("")
				return false;
			}
		}

		let ajaxParam = 'dbcall=ADMIN_APPROVE_STATUS&txtFldFnaId=' + fnaId + "&txtFldAdminAppStatus=" + thisval +
			"&txtFldManagerEmailId=" + mangrEmailId + "&txtFldAdminRemarks=" + remarks + "&hNTUCPolicyId=" + hNTUCPolicyId + "&txtFldPolNum=" + polNo +
			"&txtFldAdviserName=" + advName + "&txtFldCustName=" + custName + "&txtFldAdvEmailId=" + advstfEmailId
			+ "&txtFldApprAdvStfId=" + approverAdvStfId + "&txtFldApprUserId=" + approverUserId + "&txtFldFnaPrin=" + fnaPrin + "&txtFldCustId=" + custId;

		setTimeout(function () {
			document.getElementById('cover-spin').style.display = "block";
		}, 0);

		setTimeout(function () {
			let ajaxResponse = callAjax(ajaxParam, false);
			retval = jsonResponse = eval('(' + ajaxResponse + ')');

			for (let val in retval) {
				let tabdets = retval[val];
				if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				}
				for (let tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						let value = tabdets[tab];

						if (tab == "TAB_ADMIN_APPROVE_DETAILS") {
							if (value[0].ADMIN_APPROVE_STATUS == "APPROVE") {
								if (strAdminFlg == "Y" && strCompFlg == "Y") {

								}
							}

							if (value[0].ADMIN_APPROVE_STATUS == "APPROVE") {
								//									document.getElementById("txtFldAdminApproveStatus").options[0].innerHTML = "Approved";
							}
							if (value[0].ADMIN_APPROVE_STATUS == "REJECT") {
								//									document.getElementById("txtFldAdminApproveStatus").options[1].innerHTML = "Rejected";
							}

							//								alert(value[0].ADMIN_POLICY_STATUS);
							//								window.location.reload(true);

							$('#cover-spin').hide(0);
							Swal.fire({
								title: "eKYC Notification",
								html: value[0].ADMIN_POLICY_STATUS,
								icon: "info",
								//								  	  showCancelButton: true,
								allowOutsideClick: false,
								allowEscapeKey: false,
								confirmButtonClass: "btn-danger",
								confirmButtonText: "OK",
								//								  	  cancelButtonText: "No,Cancel",
								//								  	  closeOnConfirm: false,
								//showLoaderOnConfirm: true
								//								  	  closeOnCancel: false
							}).then((result) => {
								if (result.isConfirmed) {
									parent.location.reload(true);
								}
							});
						}
					}
				}
			}
		}, 1000);
	}
}

function setComplianceStatus(currobj) {

	let thisval = $(currobj).val();
	if (!isEmpty(thisval)) {
		let fnaId = $("#txtFldFnaId").val()
		let mangrEmailId = strLoggedUserEmailId;
		let remarks = $("#txtFldCompApproveRemarks").val();
		let hNTUCPolicyId = $("#txtFldNTUCCaseId").val(),
			ntucCaseId = $("#txtFldNTUCCaseId").val();
		let polNo = $("#txtFldNTUCPolNo").val();
		let advName = $("#txtFldAdvStfName").val();
		let advstfId = "";
		let custName = $("#txtFldCustName").val();
		let custId = $("#txtFldCustId").val();
		let custNRIC = "";
		let advstfEmailId = $("#txtFldAdvEmailId").val();

		let approverAdvStfId = "";
		let approverUserId = "";
		let advstfcode = "";

		let ntucCasests = "";
		let ntucPolCrtdFlg = "";

		let mgrApproveSts = $("#txtFldMgrApproveStatus").val();
		let adminApproveSts = $("#txtFldAdminApproveStatus").val();

		if (mgrApproveSts != "APPROVE") {

			Swal.fire({
				width: "650px",
				title: "eKYC Notification",
				html: "Manager is not yet approved this FNA Details!. Once manager approved then admin can proceed to approval process!",
				icon: "error",
				//			  	  showCancelButton: true,
				allowOutsideClick: false,
				allowEscapeKey: false,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "OK",
				//			  	  cancelButtonText: "No,Cancel",
				//			  	  closeOnConfirm: false,
				//showLoaderOnConfirm: true
				//			  	  closeOnCancel: false
			}).then((result) => {
				if (result.isConfirmed) {
					Swal.close();
				}
			});
			$(currobj).val("");
			return false;
		}

		if (adminApproveSts != "APPROVE") {
			Swal.fire({
				width: "650px",
				title: "eKYC Notification",
				html: "Admin is not yet approved this FNA Details!. Once Admin approved then Compliance can proceed to approval process!",

				icon: "error",
				//			  	  showCancelButton: true,
				allowOutsideClick: false,
				allowEscapeKey: false,
				confirmButtonClass: "btn-danger",
				confirmButtonText: "OK",
				//			  	  cancelButtonText: "No,Cancel",
				//			  	  closeOnConfirm: false,
				//showLoaderOnConfirm: true
				//			  	  closeOnCancel: false
			}).then((result) => {
				if (result.isConfirmed) {
					Swal.close();
				}

			});
			$(currobj).val("");
			return false;
		}

		let fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";

		if (thisval == "REJECT" && isEmpty(remarks)) {
			alert(ADMIN_KEYIN_REMARKS);
			$("#txtFldCompApproveRemarks").removeAttr("readOnly");
			$("#txtFldCompApproveRemarks").focus();
			$(currobj).val("")
			return false;
		}

		let jsontext = ""//formJsonText(fnaId,advstfcode);

		if (thisval == "REJECT") {
			let conf = window.confirm(ADMIN_REJECTSTATUS_CONFIRM);
			if (!conf) {
				$(currobj).val("")
				return false;
			}
		} else if (thisval == "APPROVE") {
			let conf = window.confirm(ADMIN_APPROVESTATUS_CONFIRM);
			if (!conf) {
				$(currobj).val("")
				return false;
			}
		}

		if (thisval == "APPROVE") {
			if (fnaPrin == "NTUC") {
				$('#modalNTUCApproval').modal({
					backdrop: 'static',
					keyboard: false,
					show: true,
				});
			} else {
				let ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId + "&txtFldCompAppStatus=" +
					thisval + "&txtFldManagerEmailId=" + mangrEmailId + "&txtFldCompRemarks=" + remarks + "&hNTUCPolicyId=" + hNTUCPolicyId + "&txtFldPolNum=" + polNo +
					"&txtFldAdviserName=" + advName + "&txtFldCustName=" + custName + "&txtFldAdvEmailId=" + advstfEmailId
					+ "&txtFldApprAdvStfId=" + approverAdvStfId + "&txtFldApprUserId=" + approverUserId + "&txtFldNtucCaseId=" + hNTUCPolicyId
					+ "&txtFldNtucCasests=" + ntucCasests + "&txtFldNtucPolCrtdFlg=" + ntucPolCrtdFlg
					+ "&CASEID=" + ntucCaseId + "&CASESTATUS=" + thisval + "&ADVSTFCODE=" + advstfcode
					+ "&FNAID=" + fnaId + "&CUSTID=" + custId + "&CUSTNRIC=" + custNRIC
					+ "&SERVADVID=" + advstfId + "&txtFldCustId=" + custId + "&txtFldFnaPrin=" + fnaPrin + "&KYCJSON=" + jsontext
				console.log(ajaxParam);

				setTimeout(function () {
					$('#cover-spin').show(0);
					document.getElementById('cover-spin').style.display = "block";
				}, 0);

				setTimeout(function () {
					$.ajax({
						url: baseUrl + "/KYCServlet?" + ajaxParam,
						type: "POST",
						//						        data :data,
						cache: false,
						async: false,
						//						        enctype: 'multipart/form-data',
						contentType: false,
						processData: false
					}).done(function (response) {
						document.getElementById('cover-spin').style.display = "none";

						let retval = $.parseJSON(response);
						for (let val in retval) {
							let tabdets = retval[val];
							if (tabdets["SESSION_EXPIRY"]) {
								window.location = baseUrl + SESSION_EXP_JSP;
								return;
							}
							for (let tab in tabdets) {
								if (tabdets.hasOwnProperty(tab)) {
									let value = tabdets[tab];
									//											alert(tab +","+value[0]);

									if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
										//	alert(value[0].COMP_POLICY_STATUS);

										if ("APPROVED" == value[0].COMP_POLICY_STATUS) {
											Swal.fire({
												title: "Policy has been Approved by Compliance",
												html: "Do you want to upload this FNA document to <strong>FPMS</strong> client attachments?",
												icon: 'question',
												allowOutsideClick: false,
												allowEscapeKey: false,
												showCancelButton: true,
												confirmButtonColor: '#3085d6',
												cancelButtonColor: '#d33',
												confirmButtonText: 'Yes, Proceed'
											}).then((result) => {
												if (result.isConfirmed) {
													//														    	var cfnaId=$("#hdnSessFnaId").val();
													//														    	Cookies.remove(cfnaId, { path: '/' })

													$('#cover-spin').show(0);
													setTimeout(function () { insertCustAttach() }, 1200);
												}
												if (result.isDismissed) {
													window.location.reload(true);
												}
											})
										} else {
											$('#cover-spin').hide(0);
											Swal.fire({
												title: "eKYC Notification",
												html: "Policy has been Rejected by Compliance",
												icon: "info",
												//													  	  showCancelButton: true,
												allowOutsideClick: false,
												allowEscapeKey: false,
												confirmButtonClass: "btn-danger",
												confirmButtonText: "OK",
												//													  	  cancelButtonText: "No,Cancel",
												//													  	  closeOnConfirm: false,
												//showLoaderOnConfirm: true
												//													  	  closeOnCancel: false
											}).then((result) => {
												if (result.isConfirmed) {
													parent.location.reload(true);
												}
											});
										}
									}

									if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
										//												alert(value);
										$('#cover-spin').hide(0);
										Swal.fire({
											title: "eKYC Notification",
											html: value,
											icon: "info",
											//												  	  showCancelButton: true,
											allowOutsideClick: false,
											allowEscapeKey: false,
											confirmButtonClass: "btn-danger",
											confirmButtonText: "OK",
											//												  	  cancelButtonText: "No,Cancel",
											//												  	  closeOnConfirm: false,
											//showLoaderOnConfirm: true
											//												  	  closeOnCancel: false
										}).then((result) => {
											if (result.isConfirmed) {
												parent.location.reload(true);
											}
										});
									}
								}
							}
						}
					});
				}, 1000);
			}
		} else if (thisval == "REJECT") {
			let ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId + "&txtFldCompAppStatus=" +
				thisval + "&txtFldManagerEmailId=" + mangrEmailId + "&txtFldCompRemarks=" + remarks + "&hNTUCPolicyId=" + hNTUCPolicyId + "&txtFldPolNum=" + polNo +
				"&txtFldAdviserName=" + advName + "&txtFldCustName=" + custName + "&txtFldAdvEmailId=" + advstfEmailId
				+ "&txtFldApprAdvStfId=" + approverAdvStfId + "&txtFldApprUserId=" + approverUserId + "&txtFldNtucCaseId=" + ntucCaseId
				+ "&txtFldNtucCasests=" + ntucCasests + "&txtFldNtucPolCrtdFlg=" + ntucPolCrtdFlg
				+ "&CASEID=" + ntucCaseId + "&CASESTATUS=" + thisval + "&ADVSTFCODE=" + advstfcode
				+ "&FNAID=" + fnaId + "&KYCJSON=" + jsontext + "&CUSTID=" + custId + "&CUSTNRIC=" + custNRIC
				+ "&SERVADVID=" + advstfId + "&txtFldCustId=" + custId + "&txtFldFnaPrin=" + fnaPrin;

			console.log(ajaxParam);

			setTimeout(function () {
				document.getElementById('cover-spin').style.display = "block";
			}, 0);

			setTimeout(function () {
				$.ajax({
					url: baseUrl + "/KYCServlet?" + ajaxParam,
					type: "POST",
					//				        data :data,
					cache: false,
					//				        enctype: 'multipart/form-data',
					contentType: false,
					processData: false,
					async: false
				}).done(function (response) {
					document.getElementById('cover-spin').style.display = "none";
					let retval = $.parseJSON(response);
					for (let val in retval) {
						let tabdets = retval[val];
						if (tabdets["SESSION_EXPIRY"]) {
							window.location = baseUrl + SESSION_EXP_JSP;
							return;
						}
						for (let tab in tabdets) {
							if (tabdets.hasOwnProperty(tab)) {
								let value = tabdets[tab];
								if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
									//										alert(value[0].COMP_POLICY_STATUS);
									$('#cover-spin').hide(0);
									Swal.fire({
										title: "eKYC Notification",
										html: "Policy has been Rejected by Compliance",
										icon: "info",
										//										  	  showCancelButton: true,
										allowOutsideClick: false,
										allowEscapeKey: false,
										confirmButtonClass: "btn-danger",
										confirmButtonText: "OK",
										//										  	  cancelButtonText: "No,Cancel",
										//										  	  closeOnConfirm: false,
										//showLoaderOnConfirm: true
										//										  	  closeOnCancel: false
									}).then((result) => {
										if (result.isConfirmed) {
											parent.location.reload(true);
										}

									});
									//										window.location.reload(true);
								}
								if (tab == "UPDATEKYC2_STATUS") {
									alert(value);
									parent.location.reload(true);
								}

								if (tab == "UPDATEKYC2_DSF_STATUS") {
									if (value.toLowerCase() == "success") {

									} else if (value.toLowerCase() == "failure") {

									}
								}
								if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
									alert(value);
									parent.location.reload(true);

								}
							}
						}
					}
				});
			}, 1000);
		}
	}
}

function logoutKyc() {
	window.location.href = "logout";
}

function validateNTUCApprDoc() {
	let file = document.getElementById("fileKycCompApprove").value;

	if (isEmpty(file)) {
		alert("Select the document");
		return false;
	} else {
		let fnaId = $("#txtFldFnaId").val()
		let mangrEmailId = strLoggedUserEmailId;
		let remarks = $("#txtFldAdminApproveRemarks").val();
		let hNTUCPolicyId = $("#txtFldNTUCCaseId").val(),
			ntucCaseId = $("#txtFldNTUCCaseId").val();
		let polNo = $("#txtFldNTUCPolNo").val();
		let advName = $("#txtFldAdvStfName").val();
		let advstfId = "";
		let custName = $("#txtFldCustName").val();
		let custId = $("#txtFldCustId").val();
		let custNRIC = "";
		let advstfEmailId = $("#txtFldAdvEmailId").val();

		let approverAdvStfId = "";
		let approverUserId = "";
		let advstfcode = "";

		let jsontext = ""//formJsonText(fnaId,advstfcode);
		let thisval = $("#txtFldCompApproveStatus").val();

		let ntucCasests = $("#txtFldNTUCCaseStatus").val();
		let ntucPolCrtdFlg = "";

		let fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";

		let ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId + "&txtFldCompAppStatus=" +
			thisval + "&txtFldManagerEmailId=" + mangrEmailId + "&txtFldCompRemarks=" + remarks + "&hNTUCPolicyId=" + hNTUCPolicyId + "&txtFldPolNum=" + polNo +
			"&txtFldAdviserName=" + advName + "&txtFldCustName=" + custName + "&txtFldAdvEmailId=" + advstfEmailId
			+ "&txtFldApprAdvStfId=" + approverAdvStfId + "&txtFldApprUserId=" + approverUserId + "&txtFldNtucCaseId=" + hNTUCPolicyId
			+ "&txtFldNtucCasests=" + ntucCasests + "&txtFldNtucPolCrtdFlg=" + ntucPolCrtdFlg
			+ "&CASEID=" + ntucCaseId + "&CASESTATUS=" + thisval + "&ADVSTFCODE=" + advstfcode
			+ "&FNAID=" + fnaId + "&CUSTID=" + custId + "&CUSTNRIC=" + custNRIC
			+ "&SERVADVID=" + advstfId + "&txtFldCustId=" + custId + "&txtFldFnaPrin=" + fnaPrin + "&KYCJSON=" + jsontext
		console.log(ajaxParam)

		let form = $('#hdnFormKycApprDoc')[0];
		let data = new FormData(form);

		$.ajax({
			url: baseUrl + "/KYCServlet?" + ajaxParam,
			type: "POST",
			data: data,
			cache: false,
			enctype: 'multipart/form-data',
			contentType: false,
			processData: false
		}).done(function (response) {
			//	    	loadMsg.style.display="none";	 

			let retval = $.parseJSON(response);

			for (let val in retval) {
				let tabdets = retval[val];
				if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				}
				for (let tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						let value = tabdets[tab];
						if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
							alert(value[0].COMP_POLICY_STATUS);
						}
						if (tab == "UPDATEKYC2_STATUS") {
							alert(value);
						}
						if (tab == "UPDATEKYC2_DSF_STATUS") {
							//							alert(value.toLowerCase())
							if (value.toLowerCase() == "success") {

							} else if (value.toLowerCase() == "failure") {

							}
						}
						if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
							alert(value);
						}
					}
				}
			}
		});
	}
	return true;
}

function loadDocJsnDataNew(jsnData, fnaid) {

	//check empty field validation for AttachId and InsurarName
	if (isEmpty(jsnData[1])) {
		jsnData[1] = "-NIL-";
	} else {
		jsnData[1] = jsnData[1];
	}

	if (isEmpty(jsnData[5])) {
		jsnData[5] = "-NIL-";
	} else {
		jsnData[5] = jsnData[5];
	}
	//Validation End

	let docid = jsnData.txtFldDocNdId;
	//var strList = '<a href="#" class="list-group-item list-group-item-action p-2">'+
	//'<div class="d-flex w-100 justify-content-between "><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Category : '+jsnData.txtFldDocTitle+'</h6> <small><i class="fa fa-download cursor-pointer" style="color: #009A78;" onclick="downLoadNtucAttach('+docid+')" title="Click to Download Doc."></i></small></div>'+
	//'<small>Title : '+jsnData.txtFldDocFileName+'</small><small class="font-sz-level6"><span style="font-weight: 450;">Insurar Name :</span> '+jsnData[5]+'</small>  </a>';

	let strList = '<a href="#" class="list-group-item list-group-item-action p-2" id="' + docid + '">' +
		'<div class="row">' +
		'<div class="col-8"><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Category : ' + jsnData.txtFldDocTitle + '</h6></div>' +
		'<div class="col-1"><small><i class="fa fa-trash cursor-pointer" style="color: #f04040;display:none" onclick=deleteDoc(this) title="Delete Doc."></i></small></div>' +
		'<div class="col-1"><small><i class="fa fa-download cursor-pointer" style="color: #007bff;" onclick="downLoadNtucAttach(' + docid + ')" title="Click to Download Doc."></i></small></div>' +
		'</div>' +
		'<div class="row">' +
		'<div class="col-12"><small class="font-sz-level6">Title : ' + jsnData.txtFldDocFileName + '</small></div>' +
		'<div class="col-12"><small class="font-sz-level6"><span style="font-weight: 450;">Insurar Name :</span> ' + jsnData[5] + '</small> </div>' +
		'</div>' +
		'</a>';

	$("#dynaAttachNTUCList").append(strList);

	let totlDocListLen = $("#dynaAttachNTUCList").children().length;
	if (totlDocListLen > 0) {
		$("#noDocFoundSec").addClass("hide").removeClass("show")
		$("#DocListSec").addClass("show").removeClass("hide")
	} else {
		$("#noDocFoundSec").addClass("show").removeClass("hide")
		$("#DocListSec").addClass("hide").removeClass("show")
	}

	$('#dynaAttachNTUCList a:even').addClass('bsc-pln-bg');
}

function deleteDoc(thisObj) {
	let cusAttachId = $(thisObj).parents(".list-group-item").attr("id");
	//alert(cusAttachId)
	Swal.fire({
		title: 'Are you sure?',
		text: "Want to Delete the Document?",
		icon: 'warning',
		allowOutsideClick: false,
		allowEscapeKey: false,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, Delete it!'
	}).then((result) => {
		if (result.isConfirmed) {
			$.ajax({
				url: "CustAttach/delete/" + cusAttachId,
				type: "POST", async: false,
				// contentType: "application/json",
				success: function (response) {
					let totlDocListLen = $("#dynaAttachNTUCList").children().length;

					if (totlDocListLen > 1) {
						$(thisObj).parents(".list-group-item").remove();
						hideNoFileFoundImg();
					} else {
						$(thisObj).parents(".list-group-item").remove();
						showNoFileFoundImg();
					}
					Swal.fire("Deleted!", "File / Document has been deleted.", "success");
				},
				error: function (xhr, textStatus, errorThrown) {
					ajaxCommonError();
				}
			});
		} else {
			swal.fire("Cancelled", "File / Document detail is safe :)", "error");
		}
	});
}

function hideNoFileFoundImg() {
	$("#noDocFoundSec").addClass("hide").removeClass("show");
	$("#DocListSec").addClass("show").removeClass("hide");
}

function showNoFileFoundImg() {
	$("#noDocFoundSec").addClass("show").removeClass("hide");
	$("#DocListSec").addClass("hide").removeClass("show");
}

function populateApprovedRec(dataset, loggedUser) {

	let fnaId = dataset[1];
	let custname = dataset[2].toUpperCase();
	let mgrstatus = dataset[19]//value[0]["txtFldMgrApproveStatus"];
	let adminstatus = dataset[20]//value[0]["txtFldAdminApproveStatus"];
	let compstatus = dataset[21]//value[0]["txtFldCompApproveStatus"];
	let custid = dataset[6];
	let advId = dataset[7];
	let emailId = dataset[8];
	let ntucCaseId = ""//value[0]["txtFldFnaNtucCaseId"];
	let mgrapprby = dataset[10];
	let mgrapprdate = dataset[11];
	let mgrremarks = dataset[12];
	let adminapprby = dataset[13];
	let adminapprdate = dataset[14];
	let adminremarks = dataset[15];
	let compapprby = dataset[16];
	let compapprdate = dataset[17];
	let compremarks = dataset[18];

	$("#dropDownQuickLinkFNAHist").val(custname);

	$("#optgrpPending").append('<option value="' + fnaId + '">' + custname + '</option>');
	$('#dropDownQuickLinkFNAHist').val(custname);
	$('#dropDownQuickLinkFNAHist').trigger('change.select2');

	/*$('#dropDownQuickLinkFNAHist').select2({
			width :'400',				
			 templateResult: formatDataSelect2
		})*/
	$("#landingpage").find("li:eq(1) a").trigger("click");

	$("#txtFldFnaId").val(fnaId)
	$("#txtFldCustName").val(custname)
	//	$("#txtFldAdvStfName").val(advId);
	$("#spanLoggedAdviser").text($("#txtFldAdvStfName").val())
	$("#txtFldMgrName").val();

	//poovathi get hidden text field values below on 16-03-2020
	$("#txtFldNTUCCaseId").val(ntucCaseId);
	$("#txtFldCustId").val(custid);
	//alert(custid)
	//setcustId to hidden field
	$("#hTxtFldCurrCustId").val(custid);
	$("#txtFldAdvEmailId").val(emailId);

	if (isEmpty(ntucCaseId)) {
		$("#ntucblock").addClass("fa fa-times-circle-o");
	} else {
		$("#ntucblock").addClass("fa fa-check-square-o").removeClass("fa fa-times-circle-o");
	}

	//MANAGER	    
	if ((mgrstatus.toUpperCase()) == "APPROVE") {
		$("#mngrapprLbl").text('Approved Date');
		$("#mngrapprByLbl").text('Approved By');
		$("#iFontMgr").removeClass("fa-question-circle-o");
		$("#iFontMgr").removeClass("fa-times-circle-o");
		$("#iFontMgr").addClass("fa-check-circle-o");

		$("#txtFldMgrApproveStatus").prop("disabled", true);
		$("#txtFldMgrApproveRemarks").prop("readOnly", true)
	} else if ((mgrstatus.toUpperCase()) == "REJECT") {
		$("#mngrapprLbl").text('Rejected Date');
		$("#mngrapprByLbl").text('Rejected By');
		$("#iFontMgr").addClass("fa fa-times-circle-o");
		$("#iFontMgr").removeClass("fa fa-check-circle-o");
		$("#iFontMgr").removeClass("fa fa-question-circle");
	} else {
		$("#mngrapprLbl").text('');
		$("#mngrapprByLbl").text('');
		//$("#mngrapprByLbl").text('Yet to Approve By');
		$("#iFontMgr").removeClass("fa fa-times-circle-o")
		$("#iFontMgr").removeClass("fa fa-check-circle-o")
		$("#iFontMgr").addClass("fa fa-question-circle");
	}

	//ADMIN

	if (adminstatus.toUpperCase() == "APPROVE") {
		$("#admnapprLbl").text('Approved Date');
		$("#admnapprByLbl").text('Approved By');
		$("#iFontAdmin").removeClass("fa fa-times-circle-o")
		$("#iFontAdmin").addClass("fa fa-check-circle-o");
		$("#iFontAdmin").removeClass("fa fa-question-circle");
	} else if ((adminstatus.toUpperCase()) == "REJECT") {
		$("#admnapprLbl").text('Rejected Date');
		$("#admnapprByLbl").text('Rejected By');
		$("#iFontAdmin").addClass("fa fa-times-circle-o");
		$("#iFontAdmin").removeClass("fa fa-check-circle-o");
		$("#iFontAdmin").removeClass("fa fa-question-circle");
	} else {
		$("#admnapprLbl").text('');
		$("#admnapprByLbl").text('');
		//$("#admnapprByLbl").text('Yet to Approve By');
		$("#iFontAdmin").removeClass("fa fa-times-circle-o")
		$("#iFontAdmin").removeClass("fa fa-check-circle-o")
		$("#iFontAdmin").addClass("fa fa-question-circle");
	}

	//COMPLIANCE					
	if (compstatus.toUpperCase() == "APPROVE") {
		$("#compapprLbl").text('Approved Date');
		$("#compapprByLbl").text('Approved By');
		$("#iFontComp").removeClass("fa fa-times-circle-o")
		$("#iFontComp").addClass("fa fa-check-circle-o")
		$("#iFontComp").removeClass("fa fa-question-circle");
	} else if (compstatus.toUpperCase() == "REJECT") {
		$("#compapprLbl").text('Rejected Date');
		$("#compapprByLbl").text('Rejected By');
		$("#iFontComp").addClass("fa fa-times-circle-o")
		$("#iFontComp").removeClass("fa fa-check-circle-o")
		$("#iFontComp").removeClass("fa fa-question-circle");
	} else {
		$("#compapprLbl").text('');
		$("#compapprByLbl").text('');
		//$("#compapprByLbl").text('Yet to Approve By');
		$("#iFontComp").removeClass("fa fa-times-circle-o")
		$("#iFontComp").removeClass("fa fa-check-circle-o")
		$("#iFontComp").addClass("fa fa-question-circle");
	}

	// FOR MANAGER
	$("#txtFldMgrApproveStatus").val(mgrstatus)
	$("#txtFldMgrApproveDate").val(mgrapprdate);
	$("#txtFldMgrApproveRemarks").val(mgrremarks);
	$("#txtFldMgrStsApprBy").val(mgrapprby);
	setApprovalStatus($("#txtFldMgrApproveStatus"));

	// FOR ADMIN				
	$("#txtFldAdminApproveStatus").val(adminstatus);
	$("#txtFldAdminApproveDate").val(adminapprdate);
	$("#txtFldAdminApproveRemarks").val(adminremarks);
	$("#txtFldAdminStsApprBy").val(adminapprby);
	setApprovalStatus($("#txtFldAdminApproveStatus"));

	// FOR COMPLIANCE	
	$("#txtFldCompApproveStatus").val(compstatus);
	$("#txtFldCompApproveDate").val(compapprdate);
	$("#txtFldCompApproveRemarks").val(compremarks);
	$("#txtFldCompStsApprBy").val(compapprby);
	setApprovalStatus($("#txtFldCompApproveStatus"));

	let mgrApprStatus = mgrstatus;//$("#txtFldMgrApproveStatus").val();

	let adminApprStatus = adminstatus;//$("#txtFldAdminApproveStatus").val();

	let compApprStatus = compstatus;//$("#txtFldCompApproveStatus").val();

	openEKYCNew(dataset);

	//downLoadAllFile(fnaId,false);

	getAllExisitingDocs(custid);
	//	alert(loggedUser)

	if (loggedUser == "ADMIN") {//|| loggedUser == "COMPANDADMIN"
		$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
		$("#appr-mgr-tab-cont").removeClass("show").removeClass("active");;

		$("#appr-admin-tab").removeClass("no-drop1");
		$("#appr-admin-tab").addClass("active");
		$("#appr-admin-tab-cont").addClass("show").addClass("active");
		$("#txtFldAdminApproveRemarks").prop("readOnly", false);

		$("#appr-comp-tab").addClass("no-drop1");
		$("#appr-comp-tab").removeClass("show").removeClass("active");
		$("#appr-comp-tab-cont").removeClass("show").removeClass("active");

		setApprovalStatus($("#txtFldAdminApproveStatus"));
	} else if (loggedUser == "COMPLIANCE") {//|| loggedUser == "COMPANDADMIN"

		let tabfocus = true;

		$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
		$("#appr-mgr-tab-cont").addClass("show").removeClass("active");

		$("#appr-admin-tab").addClass("no-drop1").removeClass("active");
		$("#appr-admin-tab-cont").addClass("show").removeClass("active");
		$("#txtFldAdminApproveRemarks").prop("readOnly", true);

		$("#appr-comp-tab").removeClass("no-drop1");
		$("#appr-comp-tab").removeClass("show");
		$("#appr-comp-tab").addClass("active");
		$("#appr-comp-tab-cont").addClass("show").addClass("active");

		if (strAdminFlg == "Y" && strCompFlg == "Y") {

			if (adminApprStatus == "APPROVE") { } else { }

			setApprovalStatus($("#txtFldAdminApproveStatus"));

		} else if (strAdminFlg != "Y" && strCompFlg == "Y") {
			/*			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
						$("#appr-admin-tab-cont").removeClass("show").removeClass("active");				    	
						$("#txtFldAdminApproveRemarks").prop("readOnly",true);
						 
						$("#appr-comp-tab").removeClass("show").addClass("active");
						$("#appr-comp-tab").addClass("active");
						$("#appr-comp-tab-cont").addClass("show").addClass("active");
			*/
		}

		setApprovalStatus($("#txtFldCompApproveStatus"));

	} else if (loggedUser == "COMPANDADMIN") {//|| loggedUser == "COMPANDADMIN"

		let tabfocus = true;

		$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
		$("#appr-mgr-tab-cont").addClass("show").removeClass("active");

		$("#appr-admin-tab").addClass("no-drop1").removeClass("active");
		$("#appr-admin-tab-cont").addClass("show").removeClass("active");
		$("#txtFldAdminApproveRemarks").prop("readOnly", true);

		$("#appr-comp-tab").removeClass("no-drop1");
		$("#appr-comp-tab").removeClass("show");
		$("#appr-comp-tab").addClass("active");
		$("#appr-comp-tab-cont").addClass("show").addClass("active");

		if (strAdminFlg == "Y" && strCompFlg == "Y") {

			if (adminApprStatus == "APPROVE") {

				$("#appr-comp-tab").removeClass("no-drop1");
				$("#appr-comp-tab").removeClass("show");
				$("#appr-comp-tab").addClass("active");
				$("#appr-comp-tab-cont").addClass("show").addClass("active");

			} else {

				$("#appr-admin-tab").removeClass("no-drop1");
				$("#appr-admin-tab").addClass("active");
				$("#appr-admin-tab-cont").addClass("show").addClass("active");
				$("#txtFldAdminApproveRemarks").prop("readOnly", false);

				$("#appr-comp-tab").addClass("no-drop1");
				$("#appr-comp-tab").removeClass("show").removeClass("active");
				$("#appr-comp-tab-cont").removeClass("show").removeClass("active");

			}

			setApprovalStatus($("#txtFldAdminApproveStatus"));

		} else if (strAdminFlg != "Y" && strCompFlg == "Y") {

			/*			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
						$("#appr-admin-tab-cont").removeClass("show").removeClass("active");				    	
						$("#txtFldAdminApproveRemarks").prop("readOnly",true);
						 
						$("#appr-comp-tab").removeClass("show").addClass("active");
						$("#appr-comp-tab").addClass("active");
						$("#appr-comp-tab-cont").addClass("show").addClass("active");
			*/
		}

		setApprovalStatus($("#txtFldCompApproveStatus"));
	}

	/*if(mgrApprStatus.toUpperCase() == "APPROVE"){
		$("#appr-mgr-tab-cont").find(":input").prop("disabled",true);
	}else{
		$("#appr-mgr-tab-cont").find(":input").prop("disabled",false);
	}*/

	if (adminApprStatus.toUpperCase() == "APPROVE") {
		$("#appr-admin-tab-cont").find(":input").prop("disabled", true);
	} else {
		$("#appr-admin-tab-cont").find(":input").prop("disabled", false);
	}

	if (compApprStatus.toUpperCase() == "APPROVE") {
		$("#appr-comp-tab-cont").find(":input").prop("disabled", true);
	} else {
		$("#appr-comp-tab-cont").find(":input").prop("disabled", false);
	}

	if (ntuc_policy_access_flag == "true" && compApprStatus.toUpperCase() == "APPROVE") {
		$("#imgpolapi").show()
	} else {
		$("#imgpolapi").hide()
	}

}

function generateFNA() {
	//	$("#dynaFrame").get(0).contentWindow.print();


	//	var machine = "";
	//	machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC //
	//			+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaId+"&__format=pdf"
	//			+ "&P_PRDTTYPE=" + lobArrtoKYC
	//			+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + kycformname
	//			+ "&P_DISTID=" + LOGGED_DISTID;
	//	
	//	alert(machine)
}

function setApprovalStatus(object) {
	let thisval = $(object).val();
	//	thisval = $(object).find('option[value="' + thisval+ '"]').html();
	//	  option = options.find("[value='" + i + "']");

	if (thisval == "APPROVE") {
		$(object).find('option:contains("Approve")').text('Approved');
		//		document.getElementById($(object).prop("id")).options[0].innerHTML = "Approved";
	}
	if (thisval == "REJECT") {
		$(object).find('option:contains("Reject")').text('Rejected');
		//		document.getElementById($(object).prop("id")).options[1].innerHTML = "Rejected";
	}
}

function openList(statusMsg) {
	if (statusMsg == "PENDING") {
		$("#yourAprovTabs").find("li:eq(0) a").trigger("click");
		$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
	}

	if (statusMsg == "APPROVED") {
		$("#yourAprovTabs").find("li:eq(1) a").trigger("click");
		$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
	}

	if (statusMsg == "REJECTED") {
		$("#yourAprovTabs").find("li:eq(2) a").trigger("click");
		$.fn.dataTable.tables({ visible: true, api: true }).columns.adjust();
	}
}

/*PIE CHART START*/
function sliceSize(dataNum, dataTotal) {
	return (dataNum / dataTotal) * 360;
}

function addSlice(sliceSize, pieElement, offset, sliceID, color) {
	$(pieElement).append("<div class='slice " + sliceID + "'><span></span></div>");
	offset = offset - 1;
	let sizeRotation = -179 + sliceSize;
	$("." + sliceID).css({
		"transform": "rotate(" + offset + "deg) translate3d(0,0,0)"
	});
	$("." + sliceID + " span").css({
		"transform": "rotate(" + sizeRotation + "deg) translate3d(0,0,0)",
		"background-color": color
	});
}

function iterateSlices(sliceSize, pieElement, offset, dataCount, sliceCount, color) {
	let sliceID = "s" + dataCount + "-" + sliceCount;
	let maxSize = 179;
	if (sliceSize <= maxSize) {
		addSlice(sliceSize, pieElement, offset, sliceID, color);
	} else {
		addSlice(maxSize, pieElement, offset, sliceID, color);
		iterateSlices(sliceSize - maxSize, pieElement, offset + maxSize, dataCount, sliceCount + 1, color);
	}
}

function createPie(dataElement, pieElement) {
	let listData = [];
	$(dataElement + " span").each(function () {
		listData.push(Number($(this).html()));
	});
	let listTotal = 0;
	for (let listIdx = 0; listIdx < listData.length; listIdx++) {
		listTotal += listData[listIdx];
	}
	let offset = 0;
	let color = ["#333E48", "#009A78", "#d9534f"];
	for (let lstIdx = 0; lstIdx < listData.length; lstIdx++) {
		if (listTotal > 0) {
			let size = sliceSize(listData[lstIdx], listTotal);
			iterateSlices(size, pieElement, offset, lstIdx, 0, color[lstIdx]);
			$(dataElement + " li:nth-child(" + (lstIdx + 1) + ")").css("border-color", color[lstIdx]);
			offset += size;
		}
	}
}
/*PIE CHART END*/

function getQrorSign() {

	let selectedFnaId = $("#txtFldFnaId").val();

	let ff = getCurrFNASigns(selectedFnaId);

	if (!ff) {
		toastr.clear($('.toast'));
		toastr.options = { timeOut: "5000", extendedTimeOut: "1000" };
		toastr["error"]("Client(1)/Client(2) and Adviser Signature cannot be empty!");
		return;
	}

	if (!$("#suprevMgrFlgA").is(":checked")) {
		// clear all fields
		//		$("#diManagerSection").find(":input").val("");
		$("#suprevFollowReason").val("")
		$("#diManagerSection").find('input:radio').prop('checked', false);

		$("#txtFldMgrSignDateDeclarepage").prop('readOnly', true);

		//		hide if any error msh shows in modal
		$("#mngrBtnErrorMsg").html("");
		$("#errorFld").css("border", "");
		$("#mngrDisagrReason").html("");
		$("#suprevFollowReason").removeClass("err-fld");
	} else {

		$("#openMngesecModal").find(".modal-footer button").hide();
		$("#btnApprMgrFlag").removeClass("d-none").show()

	}

	enableMgr();

	let fnaId = $("#txtFldCurFnaId").val();
	if (isEmpty(fnaId)) {
		curntFnaId = $("#hTxtFldFnaId").val();
	} else {
		curntFnaId = fnaId;
	}

	getSignPageData(curntFnaId);

	getLatestFnaIdDetls(curntFnaId)

	getAllSign(curntFnaId);

	$("#openMngesecModal").modal({
		backdrop: 'static',
		keyboard: false,
		show: true
	});
}

function getAllExisitingDocs(currentCustId) {
	//var currentCustId = $("#txtFldCustId").val();
	$("#dynaAttachNRICList").empty();

	$.ajax({
		url: baseUrl + "/CustAttach/getAllNricCustDocs/" + currentCustId,
		type: "GET",
		dataType: "json",
		async: false,
		contentType: "application/json",
		success: function (response) {
			//            	alert(response.length)
			for (let respIdx = 0; respIdx < response.length; respIdx++) {
				//            		console.log(response[d][0],response[d][1])
				loadDocJsnDataNew(response[respIdx], null)
			}

			if (response.length == 0) {
				//            		$("#dynaAttachNRICList").html("-No Refernce Docs-")

				$("#dynaAttachNRICList").html("-No Refernce Docs-");

				let totlDocListLen = $("#dynaAttachNTUCList").children().length;
				//alert(totlDocListLen)
				//		if(totlDocListLen > 0){


				//}else{
				$("#noDocFoundSec").addClass("show").removeClass("hide");
				$("#DocListSec").addClass("hide").removeClass("show");
				//}
			} else {
				$("#noDocFoundSec").addClass("hide").removeClass("show");
				$("#DocListSec").addClass("show").removeClass("hide");
			}
		},
		error: function (xhr, textStatus, errorThrown) {
			ajaxCommonError();
		}
	});
}

function loadDocJsnDataNew(jsnData, fnaid) {

	let docid = jsnData[0];

	let remarks = isEmpty(jsnData[2]) ? "" : jsnData[2];
	let remarksLbl = isEmpty(remarks) ?
		'' : '<small class="font-sz-level9"><span style="font-weight: 450;">Remarks :</span> ' + remarks + '</small><br/>'

	//		var strList = '<a href="#" class="list-group-item list-group-item-action p-2">'+
	//		'<div class="d-flex w-100 justify-content-between "><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Doc.Title : '+jsnData[1]+'</h6> <small><i class="fa fa-download cursor-pointer" style="color: #009A78;" onclick=downloadDoc(\"'+docid+'\") title="Click to Download Doc."></i></small></div>'+
	//	    '<small class="font-sz-level6">File Name : '+jsnData[3]+'</small><small class="font-sz-level6"><span style="font-weight: 450;">Insurar Name :</span> '+jsnData[5]+'</small>    </a>';

	if (isEmpty(jsnData[5])) {
		jsnData[5] = "-NIL-";
	} else {
		jsnData[5] = jsnData[5];
	}

	//		var strList =
	//				'<a href="#" class="list-group-item list-group-item-action p-2">'+
	//				'<div class="d-flex w-100 justify-content-between "><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Category : '+jsnData[1]+'</h6> <small><i class="fa fa-download cursor-pointer" style="color: #009A78;" onclick=downloadDoc(\"'+docid+'\") title="Click to Download Doc."></i></small></div>'+
	//			    '<small>Title : '+jsnData[3]+'</small><br/>'+remarksLbl+			    
	//			    '<small class="font-sz-level6"><span style="font-weight: 450;">Insurer Name :</span> '+jsnData[5]+'</small>'+
	//			    '</a>';

	let strList = '<a href="#" class="list-group-item list-group-item-action p-2" id="' + docid + '">' +
		'<div class="row">' +
		'<div class="col-8"><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Category : ' + jsnData[1] + '</h6></div>' +
		'<div class="col-1"><small><i class="fa fa-trash cursor-pointer" style="color: #f04040;display:none" onclick=deleteDoc(this) title="Delete Doc."></i></small></div>' +
		'<div class="col-1"><small><i class="fa fa-download cursor-pointer" style="color: #007bff;" onclick=downloadDoc(\"' + docid + '\") title="Click to Download Doc."></i></small></div>' +
		'</div>' +
		'<div class="row">' +
		'<div class="col-12"><small class="font-sz-level6">Title : ' + jsnData[3] + '</small></div>' + remarksLbl +
		'<div class="col-12"><small class="font-sz-level6"><span style="font-weight: 450;">Insurar Name :</span> ' + jsnData[5] + '</small> </div>' +
		'</div>' +
		'</a>';

	$("#dynaAttachNRICList").append(strList);

	//		var totlDocListLen = $("#dynaAttachNTUCList").children().length;
	//		if(totlDocListLen > 0){
	//		$("#noDocFoundSec").addClass("hide").removeClass("show")
	//		$("#DocListSec").addClass("show").removeClass("hide")
	//		}else{
	//		$("#noDocFoundSec").addClass("show").removeClass("hide")
	//		$("#DocListSec").addClass("hide").removeClass("show")
	//		}

	$('#dynaAttachNTUCList a:even').addClass('bsc-pln-bg');
}

function downloadDoc(id) {
	let url = baseUrl + '/downloadDoc/' + id;
	window.open(url, "_blank");
}

//manager click  sendTo Approve button
function mngrSendToApprove() {

	if (!validateAgDagFld()) { return; }
	let status_m = "", reason_m = "";

	let disAgreeFlg = $("#suprevMgrFlgD").is(":checked");
	let AgreeFlg = $("#suprevMgrFlgA").is(":checked");
	let mngrVal = $('input[name="suprevMgrFlg"]:checked').val();

	reason_m = $("#suprevFollowReason").val();

	if (disAgreeFlg == true) {
		if (!validateDisagreeReasonFld()) { return; }

		//trigger send mail to Manager
		// sendMailtoManger();
		status_m = "REJECT";
	}

	if (AgreeFlg == true) {

		status_m = "APPROVE";

		//open signature Modal
		/*$("#ScanToSignModal").modal({
			backdrop: 'static',
			keyboard: false,
			show: true
		  })*/
	}

	let mangrEmailId = "";//$("#txtFldAdvMgrEmailId").val()
	let mangrId = "";// $("#txtFldMgrId").val()
	let hNTUCPolicyId = "";//$("#ntucCaseId").val();
	let polNo = "";

	let advstfEmailId = "";//$("#txtFldAdvEmailId").val();			            	  					
	let advName = "";// $("#txtFldAdvName").val();
	let custName = "";//$("#dfSelfName").val();

	let approverAdvStfId = "";
	let approverUserId = "";//$("#hTxtFldFnaLoggedUserId").val();
	let strLoggedUserEmailId = "";
	let fnaPrin = "";
	let fnaId = $("#txtFldFnaId").val();

	let ajaxParam = "dbcall=MANAGER_APPROVE_STATUS"
		+ "&txtFldFnaId=" + fnaId

		+ "&txtFldMgrAppStatus=" + status_m
		+ "&txtFldManagerEmailId=" + mangrEmailId
		+ "&txtFldManagerId=" + mangrId
		+ "&txtFldManagerRemarks=" + reason_m

		+ "&hNTUCPolicyId=" + hNTUCPolicyId
		+ "&txtFldPolNum=" + polNo

		+ "&txtFldAdvEmailId=" + advstfEmailId
		+ "&txtFldAdviserName=" + advName
		+ "&txtFldCustName=" + custName

		+ "&txtFldApprAdvId=" + approverAdvStfId
		+ "&txtFldApprUserId=" + approverUserId
		+ "&txtFldApprUserEmailId=" + strLoggedUserEmailId
		+ "&txtFldFnaPrin=" + fnaPrin
		;

	setTimeout(function () {
		window.parent.document.getElementById('cover-spin').style.display = "block";
	}, 0);
	setTimeout(function () {
		let ajaxResponse = callAjax(ajaxParam, false);
		window.parent.document.getElementById('cover-spin').style.display = "block";
		retval = jsonResponse = eval('(' + ajaxResponse + ')');

		for (let val in retval) {
			let tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for (let tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					let value = tabdets[tab];

					if (tab == "TAB_MANAGER_APPROVE_DETAILS") {

						$('#cover-spin').hide(0);

						//							alert(value[0].MANAGER_POLICY_STATUS);
						//							parent.location.reload(true);
						Swal.fire({
							title: "eKYC Notification",
							html: value[0].MANAGER_POLICY_STATUS,

							icon: "info",
							//							  	  showCancelButton: true,
							allowOutsideClick: false,
							allowEscapeKey: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "OK",
							//							  	  cancelButtonText: "No,Cancel",
							//							  	  closeOnConfirm: false,
							//showLoaderOnConfirm: true
							//							  	  closeOnCancel: false
						}).then((result) => {
							if (result.isConfirmed) {
								parent.location.reload(true);
							}

						});
						//							 location.reload(true);
					}
				}
			}
		}
	}, 1000);
}

function validateAgDagFld() {

	let disAgreeFlg = $("#suprevMgrFlgD").is(":checked");
	let AgreeFlg = $("#suprevMgrFlgA").is(":checked");

	console.log("disAgreeFlg" + disAgreeFlg + " " + "AgreeFlg" + AgreeFlg)

	if ((disAgreeFlg == false) && (AgreeFlg == false)) {
		$("#mngrBtnErrorMsg").html("Manager must select any one the options above !")
		$("#errorFld").css("border", "1px solid red")
		return;
	} else {
		$("#mngrBtnErrorMsg").html("");
		$("#errorFld").css("border", "");
	}
	return true;
}

function validateDisagreeReasonFld() {

	let mngrDisReason = $("#suprevFollowReason").val();
	if (isEmpty(mngrDisReason)) {
		$("#mngrDisagrReason").html("please state the Reasons and Follow-up Action for Disagree the option!");
		$("#suprevFollowReason").focus();
		$("#suprevFollowReason").addClass("err-fld");
		return;
	} else {
		$("#mngrDisagrReason").html("");
		$("#suprevFollowReason").removeClass("err-fld");
	}

	return true;
}

function hideErrmsg(obj) {
	if ($(obj).val().length > 0) {
		$("#mngrDisagrReason").html("");
		$("#suprevFollowReason").removeClass("err-fld");
	}
}

function insertCustAttach() {

	$('#cover-spin').show(0);

	let fnaId = $("#hTxtFldCurrFNAId").val();
	let custId = $("#hTxtFldCurrCustId").val();

	let advName = $("#txtFldAdvStfName").val();
	let custName = $("#txtFldCustName").val();

	let ajaxParam = 'dbcall=INSERT_CUSTOMER_ATTACHMENTS&paramFnaId=' + fnaId + "&paramcustId=" + custId
		+ "&paramAdviserName=" + advName + "&paramCustName=" + custName;

	let ajaxResponse = callAjax(ajaxParam, false);
	retval = jsonResponse = eval('(' + ajaxResponse + ')');

	for (let val in retval) {
		let tabdets = retval[val];
		if (tabdets["SESSION_EXPIRY"]) {
			window.location = baseUrl + SESSION_EXP_JSP;
			return;
		}
		for (let tab in tabdets) {
			if (tabdets.hasOwnProperty(tab)) {
				let value = tabdets[tab];

				if (tab == "CUST_ATTACH_ID") {
					//						custattachId = value[0].txtFlsCustAttachId;
					if (value == "FAIL") {
						//							alert("FNA Document upload failed, try again")
						$('#cover-spin').hide(0);
					} else {
						$('#cover-spin').hide(0);
						Swal.fire({
							//							  	  title: "FNA Form is uploaded successfully in FPMS client attachments!",
							html: "FNA Form is uploaded successfully in FPMS client attachments!",
							icon: "info",
							//							  	  showCancelButton: true,
							allowOutsideClick: false,
							allowEscapeKey: false,
							confirmButtonClass: "btn-danger",
							confirmButtonText: "OK",
							//							  	  cancelButtonText: "No,Cancel",
							//							  	  closeOnConfirm: false,
							//showLoaderOnConfirm: true
							//							  	  closeOnCancel: false
						}).then((result) => {
							if (result.isConfirmed) {
								parent.location.reload(true);
							}

						});
						//							alert("FNA Document is added to customer!")	
					}
					//						window.location.reload(true);
				}
			}
		}
	}
}

function loadALLFNAId() {

	if (strALLFNAIds != null) {

		let retval = strALLFNAIds;

		for (let val in retval) {
			let tabdets = retval[val];
			for (let tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					let key = tab;
					let value = tabdets[tab];
					if (key == "MANAGER_FNA_DETAILS" || key == "ADMIN_FNA_DETAILS" || key == "COMPLIANCE_FNA_DETAILS") {
						let ResultLength = value.length;

						for (let len = 0; len < ResultLength; len++) {
							let status = "";

							if (key == "MANAGER_FNA_DETAILS") {
								status = value[len]["txtFldMgrApproveStatus"];
							} else if (key == "ADMIN_FNA_DETAILS") {
								status = value[len]["txtFldAdminApproveStatus"];
							} else if (key == "COMPLIANCE_FNA_DETAILS") {
								status = value[len]["txtFldCompApproveStatus"];
							}

							//PENDING Table List
							if (isEmpty(status)) {

								let fnaid = value[len]["txtFldFnaId"],
									custname = value[len]["txtFldCustName"];
								pendingdatatable.row.add([
									pendingdatatable.rows().count() + 1,
									value[len]["txtFldFnaId"],
									value[len]["txtFldCustName"],
									(value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									(value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									(value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									value[len]["txtFldCustId"],
									value[len]["txtFldAdvStfId"],
									value[len]["txtFldAdvStfEmailId"],
									(value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
									value[len]["txtFldMgrStsApprBy"], value[len]["txtFldMgrApproveDate"], value[len]["txtFldMgrApproveRemarks"],
									value[len]["txtFldAdminStsApprBy"], value[len]["txtFldAdminApproveDate"], value[len]["txtFldAdminApproveRemarks"],
									value[len]["txtFldCompStsApprBy"], value[len]["txtFldCompApproveDate"], value[len]["txtFldCompApproveRemarks"],
									value[len]["txtFldMgrApproveStatus"], value[len]["txtFldAdminApproveStatus"], value[len]["txtFldCompApproveStatus"]
								]).draw(false);

								$("#optgrpPending").append('<option value="' + fnaid + '">' + custname + '</option>');

								console.log("Pending" + "  " + fnaid + " " + custname)
							}

							//APPROVE Table List							
							if (status.toUpperCase() == "APPROVE") {

								approveddatatable.row.add([
									approveddatatable.rows().count() + 1,
									value[len]["txtFldFnaId"],
									value[len]["txtFldCustName"],
									(value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									(value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									(value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									value[len]["txtFldCustId"],
									value[len]["txtFldAdvStfId"],
									value[len]["txtFldAdvStfEmailId"],
									(value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
									value[len]["txtFldMgrStsApprBy"], value[len]["txtFldMgrApproveDate"], value[len]["txtFldMgrApproveRemarks"],
									value[len]["txtFldAdminStsApprBy"], value[len]["txtFldAdminApproveDate"], value[len]["txtFldAdminApproveRemarks"],
									value[len]["txtFldCompStsApprBy"], value[len]["txtFldCompApproveDate"], value[len]["txtFldCompApproveRemarks"],
									value[len]["txtFldMgrApproveStatus"], value[len]["txtFldAdminApproveStatus"], value[len]["txtFldCompApproveStatus"]
								]).draw(false);
								//									
								let fnaid = value[len]["txtFldFnaId"],
									custname = value[len]["txtFldCustName"];

								$("#optgrpApproved").append('<option value="' + fnaid + '">' + custname + '</option>');

								console.log("Approve" + "  " + fnaid + " " + custname)

							}

							//		REJECTED Table List						
							if (status.toUpperCase() == "REJECT") {

								let fnaid = value[len]["txtFldFnaId"],
									custname = value[len]["txtFldCustName"];

								rejecteddatatable.row.add([
									rejecteddatatable.rows().count() + 1,
									value[len]["txtFldFnaId"],
									value[len]["txtFldCustName"],
									(value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									(value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									(value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
									value[len]["txtFldCustId"],
									value[len]["txtFldAdvStfId"],
									value[len]["txtFldAdvStfEmailId"],
									(value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
									value[len]["txtFldMgrStsApprBy"], value[len]["txtFldMgrApproveDate"], value[len]["txtFldMgrApproveRemarks"],
									value[len]["txtFldAdminStsApprBy"], value[len]["txtFldAdminApproveDate"], value[len]["txtFldAdminApproveRemarks"],
									value[len]["txtFldCompStsApprBy"], value[len]["txtFldCompApproveDate"], value[len]["txtFldCompApproveRemarks"],
									value[len]["txtFldMgrApproveStatus"], value[len]["txtFldAdminApproveStatus"], value[len]["txtFldCompApproveStatus"]
								]).draw(false);

								$("#optgrpRejected").append('<option value="' + fnaid + '">' + custname + '</option>');

								console.log("Reject" + "  " + fnaid + " " + custname)

							}
						}

						$('#cover-spin').hide(0);
					}
					if (key == "MANAGER_NO_RECORDS_FNA_DETAILS") {
						$('#cover-spin').hide(0);
						return false;
					}
				}
			}
		}

		let currFNAID = $("#hTxtFldFnaId").val();

		//alert(custname)
		//$("#dropDownQuickLinkFNAHist").val(custname.toUpperCase())
		//$("#dropDownQuickLinkFNAHist").val(currFNAID);

		$('#tblPending tbody').on('click', 'tr', function () {
			$(this).toggleClass('selected');
			let data = pendingdatatable.row(this).data();
			// getSelectedFNADetsNew(data);
			openEKYCNew(data);

			populateApprovedRec(data, acsmode);
		});

		$('#tblApproved tbody').on('click', 'tr', function () {
			$(this).toggleClass('selected');
			let data = approveddatatable.row(this).data();
			// getSelectedFNADetsNew(data);
			openEKYCNew(data);

			populateApprovedRec(data, acsmode);
		});

		$('#tblRejected tbody').on('click', 'tr', function () {
			$(this).toggleClass('selected');
			let data = rejecteddatatable.row(this).data();
			// getSelectedFNADetsNew(data);

			openEKYCNew(data);

			populateApprovedRec(data, acsmode);
		});

		pendingdatatable.rows().every(function (rowIdx, tableLoop, rowLoop) {
			let data = this.data();
			if (data[1] == currFNAID) {

				$(this).addClass('selected');

				$("#suprevFollowReason").val("")
				$("#diManagerSection").find('input:radio').prop('checked', false);

				$("#txtFldMgrSignDateDeclarepage").prop('readOnly', true);

				$("#diManagerSection").find(":input").removeAttr("disabled");
				$("#diManagerSection").removeClass("disabledsec")

				$("#txtFldMgrStsApprBy").val("");
				$("#txtFldAdminStsApprBy").val("");
				$("#txtFldCompStsApprBy").val("");

				//						$("#hdnSessFnaId").val(data[1] );
				//						callBodyInit();

				getAllSign(data[1]);

				openEKYCNew(data);

				populateApprovedRec(data, acsmode);

			} else {
				$(this).removeClass('highlight');
			}
		});

		approveddatatable.rows().every(function (rowIdx, tableLoop, rowLoop) {
			let data = this.data();

			if (data[1] == currFNAID) {
				$(this).addClass('selected');

				$("#suprevFollowReason").val("")
				$("#diManagerSection").find('input:radio').prop('checked', false);

				$("#txtFldMgrSignDateDeclarepage").prop('readOnly', true);

				$("#diManagerSection").find(":input").removeAttr("disabled");
				$("#diManagerSection").removeClass("disabledsec")

				$("#txtFldMgrStsApprBy").val("");
				$("#txtFldAdminStsApprBy").val("");
				$("#txtFldCompStsApprBy").val("");

				//							$("#hdnSessFnaId").val(data[1] );
				//							callBodyInit();

				getAllSign(data[1]);

				openEKYCNew(data);

				populateApprovedRec(data, acsmode);

			} else {
				$(this).removeClass('highlight');
			}
		});

		rejecteddatatable.rows().every(function (rowIdx, tableLoop, rowLoop) {
			let data = this.data();

			if (data[1] == currFNAID) {

				$(this).addClass('selected');

				$("#suprevFollowReason").val("")
				$("#diManagerSection").find('input:radio').prop('checked', false);

				$("#txtFldMgrSignDateDeclarepage").prop('readOnly', true);

				$("#diManagerSection").find(":input").removeAttr("disabled");
				$("#diManagerSection").removeClass("disabledsec")

				$("#txtFldMgrStsApprBy").val("");
				$("#txtFldAdminStsApprBy").val("");
				$("#txtFldCompStsApprBy").val("");

				//							$("#hdnSessFnaId").val(data[1] );
				//							callBodyInit();

				getAllSign(data[1]);

				openEKYCNew(data);

				populateApprovedRec(data, acsmode);

			} else {
				$(this).removeClass('highlight');
			}
		});

		$('#tblApproved').on('mousemove', 'tr', function (e) {
			let rowData = approveddatatable.row(this).data();

			let info = '<h6>' + rowData[1] + '<br/><small>(Click the row to view the details)</small></h6><small><strong>' + rowData[3] + '</strong> by(Manager) : <strong>' + rowData[10] + '</strong>' +
				'<hr/>';
			if (rowData[4] != "Pending") {
				info += ' <strong>' + rowData[4] + '</strong> by(Admin) : ' + rowData[12] + '<hr/>';
			}
			if (rowData[5] != "Pending") {
				info += '<strong>' + rowData[5] + '</strong> by(Compliance) : ' + rowData[14] + '<hr/>';
			}

			info += '</small>';

			$("#tooltip").html(info).animate({ left: e.pageX, top: e.pageY }, 1);
			if (!$("#tooltip").is(':visible')) $("#tooltip").show();
		})

		$('#tblApproved').on('mouseleave', function (e) {
			$("#tooltip").hide();
		});

		$('#tblRejected').on('mousemove', 'tr', function (e) {
			let rowData = rejecteddatatable.row(this).data();

			let info = '<h6>' + rowData[1] + '</h6><small>(Click the row to view the details)</small><br/><small><strong>' + rowData[3] + '</strong> by(Manager) : <strong>' + rowData[10] + '</strong>' +
				'<hr/>';
			if (rowData[4] != "Pending") {
				info += ' <strong>' + rowData[4] + '</strong> by(Admin) : ' + rowData[12] + '<hr/>';
			}
			if (rowData[5] != "Pending") {
				info += '<strong>' + rowData[5] + '</strong> by(Compliance) : ' + rowData[14] + '<hr/>';
			}

			info += '</small>';

			$("#tooltip").html(info).animate({ left: e.pageX, top: e.pageY }, 1);
			if (!$("#tooltip").is(':visible')) $("#tooltip").show();
		})

		$('#tblRejected').on('mouseleave', function (e) {
			$("#tooltip").hide();
		});

		/*$('#dropDownQuickLinkFNAHist').select2({
			width :'400',				
			 templateResult: formatDataSelect2
		})*/
	}
}

function fullViewFNA() {

}

function toggleApprovPolcySec(obj) {
	let btnLabel = $("#cardBtnTitle").text();
	if (btnLabel == "Click here to View Policy Docs.") {
		$("#policySubDocSec").removeClass("hide").addClass("show")
		$("#ApprovalSec").removeClass("show").addClass("hide");
		$("#polAllDocDownld").removeClass("hide").addClass("show");
		$("#cardSecTitle").text("Policy Sub. Docs");
		$("#cardBtnTitle").text("Click here to View Approval Sec.");
		$("#cardBtnTitle").attr("title", "View Approval Sec");
	} else if (btnLabel == "Click here to View Approval Sec.") {
		$("#policySubDocSec").removeClass("show").addClass("hide")
		$("#ApprovalSec").removeClass("hide").addClass("show")
		$("#cardSecTitle").text("Approval Sections");
		$("#cardBtnTitle").text("Click here to View Policy Docs.");
		$("#cardBtnTitle").attr("title", "View Policy Docs.");
		$("#polAllDocDownld").removeClass("show").addClass("hide");
	}
}