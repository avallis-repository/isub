
let fnaDetails = {};
$(document).ready(function () {
	$('#goPreviousArrow').hide();
	$("#wizard").steps({
		headerTag: "h2",
		bodyTag: "section",
		transitionEffect: "slideLeft",
		stepsOrientation: "vertical",
		titleTemplate: '<span class="badge badge--info"></span> #title#',
		onFinishing: function (event, currentIndex) {
			// alert("Fna details is verified");			
			return true;
		}
	});

	$("#addInsurTabId").remove();
	disableAllSection();
	loginSection(fnaId);
	let fnaId = $("#hdnSessFnaId").val();
	$("#viewFnaId").text(fnaId);
	//alert(fnaId);
	getApprovalFnaDets(fnaId);

	getFnaDetsList('APPROVE', 'tblViewApprove', 'apprTblHead', 'tblApproved');
	getFnaDetsList('PENDING', 'tblViewPending', 'pendTblHead', 'tblPending');
	getFnaDetsList('REJECT', 'tblViewReject', 'rejectTblHead', 'tblRejected');

	getAllUploadedFileById(fnaId);
});

function disableAllSection() {
	$('.readonly').find('input, textarea, select').attr('readonly', 'readonly');
	$("input[type=checkbox]").each(function () { $(this).attr("disabled", "disabled") });
	$("input[type=radio]").each(function () { $(this).attr("disabled", "disabled") });
	/*$("input[type=button]").each( function() {$(this).attr("disabled", "disabled")});
	 $("button").each( function() {$(this).attr("disabled", "disabled")});*/
	$("i").each(function () { $(this).attr("disabled", "disabled") });
	$("#cashFlowSec").prop('disabled', false);
	$("#assetLiableSec").prop('disabled', false);
}
function loginSection(fnaId) {
	let accRole = $("#hdnSessAccRole").val();
	if (accRole == "MANAGER") {
		showHideTab('admin', 'comp', 'mgr', 'pop1', 'mgrStatusRemarks');
		getQrCode('MANAGER', 'qrCodeManager', 'signLoaderManager');
		getAllSign(fnaId);
	} else if (accRole == "ADMIN") {
		showHideTab('mgr', 'comp', 'admin', 'pop2', 'adminRemarks');
	} else if (accRole == "COMP") {
		showHideTab('mgr', 'admin', 'comp', 'pop3', 'compRemarks');
	}
}

var approveMap = new Map();
var count = 0;

function getFnaDetsList(status, tableId, tblHeadId, dashTableId) {
	let accRole = $("#hdnSessAccRole").val();
	$("#" + tblHeadId).text(accRole + " Status");

	$.ajax({
		url: "fnadetails/getAllFnaDetsByStatus/" + status,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {

			setApproveTableData(data, tableId, status);
			setApproveDashBoard(data, dashTableId, status);

			approveMap.set(status, data.length);
			let progressId = document.getElementById(status.toLowerCase() + "ProgressBar");
			progressId.style.width = data.length + "%";
			// updateChart();
			if (count >= 2) {
				google.charts.load('current', {
					packages: ['corechart'],
					callback: drawChart
				});
			}
			count++;
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function setApproveTableData(data, tableId, status) {

	/*Data table config*/
	let approvetable = $("#" + tableId).DataTable({
		processing: true,
		language: {
			'loadingRecords': '&nbsp;',
			'processing': '<div class="spinner"></div>'
		},

		scrollX: true,
		scrollY: "200px",
		scroller: true,
		scrollCollapse: true,
		autoWidth: false,
		paging: true,
		pagingType: "simple",
		columnDefs: [
			{
				"targets": [4],
				"visible": false,
				"searchable": false
			}
		]
	});
	approvetable.clear().draw();
	let col0 = "", col1 = "", col2 = "", col3 = "", col4 = "";

	/*Add data to datatable*/
	for (let idx = 0; idx < data.length; idx++) {
		col0 = data[idx].fnaId;
		col1 = "Client Name";
		col2 = "Advisor";
		col3 = status;
		col4 = data[idx].fnaId;
		approvetable.row.add([col0, col1, col2, col3, col4]).draw(false);
	}

	/*Table click action*/
	$('#' + tableId + ' tbody').on('click', 'tr', function () {
		$(this).addClass("bg-success");
		let data = approvetable.row(this).data();

		let fnaId = data[4];
		$("#viewFnaId").text(fnaId);

		let modalId = tableId.replace("tblView", "modal");
		$("#" + modalId).modal("hide");

		viewApproveList(fnaId);

		$(this).removeClass("bg-success");
	});
}

function setApproveDashBoard(data, dashTableId, status) {
	for (let tblDtIdx = 0; tblDtIdx < data.length; tblDtIdx++) {
		let tableData = data[tblDtIdx];
		let rowData = "<tr title=MgrApproveBy:Salim&#013;MgrApproveDate:2020-02-01&#013;AdminApproveBy:Salim&#013;AdminApproveDate:2020-02-01&#013;CompApproveBy:Salim&#013;CompApproveDate:2020-02-01><td>"
			+ "</td><td>" + tableData.fnaId + "</td><td>Client Name</td>"
			+ "<td></td> <td></td><td></td><td>" + tableData.custDetails.custId + "</td>"
			+ "<td>" + tableData.advstfId + "</td><td>" + tableData.custDetails.emailId + "</td><td></td></tr>";
		$("#" + dashTableId).append(rowData);
	}
}

function showHideTab(tab1, tab2, tab3, popId, slctRem) {
	$("#" + tab1 + "Tab").removeClass("active");
	$("#" + tab2 + "Tab").removeClass("active");
	$("#" + tab3 + "Tab").addClass("active");
	$("#" + popId).addClass("show active");

	$("#" + tab3 + "ApproveStatus").prop('disabled', false);
	$("#" + slctRem).prop('disabled', false);
	$("#" + tab3 + "Btn").prop('disabled', false);
}

function showModal() {
	$("#mgrMailModal").modal("show");
}

var arrProdRecomSwtchData = ["swrepConflg", "swrepAdvbyflg", "swrepDisadvgflg", "swrepProceedflg"];

var arrAdvBasData = ["advrecReason", "advrecReason1", "advrecReason2", "advrecReason3"];

var arrAdvDeclrData = ["cdackAgree", "cdConsentApprch", "cdMrktmatEmailflg", "cdMrktmatPostalflg"];

function setApprovalFnaData(fnaDets) {
	fnaDetails = fnaDets;
	//Prod Recom Switch
	for (let prodIdx = 0; prodIdx < arrProdRecomSwtchData.length; prodIdx++) {
		if (fnaDets[arrProdRecomSwtchData[prodIdx]] == "Y") {
			$("#" + arrProdRecomSwtchData[prodIdx] + "Y").prop('checked', true);
		}
		if (fnaDets.swrepConfDets != undefined) {
			$("#swrepConfDets").val(fnaDets.swrepConfDets);
		}
	}

	//Advice & basis recomm
	for (let advIdx = 0; advIdx < arrAdvBasData.length; advIdx++) {
		$("#" + arrAdvBasData[advIdx]).val(fnaDets[arrAdvBasData[advIdx]]);
	}

	//Advisor declaration
	if (fnaDets.cdMrktmatPostalflg == "Y") {
		$("#option8").prop('checked', true);
	} if (fnaDets.cdMrktmatEmailflg == "Y") {
		$("#cdMrktmatEmailflg").prop('checked', true);
	}
	if (fnaDets.cdackAgree == "Y") {
		$("#radFldackAgree").prop('checked', true);
	}
	else if (fnaDets.cdackAgree == undefined) {
		$("#radFldackAgree").prop('checked', false);
		$("#radFldackPartial").prop('checked', false);
	}
	else { $("#radFldackPartial").prop('checked', true); }

	if (fnaDets.cdConsentApprch == "Y") {
		$("#option3").prop('checked', true);
	} else if (fnaDets.cdackAgree == undefined) {
		$("#option3").prop('checked', false);
		$("#option4").prop('checked', false);
	} else {
		$("#option4").prop('checked', true);
	}


	$("#cdackAdvrecomm").val(fnaDets.cdackAdvrecomm);

	setMgrApprovalFnaData(fnaDets);
}

//Approval section
function enableTxtArea() {
	let status = document.getElementById("mgrApproveStatus").value;
	if (status == "REJECT" || status == "PENDING" || status == "") {
		$("#mgrStatusRemarks").val("");
		$("#mgrStatusRemarks").prop("disabled", false);
		//document.getElementById("apprSecSignTabCnt").style.display="none";
		$("#mgrSignModal").modal("hide");
	} else {
		$("#mgrStatusRemarks").val("");
		$("#mgrStatusRemarks").prop("disabled", true);
		$("#apprSecSignTabCnt").prop("disabled", true);
		//document.getElementById("apprSecSignTabCnt").style.display="block";
		$("#mgrSignModal").modal("show");
	}
}

function enableAdminTxtArea() {
	let status = document.getElementById("adminApproveStatus").value;
	if (status == "REJECT" || status == "PENDING") {
		$("#adminRemarks").val("");
		$("#adminRemarks").prop("disabled", false);
	} else {
		$("#adminRemarks").val("");
		$("#adminRemarks").prop("disabled", true);
	}
}

function enableCompTxtArea() {
	let status = document.getElementById("compApproveStatus").value;
	if (status == "REJECT" || status == "PENDING") {
		$("#compRemarks").val("");
		$("#compRemarks").prop("disabled", false);
	} else {
		$("#compRemarks").val("");
		$("#compRemarks").prop("disabled", true);
	}
}

//Set and get Manager approval data

var arrApprMgrData = ["mgrApproveStatus", "mgrStatusRemarks", "mgrApproveDate"];
var arrCompData = ["compApproveStatus", "compRemarks", "compApproveDate"];
var arrAdminData = ["adminApproveStatus", "adminRemarks", "adminApproveDate"]

function getApprovalFnaData(fnaDets) {
	let roleAccess = $("#hdnSessAccRole").val();
	if (roleAccess == "MANAGER") {
		for (let apprIdx = 0; apprIdx < arrApprMgrData.length - 1; apprIdx++) {
			fnaDets[arrApprMgrData[apprIdx]] = document.getElementsByName(arrApprMgrData[apprIdx])[0].value;
		}
		fnaDets.mgrApproveDate = getCurrentDate();
	} else if (roleAccess == "ADMIN") {
		for (let admIdx = 0; admIdx < arrAdminData.length - 1; admIdx++) {
			fnaDets[arrAdminData[admIdx]] = document.getElementsByName(arrAdminData[admIdx])[0].value;
		}
		fnaDets.adminApproveDate = getCurrentDate();
	} else if (roleAccess == "COMP") {
		for (let compIdx = 0; compIdx < arrCompData.length - 1; compIdx++) {
			fnaDets[arrCompData[compIdx]] = document.getElementsByName(arrCompData[compIdx])[0].value;
		}
		fnaDets.compApproveDate = getCurrentDate();
	}
}

function setMgrApprovalFnaData(fnaDets) {

	let roleAccess = $("#hdnSessAccRole").val();
	//if(roleAccess  == "MANAGER"){
	setStatusInfo(fnaDets.mgrApproveStatus, "Mgr");
	for (let mgrIdx = 0; mgrIdx < arrApprMgrData.length - 1; mgrIdx++) {
		// enableTxtArea();
		document.getElementsByName(arrApprMgrData[mgrIdx])[0].value = fnaDets[arrApprMgrData[mgrIdx]];
	}

	//}else if(roleAccess == "ADMIN"){
	setStatusInfo(fnaDets.adminApproveStatus, "Admin");
	for (let adminIdx = 0; adminIdx < arrAdminData.length - 1; adminIdx++) {
		enableAdminTxtArea();
		document.getElementsByName(arrAdminData[adminIdx])[0].value = fnaDets[arrAdminData[adminIdx]];
	}

	//}else if(roleAccess == "COMP"){
	setStatusInfo(fnaDets.compApproveStatus, "Comp");
	for (let cmpIdx = 0; cmpIdx < arrCompData.length - 1; cmpIdx++) {
		enableCompTxtArea();
		document.getElementsByName(arrCompData[cmpIdx])[0].value = fnaDets[arrCompData[cmpIdx]];
	}
	//}
}

function setStatusInfo(approveStatus, role) {
	if (approveStatus == "APPROVE") {
		$("#iFont" + role).removeClass();
		$("#iFont" + role).addClass("fa fa-check-circle");
	} else if (approveStatus == "REJECT") {
		$("#iFont" + role).removeClass();
		$("#iFont" + role).addClass("fa fa-times-circle");
	} else if (approveStatus == "PENDING") {
		$("#iFont" + role).removeClass();
		$("#iFont" + role).addClass("fa fa-question-circle");
	}
}

function getCurrentDate() {
	let today = new Date();
	let dd = String(today.getDate()).padStart(2, '0');
	let mm = String(today.getMonth() + 1).padStart(2, '0');
	let yyyy = today.getFullYear();

	today = yyyy + '-' + mm + '-' + dd;
	return today;
}

//Send mail
function sendAuthEmail() {
	let email = document.getElementById("advMailId").value;
	let roleAccess = $("#hdnSessAccRole").val();
	document.getElementById("mailLoaderAdv").style.display = "block";

	if (roleAccess == "MANAGER") {
		let approveStatus = $("#mgrApproveStatus").val();
		let reason = $("#mgrStatusRemarks").val();
	} else if (roleAccess == "ADMIN") {
		let approveStatus = $("#adminApproveStatus").val();
		let reason = $("#adminRemarks").val();
	} else if (roleAccess == "COMP") {
		let approveStatus = $("#compApproveStatus").val();
		let reason = $("#compRemarks").val();
	}
	$.ajax({
		url: 'FnaSignature/sentAdvMail',
		type: 'POST',
		// async:false,
		data: {
			approveStatus: approveStatus,
			reason: reason,
			email: email,
			role: roleAccess
		},

		success: function (data) {

			$('.circle-loader').addClass('load-complete');
			$('.checkmark').toggle();
			$("#modalMsgLblAdv").html("Mail was sent !!!");
			updateApprovalFnaDets();
		},
		error: function (e) {
			console.log("ERROR : ", e);
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
}

function drawChart() {
	// init google table
	let dataTable = new google.visualization.DataTable({
		cols: [
			{ label: 'Status', type: 'string' },
			{ label: 'Result', type: 'number' }
		]
	});

	for (let key of approveMap.keys()) {
		let status = approveMap.get(key);
		dataTable.addRow([
			{ v: key },
			{ v: status }
		]);
	}
	let dataSummary = google.visualization.data.group(
		dataTable,
		[0],
		[{ 'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number' }]
	);

	let options = {
		title: 'Approval Status'
	};

	let chart = new google.visualization.PieChart(document.getElementById('pieChart'));
	chart.draw(dataSummary, options);
}

function updateChart() {

	let dps = chart.options.data[0].dataPoints;
	//var dps;
	let i = 0;
	for (let key of approveMap.keys()) {
		let status = approveMap.get(key);
		dps[i] = { y: status };
		i++
	}
	/*for (var i = 0; i < dps.length; i++) {
		deltaY = Math.round(2 + Math.random() *(-2-2));
		yVal = deltaY + dps[i].y > 0 ? dps[i].y + deltaY : 0;		
		dps[i] = {y: yVal};
	}*/
	chart.options.data[0].dataPoints = dps;
	chart.render();
}

function showSignModal() {
	$("#mgrSignModal").modal("show");
}

//View  list action
function viewApproveList(fnaId) {
	activeTab('detailspage');

	let fnaViewId = document.getElementById("viewFnaDets");
	fnaViewId.remove();

	$("#fnaViewCardBody").load("viewFnaDetails");

	$("#hdnSessFnaId").val(fnaId);

	//$("#fnaViewCardBody").load("https://internal-ekyc-ias.avallis.com/EKYC_IAS/prodSummary/getProductSummaryDets");
	/*Kyc Home*/
	/*getClientDetails(fnaId);
	getAllOtherPerDataById(fnaId);*/

	/*Financial wellness review*/

	//Get all dependent data
	getAllDepnData();
	//Get all cashflow and asset Data
	getCashAssetData(fnaId);

	/*Tax residency*/
	getFnaSelfSpouseDets(fnaId);

	/* Approval section */
	getApprovalFnaDets(fnaId);

	/* Signature */
	getAllSign(fnaId);

	/*Product Recommendation*/
	$("input:radio[name=recomPpBasrid]:first").prop('checked', true);

	//Select 2 Fields Initialization in Page6
	$('#recomPpName').select2({
		placeholder: "Select a Client Name",
		width: '100%',
		templateResult: loadSelect2ClntNameCombo,
		allowClear: true
	});

	$('#recomPpProdname').select2({
		placeholder: "Select a Product Name ",
		width: '100%',
		allowClear: true
	});

	$('#recomPpPrin').select2({
		placeholder: "Select an Insurance Company / Principal",
		width: '100%',
		templateResult: loadSelect2PrinCombo,
		allowClear: true
	});

	getSelfSpouseDets(fnaId);

	getFnaDets(fnaId);

	getAllProdRecommData(fnaId);

	// getAllUploadedFileById(fnaId);
}

function activeTab(tab) {
	$('.nav-tabs a[href="#' + tab + '"]').tab('show');
}

function getAllUploadedFileById(fnaId) {

	$.ajax({
		url: "CustomerAttachments/getAllUploadFileById/" + fnaId,
		type: "GET",
		// async:false,
		success: function (data) {
			loadDocJsnDataApprove(data);
			// alert("success");
		},
		error: function (data, textStatus, jqXHR) {
			// alert("error");
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
}

function loadDocJsnDataApprove(jsnData) {
	for (let jsnIdx = 0; jsnIdx < jsnData.length; jsnIdx++) {
		let doc = jsnData[jsnIdx];
		if (doc.attachCategName == "NRIC Document") {
			let strList = '<a href="#" class="list-group-item list-group-item-action p-2">' +
				'<div class="d-flex w-100 justify-content-between "><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Doc.Title : ' + doc.title + '</h6> <small><i class="fa fa-download cursor-pointer" style="color: #009A78;" onclick=downloadDoc(\"' + doc.documentData + '\",\"' + doc.fileType + '\") title="Click to Download Doc."></i></small></div>' +
				'<small class="font-sz-level6">File Name : ' + doc.fileName + '</small>  </a>';
			$("#dynaAttachNricList").append(strList);
		} else {
			let strList = '<a href="#" class="list-group-item list-group-item-action p-2">' +
				'<div class="d-flex w-100 justify-content-between "><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Doc.Title : ' + doc.title + '</h6> <small><i class="fa fa-download cursor-pointer" style="color: #009A78;" onclick=downloadDoc(\"' + doc.documentData + '\",\"' + doc.fileType + '\") title="Click to Download Doc."></i></small></div>' +
				'<small class="font-sz-level6">File Name : ' + doc.fileName + '</small>  </a>';
			$("#dynaAttachOthList").append(strList);
		}
	}
}

function downloadDoc(docData, fileType) {
	let arrrayBuffer = base64ToArrayBuffer(docData);
	let blob = new Blob([arrrayBuffer], { type: "application/" + fileType });
	let link = window.URL.createObjectURL(blob);
	window.open(link, "_blank");
}
