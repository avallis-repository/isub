$(document).ready(function () {
	let fnaId = $("#hdnSessFnaId").val();
	getFnaDets(fnaId);
	window.addEventListener("beforeunload", function (e) {
		// saveData();
	});
});

//Set and Get Product Switch Plan Recommendation
var arrProdRecomSwtchData = ["swrepConflg", "swrepAdvbyflg", "swrepDisadvgflg", "swrepProceedflg"];
function getFnaData(fnaDets) {
	for (let prodRecIdx = 0; prodRecIdx < arrProdRecomSwtchData.length; prodRecIdx++) {
		fnaDets[arrProdRecomSwtchData[prodRecIdx]] = $('input[name=' + arrProdRecomSwtchData[prodRecIdx] + ']').val();
	}
	fnaDets.swrepConfDets = $("#swrepConfDets").val();
}

function setFnaData(fnaDets) {
	for (let sProdRecIdx = 0; sProdRecIdx < arrProdRecomSwtchData.length; sProdRecIdx++) {
		if (fnaDets[arrProdRecomSwtchData[sProdRecIdx]] != "") {
			$("#" + arrProdRecomSwtchData[sProdRecIdx] + fnaDets[arrProdRecomSwtchData[sProdRecIdx]]).prop('checked', true);
			$("#" + arrProdRecomSwtchData[sProdRecIdx] + fnaDets[arrProdRecomSwtchData[sProdRecIdx]]).val(fnaDets[arrProdRecomSwtchData[sProdRecIdx]]);
			$("#" + arrProdRecomSwtchData[sProdRecIdx]).val(fnaDets[arrProdRecomSwtchData[sProdRecIdx]]);
		}
		$("#swrepConfDets").val(fnaDets.swrepConfDets);
	}
}

//SaveData
function saveData(obj, nextscreen, navigateflg, infoflag) {
	updateFnaDets(obj, nextscreen, navigateflg, infoflag);
}
