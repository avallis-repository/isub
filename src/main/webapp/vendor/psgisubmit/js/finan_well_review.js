$(function () {
	$('#fwrDepntflg').on('click', function () {
		if ($(this).is(":checked") == true) {
			$("#collapseDepend").removeClass("hide").addClass("show");
		} else {
			$("#collapseDepend").addClass("hide").removeClass("show");
		}
	});
	$('#fwrFinassetflg').on('click', function () {
		if ($(this).is(":checked") == true) {
			$("#collapsecashFlow").removeClass("hide").addClass("show");;
		} else {
			$("#collapsecashFlow").addClass("hide").removeClass("show");
		}
	});

	$('[data-toggle="pg4DependNote"]').popover({
		placement: 'top',
		html: true,
		content: function () {
			return $('#popover-pg4DependNote').html();
		}
	});

	$('[data-toggle="pg4CashFlwNote"]').popover({
		placement: 'top',
		html: true,
		content: function () {
			return $('#popover-pg4CashFlwNote').html();
		}
	});

	$('[data-toggle="pg4AssetNote"]').popover({
		placement: 'top',
		html: true,
		content: function () {
			return $('#popover-pg4AssetNote').html();
		}
	});

	$('[data-toggle="Pg4assetETANote"]').popover({
		placement: 'left',
		html: true,
		content: function () {
			return $('#popover-Pg4assetETANote').html();
		}
	});

	$('[data-toggle="Pg4assetETLNote"]').popover({
		placement: 'left',
		html: true,
		content: function () {
			return $('#popover-Pg4assetETLNote').html();
		}
	});

	//menuNavSave();
	window.addEventListener("beforeunload", function (e) {
		// saveData();
		//return (e || window.event).returnValue = "Your data is saved";
		/* var dependant={};
		 dependant=getDependantData(dependant);
		 
		 if(checkEmpty(dependant)){
		   return (e || window.event).returnValue = "Your data may be lost";
		 }*/
	});

	let fnaId = $("#hdnSessFnaId").val();

	getFnaDets(fnaId);
	//Get all dependent data
	getAllDepnData();
	//Get all cashflow and asset Data
	getCashAssetData(fnaId);
});

function checkEmpty(obj) {
	for (let key in obj) {
		/*if(key == "depnGender" && obj[key] == "Male"){
		   obj[key]="";
		}*/
		if (obj[key] != "") {
			return true;
		}
	}
	return false;
}

function getAllDepnData() {
	//document.getElementById('loader').style.display = 'block';
	pageLoaderShow();
	$.ajax({
		url: "FnaDependantDets/getAllData",
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			//show hide dependent image based on dependent List length
			var emptObjFlg = data.length;

			if (emptObjFlg == 0) {
				$("#NoDepnDetlsRow").removeClass("hide").addClass("show");
			} else if (emptObjFlg > 0) {
				$("#NoDepnDetlsRow").removeClass("show").addClass("hide");
			}
			for (let listItemIdx = 0; listItemIdx < data.length; listItemIdx++) {
				addListItem(data[listItemIdx]);
			}
			pageLoaderHide();
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});

}

function pageLoaderShow() {
	$(".page-loader").show();
	$(".overlay").show();
}
function pageLoaderHide() {
	$(".page-loader").hide();
	$(".overlay").hide();
}

function addListItem(dependent) {

	// gender validation
	let genIcon = "";
	var genColor = "";
	if (isEmpty(dependent.depnGender)) {
		genIcon = "";
		dependent.depnGender = "NIL"
	} else {
		if (dependent.depnGender == "Male") {
			genIcon = "fa fa-male imale";
		}
		else if (dependent.depnGender == "Female") {
			genIcon = "fa fa-female iFemale";
		}
	}

	//age validation
	if (isEmpty(dependent.depnAge)) {
		dependent.depnAge = "NIL"
	} else {
		dependent.depnAge = dependent.depnAge + " Yrs."
	}

	//yrs to sup validation
	if (isEmpty(dependent.depnYrsSupport)) {
		dependent.depnYrsSupport = "NIL"
	} else {
		dependent.depnYrsSupport = dependent.depnYrsSupport + " Yrs."
	}

	//Relationship validation
	if (isEmpty(dependent.depnRelationShip)) {
		dependent.depnRelationShip = "NIL"
	}

	//occupation validation 
	if (isEmpty(dependent.depnOccp)) {
		dependent.depnOccp = "NIL"
	}

	let listGroup = '<a id=' + dependent.depnId + ' href="#" class="list-group-item list-group-item-action flex-column align-items-start p-1">'
		+ '<div class="d-flex w-100 justify-content-between">'
		+ '<h6 class="mb-1 text-primaryy font-sz-level6 bold">' + dependent.depnName + '</h6><input type="hidden" id="htxtdepnName" name="htxtdepnName" value="' + dependent.depnName + '">'
		+ '<small id="' + dependent.depnId + '"><i class="fa fa-pencil-square-o mr-2" aria-hidden="true" title="Edit Dependent data" style="color:blue;" onclick="getEditData(this);"></i>&nbsp;&nbsp;<i class="fa fa-trash checkdb mr-1" aria-hidden="true" title="Delete Dependent data" style="color: red;" onclick="dltData(this);"></i></small>'
		+ '</div>'
		+ ' <div class="row">'

		+ ' <div class="col-md-12" id="FirstRow">'
		+ '<div class="row">'
		+ '  <div class="col-md-4" id="gender">'
		+ '<span class="font-sz-level7  text-custom-color-gp"><strong>Gender&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal"><i class="' + genIcon + '" aria-hidden="true"></i> <span id="gndrtxt">&nbsp;' + dependent.depnGender + '</span></span>'
		+ '</div>'
		+ '<div class="col-md-3" id="age">'
		+ ' <span class="font-sz-level7  text-custom-color-gp"><strong>Age&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">' + dependent.depnAge + '</span>'
		+ '</div>'
		+ '<div class="col-md-5" id="yrsSup">'
		+ '<span class="font-sz-level7  text-custom-color-gp"><strong >No of Yrs to Support&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">' + dependent.depnYrsSupport + ' </span>'
		+ '</div>'

		+ '</div>'
		+ '</div>'

		+ '<div class="col-md-12" id="secondRow">'
		+ ' <div class="row">'
		+ '<div class="col-md-4" id="relation">'
		+ '<span class="font-sz-level7  text-custom-color-gp"><strong>RelationShip&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">' + dependent.depnRelationShip + '</span>'
		+ '</div>'
		+ '<div class="col-md-8" id="occupation">'
		+ '<span class="font-sz-level7  text-custom-color-gp"><strong>Occupation&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">' + dependent.depnOccp + '.</span>'
		+ '</div>'
		+ '</div>'
		+ '</div>'
		+ '</div>';

	let ListLen = $("#addlist").find("#" + dependent.depnId).length;
	if (ListLen == 0) {
		$("#NoDepnDetlsRow").removeClass("show").addClass("hide");
		$("#addlist").append(listGroup);
	}
}

//Delete Data
function dltData(thisobj) {

	let isSigned = $("#hTxtFldSignedOrNot").val();
	
	if(isSigned == "Y") {
		enableReSignIfRequiredforEdtDeltAction();
	} else {

		let depnId = $(thisobj).parent().attr("id");
		let depnName = $("#" + depnId).find('input:hidden[name=htxtdepnName]').val();

		const swalWithBootstrapButtons = Swal.mixin({
			customClass: {
				confirmButton: 'btn btn-success',
				cancelButton: 'btn btn-danger mr-3'
			},
			buttonsStyling: false
		})

		swalWithBootstrapButtons.fire({
			title: 'Are you sure?',
			text: "You will not be able to recover this Dependent Details : (" + depnName + ")",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonText: 'Yes, delete it!',
			cancelButtonText: 'No, cancel!',
			reverseButtons: true
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: "FnaDependantDets/deleteData/" + depnId,
					type: "DELETE",
					dataType: "json",
					contentType: "application/json",
					// async:false,
					success: function (response) {
						$(thisobj).parents("#" + depnId).remove();
						let totlLst = $("#addlist").children().length;

						if (totlLst == 0) {
							$("#NoDepnDetlsRow").removeClass("hide").addClass("show");
						}
						swalWithBootstrapButtons.fire("Deleted!", "Your Dependent : (" + depnName + ")  Details has been deleted.", "success");
						//checkdb onchange function for delete operation
						enableReSignIfRequiredforEdtDeltAction();
					},
					error: function (xhr, textStatus, errorThrown) {
						Swal.fire({
							icon: 'error',
							text: 'Please Try again Later or else Contact your System Administrator',
						});
					}
				});
			} else if (
				/* Read more about handling dismissals below */
				result.dismiss === Swal.DismissReason.cancel
			) {
				swalWithBootstrapButtons.fire(
					"Cancelled", "Your Dependent  (" + depnName + ") Details is safe :)", "error")
			}
		})
	}
}//End of Delete Each dependent  Details...

//Edit dependent data
function getEditData(thisobj) {

	//checkdb onchange function for edit operation
	enableReSignIfRequiredforEdtDeltAction();

	clearData();
	setEdtFun();

	let depnId = $(thisobj).parent().attr("id");

	$.ajax({
		url: "FnaDependantDets/getDataById/" + depnId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (dependent) {
			setDependentData(dependent);
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function setEdtFun() {
	$("#hdnMode").val("E");
	$("#btnSaveDepnData").attr("value", "Edit Details");
	$("#btnSaveDepnData").removeClass("btn-success").addClass("btn-primary");
}

function setInsFun() {
	$("#hdnMode").val("I");
	$("#btnSaveDepnData").attr("value", "Add Details");
	$("#btnSaveDepnData").removeClass("btn-primary").addClass("btn-success");
}

$("#depnGender").click(function () {
	getGender();
});
function getGender() {
	let gender = "Male";
	let attr = $("#depnGender").attr("class");
	if (attr.includes("active")) {
		gender = "Male";
	} else {
		gender = "Female";
	}
}

var arrDependentDet = ["depnName", "depnAge", "depnYrsSupport", "depnRelationShip", "depnOccp", "depnId"];
function setDependentData(dependent) {

	/*if(dependent.depnGender != "Male"){
		$('#depnGender').addClass('active');
	}else{
		$('#depnGender').removeClass('active');
	}*/
	let gender = dependent.depnGender;
	if (gender == "Male") {
		$("#radBtnDepnmale").prop("checked", true)
		$('input[name="depnGender"]:checked').val(gender);
	}
	else if ((gender == "Female")) {
		$("#radBtnDepnfemale").prop("checked", true);
		$('input[name="depnGender"]:checked').val(gender);
	} else {
		$('input[name="depnGender"]:checked').val("");
	}

	for (let depIdx = 0; depIdx < arrDependentDet.length; depIdx++) {
		document.getElementsByName(arrDependentDet[depIdx])[0].value = dependent[arrDependentDet[depIdx]];
	}
}
function getDependantData(dependent) {
	for (let depndIdx = 0; depndIdx < arrDependentDet.length; depndIdx++) {
		console.log(arrDependentDet[depndIdx])
		dependent[arrDependentDet[depndIdx]] = document.getElementsByName(arrDependentDet[depndIdx])[0].value;
	}

	let depnGndr = $('input[name="depnGender"]:checked').val();
	dependent.depnGender = depnGndr;

	return dependent;
}

function saveDependantData() {
	//Name validation
	let depnName = document.getElementsByName("depnName")[0].value;
	if (depnName == "") {
		$("#depName").addClass("errorclz");
		$("#depnNameError").show();
		$("#depName").focus();
		return false;
	}
	$("#depName").removeClass("errorclz");
	$("#depnNameError").hide();

	let dependent = {};
	dependent = getDependantData(dependent);

	//alert(JSON.stringify(dependent));
	$.ajax({
		url: "FnaDependantDets/saveData",
		type: "POST",
		data: JSON.stringify(dependent),
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			$("#" + data.depnId).remove();
			addListItem(data);
			clearData();

			if (page_wise_err) {
				page_wise_err = false;
				$("#btntoolbarsign").trigger("click");
			}
		},
		error: function (data) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function clearData() {
	for (let dependIdx = 0; dependIdx < arrDependentDet.length; dependIdx++) {
		document.getElementsByName(arrDependentDet[dependIdx])[0].value = "";
	}

	$("#radBtnDepnmale").prop("checked", false)
	$("#radBtnDepnfemale").prop("checked", false);
	$('input[name="depnGender"]:checked').val("");

	// set Insert mode after clear form Details
	setInsFun();

	// hide depn error msg --> when it shows in screen
	$("#depName").removeClass("err-fld");
	$("#depnNameError").addClass("hide").removeClass("show");

	// hide age and yrs support field error msg
	$("#depnAge").removeClass("err-fld");
	$("#ageErrMsg").hide();

	$("#depnYrsSupport").removeClass("err-fld");
	$("#yrsErrMsg").addClass("hide").removeClass("show");

	$("#span_collapseDepend").remove();

	removeErrFld($("#collapseDepend"));
}

//Validation
function isValidName() {
	let depnName = document.getElementsByName("depnName")[0].value;
	if (depnName == "") {
		$("#depName").addClass("errorclz");
		$("#depnNameErrMsg").show();
		return false;
	}
	$("#depName").removeClass("errorclz");
	$("#depnNameErrMsg").hide();
}

function isNumber(event, errMsgId) {
	event = (event) ? event : window.event;
	let charCode = (event.which) ? event.which : event.keyCode;
	if (charCode > 31 && (charCode < 48 || charCode > 57)) {
		//$("#depName").addClass("errorclz");
		// $("#numErrMsg").show();
		$("#" + errMsgId).show();
		return false;
	}
	$("#" + errMsgId).hide();
	return true;
}

//CashFlow
function calcFinCommit(txtobj, op) {
	let txtval = Number(txtobj.value);
	//Self
	let sa = $("#cfSelfAnnlinc").val()
	let sb = $("#cfSelfCpfcontib").val();
	let sc = $("#cfSelfEstexpense").val();

	let stot = Number(sa) - Number(sb) - Number(sc);

	$("#cfSelfSurpdef").val(stot);
	//Spouse
	let spa = $("#cfSpsAnnlinc").val()
	let spb = $("#cfSpsCpfcontib").val();
	let spc = $("#cfSpsEstexpense").val();
	let sptot = Number(spa) - Number(spb) - Number(spc);

	$("#cfSpsSurpdef").val(sptot);
	//Combined
	let sfa = $("#cfFamilyAnnlinc").val()
	let sfb = $("#cfFamilyCpfcontib").val();
	let sfc = $("#cfFamilyEstexpense").val();
	let sftot = Number(sfa) - Number(sfb) - Number(sfc);
	$("#cfFamilySurpdef").val(sftot);

}
//asset & Liablities
function calcCombined(txtobj) {
	let parrow = txtobj.parentNode.parentNode;
	/* var client = parrow.cells[1].childNodes[0];//alSelfTotasset
	 var spouse = parrow.cells[2].childNodes[0]//alSpsTotasset
	 var combined = parrow.cells[3].childNodes[0]//alFamilyTotasset
	*/
	//poovathi change field Type (Table to TextField) on 12-11-2020
	let client = $("#alSelfTotasset");//alSelfTotasset
	let spouse = $("#alSpsTotasset");//alSpsTotasset
	let combined = $("#alFamilyTotasset");//alFamilyTotasset

	combined.value = Number(client.value) + Number(spouse.value)

	let sfa = $("#cfFamilyAnnlinc").val()
	let sfb = $("#cfFamilyCpfcontib").val();
	let sfc = $("#cfFamilyEstexpense").val();
	let sftot = Number(sfa) - Number(sfb) - Number(sfc);

	$("#cfFamilySurpdef").val(sftot);


	let sa2 = $("#alFamilyTotasset").val()
	let sb2 = $("#alFamilyTotliab").val()
	$("#alFamilyNetasset").val(Number(sa2) - Number(sb2))
}


function calcAsset(txtobj) {
	let txtval = Number(txtobj.value)

	let sa = $("#alSelfTotasset").val()
	let sa1 = $("#alSpsTotasset").val()
	let sa2 = $("#alFamilyTotasset").val()

	let sb = $("#alSelfTotliab").val()
	let sb1 = $("#alSpsTotliab").val();
	let sb2 = $("#alFamilyTotliab").val()

	$("#alSelfNetasset").val(Number(sa) - Number(sb))
	$("#alSpsNetasset").val(Number(sa1) - Number(sb1))
	$("#alFamilyNetasset").val(Number(sa2) - Number(sb2))
}

//Save cashflow and Assert details

var arrExpendData = ["expdId", "cfSelfAnnlinc", "cfSelfCpfcontib", "cfSelfEstexpense", "cfSelfSurpdef",
	"cfSpsAnnlinc", "cfSpsCpfcontib", "cfSpsEstexpense", "cfSpsSurpdef",
	"cfFamilyAnnlinc", "cfFamilyCpfcontib", "cfFamilyEstexpense", "cfFamilySurpdef",
	"alSelfTotasset", "alSelfTotliab", "alSelfNetasset",
	"alSpsTotasset", "alSpsTotliab", "alSpsNetasset",
	"alFamilyTotasset", "alFamilyTotliab", "alFamilyNetasset"];

function saveData(obj, nextscreen, navigateflg, infoflag) {

	let expendDets = {};
	for (let expIdx = 0; expIdx < arrExpendData.length; expIdx++) {
		expendDets[arrExpendData[expIdx]] = document.getElementsByName(arrExpendData[expIdx])[0].value;
	}

	//alert(JSON.stringify(expendDets));
	$.ajax({
		url: "ExpendDets/saveData",
		type: "POST",
		data: JSON.stringify(expendDets),
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			if (data && data.expdId) {
				$("#expdId").val(data.expdId);
			}
			if (navigateflg) {
				// update fna details
				updateFnaDets(obj, nextscreen, navigateflg, infoflag);
			}
		},
		error: function (data) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

//Get Cashflow and Asset data
function getCashAssetData(fnaId) {

	$.ajax({
		url: "ExpendDets/getDataById/" + fnaId,
		type: "GET",
		dataType: "text",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			if (data) {
				setCashFlowAndAssetData(JSON.parse(data));
			}
		},
		error: function (xhr, textStatus, errorThrown) {
			//alert("Error");
			Swal.fire({
				icon: 'error',
				//title: 'Something went wrong!',
				text: 'Please Try again Later or else Contact your System Administrator',
				//footer: '<a href>Please Contact Administrator?</a>'
			});
		}
	});
}

function setCashFlowAndAssetData(expendData) {
	for (let arrExpendIdx = 0; arrExpendIdx < arrExpendData.length; arrExpendIdx++) {
		document.getElementsByName(arrExpendData[arrExpendIdx])[0].value = expendData[arrExpendData[arrExpendIdx]];
	}
}

//Get fna data
function getFnaData(fnaDets) {
	let assetFlg = $('input[name="fwrFinassetflg"]:checked').val();
	let depnFlg = $('input[name="fwrDepntflg"]:checked').val();
	if (assetFlg == "Y") {
		fnaDets.fwrFinAssetFlg = assetFlg;
	} else {
		fnaDets.fwrFinAssetFlg = "N";
	}
	if (depnFlg == "Y") {
		fnaDets.fwrDepntFlg = depnFlg;
	} else {
		fnaDets.fwrDepntFlg = "N";
	}
}

//Set fna data
function setFnaData(fnaDetails) {
	if (fnaDetails.fwrFinAssetFlg == "Y") {
		$("#fwrFinassetflg").trigger("click");
	} if (fnaDetails.fwrDepntFlg == "Y") {
		$("#fwrDepntflg").trigger("click");
	}
}

$(".depnchkMand").bind("change", function () {
	let thisfld = $(this).val();
	if (thisfld.length > 0) {
		$("#depnNameError").hide();
		$("#depName").removeClass("errorclz");
	}
});

$("#depnAge").on("blur", function() {
	$("#ageErrMsg").hide();
});
