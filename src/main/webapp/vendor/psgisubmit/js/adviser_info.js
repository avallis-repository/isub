$('#cover-spin').hide();

userType = userType ? userType : $("#hTxtFldUserType").val();
if (userType !== 'Admin') {
	$("#adviserLink").hide();
	window.history.go(-1);
}

 

let adviserTable;
$(function() {
	adviserTable = $('#adviserTable').DataTable({
		scrollX: false,
		scrollY: "600px",
		scroller: true,
		scrollCollapse: true,
		autoWidth: false
	});
	
	listAdvisers();
	
	$("#tBodyAdviser").on("click", "tr", function() {
		if (adviserTable) {
			$("#btnUpdateAdviser").prop("disabled", false);
			$("#btnDeleteAdviser").prop("disabled", false);
			
			let adviserData = adviserTable.row(this).data();
			console.log("adviserData: " + adviserData);
			
			$("#adviserId").val(adviserData[4]);
			$("#adviserName").val(adviserData[0]);
			$("#adviserEmail").val(adviserData[1]);
			$("#hTxtFldTenantId").val(adviserData[2]);
			
			if (adviserData[3]) {
				$("#hTxtFldUserLoginData").val(adviserData[3]);
				
				let userLoginData = JSON.parse(adviserData[3]);
				$("#adviserPassword").val(userLoginData.password);
			} else {
				$("#hTxtFldUserLoginData").val("");
				$("#adviserPassword").val("");
			}
		}
	});
	
	$("#adviserPassword").on("keydown", function() {
		$("#adviserPasswordError").addClass("hide").removeClass("show");
		$("#adviserPassword").removeClass('err-fld');
	});
});

function listAdvisers() {
	$.ajax({
		type: "GET",
		url: "masterLogin/listAdvisers",
		beforeSend: function () {
			$('#cover-spin').show();
		},
		success: function(result) {
			let advisers = JSON.parse(result);
			if (advisers) {
				adviserTable.clear().draw();
				let advId = "", advName = "", advEmail = "", tenantId = "", userLoginData = "";
				for(let adviserIndex = 0; adviserIndex < advisers.length; adviserIndex++) {
					advName = advisers[adviserIndex].adviserName;
					advEmail = advisers[adviserIndex].adviserEmailId;
					tenantId = advisers[adviserIndex].tenantId;
					userLoginData = advisers[adviserIndex].userLogin;
					advId = advisers[adviserIndex].adviserId;
					adviserTable.row.add([ advName, advEmail, tenantId, userLoginData, advId]).draw(false);
				}
				$("#tBodyAdviser tr").css("cursor", "pointer");
				
				setTimeout(function () {
					adviserTable.columns.adjust();
					$('#cover-spin').hide();
				}, 200);
			}
		},
		complete: function () {
			$('#cover-spin').hide();
		},
		error: function(errorRes) {
			$('#cover-spin').hide();
			console.log(errorRes);
		}
	});
}

function updateAdviserInfo() {
	let userLoginDataStr = $("#hTxtFldUserLoginData").val();
	
	let advPswd = $("#adviserPassword").val();
	
	if (!advPswd) {
		$("#adviserPasswordError").addClass("show").removeClass("hide");
		$("#adviserPassword").addClass('err-fld');
		$("#adviserPassword").focus();
		return;
	}
	
	if (userLoginDataStr) {
		let userLoginData = JSON.parse(userLoginDataStr);
		userLoginData.password = advPswd;
		$.ajax({
			type: "PUT",
			url: "masterLogin/updateAdviserInfo",
			data: JSON.stringify(userLoginData),
			contentType: "application/json",
			beforeSend: function () {
				$('#cover-spin').show();
			},
			success: function(result) {
				if (result === 'Updated') {
					Swal.fire({
						icon: 'success',
						text: 'Adviser password is updated!'
					}).then((result) => {
						if (result.isConfirmed) {
							window.location.reload();
						}
					});
				} else {
					Swal.fire({
						icon: 'error',
						text: 'Adviser password is not updated!'
					});
				}
			},
			complete: function () {
				$('#cover-spin').hide();
			},
			error: function(errorRes) {
				$('#cover-spin').hide();
				console.log("error result: " + errorRes);
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	} else {
	
		let userLogin = {};
		userLogin.tenantId = {};
		userLogin.tenantId.tenantId = $("#hTxtFldTenantId").val();
		userLogin.advStfId = {};
		userLogin.advStfId.advStfId = $("#adviserId").val();
		userLogin.emailId = $("#adviserEmail").val();
		userLogin.password = advPswd;
		userLogin.userId = "";
	
		$.ajax({
			type: "POST",
			url: "masterLogin/createAdviserInfo",
			data: JSON.stringify(userLogin),
			contentType: "application/json",
			beforeSend: function () {
				$('#cover-spin').show();
			},
			success: function(result) {
				if (result === 'Created') {
					Swal.fire({
						icon: 'success',
						text: 'Adviser password is created!',
					}).then((result) => {
						if (result.isConfirmed) {
							window.location.reload();
						}
					});
				} else {
					Swal.fire({
						icon: 'error',
						text: 'Adviser password is not created!'
					});
				}
			},
			complete: function () {
				$('#cover-spin').hide();
			},
			error: function(errorRes) {
				$('#cover-spin').hide();
				console.log("error result: " + errorRes);
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	}
}

function deleteAdviserInfo() {
	Swal.fire({
		text: "Do you want to delete the Adviser login?",
		icon: 'question',
		allowOutsideClick: false,
		allowEscapeKey: false,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, Delete it!'
	}).then((result) => {
		if (result.isConfirmed) {
			let userLoginData = $("#hTxtFldUserLoginData").val();
			if (userLoginData) {
				let userLoginId = JSON.parse(userLoginData).userId;
				if (userLoginId) {
					$.ajax({
						type: "DELETE",
						url: "masterLogin/deleteAdviserInfo/" + userLoginId,
						beforeSend: function () {
							$('#cover-spin').show();
						},
						success: function(result) {
							console.log("delete adviser result: " + result);
							if (result === 'Deleted') {
								Swal.fire({
									icon: 'success',
									text: 'Adviser login is deleted!',
								}).then((result) => {
									if (result.isConfirmed) {
										window.location.reload();
									}
								});
							}
						},
						complete: function () {
							$('#cover-spin').hide();
						},
						error: function(errorRes) {
							$('#cover-spin').hide();
							console.log("error result: " + errorRes);
							Swal.fire({
								icon: 'error',
								text: 'Please Try again Later or else Contact your System Administrator',
							});
						}
					});
				}
			}
		}
	});
}