$(document).ready(function () {
	$('#goPreviousArrow').hide();
	$("#wizard").steps({
		headerTag: "h2",
		bodyTag: "section",
		transitionEffect: "slideLeft",
		stepsOrientation: "vertical",
		titleTemplate: '<span class="badge badge--info"></span> #title#',
		onFinishing: function (event, currentIndex) {
			updateApprovalFnaDets();
			return true;
		}
	});

	$("#addInsurTabId").remove();
	disableAllSection();
	loginSection();

	/*Kyc Home*/
	let fnaId = $("#hdnSessFnaId").val();
	getClientDetails(fnaId);
	getAllOtherPerDataById(fnaId);
	/*var fnaId=$("#hdnSessFnaId").val();
	$("#viewFnaId").text(fnaId);
	//alert(fnaId);
   getApprovalFnaDets(fnaId);

	getClientDetails(fnaId,"Y");
	getAllOtherPerDataById(fnaId);*/

	$('#dependentSec').on('click', function () {
		$(this).parent().find('a').trigger('click');
		$("#collapseDepend").removeClass("hide").addClass("show");
		//$("#collapseSec2").removeClass("show").addClass("hide");
	});
	$('#cashFlowSec').on('click', function () {
		$(this).parent().find('a').trigger('click');
		$("#collapsecashFlow").removeClass("hide").addClass("show");
		// $("#collapseSec1").removeClass("show").addClass("hide");
	});

	$('[data-toggle="pg4CashFlwNote"]').popover({
		placement: 'top',
		html: true,
		content: function () {
			return $('#popover-pg4CashFlwNote').html();
		}
	});

	$('[data-toggle="pg4AssetNote"]').popover({
		placement: 'top',
		html: true,
		content: function () {
			return $('#popover-pg4AssetNote').html();
		}
	});

	$('[data-toggle="Pg4assetETANote"]').popover({
		placement: 'left',
		html: true,
		content: function () {
			return $('#popover-Pg4assetETANote').html();
		}
	});

	$('[data-toggle="Pg4assetETLNote"]').popover({
		placement: 'left',
		html: true,
		content: function () {
			return $('#popover-Pg4assetETLNote').html();
		}
	});
});
