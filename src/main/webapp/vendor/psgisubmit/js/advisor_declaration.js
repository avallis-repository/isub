
$(document).ready(function () {
	// selfInit('selfDiagram','selfSavedDiagram');
	//spsInit('spsDiagram','spsSavedDiagram');
	//advInit('advDiagram','advSavedDiagram');
	//mgrInit('mgrDiagram','mgrSavedDiagram');

	let fnaId = $("#hdnSessFnaId").val();
	//getAllSign(fnaId);

	getFnaDets(fnaId);

	window.addEventListener("beforeunload", function (e) {
		// saveData();
	});


});

//Set and Get Advisor Declaration
var arrAdvDeclrData = ["cdackAgree", "cdConsentApprch", "cdMrktmatEmailflg", "cdMrktmatPostalflg"];
function getFnaData(fnaDets) {
	for (let declrIdx = 0; declrIdx < arrAdvDeclrData.length; declrIdx++) {
		fnaDets[arrAdvDeclrData[declrIdx]] = $('input[name=' + arrAdvDeclrData[declrIdx] + ']:checked').val();
	}

	fnaDets.cdackAdvrecomm = $("#cdackAdvrecomm").val();

}
function setFnaData(fnaDets) {
	/*for(var i=2;i<arrAdvDeclrData.length;i++){
		if(fnaDets[arrAdvDeclrData[i]] == "Y"){
			$("#"+arrAdvDeclrData[i]).prop('checked',true);
		}
	}*/

	if (fnaDets.cdMrktmatPostalflg == "Y") {
		$("#cdMrktmatPostalflg").prop('checked', true);
		$("#cdMrktmatPostalflg").val(fnaDets.cdMrktmatPostalflg);
	}

	if (fnaDets.cdMrktmatEmailflg == "Y") {
		$("#cdMrktmatEmailflg").prop('checked', true);
		$("#cdMrktmatEmailflg").val(fnaDets.cdMrktmatEmailflg);
	}

	if (fnaDets.cdackAgree == "agree") {
		$("#radFldackAgree").prop('checked', true);
	} else if (fnaDets.cdackAgree == "partial") {
		$("#radFldackPartial").prop('checked', true);
	}

	if (fnaDets.cdConsentApprch == "F2F") {
		$("#cdConsentApprchF").prop('checked', true);
	} else if (fnaDets.cdConsentApprch == "NONF2F") {
		$("#cdConsentApprchNF").prop('checked', true);
	}

	$("#cdackAdvrecomm").val(fnaDets.cdackAdvrecomm);
}

//SaveData
function saveData(obj, nextscreen, navigateflg, infoflag) {
	updateFnaDets(obj, nextscreen, navigateflg, infoflag);
}

function consentFld() {
	let isSigned = $("#hTxtFldSignedOrNot").val();
	if (page_wise_err && isSigned != "Y") {
		page_wise_err = false;
		saveData();
		setTimeout(function () {
			$("#btntoolbarsign").trigger("click");
		}, 1200);
	}
}