
$(function () {
	$('#goPreviousArrow').hide();
	$("#wizard").steps({
		headerTag: "h2",
		bodyTag: "section",
		transitionEffect: "slideLeft",
		stepsOrientation: "vertical",
		titleTemplate: '<span class="badge badge--info"></span> #title#',
		onFinishing: function (event, currentIndex) {
			//Update Fna Details
			updateFnaDets(null, "kycHome", true, false);

			// window.location.href = "kycHome";
			// return true;
		}
	});

	let fnaId = $("#hdnSessfnaId").val();
	getFnaDets(fnaId);
});

function showClientInfo() {
	let custId = $("#custIdInfo").val();
	window.location.href = "editClientInfo?" + window.btoa(custId);
}

var arrIntroData = ["advdecOptions", "apptypesClient"];

function getFnaData(kycIntro) {
	for (let introIdx = 0; introIdx < arrIntroData.length; introIdx++) {
		kycIntro[arrIntroData[introIdx]] = document.getElementsByName(arrIntroData[introIdx])[0].value;
	}
	kycIntro.ccRepexistinvestflg = $('#ccRepexistinvestflg').val();
}

function setFnaData(data) {
	let slctCount = 0;

	for (let intrIdx = 0; intrIdx < arrIntroData.length; intrIdx++) {
		document.getElementsByName(arrIntroData[intrIdx])[0].value = data[arrIntroData[intrIdx]];
	}
	let advOptions = data.advdecOptions;

	let appTypeOpts = data.apptypesClient;
	if (advOptions != "" && advOptions != null && advOptions != "undefined") {
		let advData = JSON.parse(advOptions);
		setAdvOptionData(advData);
	}
	if (appTypeOpts != "" && appTypeOpts != null && appTypeOpts != "undefined") {
		let appTypaData = JSON.parse(appTypeOpts);
		slctCount = setAppTypeData(appTypaData, slctCount);
	}
	if (data.ccRepexistinvestflg == "Y") {
		$("#ccRepexistinvestflgY").prop('checked', true);
		$("#ccRepexistinvestflgY").val("Y");
		$("#ccRepexistinvestflg").val("Y");
		slctCount += 1;
	} else if (data.ccRepexistinvestflg == "N") {
		$("#ccRepexistinvestflgN").prop('checked', true);
		$("#ccRepexistinvestflgN").val("N");
		$("#ccRepexistinvestflg").val("N");
		slctCount += 1;
	} else {
		$("#ccRepexistinvestflgY").prop('checked', false);
		$("#ccRepexistinvestflgY").val("");
		$("#ccRepexistinvestflg").val("");

		$("#ccRepexistinvestflgN").prop('checked', false);
		$("#ccRepexistinvestflgN").val("");
		$("#ccRepexistinvestflg").val("");
		slctCount += 1;
	}

	if (slctCount == 3) {
		$("#chkSelectAll").trigger("click");
	}
	//if(emptObjFlg == true){
	//	$("#noteInfoFnaId").removeClass("show").addClass("hide");
	//}else if(emptObjFlg == false){
	$("#noteInfoFnaId").removeClass("hide").addClass("show");
	//}  
	$("#noteInfoFnaId").text("FNA Id : " + data.fnaId);
	$("#noteInfoFnaId").attr("title", data.fnaId);
}

var optData = ["NO1A", "NO1B", "NO2", "NO3", "NO4", "NO5", "NO1ALL"];
function setAdvOptionData(advData) {
	for (let advOptIdx = 0; advOptIdx < optData.length; advOptIdx++) {
		if (advData[optData[advOptIdx]] == "Y") {
			let optId = "htfrepdec" + optData[advOptIdx].toLowerCase();
			$("#" + optId).prop('checked', true);
		}
	}

}
var arrAppData = ["LIF1", "LIF2"];
function setAppTypeData(appData, slctCount) {
	for (let appDatIdx = 0; appDatIdx < arrAppData.length; appDatIdx++) {
		if (appData[arrAppData[appDatIdx]] == "Y") {
			let no = appDatIdx + 1;
			$("#htfcclife" + no).prop('checked', true);
			slctCount += 1;
		}
	}
	return slctCount;
}

function chkAdvAppTypeOptions(chkbox) {

	let advAppTypeOpt = $("#apptypesClient");
	let jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	let chkd = chkbox.checked;
	let val = $(chkbox).attr("data");

	let newobj = jsonvalues;
	newobj[val] = (chkd == true ? "Y" : "N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}

function chkJsnOptions(chkbox, jsnFld) {
	let advAppTypeOpt = $("#" + jsnFld + "");
	let jsonvalues = JSON.parse(emptyOrUndefined(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	let chkd = chkbox.checked;
	let val = $(chkbox).attr("data");

	let newobj = jsonvalues;
	newobj[val] = (chkd == true ? "Y" : "N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}


function slctAllDeclTypes(obj) {
	let chkObj = $(obj).is(":checked");
	if (chkObj) {
		$("#advDclSec [type=checkbox]").each(function () {
			let x = $(this);
			let ckkobj = $(this).prop("checked", true);
			let cked = $(x).is(":checked")
			if (cked) {
				let advDecOpt = $("#advdecOptions");
				let jsonvalues = JSON.parse(emptyOrUndefined(advDecOpt.val()) ? "{}" : advDecOpt.val());
				let val = $(x).val();

				let newobj = jsonvalues;
				newobj[val] = (cked == true ? "Y" : "N");
				advDecOpt.val(JSON.stringify(newobj));


			}

		});
		hideclntDeclError();
	} if (!chkObj) {

		$("#advDclSec [type=checkbox]").each(function () {
			let chkid = $(this).prop("id");
			if (chkid != "htfrepdecno1all") {
				$("#" + chkid).prop("checked", false);
			}

		});
		showclntDeclError();
	}

}

function slctDslectAll() {

	let blnSlctAllSts = true;
	$("#advDclSec [type=checkbox]").each(function () {
		let chkid = $(this).prop("id");
		if (chkid != "htfrepdecno1all") {
			if (!this.checked) { blnSlctAllSts = false; return false; }
		}

	});
	hideclntDeclError();
	if (blnSlctAllSts) $("#htfrepdecno1all").prop("checked", true);
	if (!blnSlctAllSts) $("#htfrepdecno1all").prop("checked", false);

}

function validateApplcntType() {

	let chkOptOneLandN = document.getElementById('htfcclife1');
	let chkOptOneAandH = document.getElementById('htfccah1');
	let chkOptTwoLandN = document.getElementById('htfcclife2');
	let chkOptTwoAandH = document.getElementById('htfccah2');

	if ((chkOptOneLandN.checked) || (chkOptOneAandH.checked)
		|| (chkOptTwoLandN.checked) || (chkOptTwoAandH.checked)) {
		showLifeError();
		// hideSelcetAllLblProp();

	} else if (!(chkOptOneLandN.checked) && !(chkOptOneAandH.checked)
		&& !(chkOptTwoLandN.checked) && !(chkOptTwoAandH.checked)) {
		$("#life12Error").removeClass("hide").addClass("show");

		hideLifeError();
		chkOptOneLandN.focus();
	}
}// end of validateApplcntType



function chkAdvAppTypeOptions(chkbox) {

	let advAppTypeOpt = $("#apptypesClient");
	let jsonvalues = JSON.parse(emptyOrUndefined(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	let chkd = chkbox.checked;
	let val = $(chkbox).attr("data");

	let newobj = jsonvalues;
	newobj[val] = (chkd == true ? "Y" : "N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}

function slectAllAppType(obj) {

	//hide if any error message shows
	showLifeError();
	hideRepexistinvestflgError();
	hideclntDeclError();

	if ($(obj).is(":checked")) {
		$("#htfcclife1").prop("checked", true);
		$("#ccRepexistinvestflgYN").prop("checked", true);
		$("#htfcclife2").prop("checked", true);
		$("#htfcclife1").val("Y");
		$("#ccRepexistinvestflg").val("Y");
		let advProcedObj = { LIF1: "Y", LIF2: "Y" };
		$("#apptypesClient").val(JSON.stringify(advProcedObj));
	}
	if (!$(obj).is(":checked")) {
		$("#htfcclife1").prop("checked", false);
		$("#ccRepexistinvestflgYN").prop("checked", false);
		$("#htfcclife2").prop("checked", false);
		$("#htfcclife1").val("");
		$("#ccRepexistinvestflg").val("");
		$("#apptypesClient").val("");
	}
}
function setJsonObjToElm(cont, data) {
	let val = JSON.stringify(data);

	$('input[name="' + cont + '"]').val("");
	if (!isEmpty(data) && !jQuery.isEmptyObject(val)) {
		$('input[name="' + cont + '"]').val(val);
		$.each(data, function (obj, val) {
			if (val == 'Y')
				$('input[data="' + obj + '"]').prop("checked", true);
			else
				$('input[data="' + obj + '"]').prop("checked", false);
		});
	}
}


function validateDeclSec(crntIndex) {


	if (crntIndex == 2) {
		let numberOfChecked = $("#advDclSec").find('input:checkbox:checked').length;

		if (isEmptyFld(numberOfChecked)) {
			showclntDeclError();
			return;
		} else {
			hideclntDeclError();
		}
	}
	return true;
}

//kyc Intro sec 3 validation part while click finish button 

function valiTypeOfAdvSec(crntIndex) {

	if (crntIndex == 2) {
		let life1 = $('input[name="htfcclife1"]:checked').val();
		let life2 = $('input[name="htfcclife2"]:checked').val();
		if ((isEmptyFld(life1)) && (isEmptyFld(life2))) {
			hideLifeError();
			return;
		} else {
			showLifeError();
		}

		let life3Flg = $("#ccRepexistinvestflg").val();
		if (isEmptyFld(life3Flg)) {
			showRepexistinvestflgError();
			return;
		} else {
			hideRepexistinvestflgError();
		}
	}

	return true;
}

function hideLifeError() {
	$("#life1Error").addClass("err-fld");
	$("#life2Error").addClass("err-fld");
	$("#life12Error").removeClass("hide").addClass("show");
}

function showLifeError() {
	$("#life1Error").removeClass("err-fld");
	$("#life2Error").removeClass("err-fld");
	$("#life12Error").removeClass("show").addClass("hide");
}
function showRepexistinvestflgError() {
	$("#repInvExistFlgError").addClass("err-fld");
	$("#repInvExtFlg").removeClass("hide").addClass("show");
}

function hideRepexistinvestflgError() {
	$("#repInvExistFlgError").removeClass("err-fld");
	$("#repInvExtFlg").removeClass("show").addClass("hide");
}

function hideclntDeclError() {
	$("#advDclSec").css("border", "");
	$("#advDclSec").find("#appClntChioceError").addClass("hide").removeClass("show");
}

function showclntDeclError() {
	$("#advDclSec").css("border", "1px solid #f75c02");
	$("#advDclSec").find("#appClntChioceError").addClass("show").removeClass("hide");
}

//uncheck select all label if life and inv flag is unchecked

function hideSelcetAllLblProp() {
	let selAllFld = $("#chkSelectAll").is(":checked");
	if (selAllFld == true) {
		$("#chkSelectAll").prop("checked", false);
	} else {

	}
}

function slctChkField(obj, id) {
	let numberOfChecked = $("#advDclSec").find('input:checkbox:checked').length;
	if (id == "chk") {
		if (obj.checked) {
			obj.value = "Y";
			hideclntDeclError();
			if (numberOfChecked == 6) {
				$("#htfrepdecno1all").prop("checked", true);
				$("#htfrepdecno1all").val("Y");
				let clntDelcObj = { NO1A: "Y", NO1B: "Y", NO1ALL: "Y", NO2: "Y", NO3: "Y", NO4: "Y", NO5: "Y" };
				$("#advdecOptions").val(JSON.stringify(clntDelcObj));
			}

		} else if (!obj.checked) {
			obj.value = "";

			if (numberOfChecked == 0) {
				showclntDeclError();
			}

			let otherval = $("#htfrepdecno1all").val();
			if (otherval == "Y") {
				$("#htfrepdecno1all").prop("checked", false);
				$("#htfrepdecno1all").val("");

				let clntDelcObj = { NO1A: "Y", NO1B: "Y", NO2: "Y", NO3: "Y", NO4: "Y", NO5: "Y" }
				$("#advdecOptions").val(JSON.stringify(clntDelcObj));

			}

		}

	} else {
		if (obj.checked) {
			document.getElementById(id).value = obj.value;
		}
	}
}


function enableselectAllOpt() {
	let numberOfChecked = $("#AppTypeSec").find('input:checkbox:checked').length;
	if (numberOfChecked == 3) {
		document.getElementById("chkSelectAll").checked = true;
		//$("#chkSelectAll").trigger("click");
		//$("#chkSelectAll").prop("cheked",true);
	}
}


function toggleExistInvest(obj) {
	let id = $(obj).attr("id");
	if (id == 'ccRepexistinvestflgY') {
		if (obj.checked == true) {
			$("#ccRepexistinvestflg").val("");
			$("#ccRepexistinvestflgN").val("");
			$("#ccRepexistinvestflgN").prop("checked", false)
			$("#ccRepexistinvestflg").val("Y");
			$("#ccRepexistinvestflgY").val("Y");
			hideRepexistinvestflgError();
		}
		else if (obj.checked == false) {
			$("#ccRepexistinvestflgY").prop("checked", false)
			$("#ccRepexistinvestflg").val("");
			$("#ccRepexistinvestflgY").val("");
			showRepexistinvestflgError();
		}
	}

	if (id == 'ccRepexistinvestflgN') {
		if (obj.checked == true) {
			$("#ccRepexistinvestflg").val("");
			$("#ccRepexistinvestflgY").val("");
			$("#ccRepexistinvestflgY").prop("checked", false)
			$("#ccRepexistinvestflg").val("N");
			$("#ccRepexistinvestflgN").val("N");
			hideRepexistinvestflgError();
		}
		else if (obj.checked == false) {
			$("#ccRepexistinvestflgN").prop("checked", false)
			$("#ccRepexistinvestflg").val("");
			$("#ccRepexistinvestflgN").val("");
			showRepexistinvestflgError();
		}
	}

}

function fnaAssgnFlgs(obj, id) {
	if (id == "chk") {
		if (obj.checked) {
			obj.value = "Y";
			//enableselectAllOpt();

		} else if (!obj.checked) {
			obj.value = "";
		}

	} else {
		if (obj.checked) {
			document.getElementById(id).value = obj.value;
		}
	}
}



function slctAllDeclTypes(obj) {
	let chkFlg = $(obj).is(":checked");
	if (chkFlg == true) {
		$("#htfrepdecno1a").prop("checked", true);
		$("#htfrepdecno1b").prop("checked", true);
		$("#htfrepdecno2").prop("checked", true);
		$("#htfrepdecno3").prop("checked", true);
		$("#htfrepdecno4").prop("checked", true);
		$("#htfrepdecno5").prop("checked", true);
		let clntDelcObj = { NO1A: "Y", NO1B: "Y", NO1ALL: "Y", NO2: "Y", NO3: "Y", NO4: "Y", NO5: "Y" };
		$("#advdecOptions").val(JSON.stringify(clntDelcObj));
		hideclntDeclError();
	} else if (chkFlg == false) {
		$("#htfrepdecno1a").prop("checked", false);
		$("#htfrepdecno1b").prop("checked", false);
		$("#htfrepdecno2").prop("checked", false);
		$("#htfrepdecno3").prop("checked", false);
		$("#htfrepdecno4").prop("checked", false);
		$("#htfrepdecno5").prop("checked", false);
		$("#advdecOptions").val("");
		showclntDeclError();
	}
}

function chkJsnOptions(obj, jsnFld) {
	let advAppTypeOpt = $("#" + jsnFld);
	let jsonvalues = advAppTypeOpt.val() != "undefined" && !isEmpty(advAppTypeOpt.val())
		? JSON.parse(advAppTypeOpt.val()) : {};
	let isNotAllChkFlg;
	switch (obj.id) {

		case "htfrepdecno1all":
			if (obj.checked) {
				isNotAllChkFlg = "N";
				jsonvalues.NO1ALL = "Y";
				$("#" + obj.id).prop("checked", true);
			} else {
				isNotAllChkFlg = "Y";
				jsonvalues.NO1ALL = "N";
				$("#" + obj.id).prop("checked", false);
			}
			// slctAllDeclTypes(obj);
			break;

		case "htfrepdecno1a":
			if (obj.checked) {
				jsonvalues.NO1A = "Y";
				$("#" + obj.id).prop("checked", true);
			} else {
				jsonvalues.NO1A = "N";
				$("#" + obj.id).prop("checked", false);
			}
			break;

		case "htfrepdecno1b":
			if (obj.checked) {
				jsonvalues.NO1B = "Y";
				$("#" + obj.id).prop("checked", true);
			} else {
				jsonvalues.NO1B = "N";
				$("#" + obj.id).prop("checked", false);
			}
			break;

		case "htfrepdecno2":
			if (obj.checked) {
				jsonvalues.NO2 = "Y";
				$("#" + obj.id).prop("checked", true);
			} else {
				jsonvalues.NO2 = "N";
				$("#" + obj.id).prop("checked", false);
			}
			break;

		case "htfrepdecno3":
			if (obj.checked) {
				jsonvalues.NO3 = "Y";
				$("#" + obj.id).prop("checked", true);
			} else {
				jsonvalues.NO3 = "N";
				$("#" + obj.id).prop("checked", false);
			}
			break;

		case "htfrepdecno4":
			if (obj.checked) {
				jsonvalues.NO4 = "Y";
				$("#" + obj.id).prop("checked", true);
			} else {
				jsonvalues.NO4 = "N";
				$("#" + obj.id).prop("checked", false);
			}
			break;

		case "htfrepdecno5":
			if (obj.checked) {
				jsonvalues.NO5 = "Y";
				$("#" + obj.id).prop("checked", true);
			} else {
				jsonvalues.NO5 = "N";
				$("#" + obj.id).prop("checked", false);
			}
			break;
	}
	let numberOfChecked = 0;
	$.each(jsonvalues, function (chkFld, flag) {
		if (flag == "Y" && chkFld != "NO1ALL") {
			numberOfChecked++;
		}
	});

	if (numberOfChecked == 0) {
		showclntDeclError();
	} else {
		hideclntDeclError();
	}

	if (isNotAllChkFlg == "N") {
		jsonvalues.NO1ALL = "Y";
		$("#htfrepdecno1all").prop("checked", true);
	} else if (numberOfChecked < 6 || isNotAllChkFlg == "Y") {
		jsonvalues.NO1ALL = "N";
		$("#htfrepdecno1all").prop("checked", false);
	} else if (numberOfChecked == 6) {
		jsonvalues.NO1ALL = "Y";
		$("#htfrepdecno1all").prop("checked", true);
	}
	advAppTypeOpt.val(JSON.stringify(jsonvalues));
}