var arrAddrData = ["Addr1", "Addr2", "Addr3", "City", "State", "Country", "PostalCode"];

var searchtable2 = $('#tblNricListRec').DataTable({
	scrollX: false,
	scrollY: "500px",
	scroller: true,
	scrollCollapse: true,
	autoWidth: false,
	/* paging: true,
	pagingType: "simple", */
	columnDefs: [
		{
			"targets": [6],
			"visible": false,
			"searchable": false
		}
	]
});

// $(document).ready(function () {

//set Nationality field as   Default Singaporean 
$("#nationality").val("SG");

// Set country field Default as Singapore
$("#resCountry").val("SINGAPORE");
$("#resState").val("").prop("readOnly", true)
$("#resCity").val("").prop("readOnly", true)

// set Customer Status default as A-APPLICANT
$("#customerStatusId").val("STA003");

$('#finishedModal').on('shown.bs.modal', function () {
	setTimeout(function () {
		$('.circle-loader').addClass('load-complete');
		$('.checkmark').show();
		$("#modalMsgLbl").html("Client Details Saved Successfully !");
		$("#generateKYCBtn").removeClass("disabled");
	}, 2500);

});

// check logged user adviser staff level
chkLogUsrAdvStaffLevel();

//   });

//Nric validation

$('input[name=nric]').change(function () {

	// var strCustId = $("#custid").val();

	var strNric = $(this).val();
	// var strAdvId = $("#hTxtFldLoggedAdvId").val();

	if (strNric) {

		//		var tblId = document.getElementById("tblNricListRec");
		//		var tbody = tblId.tBodies[0];
		searchtable2.clear().draw();
		var col0 = "", col1 = "", col2 = "", col3 = "", col4 = "", col5 = "", col6 = "";
		$.ajax({
			url: "CustomerDetails/getAllNricList/" + strNric,
			type: "GET",
			// async:false,
			dataType: "json",
			contentType: "application/json",
			success: function (data) {

				if (data && data.length > 0) {
					// $('#srchClntForNricModal').modal('show');
					// } else {
					//	$('#srchClntModal').modal('hide');
					//	swal.fire("No Nric List Found for You.");
					// }

					// $("#srchClntForNricModal").find(".modal-body").find("#srchClntTblForNricSec").removeClass('hide').addClass('show');

					//$("#tblNricListRec th").trigger("click")

					//var data = JSON.parse(data);
					for (let custIdx = 0; custIdx < data.length; custIdx++) {
						let jsonObj = (data[custIdx]);

						col0 = jsonObj.custName;
						col1 = jsonObj.custInitials;
						col2 = jsonObj.nric;
						col3 = jsonObj.dob;
						col4 = jsonObj.resHandPhone;
						col6 = jsonObj.custCateg;
						col5 = jsonObj.custId;
						searchtable2.row.add([col0, col1, col2, col3, col4, col6, col5]).draw(false);
					}

					$('#srchClntForNricModal').modal('show');
					$("#srchClntForNricModal").find(".modal-body").find("#srchClntTblForNricSec").removeClass('hide').addClass('show');
					setTimeout(function () {
						searchtable2.columns.adjust();
					}, 200);
				}

			},
			error: function (xhr, textStatus, errorThrown) {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	}
});

$('#tblNricListRec tbody').on('click', 'tr', function () {
	let custData = searchtable2.row(this).data();
	let custId = custData[6];
	window.location.href = "editClientInfo?" + window.btoa(custId);
});

$('#tblNricListRec tbody').on('mouseover', 'tr', function () {
	$(this).css('cursor', 'pointer');
	$(this).css('background', '#337ab7');
	$(this).css('color', '#fff');
});

$('#tblNricListRec tbody').on('mouseout', 'tr', function () {
	$(this).css('cursor', '');
	$(this).css('background', '');
	$(this).css('color', '');
});

function custCategType(type) {
	if (type == "IND") {
		$("#individualSec").addClass("show").removeClass("hide");
		$("#corporateSec").addClass("hide").removeClass("show");
	} else if (type == "COR") {
		$("#individualSec").addClass("hide").removeClass("show");
		$("#corporateSec").addClass("show").removeClass("hide");
	}
}

function setClientDob(obj) {
	$("#txtFldClntAge").val(calcAge(obj.value));
}

function calcAge(dob) {
	let age = 0;
	if (!isEmpty(dob)) {
		let birthday = dob.split("/")[2] + "-" + dob.split("/")[1] + "-" + dob.split("/")[0];
		let now = new Date();
		let past = new Date(birthday);

		age = now.getFullYear() - past.getFullYear();
	}
	return age;
}

function openTabSec(value) {

	if (value == 'Personal') {
		showPersnalSec();
	}

	if (value == 'Address') {
		//client personal details validation here
		if (!validateClntInfoPersonalDetls()) {
			showPersnalSec();
			return;
		}
		showAddrSec();
	}

	if (value == 'LOB') {
		//client address details validation here
		if (!validateClntInfoAddrDetls()) {
			showAddrSec();
			return;
		}
		showLobSec();
	}
}

$("#generateKYCBtn").on("click", function () {
	window.location.href = "kycIntro";
});

// Client Info Personal Detls mandotary Fld Validation
function validateClntInfoPersonalDetls() {

	let custCateg = $('input[name="custCateg"]:checked').val();
	if (custCateg == "COMPANY") {

		let custNameCom, custInitialsCom, Fld = "Error";
		custNameCom = $('#custNameCom').val();
		custInitialsCom = $('#custInitialsCom').val();

		if (isEmptyFld(custNameCom)) {
			$("#custNameCom" + Fld).removeClass("hide").addClass("show");
			$("#custNameCom").addClass('err-fld');
			return;
		} else {
			$("#custNameCom" + Fld).removeClass("show").addClass("hide");
			$("#custNameCom").removeClass('err-fld');
		}

		if (isEmptyFld(custInitialsCom)) {
			$("#custInitialsCom" + Fld).removeClass("hide").addClass("show");
			$("#custInitialsCom").addClass('err-fld');
			return;
		} else {
			$("#custInitialsCom" + Fld).removeClass("show").addClass("hide");
			$("#custInitialsCom").removeClass('err-fld');
		}
	}
	if (custCateg == "PERSON") {

		let custName, custInials, dob, clntStatus, custEmail, busNature, busNatOthDet, Fld = "Error";
		custName = $('#custName').val();
		custInials = $('#custInitials').val();
		dob = $('#dob').val();
		clntStatus = $('#customerStatusId').val();
		custEmail = $('#emailId').val();
		busNature = $('#businessNatr').val();
		busNatOthDet = $('#businessNatrDets').val();

		if (isEmptyFld(clntStatus)) {
			$("#customerStatusId" + Fld).removeClass("hide").addClass("show");
			$("#customerStatusId").addClass('err-fld');
			$("#customerStatusId").focus();
			return;
		} else {
			$("#customerStatusId" + Fld).removeClass("show").addClass("hide");
			$("#customerStatusId").removeClass('err-fld');
		}

		if (isEmptyFld(custName)) {
			$("#custName" + Fld).removeClass("hide").addClass("show");
			$("#custName").addClass('err-fld');
			$("#custName").focus();
			return;
		} else {
			$("#custName" + Fld).removeClass("show").addClass("hide");
			$("#custName").removeClass('err-fld');
		}

		if (isEmptyFld(custInials)) {
			$("#custInitials" + Fld).removeClass("hide").addClass("show");
			$("#custInitials").addClass('err-fld');
			$("#custInitials").focus();
			return;
		} else {
			$("#custInitials" + Fld).removeClass("show").addClass("hide");
			$("#custInitials").removeClass('err-fld');
		}

		/*	if(isEmptyFld(dob)){
				$("#dob"+Fld).removeClass("hide").addClass("show");
				$("#dob").addClass('err-fld');
				return;
			}else{
				$("#dob"+Fld).removeClass("show").addClass("hide");
				$("#dob").removeClass('err-fld');
			}*/

		if (isEmptyFld(custEmail)) {
			$("#emailId" + Fld).removeClass("hide").addClass("show");
			$("#emailId").addClass('err-fld');
			$("#emailId").focus();
			return;
		} else {
			$("#emailId" + Fld).removeClass("show").addClass("hide");
			$("#emailId").removeClass('err-fld');
		}

		//validate email format while ckick next btn
		if (!isEmptyFld(custEmail)) {
			let regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
			if (!regex.test(custEmail)) {
				$("#emailId" + Fld).removeClass('hide').addClass('show');
				$("#emailId" + Fld).html("Invalid Email Id Format");
				$("#emailId").focus();
				return;
			} else {
				$("#emailId" + Fld).removeClass('show').addClass('hide');
			}
		}

		let busNatOthDetFlg = $("#busiNatOthSec").hasClass("show")
		if ((busNatOthDetFlg == true) && (busNature == 'Others')) {
			if (isEmptyFld(busNatOthDet)) {
				$("#businessNatrDets" + Fld).removeClass("hide").addClass("show");
				$("#businessNatrDets").addClass('err-fld');
				$("#businessNatrDets").focus();
				return;
			} else {
				$("#businessNatrDets" + Fld).removeClass("show").addClass("hide");
				$("#businessNatrDets").removeClass('err-fld');
			}
		}
	}

	return true;
}
//Validate address details
function validateClntInfoAddrDetls() {

	let resaddr1, resaddr2, resContry, resPostCde; Fld = "Error";

	resaddr1 = $('#resAddr1').val();
	//resaddr2 = $('#resAddr2').val();

	resContry = $('#resCountry').val();
	resPostCde = $('#resPostalCode').val();

	if (isEmptyFld(resaddr1)) {
		$("#resAddr1" + Fld).removeClass("hide").addClass("show");
		$("#resAddr1").addClass('err-fld');
		$("#resAddr1").focus();
		return;
	} else {
		$("#resAddr1" + Fld).removeClass("show").addClass("hide");
		$("#resAddr1").removeClass('err-fld');
	}

	/*if(isEmptyFld(resaddr2)){
		 $("#resAddr2"+Fld).removeClass("hide").addClass("show");
		 $("#resAddr2").addClass('err-fld');
		 return;
	 }else{
		 $("#resAddr2"+Fld).removeClass("show").addClass("hide");
		 $("#resAddr2").removeClass('err-fld');
	 }*/


	if (isEmptyFld(resContry)) {
		$("#resCountry" + Fld).removeClass("hide").addClass("show");
		$("#resCountry").addClass('err-fld');
		$("#resCountry").focus();
		return;
	} else {
		$("#resCountry" + Fld).removeClass("show").addClass("hide");
		$("#resCountry").removeClass('err-fld');
	}

	if (isEmptyFld(resPostCde)) {
		$("#resPostalCode" + Fld).removeClass("hide").addClass("show");
		$("#resPostalCode").addClass('err-fld');
		$("#resPostalCode").focus();
		return;
	} else {
		$("#resPostalCode" + Fld).removeClass("show").addClass("hide");
		$("#resPostalCode").removeClass('err-fld');
	}

	return true;
}

function showPersnalSec() {
	$('#btnResiAddress').removeClass('js-active');
	$('#btnLOB').removeClass('js-active');
	$('#btnPersonalInfo').addClass('js-active');

	$('#AddrSec').removeClass('js-active');
	$('#LOBSec').removeClass('js-active');
	$('#PerInfoSec').addClass('js-active');
}

function showAddrSec() {
	$('#btnPersonalInfo').removeClass('js-active');
	$('#btnLOB').removeClass('js-active');
	$('#btnResiAddress').addClass('js-active');

	$('#LOBSec').removeClass('js-active');
	$('#PerInfoSec').removeClass('js-active');
	$('#AddrSec').addClass('js-active');
}

function showLobSec() {
	$('#btnPersonalInfo').removeClass('js-active');
	$('#btnResiAddress').removeClass('js-active');
	$('#btnLOB').addClass('js-active');

	$('#PerInfoSec').removeClass('js-active');
	$('#AddrSec').removeClass('js-active');
	$('#LOBSec').addClass('js-active');
}

var customerDetails = {};
var smoke = "NotSmoking";
var custSmokeFlg = "N";
var gender = "Male";
var custGender = "M";

//Edit 
let id = $("#custId").val();
if (id != null && id != '') {
	getCustomerDetails(id);
}

//Function GetClientDetails
function getCustomerDetails(custId) {
	var clientData = {};
	$.ajax({
		url: "CustomerDetails/getDataById/" + custId,
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		async: false,
		success: function (data) {
			setCustomerInfo(data);
			clientData = data;
			$("#createdBy").val(clientData.createdBy);
			$("#createdDate").val(clientData.createdDate);
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
	return clientData;
}

//Function SetClientInfo
function setCustomerInfo(custDets) {

	if (custDets.custCateg == "COMPANY") {
		$("#custCatComp").trigger("click");
		$("#custNameCom").val(custDets.custName);
		$("#custInitialsCom").val(custDets.custInitials);
		$("#emailIdCom").val(custDets.emailId);
		$("#businessNatrCom").val(custDets.businessNatr);

		custDets.custName = "";
		custDets.custInitials = "";
		custDets.emailId = "";
		custDets.businessNatr = "";

		for (let addrIdx = 0; addrIdx < arrAddrData.length; addrIdx++) {
			custDets["res" + arrAddrData[addrIdx]] = custDets["off" + arrAddrData[addrIdx]];
			custDets["off" + arrAddrData[addrIdx]] = "";
		}
	}

	let frmElements = $('.customerDetailsFrm :input').serializeObject();
	$.map(frmElements, function (n, elem) {
		if (elem != "txtFldClntAge" && custDets[elem] != undefined) {
			document.getElementsByName(elem)[0].value = custDets[elem];
		}
	});
	$("#txtFldClntAge").val(calcAge(custDets.dob));
	setClntGender(custDets.sex);
	setSmokerFlg(custDets.smokerFlg);
	setTypeOfPdt(custDets.typeOfPdt);
	setCountry(custDets.resCountry);
	if ($("#businessNatr").val() == "Others") {
		$("#busiNatOthSec").addClass("show").removeClass("hide");
		if (isEmptyFld($("#businessNatrDets").val())) {
			$("#businessNatrDetsError").removeClass("hide").addClass("show");
			$("#businessNatrDets").addClass('err-fld');
			return;
		} else {
			$("#businessNatrDetsError").removeClass("show").addClass("hide");
			$("#businessNatrDets").removeClass('err-fld');
		}
	} else {
		$("#busiNatOthSec").removeClass("show").addClass("hide");
		$("#businessNatrDetsError").removeClass("show").addClass("hide");
	}
}
function setCountry(custvalue) {
	custvalue = custvalue.toUpperCase();

	if (custvalue == "SINGAPORE") {
		$("#resCountry").val(custvalue);
		$("#resCity").val("").prop("readonly", true)
		$("#resState").val("").prop("readonly", true)
	} else {
		$("#resCity").prop("readonly", false)
		$("#resState").prop("readonly", false)
		$("#resCountry").val(custvalue);
	}
}
function setClntGender(custValue) {
	let clntGndr = isEmpty(custValue) ? "M" : custValue;
	if (clntGndr == "F") {
		$("INPUT[name=sex]").val([custValue]);
		$("#radBtnmale").prop("checked", false);
	} else {
		$("INPUT[name=sex]").val([custValue]);
		$("#radBtnfemale").prop("checked", false);
	}
}
function setSmokerFlg(custvalue) {
	let clntSmgFlg = isEmpty(custvalue) ? "N" : custvalue;
	if (clntSmgFlg == "Y") {
		$("#smokerFlg").val("Y");
		$("#smokerFlg").prop("checked", true);
	} else {
		$("#notsmokerFlg").val("N");
		$("#notsmokerFlg").prop("checked", true);
	}
}

function setTypeOfPdt(custvalue) {
	let typeofprd = isEmpty(custvalue) ? "ALL" : custvalue;
	$("input[name=typeOfPdt][value=" + typeofprd + "]").attr('checked', true);
}

function validateClientMand() {

	//validate 3 tabe data with mandatory and return false if not

	if (!validateClntInfoPersonalDetls()) { showPersnalSec(); return; }

	if (!validateClntInfoAddrDetls()) { showAddrSec(); return; }

	// if(!validateClntInfoLobDetls()){return;}

	let clientData = {};
	let frmElements = $('.customerDetailsFrm :input').serializeObject();
	$.map(frmElements, function (n, element) {
		if (element != "txtFldClntAge" && element != "custNameCom" && element != "custInitialsCom"
			&& element != "emailIdCom") {
			clientData[element] = n;
		}
	});

	if (clientData.custCateg == "COMPANY") {
		clientData.custName = $("#custNameCom").val();
		clientData.custInitials = $("#custInitialsCom").val();
		clientData.emailId = $("#emailIdCom").val();
		clientData.businessNatr = $("#businessNatrCom").val();

		for (let arrAddrIdx = 0; arrAddrIdx < arrAddrData.length; arrAddrIdx++) {
			clientData["off" + arrAddrData[arrAddrIdx]] = clientData["res" + arrAddrData[arrAddrIdx]];
			clientData["res" + arrAddrData[arrAddrIdx]] = "";
		}
        /* clientData.offAddr1=clientData.resAddr1;
		 clientData.offAddr2=clientData.resaddr2;
		 clientData.offAddr3=clientData.resAddr3;
	     clientData.offCity=clientData.resCity;
         clientData.offState=clientData.resState;
         clientData.offCountry=clientData.resCountry;
		 clientData.offPostalCode=clientData.resPostalCode;*/
	}

	clientData.typeOfPdt = "LIFE";
	clientData.custId = $("#custId").val();
	clientData.createdBy = $("#createdBy").val();
	clientData.createdDate = $("#createdDate").val();

	//alert(JSON.stringify(clientData));

	$.ajax({
		url: "CustomerDetails/saveData",
		type: "POST",
		dataType: "json",
		contentType: "application/json",
		data: JSON.stringify(clientData),
		// async:false,
		beforeSend: function () {
			$('#cover-spin').show();
		},
		success: function (response) {
			$('#cover-spin').hide();
			$('.checkmark').hide();
			$('.circle-loader').removeClass('load-complete');
			$("#modalMsgLbl").html("Client Information being Saved...");
			$("#generateKYCBtn").addClass("disabled");
			$('#finishedModal').modal('show');
			/* $('.circle-loader').addClass('load-complete');
			$('.checkmark').toggle();
			$("#modalMsgLbl").html("Client Details Save Success!");
			$("#generateKYCBtn").removeClass("disabled");*/

			$("#custid").val(response.custid);
			$("#createdDate").val(response.createdDate);
			$("#createdBy").val(response.createdBy);
		},
		complete: function () {
			$('#cover-spin').hide();
		},
		error: function (xhr, textStatus, errorThrown) {
			//alert("Error while updating client!" );//"Error!"  - replace by constant value
			$('.circle-loader').hide();
			$("#modalMsgLbl").html(xhr.responseText);
			$('#cover-spin').hide();
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

function calculateAge() {
	let dateString = document.getElementsByName("dfSelfDob")[0].value;
	let today = new Date();
	let birthDate = new Date(dateString);
	let age = today.getFullYear() - birthDate.getFullYear();
	let m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		age--;
	}
	document.getElementsByName("dfSelfAge")[0].value = age;
}

function formatDate(date) {
	let d = new Date(date),
		month = '' + (d.getMonth() + 1),
		day = '' + d.getDate(),
		year = d.getFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;

	return [year, month, day].join('-');
}

//validate lob Details

function validateClntInfoLobDetls() {

	let strClntLob = $('input[name="typeOfPdt"]:checked').val();

	if (isEmptyFld(strClntLob)) {
		$("#LOBError").removeClass("hide").addClass("show");
		return;
	} else {
		$("#LOBError").removeClass("show").addClass("hide");
	}
	return true;
}

function chkLob(obj) {

	if (obj.checked == true) {
		$("#LOBError").removeClass("show").addClass("hide");
	}
	if (obj.checked == false) {
		$("#LOBError").removeClass("hide").addClass("show");
	}
}

function setCustGender(obj, Ele) {
	let gndrFlg = $(obj).hasClass('active');
	if (gndrFlg == true) {
		$("#" + Ele).val("M");
	} else {
		$("#" + Ele).val("F");
	}
}

function getAge(obj) {

	var Dob = $("#" + obj).val();

	if (isEmptyFld(Dob)) {
		$("#txtFldClntAge").val("");
		$("#txtFldClntAge").removeAttr("disabled", true);
		return;
	} else {
		let today = new Date();
		let birthDate = new Date(Dob);
		let age = today.getFullYear() - birthDate.getFullYear();

		let m = today.getMonth() - birthDate.getMonth();

		if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
			age--;
		}
		if (age >= 0) {
			$("#dobError").removeClass("show").addClass("hide");
			$("#txtFldClntAge").val(age);
			$("#txtFldClntAge").prop('disabled', "disabled");
		} else {
			//$("#dobError").removeClass("hide").addClass("show");
			//  $("#dobError").html("Keyin Valid DOB.");
			$("#txtFldClntAge").val(age);
			$("#txtFldClntAge").removeAttr("disabled", true);
		}
	}
}

// contry field validation 

function filtrCuntry(obj) {
	let thisVal = $(obj).find("option:selected").val();

	if (thisVal == "SINGAPORE") {
		$("#resCity").val("").prop("readonly", true)
		$("#resState").val("").prop("readonly", true)
	} else {
		$("#resCity").prop("readonly", false)
		$("#resState").prop("readonly", false)
	}
}

// Client Status Field Validation

function chkLogUsrAdvStaffLevel() {
	let loggedUserStfLvl = $("#hTxtloggedUsrStftype").val();
	if (loggedUserStfLvl == 'ADVISER') {
		$("#custStatus > option").each(function () {
			if ($(this).val() == 'C-CLIENT') {
				$(this).prop("disabled", true);
				$(this).text($(this).text() + "(Adviser Cannot select this)")
				//$("#custStatus").val("");
			}
		});
	} else {
		//$(obj).find("option:selected").prop("disabled",false);
	}

}

//clientInfo page Mandotary field Validation onchange input value hide error Messages
$(".checkMand").bind("change", function () {
	let thisFldId = $(this).attr("id");
	let thisFldVal = $(this).val();

	if (isEmptyFld(thisFldVal)) {
		$("#" + thisFldId + "Error").removeClass("hide").addClass("show");
		$("#" + thisFldId).addClass('err-fld');
		return;
	} else {
		$("#" + thisFldId + "Error").removeClass("show").addClass("hide");
		$("#" + thisFldId).removeClass('err-fld');
	}
})

$("#btnMyInfo").on("click", function () {
	let nricCode = $("#nric").val();
	if (nricCode) {
		$.ajax({
			type: "GET",
			// url: "https://sandbox.api.myinfo.gov.sg/com/v3/person-sample/" + nricCode,
			url: "getMyInfo/" + nricCode,
			success: function (resultStr) {
				if (resultStr) {
					let result = JSON.parse(resultStr);
					if (result.status == "success" && result.response) {
						populateClientInfoFields(JSON.parse(result.response));
					} else if (result.status == "failed" && result.response) {
						if (JSON.parse(result.response) && JSON.parse(result.response).message) {
							Swal.fire({
								icon: 'error',
								text: JSON.parse(result.response).message,
							});
						}
					} else {
						Swal.fire({
							icon: 'error',
							text: 'Please Try again Later or else Contact your System Administrator',
						});
					}
				} else {
					Swal.fire({
						icon: 'info',
						text: 'myInfo is not available for the given NRIC code',
					});
				}
			},
			error: function (error) {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	} else {
		$("#nricError").addClass("show").removeClass("hide");
		$("#nric").focus();
		Swal.fire({
			icon: "error",
			html: "Please key in <b>NRIC</b>!"
		}).then((result) => {
			if (result.isConfirmed) {
				$("#nric").addClass("err-fld");
			}
		});
	}
});

$("#nric").on("keydown", function () {
	$("#nricError").addClass("hide").removeClass("show");
	$("#nric").removeClass("err-fld");
});

function populateClientInfoFields(myInfo) {
	if (myInfo.name) {
		$("#custName").val(myInfo.name.value);
	}

	if (myInfo.dob && myInfo.dob.value) {
		let dobArray = myInfo.dob.value.split("-");
		if (dobArray && dobArray.length == 3) {
			$("#dob").val(dobArray[2] + "/" + dobArray[1] + "/" + dobArray[0]);
			$("#simple-date1 .input-group.date").datepicker("setDate", $("#dob").val());
			$("#txtFldClntAge").val(calcAge($("#dob").val()));
		}
	}

	if (myInfo.marital && myInfo.marital.desc && myInfo.marital.desc.length > 0) {
		$("#maritalStatus").val(myInfo.marital.desc[0].toUpperCase() + myInfo.marital.desc.substring(1).toLowerCase());
	}

	if (myInfo.sex) {
		$("input[name='sex'][value='" + myInfo.sex.code).prop("checked", true);
	}

	if (myInfo.race && myInfo.race.desc) {
		let raceArr = ["American", "Australian", "Chinese", "Indian", "Singaporean", "Malay", "European"];
		let desc = myInfo.race.desc.substring(0, 1) + myInfo.race.desc.substring(1).toLowerCase();
		if (!raceArr.includes(desc)) {
			desc = "Others";
		}
		if (desc) {
			$("#race").val(desc);
		}
	}

	if (myInfo.email) {
		$("#emailId").val(myInfo.email.value);
	}

	if (myInfo.mobileno) {
		let mobilenoValue = "";
		if (myInfo.mobileno.prefix) {
			mobilenoValue += myInfo.mobileno.prefix.value;
		}
		if (myInfo.mobileno.areacode) {
			mobilenoValue += myInfo.mobileno.areacode.value;
		}
		if (myInfo.mobileno.nbr) {
			mobilenoValue += myInfo.mobileno.nbr.value;
		}
		$("#resHandPhone").val(mobilenoValue);
	}

	if (myInfo.nationality && myInfo.nationality.code) {
		$("#nationality").val(myInfo.nationality.code);
	}

	if (myInfo.cpfemployers && myInfo.cpfemployers.history && myInfo.cpfemployers.history.length > 0) {
		let lastEmployerHistory = myInfo.cpfemployers.history[myInfo.cpfemployers.history.length - 1];
		if (lastEmployerHistory && lastEmployerHistory.employer) {
			$("#companyName").val(lastEmployerHistory.employer.value);
		}
	}

	if (myInfo.cpfcontributions && myInfo.cpfcontributions.history && myInfo.cpfcontributions.history.length > 0) {
		let lastContributionHistory = myInfo.cpfcontributions.history[myInfo.cpfcontributions.history.length - 1];
		if (lastContributionHistory && lastContributionHistory.amount) {
			$("#income").val(lastContributionHistory.amount.value);
		}
	}

	if (myInfo.occupation) {
		$("#occpnDesc").val(myInfo.occupation.value);
	}

	if (myInfo.hdbownership && myInfo.hdbownership.length > 0) {
		$("#tBodyAddress").empty();
		if (myInfo.hdbownership.length == 1) {
			let ownerAddress = myInfo.hdbownership[0];
			if (ownerAddress && ownerAddress.address) {
				if (ownerAddress.address.block) {
					$("#resAddr1").val(ownerAddress.address.block.value);
				}
				if (ownerAddress.address.building) {
					$("#resAddr2").val(ownerAddress.address.building.value);
				}
				if (ownerAddress.address.street) {
					$("#resAddr3").val(ownerAddress.address.street.value);
				}
				if (ownerAddress.address.postal) {
					$("#resPostalCode").val(ownerAddress.address.postal.value);
				}
			}
		} else {
			let tableData = "";
			for (let addrIndex = 0; addrIndex < myInfo.hdbownership.length; addrIndex++) {
				let ownerAddress = myInfo.hdbownership[addrIndex];
				tableData += "<tr>"
				if (ownerAddress && ownerAddress.address) {
					if (ownerAddress.address.block) {
						tableData += "<td class='tdBlkAddr'>" + ownerAddress.address.block.value + "</td>";
					}
					if (ownerAddress.address.building) {
						tableData += "<td class='tdBldngAddr'>" + ownerAddress.address.building.value + "</td>";
					}
					if (ownerAddress.address.street) {
						tableData += "<td class='tdStrtAddr'>" + ownerAddress.address.street.value + "</td>";
					}
					if (ownerAddress.address.postal) {
						tableData += "<td class='tdPostAddr'>" + ownerAddress.address.postal.value + "</td>";
					}
				}
				tableData += "</tr>";
			}
			$("#tBodyAddress").append(tableData);
			$("#addressModal").modal("show");
			$("#tBodyAddress tr").css("cursor", "pointer");
		}
	}
}

$("#tBodyAddress").on("click", "tr", function () {
	let block = $(this).closest("tr").find(".tdBlkAddr").text();
	let building = $(this).closest("tr").find(".tdBldngAddr").text();
	let street = $(this).closest("tr").find(".tdStrtAddr").text();
	let postal = $(this).closest("tr").find(".tdPostAddr").text();
	if (block) {
		$("#resAddr1").val(block).removeClass("err-fld");
		$("#resAddr1Error").removeClass("show").addClass("hide");
	}
	if (building) {
		$("#resAddr2").val(building);
	}
	if (street) {
		$("#resAddr3").val(street).removeClass("err-fld");
		$("#resAddr3Error").removeClass("show").addClass("hide");
	}
	if (postal) {
		$("#resPostalCode").val(postal);
		$("#resPostalCode").removeClass("err-fld");
		$("#resPostalCodeError").removeClass("show").addClass("hide");
	}
	$("#addressModal").modal("hide");
});

$("#resPostalCode, #resAddr3, #resAddr1").on("keydown", function () {
	$("#resPostalCode, #resAddr3, #resAddr1").removeClass("err-fld");
});

$("#btnSgLocate").on("click", function () {
	let postalCode = $("#resPostalCode").val();
	let buildingNumber = $("#resAddr1").val();
	let streetName = $("#resAddr3").val();
	let requestBody = {};
	requestBody["Postcode"] = postalCode;
	requestBody["Block"] = buildingNumber;
	requestBody["StreetName"] = streetName;
	let api;
	if (postalCode) {
		api = "postCode";
	} else if (streetName && buildingNumber) {
		api = "streetAndBuilding";
	}
	if (api) {
		$.ajax({
			type: "POST",
			// url: api,
			url: "getSgLocate",
			data: JSON.stringify(requestBody),
			success: function (resultStr) {
				if (resultStr) {
					let result = JSON.parse(resultStr);
					if (result && result.status == "success" && result.response) {
						let response = JSON.parse(result.response);
						if (response.ErrorCode == 1) {
							if (response.Postcodes && response.Postcodes.length > 0) {
								$("#tBodyAddress").empty();
								if (response.Postcodes.length == 1) {
									let clientAddress = response.Postcodes[0];
									if (clientAddress) {
										$("#resAddr1").val(clientAddress.BuildingNumber);
										$("#resAddr2").val(clientAddress.BuildingName);
										$("#resAddr3").val(clientAddress.StreetName);
										$("#resPostalCode").val(clientAddress.Postcode);
										if (clientAddress.BuildingNumber) {
											$("#resAddr1").removeClass("err-fld");
											$("#resAddr1Error").removeClass("show").addClass("hide");
										}
										if (clientAddress.StreetName) {
											$("#resAddr3").removeClass("err-fld");
											$("#resAddr3Error").removeClass("show").addClass("hide");
										}
										if (clientAddress.Postcode) {
											$("#resPostalCode").removeClass("err-fld");
											$("#resPostalCodeError").removeClass("show").addClass("hide");
										}
									}
								} else {
									let tableData = "";
									for (let addIndex = 0; addIndex < response.Postcodes.length; addIndex++) {
										let clientAddress = response.Postcodes[addIndex];
										tableData += "<tr>"
										if (clientAddress) {
											tableData += "<td class='tdBlkAddr'>" + clientAddress.BuildingNumber + "</td>";
											tableData += "<td class='tdBldngAddr'>" + clientAddress.BuildingName + "</td>";
											tableData += "<td class='tdStrtAddr'>" + clientAddress.StreetName + "</td>";
											tableData += "<td class='tdPostAddr'>" + clientAddress.Postcode + "</td>";
										}
										tableData += "</tr>";
									}
									$("#tBodyAddress").append(tableData);
									$("#addressModal").modal("show");
									$("#tBodyAddress tr").css("cursor", "pointer");
								}
							}
						} else {
							let errorText = 'Please Try again Later or else Contact your System Administrator';
							if (response.ErrorDetails) {
								errorText = response.ErrorDetails;
							}
							Swal.fire({
								icon: 'error',
								text: errorText,
							});
						}
					} else {
						Swal.fire({
							icon: 'error',
							text: 'Please Try again Later or else Contact your System Administrator',
						});
					}
				}
			},
			error: function (error) {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	} else {
		Swal.fire({
			icon: "error",
			html: "Please key in <b>PostalCode</b> alone or both <b>Address1(Building Number)</b> and <b>Address3(Street Name)</b>!"
		}).then((result) => {
			if (result.isConfirmed) {
				$("#resAddr1").addClass("err-fld");
				$("#resAddr3").addClass("err-fld");
				$("#resPostalCode").addClass("err-fld");
			}
		});
	}
});

$(".btnNricMdlClse").bind("click", function () {

	Swal.fire({
		//		  title: 'Are you Not Proceed?',
		text: "Do you want to clear NRIC value in client information screen?",
		icon: 'question',
		allowOutsideClick: false,
		allowEscapeKey: false,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, Clear it!'
	}).then((result) => {
		if (result.isConfirmed) {
			$("#nric").val('');
			swal.fire("Cleared!", "Your NRIC Value in Client Information Screen", "success");
			$('#srchClntForNricModal').modal('hide');
		} else {
			swal.fire("Select!", "Any on of the Row in above Table", "info");
			$('#srchClntForNricModal').modal('show');
		}
	});
});

$("#businessNatr").on("change", function () {
	if ($(this).val() == "Others") {
		$("#busiNatOthSec").addClass("show").removeClass("hide");
		if (isEmptyFld($("#businessNatrDets").val())) {
			$("#businessNatrDetsError").removeClass("hide").addClass("show");
			$("#businessNatrDets").addClass('err-fld');
			$("#businessNatrDets").focus();
			return;
		} else {
			$("#businessNatrDetsError").removeClass("show").addClass("hide");
			$("#businessNatrDets").removeClass('err-fld');
		}
	} else {
		$("#busiNatOthSec").removeClass("show").addClass("hide");
		$("#businessNatrDetsError").removeClass("show").addClass("hide");
	}
});