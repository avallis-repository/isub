/**
 * 
 */
var BIRT_URL = "https://secure-ekyc-uat.avallis.com/birt-viewer",
	FNA_RPT_FILELOC = "report/FNA/Financial Needs Analysis V 4.1.rptdesign";


var SESSION_EXP_JSP = "/jsp_common/sessionExpired.jsp";

var STR_SEX = "MALE^FEMALE";
//added by kavi 02/03/2018
//var STR_MARSTS = "Single^Married^Separated^Divorced^Attached^Widow^Widower";
var STR_MARSTS = "Single^Married^Divorced^Widowed";
var STR_FNA_YESNO_LIST = "YES^NO";

var NULL = '';//added by johnson on 22042015

var LIFE_AND_INVEST = "ALL";
var LIFE_INS = "LIFE";
var ILP = "LIFE INSURANCE - ILP";
var INVEST = "ILP/INVESTMENTS";
var DESIG_GMR = "GENERAL MANAGER";


var INS_MODE = "I";
var UPD_MODE = "U";
var DEL_MODE = "D";
var QRY_MODE = "Q";//added by johnson on 08042015

var FNA_FROM_NEWKYC = "NEW KYC";
var STR_ARCHSTS_FINAL = "FINAL";
var STR_ARCHSTS_OPEN = "OPEN";
var MGRAPPRSTS_APPROVE = "APPROVE";
var STFTYPE_ADVISER = "ADVISER";
var STFTYPE_STAFF = "STAFF";
var STFTYPE_PARA = "PARAPLANNER";

var COMM_NOEMAIL_VALMSG = "Email Id not found";

var OLD_ARCH_TEMPLATE = "The selected archive is not latest!. Cannot save this archive furtherly!";

var FNA_FINALARCH_VALMSG = "The selected KYC Form is finalised. Cannot change without client consent!";
var FNA_FINALARCHANYONE_VALMSG = "One of the KYC Form is finalised. Cannot change without client consent!";
var FNA_YETTOAPPR_BYMGR = "The selected archive yet to approve by manager!";

var FNA_EMAILTO_VALMSG = "Key in the e-mail TO address!";
var FNA_EMAILSUB_VALMSG = "Key in the e-mail subject! ";
var FNA_EMAILCONTENT_VALMSG = "Key in the e-mail content!";

var FNA_SELFNAMEMAND_VALMSG = "Key in the client(1) name!";
var FNA_SELFNRICMAND_VALMSG = "Key in the client(1) NRIC!";




var FNA_INVGOALPUR_VALMSG = "Key in the Investment Goal Purpose!";

var FNA_FLOWTYPE_VALMSG = "Select the flow type!";

var FNA_FATCACONTRY_VALMSG = "select the country of tax residence";
var FNA_FATTAXREFERNUM_VALMSG = "Key in the Tax Identification Number (TIN)";
var FNA_NOTAXREASON = "If the TIN is unavailable, please indicate Reason A,B,C.";
var FNA_REASONDETAILS = "Key in the reason of selecting Reason B";
var FNA_FATCA_ATLEAST_1TAX = "Key in at least one country/jurisdiction of Tax Residence and Tax Identification Number (TIN) ";
var FNA_FATCA_USORNOT = " Select any FATCA option ";
var FNA_FATCA_ATTACH = "Attach a supporting document in client(1)(2) attachments screen";

var FNA_DEPNTCONTNAME_VALMSG = "Key in the child name dependant contingency!";

var FNA_HLTHINSPOLTYPE_VALMSG = "Key in the policy type in Health insurance!";

var FNA_PROPOWNOWNSHIP_VALMSG = "Key in the ownership in the property ownership!";

var FNA_VEHIOWNOWNER_VALMSG = "Key in the owner in the vehicle ownership!";

var FNA_ADVRECPRDTREL_VALMSG = "Select the relationship in plan details!";

var FNA_ADVRECPRDTTYPE_VALMSG = "Select the product type in plan details!";

//var FNA_ARTUPLANPRDTTYPE_VALMSG = "Select the Product LOB in New purchase & topup plan details!";
var FNA_ARTUPLANPRDTTYPE_VALMSG = "Key in Company in New purchase & topup plan details!";

var FNA_ARTUFUNDPRDTTYPE_VALMSG = "Select the Product LOB in New purchase & topup fund details!";

//var FNA_SWREPPLANPRDTTYPE_VALMSG = "Select the Product LOB in Switching & replacement plan details!";

var FNA_SWREPPLANPRDTTYPE_VALMSG = "Key in Company in Switching & replacement plan details!";

var FNA_SWREPFUNDPRDTTYPE_VALMSG = "Select the Product LOB in Switching & replacement fund details!";

var FNA_LIFEINSOWNER_VALMSG = "Key in the owner in Life insurance plan details!";

var FNA_INVPLANINVTYPE_VALMSG = "Key in the investment type in investment details!";

var FNA_ARCHSTS_VALMSG = "Select the archive status!";
var FNA_MGRAPPRSTS_VALMSG = "Select the manager approve status!";
var FNA_MGRAPPRSTSUPD_MSG = "Manager Approve Status Updated Successfully!";
var FNA_STFUPD_MSG = "Archive status/Client consent Updated Successfully!";
var FNA_ARTUPLANCLNT_VALMSG = "Key in Client(1)(2) in New purchase & topup plan details!";
var FNA_SWPLANCLNT_VALMSG = "Key in Client(1)(2) in Switching & Replacement plan details!";

var KYC_ALL_PAGE_IMAGES = {
	"DEPENTDET": [{ "ADD": "fnaDepnAddRow", "EDIT": "fnaDepnModRow", "DEL": "fnaDepnDelRow", "EXP": "fnaDepnExpndRow" }],
	"INVGOALDET": [{ "ADD": "fnaInvGoalAddRow", "EDIT": "fnaInvGoalModRow", "DEL": "fnaInvGoalDelRow", "EXP": "fnaInvGoalExpndRow" }],
	"FATCADET": [{ "ADD": "fnaFatcaAddRow", "EDIT": "fnaFatcaModRow", "DEL": "fnaFatcaDelRow", "EXP": "fnaFatcaExpndRow" }],
	"FLOWDET": [{ "ADD": "fnaFlowDetAddRow", "EDIT": "fnaFlowDetModRow", "DEL": "fnaFlowDetDelRow", "EXP": "fnaFlowDetExpndRow" }],
	"DEPENCONTDET": [{ "ADD": "fnaContDepAddRow", "EDIT": "fnaContDepModRow", "DEL": "fnaContDepDelRow", "EXP": "fnaContDepExpndRow" }],
	"HEALINFONEEDS": [{ "ADD": "fnaHlthInsAddRow", "EDIT": "fnaHlthInsModRow", "DEL": "fnaHlthInsDelRow", "EXP": "fnaHlthInsExpndRow" }],
	"PROPOWNRSP": [{ "ADD": "fnaPropAddRow", "EDIT": "fnaPropModRow", "DEL": "fnaPropDelRow", "EXP": "fnaPropExpndRow" }],
	"VEHIOWNRSP": [{ "ADD": "fnaVehiAddRow", "EDIT": "fnaVehiModRow", "DEL": "fnaVehiDelRow", "EXP": "fnaVehiExpndRow" }],
	"LIFEPLNLST": [{ "ADD": "fnaLiPlanAddRow", "EDIT": "fnaLiPlanModRow", "DEL": "fnaLiPlanDelRow", "EXP": "fnaLiPlanExpndRow" }],
	"INPTINVDET": [{ "ADD": "fnaInvstPlanAddRow", "EDIT": "fnaInvstPlanModRow", "DEL": "fnaInvstPlanDelRow", "EXP": "fnaInvstPlanExpndRow" }],
	"ADVRECM": [{ "ADD": "fnaAdvRecAddRow", "EDIT": "fnaAdvRecModRow", "DEL": "fnaAdvRecDelRow", "EXP": "fnaAdvRecExpndRow" }],
	"ADVRECMLIFHEL": [{ "ADD": "fnaArtuPlanAddRow", "EDIT": "fnaArtuPlanModRow", "DEL": "fnaArtuPlancDelRow", "EXP": "fnaArtuPlancExpndRow" }],
	"UTILPFUNDET": [{ "ADD": "fnaArtuFundAddRow", "EDIT": "fnaArtuFundModRow", "DEL": "fnaArtuFundcDelRow", "EXP": "fnaArtuFundcExpndRow" }],
	"LIFHELINSPLN": [{ "ADD": "fnaSwrepPlanAddRow", "EDIT": "fnaSwrepPlanModRow", "DEL": "fnaSwrepPlanDelRow", "EXP": "fnaSwrepPlanExpndRow" }],
	"SWREPUTILPFUN": [{ "ADD": "fnaSwrepFundAddRow", "EDIT": "fnaSwrepFundModRow", "DEL": "fnaSwrepFundDelRow", "EXP": "imgClntCompAddrExpRow" }]
};

var CPF_PART_FLD = {
	"CPFSELFCUR": "txtFldCPFOrdAcCurSelf",
	"CPFSELFCONTR": "txtFldCPFOrdAcContSelf",
	"CPFSPSCUR": "txtFldCPFOrdAcCurSps",
	"CPFSPSCONTR": "txtFldCPFOrdAcContSps"
};

var NAMEOFCLIENT = "Name cannot be blank for client(1) ";
var NRICOFCLIENT = "NRIC/Passport Cannot be blank for client(1)";
var BIRTHCOUNTRYCLIENT = "Country of birth Cannot be blank for client(1)";
var BIRTHCOUNTRYSPOUSE = "Country of Birth Cannot be blank for client(2)";
var GENDEROFCLIENT = "Select Gender for Client(1)";
var DOBOFCLIENT = "DOB cannot be blank for client(1)";
var MARTIALSTATUSOFCLIENT = " Martial Status cannot be blank for client(1)";
var CONTACTDETAILS = "Atleast 2 Contact details required for client(1)";
var CONTACTDETAILS1 = "Atleast 1 Contact detail required for client(1)";
var OCCUPATIONOFCLIENT = "Occupation cannot be empty for client(1)";
var EMPLOYEEROFCLIENT = "Employer cannot be empty for client(1)";
var NATUREBUSINESSOFCLIENT = "Nature of Business cannot be empty for client(1)";
var NATUREBUSINESSOTHERSOFCLIENT = "Nature of Business's Others cannot be empty for client(1)";
var ANNUALINCOMEOFCLIENT = "Annual Income of the Client(1) cannot be empty";
var SOURCEOFFFUNDCLIENT = "Check the Source of Fund";
var SRCFUNDOTHCLIENT = "Key in Source of Fund Others Details";
var MAILINGADDRESSSELF = "Select the Mailing Address of the Client(1)";
var MAILINGADDRESSPOUSE = "Select the Mailing Address of the Client(2)";
var MAILINGADDRESSOTHERS = "Mailing Address cannot be empty";
var REGADDRESSCLIENT = "Registered Address cannot be empty";
var REASONMAILINGADDRESS = "Select Reason for Mailing Address";
var REASONMAILINGADDRESSOTHERS = "Reason for Others Mailing Address cannot be empty";
var CNTRYOFRESIOFCLIENT = "Country of Residence of the Client(1) cannot be empty";
var LANGSPOKCLIENT = "Client(1) English Language Proficiency Level-Spoken Cannot be empty";
var LANGWRITECLIENT = "Client(1) English Language Proficiency Level-Written Cannot be empty";
var EDULVLCLIENT = "Client(1) Highest Educational Qualification Cannot be empty";

var AGECLIENT = "Select Age Next Birthday of the Client(1)";
var NATLYCLIENT = "Select Nationality of the Client(1)";
var NATLYCLIENTOTH = "Select the Citizenship of the Client(1)";
var AGESPOUSE = "Select Age Next Birthday of the Client(2)";
var NATLYSPOUSE = "Select Nationality of the Client(2)";
var NATLYSPOUSEOTH = "Select the Citizenship of the Client(2)";

var NAMEOFSPOUSECLIENT = "Name of the Client(2) cannot be empty";
var NAMEOFSPOUSEGENDER = "Select Gender of the Client(2)";
var DOBOFSPOUSE = "Client(2) DOB cannot be empty";
var NRICOFSPOUSE = "Client(2) NRIC/Passport Cannot be empty";
var MARTIALSTATUSOFSPOUSE = " Select Martial Status of the Client(2)";
var CONTACTDETAILSSPOUSE = "Atleast 2 Contact details of the Client(2) is mandatory";
var CONTACTDETAILSSPOUSE1 = "Atleast 1 Contact details of the Client(2) is mandatory";
var OCCUPATIONOFSPOUSE = "Occupation of the Client(2) cannot be empty";
var EMPLOYEEROFSPOUSE = "Employer of the Client(2) cannot be empty";
var NATUREBUSINESSOFSPOUSE = "Nature of Business of the Client(2) cannot be empty";
var NATUREBUSINESSOTHERSOFSPOUSE = "Nature of Business Others of the Client(2) cannot be empty";
var ANNUALINCOMEOFSPOUSE = "Annual Income of the Client(2) cannot be empty";
var REGADDRESSSPOUSE = "Registered Address cannot be empty";
var MAILINGADDRESSSPOUSE = "Select the Mailing Address for Client(2)";
var MAILINGADDRESSOTHERSSPOUSE = "Mailing Address Client(2) cannot be empty";
var SOURCEOFFFUNDSPOUSE = "Check the Source of Fund for Client(2)";
var SRCFUNDOTHSPOUSE = "Key in Source of Fund Others Details for Client(2)";
var LANGSPOKSPOUSE = "Client(2) English Language Proficiency Level-Spoken Cannot be empty";
var LANGWRITESPOUSE = "Client(2) English Language Proficiency Level-Written Cannot be empty";
var EDULVLSPOUSE = "Client(2) Highest Educational Qualification Cannot be empty";

var SELOBJ_CMPNYNAME_ALERT = 'Select the Company Name ';
var NO_EXIST_POL_REC = "No existing policy record found!";

var SIMPLIFIED_VER = "SIMPLIFIED";
var ALL_VER = "ALL";
var CUST_CATEG_COMPANY = "COMPANY";
var CUST_CATEG_PESRON = "PERSON";



//added by johnson 18062015
//health insurace
var HLTH_INS_POL = 'Key in Insurance policy !';

//Mailing Address
var MAIL_ADDR_SELF = 'Key in Mailing Address !';
var MAIL_ADDR_SPS = 'Key in Client(2) Mailing Address !';
var MAIL_ADDR_RESN = 'Key in Reason for Mailing !';

//Interpreter Details
var INTEPRE_NAME = 'Key in Interpreter Name !';
var INTEPRE_NRIC = 'Key in Intrepreter NRIC/Passport !';
var INTEPRE_CONTACT = 'Key in Interpreter Contact !';
var INTEPRE_SEL_RELSHIP = 'Select Interpreter Relationship !';
var INTEPRE_SEL_UPLOAD = 'Attach a copy of Interpreter NRIC/Passport in client(1)(2) attachment tab!';

//advice and recommandation
var ADV_RECMM_Q1 = 'Please provide reasons/details.';

//CKA Validations
var CKA_EDU_DETS = 'Key in the Name of the Educional Institution';
var CKA_FIN_DETS = 'Key in the Financial Institution Details';
var CKA_INVST_DETS = 'Key in the Investment Institution Details';
var CKA_WRK_EXP_DETS = 'Key in the Name of the Organization';


var Ann_Exp = {
	Ann_Exp_Self: ['txtFldFnaExpRentSelf', 'txtFldFnaExpUtilSelf', 'txtFldFnaExpGrocSelf', 'txtFldFnaExpEatSelf',
		'txtFldFnaExpClothSelf', 'txtFldFnaExpTranSelf', 'txtFldFnaExpMedSelf', 'txtFldFnaExpPersSelf',
		'txtFldFnaExpDepntSelf', 'txtFldFnaExpTaxSelf', 'txtFldFnaExpEntSelf', 'txtFldFnaExpFesSelf',
		'txtFldFnaExpVocaSelf', 'txtFldFnaExpChrtySelf', 'txtFldFnaExpPropSelf', 'txtFldFnaExpVehiSelf',
		'txtFldFnaExpInsSelf', 'txtFldFnaExpOthSelf'],

	Ann_Exp_Sps: ['txtFldFnaExpRentSps', 'txtFldFnaExpUtilSps', 'txtFldFnaExpGrocSps', 'txtFldFnaExpEatSps',
		'txtFldFnaExpClothSps', 'txtFldFnaExpTranSps', 'txtFldFnaExpMedSps', 'txtFldFnaExpPersSps',
		'txtFldFnaExpDepntSps', 'txtFldFnaExpTaxSps', 'txtFldFnaExpEntSps', 'txtFldFnaExpFesSps',
		'txtFldFnaExpVocaSps', 'txtFldFnaExpChrtySps', 'txtFldFnaExpPropSps', 'txtFldFnaExpVehiSps',
		'txtFldFnaExpInsSps', 'txtFldFnaExpOthSps'],

	Ann_Exp_Fam: ['txtFldFnaExpRentFam', 'txtFldFnaExpUtilFam', 'txtFldFnaExpGrocFam', 'txtFldFnaExpEatFam',
		'txtFldFnaExpClothFam', 'txtFldFnaExpTranFam', 'txtFldFnaExpMedFam', 'txtFldFnaExpPersFam',
		'txtFldFnaExpMaintFam', 'txtFldFnaExpDomsFam', 'txtFldFnaExpChildFam', 'txtFldFnaExpTaxFam',
		'txtFldFnaExpEntFam', 'txtFldFnaExpFesFam', 'txtFldFnaExpVocaFam', 'txtFldFnaExpChrtyFam',
		'txtFldFnaExpPropFam', 'txtFldFnaExpVehiFam', 'txtFldFnaExpInsFam', 'txtFldFnaExpOthFam'],

	SUMOF_ANNEXP_SELF: 'SUMOF_ANNEXP_SELF',
	SUMOF_ANNEXP_SPS: 'SUMOF_ANNEXP_SPS',
	SUMOF_ANNEXP_FAM: 'SUMOF_ANNEXP_FAM'
};

var tot_SrcOfIncm = {

	tot_SrcOfIncm_Self: ['txtFldIncSrcEmpSelf', 'txtFldIncSrcAddWageSelf', 'txtFldIncSrcNEAmtSelf',
		'txtFldIncSrcDivSelf', 'txtFldIncSrcRentSelf', 'txtFldIncSrcOthAmtSelf',
		'txtFldIncSrcIncrSelf', 'txtFldIncSrcNEIncSelf', 'txtFldIncSrcDivIncSelf',
		'txtFldIncSrcReIncSelf', 'txtFldIncSrcOthIncrSelf'],

	//commented by johnson on 26052015                    
	//	    tot_SrcOfIncm_Self_Incr  :['txtFldIncSrcIncrSelf','txtFldIncSrcNEIncSelf','txtFldIncSrcDivIncSelf',
	//		            	           'txtFldIncSrcReIncSelf','txtFldIncSrcOthIncrSelf'],

	tot_SrcOfIncm_Self_Prd: ['txtFldIncSrcPerdrSelf', 'txtFldIncSrcNEPrdSelf', 'txtFldIncSrcDivPrdSelf',
		'txtFldIncSrcRePrdSelf', 'txtFldIncSrcOthPrdSelf'],

	tot_SrcOfIncm_Sps: ['txtFldIncSrcEmpSps', 'txtFldIncSrcAddWageSps', 'txtFldIncSrcNEAmtSps',
		'txtFldIncSrcDivSps', 'txtFldIncSrcRentSps', 'txtFldIncSrcOthAmtSps',
		'txtFldIncSrcIncrSps', 'txtFldIncSrcNEIncSps', 'txtFldIncSrcDivIncSps',
		'txtFldIncSrcReIncSps', 'txtFldIncSrcOthIncrSps'],

	//commented by johnson on 26052015                    
	//		tot_SrcOfIncm_Sps_Incr   :['txtFldIncSrcIncrSps','txtFldIncSrcNEIncSps','txtFldIncSrcDivIncSps',
	//		            		       'txtFldIncSrcReIncSps','txtFldIncSrcOthIncrSps'],

	tot_SrcOfIncm_Sps_Prd: ['txtFldIncSrcPerdrSps', 'txtFldIncSrcNEPrdSps', 'txtFldIncSrcDivPrdSps',
		'txtFldIncSrcRePrdSps', 'txtFldIncSrcOthPrdSps'],

	SRCOFINCM_SELF: 'SRCOFINCM_SELF',
	//		SRCOFINCM_SELF_INCR :'SRCOFINCM_SELF_INCR',
	SRCOFINCM_SELF_PRD: 'SRCOFINCM_SELF_PRD',
	SRCOFINCM_SPS: 'SRCOFINCM_SPS',
	//		SRCOFINCM_SPS_INCR  :'SRCOFINCM_SPS_INCR',
	SRCOFINCM_SPS_PRD: 'SRCOFINCM_SPS_PRD'

};

var tot_CPF = {

	tot_CPFSelf_CurrBal: ['txtFldCPFOrdAcCurSelf', 'txtFldCPFSplAcCurSelf', 'txtFldCPFMedCurSelf'],
	tot_CPFSelf_AnnContr: ['txtFldCPFOrdAcContSelf', 'txtFldCPFSplAcContSelf', 'txtFldCPFMedContSelf'],
	tot_CPFSps_CurrBal: ['txtFldCPFOrdAcCurSps', 'txtFldCPFSplAcCurSps', 'txtFldCPFMedCurSps'],
	tot_CPFSps_AnnContr: ['txtFldCPFOrdAcContSps', 'txtFldCPFSplAcContSps', 'txtFldCPFMedContSps'],

	SUMOF_CPF_SELFCR: 'SUMOF_CPF_SELFCR',
	SUMOF_CPF_SELFAC: 'SUMOF_CPF_SELFAC',
	SUMOF_CPF_SPSCR: 'SUMOF_CPF_SPSCR',
	SUMOF_CPF_SPSAC: 'SUMOF_CPF_SPSAC'

};


var tot_FincLiab = {

	tot_FincLiab_Self: ['txtFldFLOverdraftSelf', 'txtFldFLShortLoanSelf', 'txtFldFLTaxSelf', 'txtFldFLOthersSelf'],
	tot_FincLiab_Sps: ['txtFldFLOverdraftSps', 'txtFldFLShortLoanSps', 'txtFldFLTaxSps', 'txtFldFLOthersSps'],

	SUMOF_FINLIAB_SELF: 'SUMOF_FINLIAB_SELF',
	SUMOF_FINLIAB_SPS: 'SUMOF_FINLIAB_SPS'
};

var tot_CashAsst = {

	tot_CashAsstSelf: ['txtFldCAFdSelf', 'txtFldCASavSelf', 'txtFldCACashSelf', 'txtFldCASrsSelf'],
	tot_CashAsstSps: ['txtFldCAFdSps', 'txtFldCASavSps', 'txtFldCACashSps', 'txtFldCASrsSps'],
	tot_CashAsstJoin: ['txtFldCAFdJoint', 'txtFldCASavJoint', 'txtFldCACashJoint'],

	SUMOF_CASHASST_SELF: 'SUMOF_CASHASST_SELF',
	SUMOF_CASHASST_SPS: 'SUMOF_CASHASST_SPS',
	SUMOF_CASHASST_JOIN: 'SUMOF_CASHASST_JOIN'
};

var tot_OthrAsst = {

	tot_OthrAsstSelf: ['txtFldOAPersSelf', 'txtFldOAClubSelf', 'txtFldOABusiSelf', 'txtFldOAOthSelf'],
	tot_OthrAsstSps: ['txtFldOAPersSps', 'txtFldOAClubSps', 'txtFldOABusiSps', 'txtFldOAOthSps'],
	tot_OthrAsstJoin: ['txtFldOAPersJoint', 'txtFldOAClubJoint', 'txtFldOABusiJoint', 'txtFldOAOthJoint'],
	tot_OthrAsstLoan: ['txtFldOAPersLoans', 'txtFldOAClubLoans', 'txtFldOABusiLoans', 'txtFldOAOthLoans'],

	SUMOF_OTHASST_SELF: 'SUMOF_OTHASST_SELF',
	SUMOF_OTHASST_SPS: 'SUMOF_OTHASST_SPS',
	SUMOF_OTHASST_JOIN: 'SUMOF_OTHASST_JOIN',
	SUMOF_OTHASST_LOAN: 'SUMOF_OTHASST_LOAN'
};


var kycDhtmlTblId = ["fnaInvsetPlanTbl", "fnaLIPlanTbl", "fnaFatcaTbl",
	"fnaSwrepFundTbl", "fnaswrepplanTbl", "fnaartufundTbl",
	"fnaartuplanTbl", "fnaVehiOwnTbl", //"fnaadvrecTbl",
	"fnaPropOwnTbl", "fnahlthinsTbl", "fnadepntfinanceTbl",
	"fnaFlowDetTbl", "fnaInvGoalTbl", "fnaDepnTbl", "fnaartuplanTblss", "fnaswrepplanTblss"];

var dateFldArr = ['txtFldFnaClntDOB', 'txtFldFnaSpsDob'];//added by johnson on 09022015

var NTUC_SFTP_ACCESS = "NTUC_SFTP_ACCESS";
var NTUC_POLICY_ACCESS = "NTUC_POLICY_ACCESS";
var ADMIN_KYC_APPROVAL = "ADMIN_KYC_APPROVAL";
var COMP_KYC_APPROVAL = "COMP_KYC_APPROVAL";
var ADMIN_APPROVE_STATUS = " Admin Approval is Pending.Compliance Cannot Approve the case.";
var COMPLIANCE_KEYIN_REMARKS = "Remarks cannot be empty.";

var ADMIN_APPROVESTATUS_CONFIRM = "Once Approved, Approval Status cannot be changed. Do you want to Continue?";
var ADMIN_REJECTSTATUS_CONFIRM = "Are you want to Reject the Case.?";
var MANAGER_APPROVESTATUS = "Manager Approval is Pending. Admin Cannot Approve the case.";
var COMPLIANCE_APPROVESTATUS_CONFIRM = "Once Approved, Approval Status cannot be changed. Do you want to Continue?";
var ADMIN_KEYIN_REMARKS = "Remarks cannot be empty.";
var POLICYID_RECEIVED = "Export Cannot be done. Case already submitted to NTUC.";
var ADVISER_ALLOW_EXPORT = "Only Advisers are allowed to export the case.";
var NO_ATTACHID_FOUND = "No attachment Id has been found.";

var STR_CLNTSTS_CLNT = "C-CLIENT";

var AGNT_DECLR_VAL_MSG = 'Representative must select any one of options \nin the Representative Declaration';
var CLNT_CHOICE_VAL_MSG = 'Select any one of the highlighted option in the Application Types';


var USTAX_PRSNCLNT_VAL_MSG = 'US Tax Number for client(1) is required';
var USTAX_PRSNSPS_VAL_MSG = 'US Tax Number for client(2) is required';

var ADNT_INCMPROTECT_VAL_MSG = 'Representative must select any objective(s) in Product Recommendations (New Purchase & Top Up) section';
var ADNT_RESNRECOM_VAL_MSG = 'Representative must key in the Basis of Recommendation - Client\'s Objective(s)';
var ADNT_RESNRECOM1_VAL_MSG = 'Representative must key in the Basis of Recommendation - Reason(s) for Recommendations';
var ADNT_RESNRECOM2_VAL_MSG = 'Representative must key in the Basis of Recommendation - Risk / Limitation(s) of Product';
var ADNT_RESNRECOM3_VAL_MSG = 'Representative must key in the Basis of Recommendation - Reason(s) for Deviation(s) on Fin. Advisers recomm';


var CLNT_ACKN_VAL_MSG = 'Client must select Agree/Partially agree from the given option in Client declaration & Acknowledgement section';
var CLNT_CHOICE_REPRECOMM_VAL_MSG = 'Representative must fill the Client\'s Choice/Remarks on Representative\'s Recommendation/(s)';

var SUPR_REVIEW_VAL_MSG = 'Supervisor must agree/disagree under Supervisor Review section in Page14';
var SUPRREVW_RESN_VAL_MSG = 'Supervisor must key in the Reason(s) and Follow-up Action under Supervisor Review section in Page14';
var TPP_UPLOAD_VAL_MSG = 'Attach the copy of third parties  NRIC / Passport / ACRA(for company) ';
var KYC_APPROVAL_ENSURE_MSG = 'Please ensure you have verified all the pages in the FNA Form before your approval';

var SELF_PROF_EDULEVEL = "Select Highest Educational Qualification for Client(1)";
var SPS_PROF_EDULEVEL = "Select Highest Educational Qualification for Client(2)";

var SELF_LANG_PROFLEVEL = "Select the Client(1) Language Proficiency in Proficiency Level Section";
var SPS_LANG_PROFLEVEL = "Select the Client(2) Language Proficiency in Proficiency Level Section";


var FNA_ARTUPLAN_COMP_VALMSG = "Key in Product Name!";
var FNA_ARTUPLAN_PT_VALMSG = "Product Name cannot be left blank in Product & Recommendations (New Purchase &amp; Top Up) [For LIFE & HEALTH INSURANCE]";

var FNA_ARTUPLAN_RISKRATE_VALMSG = "Select the Risk Rating for ILP Product!";

var FNA_ARTUFUND_COMP_VALMSG = "Key in Company!";
var FNA_ARTUFUND_PT_VALMSG = "Product / Fund Name cannot be left blank in Product & Recommendations (New Purchase &amp; Top Up) [For UT and/or ILP]";


var FNA_SWREPPLAN_COMP_VALMSG = "Key in Product Name!";
var FNA_SWREPPLAN_PT_VALMSG = "Product Name cannot be left blank in Advice & Recommendations (Switching & Replacement) [For LIFE & HEALTH INSURANCE (PLAN DETAILS)]";

var FNA_SWREPFUND_COMP_VALMSG = "Key in Company!";
var FNA_SWREPFUND_PT_VALMSG = "Product Name cannot be left blank in Advice & Recommendations (Switching & Replacement) [For UT and ILP (FUND DETAILS)]";

var FNA_EXSTPOLREP_POLDETMAND = "Key in Withdrawn / Terminated Policy Details  in Policy Status Review Section of Financial Wellness Review";
var FNA_EXSTPOLREP_POLDET = "No  Withdrawn / Terminated Polic(ies) found in Policy Status Review Section of Financial Wellness Review";

var FNA_DEPNTNAME_VALMSG = "Key in the Dependant Name in Dependents Section of Financial Wellness Review";
var FNA_DEPENT_NODET = "No Dependents found in Dependents Section of Financial Wellness Review";

var FNA_REPGEN_INTRPRTR = "You have changed the Interpreter / Trusted Individual section";
var FNA_REPGEN_BENTPPPEP = "You have changed the Beneficial Owner / Third Party Payer / Political Exposed Person (PEP) section";
var FNA_REPGEN_TAXRESIDEC = "You have changed Tax Residency Declaration section";
var FNA_REPGEN_TAXRESIDECCRS = "You have changed Tax Residency Declaration' Declaration for Common Reporting Standard (CRS) section";
var FNA_REPGEN_FINWELLREVIEW = "You have changed the Financial Wellness Review Dependents / Financial Situation and Commitments sections";
var FNA_REPGEN_FINWELLDEPENT = "You have changed the Financial Wellness Review Dependents sections";
var FNA_REPGEN_FINWELLPOLREP = "You have changed the Policy Status Review section";
var FNA_REPGEN_PRODRECOMPRODOBJ = "You have changed the Product Recommendations (New Purchase & Top Up) objective(s) section";
var FNA_REPGEN_PRODRECOMPRODLOB = "You have changed the Product Recommendations (New Purchase & Top Up) Details";
var FNA_REPGEN_BASISRECOM = "You have changed the Basis of Recommendation Detail(s) section";
var FNA_REPGEN_CLIENTACK = "You have changed the Declaration and Acknowledgement section";
var FNA_REPGEN_SUPERVI = "You have changed the Supervisor's Review section";
var FNA_REPGEN_CUSTFB = "You have changed the Feedback section";

var FNA_NONVALIDATION_FIELDS = "You have changed some data in current FNA.";

var FNA_PROD_REC_TBLDEL_CONFIRM = "Do you want to delete all the product details for the ~CLIENT~ ?";
var FNA_PROD_REC_TBLROWDEL_CONFIRM = "Do you want to delete the product details?";

var FNA_ADV_REC_TBLDEL_CONFIRM = "Do you want to delete all the product details for the ~CLIENT~ ?";
var FNA_ADV_REC_TBLROWDEL_CONFIRM = "Do you want to delete the product details?";


var CLIENT_NRICNAME_VAL_MSG = 'Key in Client(1) Name as per NRIC ';
var CLIENT_IDENTITY_VAL_MSG = 'Atleast any one of the client(1) IC detail is mandatory';
var CLIENT_DOB_VAL_MSG = 'Key in Client(1) Date of Birth ';
var CLIENT_ADDR_VAL_MSG = 'Key in Client(1) Registered Address Details';
var APP_ATTACH_CATEGORY_FINPLAN = "FINANCIAL PLANNING";
var APP_ATTACH_CATEGORY_NRICPP = "NRIC/PASSPORT";

var educationList = [{ name: "P", value: "Primary & below" },
{ name: "S", value: "Secondary" },
{ name: "O", value: "GCE O/N Level" },
{ name: "A", value: "Pre-U/JC" },
{ name: "D", value: "Diploma" },
{ name: "G", value: "Degree" },
{ name: "M", value: "Post Graduate" }];

var genderList = [{ name: "M", value: "Male" },
{ name: "F", value: "Female" }];

var languageList = [{ name: "011", value: "English" },
{ name: "211", value: "Mandarin" },
{ name: "264", value: "Malay" },
{ name: "201", value: "Tamil" },
{ name: "99", value: "Others" }];

var maritalList = [{ name: "1", value: "Married" },
{ name: "2", value: "Single" },
{ name: "3", value: "Divorced" },
{ name: "4", value: "Widowed" }];

var nationalityList = [{ name: "702", value: "SG" },
{ name: "703", value: "SGPR" },
{ name: "458", value: "MALAYSIAN" },
{ name: "-1", value: "Others" }];

var occupationList = [{ name: "D088", value: "Housewife" },
{ name: "D161", value: "Student" },
{ name: "D999", value: "Others" }];

var raceList = [{ name: "C", value: "Chinese" },
{ name: "I", value: "Indian" },
{ name: "M", value: "Malay" },
{ name: "OTH", value: "Others" }];

var ATTACH_FOR_PROPOSER = "CLIENT";
var ATTACH_FOR_ASSURED = "SPOUSE/CLIENT";

var NTUC_PRODUCT_ALERT = "Recommend atleast one NTUC Product and then proceed to DSF.";
var NTUC_CHECK_CHANGE = "Do you want to save the current changes ?";

var NO_NRIC_DOC_FOUND = "No NRIC document(s) uploaded for either client(1) or client(2)";
var NO_NRIC_DOC_FOUND_CLNT = "No NRIC document uploaded for client(1)";
var NO_NRIC_DOC_FOUND_SPS = "No NRIC document uploaded for client(2)";

var CLIENT_SIGN_B4_NONDSF = "Please sign the FNA Form and then proceed to esubmission!";
var CLIENT_SIGN_B4_DSF = "Please sign the FNA Form and then proceed to NTUC-DSF esubmission!";


var FNA_APPR_MGR_AGREE = "Select either agree/disagree under Supervisor's section in Page-14";
var MANAGER_KEYIN_REMARKS = "If you disagree, please indicate the reasons and advise on the follow-up action to be taken, where applicable.";


var CONFIRM_APPROVE_MANAGER = "Once Approved, Approval Status cannot be changed. Do you want to Continue?";
var CONFIRM_REJECT_MANAGER = "Are you want to Reject the Case?";


var FNA_CONS_REQ_INFO = "Please key-in all the required information before signature";
var FNA_CONS_MODIFY_INFO = " You have modified the current FNA Form.<br/> Click Proceed to save button to proceed.<br/>Click Cancel to ignore changes.";
var FNA_CONS_MODIFY_SIGN_INFO = " You have modified the current FNA Form.<br/>This FNA Form is already signed by Client.<br/>By modifying the FNA form, you need to re-sign again the FNA Form!.";
var FNA_CONS_SIGN_CLEAR_CONF = " This FNA Form is already signed by Client.<br/>By modifying the FNA form, you need to re-sign again the FNA Form!.";
var FNA_CONS_MODIFY_PDF = "You have modified the current FNA Form. Please save and then generate PDF.";
var FNA_CONS_REQINFO_PDF = "Please key-in all the required information and then generate PDF.";


var FNA_EMPTY_SIGN = "Signature cannot be empty.";

var SWREPCONFLGQ1 = "Select anyone Option in Q1 [Switching / Replacement] Question Section";
var SWREPADVBYFLGQ2 = "Select anyone Option in Q2 [Switching / Replacement] Question Section";
var SWREPDISADVGFLGQ3 = "Select anyone Option in Q3 [Switching / Replacement] Question Section";
var SWREPPROCEEDFLGQ4 = "Select anyone Option in Q4 [Switching / Replacement] Question Section";

var CLNT_CONSENT_VAL_MSG = "Client must select the approach used in conducting the financial advisory process in Client declaration & Acknowledgement section";
var FNA_PROD_RECOM_VALMSG = "Key in at least one Product Recommendation";