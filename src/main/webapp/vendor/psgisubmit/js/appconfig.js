/**
 * 
 */

/*$('.input-group.date').datepicker({
	dateFormat: 'dd/mm/yyyy',
	format: 'dd/mm/yyyy',
	todayBtn: 'linked',
	language: "it",
	todayHighlight: true,
	autoclose: true,
});*/

toastr.options = {
	"closeButton": true,
	"debug": false,
	"newestOnTop": false,
	"progressBar": true,
	"positionClass": "toast-top-right",
	"preventDuplicates": true,
	"showDuration": "1100",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
}

$("input[type=text]").prop("autocomplete", "off");
