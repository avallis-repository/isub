
$(document).ready(function () {
	$("#file-0").fileinput();

	//var fnaId=$("#hdnSessFnaId").val();
	// alert(fnaId);
	//getAllUploadedFile();
	/**/
	/*$("#file-0").fileinput({
		uploadUrl: '/'
	});
*/

});

$("#btnUplodCustAttchFile").click(function () {
	saveDocuments(true);
});

$("#btnUplodAndEmail").click(function () {
	saveDocuments(false);
});

function saveDocuments(flag) {
	let data = new FormData();
	let customerAttachments = {};
	/* var fileslen=document.getElementById('file-0').files.length;
	for(var i=0;i<fileslen;i++){
		var fileInfo=$('#file-0')[0].files[i];
		alert(fileInfo.name);
	}*/

	/*var tmpfilesLength = $("div.file-drop-zone").find("div.file-preview-frame").length;
	tmpfilesLength = tmpfilesLength/2;
	alert(tmpfilesLength)
	for(var i=0;i<tmpfilesLength;i++){
		var fileInfo=$('#input-44')[0].files[i];
		if(fileInfo.name != undefined){
			alert(fileInfo.name);
		}
	}*/

	let filesLength = filesArr.length;

	for (let filesIndex = 0; filesIndex < filesLength; filesIndex++) {
		//var fileInfo=$('#file-0')[0].files[i];
		let fileInfo = filesArr[filesIndex];
		let filePrefix = fileInfo.name.replaceAll("(", "_");
		let fileId = filePrefix.replaceAll(")", "_");
		let categ = document.getElementById("categ" + fileId);
		//var selCateg = categ.options[categ.selectedIndex].text;
		let selCateg = categ.value;
		data.append('fileUpload', fileInfo);

		customerAttachments.title = document.getElementsByName("tit" + fileId)[0].value;
		customerAttachments.attachFor = document.getElementsByName("attach" + fileId)[0].value;
		customerAttachments.attachCategName = selCateg;
		//document.getElementsByName("categ"+fileInfo.name)[0].value;
		customerAttachments.attachCategId = categMap.get(selCateg);
		customerAttachments.fileName = fileInfo.name;
		customerAttachments.remarks = document.getElementsByName("rem" + fileId)[0].value;
		customerAttachments.insurerName = document.getElementsByName("insur" + fileId)[0].value;
		data.append("custAttachments", JSON.stringify(customerAttachments));
	}

	if (filesLength == 1) {
		data.append("custAttachments", "{}");
	}
	if (filesLength > 0) {
		$.ajax({
			url: "CustomerAttachments/saveCustomerAttachment",
			type: "POST",
			data: data,
			processData: false,
			contentType: false,
			dataType: "text",
			cache: false,
			// async:false,
			beforeSend: function () {
				$('#cover-spin').show();
			},
			success: function (data, textStatus, jqXHR) {
				$('#cover-spin').hide();
				//alert("Upload Success");
				toastr.clear($('.toast'));
				toastr.options = { timeOut: "5000", extendedTimeOut: "1000" };
				toastr["success"]("Document(s) are uploaded successfully!");

				$("#uplodModal").modal('hide');
				if (!flag) {
					$("#mailModal").modal("show");
				}

				$("#file-0").fileinput('reset');
				// cacheClear();

				return true;
			},
			error: function (data, textStatus, jqXHR) {
				$('#cover-spin').hide();
				// 	alert("Upload Failed, try again");
				toastr.clear($('.toast'));
				toastr.options = { timeOut: "5000", extendedTimeOut: "1000" };
				toastr["error"]("Document(s) upload failed!");
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator'
				});
				return false;
			}
		});
	} else {
		toastr.clear($('.toast'));
		toastr.options = { timeOut: "5000", extendedTimeOut: "1000" };
		toastr["error"]("Document(s) are not selected to upload!");
		return false;
	}
}

var filesArr = [];

function handleFiles(files) {
	for (let fileIndex = 0; fileIndex < files.length; fileIndex++) {
		filesArr.push(files[fileIndex]);
	}
}

function dltFileName(id) {
	let fileId = id.substring(id.indexOf("_") + 1, id.length);
	const regex = /_20/ig;
	let fName = fileId.replaceAll(regex, ' ');
	for (let fileIdx = 0; fileIdx < filesArr.length; fileIdx++) {
		let fileName = filesArr[fileIdx].name;
		if (fileName == fName) {
			filesArr.splice(fileIdx, 1);
		}
	}
}

function setAllUploadFileData(data) {
	let urlArr = [];
	let preConArr = [];
	let fileNameArr = [];
	for (let docIdx = 0; docIdx < data.length; docIdx++) {
		let obj = data[docIdx];
		let arrrayBuffer = base64ToArrayBuffer(obj.documentData);
		let blob = new Blob([arrrayBuffer], { type: "application/pdf" });
		let link = window.URL.createObjectURL(blob);
		//window.open(link,'', 'height=650,width=840');
		fileNameArr.push(obj.fileName);
		/*var pdfAsDataUri = "data:application/pdf;base64,"+obj.documentData;
		window.open(pdfAsDataUri);*/

		urlArr.push(link);

		let preConJson = {
			type: obj.fileType, caption: obj.fileName, filename: obj.fileName,
			downloadUrl: link, size: obj.fileSize, width: "120px", key: (docIdx + 1)
		};

		preConArr.push(preConJson);

		//var url1 =link;
	}
	setUploadFileView(urlArr, preConArr, fileNameArr);
	setUploadCategDets(data);
}

//View Decrypted file
function setUploadFileView(urlArrData, preConArrData, fileNameArr) {

	//[{caption: obj.fileName, filename: obj.fileName, downloadUrl: url1, size:obj.fileSize, width: "120px", key: 1},]
	$("#file-0").fileinput({
		initialPreview: urlArrData,
		initialPreviewAsData: true,
		initialPreviewFileType: 'image',
		initialPreviewConfig: preConArrData,
		//deleteUrl: "/site/file-delete",
		overwriteInitial: false,
		maxFileSize: 100,
		initialCaption: "Document Upload"
	});

	for (let fileName of fileNameArr) {
		getCategData(fileName);
	}
}

//Set catg name,id and remarks 
function setUploadCategDets(data) {
	for (let obj of data) {
		document.getElementsByName("categ" + obj.fileName)[0].value = obj.attachCategId;

		let objSelect = document.getElementById("categ" + obj.fileName);

		//Set selected
		//setSelectedValue(objSelect, obj.attachCategId);
		setDocTitle(objSelect);

		document.getElementsByName("tit" + obj.fileName)[0].value = obj.title;
		document.getElementsByName("rem" + obj.fileName)[0].value = obj.remarks;
	}
}

function base64ToArrayBuffer(base64) {
	let binaryString = window.atob(base64);
	let binaryLen = binaryString.length;
	let bytes = new Uint8Array(binaryLen);
	for (let binaryIdx = 0; binaryIdx < binaryLen; binaryIdx++) {
		let ascii = binaryString.charCodeAt(binaryIdx);
		bytes[binaryIdx] = ascii;
	}
	return bytes;
}

function checkForEmptyCombo(fldObj, idMsg) {
	/*alert($(fldObj).attr("id"))
	$("#selCustAttachMastAttachCateg").removeClass("hide").addClass("show");
	if(fldObj.length<=1){
		$("#"+idMsg+"Error").removeClass("hide").addClass("show");
		$("#"+idMsg).addClass("err-fld");
		return false;
	}*/
}

//get all Principal Names for Insurer combo field
var prinList = {};
var fnaId = $("#hdnSessFnaId").val();
function getAllInsurPrinList() {
	$.ajax({
		url: "RecommPrdtPlan/getAllProdRecomDets/" + fnaId,
		type: "GET",// async:false,
		dataType: "json",
		contentType: "application/json",
		success: function (response) {
			prinList = response;
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}//End

//Function getAllCategoryList
var categList = {};
function getAllCategoryList() {
	$.ajax({
		url: "MasterAttachCateg/getAllData",
		type: "GET",
		dataType: "json",
		contentType: "application/json",
		// async:false,
		success: function (data) {
			categList = data;
			getAllInsurPrinList();
		},
		error: function (xhr, textStatus, errorThrown) {
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});
}

var titMap = new Map();
var categMap = new Map();
function getCategData(id) {

	let filePrefix = id.replaceAll("(", "_");
	id = filePrefix.replaceAll(")", "_");
	let slctId = "categ" + id;

	let insurId = "insur" + id;

	for (let categIndex = 0; categIndex < categList.length; categIndex++) {
		let categData = categList[categIndex];
		let attchName = categData.attachCategName;
		let titName = categData.docTitle;

		setMapValue(titMap, attchName, titName);
		setCategMap(categMap, attchName, categData.attacCategId);

	}
	/*for(let key of categMap.keys()){
			
			var option = new Option(key,categMap.get(key));
			$(option).html(key);
			document.getElementById(slctId).append(option);
			cateName=attchName;
		}*/
	let arrCateg = [];
	for (let key of categMap.keys()) {
		console.log(slctId, "slctIdslctId")
		let option = new Option(key, categMap.get(key));
		$(option).html(key);
		document.getElementById(slctId).append(option);
		arrCateg.push(key);
		// cateName = attchName;
	}
	//autocomplete(document.getElementById(slctId), arrCateg);
	/*document.getElementById(slctId).autosuggest({
	  sugggestionsArray: arrCateg

  });*/
	/*var newId=slctId.substr(0,slctId.length-4);
	document.getElementById(slctId).id=newId;
	//$("#"+slctId).prop("id",newId);
	 var countriesArray = $.map(countries, function (value, key) { return { value: value, data: key }; });
	$("#"+newId).autocomplete({
        lookup: arrCateg
    });
 $('#dynamic').autocomplete({
        lookup: countriesArray
    });*/
	$("input[name='+slctId+']").change(function () {
		// alert("The text has been changed.");
	});
	autocomplete(document.getElementById(slctId), arrCateg);

	let prodArr = [];
	//set PrincipalNames in Insurer Combo filed
	for (let prinIndex = 0; prinIndex < prinList.length; prinIndex++) {
		let prinData = prinList[prinIndex];
		// var prinId = prinData.recomPpPrin;
		let prinName = prinData.recomPpPrin;

		let selFldInsurId = document.getElementById(insurId), curntVal;
		for (let insurIndex = 0; insurIndex < selFldInsurId.length; insurIndex++) {
			curntVal = selFldInsurId[insurIndex];
			prodArr.push(curntVal.value.trim());
		}
		if (!prodArr.includes(prinName)) {
			let prinOpts = document.createElement('option');
			prinOpts.value = prinName;
			prinOpts.text = prinName;
			document.getElementById(insurId).appendChild(prinOpts);
		}
	}
	//End
}


function setCategMap(map, key, value) {
	if (!map.has(key)) {
		map.set(key, value);
		return;
	}
}
function setMapValue(map, key, value) {
	if (!map.has(key)) {
		var setVal = new Set();
		map.set(key, setVal.add(value));
		return;
	}
	map.get(key).add(value);
}

function setDocTitle(slctBoxid) {
	let id = $(slctBoxid).attr("id");
	let titId = "tit" + id.substring(5, id.length);
	//clear all selectbox value except first option
	let select = document.getElementById(titId);
	let length = select.options.length;
	if (length > 1) {
		for (let optIdx = length - 1; optIdx >= 0; optIdx--) {
			select.options[optIdx] = null;
		}

		let option = new Option("--SELECT--", "");
		// $(option).html(val);
		document.getElementById(titId).append(option);
	}

	document.getElementById(titId).value = "";

	let text = slctBoxid.options[slctBoxid.selectedIndex].text
	//       console.log("text-->",text,"titMap",titMap)
	//Set doctitle 
	let arrTit = [];
	let titKeys = titMap.get(text);
	if (titKeys != undefined) {
		for (let val of titKeys) {
			let option = new Option(val, val);
			$(option).html(val);
			document.getElementById(titId).append(option);
			arrTit.push(val);
		};
		//autocomplete(document.getElementById(titId), arrTit);
	}
}

function cacheClear() {
	//clear browser cache using jquery
	$.ajax({
		url: "",
		context: document.body,
		success: function (s, x) {
			$("html[manifest=saveappoffline.appcache]").attr('content', '');
			$(this).html(s);
		}
	});
}

function getAllUploadedFile() {
	$('#file-0').fileinput('clear');
	filesArr = [];
	$("#btnUplodAndEmail").addClass('d-none');

	//if(fnaId != "null"){
	$.ajax({
		url: "CustomerAttachments/getAllUploadFile",
		type: "GET",
		// async:false,
		beforeSend: function () {
			$('#cover-spin').show();
		},
		success: function (data) {
			$('#cover-spin').hide();
			//setAllUploadFileData(data);
			if (data.length > 0) {
				// $("#DocListSec").removeClass("hide").addClass("show");
				// $("#noDocFoundSec").removeClass("show").addClass("hide");
				hideNoFileFoundImg();
				loadDocJsnDataNew(data);
			} else {
				// $("#DocListSec").removeClass("show").addClass("hide");
				// $("#noDocFoundSec").removeClass("hide").addClass("show");
				showNoFileFoundImg();
			}
		},
		error: function (data, textStatus, jqXHR) {
			$('#cover-spin').hide();
			Swal.fire({
				icon: 'error',
				text: 'Please Try again Later or else Contact your System Administrator',
			});
		}
	});

	//}
}

/*function loadDocJsnDataNew(jsnData){
	$('#dynaAttachNTUCList').empty();
   for(var i=0;i<jsnData.length;i++){
	   var doc=jsnData[i];
	   let docFileName = doc.fileName.replaceAll(" ", "%20");
	   var strList = '<a href="#" class="list-group-item list-group-item-action p-2">'+
	   '<div class="d-flex w-100 justify-content-between "><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Doc.Title : '+doc.title+'</h6> <small><i class="fa fa-download cursor-pointer" style="color: #009A78;" onclick=downloadDoc(\"'+doc.documentData+'\",\"'+doc.fileType+'\",\"'+docFileName+'\") title="Click to Download Doc."></i></small></div>'+
	   '<small class="font-sz-level6">File Name : '+doc.fileName+'</small>  </a>';
	
	   $("#dynaAttachNTUCList").append(strList);
	}

}*/

function loadDocJsnDataNew(jsnData) {
	$('#dynaAttachNTUCList').empty();
	for (let jsnIdx = 0; jsnIdx < jsnData.length; jsnIdx++) {
		let doc = jsnData[jsnIdx];
		let docFileName = doc.fileName.replaceAll(" ", "%20");
		let strList = '<a href="#" class="list-group-item list-group-item-action p-2" id="' + doc.custAttachId + '">' +
			'<div class="row">' +
			'<div class="col-8"><h6 class="mb-1 font-sz-level5 text-custom-color-gp bold">Category : ' + doc.attachCategName + '</h6></div>' +
			'<div class="col-1"><small><i class="fa fa-trash cursor-pointer" style="color: #f04040;" onclick=deleteDoc(this) title="Delete Doc."></i></small></div>' +
			'<div class="col-1"><small><i class="fa fa-download cursor-pointer" style="color: #007bff;" onclick=downloadDoc(\"' + doc.documentData + '\",\"' + doc.fileType + '\",\"' + docFileName + '\") title="Click to Download Doc."></i></small></div>' +
			'</div>' +
			'<div class="row">' +
			'<div class="col-12"><small class="font-sz-level6">Title : ' + doc.title + '</small></div>' +
			'<div class="col-12"><small class="font-sz-level6"><span style="font-weight: 450;">Insurer Name :</span> ' + doc.insurerName + '</small> </div>' +
			'</div>' +
			'</a>';

		$("#dynaAttachNTUCList").append(strList);
	}

	let totlDocListLen = $("#dynaAttachNTUCList").children().length;
	if (totlDocListLen > 0) {
		// $("#noDocFoundSec").addClass("hide").removeClass("show")
		// $("#DocListSec").addClass("show").removeClass("hide")
		hideNoFileFoundImg();
	} else {
		// $("#noDocFoundSec").addClass("show").removeClass("hide")
		// $("#DocListSec").addClass("hide").removeClass("show")
		showNoFileFoundImg();
	}

	$('#dynaAttachNTUCList a:even').addClass('bsc-pln-bg');
}

function deleteDoc(thisObj) {
	let cusAttachId = $(thisObj).parents(".list-group-item").attr("id");
	//alert(cusAttachId)
	Swal.fire({
		title: 'Are you sure?',
		text: "Want to Delete Document File?",
		icon: 'warning',
		allowOutsideClick: false,
		allowEscapeKey: false,
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes, Delete it!'
	}).then((result) => {
		if (result.isConfirmed) {
			$.ajax({
				url: "CustomerAttachments/delete/" + cusAttachId,
				type: "DELETE",
				dataType: "text",
				// async:false,
				success: function (response) {
					let totlDocListLen = $("#dynaAttachNTUCList").children().length;
					if (totlDocListLen > 1) {
						$(thisObj).parents(".list-group-item").remove();
						hideNoFileFoundImg();
					} else {
						$(thisObj).parents(".list-group-item").remove();
						showNoFileFoundImg();
					}

					Swal.fire("Deleted!", "File / Document has been deleted.", "success");
				},
				error: function (xhr, textStatus, errorThrown) {
					Swal.fire({
						icon: 'error',
						text: 'Please Try again Later or else Contact your System Administrator',
					});
				}
			});
		} else {
			Swal.fire("Cancelled", "File / Document detail is safe :)", "error");
		}
	});
}

function hideNoFileFoundImg() {
	$("#noDocFoundSec").addClass("hide").removeClass("show");
	$("#DocListSec").addClass("show").removeClass("hide");
}

function showNoFileFoundImg() {
	$("#noDocFoundSec").addClass("show").removeClass("hide");
	$("#DocListSec").addClass("hide").removeClass("show");
}

function downloadDoc(docData, fileType, fileName) {
	let arrrayBuffer = base64ToArrayBuffer(docData);
	let blob = new Blob([arrrayBuffer], { type: "application/" + fileType });
	let link = window.URL.createObjectURL(blob);

	let a = document.createElement("a");
	document.body.appendChild(a);
	a.style = "display: none";
	a.href = link;
	a.download = fileName.replaceAll("%20", " ");
	a.click();
}

//AutoComplete

/*Auto complete text box*/
function autocomplete(inp, arr) {
	/*the autocomplete function takes two arguments,
	the text field element and an array of possible autocompleted values:*/
	let currentFocus;
	/*execute a function when someone writes in the text field:*/
	inp.addEventListener("input", function (e) {
		let a, b, val = this.value;
		/*close any already open lists of autocompleted values*/
		closeAllLists();
		if (!val) { return false; }
		currentFocus = -1;
		/*create a DIV element that will contain the items (values):*/
		a = document.createElement("DIV");
		a.setAttribute("id", this.id + "autocomplete-list");
		a.setAttribute("class", "autocomplete-items");
		/*append the DIV element as a child of the autocomplete container:*/
		this.parentNode.appendChild(a);
		/*for each item in the array...*/
		for (let arrIdx = 0; arrIdx < arr.length; arrIdx++) {
			/*check if the item starts with the same letters as the text field value:*/
			//if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
			if (arr[arrIdx].indexOf(val.toUpperCase()) != -1) {
				/*create a DIV element for each matching element:*/
				b = document.createElement("DIV");
				/*make the matching letters bold:*/

				//b.innerHTML = "<strong>" + arr[i].substr(arr[i].indexOf(val), val.length) + "</strong>";
				// b.innerHTML += arr[i].substr(val.length);
				b.innerHTML += arr[arrIdx].replace(val.toUpperCase(), '<strong>' + val.toUpperCase() + '</strong>');
				/*insert a input field that will hold the current array item's value:*/
				b.innerHTML += "<input type='hidden' value='" + arr[arrIdx] + "'>";
				/*execute a function when someone clicks on the item value (DIV element):*/
				b.addEventListener("click", function (e) {
					/*insert the value for the autocomplete text field:*/
					inp.value = this.getElementsByTagName("input")[0].value;
					/*close the list of autocompleted values,
					(or any other open lists of autocompleted values:*/
					closeAllLists();
				});
				a.appendChild(b);
			}
		}
	});
	/*execute a function presses a key on the keyboard:*/
	inp.addEventListener("keydown", function (e) {
		let x = document.getElementById(this.id + "autocomplete-list");
		if (x) x = x.getElementsByTagName("div");
		if (e.keyCode == 40) {
			/*If the arrow DOWN key is pressed,
			increase the currentFocus variable:*/
			currentFocus++;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 38) { //up
			/*If the arrow UP key is pressed,
			decrease the currentFocus variable:*/
			currentFocus--;
			/*and and make the current item more visible:*/
			addActive(x);
		} else if (e.keyCode == 13) {
			/*If the ENTER key is pressed, prevent the form from being submitted,*/
			e.preventDefault();
			if (currentFocus > -1) {
				/*and simulate a click on the "active" item:*/
				if (x) x[currentFocus].click();
			}
		}
	});
	function addActive(x) {
		/*a function to classify an item as "active":*/
		if (!x) return false;
		/*start by removing the "active" class on all items:*/
		removeActive(x);
		if (currentFocus >= x.length) currentFocus = 0;
		if (currentFocus < 0) currentFocus = (x.length - 1);
		/*add class "autocomplete-active":*/
		x[currentFocus].classList.add("autocomplete-active");
	}
	function removeActive(x) {
		/*a function to remove the "active" class from all autocomplete items:*/
		for (let xIdx = 0; xIdx < x.length; xIdx++) {
			x[xIdx].classList.remove("autocomplete-active");
		}
	}
	function closeAllLists(elmnt) {
		/*close all autocomplete lists in the document,
		except the one passed as an argument:*/

		let x = document.getElementsByClassName("autocomplete-items");
		for (let itemIdx = 0; itemIdx < x.length; itemIdx++) {
			if (elmnt != x[itemIdx] && elmnt != inp) {
				x[itemIdx].parentNode.removeChild(x[itemIdx]);
			}
		}
	}
	/*execute a function when someone clicks in the document:*/
	document.addEventListener("click", function (e) {
		let titId = e.target.id;
		if (titId.startsWith("tit")) {
			let categId = "categ" + titId.substring(3, titId.length);
			let text = document.getElementById(categId).value;
			let arrTit = [];
			let titKeys = titMap.get(text);
			if (titKeys != undefined) {
				for (let val of titKeys) {
					arrTit.push(val);
				};
				document.getElementById(titId).value = arrTit[0];
				autocomplete(document.getElementById(titId), arrTit);
			}
		}
		closeAllLists(e.target);
	});

}