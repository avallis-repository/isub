<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>eKYC Approval Section</title>

<%--   <link rel="stylesheet" href="${contextPath}/newapproval/bootstrap.min.css">
  
  <link rel="stylesheet" href="${contextPath}/newapproval/font-awesome.min.css">
  
<script src="${contextPath}/newapproval/jquery.min.js"></script>

<script src="${contextPath}/newapproval/popper.min.js"></script>

<script src="${contextPath}/newapproval/bootstrap.min.js"></script>

<script src="${contextPath}/newapproval/pdfobject.min.js"></script>

<link rel="stylesheet" href="${contextPath}/newapproval/normalize.min.css">

<script src="${contextPath}/newapproval/prefixfree.min.js"></script>

 <link rel="stylesheet" href="${contextPath}/newapproval/select2.min.css">
 <script src="${contextPath}/newapproval/select2.min.js"></script>
 
 <link rel="stylesheet" href="${contextPath}/newapproval/dataTables.bootstrap4.min.css">

<script src="${contextPath}/newapproval/jquery.dataTables.min.js"></script>

<script src="${contextPath}/newapproval/dataTables.bootstrap4.min.js"></script> --%>
 
  <link rel="stylesheet" href="${contextPath}/newapproval/style.css"> 
 

</head>
<body>

<!-- <div id="cover-spin"></div> -->
 <div id="tooltip"></div>
 
 
<form id="frmTempLogin" method="POST" action="KycApprovalPre.action" autocomplete="off">

	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_DISTID")%>" name="hTxtFldDistId"/>
	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_ADVID")%>" name="hTxtFldAdvId"/>
	<input type="hidden" value="<%=session.getAttribute("LOGGED_ADVSTFID")%>" name="txtFldLogUserAdvId"/>
	<input type="hidden" name="hTxtFldUserId"/>
	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_USERID")%>" name="txtFldCrtdUser" id="txtFldCrtdUser"/>
	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_MGRACSFLG")%>" name="hTxtFldMgrAcsFlg"/>
	<input type="hidden" value="<%=session.getAttribute("MANAGER_ACCESSFLG")%>" name="mgrAcsFlg"/>
	<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_FNAID")%>" name="hTxtFldFnaId" id="hTxtFldFnaId"/>
	<input type="hidden" id="hTxtFldReload" name="hTxtFldReload"/>
</form>


<%-- <div class="kyc-navbar">
    <nav class="navbar fixed-top navbar-expand-md navbar-light bg-light" style="background-image: url('${contextPath}/newapproval/images/AvallisCorpLeftCornerImage.png'); background-repeat: no-repeat;background-size: 490px;">
    
        <a href="#" class="navbar-brand" style="color:#fff">eKYC Approval</a>
        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
        
        	 <div class="navbar-nav">
        	 
        	 </div>
        	 
            <div class="navbar-nav">
               <input type="hidden" id="txtFldNTUCCaseId" name="txtFldNTUCCaseId"/>
               <input type="hidden" id="txtFldNTUCCaseStatus" name="txtFldNTUCCaseStatus"/>
               <input type="hidden" id="txtFldAdvStfName"  name="txtFldAdvStfName"/>
               <input type="hidden" id="txtFldAdvStfId" name="txtFldAdvStfId" />
               <input type="hidden" id="txtFldCustName" name="txtFldCustName"/>
               <input type="hidden" id="txtFldCustId" name="txtFldCustId"/>
               <input type="hidden" id="txtFldAdvEmailId" name="txtFldAdvEmailId"/>
               <input type="hidden" id="htxtFldFnaPolCrtdFlg" name="htxtFldFnaPolCrtdFlg"/>
            </div>
            
            
        	 <div class="navbar-nav invisible">
        	 	<select id="dropDownClntNameDiv">
		        		<option disabled="disabled" selected="true">Client Search.....</option>
		        </select>
        	 </div>
            
           
        </div>
    </nav>
</div> --%>


<nav class="navbar navbar-expand-sm bg-light navbar-light">

  <div class="container">
    <!-- Links -->
    <ul class="nav nav-tabs block" id="landingpage">
      <li class="nav-item">
        <a class="nav-link active" data-toggle="tab" href="#homepage"><i class="fa fa-tachometer"></i><span> Dashboard</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#detailspage"><i class="fa fa-wpforms"></i><span> View FNA</span></a>
      </li>
      
    </ul>
     <div class="navbar-nav">
        	 	    <select id="dropDownQuickLinkFNAHist" class="form-control"  >
		        		  <option disabled="disabled" selected="true">FNA List.....</option>
		        		  <optgroup label="Pending" id="optgrpPending"></optgroup>
		        		  <optgroup label="Approved" id="optgrpApproved"></optgroup>
		        		  <optgroup label="Rejected" id="optgrpRejected"></optgroup>
		           </select>
        	 </div>
  </div>

</nav>

<div class="tab-content block">

<div class="tab-pane container-fluid active" id="homepage">

<div class="row" style="margin-top: 20px;">

    <div class="col-sm-4">
    	<div class="right-tabs clearfix">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs block">
          <li class="nav-item ">
            <a class="nav-link" data-toggle="tab" href="#shome"><span class="txt_colr"><i class="fa fa-tasks"></i>Your Approvals</span></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content block">

          <div class="tab-pane container active" id="shome">
          
            <div class="row stats cht cursor-pointer" onclick="openList('PENDING')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle; font-size:130%;" ><i class="fa fa-question-circle"></i> Pending</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc-secondary progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.PENDING}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                      	 ${FNA_MGR_STATUS_LIST.PENDING}
		                      	 </c:if>	</span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
          
          

            <div class="row stats cht cursor-pointer" onclick="openList('APPROVED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle; font-size:130%;"><i class="fa fa-check-circle"></i> Approved</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.APPROVE}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                    	${FNA_MGR_STATUS_LIST.APPROVE}
		                     </c:if> </span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          

            <div class="row stats cht cursor-pointer" onclick="openList('REJECTED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle;font-size:130%;"><i class="fa fa-times-circle"></i> Rejected</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.REJECT}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                      	 ${FNA_MGR_STATUS_LIST.REJECT}
		                      	 </c:if></span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>
          
          <hr/>
          <div class="clearfix"></div>
          
          <section>
                  <div class="pieID pie"></div>
                    <ul class="pieID legend">
                         <li>
                            <em>Pending</em>
                            <span>${FNA_MGR_STATUS_LIST.PENDING}</span>
                         </li>
                         
                          <li>
                             <em>Approved</em>
                             <span>${FNA_MGR_STATUS_LIST.APPROVE}</span>
                          </li>
                          
                          <li>
                             <em>Rejected</em>
                             <span>${FNA_MGR_STATUS_LIST.REJECT}</span>
                         </li>
      
                    </ul>
            </section>

        </div>
        </div>
        
       
     
    </div>
    
    <div class="col-sm-8">
   
    	<div class="right-tabs clearfix">
    	
    	 <ul class="nav nav-tabs block"  id="yourAprovTabs">
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#home"><i class="fa fa-question-circle-o"></i><span>Pending</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu1"><i class="fa fa-check-circle-o"></i><span>Approved</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu2"><i class="fa fa-times-circle-o"></i><span>Rejected</span></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

          <div class="tab-pane  active" id="home">
          
          <table id="tblPending" class="table table-striped table-bordered hover"  style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:200px">Client</div></th>				                
				                <th><div style="width:80px">Manager</div></th>
				                <th><div style="width:80px">Admin</div></th>
				                <th><div style="width:80px">Compliance</div></th>
				                <th><div style="width:100px">CustId</div></th>
				                <th><div style="width:100px">AdvId</div></th>
				                <th><div style="width:100px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>


          </div>

          <div class="tab-pane fade" id="menu1">
          
          	<table id="tblApproved" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				           <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:200px">Client</div></th>				                
				                <th><div style="width:80px">Manager</div></th>
				                <th><div style="width:80px">Admin</div></th>
				                <th><div style="width:80px">Compliance</div></th>
				                <th><div style="width:100px">CustId</div></th>
				                <th><div style="width:100px">AdvId</div></th>
				                <th><div style="width:100px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>

          <div class="tab-pane fade" id="menu2">
         	 <table id="tblRejected" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:200px">Client</div></th>				                
				                <th><div style="width:80px">Manager</div></th>
				                <th><div style="width:80px">Admin</div></th>
				                <th><div style="width:80px">Compliance</div></th>
				                <th><div style="width:100px">CustId</div></th>
				                <th><div style="width:100px">AdvId</div></th>
				                <th><div style="width:100px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                 <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>

        </div>
    	
       
        </div>

    </div>

  </div> 


</div>



<div class="tab-pane container-fluid fade stats" id="detailspage">

<form name="frmKycApprovalNew"  id="frmKycApprovalNew" >
    <div class="container-fluid">
    
    <div class="row">
    
    	<div class="col-1 col-md-1">
    	
    		<div class="card">
		     	<div class="card-header text-center "><i class="fa fa-info-circle"> </i></div>
		        <div class="card-body justify-content-center">
		        <div class="row">
			    	<div class="col-12  ">
			    		<div class="font-weight-bold text-center text-ekyc" style="font-size:60%">Manager</div>	
			    		<div class="badge"><i id="iFontMgr" class="fa fa-question-circle-o"></i></div>		
			    	</div>
			    	
			    	<div class="col-12 mt-3 border-bottom border-success"></div>
			    	
			    	<div class="col-12 mt-3 ">
			    		<div class="font-weight-bold text-center text-ekyc" style="font-size:60%">Admin</div>			    		
			    		<div class="badge">
				    			<i id="iFontAdmin" class="fa fa-question-circle-o" ></i>
				    		</div>
			    	</div>
			    	
			    	<div class="col-12 mt-3 border-bottom border-success"></div>
			    	<hr/>
			    	
			    	<div class="col-12 mt-3 ">
			    		<div class="font-weight-bold text-center text-ekyc" style="font-size:55%">Comp.</div>
			    		
			    			<div class="badge">
				    			<i id="iFontComp" class="fa fa-question-circle-o"></i>
				    		</div>
			    	</div>
			    </div>
		        
		        </div>
			 </div>
			 
			 	<div class="col-12 mt-3 border-bottom border-default"></div> 
			 <div class="card" id="ntucblock">
		     	<div class="card-header text-center"><span class="text-center"><img src="${contextPath}/newapproval/images/ntucincome.png"  class="rounded mx-auto d-block" alt="NTUC Income - ESubmission"  title="NTUC Income - ESubmission" style="width:60px"/></span><i class="fa fa-info-circle"> </i></div>
		        <div class="card-body justify-content-center" id="NTUCFieldChkDiv">
		        	<span class="text-center"><img src="${contextPath}/newapproval/images/policy-pdf.png"  class="rounded mx-auto" alt="Get NTUC Policy Details"  title="Get NTUC Policy Details" onclick="callPolNoAPINew(this)" id="imgpolapi" style="display:none" /></span>
		        	
		        </div>
			 </div>
    	
    	</div>
		 
		 <div class="col-3 col-md-3">
			  <div class="card">			
				<div class="card-header txt_colr">Approval Sections </div>			
				<div class="card-body" >			
				<nav class="nav-justified ">
				                  <div class="nav nav-tabs " id="nav-tab" role="tablist">
				                    <a class="nav-item nav-link active" id="appr-mgr-tab" data-toggle="tab" href="#appr-mgr-tab-cont" role="tab" aria-controls="appr-mgr-tab-cont" aria-selected="true"><strong>Manager</strong> </a>
				                    <a class="nav-item nav-link" id="appr-admin-tab" data-toggle="tab" href="#appr-admin-tab-cont" role="tab" aria-controls="appr-admin-tab-cont" aria-selected="false"><strong>Admin</strong> </a>
				                    <a class="nav-item nav-link" id="appr-comp-tab" data-toggle="tab" href="#appr-comp-tab-cont" role="tab" aria-controls="appr-comp-tab-cont" aria-selected="false"><strong>Compliance</strong> </a>
				                    
				                  </div>
				                </nav>
				                <div class="tab-content" id="nav-tabContent" style="padding:15px;">
				                
				                
				                  <div class="tab-pane fade show active" id="appr-mgr-tab-cont" role="tabpanel" aria-labelledby="appr-mgr-tab">
				                  
				                        <div class="pt-3"></div>
				                        
				                        <div class="row">
										   <div class="col-md-12">
										   <label for="" class="text-ekyc">Status</label>
										    <select class="form-control no-border no-bgcolor bottom-border" id="txtFldMgrApproveStatus" > 
										        <option value="" >Pending</option>
										        <option value="APPROVE">Approve</option>
										        <option value="REJECT">Reject</option>
										       
										    </select>
										   </div>
										</div>
										
										<div class="row">
											<div class="col-md-12">
											 <label for="" class="text-ekyc">Remarks:</label>
											  <textarea class="form-control no-border no-bgcolor bottom-border" id="txtFldMgrApproveRemarks" readonly></textarea>
											</div>
										</div>
										
										<div class="row">
											 <div class="col-md-6">
										        <label class="text-ekyc" id="mngrapprLbl" ></label> 
										             <div class="input-group form">
														<input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldMgrApproveDate" readonly>											                 
											         </div>
										    </div>
										    
										    <div class="col-md-6">
										        <label class="text-ekyc" id="mngrapprByLbl"></label> 
										             <div class="input-group form">
														<input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldMgrStsApprBy" readonly>											                 
											         </div>
										    </div>
										    
										    
										</div>
										<div class="row">
											<div class="col-md-6 text-center">
												<input type="button" class="btn btn-primary btn-sm" value="Update" onclick="updateStatus()"/>
												</div>
										</div>
						
				                        
				                  </div>
				                  <div class="tab-pane fade" id="appr-admin-tab-cont" role="tabpanel" aria-labelledby="appr-admin-tab">
				                       <div class="pt-3"></div>
				                       
				                       <div class="row">
										   <div class="col-md-12">
										   <label for="" class="text-ekyc">Status</label>
										    <select class="form-control no-border no-bgcolor bottom-border" id="txtFldAdminApproveStatus"> 
										        <option value="APPROVE">Approve</option>
										        <option value="REJECT">Reject</option>
										        <option value="">Pending</option>
										    </select>
										   </div>
										</div>
										
										<div class="row">
											<div class="col-md-12">
											 <label for="" class="text-ekyc">Remarks:</label>
											  <textarea class="form-control no-border no-bgcolor bottom-border" id="txtFldAdminApproveRemarks" readOnly=true></textarea>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6">
										        <label class="text-ekyc" id="admnapprLbl" ></label> 
										             <div class="input-group form">
											               <input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldAdminApproveDate" readonly>
											                   
											         </div>
										    </div>
										    
										    <div class="col-md-6">
										        <label class="text-ekyc" id="admnapprByLbl" ></label> 
										             <div class="input-group form">
											               <input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldAdminStsApprBy" readonly>
											                   
											         </div>
										    </div>
										    
										</div>
						
				                  </div>
				                  <div class="tab-pane fade" id="appr-comp-tab-cont" role="tabpanel" aria-labelledby="appr-comp-tab">
				                       <div class="pt-3"></div>
				                        <div class="row">
										   <div class="col-md-12">
										   <label for="" class="text-ekyc">Status</label>
										    <select class="form-control no-border no-bgcolor bottom-border" id="txtFldCompApproveStatus"> 
										        <option value="APPROVE">Approved</option>
										        <option value="REJECT">Rejected</option>
										        <option value="">Pending</option>
										    </select>
										   </div>
						   
										   
										</div>
						
										<div class="row">
											<div class="col-md-12">
											 <label for="" class="text-ekyc">Remarks:</label>
											  <textarea class="form-control no-border no-bgcolor bottom-border" id="txtFldCompApproveRemarks" readonly>Enter Remarks here...</textarea>
											</div>
										</div>
						
										<div  class="row">
											<div class="col-md-6">
														     <label class="text-ekyc" id="compapprLbl" ></label> 
														             <div class="input-group form">
															               <input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldCompApproveDate" readonly>
															                  
															         </div>
											</div>
											
											<div class="col-md-6">
														     <label class="text-ekyc" id="compapprByLbl" ></label> 
														             <div class="input-group form">
															               <input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldCompStsApprBy" readonly>
															                  
															         </div>
														    </div>
										
										</div>
				                  </div>
				                </div>
				
				
				</div>
				
				 <!--  <div class="card-footer text-muted text-center invisible">
				    
				    <i id="iFontMgr" class="fa fa-check-circle-o">&nbsp;-&nbsp;Approved</i> |
				    <i id="iFontMgr" class="fa fa-times-circle">&nbsp;-&nbsp;Rejected</i> |
				    <i id="iFontMgr" class="fa fa-question-circle-o">&nbsp;-&nbsp;Yet to Review</i>
				  </div>  -->
				
				</div>
				
				<hr/>
				
				<div class="card">
				<div class="card-header txt_colr">Policy Submission Docs.
				<i class="fa fa-file-archive-o pull-right cursor-pointer"  title="Zip all Documents" onclick="downLoadAllFile();"></i>
				</div>
				<div class="card-body">
				  
				  <!--##### poovathi add this block for both NTUC and NRIC Doc Fetch start  -->
				<div class="list-group">
                    <a href="#" class="list-group-item text-ekyc disabled"><strong>NRIC Documents</strong></a>
                       <div class="list-group-item" id="dynaAttachNRICList"></div>
                      
                   
                   <a href="#" class="list-group-item text-ekyc disabled"><strong>Other Documents</strong></a>
                      <div class="list-group-item" id="dynaAttachNTUCList">
                      <c:forEach items="${listoffiles}" var="file">
                      
                      
                      <a href="#" class="list-group-item list-group-item-action">
	<div class="d-flex w-100 justify-content-between ">
	<h6 class="mb-1">Doc.Title : ${file[3]} </h6> 
	<small><i class="fa fa-download cursor-pointer" style="color: #009A78;" onclick="downLoadNtucAttach('${file[0]}')" title="Click to Download Doc."></i></small></div>
    <small>File Name : ${file[4]}</small>  </a>
                      </c:forEach>
                      </div>
  
                 </div>
				  <!-- End#### -->
				
				
				</div>
				</div>
			 
			 </div>
			 
			 
			 <div class="col-8 col-md-8">
				<div class="card">
				<div class="card-header">
				
				<span class="pull-left">
					<strong class="text-center" >FNA ID:</strong>  <input type="text"  id="txtFldFnaId" value="${fnaId}" readonly style="border:none;background:transparent"/>
					
				</span>
				
				
			    
			     	<i title="Print FNA Form" class="fa fa-print pull-right cursor-pointer" aria-hidden="true" onclick="generateFNA()" data-toggle="modal" data-target="#PDFReportModal" data-backdrop="static"></i>
				 </div>
				<div class="card-body" style="overflow-x:auto">
				<div id="KycIntroFrm">
					<!-- <iframe  style="height:100vh;width:100%;border:0px;" id="dynaFrame"> -->
					           <div id="wizard">
		      
		      
                 <h2><i class="fa fa-user icon-color">&nbsp;Personal Details</i></h2>
                <section class="readonly">
                  <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
			         <div class="card-header" id="cardHeaderStyle">
			         <i class="fa fa-user"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Personal Details</span></div>
			         <hr>
                    	<jsp:include page="/views/pages/personalDetails.jsp"></jsp:include>
                    </div>
                </section>
                
                <h2><i class="fa fa-plus icon-color">&nbsp;Fin.Wellness Review</i></h2>
                <section class="readonly">
                    <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-plus"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Fin.Wellness Review</span></div>
				         <hr>
	                    <jsp:include page="/views/pages/finanWellContent.jsp"></jsp:include>
                    </div>
				</section>
                
               
               <!--  <h2><i class="fa fa-columns icon-color">&nbsp;Product Recomm </i></h2> -->
                <section class="readonly">
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-columns"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Product Recommendations</span></div>
				         <hr>
                    <jsp:include page="/views/pages/prodRecomContent.jsp"></jsp:include>
                    </div>
                </section>
                
                <h2><i class="fa fa-columns icon-color">&nbsp;Product Switching </i></h2>
                <section class="readonly">
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-columns"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Product Switching</span></div>
				         <hr>
                    	<jsp:include page="/views/pages/prodSwitchContent.jsp"></jsp:include>
					</div>
                </section>
                
                <h2><i class="fa fa-chart-area icon-color">&nbsp;Adv & Basis Recomm </i></h2>
                <section class="readonly">
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-chart-area"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Adv & Basis Recomm</span></div>
				         <hr>
                    <jsp:include page="/views/pages/advBasisContent.jsp"></jsp:include>
                    </div>
				</section>

                 <h2><i class="fa fa-chart-area icon-color">&nbsp;Tax Residency</i></h2>
                <section class="readonly">
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-chart-area"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Tax Residency</span></div>
				         <hr>
                    <jsp:include page="/views/pages/taxResiContent.jsp"></jsp:include>
                    </div>
                </section>
                
                <h2><i class="fa fa-user icon-color">&nbsp;Client Signature</i></h2>
                <section>
                   <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-user"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Client Signature</span></div>
				         <hr>
                    <jsp:include page="/views/pages/clientSignContent.jsp"></jsp:include>
                    </div>
                </section>
                
                <h2><i class="fa fa-users icon-color">&nbsp;Advisor Declr.</i></h2>
                <section class="readonly">
                    <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-users"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Advisor Declaration</span></div>
				         <hr>
                    <jsp:include page="/views/pages/advisorDeclrContent.jsp"></jsp:include>
					</div>
                </section>
                
                <h2><i class="fa fa-file-archive-o icon-color">&nbsp;Attachment Details </i></h2>
                <section class="readonly">
                    <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-file-archive-o"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Attachment Details</span></div>
				         <hr>
                    <jsp:include page="/views/pages/attachmentDetlsContent.jsp"></jsp:include>
					</div>
                </section>
                
                <h2><i class="fa fa-eye icon-color">&nbsp;Approval Sections </i></h2>
                <section>
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-eye"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Approval Sections</span></div>
				         <hr>
                    <jsp:include page="/views/pages/approvalContent.jsp"></jsp:include>
                    </div>
                </section>
                 
            </div>
				</div>	
					
					<!-- </iframe> -->	
				</div>
				</div>
			</div>
			
			
		 
    </div>
    
    </div>
    </form>

</div>
</div>

    <form id="hiddenForm" method="post" action="" target="TheWindowBIRT" ></form>
    
    <!--lOGOUT MODAL   -->
<div class="modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><h4>Logout <i class="fa fa-lock"></i></h4><button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body"><i class="fa fa-question-circle"></i> Are you sure you want to Logout?</div>
      <div class="modal-footer"><a href="javascript:;" class="btn btn-primary " onclick="logoutKyc();">Ok</a>
       <a href="javascript:;" class="btn btn-primary" data-dismiss="modal">Cancel</a> 
      </div>
    </div>
  </div>
</div>
<!--LOGOUT MAODAL END  -->

<div class="modal" tabindex="-1" role="dialog" aria-hidden="true" id="modalNTUCApproval">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><h6>KYC Approval Document<i class="fa fa-upload"></i></h6></div>
      <div class="modal-body">
      		<form id='hdnFormKycApprDoc' method='POST'>
      			<div class="form-control">
      				<input type='file' name='fileKycCompApprove' id='fileKycCompApprove' class='fpFileBrowseBtn' style='width:100%;'>
      			</div>
      			
      		</form>
      </div>
      <div class="modal-footer"><a href="javascript:;" class="btn btn-primary " onclick="validateNTUCApprDoc()">Upload</a>
       
      </div>
    </div>
  </div>
</div>


<!--REPORT MODAL START  -->
  <div class="modal fade" id="PDFReportModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
       <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">PDF REPORT</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="">
          <iframe  style="height:100vh;width:53vw;border:0px;" id="PDFReportFrame"></iframe>
          </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
<!--REPORT MODAL END  -->
    
</body>

<script src="${contextPath}/newapproval/Constants.js"></script>
<script src="${contextPath}/newapproval/commonScript.js"></script>
<script src="${contextPath}/newapproval/lookupJAXreq.js"></script>
<script src="${contextPath}/newapproval/kycApprovalScript.js"></script>

<script>
var caseId = '${caseId}';
var context = '${contextPath}';

$(document).ready(function(){

	//$('#cover-spin').show(0);
	
	baseUrl = "<%=request.getContextPath()%>";
	

	COMMON_SESS_VALS = <%=session.getAttribute("COMMON_SESS_OBJ")%>;	
	
	isFromMenu=<%=request.getAttribute("FROM_MENU")%>;
	strFnaMgrUnAppDets=<%=request.getAttribute("FNA_MGR_NOTUPDATED_DETS")%>;	
	strAdminFNADets=<%=request.getAttribute("ADMIN_FNA_VERSION_DETS")%>;
	strComplianceFNADets=<%=request.getAttribute("COMPLIANCE_FNA_VERSION_DETS")%>;
	strALLFNAIds=<%=request.getAttribute("ALL_FNA_IDS")%>;

	callBodyInit();

	 
});

function isEmpty(str){
	if(str == null || str.length == 0 || str===undefined)return true;
	else return false;
}

function updateStatus() {
	var apprsts = $("#txtFldMgrApproveStatus").val();

	if (apprsts == "APPROVE") {

		$('#modalNTUCApproval').modal({
			  backdrop: 'static',
			  keyboard: false,
			  show:true,
			}); 

	}else if(apprsts == "REJECT"){

	}else{
		alert("Select either Approve or reject ");
	}
}

function validateNTUCApprDoc(){

	
	var file = document.getElementById("fileKycCompApprove").value;

	if(isEmpty(file)){
		alert("Select the document");
		return false;
	}else{

		
		
		var fnaId = $("#txtFldFnaId").val() 
		var mangrEmailId = "";
		var remarks = "";
		var hNTUCPolicyId = caseId,ntucCaseId =caseId;
		var polNo = "";
		var advName = "";
		var advstfId = "";
		var custName = "";
		var custId = "";
		var custNRIC = "";
		var advstfEmailId = "";
		
		var approverAdvStfId = "";
		var approverUserId = "";
		var advstfcode = "586043";
		
		var jsontext = ""//formJsonText(fnaId,advstfcode);
		var thisval =$("#txtFldMgrApproveStatus").val().toUpperCase();
		
		var ntucCasests = "";
		var ntucPolCrtdFlg = "";
		
		var fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";

		
		
		var ajaxParam = 'CALLFOR=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
		thisval+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
		"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
		+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+hNTUCPolicyId
		+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
		+"&CASEID="+ntucCaseId+"&CASESTATUS="+thisval+"&ADVSTFCODE="+advstfcode
		+"&FNAID="+fnaId+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
		+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin+"&KYCJSON="+jsontext
		console.log(ajaxParam)
		
		
		var form = $('#hdnFormKycApprDoc')[0];
		var data = new FormData(form);
		
		
		$.ajax({
	        url : context +"/OAuthServlet"+"?"+ajaxParam,
	        type: "POST",
	        data :data,
	        cache: false,
	        enctype: 'multipart/form-data',
	        contentType: false,
	        processData: false
	    }).done(function(response){ 
	    	
	    	
//	    	loadMsg.style.display="none";	

			$('#modalNTUCApproval').modal('hide');
	    	
	    	var retval =  $.parseJSON(response);
			
			for ( var val in retval) {
				
				var tabdets = retval[val];
				
				/* if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				} */
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						var value = tabdets[tab];
						console.log(tab,value)
						 
						if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
							
							
							alert(value[0].COMP_POLICY_STATUS);
							
						}
						
						if(tab == "UPDATEKYC2_STATUS"){
						//	alert(value);
						}
						
						
						if(tab == "UPDATEKYC2_DSF_STATUS" ){
//							alert(value.toLowerCase())
							if(value.toLowerCase() == "success"){
							
							}else if(value.toLowerCase() == "failure"){
								

								
							}
							
						}
						
						
						if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
							alert(value);
							
						}
					}
				}
			}
	    });
	
	}
}


</script>
<script src="vendor/psgisubmit/js/kyc_approval.js"></script>
<script type="text/javascript" src="vendor/psgisubmit/js/fna_common.js"></script>

<!-- App js -->
  <script src="vendor/psgisubmit/js/kyc_home.js"></script>
 <script src="vendor/psgisubmit/js/finan_well_review.js"></script>
 
 <script src="vendor/psgisubmit/js/tax_residency.js"></script>
 <script src="vendor/psgisubmit/js/client_signature.js"></script>
 
 <script src="vendor/GitUpToast/js/jquery.toast.js"></script>
 <script src="vendor/psgisubmit/js/product_recomm.js"></script> 
 <script>jsnDataProdRecomDetls = ${PRODUCT_RECOM_PLAN_DETS}</script>
</html>

