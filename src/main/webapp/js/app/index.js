var searchtable = $('#dataTableHover').DataTable({
	        
	scrollX: true,
	scrollY:"200px",
	scroller: true,
	scrollCollapse:true,
	autoWidth:false,
	paging:true,
	pagingType: "simple",
	processing: true,
	language: {
     processing: "<img src='img/loading.gif'>"
  },
	columnDefs: [
	                {
	                    "targets": [ 3 ],
	                    "visible": false,
	                    "searchable": false
	                }
	            ]
	 
	
});


$(document).ready(function () {
	

    	$('#searchIcon').hide();
    	$('#goPreviousArrow').hide();
	   
	   /* $('#dataTableHover').DataTable(
			{
				"pageLength": 5,
				 "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
				
			}
		); // ID From dataTable with Hover*/
		
		
		 /*$('#dataTableHover').DataTable( {
        "columnDefs": [ 
            {
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            }
        ]
    } );*/
	  
	  $('#dataTableHover tbody').off('click', 'tr').on( 'click', 'tr', function () {            
			    if ( $(this).hasClass('selected') ) { 
			        $(this).removeClass('selected'); 
			    } 
			    else {                 
			        $('#dataTableHover tr.selected').removeClass('selected'); 
			        
			        var data = searchtable.row( this ).data();
                    var custId = data[3];
                   // var id=$(this).find("td:first").html();
                    window.location.href="ClientInfo"+custId;
                    $(this).addClass('selected').css('color', 'red');                
			    }  
			    
	  });
	$('#exampleModalScrollable').on('hidden.bs.modal', function () {
		  window.location.href="ClientInfo";
	})
	$('#exampleModalScrollable').on('show.bs.modal', function () {
		//GetAllData
      	getAllCustomerDetails();
	})

	$("#radNewClient").on("click",function(){
		 window.location.href="ClientInfo";
	})
	
	
	  
 });

function getAllCustomerDetails(){

searchtable.clear().draw();
	var col0="",col1="",col2="",col3="";
	
	$.ajax({
            url:baseUrl+"/CustomerDetails/getAllData",
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            
            success: function (data) {
	            for (var i=0; i<data.length; i++) {
	                 col0=data[i].custName;
	            	col1=formatDate(data[i].dob);
	            	col2=data[i].othHandPhone;
	            	col3=data[i].custId;
	            	
	            	searchtable.row.add( [col0,col1,col2,col3] ).draw( false );
	            	
	            
	            
	            
	            
		            /*var row = $('<tr><td>'+data[i].custId+'</td><td>' + data[i].custName+ '</td><td>' 
		            		+ formatDate(data[i].dob) + '</td><td>' + data[i].othHandPhone + '</td></tr>');
		            $('#dataTableHover tbody').append(row);*/
        	}
            	alert("Success");
            },
	 		error: function(xhr,textStatus, errorThrown){
	 			alert("Error");
	 			}
            });
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('/');
}
