    
         $(function ()
         {
             $("#wizard").steps({
                 headerTag: "h2",
                 bodyTag: "section",
                 transitionEffect: "slideLeft",
                 stepsOrientation: "vertical",
         titleTemplate: '<span class="badge badge--info">#index#.</span> #title#',
         onFinishing: function (event, currentIndex) {
         //$("#genKYCModalScrollable").modal("show");
         window.location.href="kycHome"
         return true; 
         
         }
         
         
             });
			 
			  $('.example-movie').barrating('show', {
            theme: 'bars-movie'
        });
		
		
		 $('#selFldCompName').select2({
		  templateResult: formatState,
		   placeholder: "Select an Insurance Company"
		  });
		  
		  
		  var data = [
    {
        id: 0,
        text: 'enhancement'
    },
    {
        id: 1,
        text: 'bug'
    },
    {
        id: 2,
        text: 'duplicate'
    },
    {
        id: 3,
        text: 'invalid'
    },
    {
        id: 4,
        text: 'wontfix'
    }
];
		  
		  $('#selFldProdName').select2({
		   placeholder: "Select a Product Name",
		  });
		  

         });
         
		 
	 function formatState (state) {
  if (!state.id) {
    return state.text;
  }
  
  
  var prin = state.element.value.toLowerCase() ;
  var icon = "";
  switch(prin){
  
  case "aviva":
  icon = "img/aviva.png";
  break;
  
  case "ntuc":
  icon = "img/ntucincome.png";
  break;
  
  case "manulife":
  icon = "img/Manulife_logo.png";
  break;
  
  case "aia":
  icon = "img/aia.jpg";
  break;
  default:
  icon="img/noimage.png";
  }
  
 var $state = $('<span><img src="' + icon +'" class="img-flag" style="width:100px;height:20px;"/> &nbsp; | &nbsp; <strong>' + state.text + '</strong></span>'
  );
  return $state;
};
  // add new product details
  $("#btnaddNewProdDetls").click(function(){
	  addNewProdDetails();
	  ClearFormFlds();
  });
  
  // save and Add new Product Details
  
  $("#btnsaveAddNewProdDetls").click(function(){
	  addNewProdDetails();
	  clearPartialFrmDetls();
	  
	  $("#selFldClntName").change(function(){
		  var clntFlag = confirm("Are you want to clear client Name");
			if(clntFlag == true){
			  ClearFormFlds();	
			}  
		  
	  });
	  
	  $("#selFldCompName").change(function(){
		  var CompFlag = confirm("Are you want to clear Company Name");
		     if(CompFlag == true){
			  ClearFormFlds();	
			}     
	   });
	  
  });

     

function addNewProdDetails(){

	var strClntName = $("#selFldClntName option:selected").text();
	var strCompName = $("#selFldCompName option:selected").text();
	var strProdName = $("#selFldProdName option:selected").text();
	var strProdType = $('input[name="radProdType"]:checked').val();
	var strProdLob  = $('input[name="radProdLOB"]:checked').val();
	var strPremium  = $("#txtFldPremium").val();
	var strSumAssured = $("#txtFldSumAssured").val();
	var strPayMethod = $("#selFldPayMethod option:selected").text();
	var strPlanTerm = $("#txtFldPlanTerm").val();
	var strPremTerm = $("#txtFldPremTerm").val();
	var strProdRiskRate = $("#selFldPrdRiskRate option:selected").text();
	
	
		
	var strProdDetls = '<div  class="card mb-1 p-1 m-1 " >'
	                        +'<div class="card-body " style="border:1px solid #ddd">'
						        +'<div class="row pb-1">'
					                     +'<div class="col-8">'
					                      +'<span class="badge badge-light">Product Name :</span><span class="badge badge-pill badge-light">'+strProdName+'</span>'  
					                     +'</div>'                
										+'<div class="col-2"><span class="badge" id="dynamicProdType">'+strProdType+'</span> </div>'														 
					                      +'<div class="col-1"><a class="btn"> <i class="fa fa-pencil-square-o "></i></a></div>' 
										+'<div class="col-1"><a class="btn"><i class="fa fa-trash-o "></i></a></div>'
					           +'</div>'
					                 
					           +'<div class="row pb-1">'
					                     
					                     +'<div class="col-4"><span class="badge badge-light">LOB : </span>  <span class="badge badge-pill badge-light">'+strProdLob+'</span> </div>'
					                     +'<div class="col-4"><span class="badge badge-light">Premium : </span>  <span class="badge badge-pill badge-light">$'+strPremium+'</span> </div>'                                                         
					                     +'<div class="col-4"><span class="badge badge-light">SA : </span> <span class="badge badge-pill badge-light">$'+strSumAssured+'</span> </div>'
					           +'</div>'
					           
					           +'<div class="row pb-1">'
					                     +'<div class="col-4"><span class="badge badge-light">Pay.Method :</span><span class="badge badge-pill badge-light"> '+strPayMethod+'</span> </div>' 
					                     +'<div class="col-4"><span class="badge badge-light">Plan Term :</span> <span class="badge badge-pill badge-light">'+strPlanTerm+'&nbsp;yrs</span> </div>' 
										 +'<div class="col-4"><span class="badge badge-light">Payment Term  : </span><span class="badge badge-pill badge-light">'+strPremTerm+'&nbsp;yrs</span> </div>'
					           +'</div>'
					           
					         //Prod Risk Rate Field Details Append later here...
	             +'</div>'
    +'</div>';
   
    /*  */
          var strCard = 	
         '<div  class="card mb-1 p-1 m-1 ">'
	
			+'<div class="card-header">'
					 +'<div class="row">'
					      +'<div class="col-10" id="'+strCompName+'" >  </div>'
					 +'</div>'
			+'</div>'
	
	        +'<div class="card-body" style="border:1px solid #ddd">'
					+'<div  class="card p-1 m-1 " >'
		                    +'<div class="card-body " style="border:1px solid #ddd">'
		                          +'<div class="row pb-1">'
		                                 +'<div class="col-8">'
                                                +'<span class="badge badge-light">Product Name :</span><span class="badge badge-pill badge-light">'+strProdName+'</span>'  
                                         +'</div>'                
		                                 +'<div class="col-2">'
                                                +'<span class="badge" id="dynamicProdType">'+strProdType+'</span>'
                                         +'</div>'														 
                                         +'<div class="col-1"><a class="btn"> <i class="fa fa-pencil-square-o "></i></a></div>' 
		                                 +'<div class="col-1"><a class="btn"><i class="fa fa-trash-o "></i></a></div>'
                                  +'</div>'
   
                                  +'<div class="row pb-1">'
		       							 +'<div class="col-4"><span class="badge badge-light">LOB : </span>  <span class="badge badge-pill badge-light">'+strProdLob+'</span> </div>'
									     +'<div class="col-4"><span class="badge badge-light">Premium : </span>  <span class="badge badge-pill badge-light">$'+strPremium+'</span> </div>'                                                         
									     +'<div class="col-4"><span class="badge badge-light">SA : </span> <span class="badge badge-pill badge-light">$'+strSumAssured+'</span> </div>'
                                  +'</div>'

                                  +'<div class="row pb-1">'
									       +'<div class="col-4"><span class="badge badge-light">Pay.Method :</span><span class="badge badge-pill badge-light"> '+strPayMethod+'</span> </div>' 
									       +'<div class="col-4"><span class="badge badge-light">Plan Term :</span> <span class="badge badge-pill badge-light">'+strPlanTerm+'&nbsp;yrs</span> </div>' 
										   +'<div class="col-4"><span class="badge badge-light">Payment Term  : </span><span class="badge badge-pill badge-light">'+strPremTerm+'&nbsp;yrs</span> </div>'
								  +'</div>'
		                  +'</div>'
                  +'</div>'
	   +'</div>' ;
    
    
        /* alert("#"+strClntName); */
    
           console.log("Prod Detls CompanyName--->>>>>"+ strCompName);
		   //Based on Company Name Add New Product Details
		   switch(strCompName){
		   
		   case "Aviva Ltd":
			   $("#prodTabContent").find("#"+strClntName).hasClass("active show");
			   $("#prodTabContent").find("#"+strClntName).append(strCard);
			   var PrinImg = '<img id="cardHderPrincipeImg" src="img/aviva.png" class="rounded float-left"  style="width:150px;"/>';
			   $("#"+strCompName).appendTo(PrinImg);
		   break;
		   
		   case "NTUC Income":
			   $("#prodTabContent").find("#"+strClntName).hasClass("active show");
			   $("#prodTabContent").find("#"+strClntName).append(strCard);
			   var PrinImg = '<img id="cardHderPrincipeImg" src="img/ntucincome.png" class="rounded float-left"  style="width:150px;"/>';
			   $("#"+strCompName).append(PrinImg);
		   break;
		   
		   case "ManuLife Financial":
			   $("#prodTabContent").find("#"+strClntName).hasClass("active show");
			   $("#prodTabContent").find("#"+strClntName).append(strCard);
			   var PrinImg = '<img id="cardHderPrincipeImg" src="img/Manulife_logo.png" class="rounded float-left"  style="width:150px;"/>';
			   $("#"+strCompName).append(PrinImg);
		   break;
		   
		   case "AIA Singapore Pte Ltd":
			   $("#prodTabContent").find("#"+strClntName).hasClass("active show");
			   $("#prodTabContent").find("#"+strClntName).append(strCard);
			   var PrinImg = '<img id="cardHderPrincipeImg" src="img/aia.jpg" class="rounded float-left"  style="width:150px;"/>';
			   $("#"+strCompName).append(PrinImg);
		   break;
		   
		   default:
		   break;
		   
		   
		   
		   /* case "Aviva Ltd":
			   $("#avivaCardSecDetls").append(strCard);
		   break;
		   
		   case "NTUC Income":
			   $("#ntucIncomeCardSecDetls").append(strProdDetls);
		   break;
		   
		   case "ManuLife Financial":
			   $("#manuLifeCardSecDetls").append(strProdDetls);
		   break;
		   
		   case "AIA Singapore Pte Ltd":
			   $("#AIACardSecDetls").append(strProdDetls);
		   break;
		   
		   default:
		   break; */
		}
      
		   
		   
	
		   
	if(strProdType == "BASIC"){
		$("#dynamicProdType").addClass("badge badge-success");
	  }else{
		$("#dynamicProdType").addClass("badge badge-secondary");  
	  }	   
	
	
}

 
	
 
function ClearFormFlds(){
	$("select").prop('selectedIndex',0);
	$('input[type="text"]').val("");
	$('input[type="radio"]').prop('checked', false);
	$('#selFldCompName').select2("val", "");
	$('#selFldProdName').select2("val", "");
}
 function clearPartialFrmDetls(){
	 
	 $('#selFldProdName').select2("val", "");
	 $('input[type="text"]').val(""); 
	 $('input[type="radio"]').prop('checked', false);
	 $("select:not(#selFldClntName)").val("");
 }

 function createNewClntTab(strNewClntName){
    var nextTab = $('#prodTab li').length+1;
    // create the tab
	$('<li class="nav-item"><a class="nav-link" href="#'+strNewClntName+'" id="'+strNewClntName+''+nextTab+'" data-toggle="tab" role="tab"  aria-controls="#'+strNewClntName+'">'+strNewClntName+' </a></li>').appendTo('#prodTab');
	// create the tab content
	$('<div class="tab-pane fade" id="'+strNewClntName+'" role="tabpanel" aria-labelledby="'+strNewClntName+''+nextTab+'"> </div>').appendTo('#prodTabContent');
	// make the new tab active
	$('#prodTab a:last').tab('show');
 }
 



   function getClientNames(selObj){
	  var strNewClntName = $(selObj).find('option:selected').text(); 
	 // createNewClntTab(strNewClntName );
	  getTabsCounts(strNewClntName);
	  
   }


     
     
     
     
     function getTabsCounts(strNewClntName){
    	/*  alert("strNewClntName"+strNewClntName); */
    	 var strTab;
     $('#prodTab li a').each(function(){
    	 /* alert($(this).text()) */
    	  strTab = $(this).text();
    	 
        });
     
     tabCreate(strNewClntName,strTab);
     
     }
      
     
  
  function tabCreate(client,tab){
	  
	  if(client==tab){
	 		console.log("strNewClntName--->>>>"+client);
	 		console.log("strTab--->>>>"+tab);
	 	   }else{
	 	     console.log("not-equal-->allow tab create");
	 		 createNewClntTab(client ); 
	 	 }
     
  }
  
     
     
  
    $("#btnAddNewClnt").click(function(){
    	$("#dropdwnSec").removeClass("hide").addClass("show");
    	$("#txtFldAddNewClient").removeClass("error").addClass("border-blue");
    	$("#errorFld").removeClass("show").addClass("hide");
	  var strNewClntName = $("#txtFldAddNewClient").val();
	  if((strNewClntName == "")|(strNewClntName == null)){
		  $("#txtFldAddNewClient").removeClass("border-blue").addClass("error");
		  $("#errorFld").removeClass("hide").addClass("show");
		  $("#txtFldAddNewClient").focus();
		  return false;
	  }
	  var srtNewOpt = '<option>'+strNewClntName+'</option>';
	  $("#selFldClntName").append(srtNewOpt);
	  $("#dropdwnSec").removeClass("show").addClass("hide");
	  //$("#selFldClntName option:selected").text(strNewClntName);
	  $("#txtFldAddNewClient").val("");
	  
	  createNewClntTab(strNewClntName );
	   $("#selFldClntName").focus();
	  	
	  	//$("#selFldClntName option:selected").text(strNewClntName);
	  	
	   });
    
  
   


$('.select2-single').select2();

// Select2 Single  with Placeholder
$('#selFldClntSave').select2({
  templateResult: formatState,
  placeholder: "Select a Client to Save",
  allowClear: true
});  


function formatState (state) {
if (!state.id) {
return state.text;
}


var prin = state.element.value.toLowerCase() ;
var icon = "";
switch(prin){

case "aviva":
icon = "img/aviva.png";
break;

case "ntuc":
icon = "img/ntucincome.png";
break;

case "manulife":
icon = "img/Manulife_logo.png";
break;

case "aia":
icon = "img/aia.jpg";
break;
default:
icon="img/noimage.png";


}
var $state = $('<span><img src="' + icon +'" class="img-flag" style="width:100px;height:20px;" /> &nbsp; | &nbsp; <span style="font-size: 13px;">' + state.text + '</span></span>'
);
return $state;
};



