   $('#dependentSec').on('click', function(){
    	$(this).parent().find('a').trigger('click');
    	$("#collapseDepend").removeClass("hide").addClass("show");
    	//$("#collapseSec2").removeClass("show").addClass("hide");
	 });
	 $('#cashFlowSec').on('click', function(){
	   $(this).parent().find('a').trigger('click');
	   $("#collapsecashFlow").removeClass("hide").addClass("show");
	  // $("#collapseSec1").removeClass("show").addClass("hide");
	 });
	
	 $('#assetLiableSec').on('click', function(){
		   $(this).parent().find('a').trigger('click');
		   $("#collapseassetLiable").removeClass("hide").addClass("show");
		  // $("#collapseSec1").removeClass("show").addClass("hide");
		 });
	                  
   /*$('[data-toggle="pg4DependNote"]').popover({
	   
        placement :'top',   
        html: true,
        content: function() {
         return $('#popover-pg4DependNote').html();
       }
     });*/
   
   $('[data-toggle="pg4CashFlwNote"]').popover({
	   
	   placement :'top', 
       html: true,
       content: function() {
         return $('#popover-pg4CashFlwNote').html();
       }
     });
   
   $('[data-toggle="pg4AssetNote"]').popover({
	   
	   placement :'top',
       html: true,
       content: function() {
         return $('#popover-pg4AssetNote').html();
       }
     });
   
    $('[data-toggle="Pg4assetETANote"]').popover({
    	placement :'left',
    	html: true,
       content: function() {
         return $('#popover-Pg4assetETANote').html();
       }
     }); 
    
    $('[data-toggle="Pg4assetETLNote"]').popover({
    	placement :'left',
    	html: true,
        content: function() {
          return $('#popover-Pg4assetETLNote').html();
        }
      }); 

window.addEventListener("beforeunload", function (e) {
     var dependant={};
     dependant=getDependantData(dependant);
     
     if(isEmpty(dependant)){
       return (e || window.event).returnValue = "Your data may be lost";
     }
     
});
function isEmpty(obj) {
	
    for(var key in obj) {
         if(key == "depnGender" && obj[key] == "Male"){
            obj[key]="";
         }
         if(obj[key]!=""){
            return true;
         }
            
    }
    return false;
   
}
//Get all dependent data
	getAllDepnData();
	
function getAllDepnData(){
  //document.getElementById('loader').style.display = 'block';
  pageLoaderShow();
   $.ajax({
            url:"/PSGISUBMIT/FnaDependantDets/getAllData",
            type: "GET",
            dataType: "json",
            contentType: "application/json",
           
            success: function (data) {
                 if(data.length>0){
                    $('#dependentSec').click();
                 }
	            for(var i=0;i<data.length;i++){
		           
		            addListItem(data[i]);
		        }
            	
            	pageLoaderHide();

            },
	 		error: function(xhr,textStatus, errorThrown){
	 			//alert("Error");
	 			}
            });

}
function pageLoaderShow(){
    $(".page-loader").show();
	$(".overlay").show();
}
function pageLoaderHide(){
   $(".page-loader").hide();
   $(".overlay").hide();
}


function addListItem(dependent){


	var genIcon;
	var genColor;
	if(dependent.depnGender == "Male"){
		genIcon="fa fa-male";
		genColor="blue";
	}
	else{
		genIcon="fa fa-female";
		genColor="deeppink";
	}
var listGroup='<a id='+dependent.depnId+' href="#" class="list-group-item list-group-item-action flex-column align-items-start ">'
    +'<div class="d-flex w-100 justify-content-between">'
      +'<h6 class="mb-1 text-primaryy font-sz-level6 bold">'+dependent.depnName+'</h6>'
      +'<small><i class="fa fa-pencil-square-o" aria-hidden="true" style="color:blue;" onclick="getEditData(/'+dependent.depnId+'/);"></i>&nbsp;&nbsp;<i class="fa fa-trash" aria-hidden="true" style="color: red;" onclick="dltData(/'+dependent.depnId+'/);"></i></small>'
    +'</div>'
   +' <div class="row">'
         
        +' <div class="col-md-12">'
             +'<div class="row">'
               +'  <div class="col-md-4">'
                 +'<span class="font-sz-level7 bold text-custom-color-gp"><strong>Gender&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal"><i class="'+genIcon+'" aria-hidden="true" style="font-size:larger;color:'+genColor+';"></i>&nbsp;('+dependent.depnGender+')</span>'
                 +'</div>'
                 +'<div class="col-md-3">'
                 +' <span class="font-sz-level7 bold text-custom-color-gp"><strong>Age&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+dependent.depnAge+' Yrs.</span>'
                 +'</div>'
                +'<div class="col-md-5" >'
                  +'<span class="font-sz-level7 bold text-custom-color-gp"><strong >No of Yrs to Support&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+dependent.depnYrsSupport+' Yrs.</span>'
                 +'</div>'
                 
            +'</div>'
         +'</div>'
         +'<hr>'
         +'<div class="col-md-12">'
           +' <div class="row">'
                 +'<div class="col-md-4" >'
                 +'<span class="font-sz-level7 bold text-custom-color-gp"><strong>RelationShip&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+dependent.depnRelationShip+'</span>'
                 +'</div>'
                 +'<div class="col-md-8">'
                  +'<span class="font-sz-level7 bold text-custom-color-gp"><strong>Occupation&nbsp;:&nbsp;</strong></span>&nbsp;<span class="font-sz-level7 font-normal">'+dependent.depnOccp+'.</span>'
                 +'</div>'
            +'</div>'
       +'</div>'
    +'</div>';
  


$("#addlist").append(listGroup);
 

}

//Delete Data
function dltData(depnId){
	
	var chk = confirm("Are you want to delete ?");
	if (chk == true) {
		  var len=$('#addlist .list-group-item').length;
		  var fwrFlg;
	 	  if(len-1 ==0){
	          fwrFlg="N"; 	  
	 	  }
	 	  else{
	 	     fwrFlg="N";
	 	  }	
    $.ajax({
            url:"/PSGISUBMIT/FnaDependantDets/deleteData/"+depnId+"/"+fwrFlg,
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
           
            success: function (data) {
	          var id=String(depnId);
	 			var depId=id.substring(1,id.length-1);
	 			$("#"+depId).remove();	
	        },
	 		error: function(data){
	 			
	 			alert(data.errMsg);
	 			 
	 			}
            });
}
}
//Edit dependent data
function getEditData(depnId){
	
    $.ajax({
            url:"/PSGISUBMIT/FnaDependantDets/getDataById/"+depnId,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
           
            success: function (dependent) {
	            
                setDependentData(dependent);

            },
	 		error: function(xhr,textStatus, errorThrown){
	 			//alert("Error");
	 			}
            });

}


var gender="Male";
$("#depnGender").click(function(){
	
	getGender();
	
});
function getGender(){
   var attr=$("#depnGender").attr("class");
	if(attr.includes("active")){
		gender="Male";
	}else{
	    gender="Female";
	}
   
}
var arrDependentDet=["depnName","depnAge","depnYrsSupport","depnRelationShip","depnOccp","depnId"];
function setDependentData(dependent){
	
	if(dependent.depnGender != "Male"){
		$('#depnGender').addClass('active');
	}else{
		$('#depnGender').removeClass('active');
	}
		
	for(var i=0;i<arrDependentDet.length;i++){
		document.getElementsByName(arrDependentDet[i])[0].value=dependent[arrDependentDet[i]];
	}
}
function getDependantData(dependent){
    for(var i=0; i<arrDependentDet.length; i++){
		dependent[arrDependentDet[i]]=document.getElementsByName(arrDependentDet[i])[0].value;
	}
	dependent.depnGender=gender;
	return dependent;

}
function saveDependantData(){
   //Name validation
    var depnName=document.getElementsByName("depnName")[0].value;
    if(depnName==""){
        $("#depName").addClass("errorclz");
          $("#depnNameErrMsg").show();
          return false;
    }
	 $("#depName").removeClass("errorclz");
     $("#depnNameErrMsg").hide();
         
       
	var dependent={};
	dependent=getDependantData(dependent);
	
	
	alert(JSON.stringify(dependent));
	$.ajax({
            url:"/PSGISUBMIT/FnaDependantDets/saveData",
            type: "POST",
             data: JSON.stringify(dependent),
             dataType: "json",
            contentType: "application/json",
           
            success: function (data) {
               $("#"+data.depnId).remove();
            	addListItem(data);
            	clearData();
            },
	 		error: function(data){
	 			    alert(data.errMsg);
	 			}
            });
}
function clearData(){
     for(var i=0; i<arrDependentDet.length; i++){
		document.getElementsByName(arrDependentDet[i])[0].value="";
	}
	/*$('#depnGender').removeClass('active');
	$('#depnGender').addClass('active');*/
	
	 var attr=$("#depnGender").attr("class");
	 if(attr.includes("active")){
			$('#depnGender').toggleClass('active');
			gender="Male";
	  }
   
	 
}
//Validation
function isValidName(){
	var depnName=document.getElementsByName("depnName")[0].value;
    if(depnName==""){
        $("#depName").addClass("errorclz");
          $("#depnNameErrMsg").show();
          return false;
    }
	 $("#depName").removeClass("errorclz");
     $("#depnNameErrMsg").hide();

}
function isNumber(event,errMsgId){
    
    event = (event) ? event : window.event;
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    //$("#depName").addClass("errorclz");
         // $("#numErrMsg").show();
        $("#"+errMsgId).show();
        return false;
    }
    $("#"+errMsgId).hide();
    return true;
	
}

//CashFlow
function calcFinCommit(txtobj,op){
    var txtval = Number(txtobj.value);

	var sa  = $("#cfSelfAnnlinc").val()
	var sb = $("#cfSelfCpfcontib").val();
	var sc = $("#cfSelfEstexpense").val();
	var stot = Number(sa)-Number(sb)-Number(sc);

	$("#cfSelfSurpdef").val(stot);

	var spa  = $("#cfSpsAnnlinc").val()
	var spb = $("#cfSpsCpfcontib").val();
	var spc = $("#cfSpsEstexpense").val();
	var sptot = Number(spa)-Number(spb)-Number(spc);

	$("#cfSpsSurpdef").val(sptot);

	var sfa  = $("#cfFamilyAnnlinc").val()
	var sfb = $("#cfFamilyCpfcontib").val();
	var sfc = $("#cfFamilyEstexpense").val();
	var sftot = Number(sfa)-Number(sfb)-Number(sfc);
	$("#cfFamilySurpdef").val(sftot);
	
}
//asset & Liablities
function calcCombined(txtobj){
    var parrow= txtobj.parentNode.parentNode;
   /* var client = parrow.cells[1].childNodes[0];//alSelfTotasset
    var spouse = parrow.cells[2].childNodes[0]//alSpsTotasset
	var combined = parrow.cells[3].childNodes[0]//alFamilyTotasset
   */	
    //poovathi change field Type (Table to TextField) on 12-11-2020
	var client = $("#alSelfTotasset");//alSelfTotasset
    var spouse = $("#alSpsTotasset");//alSpsTotasset
	var combined = $("#alFamilyTotasset");//alFamilyTotasset
    
	combined.value = Number(client.value)+Number(spouse.value)
	
    var sfa  = $("#cfFamilyAnnlinc").val()
	var sfb = $("#cfFamilyCpfcontib").val();
	var sfc = $("#cfFamilyEstexpense").val();
	var sftot = Number(sfa)-Number(sfb)-Number(sfc);

	$("#cfFamilySurpdef").val(sftot);


	var sa2  = $("#alFamilyTotasset").val()
	var sb2 = $("#alFamilyTotliab").val()
	$("#alFamilyNetasset").val(Number(sa2)-Number(sb2))
}


function calcAsset(txtobj){
	var txtval = Number(txtobj.value)

	var sa  = $("#alSelfTotasset").val()
	var sa1  = $("#alSpsTotasset").val()
	var sa2  = $("#alFamilyTotasset").val()

	var sb  = $("#alSelfTotliab").val()
	var sb1  = $("#alSpsTotliab").val();
	var sb2 = $("#alFamilyTotliab").val()

	$("#alSelfNetasset").val(Number(sa)-Number(sb))
	$("#alSpsNetasset").val(Number(sa1)-Number(sb1))
	$("#alFamilyNetasset").val(Number(sa2)-Number(sb2))
}



 