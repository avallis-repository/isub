 $(function ()
                {
                    $("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft",
                        stepsOrientation: "vertical",
						titleTemplate: '<span class="badge badge--info">#index#.</span> #title#',
						onFinishing: function (event, currentIndex) {
							//$("#genKYCModalScrollable").modal("show");
							window.location.href="kycHome"
							return true; 
						
						}
						
						
                    });
        });
				
	       
   $('[data-toggle="popover1"]').popover({
	   
        placement :'top',   
        html: true,
        content: function() {
         return $('#popover-contentPg10CardNotes1').html();
       }
     });
   
   $('[data-toggle="popover2"]').popover({
	   
	   placement :'top', 
       html: true,
       content: function() {
         return $('#popover-contentPg10CardNotes2').html();
       }
     });
   
   $('[data-toggle="popover3"]').popover({
       html: true,
       content: function() {
         return $('#popover-contentPg10CardNotes3').html();
       }
     });
   
    $('[data-toggle="popover4"]').popover({
       html: true,
       content: function() {
         return $('#popover-contentPg10CardNotes4').html();
       }
     }); 


$('.select2-single').select2();

// Select2 Single  with Placeholder
$('#selFldClntSave').select2({
  templateResult: formatState,
  placeholder: "Select a Client to Save",
  allowClear: true
});  


function formatState (state) {
if (!state.id) {
return state.text;
}


var prin = state.element.value.toLowerCase() ;
var icon = "";
switch(prin){

case "aviva":
icon = "img/aviva.png";
break;

case "ntuc":
icon = "img/ntucincome.png";
break;

case "manulife":
icon = "img/Manulife_logo.png";
break;

case "aia":
icon = "img/aia.jpg";
break;
default:
icon="img/noimage.png";


}
var $state = $('<span><img src="' + icon +'" class="img-flag" style="width:100px;height:20px;" /> &nbsp; | &nbsp; <strong>' + state.text + '</strong></span>'
);
return $state;
};
