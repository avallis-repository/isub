
                $(function ()
                {
                	$('#goPreviousArrow').hide();
                    $("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft",
                        stepsOrientation: "vertical",
						titleTemplate: '<span class="badge badge--info"></span> #title#',
						onFinishing: function (event, currentIndex) {
							//$("#genKYCModalScrollable").modal("show");
							window.location.href="kycHome"
							return true; 
						
						}
						
						
                    });
                });
            


$('.select2-single').select2();

// Select2 Single  with Placeholder
$('#selFldClntSave').select2({
  templateResult: formatState,
  placeholder: "--Select Policy--",
  allowClear: true
});  


function formatState (state) {
if (!state.id) {
return state.text;
}


var prin = state.element.value.toLowerCase() ;
var icon = "";
switch(prin){

case "aviva":
icon = "img/aviva.png";
break;

case "ntuc":
icon = "img/ntucincome.png";
break;

case "manulife":
icon = "img/Manulife_logo.png";
break;

case "aia":
icon = "img/aia.jpg";
break;
default:
icon="img/noimage.png";


}
var $state = $('<span><img src="' + icon +'" class="img-flag" style="width:100px;height:20px;" /> &nbsp; | &nbsp; <strong>' + state.text + '</strong></span>'
);
return $state;
};




// $('#profileModal').css("margin-left", $(window).width() - $('.modal-content').width());
