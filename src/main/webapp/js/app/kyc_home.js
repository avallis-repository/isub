   
                $(function ()
                {
                    $("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft",
                        stepsOrientation: "vertical",
						titleTemplate: '<span class="badge badge--info">#index#.</span> #title#',
						onFinishing: function (event, currentIndex) {
							//$("#genKYCModalScrollable").modal("show");
							window.location.href="kycHome"
							return true; 
						
						}
						
						
                    });
                    
                    
                });
				
				
            
            
            
            

$('.select2-single').select2();

// Select2 Single  with Placeholder
$('#selFldClntSave').select2({
  templateResult: formatState,
  placeholder: "Select a Client to Save",
  allowClear: true
});  


function formatState (state) {
if (!state.id) {
return state.text;
}


var prin = state.element.value.toLowerCase() ;
var icon = "";
switch(prin){

case "aviva":
icon = "img/aviva.png";
break;

case "ntuc":
icon = "img/ntucincome.png";
break;

case "manulife":
icon = "img/Manulife_logo.png";
break;

case "aia":
icon = "img/aia.jpg";
break;
default:
icon="img/noimage.png";


}
var $state = $('<span><img src="' + icon +'" class="img-flag" style="width:100px;height:20px;" /> &nbsp; | &nbsp; <strong>' + state.text + '</strong></span>'
);
return $state;
};

            


// inter/Trust stepper js code start
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	next_fs = $(this).parent().next();
	
	//activate next step on progressbar using the index of next_fs
	$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
	
	//show the next fieldset
	next_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale current_fs down to 80%
			scale = 1 - (1 - now) * 0.2;
			//2. bring next_fs from the right(50%)
			left = (now * 50)+"%";
			//3. increase opacity of next_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
			next_fs.css({'left': left, 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".previous").click(function(){
	if(animating) return false;
	animating = true;
	
	current_fs = $(this).parent();
	previous_fs = $(this).parent().prev();
	
	//de-activate current step on progressbar
	$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
	
	//show the previous fieldset
	previous_fs.show(); 
	//hide the current fieldset with style
	current_fs.animate({opacity: 0}, {
		step: function(now, mx) {
			//as the opacity of current_fs reduces to 0 - stored in "now"
			//1. scale previous_fs from 80% to 100%
			scale = 0.8 + (1 - now) * 0.2;
			//2. take current_fs to the right(50%) - from 0%
			left = ((1-now) * 50)+"%";
			//3. increase opacity of previous_fs to 1 as it moves in
			opacity = 1 - now;
			current_fs.css({'left': left});
			previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
		}, 
		duration: 800, 
		complete: function(){
			current_fs.hide();
			animating = false;
		}, 
		//this comes from the custom easing plugin
		easing: 'easeInOutBack'
	});
});

$(".submit").click(function(){
	return false;
});
// end##########
 


/* Qr code */
document.getElementById("signLoader").style.display = "none";
function getQrCode(){
	
	    document.getElementById("signLoader").style.display = "block";
	  
	    $.ajax({
	    	  url : '/EKYCQR/customer/getQrCodeById/FNA00000000000654',
			 
			 type: 'GET',
			 success: function (byteArray) {
		       $("#qrCodeManager").attr("src", 'data:image/png;base64,'+byteArray);
				// console.log("SUCCESS : ", document.getElementById("qrCode").src);
			  document.getElementById("signLoader").style.display = "none";
	       },
	       error: function (e) {
	       	console.log("ERROR : ", e);
	      }
	   });
}
//Add new insured details
//Create personal details tab

 //Get all self spouse data
    getAllCustomerData();
//Search nric 

function getNricSearch() {
    
    var input, filter, ul, li, a, i, txtValue;
    input = document.getElementById("nricTypeText");
    filter = input.value.toUpperCase();
    ul = document.getElementById("searchListItems");
    
    ul.style.display="block";
    
    if(input.value == ""){
       ul.style.display="none";
    }
    
    
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        txtValue = a.textContent || a.innerText;
       
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
            
        }
    }
}
function addInsured(){
    var selfName="new";
  	var dataFormId="new";
  	addNewInsured(dataFormId,selfName);
  	  var firstTab=0;
    var secondTab=1;
      $('#myTab a:eq('+firstTab+')').tab('show');
  	  $('#myTab a:eq('+secondTab+')').tab('show');

}
function addNewInsured(custId,custName){
	$('.modal').modal('hide');
		
			var nextTab = $('#myTab li').length+1;
			var toShowTab = $('#myTab li').length-2;
			
	
  	 
	$('#myTab').find("a.nav-link").removeClass("active");
	$('#myTab').find(' > li:nth-last-child(2)').before('<li class="nav-item"> <a class="nav-link" id="'+custId+'" data-toggle="tab" href= "#tab'+nextTab+'" role="tab" aria-controls="client'+nextTab+'" aria-selected="false">'+custName+'</a> </li>');
	
   
	
  	// create the tab content
	
	 $('#myTabContent').find("div.tab-pane").removeClass("active");
   
  	 /*$('<div class="tab-pane fade show active" id="tab'+nextTab+'" role="tabpanel" aria-labelledby="client'+nextTab+'-tab" ><input type="text" id="newone'+nextTab+'"/></div>').appendTo('#myTabContent');*/
  	 
  	  var tabPane=$('<div class="tab-pane fade show active" id="client'+nextTab+'-tab" role="tabpanel" aria-labelledby="client'+nextTab+'-tab" > '+$("#myTabContent").find("div.tab-pane").html()+'</div>');
  	  tabPane.appendTo('#myTabContent');
  	  
  	  setCurrentClientData(nextTab,toShowTab,custId);
  	 
  	// make the new tab active
  	//$('#myTab a:eq('+toShowTab+')').tab('show');
	 
 }



//Set Insured Data
$('#myTab').click('shown.bs.tab', function(event){
   event.preventDefault();
   // var x = $(event.target).text();         // active tab
   
   var x=$(event.target).attr("id");
   var tabName=$(event.target).text();
   
   var y = $(event.relatedTarget).text();  // previous tab
  if(!tabName.includes("Expand All") && !tabName.includes("Add New Insured") && !tabName.includes("Collapse All")){
	  /* var hdnSessDataFormId=$("#hdnSessDataFormId").val();
	   updateSelfAndSpouseData(hdnSessDataFormId);*/
	  	updateClientDetails();
	   	
	  	/*alert(client.dfSpsName);
	   		if(client.dfSpsName!=null){
	   		   setSelfSpouseData(client);
	   		}else{
	   		   
	   		}*/
   		
   		
	   if(x.includes("CUST")){
	       			getCustomerDetails(x);
	   			}
	   if(x.includes("SPS")){
	       //setSelfSpouseData(client);
	       getClientDetails(client.dataFormId,"YN");
	   }
	   if(x.includes("new")){
	        setEmptyClientDetails();
	   }
	   else{
	       getClientDetails(x,"N");
	   }
   }
  });
function setCurrentClientData(nextTab,toShowTab,custId){
   if(custId.includes("CUST")){
   		getCustomerDetails(custId);
   }else{
      setEmptyClientDetails();
   }
    
    var firstTab=0;
    var secondTab=toShowTab-2;
      $('#myTab a:eq('+firstTab+')').tab('show');
  	  $('#myTab a:eq('+secondTab+')').tab('show');
}
function getCustomerDetails(custId){
	 $.ajax({
            url:"/PSGISUBMIT/CustomerDetails/getDataById/"+custId,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
               setCustomerData(data);
              
            },
	 		error: function(xhr,textStatus, errorThrown){
	 			  //alert("Error");
	 			}
            });

}
function setCustomerData(data){
    setCustPersonalData(data);
    setCustAddressData(data);
    setCustContactData(data);
    setCustEmpAndFinData(data);
}
function setEmptyClientDetails(){
    setEmptyPersonalData();
    setEmptyAddressData();
    setEmptyContactData();
    setEmptyEmpandFinData();
    setEmptyEduData();
}
function setInsuredData(custId,custName)
{
	 var name=String(custName);
	 var id=String(custId);
	 addNewInsured(id.substring(1,id.length-1),name.substring(1,name.length-1));
}

//Get all customer data
function getAllCustomerData(){
      $.ajax({
            url:"/PSGISUBMIT/CustomerDetails/getAllData",
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
               for(var i=0;i<data.length;i++){
                  setNricSearchListData(data[i]);
               }
            	//alert("success");
            },
	 		error: function(xhr,textStatus, errorThrown){
	 			//alert("Error");
	 			}
            });

}
function setNricSearchListData(selfSpouseData){
  var li='<li><a href="#" onclick="setInsuredData(/'+selfSpouseData.custId+'/,/'+selfSpouseData.custName+'/)">'+selfSpouseData.nric+'</a></li>';
  $("#searchListItems").append(li);


}


//


$('#selFldNRICName').select2({
	   placeholder: "---Add insured Details---",
	   templateResult: formatState,
	   
	  });
	  
function formatState (state) {
	if (!state.id) {
	return state.text;
	}


	var prin = state.element.value.toLowerCase() ;
	var icon = "";
	switch(prin){

	case "Salim M Amin":
	icon = "search1img/.png";
	break;

	case "Danielle Salim":
	icon = "search1img/.png";
	break;

	
	default:
    icon="img/follow.png";
}
	var $state = $('<span><img src="' + icon +'" class="img-flag" style="width:35px;height:20px;" /> &nbsp; | &nbsp; <strong>' + state.text + '</strong></span>'
	);
	return $state;
	};



  
  
  $('[data-toggle="popoverPersDetls"]').popover({
	  placement:'top',
      html: true,
      content: function() {
        return $('#popover-contentPg2PersonalDetlsCardNotes').html();
      }
    }); 
  
  
  function expandAllPage2Accordian(){
	  
	 /*  $('#collexpans').on('click', function () { */
		    $('#accordion .panel-collapse').collapse('toggle');
		/* }); */ 
  }
  
  
  
//Open active tab based on button clicked
/*   $('#testId').on('click', function() {
    var switchTab = $(this).data('tab');   
    activaTab(switchTab);
    function activaTab(switchTab) {
        $('.nav-tabs a[href="#' + switchTab + '"]').tab('show');
    };
  }); */
 
 // Toggle New/Existing Customer
  var custType = $('#customer-type'),
      newCust = $('.new-customer'),
      existCust = $('.existing-customer'),
      createAccBtn = $('.create-account'),
      verifyAccBtn = $('.verify-account');
 
  custType.val($(this).is(':checked'))
          .change(function() {
  if ($(this).is(':checked')) {
        newCust.fadeToggle(400, function() { // Hide Full form when checked
          existCust.fadeToggle(500); //Display Small form when checked
          createAccBtn.toggleClass('hide');
          verifyAccBtn.toggleClass('hide');
        });
        
      } else {
        existCust.fadeToggle(400, function() { //Hide Small form when unchecked
          newCust.fadeToggle(500); //Display Full form when unchecked
          createAccBtn.toggleClass('hide');
          verifyAccBtn.toggleClass('hide');
        });
        
      }
 });
  
   $("#addNewClient2Btn").on("click",function(){
		$('.modal').modal('hide');
		console.log( $(".nav-tabs").children().length );
		var nextTab = $('#myTab li').length+1;
			var toShowTab = $('#myTab li').length-2;
	
  	 
	$('#myTab').find("a.nav-link").removeClass("active");
	$('#myTab').find(' > li:nth-last-child(3)').before('<li class="nav-item"> <a class="nav-link active" id="client'+nextTab+'-tab" data-toggle="tab" href= "#tab'+nextTab+'" role="tab" aria-controls="client'+nextTab+'" aria-selected="false"> Salim'+nextTab+'</a> </li>');
	
   
	
  	// create the tab content
	
	$('#myTabContent').find("div.tab-pane").removeClass("active");
   
  	 $('<div class="tab-pane fade show active" id="tab'+nextTab+'" role="tabpanel" aria-labelledby="client'+nextTab+'-tab" > '+$("#myTabContent").find("div.tab-pane").html()+'</div>').appendTo('#myTabContent');
  	// make the new tab active
	 
  	$('#myTab a:eq('+toShowTab+')').tab('show');
 })
  
    $('#myModalTest').on('shown.bs.modal', function () {
  // Load up a new modal...
  init();
  mode(true)
  load();
})

function enableIntrprt(){
$("#tempIntprtDiv").find(":input").prop("disabled",function(i, v) { return !v; });
}

 
    function init() {
      if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
      var $ = go.GraphObject.make;

      myDiagram =
        $(go.Diagram, "myDiagramDiv");

      myDiagram.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());

      myDiagram.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));

      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "green", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = false;
      // install as first mouse-move-tool
      myDiagram.toolManager.mouseMoveTools.insertAt(0, tool);

      load();  // load a simple diagram from the textarea
    }

    function mode(draw) {
      var tool = myDiagram.toolManager.findTool("FreehandDrawing");
      tool.isEnabled = draw;
    }

    function updateAllAdornments() {  // called after checkboxes change Diagram.allow...
      myDiagram.selection.each(function(p) { p.updateAdornments(); });
    }

    // save a model to and load a model from Json text, displayed below the Diagram
    function save() {
      var str = '{ "position": "' + go.Point.stringify(myDiagram.position) + '",\n  "model": ' + myDiagram.model.toJson() + ' }';
      document.getElementById("mySavedDiagram").value = str;
    }
    function load() {
      var str = document.getElementById("mySavedDiagram").value;
      try {
        var json = JSON.parse(str);
        myDiagram.initialPosition = go.Point.parse(json.position || "0 0");
        myDiagram.model = go.Model.fromJson(json.model);
        myDiagram.model.undoManager.isEnabled = true;
      } catch (ex) {
        alert(ex);
      }
    }
	function expandAll(thisobj){
	     
       
		console.log("$(thisobj)."+$(thisobj).text());
		 $(thisobj).html(function(i, text){
	          return text === '<i class="fa fa-expand" aria-hidden="true" style="color:#656767;"></i> Expand All&nbsp;' ?
			  '<i class="fa fa-compress" aria-hidden="true" style="color:#ff9800;"></i> Collapse All&nbsp;': '<i class="fa fa-expand" aria-hidden="true" style="color:#656767;"></i> Expand All&nbsp;';
	      })
		  $('div.panel-collapse').toggleClass('show');
		  $('div.panel-heading').find('.panel-title').find('a').toggleClass('collapsed');
		  
	}
  
  
  /* function showAlertNofig(){
	  alert()
	  setTimeout(function() {
		  $('#alrtnotig').show();
		  $('#alrtnotig').alert('close'); }, 5000);
  } */

//CRUD operation
 $(document).ready(function () {
	
 			var dataFormId=$("#hdnSessDataFormId").val();
		    getClientDetails(dataFormId,"Y");
			
	});
	
	
//Get client details from particular field



//Set and get PersonalData
	var arrPersonalDet=["dfSelfName","dfSelfNric","dfSelfDob",
							"dfSelfMartsts","dfSelfBirthCntry","dfSelfNationality","dataFormId"];
							
	function getPersonalData(client){
		for(var i=0; i<arrPersonalDet.length; i++){
			client[arrPersonalDet[i]]=document.getElementsByName(arrPersonalDet[i])[0].value;
		}
		var nationality=$('input[name="dfSelfNationality"]:checked').val();
		if(nationality == "Others"){
		     client.dfSelfNationality=$("#selectNationOther").val();
		}else{
		    client.dfSelfNationality=nationality;
		}
		
		return client;
	}
	
	function getSpousePersonalData(spouse){
	  
	   spouse.dfSpsName=document.getElementsByName("dfSelfName")[0].value;
	   spouse.dfSpsNric=document.getElementsByName("dfSelfNric")[0].value;
	   spouse.dfSpsDob=document.getElementsByName("dfSelfDob")[0].value;
	   spouse.dfSpsMartsts=document.getElementsByName("dfSelfMartsts")[0].value;
	   spouse.dfSpsBirthCntry=document.getElementsByName("dfSelfBirthCntry")[0].value;
	   
	   //Nationality
	   var nationality=$('input[name="dfSelfNationality"]:checked').val();
		if(nationality == "Others"){
		     spouse.dfSpsNationality=$("#selectNationOther").val();
		}else{
		    spouse.dfSpsNationality=nationality;
		}
		return spouse;
	   
	}
	
	function setPersonalData(clientData){
		
		for(var i=0; i<arrPersonalDet.length; i++){
			if(clientData[arrPersonalDet[i]]!=undefined){
				document.getElementsByName(arrPersonalDet[i])[0].value=clientData[arrPersonalDet[i]];
			}
		}
		//Tab name as self name
		$("#client1-tab").prop('id',clientData.dataFormId);
		$("#"+clientData.dataFormId).text(clientData.dfSelfName);
		
		//Dob
		setDob(clientData.dfSelfDob);
		
		//Nationality
		setNationality(clientData.dfSelfNationality);
		
	}
	function setSpousePersonalData(spouse){
	   document.getElementsByName("dfSelfName")[0].value=spouse.dfSpsName;
	   document.getElementsByName("dfSelfNric")[0].value=spouse.dfSpsNric;
	   
	   document.getElementsByName("dfSelfMartsts")[0].value=spouse.dfSpsMartsts;
	   document.getElementsByName("dfSelfBirthCntry")[0].value=spouse.dfSpsBirthCntry;
	   setDob(spouse.dfSelfDob);
	   setNationality(spouse.dfSpsNationality);
	   
	   document.getElementsByName("dataFormId")[0].value="SPS";
	   $("#SPS").text(spouse.dfSpsName);
	
	}
	
	
	function setDob(selfDob){
	  	var dob=new Date(selfDob);
		document.getElementsByName("dfSelfDob")[0].value=formatDate(dob);
	}
	function setNationality(nationality){
		  if(nationality !="SG" && nationality != "SG-PR"){
			     $("input[name=dfSelfNationality][value=Others]").attr('checked', true);
			     document.getElementById("selectNationOther").style.display = 'block';
			     $("#selectNationOther").val(nationality);
			    
			}else{
				$("input[name=dfSelfNationality][value=" + nationality + "]").attr('checked', true);
			}
	}
	
	
	function setEmptyPersonalData(){
	  	for(var i=0; i<arrPersonalDet.length; i++){
			document.getElementsByName(arrPersonalDet[i])[0].value="";
	 	}
	 	$('#exampleRadios11').prop('checked', true);
	 	document.getElementById("selectNationOther").style.display = 'none';
	 	document.getElementsByName("dataFormId")[0].value="SPS";
	 	 $("#new").prop('id','SPS');
	}
	
	//Set and get customer details
	
	function setCustPersonalData(customerData){
	    document.getElementsByName("dfSelfName")[0].value=customerData.custName;
	    document.getElementsByName("dfSelfNric")[0].value=customerData.nric;
	    document.getElementsByName("dfSelfMartsts")[0].value=customerData.maritalStatus;
	    document.getElementsByName("dataFormId")[0].value="SPS";
	    //$("#"+customerData.custId).val("SPS");
	    
	    $("#"+customerData.custId).prop('id','SPS');
	    //Dob
	    setDob(customerData.dob);
	    
	    //Nationality
	    setNationality(customerData.nationality);
		
	
	}
	
//End


//Set and get address details

var arrAddrDet=["dfSelfMailAddr","dfSelfHomeAddrr"];
var dfSelfMailAddrFlg;

	function getMailAddressCheckBox(){
	
	  $('#time-01').prop('checked', true);
	  var address=document.getElementsByName("dfSelfHomeAddr")[0].value;
	  document.getElementsByName("dfSelfMailAddr")[0].value=address;
	  $('#time-02').prop('checked', false);
	  dfSelfMailAddrFlg="Y";
	  
	}
	function getDiffAddrCheckBox(){
	
	  $('#time-02').prop('checked', true);
	  document.getElementsByName("dfSelfMailAddr")[0].value="";
	  $('#time-01').prop('checked', false);
	  dfSelfMailAddrFlg="N";
	  
	}


	function getAddressData(client){
	
		/*for(var i=0; i<arrAddrDet.length; i++){
			client[arrAddrDet[i]]=document.getElementsByName(arrAddrDet[i])[0].value;
		}*/
		client.dfSelfMailAddr=document.getElementsByName("dfSelfMailAddr")[0].value;
		client.dfSelfHomeAddr=document.getElementsByName("dfSelfHomeAddr")[0].value;
		client.dfSelfMailAddrFlg=dfSelfMailAddrFlg;
		return client;
		
	}
	
	function getSpouseAddressData(client){
	    client.dfSpsMailAddr=document.getElementsByName("dfSelfMailAddr")[0].value;
		client.dfSpsHomeAddr=document.getElementsByName("dfSelfHomeAddr")[0].value;
		client.dfSpsMailAddrFlag=dfSelfMailAddrFlg;
		return client;
	}
	
	function setSpouseAddressData(spouse){
	     
		var mailAddrFlg=spouse.dfSelfMailAddrFlg;
		
		   document.getElementsByName("dfSelfMailAddr")[0].value=spouse.dfSpsMailAddr;
		   document.getElementsByName("dfSelfHomeAddr")[0].value=spouse.dfSpsHomeAddr;
		   if(mailAddrFlg == "Y"){
		    	 $("#time-01").prop('checked',true);
		    	 $("#time-02").prop('checked',false);
		   }
		   else{
		   		$("#time-02").prop('checked',true);
		   		$("#time-01").prop('checked',false);
		   }
	}
	
	function setAddressData(clientData){
		setAddress(clientData);
	}
	
	function setEmptyAddressData(){
		document.getElementsByName("dfSelfHomeAddr")[0].value="";
		document.getElementsByName("dfSelfMailAddr")[0].value="";
	 	$("#time-01").prop('checked',false);
		$("#time-02").prop('checked',false);
	}
	function setAddress(clientData){
	   var address=clientData.resAddr1+','+clientData.resAddr2
					+','+clientData.resAddr3+','+clientData.resCity
					+','+clientData.resCountry+','+clientData.resPostalCode;
					
		var homeAddr=clientData.dfSelfHomeAddr;
		var mailAddrFlg=clientData.dfSelfMailAddrFlg;
		if(homeAddr != null){
		   /*for(var i=0;i<arrAddrDet.length;i++){
		   		document.getElementsByName(arrAddrDet[i])[0].value=clientData[arrAddrDet[i]];
		   }*/
		   document.getElementsByName("dfSelfMailAddr")[0].value=client.dfSelfMailAddr;
		   document.getElementsByName("dfSelfHomeAddr")[0].value=client.dfSelfHomeAddr;
		   if(mailAddrFlg == "Y"){
		    	 $("#time-01").prop('checked',true);
		    	 $("#time-02").prop('checked',false);
		   }
		   else{
		   		$("#time-02").prop('checked',true);
		   		$("#time-01").prop('checked',false);
		   }
		
		}else{
			document.getElementsByName("dfSelfHomeAddr")[0].value=address;
													          
			document.getElementsByName("dfSelfMailAddr")[0].value=address;
		}
	
	}
	
	//Set customer set and get details
	function setCustAddressData(data){
	      setAddress(data);
	
	}
//End

//Set and get  contact data
var arrContactDet=["dfSelfPersEmail","dfSelfMobile","dfSelfHome","dfSelfOffice"];

	function getContactData(client){
		for(var i=0; i<arrContactDet.length; i++){
			client[arrContactDet[i]]=document.getElementsByName(arrContactDet[i])[0].value;
		}
		return client;
	}
	
	function getSpouseContactData(client){
		client.dfSpsPersEmail=document.getElementsByName("dfSelfPersEmail")[0].value;
		//client.dfSpsPersEmail=document.getElementsByName("dfSelfMobile")[0].value;
		client.dfSpsHome=document.getElementsByName("dfSelfHome")[0].value;
		client.dfSpsOffice=document.getElementsByName("dfSelfOffice")[0].value;
		return client;
	}
	
	function setContactData(clientData){
		
		for(var i=0;i<arrContactDet.length;i++){
			
			if(clientData[arrContactDet[i]]!=undefined){
				document.getElementsByName(arrContactDet[i])[0].value=clientData[arrContactDet[i]];
			}
		}
	}
	
	function setSpouseContactData(client){
		document.getElementsByName("dfSelfPersEmail")[0].value=client.dfSpsPersEmail;
		//client.dfSpsPersEmail=document.getElementsByName("dfSelfMobile")[0].value;
		document.getElementsByName("dfSelfHome")[0].value=client.dfSpsHome;
		document.getElementsByName("dfSelfOffice")[0].value=client.dfSpsOffice;
	
	}
	
	function setEmptyContactData(){
	    for(var i=0;i<arrContactDet.length;i++){
			document.getElementsByName(arrContactDet[i])[0].value="";
		}
	}
	
	//Set and get customer details
	function setCustContactData(custData){
		document.getElementsByName("dfSelfPersEmail")[0].value=custData.emailId;
	    document.getElementsByName("dfSelfMobile")[0].value=custData.othHandPhone;
	}
	
//End

//Set and get EmpFinDetails

var arrEmpAndFinDet=["dfSelfCompname","dfSelfOccpn","dfSelfAnnlIncome",
				"dfSelfBusinatr","dfSelfFundsrc","dfSelfFundsrcDets"];

	function getEmpAndFinData(client){
		for(var i=0; i<arrEmpAndFinDet.length; i++){
			client[arrEmpAndFinDet[i]]=document.getElementsByName(arrEmpAndFinDet[i])[0].value;
		}
		return client;
	}
	//Get spouse data
	function getSpouseEmpAndFinData(client){
		client.dfSpsCompname=document.getElementsByName("dfSelfCompname")[0].value;
		client.dfSpsOccpn=document.getElementsByName("dfSelfOccpn")[0].value;
		client.dfSpsAnnlIncome=document.getElementsByName("dfSelfAnnlIncome")[0].value;
		client.dfSpsBusinatr=document.getElementsByName("dfSelfBusinatr")[0].value;
		return client;
	}
	
	function setEmpAndFinData(clientData){
		
		for(var i=0; i<arrEmpAndFinDet.length; i++){
			if(clientData[arrEmpAndFinDet[i]]!=undefined){
			document.getElementsByName(arrEmpAndFinDet[i])[0].value=clientData[arrEmpAndFinDet[i]];
			}
		}
	}
	
	function setSpouseEmpAndFinData(client){
	    document.getElementsByName("dfSelfCompname")[0].value=client.dfSpsCompname;
		document.getElementsByName("dfSelfOccpn")[0].value=client.dfSpsOccpn;
		document.getElementsByName("dfSelfAnnlIncome")[0].value=client.dfSpsAnnlIncome;
		document.getElementsByName("dfSelfBusinatr")[0].value=client.dfSpsBusinatr;
	
	}
	function setEmptyEmpandFinData(){
		
		for(var i=0; i<arrEmpAndFinDet.length; i++){
			document.getElementsByName(arrEmpAndFinDet[i])[0].value="";
		 }
	}
	
	//Set and get customer details
	function setCustEmpAndFinData(custData){
	    document.getElementsByName("dfSelfCompname")[0].value=custData.companyName;
	    document.getElementsByName("dfSelfOccpn")[0].value=custData.occpnDesc;
	    document.getElementsByName("dfSelfAnnlIncome")[0].value=custData.income;
	    document.getElementsByName("dfSelfBusinatr")[0].value=custData.businessNatr;
    }

//End
//Set and get education details
var arrEduDet=["dfSelfEduLevel","dfSelfEngSpoken","dfSelfEngWritten"];

//DfSelfEngSpoken,DfSelfEngWritten
	var engSpoken,engWritten;
	$('#inlineCheckbox1').click(function() { 
	                if ($("#inlineCheckbox1").is(":checked") == true) { 
	                    engSpoken="Y"
	                } else { 
	                    engSpoken="N"; 
	                } 
	            }); 
	            
	$('#inlineCheckbox2').click(function() { 
	                if ($("#inlineCheckbox2").is(":checked") == true) { 
	                    engWritten="Y"
	                } else { 
	                    engWritten="N"; 
	                } 
	            }); 
	//End		
			
	function getEduData(client){
	     client.dfSelfEduLevel=$('input[name="dfSelfEduLevel"]:checked').val();
		 client.dfSelfEngSpoken=engSpoken;
		 client.dfSelfEngWritten=engWritten;
		 
		 return client;
	}
	
	//Get spouse data
	function getSpouseEduData(client){
		 client.dfSpsEduLevel=$('input[name="dfSelfEduLevel"]:checked').val();
		 client.dfSpsEduLevel=engSpoken;
		 client.dfSpsEduLevel=engWritten;
		 
		 return client;
	}
	
	function setEduData(clientData){
		
		$("input[name=dfSelfEduLevel][value=" + clientData.dfSelfEduLevel + "]").attr('checked', true);
		if(clientData.dfSelfEngSpoken == "Y"){
		    $("#inlineCheckbox1").prop('checked',true);
		}else{
		    $("#inlineCheckbox1").prop('checked',false);
		}
		if(clientData.dfSelfEngWritten == "Y"){
		    $("#inlineCheckbox2").prop('checked',true);
		}else{
		    $("#inlineCheckbox2").prop('checked',false);
		}
		
	}
	
	function setSpouseEduData(clientData){
		$("input[name=dfSelfEduLevel][value=" + clientData.dfSpsEduLevel + "]").attr('checked', true);
		if(clientData.dfSpsEngSpoken == "Y"){
		    $("#inlineCheckbox1").prop('checked',true);
		}else{
		    $("#inlineCheckbox1").prop('checked',false);
		}
		if(clientData.dfSpsEngWritten == "Y"){
		    $("#inlineCheckbox2").prop('checked',true);
		}else{
		    $("#inlineCheckbox2").prop('checked',false);
		}
	
	}
	
	function setEmptyEduData(){
		 document.getElementsByName("dfSelfEduLevel")[0].value="";
	     $("#inlineCheckbox1").prop('checked',false);
		 $("#inlineCheckbox2").prop('checked',false);
	 }
		
//End Edu details

function getClientData(client){
	client=getPersonalData(client);
	client=getAddressData(client);
	client=getContactData(client);
	client=getEmpAndFinData(client);
	client=getEduData(client);
	return client;
}	
function getSelfSpouseData(client){
      
      client=getSpousePersonalData(client);
      client=getSpouseAddressData(client);
      client=getSpouseContactData(client);
      client=getSpouseEmpAndFinData(client);
      client=getSpouseEduData(client);
     
      return client;
      
}
function setSelfSpouseData(client){
   
	
	setSpousePersonalData(client);
	setSpouseAddressData(client);
	setSpouseContactData(client);
	setSpouseEmpAndFinData(client);
	setSpouseEduData(client);

}
var client={};
function  updateSelfAndSpouseData(hdnSessDataFormId){
    
    var dataFormId;
	$(".tab-pane").each(function(index, value) {
			 var dataNid = [];
			if(index == 0){ 
				 /*$(this).find('input').each(function(i, item) {
				  
				   client[$(this).attr('name')]=item.value;
				 })
				 
			    console.log(JSON.stringify(client)+'---'+client.dataFormId);*/
			   // dataFormId=client.dataFormId;
			   dataFormId=document.getElementsByName("dataFormId")[0].value;
			   console.log(dataFormId);
			   client.dataFormId=hdnSessDataFormId;
				if(hdnSessDataFormId!=dataFormId){
				    client=getSelfSpouseData(client);
				    updateAjaxCall(client);
				    
				}
				else{
				    client=getClientData(client);
				    updateAjaxCall(client);
				}
		    }
		   
	  	});
	  	
	  	return client;

}


function updateAjaxCall(client){
  //alert(JSON.stringify(client));
		$.ajax({
            url:"/PSGISUBMIT/FnaSelfSpouse/updateData",
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(client),
            success: function (data) {
            	//alert(data);
            },
	 		error: function(data){
	 			//alert(data);
	 			}
            });

}


function updateClientDetails(){
     var hdnSessDataFormId=$("#hdnSessDataFormId").val();
     client=updateSelfAndSpouseData(hdnSessDataFormId);
     //alert(JSON.stringify(client));
	
}
//Get client details from client info page
 
function getClientDetails(dataFormId,spsactive){
	
	
	pageLoaderShow();
	
  var clientData={};
	$.ajax({
            url:"/PSGISUBMIT/FnaSelfSpouse/getDataById/"+dataFormId,
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
               setClientData(data);
	            if((spsactive =="Y") && (data.dfSpsName!=null)){
	               addNewInsured("SPS",data.dfSpsName);
	                setSelfSpouseData(data);
	            }else if((spsactive == "YN") && (data.dfSpsName!=null)){
	            		setSelfSpouseData(data);
	            }
	            pageLoaderHide();
            },
	 		error: function(xhr,textStatus, errorThrown){
	 			 //alert("Error");
	 			}
            });
            
            return clientData;
}
function pageLoaderShow(){
    $(".page-loader").show();
	$(".overlay").show();
}
function pageLoaderHide(){
   document.getElementById("pageLoader").style.display="none";
   document.getElementById("overlayLoader").style.display="none";
  /* $(".page-loader").hide();
   $(".overlay").hide();*/
}
//Set client data to corresponding fields
function setClientData(clientData){
	client=clientData;
	
	setPersonalData(clientData);
	setAddressData(clientData);
	setContactData(clientData);
	setEmpAndFinData(clientData);
	setEduData(clientData);
}



function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}




function getNationalityShow(){
  document.getElementById('selectNationOther').style.display = 'block';
}

function getNationalityHide(){
   document.getElementById('selectNationOther').style.display = 'none';
}

//Policy E-Submission

function submitPolicy(){
   var principalData={};
	var principalNames = [];
	$.each($("input[name='principalName']:checked"), function(){            
	    principalNames.push($(this).val());
	});
	principalData.PrincipalNames=principalNames;
	principalData.fnaId=client.fnaDetails.fnaId;
	principalData.custId=client.fnaDetails.custDetails.custId;
	principalData.isubmitData=client;
	principalData.distributorId=client.fnaDetails.custDetails.distributorId;
	alert(JSON.stringify(principalData));
	console.log(JSON.stringify(principalData));
	
	$.ajax({
	    url: "http://192.168.1.6:8085/PSGPortal/Rest/PrincipalApi/getData",
        type: "POST",
        dataType: "json",
        contentType: "application/json",
        data: JSON.stringify(principalData),
        success: function (data) {

           alert("Successfully sent principal data...");
          
        },
        error: function (e) {
	alert("Error message:"+e);

        }
    });
	
}
//Newly added
function openOthInptrSec(obj,val){
	if(val=="BENOWNER"){
		if($(obj).is(":checked")){
			$(obj).attr("data-toggle","modal");
			$(obj).attr("data-target","#txtAreaModal1");
			//$(obj).attr("data-backdrop","static");
			
		}else{
			$(obj).removeAttr("data-toggle","modal");
			$(obj).removeAttr("data-target","#txtAreaModal1");
			//$(obj).removeAttr("data-backdrop","static");
		}	
	}
	
	if(val=="TPP"){
		if($(obj).is(":checked")){
			$(obj).attr("data-toggle","modal");
			$(obj).attr("data-target","#txtAreaModal2");
			//$(obj).attr("data-backdrop","static");
			
		}else{
			$(obj).removeAttr("data-toggle","modal");
			$(obj).removeAttr("data-target","#txtAreaModal2");
			//$(obj).removeAttr("data-backdrop","static");
		}	    
	}
	
	if(val=="PEP"){
		if($(obj).is(":checked")){
			$(obj).attr("data-toggle","modal");
			$(obj).attr("data-target","#txtAreaModal3");
			//$(obj).attr("data-backdrop","static");
			
		}else{
			$(obj).removeAttr("data-toggle","modal");
			$(obj).removeAttr("data-target","#txtAreaModal3");
			//$(obj).removeAttr("data-backdrop","static");
		}	
	}
}

function chkJsnOptions(chkbox,jsnFld){
    var advAppTypeOpt = $("#"+jsnFld+"");
	var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	var chkd = chkbox.checked;
	var val = $(chkbox).attr("data");

	var newobj=jsonvalues;
	newobj[val]=(chkd == true ? "Y" :"N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}



  