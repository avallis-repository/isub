findNric();
//DOM elements
const DOMstrings = {
  stepsBtnClass: 'multisteps-form__progress-btn',
  stepsBtns: document.querySelectorAll(`.multisteps-form__progress-btn`),
  stepsBar: document.querySelector('.multisteps-form__progress'),
  stepsForm: document.querySelector('.multisteps-form__form'),
  stepsFormTextareas: document.querySelectorAll('.multisteps-form__textarea'),
  stepFormPanelClass: 'multisteps-form__panel',
  stepFormPanels: document.querySelectorAll('.multisteps-form__panel'),
  stepPrevBtnClass: 'js-btn-prev',
  stepNextBtnClass: 'js-btn-next' };


//remove class from a set of items
const removeClasses = (elemSet, className) => {

  elemSet.forEach(elem => {

    elem.classList.remove(className);

  });

};

//return exect parent node of the element
const findParent = (elem, parentClass) => {

  let currentNode = elem;

  while (!currentNode.classList.contains(parentClass)) {
    currentNode = currentNode.parentNode;
  }

  return currentNode;

};

//get active button step number
const getActiveStep = elem => {
  return Array.from(DOMstrings.stepsBtns).indexOf(elem);
};

//set all steps before clicked (and clicked too) to active
const setActiveStep = activeStepNum => {

  //remove active state from all the state
  removeClasses(DOMstrings.stepsBtns, 'js-active');

  //set picked items to active
  DOMstrings.stepsBtns.forEach((elem, index) => {

    if (index <= activeStepNum) {
      elem.classList.add('js-active');
    }

  });
};

//get active panel
const getActivePanel = () => {

  let activePanel;

  DOMstrings.stepFormPanels.forEach(elem => {

    if (elem.classList.contains('js-active')) {

      activePanel = elem;

    }

  });

  return activePanel;

};

//open active panel (and close unactive panels)
const setActivePanel = activePanelNum => {

  //remove active class from all the panels
  removeClasses(DOMstrings.stepFormPanels, 'js-active');

  //show active panel
  DOMstrings.stepFormPanels.forEach((elem, index) => {
    if (index === activePanelNum) {

      elem.classList.add('js-active');

      setFormHeight(elem);

    }
  });

};

//set form height equal to current panel height
const formHeight = activePanel => {

  const activePanelHeight = activePanel.offsetHeight;

  DOMstrings.stepsForm.style.height = `${activePanelHeight}px`;

};

const setFormHeight = () => {
  const activePanel = getActivePanel();

  formHeight(activePanel);
};

//STEPS BAR CLICK FUNCTION
DOMstrings.stepsBar.addEventListener('click', e => {

  //check if click target is a step button
  const eventTarget = e.target;

  if (!eventTarget.classList.contains(`${DOMstrings.stepsBtnClass}`)) {
    return;
  }

  //get active button step number
  const activeStep = getActiveStep(eventTarget);

  //set all steps before clicked (and clicked too) to active
  setActiveStep(activeStep);

  //open active panel
  setActivePanel(activeStep);
});

//PREV/NEXT BTNS CLICK

DOMstrings.stepsForm.addEventListener('click', e => {
	
  const eventTarget = e.target;

  //check if we clicked on `PREV` or NEXT` buttons
  if (!(eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`) || eventTarget.classList.contains(`${DOMstrings.stepNextBtnClass}`)))
  {
    return;
  }

  //find active panel
  const activePanel = findParent(eventTarget, `${DOMstrings.stepFormPanelClass}`);

  let activePanelNum = Array.from(DOMstrings.stepFormPanels).indexOf(activePanel);
  
  if(activePanelNum == 0){
	var nric=$("#nric").val();
	  if($("#client1name").val() == "" || $("#client1Initial").val() == ""){
		 if($("#client1Initial").val() == ""){
			 $("#client1name").removeClass("errorclz");
			$("#client1Initial").addClass("errorclz");
		  }
          if($("#client1name").val() == ""){
	         $("#client1Initial").removeClass("errorclz");
		  	$("#client1name").addClass("errorclz");
		  }
		  $("#nameerror").show();
			return false;
	  }
	  else if($("#simpleDataInput").val() ==""){
	      $("#simpleDataInput").addClass("errorclz");
	       $("#doberror").show();
			return false;
	  }else if(nricMap.has(nric)){
		 
	     var id=$("#custId").val();
         if(id == ""){
			  $("#nric").addClass("errorclz");
		      $("#nricerror").show();
	          return false;
         }
	  }
     
		
	 else{
		  $("#client1Initial").removeClass("errorclz");
		  $("#client1name").removeClass("errorclz");
		  $("#nameerror").hide();
		  
		  $("#simpleDataInput").removeClass("errorclz");
		  $("#doberror").hide();

          $("#nric").removeClass("errorclz");
	       $("#nricerror").hide();
	  }
  }
  else if(activePanelNum == 1){
     var resAddr1=$("#resAddr1").val();
     var resAddr2=$("#resAddr2").val();
     var resAddr3=$("#resAddr3").val();
     var resCountry=document.getElementsByName("resCountry")[0].value;
     var resPostalCode=$("#resPostalCode").val();
       if(resAddr1 == ""){
           showHideErrorMsg("Y","N","N","N","N");
           return false;
       }
       else if(resAddr2 == ""){
           showHideErrorMsg("N","Y","N","N","N");
           return false;
       }
       else if(resAddr3 == ""){
           showHideErrorMsg("N","N","Y","N","N");
           return false;
       }
       else if(resCountry == ""){
           showHideErrorMsg("N","N","N","Y","N");
           return false;
       }
       else if(resPostalCode == ""){
           showHideErrorMsg("N","N","N","N","Y");
           return false;
       }
       else{
           showHideErrorMsg("N","N","N","N","N");
       }
  }
     
		
	
  

  //set active step and active panel onclick
  if (eventTarget.classList.contains(`${DOMstrings.stepPrevBtnClass}`)) {
    activePanelNum--;

  } else {

    activePanelNum++;

  }

  setActiveStep(activePanelNum);
  setActivePanel(activePanelNum);

});

//SETTING PROPER FORM HEIGHT ONLOAD
window.addEventListener('load', setFormHeight, false);

//SETTING PROPER FORM HEIGHT ONRESIZE
window.addEventListener('resize', setFormHeight, false);

//changing animation via animation select !!!YOU DON'T NEED THIS CODE (if you want to change animation type, just change form panels data-attr)

const setAnimationType = newType => {
  DOMstrings.stepFormPanels.forEach(elem => {
    elem.dataset.animation = newType;
  });
};

//selector onchange - changing animation
const animationSelect = document.querySelector('.pick-animation__select');
if(animationSelect){
	animationSelect.addEventListener('change', () => {
  const newAnimationType = animationSelect.value;

  setAnimationType(newAnimationType);
});
}

function addressValidation(){
	var resAddr1=$("#resAddr1").val();
     var resAddr2=$("#resAddr2").val();
     var resAddr3=$("#resAddr3").val();
     var resCountry=document.getElementsByName("resCountry")[0].value;
     var resPostalCode=$("#resPostalCode").val();
       if(resAddr1 == ""){
           showHideErrorMsg("Y","N","N","N","N");
           return false;
       }
       else if(resAddr2 == ""){
           showHideErrorMsg("N","Y","N","N","N");
           return false;
       }
       else if(resAddr3 == ""){
           showHideErrorMsg("N","N","Y","N","N");
           return false;
       }
       else if(resCountry == ""){
           showHideErrorMsg("N","N","N","Y","N");
           return false;
       }
       else if(resPostalCode == ""){
           showHideErrorMsg("N","N","N","N","Y");
           return false;
       }
       else{
           showHideErrorMsg("N","N","N","N","N");
       }
}
function showHideErrorMsg(addr1,addr2,addr3,country,postalcode){
 
      if(addr1=="Y"){
        $("#resAddr1").addClass("errorclz");
          $("#addr1error").show();
      }else{
         $("#addr1error").hide();
         $("#resAddr1").removeClass("errorclz");
      }
      
      if(addr2=="Y"){
          $("#resAddr2").addClass("errorclz");
          $("#addr2error").show();
      }else{
         $("#addr2error").hide();
         $("#resAddr2").removeClass("errorclz");
      }
      
      if(addr3=="Y"){
          $("#resAddr3").addClass("errorclz");
          $("#addr3error").show();
      }else{
         $("#addr3error").hide();
         $("#resAddr3").removeClass("errorclz");
      }
      
      if(country=="Y"){
          $("#resCountry").addClass("errorclz");
          $("#countryerror").show();
      }else{
         $("#countryerror").hide();
         $("#resCountry").removeClass("errorclz");
      }
      
      if(postalcode=="Y"){
          $("#resPostalCode").addClass("errorclz");
          $("#postalerror").show();
      }else{
         $("#postalerror").hide();
          $("#resPostalCode").removeClass("errorclz");
      }
}

function isNumberAndMaxLen(event,errMsgId){
    
    event = (event) ? event : window.event;
    var charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    //$("#depName").addClass("errorclz");
         // $("#numErrMsg").show();
        $("#"+errMsgId).show();
        return false;
    }
    $("#"+errMsgId).hide();
    return true;
	
}

function findNric(){
	 $.ajax({
            url:"CustomerDetails/getAllData",
            type: "GET",
            dataType: "json",
            contentType: "application/json",
            success: function (data) {
	          setCustomerMapData(data);
            },
	 		error: function(data){
	 			  alert("Error---");
	 			}
            });

}
var nricMap=new Map();
function setCustomerMapData(data){
	for(var i=0;i<data.length;i++){
		var custDets=data[i];
		 nricMap.set(custDets.nric,custDets.custId);
	}
}


