var STR_PAYMENTMODE = "CASH^CHEQUE^CPF^CPFOA^CPFSA^CREDIT-CARD^GIRO - POSB^SRS^TT^GIRO - OTHERS^CASH CARD^CASH/CHQ";

var STR_PRODTYPE = "Insurance=Life^H&S=A&H^PA=PA^ILP=ILP";
var STR_PRDTTYPE_FUND = "UT";//^ILP
var STR_PAYMODE_FUND = "Cash^SRS^CPFOA^CPFSA";
var STR_ILP_RISK_RATE = "1=Conservative^2=Moderately Conservative^3=Moderately Aggressive^4=Aggressive";
var STR_FUND_RISK_RATE = "1=";

var STR_POL_TYPE = "LIFE^GENERAL";
var STR_SWTYPE = "Switch Out^Switch in";
var STR_FNA_ARCHSTS_LIST = "OPEN^FINAL^REJECT";
//var STR_DSF_RETSTS_LIST = "In-Progress^Completed^Submitted^Approved^Rejected^Cancelled";
var STR_DSF_RETSTS_LIST = "In-Progress=In-Progress^Submitted=Finalised^Cancelled=Cancelled";

var STR_FNA_MGRAPPRSTS_LIST = "APPROVE^REJECT";
var STR_FNAFLOWTYPE_LIST = "INFLOW^OUTFLOW";

var fnaForm;
var getBrowserApp = navigator.appName; // get the Browser type
var baseUrl;

var FNA_RPT_FILELOC = "", FNA_PDFGENLOC = "";
var strFNADets = "";
var strExistDataCust = "";
var strUnSavedCustData = "";
var strLatestCustData = "";

var LOG_USER_STFTYPE = "", FNA_CUSTNAME = "", FNA_CUSTNRIC = "",FNA_CUSTID="";
var emailDets = "";

var LOG_USER_MGRACS = "";
var custLobDets = "";

var COMMON_SESS_VALS, EMAILID_FOR_MAILSCR = "", LOG_USER_DESIG = "";
var BIRT_URL = "", FNA_CUSTSERVADVID = "", LOG_USER_ADVID = "";
var YUI_URL = "";
var SERV_PRIN_ADV_ID="";

var FNA_FROM_EXSTNEW = "", FNA_FORMTYPE = "";
var LOGGED_DISTID;
var FNA_CUSTCATEG;
var currDate = "";

var loadExistPolFlg = false;
var loadExistHsaPolFlg = false;
var glblArchDefltVal = "";

var relArray = [], custLobArr = [];
var prdtArray = [];
var fundPrdtArr = [];

var ilpRiskRateArr=[];
var paymentModeArr = [];
var fundPayMethArr = [];
var archStsArr = [];

var flowTypeArr = [];
var hospTypeArr = [];
var planNameArr = [];

var swtypeArr = [];
var contryArr = [];
var prdtplanArr = [];

var fundPlanArr = [];
var poltypeArr = [];
var prdtNamesArr = new Array();
var fundRateArr = new Array();
var prodtypeArr = new Array();

var prinIdArr = [];
var fmArr = [];
var consEmailIdArr = new Array();
var fnapoldetArr = new Array();

var archiveflg = false;
var page3Flag = false;
var page4Flag = false;

var page5Flag = false;
var page6Flag = false;
var page8Flag = false;

var page10Flag = false;
var page11Flag = false;
var page12Flag = false;
var page16Flag = false;

var page17Flag = false;
var page26Flag = false;//added by kavi 27/02/2018
var page27Flag = false;
var pageDepFlag = false;
var pageWithdTerFlag = false;

var page19Flag = false;
var emailpopupFlag = false;

var paraAdvEmailObj;
var paraAdvEmailIds = "";

var advRecommOthrs = '', clntMailAddrOthrsSpcy = '';// added by johnson on
// 22052015

var wind;

var pageDetsArr = [ {ALL_VER        : 27, "TOTAL" : 29},
                    {SIMPLIFIED_VER : 14, "TOTAL" : 29}
				  ];

var custattachId = "";
var zipcustId = "";
var strGlblUsrPrvlg = "";
// added by johnson on 02092015
var selfSpsFlds = [
// self Fields
'dfSelfName', 'dfSelfGender', 'dfSelfDob', 'dfSelfNric','dfSelfNationality', 'dfSelfMartsts', 'dfSelfBirthcntry',
'dfSelfHome','dfSelfMobile', 'dfSelfOffice', 'dfSelfPersemail','dfSelfAnnlincome', 'dfSelfOccpn', 'dfSelfCompname',
'dfSelfHomeaddr', 'htxtFldClntHeight', 'htxtFldClntWeight',	'htxtFldClntRace','dfSelfNatyDets',
//		 spouse fields
'dfSpsName', 'dfSpsGender', 'dfSpsDob',	'dfSpsNric', 'selFnaSpsNatly', 'dfSpsMartsts','dfSpsBirthcntry',
'dfSpsHome', 'dfSpsHp', 'dfSpsOffice','dfSpsPersemail', 'dfSpsAnnlincome', 'dfSpsOccpn','dfSpsCompname', 'dfSpsNatyDets',
'dfSpsHomeaddr' ];
var clntUSTaxRefNum = '', spsUSTaxRefNum = '';
var custSelObj;
var strCountryAllPair;
var CountryAllArray = new Array();
var strClientStatus = "";

var cdBenfTppPepObj = {

	BENF : [ 'txtFldCDBenfName', 'txtFldCDBenfNric', 'txtFldCDBenfIncNo','txtFldCDBenfAddr', 'txtFldCDBenfJob', 'txtFldCDBenfJobCont','txtFldCDBenfRel','txtFldCDBenfId'],
	TPP : [ 'txtFldCDTppName', 'txtFldCDTppNric', 'txtFldCDTppIncNo','txtFldCDTppAddr', 'txtFldCDTppJob', 'txtFldCDTppJobCont',	'txtFldCDTppRel','txtFldCDTppId'],
	PEP : [ 'txtFldCDPepName', 'txtFldCDPepNric', 'txtFldCDPepIncNo','txtFldCDPepAddr', 'txtFldCDPepJob', 'txtFldCDPepJobCont',	'txtFldCDPepRel','txtFldCDPepId'],
	OTH : [ 'txtFldCDTppCcontact' ]//'txtFldCDTppChqNo',//-> MAR_2018,, 'txtFldCDTppBankName', ;dec2019
};

var clntRiskPrefArr = {

	ID : [ 'crInvstobj', 'crInvstamt', 'crInvsttimehorizon','crRiskpref', 'crRiskclass', 'crRoi','crAdeqfund', 'crOthconcern' ],

	MSG : [ 'Key in Investment Objective',
			'Key in Amount to Investment', 'Key in Time Horizon',
			'Select Risk Preference',
			'Select any one from the General Risk Classification',
			'Key in Expected ROI',
			'Select Adequate funds to handle option',
			'Key in Others Concerns' ]
};// end of clntRiskPrefArr

var STR_NOTIN_REASON = "Reason A^Reason B^Reason C";
var tax_reasonArr = [];

var changedarrGlobal = [],changedarrGlobal1 = [], changedarrGlobal2 = [], changedarrGlobal3 = [];
var changedarrGlobal4 = [],changedarrGlobal5 = [],changedarrGlobal6 = [];
var changedarrGlobal7 = [],changedarrGlobal8 = [],changedarrGlobal9 = [];
var changedarrGlobal10 = [],changedarrGlobal11 = [],changedarrGlobal12 = [];
var changedarrGlobal13 = [],changedarrGlobal14 = [],changedarrGlobal15 = [];

var spsAttachAddStatus=false;
var slfAttachAddStatus=false;
var clntAtchList;
var NTUCChkChngSts=true;
var ntuc_sftp_access_flag=false;
var strExistSps;
var spsClearArr=["dfSpsNric","dfSpsDob","dfSpsHp","dfSpsGender","dfSpsOffice","dfSpsPersemail","dfSpsMartsts"
                 ,"dfSpsHomeaddr","dfSpsOccpn","dfSpsHeight","dfSpsWeight"];

var strMastAttachCateg="";
var attachmentArray = new Array();
var mastAttachCategArr = new Array();
var TOTAL_ARCHIVE=0;

function callBodyInit() {

//	$(".kycPageNo").find(":input").prop("disabled",false);
	$("#hTxtDsfStatusChk").val("");

	fnaForm = document.forms[0];


	// jQuery Tabs config
	$("#tabs").tabs();
	$("#tabs1").tabs();
	
	var accessLvl = strGlblUsrPrvlg.split(",");
	for (var len = 0; len < accessLvl.length; len++) {
//		alert(accessLvl[len])
		if (accessLvl[len] == NTUC_SFTP_ACCESS) {
			ntuc_sftp_access_flag=true;
		}
	}

//	 PAGE SHOW / HIDE BASED ON LOB

	var totalpage = 29;

	var allTotalPage = totalpage;

//	 GET AND SET SESSION VALUES

	for ( var sess in COMMON_SESS_VALS) {
		var sessDets = COMMON_SESS_VALS[sess];
		for ( var dets in sessDets) {
			if (sessDets.hasOwnProperty(dets)) {
				var sesskey = dets;
				var sessvalue = sessDets[dets];

//				 alert(sesskey + ", "+sessvalue)
				console.log(sesskey + ", "+sessvalue)
				if (sesskey == "LOGGED_USER_STFTYPE")
					LOG_USER_STFTYPE = sessvalue;
				if (sesskey == "CUST_NAME")
					FNA_CUSTNAME = sessvalue;
				if (sesskey == "CUST_NRIC")
					FNA_CUSTNRIC = sessvalue;
				if(sesskey =="STR_CUSTID")
					FNA_CUSTID = sessvalue;
				if (sesskey == "RPT_FILE_LOC")
					FNA_RPT_FILELOC = sessvalue;
				if (sesskey == "PDF_GENERATED_LOC")
					FNA_PDFGENLOC = sessvalue;
				if (sesskey == "FNA_EMAILDETS")
					emailDets = sessvalue;
				if (sesskey == "MGR_ACS_FLG"){
					LOG_USER_MGRACS = sessvalue;					
				}
				if (sesskey == "CUST_LOB_DETS")
					custLobDets = sessvalue;
				if (sesskey == "CUST_SERVADVID")
					FNA_CUSTSERVADVID = sessvalue;
				if (sesskey == "LOGGED_ADVID"){
					LOG_USER_ADVID = sessvalue;					
				}
				if (sesskey == "LOGGED_USERDESIG")
					LOG_USER_DESIG = sessvalue;
				// if(sesskey == "STR_BIRTPATH")BIRT_URL = sessvalue;
				if (sesskey == "STR_FNA_FROM")
					FNA_FROM_EXSTNEW = sessvalue;
				if (sesskey == "TODAY_DATE")
					currDate = sessvalue;
				if (sesskey == "FNA_FORM_TYPE") {
					FNA_FORMTYPE = sessvalue;
					if (document.getElementById("txtFldKycForm"))
						document.getElementById("txtFldKycForm").value = FNA_FORMTYPE;
					if (document.getElementById("htxtFldDisFormType"))
						document.getElementById("htxtFldDisFormType").value = FNA_FORMTYPE;
				}
				if (sesskey == "CUST_CATEG") {
					FNA_CUSTCATEG = sessvalue;
				}
				if (sesskey == "LOGGED_DISTID") {
					LOGGED_DISTID = sessvalue;
				}
				if (sesskey == "STR_CLIENTSTATUS") {
					strClientStatus = sessvalue;
				}
				if (sesskey == "STR_SER_ADVSTF_PRINID")
					SERV_PRIN_ADV_ID = sessvalue;

				if (sesskey == "STR_LOGGEDUSER"){
					$("#hTxtFldFnaLoggedUserId").val(sessvalue);
				}
				
				if(sesskey == "KYC_APPROVAL_FNAID"){
					$("#hTxtFldKycApprovalFnaId").val(sessvalue);
				}

			}
		}
	}

	// GET AND SET SESSION VALUES
	
	var mastAttachCateg =  strMastAttachCateg.substring(0,strMastAttachCateg.length-1).split("^");
	var attachCategoryObj = document.getElementById("selAttachMastAttachCateg");
	var totalCount=1;
	var duplicate="";
	addSelectOption(attachCategoryObj,true);
	
	for(var uniq=0;uniq<mastAttachCateg.length;uniq++){

		var result=mastAttachCateg[uniq].split("=");

		var categoryId = result[0];
		var categoryName = result[1];
		var docTitle = result[2];
		var module = result[3];

		mastAttachCategArr[uniq]=new  Array(categoryId,categoryName,docTitle,module);

		if((duplicate.search(mastAttachCategArr[uniq][1]) == -1 ) &&
				(mastAttachCategArr[uniq][3] == "CLIENT" || mastAttachCategArr[uniq][3] == "GENERAL") )  {
			duplicate=duplicate+mastAttachCategArr[uniq][1];
			attachmentArray.push(mastAttachCategArr[uniq][1]);
		}

	}//End for uniq.
	
	console.log(mastAttachCategArr)
	
	
	for(var uniqCnt = 0;uniqCnt<attachmentArray.length;uniqCnt++){
		attachCategoryObj.options[totalCount]=new Option(attachmentArray[uniqCnt],attachmentArray[uniqCnt]);
		totalCount++;
	}
	

	fnaForm.txtFldKycLob.value = custLobDets;

	custLobArr = custLobDets.split("~");

	$("#clientlbl").css("visibility", "visible");
	$("#clientName").css("visibility", "visible");
	$("#ClientName_trans").val(FNA_CUSTNAME.toUpperCase());
	$("#hTxtFldFnaCustId").val(FNA_CUSTID);

	$("input[name='txtFldfnaDate']").val(currDate);
	$("#txtFldRptFileLoc").val(FNA_RPT_FILELOC);
	$("#txtFldPdfGenLoc").val(FNA_PDFGENLOC);

	// ------------------------- PARAPLANNER's ADVISER EMAIL DETS

	/*
	 * for(var para in paraAdvEmailObj){
	 *
	 * var paraAdvDets = paraAdvEmailObj[para]; paraAdvEmailIds +=
	 * paraAdvDets["paraAdvStfEmailId"]+","; }
	 */

	// -------------------------PARAPLANNER's ADVISER EMAIL DETS
	// var TO_BE_SHOWN_IMG=[];
	// var GENERAL_SHOWN_IMG = {};
	var INSURANCE_SHOWN_IMG = {

		"DEPENTDET" : [ "ADD", "DEL" ],
		"FATCADET" : [ "ADD", "DEL" ],
		"DEPENCONTDET" : [ "ADD", "DEL" ],
		"PROPOWNRSP" : [ "ADD", "DEL" ],
		"VEHIOWNRSP" : [ "ADD", "DEL" ],
		"HEALINFONEEDS" : [ "ADD", "DEL" ],
		"LIFEPLNLST" : [ "ADD", "DEL" ],
		"ADVRECM" : [ "ADD", "DEL" ],
		"ADVRECMLIFHEL" : [ "ADD", "DEL" ],
		"LIFHELINSPLN" : [ "ADD", "DEL" ]
	};

	var INV_SHOWN_IMG = {

		"DEPENTDET" : [ "ADD", "DEL" ],
		"FATCADET" : [ "ADD", "DEL" ],
		"DEPENCONTDET" : [ "ADD", "DEL" ],
		"PROPOWNRSP" : [ "ADD", "DEL" ],
		"VEHIOWNRSP" : [ "ADD", "DEL" ],

		"INVGOALDET" : [ "ADD", "DEL" ],
		"FLOWDET" : [ "ADD", "DEL" ],
		"INPTINVDET" : [ "ADD", "DEL" ],
		"UTILPFUNDET" : [ "ADD", "DEL" ],
		"SWREPUTILPFUN" : [ "ADD", "DEL" ]
	};

//	alert(custLobArr)

/*
	if (checkValueInArray(custLobArr, LIFE_INS)	&& FNA_FORMTYPE == SIMPLIFIED_VER) {
		totalpage = 11;
	}

	if (!checkValueInArray(custLobArr, LIFE_INS) && FNA_FORMTYPE == SIMPLIFIED_VER) {
		totalpage = 14;
	}

	if (checkValueInArray(custLobArr, LIFE_INS) && FNA_FORMTYPE == ALL_VER) {
		totalpage = 19;
	}

	if (!checkValueInArray(custLobArr, LIFE_INS) && FNA_FORMTYPE == ALL_VER) {
		totalpage = 22;
	}

	This totalpage never used - commentted on MAR_2018

*/

//alert("totalpage->"+totalpage)

	// jQuery Pagination config

	// LoadPageWithPaging(FNA_FORMTYPE);

	// -----------------------------

	/*
	 * $('.pagination').jqPagination({
	 *
	 * link_string : '/?page={page_number}', max_page : totalpage, paged :
	 * function(page) {
	 *
	 * if (FNA_FORMTYPE == ALL_VER) {
	 *
	 * if (!allPageValidation()) { return; }
	 *
	 * for (var p = 1; p <= totalpage; p++) {
	 *
	 * $("#page" + p).hide(); } $("#page23").hide();//EMpty page for simplified
	 * version
	 *
	 * $("#tabs").tabs("option", "active", 1);
	 *
	 * $("#page" + page).show(); } else if (FNA_FORMTYPE == SIMPLIFIED_VER) {
	 *
	 * switch (page) {
	 *
	 * case 1: { page = 1; break; } case 2: { page = 12; break; } case 3: { page =
	 * 13; break; } case 4: { page = 10; break; } // case 5:{page=14;break;}
	 * case 5: { page = 14; break; } case 6: { page = 15; break; } case 7: {
	 * page = 16; break; } case 8: { page = 17; break; } case 9: { page = 23;
	 * break; } case 10: { page = 18; break; } case 11: { page = 19; break; }
	 * case 12: { page = 20; break; } case 13: { page = 21; break; } case 14: {
	 * page = 22; break; } }
	 *
	 * for (var p = 1; p <= allTotalPage; p++) { $("#page" + p).hide(); }
	 * $("#page23").hide();//EMpty page for simplified version
	 *
	 * $("#tabs").tabs("option", "active", 1); $("#page" + page).show(); } } });
	 */

	// ---------
	// jQuery Buttons Config
	if (!$("input[type=button], button").attr('id') == 'scwClearButton') {
		$("input[type=button], button").button();
	}

//	added on 2017_06_02
	$("input[name='radCDClntMailngAddr_032018']," +
	  "input[name='txtFldDfSelfEngProf']," +
	  "input[name='txtFldDfSelfEngProf']," +
	  "input[name='txtFldDfSpsEngProf']," +
//	  "input[name='dfSelfEdulevel']," +//COMMENTTED ON MAR_2018
//	  "input[name='dfSpsEdulevel']," +//COMMENTTED ON MAR_2018
	  "input[name='txtFldDfSelfAgenext']," +
	  "input[name='txtFldDfSpsAgenext']").attr('disabled',true);

//	COMMENTTED ON MAR_2018
//	$("input[name='dfSelfEdulevel']," +
//	  "input[name='dfSpsEdulevel']").attr("class","readOnlyText");


	// EXISTING
	// if((checkValueInArray(custLobArr,INVEST) ||
	// checkValueInArray(custLobArr,LIFE_INS)) &&
	// !checkValueInArray(custLobArr,ILP)){
	// //totalpage=21;
	// }

	if (FNA_FORMTYPE == SIMPLIFIED_VER) {
		$("#imgPage14ClntDob").show();
		$("#imgPage14SpsDob").show();
	}

	// getTotPages(custLobArr, FNA_FORMTYPE);

	if (checkValueInArray(custLobArr, LIFE_INS)) {
		tblCrudImgVisible(KYC_ALL_PAGE_IMAGES, INSURANCE_SHOWN_IMG, ""); // 24042015
	}

	if (checkValueInArray(custLobArr, INVEST)) {
		tblCrudImgVisible(KYC_ALL_PAGE_IMAGES, INV_SHOWN_IMG, ""); // 24042015
	}
/*
//	 YET TO IMPLEMENT FULL FUNCTIONALITY -- THIS IS SAMPLE --?? MAR_2018
	if (!checkValueInArray(custLobArr, INVEST) && (checkValueInArray(custLobArr, LIFE_INS) || checkValueInArray(custLobArr, ILP))) {

		// formFldEnableDisable(CPF_PART_FLD,'disable');

	} else if (checkValueInArray(custLobArr, INVEST)) {

		// formFldEnableDisable(CPF_PART_FLD,'enable');
	}

//	Commentte on MAR_2018

	*/

	// jQuery Buttons in toolbar config
	// Toolbar icons are configured thru yui.
	/*
	 * $('#saveBtn').button({ label: '', icons: {primary: null, secondary: null}
	 * }).addClass("ui-icon-custom");
	 *
	 * $('#reportBtn').button({ label: '', icons: {primary: null, secondary:
	 * null} }).addClass("ui-icon-custom_report");
	 *
	 * $('#logoutBtn').button({ label: '', icons: {primary: null, secondary:
	 * null} }).addClass("ui-icon-custom_logout");
	 *
	 *
	 * $('#blankFormBtn').button({ label: '', icons: {primary: null, secondary:
	 * null} }).addClass("ui-icon-custom_printblank");
	 */

	comboMakerByString(STR_MARSTS, fnaForm.selFnaClntMartSts, "", true);
	comboMakerByString(STR_MARSTS, fnaForm.selFnaSpsMartSts, "", true);

	comboMakerByString(STR_MARSTS, fnaForm.dfSelfMartsts, "", true);
	comboMakerByString(STR_MARSTS, fnaForm.dfSpsMartsts, "", true);

//	 comboMakerByString(STR_FNA_ARCHSTS_LIST,document.getElementById("selArchSts"),"",true);
//	added spouse combo implementation by kavi 26_02_2018
//	Commentted on DEC20219
//	setSpouseCombo(strExistSps,'txtFldSpouseName',$("#dfSpsName"));
//	setSpouseCombo(strExistSps,'txtFldSpouseName',$("[name=txtFldSpouseName]"));
	
	flowTypeArr = makeOptionArr(STR_FNAFLOWTYPE_LIST, "^");
	prdtArray = makeOptionArr(STR_PRODTYPE, "^");
	paymentModeArr = makeOptionArr(STR_PAYMENTMODE, "^");
	fundPrdtArr = makeOptionArr(STR_PRDTTYPE_FUND, "^");
	fundPayMethArr = makeOptionArr(STR_PAYMODE_FUND, "^");
	poltypeArr = makeOptionArr(STR_POL_TYPE, "^");
	swtypeArr = makeOptionArr(STR_SWTYPE, "^");
	tax_reasonArr = makeOptionArr(STR_NOTIN_REASON, "^");
//	ilpRiskRateArr = makeOptionArr(STR_ILP_RISK_RATE,"^");

	hospTypeArr = copyOptionsArrFromSel(fnaForm.selFnaContHospSelf);
	relArray = copyOptionsArrFromSel(fnaForm.intprtRelat);
	contryArr = copyOptionsArrFromSel(fnaForm.selFatcaBirthPlace);
	prinIdArr = copyOptionsArrFromSel(fnaForm.selMastPrincipal);
	fmArr = copyOptionsArrFromSel(fnaForm.selMastFm);

	var loadExistData = true;
	var isArchiveExist = false;
	strLatestCustData = strExistDataCust;

	var tblId = document.getElementById("verdettable");
	var tbody = tblId.tBodies[0];
	var retval = strFNADets;

	for ( var val in retval) {

		var tabdets = retval[val];

		for ( var tab in tabdets) {

			if (tabdets.hasOwnProperty(tab)) {

				var key = tab;
				var value = tabdets[tab];

				if (key == "txtFldCustId")
					fnaForm.txtFldCustId.value = value;
				if (key == "txtFldCrtdUser")
					fnaForm.txtFldCrtdUser.value = value;

				// if(key == "txtFldKycForm")
				// fnaForm.txtFldKycForm.value = value;

				if (key == "ARCHIVE_COUNT") {
//					alert("ARCHIVE_COUNT ->"+value)
					TOTAL_ARCHIVE =value;

					removeTblRows("verdettable");

					if (value != "0" && FNA_FROM_EXSTNEW != FNA_FROM_NEWKYC) {

						loadExistData = false;

						$("#tabs-template-li").css("display", "none");

						// $("#accordion").hide();
						// $("#paginationdiv").hide();
						// $("#btngroup").hide();
						saveBtnObj.setStyle('display', 'none');
						reportBtnObj.setStyle('display', 'none');
						ntucBtnObj.setStyle('display', 'none');
						clientSignBtnObj.setStyle('display', 'none');
						$("#esignaturepopbox").hide();
						
						$("#tblArchDets").hide();

						isArchiveExist = true;

					} else {

						loadExistData = true;

						$("#tabs-arch-li").css("display", "none");
						$("#tabs-template-li").css("display", "inline");

						// $("#paginationdiv").show();
						// $("#btngroup").show();

						saveBtnObj.setStyle('display', '');
						reportBtnObj.setStyle('display','');
						
						if(!($("#verdettable tbody tr:first td:eq(3) select:first").val()=="Submitted"))
						ntucBtnObj.setStyle('display', '');
//						clientSignBtnObj.setStyle('display', '');
						$("#esignaturepopbox").show();
						
						$("#tblArchDets").show();

						$("#tabs").tabs("option", "active", 1);
						// $('.pagination').jqPagination('option','current_page',1);

						LoadPageWithPaging(FNA_FORMTYPE);

						$("#verdettable").hide();

						$("#selArchSts").val(STR_ARCHSTS_OPEN);// .prop("disabled","true");
						$("#selArchNo").val("NEW");

						if (FNA_CUSTCATEG == CUST_CATEG_COMPANY) {
							blockSpsForCompany();
						}
					}

				}

				if (key == "VERSION_DETAILS") {

					var archcnt = value.length;
					var tmpcnt = value.length;
					// .tBodies[0];

					var latestArch = false;

					for (var cnt = 1; cnt <= value.length; cnt++) {

						if (cnt == 1)
							latestArch = true;
						else
							latestArch = false;

						var crow = tbody.insertRow(cnt - 1);

						var cell0 = crow.insertCell(0);
						cell0.innerHTML = '<input type="hidden" name="verArchFnaId" />'+
						'<input type="radio" value='+ cnt+ ' id="ver'+ cnt	+ '" name="version" onclick="populateFnaDets(this)"/>&nbsp;&nbsp;&nbsp;<label class="label" onclick="hideTooltip()" onmouseover="showTooltip(event,this.innerHTML);" onmouseout="hideTooltip();" for="ver'+ cnt	+ '">Archive-'+ (archcnt--)+ ' </label>'
						+'<img src="images/quesmark.png" width="18" height="18" onmouseover="showKycToolTip(event,this)"; onmouseout="hideTooltip();"/>'
						+'<img src="images/PDF.png" width="18" height="18" style="cursor:pointer;display:none" title="Export to PDF" onclick="generatePDF(this)"/>'
						+'<img src="images/e-submission.png" width="25" height="25" style="cursor:pointer;" title="E-Submission to NTUC Income"/>'
						+'<img src="images/policy-pdf.png" width="25" height="25" style="cursor:pointer;" title="Download Policy Document and Details"/>'
						+'<img src="images/view-pdf.png" width="25" height="25" style="cursor:pointer;" title="View/Add NRIC Attachment" onclick="fetchExistNtucDoc(this)"/>'
						;
						cell0.style.textAlign = "left";

						cell0.childNodes[6].onclick=function(){


							var agentcode = $(this).closest('tr').find("td:eq(2) input:eq(7)").val();
							var fnaid = $(this).closest('tr').find('td:eq(0)').find('input:first').val();// cell0.childNodes[0].value;
							var advstfcode = $(this).closest('tr').find("td:eq(2) input:eq(7)").val();
							var lockimg = $(this).closest('tr').find("td:eq(3) img:eq(1)");
							var jsonstring ;
							var spsNricChkSts=false;
							var blnAtlstOneNtuc=false;
							var blnSignatureSts=false;
							
							$("#hTxtFldFnaDsfSelfName").val("");
							$("#hTxtFldFnaDsfSpsName").val("");

							var parameter = "dbcall=VER&FNA_ID=" + fnaid;
							var respText = ajaxCall(parameter);

//							console.log(JSON.stringify(respText));
							$.each(respText,function(i,refarr){
								if(i==2){
									var fnsDetl=refarr['ARCH_FNASSDET_TABLE_DATA'];
									$("#hTxtFldFnaDsfSelfName").val(fnsDetl.dfSelfName);
									$("#hTxtFldFnaDsfSpsName").val(fnsDetl.dfSpsName);

									if(!isEmpty(fnsDetl.dfSpsName)){
										spsNricChkSts=true;
									}

								}

								if(i==22){
									var fnsPlnDets=refarr['ARCH_ADVRECPRDTPLAN_DATA'];

									if(typeof fnsPlnDets !== "undefined" && fnsPlnDets.length>0){
									$.each(fnsPlnDets,function(i,obj){
										if(obj.selFnaAdvRecCompName=='NTUC Income'){
											blnAtlstOneNtuc=true;
											return false;
										}

									});
									}

								}
								
								if(i == 35){
									var totalclientsign=4,clientsinged=0;
									var fnaSignature = refarr["ALL_SIGNATURE_DETS"];
									if(typeof fnaSignature !== "undefined" && fnaSignature.length>0){
										
										$.each(fnaSignature,function(k,v){
											if(v.hasOwnProperty("CLIENT_SIGNED") && v.CLIENT_SIGNED =='Y'){
												clientsinged++;
											}
										});
										
										if(totalclientsign == clientsinged){
											blnSignatureSts=true;
//											return false;
										}
									}
								}

							});



							if(!isEmpty(advstfcode)){

								var validtnArr = [];

//								validtnArr = validateClntsDets(this,spsNricChkSts);

//								console.log("temporarily returned here");

//								ModalPopups.MandatoryPopupFPMS("ClntDetsMandLayer", {width:920,onOk:"",onCancel:"ModalPopupsPromptMandCancel()"});

								//for testing
								/*$("#ClntDetsMandLayer_okButton").click(function(){


									assingClntMandVal("+isCustNricAttached+",this);

//									callDSF(fnaid,agentcode,lockimg);



								});*/
								if( validateClntsDets(this,spsNricChkSts)){
									ModalPopups.MandatoryPopupFPMS("ClntDetsMandLayer", {width:960,height:520,onOk:"",onCancel:"ModalPopupsPromptMandCancel()"});



								}else {


									var mandField=validateKycMandField(fnaid);


									if(mandField.length>0){
										alert(mandField);
										$(this).closest("td").find("[name=version]").click();
										setPageFocus(12, FNA_FORMTYPE);
										return false;
									}

									//For At least NTUC Product
									if(!blnAtlstOneNtuc){
										alert(NTUC_PRODUCT_ALERT);
										$(this).closest("td").find("[name=version]").click();
										setPageFocus(14, FNA_FORMTYPE);
										
										addNTUCRow();
										
										return false;
									}
									
//									Signature validation
									if(!blnSignatureSts){
										alert(CLIENT_SIGN_B4_DSF);
										$(this).closest("td").find("[name=version]").click();
										setPageFocus(1, FNA_FORMTYPE);
										setTimeout(function(){openPopupBox();},700);
										return false;
									}

									callDSF(fnaid,agentcode,lockimg);
								}

								$("#ClntDetsMandLayer_okButton").click(function(){

									if(!chkClntMandtryDoc())return;
									
									var loadMsg = document.getElementById("loadingMessage");
									var loadingLayer = document.getElementById("loadingLayer");

									loadMsg.style.display = "block";
									// alert(loadMsg.style.display)
									loadingLayer.style.display = "block";

									if(!assingClntMandVal("+isCustNricAttached+",this)){
//										alert("Attachment upload failed");
										
										loadMsg.style.display = "none";
										return;
									}

									var mandField=validateKycMandField(fnaid);


									if(mandField.length>0){
										alert(mandField);
//										$(this).closest("td").find("[name=version]").click();
										$(lockimg).closest("tr").find("td:eq(0)").find("[name=version]").click();
										setPageFocus(12, FNA_FORMTYPE);
										return false;
									}

									//For At least NTUC Product
									if(!blnAtlstOneNtuc){
										alert(NTUC_PRODUCT_ALERT);
//										$(this).closest("td").find("[name=version]").click();
										$(lockimg).closest("tr").find("td:eq(0)").find("[name=version]").click();
										setPageFocus(14, FNA_FORMTYPE);
//										fnaArtuPlanAddRow('fnaartuplanTbl','N',document.getElementById("imgFnaArtuPlanAddRow"));
										addNTUCRow();  //added by kavi 25/02/2019 need conformation	
										return false;
									}
									
									
//									Signature validation
									if(!blnSignatureSts){
										alert(CLIENT_SIGN_B4_DSF);
										$(this).closest("td").find("[name=version]").click();
										setPageFocus(1, FNA_FORMTYPE);
										setTimeout(function(){openPopupBox();},700);
										return false;
									}

									callDSF(fnaid,agentcode,lockimg);

								});






							}else{

								alert("Adviser is not registered with NTUC Income. Please contact compliance team.");

							}




						};

						cell0.childNodes[7].onclick=function(){

							var agentcode = $(this).closest('tr').find("td:eq(2) input:eq(7)").val();
							var fnaid = $(this).closest('tr').find('td:eq(0)').find('input:first').val();// cell0.childNodes[0].value;
							var advstfcode = $(this).closest('tr').find("td:eq(2) input:eq(7)").val();
							var lockimg = $(this).closest('tr').find("td:eq(3) img:eq(1)");
							var caseid =  $(this).closest('tr').find('td:eq(3) input:eq(3)').val();

							createPolicywithAttach(fnaid,agentcode,lockimg,caseid);

						}

						var cell1 = crow.insertCell(1);
						cell1.innerHTML = '<input type="text" name="verCretdBy" readOnly="true" class="fpNonEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type="hidden" value="Archive-'
								+ (tmpcnt--)
								+ '"/>'
								+ '<input type="hidden" name="htxtFldNTUCPolicyId" />';

						var cell2 = crow.insertCell(2);
						cell2.innerHTML = '<input type="text" name="verCretdDate"  readOnly="true" class="fpNonEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>'
								+ '<input type="hidden" name="htxtFldNTUCPolNum"/>'
								+ '<input type="hidden" name="htxtFldNTUCPolStatus"/>'
								+ '<input type="hidden" name="htxtFldNTUCPolDesc"/>'
								+ '<input type="hidden" name="htxtFldNFnaCustId" />'
								+ '<input type="hidden" name="htxtFldNMgrMailSentFlg" />'
								+ '<input type="hidden" name="htxtFldNtucCaseNo" value="'+value[cnt - 1].txtFldNtucCaseLock+'" />'
								+ '<input type="hidden" name="htxtFldNTUCSerAdvPrinId" value="'+SERV_PRIN_ADV_ID+'"/>'
								+ '<input type="hidden" name="htxtFldNtucCaseLock" value="'+value[cnt - 1].txtFldNtucLockedCase+'" />';

						if(value[cnt - 1].txtFldNtucCaseLock == "LOCKED"){
							 $(cell2).closest('tr').find("td:eq(0) img:eq(3)").show();
						}

						var cell3 = crow.insertCell(3);
						// 2015_12_12 - TIME BEING - REOPENED TO FUCTIONING -
						// 17_01_2016
						cell3.innerHTML = '<img src="images/SendMail.png" style="cursor:pointer;display:none" width="20px" height="20px" id="showmail" title="Send Mail"  onclick="showemailpopup(this)"/>'
								// cell3.innerHTML = '<img
								// src="images/SendMail.png"
								// style="cursor:pointer;" width="20px"
								// height="20px" id="showmail" title="Send Mail"
								// onclick="alert(\'EMAIL FUNCTION NOT
								// AVAILABLE.\')"/>'
								+ '<input type="hidden" name="servAdvEmailId"/><input type="hidden" name="AdvMgrEmail"/><input type="hidden" name="servAdvName"/>'
								+'<select name="selDSFReturnSts" id="selDSFReturnSts" disabled="true" class="kycSelect" style="vertical-align:middle;width:90px" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"></select>'+
								'<input type="hidden" name="hTxtCaseId"/><input type="hidden" name="hTxtCaseAppId"/><input type="hidden" name="hTxtCasePolNo"/><input type="hidden" name="hTxtCaseStatus"/>'
								+'<img src="images/lockcase.png" title="Lock the Case" width="25" height="25" style="cursor:pointer;display:none" onclick="lockCase(this)"/>'
								+'<img src="images/close.png" title="Cancel the Case" width="20" height="20" style="cursor:pointer;display:none;vertical-align:middle" onclick="cancelCase(this)"/>';
						cell3.style.textAlign = "left";
						comboMakerByKeyVal(STR_DSF_RETSTS_LIST,cell3.childNodes[4], "", true);

						cell3.childNodes[4].onchange = function(){

							setnCallUpdateDSFParams(this,null);

//							var agentcode = $(this).closest('tr').find("td:eq(2) input:eq(7)").val();
//							var fnaid = $(this).closest('tr').find('td:eq(0)').find('input:first').val();// cell0.childNodes[0].value;
//							alert(agentcode + ","+fnaid+",")
//							var pdfparam = "";
//							var casestatus= $(this).closest('tr').find('td:eq(3) select:eq(0)').val();
//							var caseid =  $(this).closest('tr').find('td:eq(3) input:eq(3)').val();
//
//							alert(caseid)
//							alert(casestatus)
//
//							updateStatusToDSF(fnaid, agentcode, caseid, pdfparam, casestatus);

						};


						var cell4 = crow.insertCell(4);
						cell4.innerHTML = '<select name="selverKycArchStatus" id="selverKycArchStatus" class="kycSelect" onclick="archDefaultVal(this);hideTooltip()"  onchange="if(!chkMgrApprSts(this))return;if(!chkFinalSts(this))return;" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"></select>';
						comboMakerByString(STR_FNA_ARCHSTS_LIST,cell4.childNodes[0], "", true);

						var cell5 = crow.insertCell(5);
						cell5.innerHTML = '<select  name="selverKycSentStatus" class="kycSelect" disabled="true" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"></select>';
						comboMakerByString(STR_FNA_YESNO_LIST,	cell5.childNodes[0],value[cnt - 1]["selKycSentStatus"], true);
						cell5.childNodes[0].value = !isEmpty(value[cnt - 1]["selKycSentStatus"]) ? value[cnt - 1]["selKycSentStatus"]
								: "";

						var cell6 = crow.insertCell(6);
						cell6.innerHTML = '<select style="width:80%;" name="selverKycMgrApprSts" class="kycSelect" disabled="true"  onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" onchange="callManagerApproveStatus(this);"></select><img src="images/update-icon.png" width="20px" height="15px" id="update" alt="Update Manager" style="display:none"   onclick="mgrApprveSts(this)"/><input type="hidden" />';
						comboMakerByString(STR_FNA_MGRAPPRSTS_LIST,	cell6.childNodes[0],	value[cnt - 1]["mgrApprSts"], true);
						
						

						var cell7 = crow.insertCell(7);
						cell7.innerHTML = '<input type="text" name="txtFldVerMgrApprRem" readOnly="true" maxlength="300" class="fpNonEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type="hidden" size="1" name="hTxtFldLastCrtdArch"/>';

						var cell8 = crow.insertCell(8);
						cell8.innerHTML = '<input type="text" name="txtFldVerMgrApprDate" readOnly="true" class="fpNonEditTblTxt" style="width:80%" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type="hidden" name="htxtFldCompapproveStats"/>'
								+ '<img src="images/info.png" onmouseover="showMgrInfo(event,this,\'mgr\');" onmouseout="hideTooltip();" style="height:18px;width:18px;vertical-align:middle" />';
						;

						var cell9 = crow.insertCell(9);
						cell9.innerHTML = '<input type="checkbox" name="chkClientConsent" onclick="chkClntConsent(this)" disabled="true"/><input type="hidden" name="hTxtFldClntConsent" /><input type="hidden"/>';

						var cell10 = crow.insertCell(10);
						// 2015_12_12 - TIME BEING
						// cell10.innerHTML = '<img src="images/update-icon.png"
						// width="20px" height="20px"
						// style="display:none;cursor:pointer;" id="update"
						// alt="Update Manager" onclick="mgrApprveSts(this)"/>';
						cell10.innerHTML = '<img src="images/update-icon.png" width="20px" height="20px" style="display:none;cursor:pointer;" id="update" alt="Update Manager"  onclick="alert(\'UPDATE NOT AVAILABLE.\')"/>';

						var cell11 = crow.insertCell(11);
						cell11.innerHTML = '<input type="text" name="txtFldTblFnaType" readOnly="true" class="fpNonEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>'
								+ '<input type="hidden" name="htxtFldMgrApprBy"/>'
								+ '<input type="hidden" name="htxtFldAdminApprBy"/>'
								+ '<input type="hidden" name="htxtFldCompApprBy"/>';

						var cell12 = crow.insertCell(12);
						cell12.innerHTML = '<img src="images/export.png" style="display:none" width="20px" onclick="exportArchiveDets(this);insertCustomerAttachments(this);" height="20px" id="export" alt="Export..."/>'; // fnaGenReport(this,
						// true

						var cell13 = crow.insertCell(13);
						cell13.innerHTML = '<select  name="selverAdminKycSentStatus"  disabled="true" class="kycSelect" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"></select>';
						comboMakerByString(STR_FNA_YESNO_LIST,	cell13.childNodes[0],value[cnt - 1]["txtFldFnaDetAdminKycSentStatus"],	true);
						cell13.childNodes[0].value = !isEmpty(value[cnt - 1]["txtFldFnaDetAdminKycSentStatus"]) ? value[cnt - 1]["txtFldFnaDetAdminKycSentStatus"]	: "";

						var cell14 = crow.insertCell(14);
						cell14.innerHTML = '<select style="width:100%;" name="selverAdminKycApprSts" onchange="callAdminApproveStatus(this);" disabled="true" class="kycSelect" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" ></select><input type="hidden" name="htxtFldAdminapproveStats"/>';
						comboMakerByString(STR_FNA_MGRAPPRSTS_LIST,cell14.childNodes[0],value[cnt - 1]["txtFldFnaDetAdminApproveStatus"],true);
						cell14.childNodes[0].value = !isEmpty(value[cnt - 1]["txtFldFnaDetAdminApproveStatus"]) ? value[cnt - 1]["txtFldFnaDetAdminApproveStatus"]: "";
						cell14.childNodes[1].value = !isEmpty(value[cnt - 1]["txtFldFnaDetAdminKycSentStatus"]) ? value[cnt - 1]["txtFldFnaDetAdminKycSentStatus"]: "";
						
						

						var cell15 = crow.insertCell(15);
						cell15.innerHTML = '<input type="text" name="txtFldVerAdminApprRem" readOnly="true" maxlength="300" class="fpNonEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';
						cell15.childNodes[0].value = value[cnt - 1]["txtFldFnaDetAdminRemarks"];

						var cell16 = crow.insertCell(16);
						cell16.innerHTML = '<input type="text" name="txtFldVerAdminApprDate" readOnly="true" class="fpNonEditTblTxt" style="width:80%" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>'
								+ '<img src="images/info.png" onmouseover="showMgrInfo(event,this,\'admin\');" onmouseout="hideTooltip();" style="height:18px;width:18px;vertical-align:middle" />';
						;
						cell16.childNodes[0].value = !isEmpty(value[cnt - 1]["txtFldFnaDetAdminApproveDate"]) ? value[cnt - 1]["txtFldFnaDetAdminApproveDate"]: "";

						var cell17 = crow.insertCell(17);
						cell17.innerHTML = '<select  name="selverCompKycSentStatus" class="kycSelect" disabled="true"  onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"></select>';
						comboMakerByString(STR_FNA_YESNO_LIST,cell17.childNodes[0],value[cnt - 1]["txtFldFnaDetCompKycSentStatus"],true);
						cell17.childNodes[0].value = !isEmpty(value[cnt - 1]["txtFldFnaDetCompKycSentStatus"]) ? value[cnt - 1]["txtFldFnaDetCompKycSentStatus"]: "";

						var cell18 = crow.insertCell(18);
						cell18.innerHTML = '<select style="width:100%;" name="selverCompKycApprSts"  class="kycSelect" onchange="callComplianceApproveStatus(this);"  onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" ></select><input type="hidden" name="htxtFldCompapproveStats"/>';
						comboMakerByString(STR_FNA_MGRAPPRSTS_LIST,cell18.childNodes[0],value[cnt - 1]["txtFldFnaDetCompApproveStatus"],true);
						cell18.childNodes[0].value = !isEmpty(value[cnt - 1]["txtFldFnaDetCompApproveStatus"]) ? value[cnt - 1]["txtFldFnaDetCompApproveStatus"]: "";
						cell18.childNodes[1].value = !isEmpty(value[cnt - 1]["txtFldFnaDetCompApproveStatus"]) ? value[cnt - 1]["txtFldFnaDetCompApproveStatus"]: "NOT YET APPROVED";
						
						

						var cell19 = crow.insertCell(19);
						cell19.innerHTML = '<input type="text" name="txtFldVerCompKycRem" readOnly="true" maxlength="300" class="fpNonEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';
						cell19.childNodes[0].value = value[cnt - 1]["txtFldFnaDetCompRemarks"];

						var cell20 = crow.insertCell(20);
						cell20.innerHTML = '<input type="text" name="txtFldVerCompApprDate" readOnly="true" class="fpNonEditTblTxt" style="width:80%" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>'
								+ '<img src="images/info.png" onmouseover="showMgrInfo(event,this,\'comp\');" onmouseout="hideTooltip();" style="height:18px;width:18px;vertical-align:middle" />';
						;

						cell20.childNodes[0].value = !isEmpty(value[cnt - 1]["txtFldFnaDetCompApproveDate"]) ? value[cnt - 1]["txtFldFnaDetCompApproveDate"]: "";

						var accessLvl = strGlblUsrPrvlg.split(",");

						// if(latestArch)
						for (var len = 0; len < accessLvl.length; len++) {
							if (accessLvl[len] == ADMIN_KYC_APPROVAL) {
								if (!(cell14.childNodes[0].value == "APPROVE")) {
									cell14.childNodes[0].disabled = true;// disabled by johnson 20160226
									cell15.childNodes[0].readOnly = true;// disabled by johnson 20160226
								}

							}
						}
						var comFlg = false;

						for (var len = 0; len < accessLvl.length; len++) {
//							alert(accessLvl[len])

							if (accessLvl[len] == COMP_KYC_APPROVAL) {
								if (!(cell18.childNodes[0].value == "APPROVE")) {
									cell18.childNodes[0].disabled = true;// disabled by johnson 20160226
									cell19.childNodes[0].readOnly = true;// disabled by johnson 20160226
								}

								if ((cell18.childNodes[0].value == "APPROVE")) {
									cell18.childNodes[0].disabled = true;
								}

								comFlg = true;
							}
						}

						if (!comFlg) {
							cell18.childNodes[0].disabled = true;
							cell19.childNodes[0].readOnly = true;
						}

//						 -------------- VALUE SETTING FROM DB--------------

						cell0.childNodes[0].value = value[cnt - 1]["fna_id"];
						cell1.childNodes[0].value = value[cnt - 1]["verCretdBy"];
						cell2.childNodes[0].value = value[cnt - 1]["verCretdDate"];
						cell1.childNodes[2].value = value[cnt - 1]["txtFldPolNTUCId"];
						cell2.childNodes[1].value = value[cnt - 1]["txtFldFnaNtucPolNum"];
						cell2.childNodes[4].value = value[cnt - 1]["txtFldFnaCustId"];


						cell3.childNodes[1].value = !isEmpty(value[cnt - 1]["txtFldFnaServAdvEmail"]) ? value[cnt - 1]["txtFldFnaServAdvEmail"]: "";
						cell3.childNodes[2].value = !isEmpty(value[cnt - 1]["txtFldFnaServMgrEmail"]) ? value[cnt - 1]["txtFldFnaServMgrEmail"]: "";
						cell3.childNodes[3].value = !isEmpty(value[cnt - 1]["txtFldFnaServAdvName"]) ? value[cnt - 1]["txtFldFnaServAdvName"]: "";

						var ntuccasestatus = value[cnt - 1]["txtFldNtucCaseSts"];
						var ntuccaseid = value[cnt - 1]["txtFldNtucCaseId"];

						cell3.childNodes[8].value=ntuccasestatus;

						cell4.childNodes[0].value = !isEmpty(value[cnt - 1]["selKycArchStatus"]) ? value[cnt - 1]["selKycArchStatus"]: "";
						var kycsts = !isEmpty(value[cnt - 1]["selKycSentStatus"]) ? value[cnt - 1]["selKycSentStatus"]	: "";
						/*
						 * cell5.childNodes[0].value = kycsts == "Y" ? "YES" :
						 * kycsts == "N" ? "NO" : "";
						 */
						cell6.childNodes[0].value = !isEmpty(value[cnt - 1]["selKycMgrApprStatus"]) ? value[cnt - 1]["selKycMgrApprStatus"] : "";
						cell6.childNodes[2].value = !isEmpty(value[cnt - 1]["txtFldFnaServAdvId"]) ? value[cnt - 1]["txtFldFnaServAdvId"] : "";
						cell7.childNodes[0].value = !isEmpty(value[cnt - 1]["txtFldMgrApprRem"]) ? value[cnt - 1]["txtFldMgrApprRem"] : "";
						cell8.childNodes[0].value = !isEmpty(value[cnt - 1]["txtFldMgrApprDate"]) ? value[cnt - 1]["txtFldMgrApprDate"] : "";
						cell8.childNodes[1].value = !isEmpty(value[cnt - 1]["selKycMgrApprStatus"]) ? value[cnt - 1]["selKycMgrApprStatus"] : "";
						cell9.childNodes[1].value = value[cnt - 1]["txtFldClntConsent"];
						cell9.childNodes[2].value = value[cnt - 1]["txtFldCustAttchFlg"];

						cell11.childNodes[0].value = value[cnt - 1]["txtFldFnaType"];
						cell11.childNodes[1].value = value[cnt - 1]["txtFldFnaMgrApprBy"];
						cell11.childNodes[2].value = value[cnt - 1]["txtFldFnaAdminApprBy"];
						cell11.childNodes[3].value = value[cnt - 1]["txtFldFnaCompApprBy"];

						if (value[cnt - 1]["txtFldClntConsent"] == "Y") {
							cell9.childNodes[0].checked = true;
						} else if (value[cnt - 1]["txtFldClntConsent"] != "Y") {
							cell9.childNodes[0].checked = false;
						}

						// -------------- VALUE SETTING FROM DB--------------

						// ------------------- OTHER THAN STAFF AND MANAGER
						// ACCESS , ARCH STS REJECT IS NOT NEEDED --------

						if (LOG_USER_STFTYPE != STFTYPE_STAFF) {
							if (LOG_USER_MGRACS != "Y") {
								$("#selverKycArchStatus option[value='REJECT']").remove();
							}
						}

						// ------------------- OTHER THAN STAFF AND MANAGE
						// ACCESS , ARCH STS REJECT IS NOT NEEDED --------

						// ------------------ LATEST TEMPLATE ALONE ABLE TO
						// PROCEED FURTHER ------

						if (latestArch) {

							if (LOG_USER_STFTYPE == STFTYPE_ADVISER) {
//								cell4.childNodes[0].disabled = true; //commented by kavi 14/09/2018
							} else {
								if (LOG_USER_STFTYPE == STFTYPE_STAFF
										&& cell4.childNodes[0].value == "FINAL") {
									cell4.childNodes[0].disabled = true;
								} else {
									cell4.childNodes[0].disabled = false;
								}
							}

							cell7.childNodes[1].value = "Y";
//							cell10.childNodes[0].style.display = "";
							if(!isEmpty(ntuccaseid)){
								$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();
							}else{
								$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();
							}

							cell3.childNodes[5].value = ntuccaseid;

							if(ntuccasestatus.toUpperCase() == 'COMPLETED'){
								var cnfrm ="The Case Id :" + ntuccaseid +" is completed all the process at DSF side.\nDo you want to agree all the changes are completed.\n\n Click OK to Lock the case (No More changes are allowed after Lock).\n\n Click Cancel to need some more changes in KYC archive.";

								if(window.confirm(cnfrm)){


//									cell3.childNodes[8].value="Submitted";
									ntuccasestatus="Submitted";
									cell3.childNodes[4].value = ntuccasestatus;
									setnCallUpdateDSFParams(cell3,null);
									$(cell3).closest('tr').find("td:eq(3) img:eq(2)").hide();
									$(cell0).closest('tr').find("td:eq(0) img:eq(2)").hide();
									if(ntuc_sftp_access_flag){
										$(cell0).closest('tr').find("td:eq(0) img:eq(3)").show();
									}else{
										$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();
									}

								}else{

									ntuccasestatus="In-Progress";
									cell3.childNodes[4].value = ntuccasestatus;
									cell3.childNodes[8].value = "Submitted";
									cell3.childNodes[4].disabled=true;
//									cell3.childNodes[8].value="";
//									setnCallUpdateDSFParams(cell3,null);
									$(cell3).closest('tr').find("td:eq(3) img:eq(2)").show();
									$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();

								}
							}else if(ntuccasestatus.toUpperCase() == 'SUBMITTED'){

								cell3.childNodes[4].value = ntuccasestatus;
								cell3.childNodes[4].disabled=true;
								$(cell3).closest('tr').find("td:eq(3) img:eq(2)").hide();
								$(cell0).closest('tr').find("td:eq(0) img:eq(2)").hide();
								if(ntuc_sftp_access_flag)
									$(cell0).closest('tr').find("td:eq(0) img:eq(3)").show();
								else
									$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();

							}else if(ntuccasestatus.toUpperCase() == 'IN-PROGRESS'){

								cell3.childNodes[4].value = ntuccasestatus;
								cell3.childNodes[4].disabled=true;
								$(cell3).closest('tr').find("td:eq(3) img:eq(2)").show();
								$(cell0).closest('tr').find("td:eq(0) img:eq(2)").show();
								$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();

							}else if(ntuccasestatus.toUpperCase() == 'CANCEL'){

								 var option = document.createElement("option");
								    option.text = "Cancelled";
								    option.value = "Cancel";
								    cell3.childNodes[4].add(option,cell3.childNodes[4].length+1);

								cell3.childNodes[4].value = "Cancel";
								cell3.childNodes[4].disabled=true;
								$(cell3).closest('tr').find("td:eq(3) img:eq(2)").hide();
								$(cell0).closest('tr').find("td:eq(0) img:eq(2)").hide();
								$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();

							}else if(ntuccasestatus.toUpperCase() == "APPROVED" || ntuccasestatus.toUpperCase() == "REJECTED"){
								var option = document.createElement("option");
							    option.text =ntuccasestatus;
							    option.value = ntuccasestatus;
							    cell3.childNodes[4].add(option,cell3.childNodes[4].length+1);

							    cell3.childNodes[4].value = ntuccasestatus;
								cell3.childNodes[4].disabled=true;
								$(cell3).closest('tr').find("td:eq(3) img:eq(2)").hide();
								$(cell0).closest('tr').find("td:eq(0) img:eq(2)").hide();
								$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();
							}



							document.getElementById("txtFldAdvNameAttach").value = value[cnt - 1]["txtFldFnaServAdvName"];
						} else {

//							cell0.childNodes[6].style.display="none";
							$(cell0).closest('tr').find("td:eq(0) img:eq(3)").hide();
							$(cell0).closest('tr').find("td:eq(0) img:eq(4)").hide();
							$(cell3).closest('tr').find("td:eq(3) img:eq(2)").hide();

							cell3.childNodes[0].style.display = "none";


							cell4.childNodes[0].disabled = true;
							cell7.childNodes[1].value = "N";
							cell10.childNodes[0].style.display = "none";
							cell12.childNodes[0].style.display = "none";


							if(isEmpty(cell3.childNodes[5].value)){
								$(cell3).closest('tr').find("td:eq(0) img:eq(2)").hide();
							}

						}

						// ------------------ LATEST TEMPLATE ALONE ABLE TO
						// PROCEED FURTHER ------

						// ----------- MANAGER ACCESS WITH GENERAL MANAGER
						// DESIGNATION ALLOWS TO UPDATE THE MANAGER's
						// DETAILS------
						// ----------- CUSTOMER's SERVICING ALSO ---------------
						cell6.childNodes[0].style.width = "100%";
						if (LOG_USER_MGRACS == "Y"
								&& FNA_CUSTSERVADVID == LOG_USER_ADVID
								&& LOG_USER_DESIG == DESIG_GMR) {

							// cell5.childNodes[0].disabled=false; //commented
							// by johnson on 29042015

							if (!(cell6.childNodes[0].value == "APPROVE")) {

								cell6.childNodes[0].disabled = true;// disabled
								// by
								// johnson
								// 20160226
								cell6.childNodes[0].style.width = "100%";
								cell6.childNodes[1].style.display = "none";// This
								// icon
								// not
								// needed.Thru
								// General
								// icon
								// update
								// will
								// happen

								cell7.childNodes[0].readOnly = true;// disabled
								// by
								// johnson
								// 20160226

//								cell10.childNodes[0].style.display = "";

								// ----------- MANAGER ACCESS ALLOWS TO UPDATE
								// THE
								// MANAGER's DETAILS------
								// ----------- CUSTOMER's SERVICING IS NOT OF
								// LOGGED
								// USER ---------------
							}
						} else if (LOG_USER_MGRACS == "Y"
								&& FNA_CUSTSERVADVID != LOG_USER_ADVID) {

							// cell5.childNodes[0].disabled=false; //commented
							// by johnson on 29042015

							if (!(cell6.childNodes[0].value == "APPROVE")) {
								cell6.childNodes[0].disabled = true;// disabled
								// by
								// johnson
								// 20160226
								cell6.childNodes[0].style.width = "100%";
								cell6.childNodes[1].style.display = "none";// This
								// icon
								// not
								// needed.Thru
								// General
								// icon
								// update
								// will
								// happen

								cell7.childNodes[0].readOnly = true;// disabled
								// by
								// johnson
								// 20160226

//								cell10.childNodes[0].style.display = "";

								// ----------- OTHER THAN THE ABOVEs ----------
								// -------- ALL THE CONDITON ONLY APPLICABLE FOR
								// LATEST COPY -------
							}

						} else {

							cell6.childNodes[0].title = " Old Archive! ";
							cell6.childNodes[0].style.width = "100%";
							cell6.childNodes[1].style.display = "none";

						}

						if (value[cnt - 1]["selKycArchStatus"] == STR_ARCHSTS_FINAL) {
							// For Staff alone client consent flag is allowed to
							// check/uncheck to update.
							if (LOG_USER_STFTYPE == STFTYPE_STAFF) {
								if (latestArch) {
									cell9.childNodes[0].disabled = false;
								}
							}
						}
						
						
						 if(cell6.childNodes[0].value == "APPROVE"){
							 cell6.childNodes[0].options[1].innerHTML = "APPROVED";
							 cell6.childNodes[0].disabled=true;	
						 }
						 
						 if(cell14.childNodes[0].value == "APPROVE"){
							 cell14.childNodes[0].disabled = true;
							 cell14.childNodes[0].options[1].innerHTML = "APPROVED";
							 
						 }
						 
						 if(cell18.childNodes[0].value == "APPROVE"){
								cell18.childNodes[0].disabled = true;
								cell18.childNodes[0].options[1].innerHTML = "APPROVED";
							 }

//						 TIME_BEING
//						cell4.childNodes[0].disabled = true; commented by kavi 14/09/2018

						/*
						 * // Time-being if (LOG_USER_MGRACS == "Y") {
						 *
						 * cell5.childNodes[0].disabled = true;
						 * cell6.childNodes[0].disabled = true;
						 * cell7.childNodes[0].disabled = true;
						 * cell8.childNodes[0].disabled = true;
						 *
						 * cell10.childNodes[0].style.display = "none"; } //
						 * Time-being if (LOG_USER_STFTYPE == "ADVISER" ||
						 * LOG_USER_STFTYPE == "STAFF") {
						 *
						 * cell4.childNodes[0].disabled = true;
						 * cell3.childNodes[0].style.display = "none";
						 * cell10.childNodes[0].style.display = "none"; }
						 */

					}
				}

			}
		}
	}

	if (loadExistData) {


		loadExistCustDet(strExistDataCust);

//		fnaForm.dfSelfGender.value = fnaForm.dfSelfGender.value == "M" ? "MALE" : fnaForm.dfSelfGender.value == "F" ? "FEMALE" : "";
//		fnaForm.dfSpsGender.value = fnaForm.dfSpsGender.value == "M" ? "MALE" : fnaForm.dfSpsGender.value == "F" ? "FEMALE" : "";

		fnaForm.dfSelfAge.value = calcAge(fnaForm.txtFldFnaClntDOB.value);
		fnaForm.dfSpsAge.value = calcAge(fnaForm.txtFldFnaSpsDob.value);

		/*chkNextAgeBirthday(); commented by kavi 17/03/2018*/
//		setTimeout(function(){
//			$("#spsResetBtn").trigger("click");	
//		},500);
		
	}

	$('.readOnlyText').attr('readOnly', true);
	$('.writeModeTextboxNumber').prop('maxLength', 22);

//	 added by johnson on 22052015 in order to prevent number fields from cliboard functions
	$('.writeModeTextboxNumber').each(function(e) {
		$(this).bind('cut copy paste', function(e) {
			e.preventDefault();
		});
	});

	if (!archiveflg) {
		fixedHeader('verdettable', '1', '100%', '0px', '97%', '33em', '2','0px', '95.5%', '6.2em', '97%', true, false, 0, 0);
		archiveflg = true;
	}

	if (getBrowserApp == "Netscape") {
		document.getElementById("verdettableFltSet").style.width = "936px";
		document.getElementById("verdettabletableContainer").style.width = "99%";
		document.getElementById("verdettableheaderContainer").style.height = "5.3em";
	}

	page3fixedHeader();
	page4fixedHeader();
	page5fixedHeader();
	page6fixedHeader();
	page8fixedHeader();
	page10fixedHeader();
	page11fixedHeader();
//	 page16fixedHeader();
	page17fixedHeader();
	page19fixedHeader();
//added by kavi 05/03/2018
	page26fixedHeader();
	pageDepfixedHeader();
	page12fixedHeader();
	
//	alert("End of fixedheader");
//v3)For â€œApplication Typesâ€� section, the different type of advice section should appear according to the type of FNA selected at the initial stage. E.g. When Representative choose Simplified version, only Option 2 section is open for selection while Option 1 is blocked.
	var $formtype = $("#txtFldKycForm").val();

	if ($formtype == SIMPLIFIED_VER) {
		$("#tblRowAllOptAppType").find("td").find("input").prop("disabled",true);//.hide();
		$("#tblRowSimpleOptAppType").find("td").find("input").prop("disabled",false);//.show();
	} else {
		$("#tblRowAllOptAppType").find("td").find("input").prop("disabled",false);//show();
		$("#tblRowSimpleOptAppType").find("td").find("input").prop("disabled",true);//.hide();
	}
//	End of v3)

	if (LOG_USER_STFTYPE == STFTYPE_STAFF) {
//		 document.getElementById("saveBtn").style.display="none";
		saveBtnObj.setStyle('display', 'none');
		reportBtnObj.setStyle('display', 'none');
		ntucBtnObj.setStyle('display', 'none');
		clientSignBtnObj.setStyle('display', 'none');
		$("#esignaturepopbox").hide();
	} else {

		if (FNA_FROM_EXSTNEW == FNA_FROM_NEWKYC) {
			saveBtnObj.setStyle('display', '');
			reportBtnObj.setStyle('display', 'none');
			ntucBtnObj.setStyle('display', 'none');
			clientSignBtnObj.setStyle('display', 'none');
			$("#esignaturepopbox").hide();
		}
	}

	// added by johnson on 22052015
	if (getBrowserApp == "Netscape")
		$('#txtFldCDBenfJobCont,#txtFldCDTppJobCont,#txtFldCDPepJobCont').css('width', '111px');
//	 if(detectBrowser.isFF())
//	 $('#txtFldCDBenfJobCont,#txtFldCDTppJobCont,#txtFldCDPepJobCont').css('width','106px');

//	 2015_12_17
	makeReadOnlyNameNric();

	/*var countryNameSplit = strCountryAllPair.substring(0,strCountryAllPair.length - 1).split("^");
	for (var cn = 0; cn < countryNameSplit.length; cn++) {
		var split = countryNameSplit[cn].split("=");
		var strCounCode = split[0];
		var strCountry = split[1];
		CountryAllArray[cn] = new Option(strCounCode, strCountry);
	}*/

	// UNSAVED CUST DATA
	// KycUtils.java > loadFpmsnlCustDets method > JSON Key values to be set.
	// alert("loadExistData :: ---->"+loadExistData);
	// alert("strUnSavedCustData ::--->"+strUnSavedCustData);
	if (strUnSavedCustData != null && loadExistData && !isArchiveExist) {
		setUnSaveData(strUnSavedCustData);
	}

	/*
	 * if(LOG_USER_MGRACS == "Y"){ fnaForm.suprevMgrFlg[0].disabled=false;
	 * fnaForm.suprevMgrFlg[1].disabled=false;
	 * fnaForm.suprevFollowReason.disabled=false; }else if(LOG_USER_MGRACS !=
	 * "Y"){ fnaForm.suprevMgrFlg[0].disabled=true;
	 * fnaForm.suprevMgrFlg[1].disabled=true;
	 * fnaForm.suprevFollowReason.disabled=true; }
	 */


	/*$("body").css({"height":"100%","width":"100%"});
	$("html").css({"height":"100%","width":"100%"});*/

/*added by kavi for tooltip 15/03/2018*/
$(function(){

	if(!isEmpty($("#selFnaSelectedMgrId").val())){
		$("#txtFldSupVName").val($("#selFnaSelectedMgrId option:selected").text());
	}else{
		$("#txtFldSupVName").val("");
	}

	if($("#dfSelfSameasregadd").is(":checked")){
		$("#dfSelfMailaddrflg,#dfSpsMailaddrflg").prop("disabled",true);
	}

		$(".addTooltip").on('click',function(){
			hideTooltip();
		}).on("mouseout",function(){
			hideTooltip();
		}).on("mouseover",function(e){
			showTooltip(e,$(this).val());
		});

		/*added by kavi 15/03/2018 for number validation*/
		$(".kycFloatFld").on('keypress',function(e){
			return blockNonNumbers(this,e,true,false);
		}).on('keyup',function(){
			chkDecimal(this,16,4);
		}).on('keydown',function(){
			chkDecimal(this,16,4);
		});

	var relOption=$("#intprtRelat option").clone();
	$("#drcIntRel").append(relOption).val($("#intprtRelat").val()).prop("disabled",true);

	$("#intprtRelat").change(function(){
		$("#drcIntRel").val($(this).val());
	});


    if(LOG_USER_STFTYPE.toUpperCase()=='STAFF'){//added by kavi 16/03/2018
    	$("#declReqTbl").hide();
    }else{
    	$("#declReqTbl").hide();
    }

    /*added by kavi for spouse 17/03/2018*/
//    This block is not required
    
    /*if(isEmpty($('input[name="txtFldSpouseName"]').val())){
    	$('input[name="txtFldSpouseName"]').removeClass('readOnlyText').addClass("writeModeTextbox")
    	.prop("disabled",false).prop('readonly',false);
    }*/

    $('input[name="txtFldSpouseName"],input[name="dfSpsName"]').on('change',function(){
    	if(isEmpty(this.value)){
    		$('input[name="txtFldSpouseName"],input[name="dfSpsName"]').val($(this).val());	
    		clearSpsfld();
    	}else{
    		
    		var parameter = "dbcall=GETSPOUSEDETAILS&txtFldSpsNric="+$(this).find("option:selected").attr("data-nric")+"&txtFldSpsName="+$(this).val();
    		var respText = ajaxCall(parameter);
    		
    		clearSpsfld();
    		
    		$.each(respText,function(i,objArr){
    			$.each(objArr,function(elm,val){
    				$("[name="+elm+"]").val(val);
    				
    				if(elm=='dfSpsDob'){
    					$("[name="+elm+"]").trigger('onblur');
    				}
    				
    				if(elm=='dfSpsNation'){
    					for(var cnt=0;cnt<3;cnt++){
							if(document.getElementsByName('dfSpsNationality'))
								document.getElementsByName('dfSpsNationality')[cnt].checked=false;
						}

						switch (val) {
							case "SG":document.getElementsByName('dfSpsNationality')[0].checked = true;;break;
							case "SGPR":document.getElementsByName('dfSpsNationality')[1].checked = true;break;
							case "OTH":document.getElementsByName('dfSpsNationality')[2].checked = true;break;
						}

						chkForContry('dfSpsNationality',val);
    					
    				}
    				
    				if(elm=='dfSpsHeight'){
    					$("[name="+elm+"]").val(removeDecimalPoint(val));
    				}
    				
    			});
    		});
    		
    		$('select[name="txtFldSpouseName"],select[name="dfSpsName"]').val($(this).val());
    	}
    })
    /*.on('keyup',function(){
    	$('select[name="txtFldSpouseName"],select[name="dfSpsName"]').val($(this).val());
    }); removed*/

    $('input[name="intprtName"],input[name="intprtNric"]').on('blur',function(){
    	$("input[name='"+$(this).attr("name")+'s'+"']").val($(this).val());
    });

    $(".kycDisabledFld").removeClass("writeModeTextbox").addClass("readOnlyText").prop("readonly",true);

    $("#usTaxGovLink").on('click',function(){
    	window.open("http://www.irs.gov");
    });

    if(custLobDets=='LIFE'){
    	$("input[name='txtFldArtuFundIaf']").prop("disabled",true);
    	$("input[name='txtFldSwrepFundIaf']").prop("disabled",true);
    }


//    Page 6 Radio to Checkbox changes 12052018
    $('.spnClntRiskPref .crRiskclass').change(function(){
    	$('.spnClntRiskPref .crRiskclass').not(this).prop('checked', false);
    });
    
    
    $('.spnClntRiskPrefILP .crRiskclassILP').change(function(){
    	$('.spnClntRiskPrefILP .crRiskclassILP').not(this).prop('checked', false);
    });
    
//    Page 6 Radio to Checkbox changes 12052018

    syncscroll.reset();
    /*$("#fnaartuplanTblfixedHead").mouseover(function(){
    	tblCommonScroll('fnaartuplanTblssfixedHead');
    });

    $("#fnaartuplanTblssfixedHead").mouseover(function(){
    	tblCommonScroll('fnaartuplanTblfixedHead');
    });*/

    $("#tabs-arch-li").on("click",function(){

    	if($("#tabs-template-li").is(":visible")){
    		var checkChange=$("#chkanychange").val();

			if(!isEmpty(checkChange) && !$.isEmptyObject($.parseJSON(checkChange)) && NTUCChkChngSts){

				if(!chkAllDataSaved("",false,FNA_CONS_MODIFY_INFO))
					{
//					chkAllDataSaved("",false);
//					hidePagingDiv();
					

					$('#reportvaliationPopup').dialog('option', 'title', 'E-Submit validation');
					$( "#reportvaliationPopup" ).dialog( "option", "height",280);
					$( "#reportvaliationPopup" ).dialog( "option", "width",500);
					$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();


					var buttons = $( ".reportvaliationPopup" ).dialog( "option", "buttons" );

					$('#reportvaliationPopup .ui-dialog-buttonset button:first').remove();

					$( "#reportvaliationPopup" ).dialog( "option", "buttons",
							[
							    {
							      text: "OK",
							      click: function() {
											$('#reportvaliationPopup').dialog("close");
//											$("#saveBtn-button").click();
											saveOrUpdate();
							      }

							    },
							    {
							    	text : "Cancel",
							    	click : function(){
							    		$('#reportvaliationPopup').dialog("close");
									  	$("#tabs-template-li").hide();
							    	}
							    }
							 ]
					);
					return;
					
					
					}



				NTUCChkChngSts=false;
			}else{
				hidePagingDiv();
			}

    	}

    });

    $("#htfrepdecno1all").on("click",function(){
    	if(this.checked){
    		$("#repDecTbl input[type=checkbox]").each(function(){
    			var chkid = $(this).prop("id");
    			if(chkid != "htfrepdecno1all"){
    				this.checked=true;
        			$(this).trigger("change");	
    			}
    			
    		});
    	}else{
    		$("#repDecTbl input[type=checkbox]").each(function(){
    			var chkid = $(this).prop("id");
    			if(chkid != "htfrepdecno1all"){
	    			this.checked=false;
	    			$(this).trigger("change");
    			}
    		});
    	}
    	
    });
    function resetClient2Dets(){
    	
    	changeSpouseNames($("#dfSpsName"));
    	
    	$("#divASpouseSign").hide();

    	$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").off("click");
    	$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").css({"cursor": "not-allowed","pointer-events": "none"});
    	$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").closest("div").css({"cursor": "not-allowed"});
    	$("#signsts_sps_div").addClass("disabledivs");
    	
    }
    
//added by kavi to reset spouse 24/02/2018
    $("#spsResetBtn,#spsResetBtn1").on("click",function(){
    	$("#tblSelfSps tbody > tr > td:nth-child(3)").each(function(){
    		$(this).find("select,input:text").val("").trigger("change");
    		$(this).find("input:radio,input:checkbox").not("[name=chkCDSpsSrcIncmOthrs],#radSpsSGPR,#radSpsOTH,[name=dfSpsMailaddrflg]").prop("checked",false);
    	});
    	
    	if($("#tblSelfSps tbody #radSpsOTH")[0].checked){
    		$("#tblSelfSps tbody #radSpsSG").trigger("click").prop("checked",false);
    	}
    	
    	if($("#tblSelfSps tbody #radSpsSGPR")[0].checked){
    		$("#tblSelfSps tbody #radSpsSG").trigger("click").prop("checked",false);
    	}
    	
    	if($("#tblSelfSps tbody #dfSpsMailaddrflg")[0].checked){
    		$("#tblSelfSps tbody #dfSpsMailaddrflg").trigger("click").prop("checked",false);
    	}
    	
    	if($("#tblSelfSps tbody #chkCDSpsSrcIncmOthrs")[0].checked){
    		$("#tblSelfSps tbody #chkCDSpsSrcIncmOthrs").trigger("click").prop("checked",false);
    	}
    	
//    	changeSpouseNames($("#dfSpsName"));
    	resetClient2Dets();
    	clearSpsfld();
    	
    });
    
    $("#reportvaliationPopup").dialog({
    	autoOpen : false,
    	height : '550',
    	width : '900',
    	modal : true,
    	resizable : false,
    	// screen center
    	my : "center",
    	at : "center",
    	of : window,
    	// screen center
    	buttons : {
    		" OK " : function() {
    				$(this).dialog("close");
    			}
    	}
    });
    
   });

$("#fnaartuplanTblfixedHead").addClass("syncscroll").attr("name","fnaTableScroll");
$("#fnaartuplanTblssfixedHead").addClass("syncscroll").attr("name","fnaTableScroll");

$("#fnaswrepplanTblfixedHead").addClass("syncscroll").attr("name","fnaTableSwScroll");
$("#fnaswrepplanTblssfixedHead").addClass("syncscroll").attr("name","fnaTableSwScroll");

var vararrGloabal=[];

$(".checkdb").each(function(){

	$(this).on("change",function(){


		var nodetype = $(this).prop('nodeName').toLowerCase();


		var chkanychange = $("#chkanychange");
		var jsonvalues = JSON.parse(isEmpty(chkanychange.val()) ? "{}" : chkanychange.val());

		var inputtype = $(this).prop("type").toLowerCase();
		if(inputtype ==  "text" || inputtype == "hidden" || inputtype == "select" || inputtype == "textarea" || nodetype == "select"){
//			vararrGloabal.push(new Array($(this).prop("name"),$(this).val()))

			var newobj=jsonvalues;
			newobj[$(this).prop("name")]=$(this).val();
			chkanychange.val(JSON.stringify(newobj));

		}else if(inputtype == "radio" || inputtype == "checkbox"){
			if($(this).prop("checked")){
//				vararrGloabal.push(new Array($(this).prop("name"),$(this).val()))

				var newobj=jsonvalues;
				newobj[$(this).prop("name")]=$(this).val();
				chkanychange.val(JSON.stringify(newobj));

			}
		}
	});
});

document.getElementById("fnaSwrepPlanAddRowBtn").onclick=function(){
	fnaSwrepPlanAddRow('fnaswrepplanTbl','N',this);
};

$('#dfSelfBirthcntry option[value="SINGAPORE"]').insertAfter('#dfSelfBirthcntry option[value=""]');
$('#dfSpsBirthcntry option[value="SINGAPORE"]').insertAfter('#dfSpsBirthcntry option[value=""]');


var page2clientName = $("#dfSelfName").val();
var page2spouseName = $("#dfSpsName").val();
$("#selCrRiskprefFor_Top option").each(function() {
	  if($(this).val() == "CLIENT") {
	    $(this).text("CLIENT - " + page2clientName);            
	  }                        
	});

/*if(LOG_USER_MGRACS != "Y"){
	$("#divManagerSign").hide();
	$("#signsts_mgr_div").addClass("disabledivs");
}else if(LOG_USER_MGRACS == "Y"){
	$("#divManagerSign").show();
	$("#signsts_mgr_div").removeClass("disabledivs");
}*/

$("#divManagerSign").hide();
$("#signsts_mgr_div").addClass("disabledivs");



if(TOTAL_ARCHIVE == 0){
	
	$("#page1_clientsign,#taxpage_clientsign,#signpage_clientsign,#page12_clientsign,#signpage_advisersign,#page13_advisersign,#page13_managersign,#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").off("click");
	$("#page1_clientsign,#taxpage_clientsign,#signpage_clientsign,#page12_clientsign,#signpage_advisersign,#page13_advisersign,#page13_managersign,#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").css({"cursor": "not-allowed","pointer-events": "none"});
	$("#page1_clientsign,#taxpage_clientsign,#signpage_clientsign,#page12_clientsign,#signpage_advisersign,#page13_advisersign,#page13_managersign,#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").closest("div").css({"cursor": "not-allowed"});
	
}

if(!isEmpty(page2spouseName)){
	$("#divASpouseSign").show();
	$("#signsts_sps_div").removeClass("disabledivs");
}else{
	$("#divASpouseSign").hide();

	$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").off("click");
	$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").css({"cursor": "not-allowed","pointer-events": "none"});
	$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").closest("div").css({"cursor": "not-allowed"});
	$("#signsts_sps_div").addClass("disabledivs");
	
}

//this settimeout function meant for approval screen manager verifications
setTimeout(function(){

	var kycapprovalfnaid = $("#hTxtFldKycApprovalFnaId").val();
	
	if(!isEmpty(kycapprovalfnaid)){
		
		var tid = document.getElementById("verdettable");
		var tb = tid.tBodies[0];
		var rl = tb.rows.length;
		var selrow=null;
		
		for(var r=0;r<rl;r++){
			var fnaid = tb.rows[r].cells[0].childNodes[0].value;
			if(fnaid == kycapprovalfnaid ){				
				selrow = tb.rows[r];				
				break;
			}
		}
		
		selrow.cells[0].childNodes[1].checked=true;
		populateFnaDets(selrow.cells[0].childNodes[1]);
				
		$(".kycPageNo").find(":input").prop("disabled",true);
		$(".kycPageNo").find("img").not("#imgClientFeedback,#imgMgrSign," +
				"#page1_clientsign,#taxpage_clientsign,#signpage_clientsign,#page12_clientsign," +
				"#signpage_advisersign,#page13_advisersign," +
				"#page13_managersign," +
				"#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").hide();
				
		$("#toolbar").hide();
		$("#esignaturepopbox").hide();
		
		
	}
	
	
	var servAdvId = $("#txtFldClntServAdv").val();
	var servAdvMgrId = $("#txtFldMgrId").val();
	var mgracs = $("#hTxtFldLoggedAdvMgrAcs").val();
	var loggeduserid = $("#hTxtFldLoggedAdvUserId").val();
	var loggedadvstfid = $("#hTxtFldLoggedAdvStfId").val();

	if (LOG_USER_MGRACS == "Y" && LOG_USER_ADVID == servAdvMgrId ) {
//		$("#tr_managersection").find(":input").prop("disabled",false);
		
		$("#tabs-arch-li").css("display", "none");
		$("#imgMgrSign").click(function(){openSignaturePad('manager');});

		
	}else{
		$("#tr_managersection").find(":input").prop("disabled",true);
		$("#page13_managersign").off("click");
		$("#page13_managersign").css({"cursor": "not-allowed","pointer-events": "none"});
		$("#page13_managersign").closest("div").css({"cursor": "not-allowed"});
	}
	
	$("#wrapper").addClass("wrapperCompact");
	
}, 200);



}

function ajaxCall(parameter) {

	var jsnResponse = "";

	$.ajax({
		type : "POST",
		url : "KYCServlet",
		data : parameter,
		dataType : "json",

		async : false,

		beforeSend : function() {
			$("#loadingLayer").show();
			$("#loadingMessage").show();
			loaderBlock();
		},

		success : function(data) {
			jsnResponse = data;
		},
		complete : function() {
			// alert("Loaded Success...");
			$("#loadingMessage").hide();
			setTimeout(function(){ loaderHide(); },200);
		},
		error : function() {
			// alert("Loading Error...");
			$("#loadingMessage").hide();
			setTimeout(function(){ loaderHide(); },200);
		}
	});

	return jsnResponse;
}

function populateFnaDets(obj) {

	$("#loadingMessage").show();


	// Clearing the existing values

	$("#sucmsg").html("");

	for (var i = 0; i < fnaForm.elements.length; i++) {

		var e = fnaForm.elements[i];
		if (!(e.name == "verArchFnaId" || e.name == "version"
				|| e.name == "verCretdBy" || e.name == "verCretdDate"
				|| e.name == "selverKycArchStatus"
				|| e.name == "selverKycSentStatus"
				|| e.name == "selverKycMgrApprSts"
				|| e.name == "txtFldVerMgrApprRem"
				|| e.name == "txtFldVerMgrApprDate"
				|| e.name == "hTxtFldLastCrtdArch"
				|| e.name == "chkClientConsent" || e.name == "txtFldTblFnaType"
				|| e.name == "curr_pageval" || e.id == "curr_pageval"
				|| e.name == "curr_pageval_simple" || e.id == "curr_pageval_simple")) {

			if (e.type == "text") {
				e.value = "";
			}
			if (e.type == "checkbox" || e.type == "radio") {
				e.checked = false;
			}
			if (e.type == "textArea") {
				e.value = "";
			}
		}
	}

	$("#chkanychange").val("")

	for ( var tbl in kycDhtmlTblId) {
		removeTblRows(kycDhtmlTblId[tbl]);
	}

	// Clearing the existing values

	var fna_id = obj.parentNode.childNodes[0].value;
	var strLastCrtdArchFlg = obj.parentNode.parentNode.childNodes[7].childNodes[1].value;
	var selectedFnaArchSts = obj.parentNode.parentNode.cells[4].childNodes[0].value;
	var selectedFnaClntCon = obj.parentNode.parentNode.cells[9].childNodes[0].checked;
	var formType = obj.parentNode.parentNode.cells[11].childNodes[0].value;




	// This is added on 30_06_2015
	if (strLastCrtdArchFlg == "Y") {
		formType = FNA_FORMTYPE;
	} else {
		formType = obj.parentNode.parentNode.cells[11].childNodes[0].value;
	}

	/*if (selectedFnaArchSts == STR_ARCHSTS_FINAL && !selectedFnaClntCon	&& strLastCrtdArchFlg == "Y") {
		// selFinalFlg=true;
		alert(FNA_FINALARCH_VALMSG);
		// $("#btngroup").hide();
		saveBtnObj.setStyle('display', 'none');
		reportBtnObj.setStyle('display', 'none');
		// return false;
	}*/ //commented by kavi on 14/09/2018

	if (strLastCrtdArchFlg == "Y") {

		/*if (selectedFnaArchSts == STR_ARCHSTS_FINAL && !selectedFnaClntCon) {
			// $("#btngroup").hide();
			saveBtnObj.setStyle('display', 'none');
			reportBtnObj.setStyle('display', 'none');
		} else {

			// $("#btngroup").show();
			saveBtnObj.setStyle('display', '');
			reportBtnObj.setStyle('display', '');
		}*/ //commented by kavi on 14/09/2018

		saveBtnObj.setStyle('display', '');
		reportBtnObj.setStyle('display', '');
		if(!($("#verdettable tbody tr:first td:eq(3) select:first").val()=="Submitted")){
			ntucBtnObj.setStyle('display', '');
		}
//		clientSignBtnObj.setStyle('display', '');
		$("#esignaturepopbox").show();
	} else {
		alert(OLD_ARCH_TEMPLATE);
		// fnaViewOnly();
		// $("#btngroup").hide();
		saveBtnObj.setStyle('display', 'none');
		reportBtnObj.setStyle('display', '');
		ntucBtnObj.setStyle('display', 'none');
		clientSignBtnObj.setStyle('display', 'none');
		$("#esignaturepopbox").hide();
	}

	$("#tabs-template-li").css("display", "inline");
	$("#tabs").tabs("option", "active", 1);

	// $("#paginationdiv").show();
	$("#tblArchDets").show();

	// if(formType == SIMPLIFIED_VER){
	// $("#paginationdiv").hide();
	// $("#simplepaginationdiv").show();
	// }
	// if(formType == ALL_VER){
	// $("#paginationdiv").show();
	// $("#simplepaginationdiv").hide();
	// }

	// var tblId = document.getElementById("verdettable");
	// var chktbody = tblId.tBodies[0];
	// var rl = chktbody.rows.length;
	// var saveflg=false;
	//
	// for(var r=0;r<rl;r++){
	// if(chktbody.rows[r].cells[4].childNodes[0].value == "FINAL"){
	// saveflg=true;
	// break;
	// }
	// }

	// if( (saveflg == true)){
	//
	// if(!selFinalFlg)
	// alert(FNA_FINALARCHANYONE_VALMSG);
	//
	// $("#btngroup").hide();
	// }

	// getTotPages(custLobArr, formType);
	// alert("totalpage"+totalpage)

	// if(formType != FNA_FORMTYPE){

	LoadPageWithPaging(formType);
	// }

	// $('.pagination').jqPagination('option', 'current_page', 1);

	loadExistCustDet(strExistDataCust);

	var parameter = "dbcall=VER&FNA_ID=" + fna_id;
	var respText = ajaxCall(parameter);

	console.log(JSON.stringify(respText));

	var retval = respText;
	changedarrGlobal.length=0;

	for ( var val in retval) {
		var tabdets = retval[val];
		if (tabdets["SESSION_EXPIRY"]) {
			window.location = baseUrl + SESSION_EXP_JSP;
			return;
		}

		NTUCChkChngSts=true;

		for ( var tab in tabdets) {
			if (tabdets.hasOwnProperty(tab)) {
				var value = tabdets[tab];

				if (tab == "ARCH_FNA_TABLE_DATA") {
					jsonDataPopulate(value, tab);

					// 2015_12_17
					makeReadOnlyNameNric();
				}

				if (tab == "ARCH_FNAGRP_TABLE_DATA") {

					var grpData = value;
					for ( var grp in grpData) {
						if (grpData.hasOwnProperty(grp)) {
							var grpvalue = grpData[grp];
							var grpElem = eval('fnaForm.' + grp);

							if (grpElem) {
								grpElem.value = grpvalue;
								if (grpvalue == "Y") {
									grpElem.checked = true;
									grpElem.value = grpvalue;
								}
							}
						}
					}
				}

				if (tab == "ARCH_FNASSDET_TABLE_DATA") {

					jsonDataPopulate(value, tab);
//					copyRegAddrToMailAddr(fnaForm.dfSelfMailaddrflg);
					mailAddrQueryMode(fnaForm.dfSelfMailaddrflg);
					setOnlyContactChkBox();

					var selfNatly = (document.getElementById("radselfSG").checked ? "SG" :
						(document.getElementById("radselfSGPR").checked ? "SGPR" : (document.getElementById("radselfOTH").checked ? "OTH" : "")));

					var spsNatly = (document.getElementById("radSpsSG").checked ? "SG" :
						(document.getElementById("radSpsSGPR").checked ? "SGPR" : (document.getElementById("radSpsOTH").checked ? "OTH" : "")));

					chkForContry('dfSelfNationality',selfNatly);
					chkForContry('dfSpsNationality',spsNatly);


//					if(cont == "dfSelfUspersonflg" && contvalue == "Y"){
//						document.getElementById(cont).checked=true;
						enabOrDisbTxtFld(document.getElementById("dfSelfUspersonflg"), 'dfSelfUstaxno', 'tdUSClntLblTxt');
//					}

//					if(cont == "dfSpsUspersonflg" && contvalue == "Y"){
//						document.getElementById(cont).checked=true;
						enabOrDisbTxtFld(document.getElementById("dfSpsUspersonflg"), 'dfSpsUstaxno', 'tdUSSpsLblTxt');
//					}

					/*if (cont == 'dfSelfUstaxno') {
						(!isEmpty(contvalue) ? clntUSTaxRefNum = contvalue : clntUSTaxRefNum = '');

					}// end of if

					if (cont == 'dfSpsUstaxno') {
						(!isEmpty(contvalue) ? spsUSTaxRefNum = contvalue : spsUSTaxRefNum = '');
					}// end of if
*/
					// 2016_03_04
					if (strUnSavedCustData != null) {
						setUnSaveData(strUnSavedCustData);
					}

					if (FNA_FORMTYPE == ALL_VER) {
						copyValuesPerstoClntDecl();
					}

					copyRegAddrToMailAddr(fnaForm.dfSelfSameasregadd);
				}

				if (tab == "ARCH_FNADEPNT_TABLE_DATA") {

					var depData = value;
					var tblobj = document.getElementById("fnaDepnTbl");
					var tbodyobj = tblobj.tBodies[0];

					for ( var dep in depData) {
						var depvalue = depData[dep];

						var rowlenobj = tbodyobj.rows.length;
						var rowobj = tbodyobj.insertRow(rowlenobj);

						var cell = rowobj.insertCell(0);
						cell.innerHTML =  '<input type="text" name="" readOnly="true" value="'+(rowlenobj+1)+'" style="width:52px;text-align:center;"/>';

						var cell0 = rowobj.insertCell(1);
						cell0.innerHTML = '<input type = "text" class="fpNonEditTblTxt"   name="txtFldFnaDepntName" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />'
								+ '<input type="hidden" name="txtFldFnaDepMode" value="'+ INS_MODE+ '"/>'
								+ '<input type="hidden" name="txtFldFnaDepId"/>';

						cell0.childNodes[0].value = depvalue["txtFldFnaDepntName"];
						cell0.childNodes[2].value = depvalue["txtFldDepId"];

						var cell3 = rowobj.insertCell(2);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<select name="selFnaDepntSex"  class="kycSelect"></select>';

						var cell2 = rowobj.insertCell(3);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type="text" name="txtFldFnaDepntAge" maxlength="3"  class="fpNonEditTblTxt"  size="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell2.childNodes[0].value = depvalue["txtFldFnaDepntAge"];

						var cell1 = rowobj.insertCell(4);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<select name="selFnaDepntRel" class="kycSelect" ></select>';

						var cell4 = rowobj.insertCell(5);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<input type = "text"  class="fpNonEditTblTxt"  name="txtFldFnaDepntYr2Sup" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);"  onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell4.childNodes[0].value = depvalue["txtFldFnaDepntYr2Sup"];

						var cell5 = rowobj.insertCell(6);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text" class="fpNonEditTblTxt"  name="txtFldFnaDepntOccp" maxlength="60"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />'+
						'<input type = "hidden" class="fpNonEditTblTxt"  name="txtFldFnaDepntMonthContr" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />'+
						'<input type = "hidden" class="fpNonEditTblTxt"  name="txtFldFnaDepntMonthSelf" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />'+
						'<input type = "hidden" class="fpNonEditTblTxt"  name="txtFldFnaDepntMonthSps" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />'
						cell5.childNodes[0].value = depvalue["txtFldFnaDepntOccp"];
						cell5.childNodes[1].value = depvalue["txtFldFnaDepntMonthContr"];
						cell5.childNodes[2].value = depvalue["txtFldFnaDepntMonthSelf"];
						cell5.childNodes[3].value = depvalue["txtFldFnaDepntMonthSps"];

						comboMaker(cell1, relArray, "", false);
						cell1.childNodes[0].value = depvalue["selFnaDepntRel"];

						comboMakerByString(STR_SEX, cell3.childNodes[0], "",true);
						cell3.childNodes[0].value = depvalue["selFnaDepntSex"];

						rowobj.onclick = function() {
							selectSingleRow(this);
						};


						var totalcell = rowobj.cells.length;
						for(var cel=0;cel<totalcell;cel++){
							if(rowobj.cells[cel].childNodes[0]){
								rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
									e = e || event;
									checkChanges(this);
								},false);
							}
						}
					};
				}

				if (tab == "ARCH_FNASAVGOAL_TABLE_DATA") {

					var fnaInvGoalTbl = document.getElementById("fnaInvGoalTbl");
					var savtbodyobj = fnaInvGoalTbl.tBodies[0];
					var savData = value;

					for ( var sav in savData) {

						var savvalue = savData[sav];
						var savrowlenobj = savtbodyobj.rows.length;
						var savrowobj = savtbodyobj.insertRow(savrowlenobj);

						var cell0 = savrowobj.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text"  name="txtFldFnaInvGoalPur" class="fpEditTblTxt"   onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" maxlength="30" />'+
						'<input type = "hidden"  name="txtFldFnaInvGoalMode" value="'+ INS_MODE	+ '" />'+
						'<input type = "hidden"  name="txtFldFnaInvGoalId"  />';
						cell0.childNodes[0].value = savvalue["txtFldFnaInvGoalPur"];

						var cell1 = savrowobj.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaInvGoalAmt" class="fpEditTblTxt"   maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell1.childNodes[0].value = savvalue["txtFldFnaInvGoalAmt"];

						var cell2 = savrowobj.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type="text" name = "txtFldFnaInvGoalYrs"  class="fpEditTblTxt" size="3" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell2.childNodes[0].value = savvalue["txtFldFnaInvGoalYrs"];

						savrowobj.onclick = function() {
							selectSingleRow(this);
						};
					}
				}

				if (tab == "ARCH_FATCATAXDET_DATA") {
					var fatcatblobj = document.getElementById("fnaFatcaTbl");
					var fatcatbodyobj = fatcatblobj.tBodies[0];

					var fatcaData = value;

					for ( var tax in fatcaData) {

						var fatcarowlen = fatcatbodyobj.rows.length;

						var fatcarowobj = fatcatbodyobj.insertRow(fatcarowlen);

						var cell0 = fatcarowobj.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<select name="txtFldFnaFatcaCntry" class="kycSelect"></select>';

						var cell1 = fatcarowobj.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaFatcaTaxNo" class="fpEditTblTxt"  maxlength="20"	/>'+
						'<input type = "hidden"  name="txtFldFnaFatcaMode" value="'+ INS_MODE+ '" />'+
						'<input type = "hidden"  name="txtFldFnaFatcaId"  />';
						cell1.childNodes[0].value = fatcaData[tax]["txtFldFnaTaxRefNo"];
						cell1.childNodes[2].value = fatcaData[tax]["txtFldFnaTaxId"];

						comboMaker(cell0, contryArr,fatcaData[tax]["selFnaTaxCntry"], false);

						var cell2 = fatcarowobj.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<select name="txtFldFnaFatcaTaxReason" class="kycSelect"></select>';

						var cell3 = fatcarowobj.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaFatcaReasonBDets" class="fpEditTblTxt"  maxlength="800"/>'+
						'<input type="hidden" name="txtFldFldFatcaFor" value="CLIENT"/>';

						comboMaker(cell2, tax_reasonArr,fatcaData[tax]["txtFldFnaNoTaxReason"], true);
						cell3.childNodes[0].value = fatcaData[tax]["txtFldFnaReasonDets"];

						fatcarowobj.onclick = function() {
							selectSingleRow(this);
						};


						var totalcell = fatcarowobj.cells.length;
						for(var cel=0;cel<totalcell;cel++){
							if(fatcarowobj.cells[cel].childNodes[0]){
								fatcarowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
									e = e || event;
									checkChanges(this);
								},false);
							}
						}
					}
				}

				if(tab == "ARCH_FATCATAXDETSPS_DATA"){

					$("#fnaFatcaTblSps tbody tr").remove();

					var fatcatblobjSps = document.getElementById("fnaFatcaTblSps");
					var fatcatbodyobjSps = fatcatblobjSps.tBodies[0];

					var fatcaDataSps = value;

					for ( var taxs in fatcaDataSps) {

						var fatcarowlens = fatcatbodyobjSps.rows.length;

						var fatcarowobjs = fatcatbodyobjSps.insertRow(fatcarowlens);

						var cell0 = fatcarowobjs.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<select name="txtFldFnaFatcaCntry" class="kycSelect"></select>';

						var cell1 = fatcarowobjs.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaFatcaTaxNo" class="fpEditTblTxt"  maxlength="20"	/>'+
						'<input type = "hidden"  name="txtFldFnaFatcaMode" value="'+ INS_MODE+ '" />'+
						'<input type = "hidden"  name="txtFldFnaFatcaId"  />';
						cell1.childNodes[0].value = fatcaDataSps[taxs]["txtFldFnaTaxRefNo"];
						cell1.childNodes[2].value = fatcaDataSps[taxs]["txtFldFnaTaxId"];

						comboMaker(cell0, contryArr,fatcaDataSps[taxs]["selFnaTaxCntry"], false);

						var cell2 = fatcarowobjs.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<select name="txtFldFnaFatcaTaxReason" class="kycSelect"></select>';

						var cell3 = fatcarowobjs.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaFatcaReasonBDets" class="fpEditTblTxt"  maxlength="800"/>'+
						'<input type="hidden" name="txtFldFldFatcaFor" value="SPOUSE"/>';

						comboMaker(cell2, tax_reasonArr,fatcaDataSps[taxs]["txtFldFnaNoTaxReason"], true);
						cell3.childNodes[0].value = fatcaDataSps[taxs]["txtFldFnaReasonDets"];

						fatcarowobjs.onclick = function() {
							selectSingleRow(this);
						};

						var totalcell = fatcarowobjs.cells.length;
						for(var cel=0;cel<totalcell;cel++){
							if(fatcarowobjs.cells[cel].childNodes[0]){
								fatcarowobjs.cells[cel].childNodes[0].addEventListener("change",function(e){
									e = e || event;
									checkChanges(this);
								},false);
							}
						}
					}

				}

				if (tab == "ARCH_REPPOLDETS_DATA") {
					$("#fnaWithdrawPolTble tbody tr").remove();
					var eptblobj = document.getElementById("fnaWithdrawPolTble");
					var eptbodyobj = eptblobj.tBodies[0];

					var repPolData = value;

					for ( var ep in repPolData) {

						var eprl = eptbodyobj.rows.length;

						var eprowobj = eptbodyobj.insertRow(eprl);

						var cell0 = eprowobj.insertCell(0);
						cell0.style.textAlign = "center";
						cell0.style.width="55px";
						cell0.innerHTML ='<input type = "text" size="3" name="txtFldFnaWithdrawSiNo" readOnly="true" class="fpEditTblTxt" style="text-align:center" value="'+(eprl+1)+ '" />';

						var cell01 = eprowobj.insertCell(1);
						cell01.style.textAlign = "left";
						cell01.innerHTML = '<select name="selFnaExistPolFor" class="kycSelect"  style="width:85px"><option value="Self">Self</option><option value="Spouse">Spouse/Client</option></select>';
						cell01.childNodes[0].value =repPolData[ep]["txtFldFnaExistPolFor"];

						var cell02 = eprowobj.insertCell(2);
						cell02.style.textAlign = "left";
						cell02.innerHTML = '<input type = "text"  name="txtFldFnaExistPolDate"  maxlength="10" onblur="CheckDate(this,NULL);"  class="fpEditTblTxt" style="width:78px" />'
							+ '<input type="hidden" id="hFnaLiCalMode" name="hFnaExistPolMode" value="'+ INS_MODE+ '" />'
							+ '<img src="images/cal.png" title="Date" alt="Date" height="16px" width="16px" onclick="getCalendarInTbl(this,event)" style="cursor:pointer">';
						cell02.childNodes[0].value =repPolData[ep]["txtFldFnaExistPolDate"];

						var cell03 = eprowobj.insertCell(3);
						cell03.style.textAlign = "left";
						cell03.innerHTML = '<input type="text" name="selFnaExistPolPrin" class="fpEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" maxlength="60"/>';

						var cell1 = eprowobj.insertCell(4);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaExistPolProdName" class="fpEditTblTxt"  maxlength="300"/>'+
						'<input type = "hidden"  name="txtFldFnaExistPolMode" value="'+ INS_MODE+ '" />'+
						'<input type = "hidden"  name="txtFldFnaExistPolId"  />'+
						 '<input type = "hidden"  name="txtFldFnaExistPolNo" class="fpEditTblTxt"  maxlength="40"/>';

						cell1.childNodes[0].value = repPolData[ep]["txtFldFnaRepProdName"];
						cell1.childNodes[2].value = repPolData[ep]["txtFldFnaRepId"];
						cell1.childNodes[3].value = repPolData[ep]["selFnaRepPolicyNo"];


//						comboMakerById(cell03.childNodes[0], prinIdArr,repPolData[ep]["selFnaRepPrinId"],true);
						cell03.childNodes[0].value=repPolData[ep]["selFnaRepPrinId"];

						eprowobj.onclick = function() {
							selectSingleRow(this);
						};

						var totalcell = eprowobj.cells.length;
						for(var cel=0;cel<totalcell;cel++){
							if(eprowobj.cells[cel].childNodes[0]){
								eprowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
									e = e || event;
									checkChanges(this);
								},false);
							}
						}
					}
				}

				if (tab == "ARCH_FNAFLOWDET_DATA") {

					var fnaFlowDetTbl = document.getElementById("fnaFlowDetTbl");
					var flowbodyobj = fnaFlowDetTbl.tBodies[0];
					var flowData = value;

					for ( var flow in flowData) {

						// if(flowData.hasOwnProperty(flow)){alert(flowData)}
						var flowvalue = flowData[flow];

						var flowrlen = flowbodyobj.rows.length;
						var rowobj = flowbodyobj.insertRow(flowrlen);

						var cell0 = rowobj.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<select name="selFlowType" class="kycSelect"></select><input type = "hidden"  name="txtFldFnaFlowMode" value="'
								+ INS_MODE
								+ '" /><input type = "hidden"  name="txtFldFnaFlowId"  />';
						cell0.childNodes[2].value = flowvalue["txtFldFnaFlowId"];

						var cell1 = rowobj.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaFundSrc" class="fpEditTblTxt" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	maxlength="30"  />';
						cell1.childNodes[0].value = flowvalue["txtFldFnaFundSrc"];

						var cell2 = rowobj.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type="text" name = "txtFldFnaFundWhen" class="fpEditTblTxt"  maxlength="3" size="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell2.childNodes[0].value = flowvalue["txtFldFnaFundWhen"];

						var cell3 = rowobj.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaFundAmt" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"  />';
						cell3.childNodes[0].value = flowvalue["txtFldFnaFundAmt"];

						var cell4 = rowobj.insertCell(4);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<input type = "text"  name="txtFldFnaFundRemark" class="fpEditTblTxt"  maxlength="300" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	  />';
						cell4.childNodes[0].value = flowvalue["txtFldFnaFundRemark"];

						comboMaker(cell0, flowTypeArr,
								flowvalue["selFlowType"], true);

						rowobj.onclick = function() {
							selectSingleRow(this);
						};

					}
				}

				if (tab == "ARCH_EXPNTRDET_DATA") {

					jsonDataPopulate(value, 'ARCH_EXPNTRDET_DATA');
					// added by johnson on 20052015 for adding the values during
					// population
					calcSum(null, Ann_Exp.SUMOF_ANNEXP_SELF);
					calcSum(null, Ann_Exp.SUMOF_ANNEXP_SPS);
					calcSum(null, Ann_Exp.SUMOF_ANNEXP_FAM);
				}

				if (tab == "ARCH_CONTGNYDET_DATA") {
					jsonDataPopulate(value, tab);
				}

				if (tab == "ARCH_DEPENTCONTDET_DATA") {

					var depntContTbl = document
							.getElementById("fnadepntfinanceTbl");
					var depentbodyobj = depntContTbl.tBodies[0];
					var depntContData = value;

					for ( var cdep in depntContData) {

						var cdeprowobj = depentbodyobj.rows.length;

						var cdepvalue = depntContData[cdep];

						var cdeprow = depentbodyobj.insertRow(cdeprowobj);

						var cell0 = cdeprow.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text" class="writeModeTextbox"  name="txtFldFnaContDepName" maxlength="60" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type = "hidden"  name="txtFldFnaContDepMode" value="'
								+ INS_MODE
								+ '" /><input type = "hidden"  name="txtFldFnaContDepId"  />';
						cell0.childNodes[0].value = cdepvalue["txtFldFnaContDepName"];
						cell0.childNodes[2].value = cdepvalue["txtFldFnaContDepId"];

						var cell1 = cdeprow.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text" class="writeModeTextbox"  name="txtFldFnaContDepAge" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';
						cell1.childNodes[0].value = cdepvalue["txtFldFnaContDepAge"];

						var cell2 = cdeprow.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type="text" class="writeModeTextbox" name = "txtFldFnaContDepYrs" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';
						cell2.childNodes[0].value = cdepvalue["txtFldFnaContDepYrs"];

						var cell3 = cdeprow.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text" class="writeModeTextbox" name="txtFldFnaContLiving" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  />';
						cell3.childNodes[0].value = cdepvalue["txtFldFnaContLiving"];

						var cell4 = cdeprow.insertCell(4);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<select name="selFnaContDepRel" class="selectbox"></select>';

						var cell5 = cdeprow.insertCell(5);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text" class="writeModeTextbox" name="txtFldFnaContDepTerYr" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell5.childNodes[0].value = cdepvalue["txtFldFnaContDepTerYr"];

						var cell6 = cdeprow.insertCell(6);
						cell6.style.textAlign = "left";
						cell6.innerHTML = '<input type = "text" class="writeModeTextbox" name="txtFldFnaContDepTerYrEdn" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell6.childNodes[0].value = cdepvalue["txtFldFnaContDepTerYrEdn"];

						var cell7 = cdeprow.insertCell(7);
						cell7.style.textAlign = "left";
						cell7.innerHTML = '<input type = "text"class="writeModeTextbox"  name="txtFldFnaContDepAnnlCost" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
						cell7.childNodes[0].value = cdepvalue["txtFldFnaContDepAnnlCost"];

						var cell8 = cdeprow.insertCell(8);
						cell8.style.textAlign = "left";
						cell8.innerHTML = '<input type="text" class="writeModeTextbox" name = "txtFldFnaContDepFund" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
						cell8.childNodes[0].value = cdepvalue["txtFldFnaContDepFund"];

						var cell9 = cdeprow.insertCell(9);
						cell9.style.textAlign = "left";
						cell9.innerHTML = '<input type = "text" class="writeModeTextbox" name="txtFldFnaContDepRb" maxlength="20" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';
						cell9.childNodes[0].value = cdepvalue["txtFldFnaContDepRb"];

						var cell10 = cdeprow.insertCell(10);
						cell10.style.textAlign = "left";
						cell10.innerHTML = '<input type = "text" class="writeModeTextbox" name="txtFldFnaContDepWard" maxlength="20" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';
						cell10.childNodes[0].value = cdepvalue["txtFldFnaContDepWard"];

						var cell11 = cdeprow.insertCell(11);
						cell11.style.textAlign = "left";
						cell11.innerHTML = '<select name="selFnaContDepHosp" class="selectbox"></select>';

						comboMaker(cell4, relArray,
								cdepvalue["selFnaContDepRel"], false);
						comboMaker(cell11, hospTypeArr,
								cdepvalue["selFnaContDepHosp"], false);

						cdeprow.onclick = function() {
							selectSingleRow(this);
						};

					}

				}

				if (tab == "ARCH_HLTHINSDET_DATA") {

					var hlthtblobj = document.getElementById("fnahlthinsTbl");
					var hlthtbodyobj = hlthtblobj.tBodies[0];

					var hlthData = value;

					for ( var hlth in hlthData) {

						var hlthrowobj = hlthtbodyobj.rows.length;

						var hlthvalue = hlthData[hlth];

						var hlthrow = hlthtbodyobj.insertRow(hlthrowobj);

						var cell0 = hlthrow.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text"  name="txtFldFnaHiPolType" class="fpEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	 maxlength="20"/><input type = "hidden"  name="txtFldFnaHiMode" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"value="'
								+ INS_MODE
								+ '" /><input type = "hidden"  name="txtFldFnahiId"/>';
						cell0.childNodes[0].value = hlthvalue["txtFldFnaHiPolType"];
						cell0.childNodes[2].value = hlthvalue["txtFldFnahiId"];

						var cell1 = hlthrow.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaHiInsured" class="fpEditTblTxt"  maxlength="60" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	 /><input type="hidden" name="txtFldHiAppId"/>';
						cell1.childNodes[0].value = hlthvalue["txtFldFnaHiInsured"];
						cell1.childNodes[1].value = hlthvalue["txtFldFnaHiAppId"];

						var cell2 = hlthrow.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type="text" name = "txtFldFnaHiBenfType" class="fpEditTblTxt"  maxlength="150" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	 />';
						cell2.childNodes[0].value = hlthvalue["txtFldFnaHiBenfType"];

						var cell3 = hlthrow.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaHiAmt" class="fpEditTblTxt" 	 maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell3.childNodes[0].value = hlthvalue["txtFldFnaHiAmt"];

						var cell4 = hlthrow.insertCell(4);
						cell4.style.textAlign = "left";
						// cell4.innerHTML ='<input type = "text"
						// name="txtFldFnaHiExpiry" class="fpEditTblTxt"
						//  maxlength="10" />';
						cell4.innerHTML = '<input type = "text"  name="txtFldFnaHiExpiry" id="txtFldFnaHiExpiry" maxlength="10" onblur="CheckDate(this,NULL);"  class="fpEditTblTxt" style="width:75%" />'
								+ '<input type="hidden" id="hFnaLiCalMode" name="hFnaHiExp" value="'
								+ INS_MODE
								+ '" />'
								+ '<img src="images/cal.png" title="Date" alt="Date" height="16px" width="16px" onclick="getCalendarInTbl(this,event)" style="cursor:pointer">';
						cell4.childNodes[0].value = hlthvalue["txtFldFnaHiExpiry"];

						var cell5 = hlthrow.insertCell(5);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text"  name="txtFldFnaHiAnnlPrem" class="fpEditTblTxt" 	 maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell5.childNodes[0].value = hlthvalue["txtFldFnaHiAnnlPrem"];

						var cell6 = hlthrow.insertCell(6);
						cell6.style.textAlign = "left";
						cell6.innerHTML = '<input type = "text"  name="txtFldFnaHiRemark" class="fpEditTblTxt" 	onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" maxlength="300"/>';
						cell6.childNodes[0].value = hlthvalue["txtFldFnaHiRemark"];

						hlthrow.onclick = function() {
							selectSingleRow(this);
						};

					}

				}

				if (tab == "ARCH_PERSPRIOINCSRC_DATA") {
					jsonDataPopulate(value, tab);
					calcSum(null, tot_SrcOfIncm.SRCOFINCM_SELF);
					// calcSum(null, tot_SrcOfIncm.SRCOFINCM_SELF_INCR);
					calcSum(null, tot_SrcOfIncm.SRCOFINCM_SELF_PRD);
					calcSum(null, tot_SrcOfIncm.SRCOFINCM_SPS);
					// calcSum(null, tot_SrcOfIncm.SRCOFINCM_SPS_INCR);
					calcSum(null, tot_SrcOfIncm.SRCOFINCM_SPS_PRD);
				}

				if (tab == "ARCH_CPFDET_DATA") {
					jsonDataPopulate(value, tab);
					// added by johnson on 20052015 for adding the values during
					// population
					calcSum(null, tot_CPF.SUMOF_CPF_SELFCR);
					calcSum(null, tot_CPF.SUMOF_CPF_SELFAC);
					calcSum(null, tot_CPF.SUMOF_CPF_SPSCR);
					calcSum(null, tot_CPF.SUMOF_CPF_SPSAC);
				}

				if (tab == "ARCH_FINLIABDET_DATA") {
					jsonDataPopulate(value, tab);
					// added by johnson on 20052015 for adding the values during
					// population
					calcSum(null, tot_FincLiab.SUMOF_FINLIAB_SELF);
					calcSum(null, tot_FincLiab.SUMOF_FINLIAB_SPS);
				}

				if (tab == "ARCH_CURRASSPDET_DATA") {
					jsonDataPopulate(value, tab);
				}

				if (tab == "ARCH_RETPLANDET_DATA") {
					jsonDataPopulate(value, tab);
					// document.getElementById("txtFldRPMssYrSelf").value =
					// "62";
					// document.getElementById("txtFldRPMssYrSps").value = "62";
					// document.getElementById("txtFldRPMssYrFam").value = "62";
				}

				if (tab == "ARCH_PROPOWNDET_DATA") {

					var proptbl = document.getElementById("fnaPropOwnTbl");
					var proptbodyobj = proptbl.tBodies[0];

					var propData = value;

					for ( var prop in propData) {

						var proprowlen = proptbodyobj.rows.length;

						var propvalue = propData[prop];

						var proprow = proptbodyobj.insertRow(proprowlen);

						var cell0 = proprow.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text"  name="txtFldFnaPropOwner" class="fpEditTblTxt" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" maxlength="60"/><input type = "hidden"  name="txtFldFnaPropOwnMode" value="'
								+ INS_MODE
								+ '" /><input type = "hidden"  name="txtFldFnaPropOwnId"/>';
						cell0.childNodes[0].value = propvalue["txtFldFnaPropOwner"];
						cell0.childNodes[2].value = propvalue["txtFldFnaPropOwnId"];

						var cell1 = proprow.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaPropMktVal" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell1.childNodes[0].value = propvalue["txtFldFnaPropMktVal"];

						var cell2 = proprow.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type="text" name = "txtFldFnaPropOutsd" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell2.childNodes[0].value = propvalue["txtFldFnaPropOutsd"];

						var cell3 = proprow.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaPropObj" class="fpEditTblTxt"  maxlength="60"/>';
						cell3.childNodes[0].value = propvalue["txtFldFnaPropObj"];

						var cell4 = proprow.insertCell(4);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<input type = "text"  name="txtFldFnaPropApprc" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell4.childNodes[0].value = propvalue["txtFldFnaPropApprc"];

						var cell5 = proprow.insertCell(5);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text"  name="txtFldFnaPropOsLoan" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell5.childNodes[0].value = propvalue["txtFldFnaPropOsLoan"];

						var cell6 = proprow.insertCell(6);
						cell6.style.textAlign = "left";
						cell6.innerHTML = '<input type = "text"  name="txtFldFnaPropLoanCash" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell6.childNodes[0].value = propvalue["txtFldFnaPropLoanCash"];

						var cell7 = proprow.insertCell(7);
						cell7.style.textAlign = "left";
						cell7.innerHTML = '<input type = "text"  name="txtFldFnaPropLoanCpf" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell7.childNodes[0].value = propvalue["txtFldFnaPropLoanCpf"];

						var cell8 = proprow.insertCell(8);
						cell8.style.textAlign = "left";
						cell8.innerHTML = '<input type="text" name = "txtFldFnaPropYrs2pay" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell8.childNodes[0].value = propvalue["txtFldFnaPropYrs2pay"];

						var cell9 = proprow.insertCell(9);
						cell9.style.textAlign = "left";
						cell9.innerHTML = '<input type = "text"  name="txtFldFnaPropSoldRet" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell9.childNodes[0].value = propvalue["txtFldFnaPropSoldRet"];

						proprow.onclick = function() {
							selectSingleRow(this);
						};

						getTotalValue(
								proptbodyobj.rows[proprowlen].cells[1].childNodes[0],
								"fnaPropOwnTbl");
						getTotalValue(
								proptbodyobj.rows[proprowlen].cells[2].childNodes[0],
								"fnaPropOwnTbl");
						getTotalValue(
								proptbodyobj.rows[proprowlen].cells[6].childNodes[0],
								"fnaPropOwnTbl");
						getTotalValue(
								proptbodyobj.rows[proprowlen].cells[7].childNodes[0],
								"fnaPropOwnTbl");
						getTotalValue(
								proptbodyobj.rows[proprowlen].cells[8].childNodes[0],
								"fnaPropOwnTbl");
						getTotalValue(
								proptbodyobj.rows[proprowlen].cells[9].childNodes[0],
								"fnaPropOwnTbl");

					}
				}

				if (tab == "ARCH_VEHIOWNDET_DATA") {

					var vehitblobj = document.getElementById("fnaVehiOwnTbl");
					var vehitbodyobj = vehitblobj.tBodies[0];
					var vehiData = value;

					for ( var vehi in vehiData) {

						var vehirowlen = vehitbodyobj.rows.length;

						var vehivalue = vehiData[vehi];

						var vehirow = vehitbodyobj.insertRow(vehirowlen);

						var cell0 = vehirow.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text"  name="txtFldFnaVehiOwner" class="fpEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"maxlength="60"/><input type = "hidden"  name="txtFldFnaVehiMode" value="'
								+ INS_MODE
								+ '"/><input type="hidden" name="txtFldFnaVehiId"/>';
						cell0.childNodes[0].value = vehivalue["txtFldFnaVehiOwner"];
						cell0.childNodes[2].value = vehivalue["txtFldFnaVehiId"];

						var cell1 = vehirow.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaVehiMktval" onchange=getTotalValue(this,"fnaVehiOwnTbl")  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell1.childNodes[0].value = vehivalue["txtFldFnaVehiMktval"];

						var cell2 = vehirow.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type="text" name = "txtFldFnaVehiLoanVal" onchange=getTotalValue(this,"fnaVehiOwnTbl")  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell2.childNodes[0].value = vehivalue["txtFldFnaVehiLoanVal"];

						var cell3 = vehirow.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaVehiPerd" onchange=getTotalValue(this,"fnaVehiOwnTbl")  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell3.childNodes[0].value = vehivalue["txtFldFnaVehiPerd"];

						var cell4 = vehirow.insertCell(4);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<input type = "text"  name="txtFldFnaVehiRemarks" class="fpEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"maxlength="300"/>';
						cell4.childNodes[0].value = vehivalue["txtFldFnaVehiRemarks"];

						vehirow.onclick = function() {
							selectSingleRow(this);
						};

						getTotalValue(
								vehitbodyobj.rows[vehirowlen].cells[1].childNodes[0],
								"fnaVehiOwnTbl");
						getTotalValue(
								vehitbodyobj.rows[vehirowlen].cells[2].childNodes[0],
								"fnaVehiOwnTbl");
						getTotalValue(
								vehitbodyobj.rows[vehirowlen].cells[3].childNodes[0],
								"fnaVehiOwnTbl");
					}
				}

				if (tab == "ARCH_INVSTDET_DATA") {
					jsonDataPopulate(value, tab);
				}

				if (tab == "ARCH_CASHASST_DATA") {
					jsonDataPopulate(value, tab);
					// added by johnson on 20052015 for adding the values during
					// population
					calcSum(null, tot_CashAsst.SUMOF_CASHASST_SELF);
					calcSum(null, tot_CashAsst.SUMOF_CASHASST_SPS);
					calcSum(null, tot_CashAsst.SUMOF_CASHASST_JOIN);
				}

				if (tab == "ARCH_OTHASST_DATA") {
					jsonDataPopulate(value, tab);
					// added by johnson on 20052015 for adding the values during
					// population
					calcSum(null, tot_OthrAsst.SUMOF_OTHASST_SELF);
					calcSum(null, tot_OthrAsst.SUMOF_OTHASST_SPS);
					calcSum(null, tot_OthrAsst.SUMOF_OTHASST_JOIN);
					calcSum(null, tot_OthrAsst.SUMOF_OTHASST_LOAN);
				}

				if (tab == "ARCH_LIFEINSDET_DATA") {

					var lifeInsData = value;

					var lifetblobj = document.getElementById("fnaLIPlanTbl");
					var lifetbodyobj = lifetblobj.tBodies[0];

					for ( var life in lifeInsData) {

						var liferowlen = lifetbodyobj.rows.length;

						var liferowobj = lifetbodyobj.insertRow(liferowlen);

						var cell0 = liferowobj.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text"  name="txtFldFnaLiOwner" class="fpEditTblTxt"   onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"maxlength="60"/><input type = "hidden"  name="txtFldFnaLiMode" value="'
								+ INS_MODE
								+ '" /><input type = "hidden"  name="txtFldFnaLiId"  />';

						var cell1 = liferowobj.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<input type = "text"  name="txtFldFnaLiAssured" class="fpEditTblTxt"   onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"maxlength="75"/>';

						var cell2 = liferowobj.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<select name = "txtFldFnaLiComp" class="kycSelect" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"/>';
						// cell2.childNodes[0] has been changed from text field
						// to select combo box by johnson on 28042015

						var cell3 = liferowobj.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaLiPolNo"   class="fpEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"maxlength="20"/><input type="hidden" name="txtFldFnaLiAppId"/>';

						var cell4 = liferowobj.insertCell(4);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<input type="text" name="selFnaLiPolType"   class="fpEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"maxlength="12" />';
						// '<select name="selFnaLiPolType"
						// class="kycSelect"></select>'; //select element has
						// been to text field by johnson on 07052015

						var cell5 = liferowobj.insertCell(5);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text"  name="txtFldFnaLiDthBenf"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell6 = liferowobj.insertCell(6);
						cell6.style.textAlign = "left";
						cell6.innerHTML = '<input type = "text"  name="txtFldFnaLiMiCover"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell7 = liferowobj.insertCell(7);
						cell7.style.textAlign = "left";
						cell7.innerHTML = '<input type = "text"  name="txtFldFnaLiCashVal"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell8 = liferowobj.insertCell(8);
						cell8.style.textAlign = "left";
						cell8.innerHTML = '<input type="text" name = "txtFldFnaLiMatrVal" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell9 = liferowobj.insertCell(9);
						cell9.style.textAlign = "left";
						cell9.innerHTML = '<input type = "text"  name="txtFldFnaLiOsLoan" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell10 = liferowobj.insertCell(10);
						cell10.style.textAlign = "left";
						cell10.innerHTML = '<input type = "text"  name="txtFldFnaLiMatrDate" id="txtFldFnaLiMatrDate" maxlength="10" onblur="CheckDate(this,NULL);"  class="fpEditTblTxt" style="width:75%" />'
								+ '<input type="hidden" id="hFnaLiCalMode" name="hFnaLiCalMode" value="'
								+ INS_MODE
								+ '" />'
								+ '<img src="images/cal.png" title="Date" alt="Date" height="16px" width="16px" onclick="getCalendarInTbl(this,event)" style="cursor:pointer">';
						// datePickerCall(cell10.childNodes[0].name);

						var cell11 = liferowobj.insertCell(11);
						cell11.style.textAlign = "left";
						cell11.innerHTML = '<input type = "text"  name="txtFldFnaLiValEdnCash"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						// ---

						var cell12 = liferowobj.insertCell(12);
						cell12.style.textAlign = "left";
						cell12.innerHTML = '<input type = "text"  name="txtFldFnaLiValRpCash"   class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell13 = liferowobj.insertCell(13);
						cell13.style.textAlign = "left";
						cell13.innerHTML = '<input type = "text"  name="txtFldFnaLiValCpf"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell14 = liferowobj.insertCell(14);
						cell14.style.textAlign = "left";
						cell14.innerHTML = '<input type = "text"  name="txtFldFnaLiAnnlPrem"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell15 = liferowobj.insertCell(15);
						cell15.style.textAlign = "left";
						cell15.innerHTML = '<input type = "text"  name="txtFldFnaLiTpdPay1"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell16 = liferowobj.insertCell(16);
						cell16.style.textAlign = "left";
						cell16.innerHTML = '<input type = "text"  name="txtFldFnaLiYrsto1"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';

						var cell17 = liferowobj.insertCell(17);
						cell17.style.textAlign = "left";
						cell17.innerHTML = '<input type = "text"  name="txtFldFnaLiYr1"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

						var cell18 = liferowobj.insertCell(18);
						cell18.style.textAlign = "left";
						cell18.innerHTML = '<input type = "text"  name="txtFldFnaLiTpdPay2"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell19 = liferowobj.insertCell(19);
						cell19.style.textAlign = "left";
						cell19.innerHTML = '<input type = "text"  name="txtFldFnaLiYrto2"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

						var cell20 = liferowobj.insertCell(20);
						cell20.style.textAlign = "left";
						cell20.innerHTML = '<input type = "text"  name="txtFldFnaLiYr2"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

						var cell21 = liferowobj.insertCell(21);
						cell21.style.textAlign = "left";
						cell21.innerHTML = '<input type = "text"  name="txtFldFnaLiTpdPay3"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell22 = liferowobj.insertCell(22);
						cell22.style.textAlign = "left";
						cell22.innerHTML = '<input type = "text"  name="txtFldFnaLiYrto3"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

						var cell23 = liferowobj.insertCell(23);
						cell23.style.textAlign = "left";
						cell23.innerHTML = '<input type = "text"  name="txtFldFnaLiYr3"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';

						var cell24 = liferowobj.insertCell(24);
						cell24.style.textAlign = "left";
						cell24.innerHTML = '<input type = "text"  name="txtFldFnaLiCompTpd"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

						var cell25 = liferowobj.insertCell(25);
						cell25.style.textAlign = "left";
						cell25.innerHTML = '<input type = "text"  name="txtFldFnaLiRem"  class="fpEditTblTxt"  maxlength="300" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

						comboMakerById(cell2.childNodes[0], prinIdArr,
								lifeInsData[life]["txtFldFnaLiComp"], true);// added
						// by
						// johnson
						// on
						// 28042015

						cell0.childNodes[0].value = lifeInsData[life]["txtFldFnaLiOwner"];
						cell0.childNodes[2].value = lifeInsData[life]["txtFldFnaLiId"];
						cell1.childNodes[0].value = lifeInsData[life]["txtFldFnaLiAssured"];
						cell2.childNodes[0].value = lifeInsData[life]["txtFldFnaLiComp"];
						cell3.childNodes[0].value = lifeInsData[life]["txtFldFnaLiPolNo"];
						cell3.childNodes[1].value = lifeInsData[life]["txtFldFnaLiAppId"];
						cell4.childNodes[0].value = lifeInsData[life]["selFnaLiPolType"];// added
						// by
						// johnson
						// on
						// 07052015
						cell5.childNodes[0].value = lifeInsData[life]["txtFldFnaLiDthBenf"];

						cell6.childNodes[0].value = lifeInsData[life]["txtFldFnaLiMiCover"];
						cell7.childNodes[0].value = lifeInsData[life]["txtFldFnaLiCashVal"];
						cell8.childNodes[0].value = lifeInsData[life]["txtFldFnaLiMatrVal"];
						cell9.childNodes[0].value = lifeInsData[life]["txtFldFnaLiOsLoan"];
						cell10.childNodes[0].value = lifeInsData[life]["txtFldFnaLiMatrDate"];
						cell11.childNodes[0].value = lifeInsData[life]["txtFldFnaLiValEdnCash"];

						cell12.childNodes[0].value = lifeInsData[life]["txtFldFnaLiValRpCash"];
						cell13.childNodes[0].value = lifeInsData[life]["txtFldFnaLiValCpf"];
						cell14.childNodes[0].value = lifeInsData[life]["txtFldFnaLiAnnlPrem"];
						cell15.childNodes[0].value = lifeInsData[life]["txtFldFnaLiTpdPay1"];
						cell16.childNodes[0].value = lifeInsData[life]["txtFldFnaLiYrsto1"];
						cell17.childNodes[0].value = lifeInsData[life]["txtFldFnaLiYr1"];

						cell18.childNodes[0].value = lifeInsData[life]["txtFldFnaLiTpdPay2"];
						cell19.childNodes[0].value = lifeInsData[life]["txtFldFnaLiYrto2"];
						cell20.childNodes[0].value = lifeInsData[life]["txtFldFnaLiYr2"];
						cell21.childNodes[0].value = lifeInsData[life]["txtFldFnaLiTpdPay3"];
						cell22.childNodes[0].value = lifeInsData[life]["txtFldFnaLiYrto3"];
						cell23.childNodes[0].value = lifeInsData[life]["txtFldFnaLiYr3"];

						cell24.childNodes[0].value = lifeInsData[life]["txtFldFnaLiCompTpd"];
						cell25.childNodes[0].value = lifeInsData[life]["txtFldFnaLiRem"];

						// comboMaker(cell4,poltypeArr,lifeInsData[life]["selFnaLiPolType"],true);

						liferowobj.onclick = function() {
							selectSingleRow(this);
						};

					}

				}

				if (tab == "ARCH_INVOWNPLANDET_DATA") {

					var invstdata = value;

					var invsttblobj = document
							.getElementById("fnaInvsetPlanTbl");
					var invsttbodyobj = invsttblobj.tBodies[0];

					for ( var inv in invstdata) {

						var invstrowlen = invsttbodyobj.rows.length;

						var invstrowobj = invsttbodyobj.insertRow(invstrowlen);

						var cell0 = invstrowobj.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text"  name="txtFldFnaInvType"  class="fpEditTblTxt" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	maxlength="20" /><input type = "hidden"  name="txtFldFnaInvmode" value="'
								+ INS_MODE
								+ '" /><input type = "hidden"  name="txtFldFnaInvId"  />';
						cell0.childNodes[0].value = invstdata[inv]["txtFldFnaInvType"];
						cell0.childNodes[2].value = invstdata[inv]["txtFldFnaInvId"];

						var cell1 = invstrowobj.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<select name="txtFldFnaInvInstName" class="kycSelect" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" >';

						// '<input type = "text" name="txtFldFnaInvInstName"
						//  class="fpEditTblTxt"
						//  onclick="hideTooltip()"
						// onmouseover="showTooltip(event,this.value);"
						// onmouseout="hideTooltip();"maxlength="60" />';

						var cell2 = invstrowobj.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type="text" name = "txtFldFnaInvDesc" class="fpEditTblTxt" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	maxlength="150" />';
						cell2.childNodes[0].value = invstdata[inv]["txtFldFnaInvDesc"];

						var cell3 = invstrowobj.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaInvAmt"   class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"	/>';
						cell3.childNodes[0].value = invstdata[inv]["txtFldFnaInvAmt"];

						var cell4 = invstrowobj.insertCell(4);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<select name="selFnaInvPayMeth" class="kycSelect"></select>';

						var cell5 = invstrowobj.insertCell(5);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text"  name="txtFldFnaInvCurrBid"  class="fpEditTblTxt" 	maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell5.childNodes[0].value = invstdata[inv]["txtFldFnaInvCurrBid"];

						var cell6 = invstrowobj.insertCell(6);
						cell6.style.textAlign = "left";
						cell6.innerHTML = '<input type = "text" id="txtFldFnaInvDate"  name="txtFldFnaInvDate"  maxlength="10" onblur="CheckDate(this,NULL);" class="fpEditTblTxt" style="width:75%;"	 />'
								+ '<input type="hidden" id="htxtFldFnaInvCalMode" name="htxtFldFnaInvCalMode" value="'
								+ INS_MODE
								+ '" />'
								+ '<img src="images/cal.png" title="Date" alt="Date" height="16px" width="16px" onclick="getCalendarInTbl(this,event)" style="cursor:pointer">';
						cell6.childNodes[0].value = invstdata[inv]["txtFldFnaInvDate"];
						// datePickerCall(cell6.childNodes[0].name);

						var cell7 = invstrowobj.insertCell(7);
						cell7.style.textAlign = "left";
						cell7.innerHTML = '<input type = "text"  name="txtFldFnaInnUnitAlloc"  class="fpEditTblTxt" 	maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell7.childNodes[0].value = invstdata[inv]["txtFldFnaInnUnitAlloc"];

						var cell8 = invstrowobj.insertCell(8);
						cell8.style.textAlign = "left";
						cell8.innerHTML = '<input type="text" name = "txtFldFnaIncCurrNav" class="fpEditTblTxt" 	maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell8.childNodes[0].value = invstdata[inv]["txtFldFnaIncCurrNav"];

						var cell9 = invstrowobj.insertCell(9);
						cell9.style.textAlign = "left";
						cell9.innerHTML = '<input type = "text"  name="txtFldFnaInvAnnlTopup"  class="fpEditTblTxt" 	maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell9.childNodes[0].value = invstdata[inv]["txtFldFnaInvAnnlTopup"];

						var cell10 = invstrowobj.insertCell(10);
						cell10.style.textAlign = "left";
						cell10.innerHTML = '<input type = "text"  name="txtFldFnaInvCpfYrtoTopup"  class="fpEditTblTxt" 	maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';
						cell10.childNodes[0].value = invstdata[inv]["txtFldFnaInvCpfYrtoTopup"];

						var cell11 = invstrowobj.insertCell(11);
						cell11.style.textAlign = "left";
						cell11.innerHTML = '<input type = "text"  name="txtFldFnaInvFvTrantoCpf"  class="fpEditTblTxt" 	maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell11.childNodes[0].value = invstdata[inv]["txtFldFnaInvFvTrantoCpf"];

						// --

						var cell12 = invstrowobj.insertCell(12);
						cell12.style.textAlign = "left";
						cell12.innerHTML = '<input type = "text"  name="txtFldFnaInvRpYrtoTopup"  class="fpEditTblTxt" 	maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';
						cell12.childNodes[0].value = invstdata[inv]["txtFldFnaInvRpYrtoTopup"];

						var cell13 = invstrowobj.insertCell(13);
						cell13.style.textAlign = "left";
						cell13.innerHTML = '<input type = "text"  name="txtFldFnaInvCashRetPrcnt"  class="fpEditTblTxt" 	maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell13.childNodes[0].value = invstdata[inv]["txtFldFnaInvCashRetPrcnt"];

						var cell14 = invstrowobj.insertCell(14);
						cell14.style.textAlign = "left";
						cell14.innerHTML = '<input type = "text"  name="txtFldFnaInvFvSrsRet"   class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"	/>';
						cell14.childNodes[0].value = invstdata[inv]["txtFldFnaInvFvSrsRet"];

						var cell15 = invstrowobj.insertCell(15);
						cell15.style.textAlign = "left";
						cell15.innerHTML = '<input type = "text"  name="txtFldFnaInvEdnCashtoTopup"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"	/>';
						cell15.childNodes[0].value = invstdata[inv]["txtFldFnaInvEdnCashtoTopup"];

						var cell16 = invstrowobj.insertCell(16);
						cell16.style.textAlign = "left";
						cell16.innerHTML = '<input type = "text"  name="txtFldFnaInvCashEdnPrcnt"  class="fpEditTblTxt" 	maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell16.childNodes[0].value = invstdata[inv]["txtFldFnaInvCashEdnPrcnt"];

						var cell17 = invstrowobj.insertCell(17);
						cell17.style.textAlign = "left";
						cell17.innerHTML = '<input type = "text"  name="txtFldFnaInvFvChildEdn"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell17.childNodes[0].value = invstdata[inv]["txtFldFnaInvFvChildEdn"];

						var cell18 = invstrowobj.insertCell(18);
						cell18.style.textAlign = "left";
						cell18.innerHTML = '<input type = "text"  name="txtFldFnaInvChilName"  class="fpEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"maxlength="60" 	 />';
						cell18.childNodes[0].value = invstdata[inv]["txtFldFnaInvChilName"];

						var cell19 = invstrowobj.insertCell(19);
						cell19.style.textAlign = "left";
						cell19.innerHTML = '<input type = "text"  name="txtFldFnaInvRem"  class="fpEditTblTxt" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	maxlength="300"/>';
						cell19.childNodes[0].value = invstdata[inv]["txtFldFnaInvRem"];

						comboMakerById(cell1.childNodes[0], fmArr,
								invstdata[inv]["txtFldFnaInvInstName"], true);// added
						// by
						// johnson
						// on
						// 07052015
						cell1.childNodes[0].value = invstdata[inv]["txtFldFnaInvInstName"];
						comboMaker(cell4, fundPayMethArr,
								invstdata[inv]["selFnaInvPayMeth"], true);

						invstrowobj.onclick = function() {
							selectSingleRow(this);
						};

					}
				}

				if (tab == "ARCH_SUMANALY_DATA") {
					jsonDataPopulate(value, tab);
				}

				// if(tab == "ARCH_INTPRTDET_DATA"){
				// jsonDataPopulate(value,tab);
				// }

				if (tab == "ARCH_BENFTPPPEP_DATA") {
					for ( var val in value) {
						jsonDataPopulate(value[val], tab);
					}
				}

				if (tab == "ARCH_ADVRECPRDTPLAN_DATA") {

					var advrecplanData = value;

					if (prdtplanArr.length == 0) {
						// getPrdtPlanName();
					}

					// var advrecplantblobj =
					// document.getElementById("fnaadvrecTbl");
					// var advrectbodyobj = advrecplantblobj.tBodies[0];

					var fnaartuplanTbl = document.getElementById("fnaartuplanTbl");
					var fnaartuplantbodyobj = fnaartuplanTbl.tBodies[0];

					var tblObjClnt = document.getElementById("fnaartuplanTblss");//added by kavi 21/06/2018
					var tBodyObjClnt = tblObjClnt.tBodies[0];
					var strOldValue = "",rowSino= 1,parentSts=true,firstLpSts=true,tempRefId="";

					for ( var advrec in advrecplanData) {
						// if(advrecplanData[advrec]["txtFldFnaScreen"] ==	 "TOPUPPLAN"){

						var planOpt = advrecplanData[advrec]["txtFldRecomPpOpt"];

						var arturl = fnaartuplantbodyobj.rows.length;

						var arturowobj = fnaartuplantbodyobj.insertRow(arturl);

//						var cell0 = arturowobj.insertCell(0);
//						cell0.style.textAlign = "left";
//						cell0.innerHTML = ' <input type = "text" size="3" name="txtFldFnaArtuPlanSiNo" readOnly="true" class="fpEditTblTxt" style="text-align:center" value="'+ (arturl + 1)	+ '" />'+
//						'<input type = "hidden"  name="txtFldFnaArtuPlanMode" value="'+INS_MODE+'" />'+
//						'<input type = "hidden"  name="txtFldFnaArtuPlanId"/> ';
//						cell0.childNodes[2].value = advrecplanData[advrec]["txtFldFnaAdvRecId"];

//						var cell0a = arturowobj.insertCell(1);//kavi
//						cell0a.innerHTML="<input type='text' value='temp' style='width:100px;'>";

						var cell1 = arturowobj.insertCell(0);
						cell1.style.textAlign = "left";
						cell1.style.border="1px solid black";
						cell1.innerHTML = '<select name="selFnaArtuPlanPrdtType" class="kycSelect" onchange="/*clearPlanNames(this)*/" ></select>'+
										  '<input type = "hidden"  name="txtFldFnaArtuPlanMode" value="'+INS_MODE+'" />'+
										  '<input type = "hidden"  name="txtFldFnaArtuPlanId"/> ';
						cell1.childNodes[2].value = advrecplanData[advrec]["txtFldFnaAdvRecId"];
						//changed by Thulasy 16.03.2018 -->
						var cell2 = arturowobj.insertCell(1);
						cell2.style.textAlign = "left";
						cell2.style.border="1px solid black";
						cell2.innerHTML = '<select name="selFnaArtuPlanCompName" class="kycSelect" onchange="getPrdtPlanName(this)" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" style="width:200px;display:inline;" ></select>'+
						'<input type="text"  name="txtFldFnaArtuPlanCompName" class="fpEditTblTxt" style="display:none;width:200px"  maxlength="60">'+
						'<select name="selFnaArtuPlanName" onchange="setPlanName(this)" onclick="hideTooltip();chkCompDets(this);" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" class="kycSelect" style="width:200px;"><option value="">--SELECT--</option></select>'+
						'<input type="hidden" name="selFnaArtuPlan"/>'+
						'<img src="images/modify.png" onclick="editPrdtPlanName(this)" style=""/>'+
						'<input type="text"  name="txtFldFnaArtuPlanNameTxt" class="fpEditTblTxt" style="display:none;width:200px" onclick="hideTooltip();" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" maxlength="300"/>'+
						'<img src="images/history.png" onclick="resetPrdtPlanName(this)" style="display:none"/>'+
						'<select name="selFnaArtuPrdtCatage" class="kycSelect" style="width:80px;display:inline"><option value="">--SELECT--</option><option value="B">Basic</option><option value="R">Rider</option></select>'+
						'<input type="hidden" name="selFnaArtuOpt" value="CUSTOM"/>';
						//cell2.childNodes[0].value = advrecplanData[advrec]["txtFldFnaAdvRecSumAssr"];

						var cell3 = arturowobj.insertCell(2);
						cell3.style.textAlign = "left";
						cell3.style.border="1px solid black";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaArtuPlanSumAssr" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell3.childNodes[0].value = advrecplanData[advrec]["txtFldFnaAdvRecSumAssr"];

						var cell4 = arturowobj.insertCell(3);
						cell4.style.textAlign = "left";
						cell4.style.border="1px solid black";
						cell4.innerHTML = '<select name="selFnaArtuPlanPayment" class="kycSelect" ></select>';

						var cell5 = arturowobj.insertCell(4);
						cell5.style.textAlign = "left";
						cell5.style.border="1px solid black";
						cell5.innerHTML = '<input type = "text"  name="txtFldFnaArtuPlanPlanTerm" class="fpEditTblTxt"  maxlength="30"/>';
						cell5.childNodes[0].value = advrecplanData[advrec]["txtFldFnaAdvRecPlanTerm"];

						var cell6 = arturowobj.insertCell(5);
						cell6.style.textAlign = "left";
						cell6.style.border="1px solid black";
						cell6.innerHTML = '<input type = "text"  name="txtFldFnaArtuPlanPremTerm" class="fpEditTblTxt"  maxlength="30"/>';
						cell6.childNodes[0].value = advrecplanData[advrec]["txtFldFnaAdvRecPremTerm"];

						var cell7 = arturowobj.insertCell(6);
						cell7.style.textAlign = "left";
						cell7.style.border="1px solid black";
						cell7.innerHTML = '<input type = "text"  name="txtFldFnaArtuPlanPrem" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell7.childNodes[0].value = advrecplanData[advrec]["txtFldFnaAdvRecPrem"];
						
						var cell8 = arturowobj.insertCell(7);
						cell8.style.textAlign = "left";
						cell8.style.border="1px solid black";
						cell8.innerHTML = '<select name="selFnaArtuPlanRiskRate" class="kycSelect"></select>';
//						comboMaker(cell1, prdtArray,advrecplanData[advrec]["selFnaAdvRecPrdtType"],false);
						comboMakerByKeyVal(STR_PRODTYPE,cell1.childNodes[0], advrecplanData[advrec]["selFnaAdvRecPrdtType"],false);
						comboMakerByKeyVal(STR_ILP_RISK_RATE,cell8.childNodes[0], advrecplanData[advrec]["selRecomProdRisk"],true);
						cell8.childNodes[0].value=advrecplanData[advrec]["selRecomProdRisk"];
						
						comboMakerById(cell2.childNodes[0], prinIdArr,advrecplanData[advrec]["selFnaAdvRecCompName"],true);
						cell2.childNodes[0].value = advrecplanData[advrec]["selFnaAdvRecCompName"];
						cell2.childNodes[1].value = advrecplanData[advrec]["selFnaAdvRecCompName"];
						cell2.childNodes[7].value = advrecplanData[advrec]["txtFldRecomPpBasRid"];
						cell2.childNodes[8].value = planOpt;

						var planName = advrecplanData[advrec]["selFnaAdvRecPrdtPlan"];;

						if(planOpt == "CUSTOM"){
//							cell2.childNodes[0].style.display="none";
							cell2.childNodes[2].style.display="none";
							cell2.childNodes[4].style.display="none";
							cell2.childNodes[5].value=planName;
							cell2.childNodes[5].style.display="";
							cell2.childNodes[6].style.display="none";
						}else{
//							cell2.childNodes[1].style.display="";
							cell2.childNodes[4].style.display="";
							cell2.childNodes[5].value="none";
							cell2.childNodes[5].style.display="none";
							cell2.childNodes[6].style.display="none";

							if (prdtplanArr.length == 0) {
//								alert("going to call prdt plan name method...");
//								getPrdtPlanName(cell2.childNodes[0]);
							}
							cell2.childNodes[1].value = advrecplanData[advrec]["selFnaAdvRecPlanName"];

//							MAR_2018
//							var tmpArr = new Array();
//							tmpArr[0] = new Option(advrecplanData[advrec]["selFnaAdvRecPlanName"],advrecplanData[advrec]["selFnaAdvRecPrdtPlan"]);
//							comboMakerById(cell2.childNodes[1], tmpArr,	advrecplanData[advrec]["selFnaAdvRecPlanName"],	true);
							cell2.childNodes[1].value = advrecplanData[advrec]["selFnaAdvRecPlanName"];
							cell2.childNodes[3].value = planName;

						}

						comboMaker(cell4, paymentModeArr,advrecplanData[advrec]["selFnaAdvRecPayment"],	true);
						
						
// Poovathi Add on 18-10-2019.						
					var cell0PrdtType = arturowobj.cells[0].childNodes[0].value;
						
					 if((cell0PrdtType=="Insurance")||(cell0PrdtType=="H&S")||(cell0PrdtType=="PA")){
	                        arturowobj.cells[7].childNodes[0].disabled = true;
	                        arturowobj.cells[7].childNodes[0].value="";
	                        arturowobj.cells[7].childNodes[0].options[0].innerHTML = "-N/A-";
//	                          arturowobj.cells[1].childNodes[0].style.display="";
//	                          arturowobj.cells[1].childNodes[1].style.display="none";
	                        
	                          
                     }else{
						 arturowobj.cells[7].childNodes[0].disabled = false;
						  arturowobj.cells[7].childNodes[0].options[0].innerHTML = "--SELECT--";
//						 arturowobj.cells[1].childNodes[0].style.display="none";
//                         arturowobj.cells[1].childNodes[1].style.display="";
					 }
					 
// Poovathi Add validation[ProductRiskRate Enable/Disable based on Product LOB] in page on  6  17-10-2019.
					 
// Poovathi Add validation[Company  Show/Hide based on Product LOB] in page 6 on  06-11-2019.	
					 
                           arturowobj.cells[0].childNodes[0].onchange = function(){
                            var curtval = $(this).val();
							
		                    if((curtval == "Insurance")||(curtval == "H&S")||(curtval == "PA")){
							
			                 arturowobj.cells[7].childNodes[0].disabled = true;
			                 arturowobj.cells[7].childNodes[0].value="";
			                 arturowobj.cells[7].childNodes[0].options[0].innerHTML = "-N/A-";
//                             arturowobj.cells[1].childNodes[0].style.display="";
//                             arturowobj.cells[1].childNodes[1].style.display="none";
                            
			                 
	                        }else{
	                         arturowobj.cells[7].childNodes[0].disabled = false;
	                         arturowobj.cells[7].childNodes[0].options[0].innerHTML = "--SELECT--";
//	                         arturowobj.cells[1].childNodes[1].style.display="";
//			                 arturowobj.cells[1].childNodes[0].style.display="none";
							}
							
						};
						
						
						  
					
						
// Poovathi add Autocomplete feature to page 6 populate function on 29-10-2019.
						
						 var list_of_principal = [];
						 list_of_principal =  copyTextArrFromOpt(arturowobj.cells[1].childNodes[0]);						 
						 var prinobj_ = $(arturowobj).find("td:eq(1)").find("input:eq(0)");						 
						 prinobj_.autocomplete({
							 source:list_of_principal
							 
						 });
						 

						/*arturowobj.onclick = function() {
							selectSingleRow(this);
						};*/
						arturowobj.onclick = function() {
//							var tempRefId=this.childNodes[1].childNodes[1].value;
//							var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");
							var rows =getParentByTagName(this,'table').tBodies[0].rows;
							var subRows=document.getElementById('fnaartuplanTblss').tBodies[0].rows;

							for (iCel=0; iCel < rows.length; iCel++)
							{
								 removeClass(rows[iCel],'fnaPlnSelected');
								 removeClass(subRows[iCel],'fnaPlnSelected');
							}

							addClass(this,'fnaPlnSelected');
						};

						var totalcell = arturowobj.cells.length;
						for(var cel=0;cel<totalcell;cel++){
							if(arturowobj.cells[cel].childNodes[0]){
								arturowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
									e = e || event;
									checkChanges(this);
								},false);
							}
						}

						// }
						var rowClntObjLen = tBodyObjClnt.rows.length;
						var rowobjClnt = tBodyObjClnt.insertRow(rowClntObjLen);
						var Sino=rowSino;
						var crntDate=new Date();


						if(strOldValue != advrecplanData[advrec]["txtFldRecomPpName"] && !firstLpSts){
							parentSts=true;
							tempRefId = "TEMP"+crntDate.getDay()+crntDate.getMonth()+crntDate.getFullYear()+crntDate.getHours()+crntDate.getMinutes()+crntDate.getSeconds()+((Number(Sino)+1));
						}

						if(firstLpSts){
							tempRefId = "TEMP"+crntDate.getDay()+crntDate.getMonth()+crntDate.getFullYear()+crntDate.getHours()+crntDate.getMinutes()+crntDate.getSeconds()+((Number(Sino)+1));
						}

						addClass(rowobjClnt,'prntRowTbl');

						var cells0 = rowobjClnt.insertCell(0);
						cells0.style.textAlign = "left";
						cells0.style.border="1px solid black";
						cells0.style.height="20px";
						cells0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaArtuPlanSiNo" readOnly="true" value="'+Sino+'" class="fpEditTblTxt" style="text-align:center" />';


						var cells1 = rowobjClnt.insertCell(1);
						cells1.style.textAlign = "left";
						cells1.style.border="1px solid black";
						cells1.innerHTML = '<input type = "text" size="3"  name="txtFldRecomPpName" value="'+advrecplanData[advrec]["txtFldRecomPpName"]+'" class="fpEditTblTxt" style="width:150px;display:inline"/>'+
										   '<input type = "hidden"  name="txtFldPrntTblRefId" value="'+tempRefId+'"/>'+
										   '<img src="images/addrow.png"   style="cursor: pointer;" height="16px" align="top" id="imgFnaArtuPlanAddRow" onclick="fnaArtuPlanAddRow(&apos;fnaartuplanTbl&apos;,&apos;Y&apos;,this)"/>';

						cells1.childNodes[0].onkeyup=function(){
							var tempRefId=getParentByTagName(this,'td').childNodes[1].value;
							var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");

							for(var i=0;i<arrElm.length;i++){
								arrElm[i].parentNode.firstChild.value=this.value;
							}
						};

						cells1.childNodes[0].onchange=function(){
							var arrPrntElm=$('tr.prntRowTbl:visible');
							var crntTrIndex=getParentByTagName(this,'tr').rowIndex;

							for(var i=0;i<arrPrntElm.length;i++){
								if(crntTrIndex!=arrPrntElm[i].rowIndex){
									if(arrPrntElm[i].childNodes[1].childNodes[0].value.trim().equalIgnoreCase(this.value.trim())){
										this.value="";
										alert("Client(1)/Client(2) name already exist");
										this.focus();
										return false;
									}
								}
							}

						};

						if(!parentSts){
							removeClass(rowobjClnt,'prntRowTbl');

							var clientName=advrecplanData[advrec]["txtFldRecomPpName"];
							var clientRef=tempRefId;

							cells0.innerHTML="";
							cells1.innerHTML='<input type = "hidden" size="3"  name="txtFldRecomPpName" readOnly="true" value="'+clientName+'" class="fpEditTblTxt" style="width:150px;display:inline"/>'+
											 '<input type = "hidden"  name="txtFldPrntTblRefId" value="'+clientRef+'" style=""/>';

							reArrngCellBorder(clientRef,'fnaartuplanTblss');


						}

						if(parentSts){
							cells0.firstChild.value=rowSino;
							parentSts=false;
							firstLpSts=false;
							rowSino++;
						}

						rowobjClnt.onclick = function() {
							var tempRefId=this.childNodes[1].childNodes[1].value;
							var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");
							var rows =getParentByTagName(this,'table').tBodies[0].rows;
							var subRows=document.getElementById('fnaartuplanTbl').tBodies[0].rows;

							for (iCel=0; iCel < rows.length; iCel++)
							{
								 removeClass(rows[iCel],'fnaPlnSelected')
								 removeClass(subRows[iCel],'fnaPlnSelected')
							}

							for(var i=0;i<arrElm.length;i++){
								addClass(getParentByTagName(arrElm[i],'tr'),'fnaPlnSelected');
								var rowNo=getParentByTagName(arrElm[i],'tr').rowIndex;
								addClass(fnaartuplantbodyobj.rows[rowNo-1],'fnaPlnSelected');
							}
						};


						strOldValue=advrecplanData[advrec]["txtFldRecomPpName"];

						tblResetScroll();

					}


				}

				if (tab == "ARCH_ADVRECFUNDPLAN_DATA") {

					var artufundData = value;

					var artuftblobj = document.getElementById("fnaartufundTbl");
					var artuftbodyobj = artuftblobj.tBodies[0];

					for ( var artuf in artufundData) {

						var artufrlen = artuftbodyobj.rows.length;

						var artufrowobj = artuftbodyobj.insertRow(artufrlen);

						var planOptf1 = artufundData[artuf]["txtFldFnaArtuFundOpt"];

						var cell0 = artufrowobj.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text" size="3" style="text-align:center;" name="txtFldFnaArtuFundSiNo" readOnly="true" value="'+(artufrlen + 1)+'" class="fpEditTblTxt"/>'+
						'<input type = "hidden"  name="txtFldFnaArtuFundMode" value="'+INS_MODE+'" />'+
						'<input type = "hidden"  name="txtFldFnaArtuFundId"  />';
						cell0.childNodes[2].value = artufundData[artuf]["txtFldFnaArtuFundId"];

						/*
						 * var cell1 = artufrowobj.insertCell(1);
						 * cell1.style.textAlign="left"; cell1.innerHTML ='<select
						 * name="selFnaArtuFundPrdtType" class="kycSelect" ></select>';
						 *
						 * var cell2 = artufrowobj.insertCell(2);
						 * cell2.style.textAlign="left"; cell2.innerHTML ='<select
						 * name="selFnaArtuFundCompName" class="kycSelect" ></select>';
						 */
						//changed by Thulasy 16.03.2018 -->
						var cell1 = artufrowobj.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<select name="selFnaArtuFundPrdtType" class="kycSelect" onchange="/*loadPrinOrFundMgr(this)*/" ></select>';

						var cell2 = artufrowobj.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<select name="selFnaArtuFundCompName" class="kycSelect" onchange="getPrdtPlanName(this)" style="width:200px;display:none;" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" ></select>'+
						'<input type="text"  name="txtFldFnaArtuFundCompName" class="fpEditTblTxt" style="display:inline;width:200px"  maxlength="60">'+
						'<select name="selFnaArtuFundProdCode" onchange="setPlanName(this)" onclick="hideTooltip();chkCompDets(this);" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" class="kycSelect" style="width:200px"><option value="">--SELECT--</option></select>'+
						'<input type="hidden" name="selFnaArtuFundPlan"/>'+
						'<img src="images/modify.png" onclick="editFundPlanName(this)"/>'+
						'<input type="text"  name="txtFnaArtuFundPlanTxt" class="fpEditTblTxt" style="display:none;width:200px"  maxlength="300" onclick="hideTooltip();" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>'+
						'<img src="images/history.png" onclick="resetFundPlanName(this)" style="display:none"/>'+
						'<select name="selFnaArtuFundCatage" class="kycSelect" style="width:80px;display:none"><option value="">--SELECT--</option><option value="B">Basic</option><option value="R">Rider</option></select>'+
						'<input type="hidden" name="selFnaArtuFundOpt" value="CUSTOM"/>';


						var cell3 = artufrowobj.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaArtuFundRisk" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell3.childNodes[0].value = artufundData[artuf]["txtFldFnaArtuFundRisk"];

						var cell4 = artufrowobj.insertCell(4);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<select name="selFnaArtuFundPayment" class="kycSelect" ></select>';

						var cell5 = artufrowobj.insertCell(5);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text"  name="txtFldFnaArtuFundPurSale" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell5.childNodes[0].value = artufundData[artuf]["txtFldFnaArtuFundPurSale"];

						var cell6 = artufrowobj.insertCell(6);
						cell6.style.textAlign = "left";
						cell6.innerHTML = '<input type = "text"  name="txtFldFnaArtuFundPurAmt" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell6.childNodes[0].value = artufundData[artuf]["txtFldFnaArtuFundPurAmt"];

						var cell7 = artufrowobj.insertCell(7);
						cell7.style.textAlign = "left";
						cell7.innerHTML = '<input type = "text"  name="txtFldFnaArtuFundPurPrct" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell7.childNodes[0].value = artufundData[artuf]["txtFldFnaArtuFundPurPrct"];

						comboMaker(cell1, fundPrdtArr,artufundData[artuf]["selFnaArtuFundPrdtType"],false);

						// comboMaker(cell2,fundPrdtArr,artufundData[artuf]["selFnaArtuFundCompName"],true);

						var productLob = artufundData[artuf]["selFnaArtuFundPrdtType"];
						if ( productLob == "ILP") {
							comboMakerById(cell2.childNodes[0],	prinIdArr,artufundData[artuf]["selFnaArtuFundCompName"],true);
						} else if (productLob == "UT") {
							comboMakerById(cell2.childNodes[0],	fmArr,artufundData[artuf]["selFnaArtuFundCompName"],true);
						}
						cell2.childNodes[0].value = artufundData[artuf]["selFnaArtuFundCompName"];
						cell2.childNodes[1].value = artufundData[artuf]["selFnaArtuFundCompName"];
						cell2.childNodes[7].value = artufundData[artuf]["txtFldFnaArtuFundBasRid"];
						cell2.childNodes[8].value = planOptf1;

						var planNameF =  artufundData[artuf]["selFnaArtuFundPlanName"];;
						if(planOptf1 == "CUSTOM"){
							cell2.childNodes[0].style.display="none";
							cell2.childNodes[2].style.display="none";
							cell2.childNodes[4].style.display="none";
							cell2.childNodes[5].value=planNameF;
							cell2.childNodes[5].style.display="";
							cell2.childNodes[6].style.display="none";
						}else{
							cell2.childNodes[2].style.display="";
							cell2.childNodes[4].style.display="";
							cell2.childNodes[5].value="";
							cell2.childNodes[5].style.display="none";
							cell2.childNodes[6].style.display="none";

							if (prdtplanArr.length == 0) {
//								alert("going to call prdt plan name method...");
//								getPrdtPlanName(cell2.childNodes[0]);
							}


							cell2.childNodes[2].value = artufundData[artuf]["selFnaArtuFundPlanCode"];
							cell2.childNodes[3].value = planNameF;

//							MAR_2018
//							var tmpArr = new Array();
//							tmpArr[0] = new Option(artufundData[artuf]["selFnaArtuFundPlanCode"],artufundData[artuf]["selFnaArtuFundPlanName"]);
//							comboMakerById(cell2.childNodes[1], tmpArr,	artufundData[artuf]["selFnaArtuFundPlanName"],	true);
						}


						comboMaker(cell4, fundPayMethArr,artufundData[artuf]["selFnaArtuFundPayment"],	true);

						$("#txtFldArtuFundIaf").val(artufundData[artuf]["txtFldArtuFundIaf"]);

						artufrowobj.onclick = function() {
							selectSingleRow(this);
						};

						var totalcell = artufrowobj.cells.length;
						for(var cel=0;cel<totalcell;cel++){
							if(artufrowobj.cells[cel].childNodes[0]){
								artufrowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
									e = e || event;
									checkChanges(this);
								},false);
							}
						}

					}
				}

				if (tab == "ARCH_SWREPPLANDET_DATA") {

					var swrepplanData = value;

					var swrpptblobj = document.getElementById("fnaswrepplanTbl");
					var swrpptbodyobj = swrpptblobj.tBodies[0];

					var tblObjClnt = document.getElementById("fnaswrepplanTblss");//added by kavi 21/06/2018
					var tBodyObjClnt = tblObjClnt.tBodies[0];
					var strOldValue = "",rowSino= 1,parentSts=true,firstLpSts=true,tempRefId="";

					for ( var swrpp in swrepplanData) {

						var swrpprlen = swrpptbodyobj.rows.length;
						var swrpprowobj = swrpptbodyobj.insertRow(swrpprlen);

						var planOptsp = swrepplanData[swrpp]["txtFldFnaSwrepPlanOpt"];
						//changed by Thulasy 16.03.2018 -->
						/*var cell0 = swrpprowobj.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaSwrepPlanSiNo" readonly="readonly" value="'
								+ (swrpprlen + 1)
								+ '" class="fpEditTblTxt" style="text-align:center"/><input type = "hidden"  name="txtFldFnaSwrepPlanMode" value="'
								+ INS_MODE
								+ '" /><input type = "hidden"  name="txtFldFnaSwrepPlanId"  />';
						cell0.childNodes[2].value = swrepplanData[swrpp]["txtFldFnaSwrepPlanId"];*/

						var cell1 = swrpprowobj.insertCell(0);
						cell1.style.textAlign = "left";
						cell1.style.border="1px solid black";
						cell1.innerHTML = '<select name="selFnaSwrepPlanPrdtType" class="kycSelect" onchange="/*clearPlanNames(this)*/"></select>'+
										  '<input type = "hidden"  name="txtFldFnaSwrepPlanMode" value="'+INS_MODE+'" />'+
										  '<input type = "hidden"  name="txtFldFnaSwrepPlanId"  />';
						cell1.childNodes[2].value=swrepplanData[swrpp]["txtFldFnaSwrepPlanId"];

						var cell2 = swrpprowobj.insertCell(1);
						cell2.style.textAlign = "left";
						cell2.style.border="1px solid black";
						cell2.innerHTML = '<select name="selFnaSwrepPlanCompName" class="kycSelect" style="width:200px;display:inline; " onchange="getPrdtPlanName(this)" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"></select>'+
						'<input type="text"  name="txtFldFnaSwrepPlanCompName" class="fpEditTblTxt" style="display:none;width:200px"  maxlength="60">'+
						'<select name="selFnaSwrepPlanName" onchange="setPlanName(this)" class="kycSelect" onclick="hideTooltip();chkCompDets(this);" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" style="width:200px"><option value="">--SELECT--</option></select>'+
						'<input type="hidden" name="selFnaSwrepPlan"/>'+
						'<img src="images/modify.png" onclick="editSwrepPlanName(this)"/>'+
						'<input type="text"  name="selFnaSwrepPlanNameTxt" class="fpEditTblTxt" style="display:inline;width:200px"  maxlength="300" onclick="hideTooltip();" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>'+
						'<img src="images/history.png" onclick="resetSwrepPlanName(this)" style="display:none"/>'+
						'<select name="selFnaSwrepCateg" class="kycSelect" style="width:80px;display:inline"><option value="">--SELECT--</option><option value="B">Basic</option><option value="R">Rider</option></select>'+
						'<input type="hidden" name="selFnaSwrepOpt" value="CUSTOM"/>';

						var cell3 = swrpprowobj.insertCell(2);
						cell3.style.textAlign = "left";
						cell3.style.border="1px solid black";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaSwrepPlanSA" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell3.childNodes[0].value = swrepplanData[swrpp]["txtFldFnaSwrepPlanSA"];

						var cell4 = swrpprowobj.insertCell(3);
						cell4.style.textAlign = "left";
						cell4.style.border="1px solid black";
						cell4.innerHTML = '<select name="selFnaSwrepPlanPayment" class="kycSelect" ></select>';

						var cell5 = swrpprowobj.insertCell(4);
						cell5.style.textAlign = "left";
						cell5.style.border="1px solid black";
						cell5.innerHTML = '<select name="selFnaSwrepPlanSwitch" class="kycSelect"></select>';

						var cell6 = swrpprowobj.insertCell(5);
						cell6.style.textAlign = "left";
						cell6.style.border="1px solid black";
						cell6.innerHTML = '<input type = "text"  name="txtFldFnaSwrepPlanTerm" class="fpEditTblTxt"  maxlength="30"/>';
						cell6.childNodes[0].value = swrepplanData[swrpp]["txtFldFnaSwrepPlanTerm"];

						var cell7 = swrpprowobj.insertCell(6);
						cell7.style.textAlign = "left";
						cell7.style.border="1px solid black";
						cell7.innerHTML = '<input type = "text"  name="txtFldFnaSwrepPlanPayTerm" class="fpEditTblTxt"  maxlength="3"  onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';
						cell7.childNodes[0].value = swrepplanData[swrpp]["txtFldFnaSwrepPlanPayTerm"];

						var cell8 = swrpprowobj.insertCell(7);
						cell8.style.textAlign = "left";
						cell8.style.border="1px solid black";
						cell8.innerHTML = '<input type = "text"  name="txtFldFnaSwrepPlanPrem" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell8.childNodes[0].value = swrepplanData[swrpp]["txtFldFnaSwrepPlanPrem"];
						
						var cell9 = swrpprowobj.insertCell(8);
						cell9.style.textAlign = "left";
						cell9.style.border="1px solid black";
						cell9.innerHTML = '<select name="selFnaSwrepPlanRiskRate" class="kycSelect"></select>';

//						comboMaker(	cell1,prdtArray,swrepplanData[swrpp]["selFnaSwrepPlanPrdtType"],false);
						comboMakerByKeyVal(STR_PRODTYPE,cell1.childNodes[0], swrepplanData[swrpp]["selFnaSwrepPlanPrdtType"], false);
						comboMakerByKeyVal(STR_ILP_RISK_RATE,cell9.childNodes[0], swrepplanData[swrpp]["selSwrepPpRiskRate"],true);
						cell9.childNodes[0].value=swrepplanData[swrpp]["selSwrepPpRiskRate"];



						comboMakerById(cell2.childNodes[0],prinIdArr,swrepplanData[swrpp]["selFnaSwrepPlanCompName"],true);
						cell2.childNodes[0].value = swrepplanData[swrpp]["selFnaSwrepPlanCompName"];
						cell2.childNodes[1].value = swrepplanData[swrpp]["selFnaSwrepPlanCompName"];
						cell2.childNodes[7].value = swrepplanData[swrpp]["txtFldFnaSwrepBasRid"];
						cell2.childNodes[8].value =planOptsp;

						var planNamesp = swrepplanData[swrpp]["selFnaSwrepPlan"];

						if(planOptsp == "CUSTOM"){
//							cell2.childNodes[0].style.display="";
							cell2.childNodes[2].style.display="none";
							cell2.childNodes[4].style.display="none";
							cell2.childNodes[5].value=planNamesp;
//							cell2.childNodes[5].style.display="";
							cell2.childNodes[6].style.display="none";
						}else{

							cell2.childNodes[2].style.display="";
							cell2.childNodes[4].style.display="";
							cell2.childNodes[5].value="";
							cell2.childNodes[5].style.display="none";
							cell2.childNodes[6].style.display="none";

							if (prdtplanArr.length == 0) {
//								alert("going to call prdt plan name method...");
//								getPrdtPlanName(cell2.childNodes[0]);
							}

//							MAR_2018
//							var tmpArr = new Array();
//							tmpArr[0] = new Option(swrepplanData[swrpp]["selFnaSwrepPlanName"],swrepplanData[swrpp]["selFnaSwrepPlan"]);
//							comboMakerById(cell2.childNodes[1], tmpArr,	swrepplanData[swrpp]["selFnaSwrepPlanName"],true);

							cell2.childNodes[2].value = swrepplanData[swrpp]["selFnaSwrepPlanName"];
							cell2.childNodes[3].value = planNamesp;

						}
						
						


						comboMaker(cell4, paymentModeArr,swrepplanData[swrpp]["selFnaSwrepPlanPayment"],	true);

						comboMaker(cell5, swtypeArr,swrepplanData[swrpp]["selFnaSwrepPlanSwitch"],true);

						/*swrpprowobj.onclick = function() {
							selectSingleRow(this);
						};*/
						
						
//						Poovathi Add on 18-10-2019	
//						Poovathi Add on 06-11-2019					
											
											var cell0PrdtTypeS = swrpprowobj.cells[0].childNodes[0].value;
										
											
											 if( (cell0PrdtTypeS == "Insurance")||(cell0PrdtTypeS == "H&S")||(cell0PrdtTypeS == "PA")){
											  
												   
											       swrpprowobj.cells[8].childNodes[0].disabled = true;
											       swrpprowobj.cells[8].childNodes[0].value="";
											       swrpprowobj.cells[8].childNodes[0].options[0].innerHTML = "-N/A-";
//					                               swrpprowobj.cells[1].childNodes[1].style.display="none";
//					                               swrpprowobj.cells[1].childNodes[0].style.display="";
					                         }else{
					                        	   swrpprowobj.cells[8].childNodes[0].disabled = false;
					                        	   swrpprowobj.cells[8].childNodes[0].options[0].innerHTML = "--SELECT--";
//					                               swrpprowobj.cells[1].childNodes[1].style.display="";	
//					                               swrpprowobj.cells[1].childNodes[0].style.display="none";
					                         }
											
					// Poovathi Add on 17-10-2019
											 
											 swrpprowobj.cells[0].childNodes[0].onchange = function(){
												
					                            var curtval = $(this).val();
					                            
						                        if((curtval == "Insurance")||(curtval == "H&S")||(curtval == "PA")){
												
					                              swrpprowobj.cells[8].childNodes[0].disabled = true;
					                              swrpprowobj.cells[8].childNodes[0].options[0].innerHTML = "-N/A-";
//					                              swrpprowobj.cells[1].childNodes[1].style.display="none";
//					                              swrpprowobj.cells[1].childNodes[0].style.display="";
					                            }else{
						                       
					                             swrpprowobj.cells[8].childNodes[0].disabled = false;
					                             swrpprowobj.cells[8].childNodes[0].options[0].innerHTML = "-N/A-";
//					                             swrpprowobj.cells[1].childNodes[1].style.display="";	
//					                             swrpprowobj.cells[1].childNodes[0].style.display="none";
												}
												
											};
						
						
// poovathi Add Autocomplete Feature to page 9 Populate Function on 29-10-2019.
						
						var list_of_principal = [];
						 list_of_principal =  copyTextArrFromOpt(swrpprowobj.cells[1].childNodes[0]);						 
						 var prinobj_ = $(swrpprowobj).find("td:eq(1)").find("input:eq(0)");						 
						 prinobj_.autocomplete({
							 source:list_of_principal
							 
						 });
						 
						
						
						
						

						swrpprowobj.onclick = function() {
							var rows =getParentByTagName(this,'table').tBodies[0].rows;
							var subRows=document.getElementById('fnaswrepplanTblss').tBodies[0].rows;

							for (iCel=0; iCel < rows.length; iCel++)
							{
								 removeClass(rows[iCel],'fnaPlnSelected');
								 removeClass(subRows[iCel],'fnaPlnSelected');
							}

							addClass(this,'fnaPlnSelected');
						};

						var totalcell = swrpprowobj.cells.length;
						for(var cel=0;cel<totalcell;cel++){
							if(swrpprowobj.cells[cel].childNodes[0]){
								swrpprowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
									e = e || event;
									checkChanges(this);
								},false);
							}
						}


						var rowClntObjLen = tBodyObjClnt.rows.length;
						var rowobjClnt = tBodyObjClnt.insertRow(rowClntObjLen);
						var Sino=rowSino;
						var crntDate=new Date();


						if(strOldValue != swrepplanData[swrpp]["txtFldSwrepPpName"] && !firstLpSts){
							parentSts=true;
							tempRefId = "TEMPSW"+crntDate.getDay()+crntDate.getMonth()+crntDate.getFullYear()+crntDate.getHours()+crntDate.getMinutes()+crntDate.getSeconds()+((Number(Sino)+1));
						}

						if(firstLpSts){
							tempRefId = "TEMPSW"+crntDate.getDay()+crntDate.getMonth()+crntDate.getFullYear()+crntDate.getHours()+crntDate.getMinutes()+crntDate.getSeconds()+((Number(Sino)+1));
						}

						addClass(rowobjClnt,'prntSwRepRowTbl');

						var cells0 = rowobjClnt.insertCell(0);
						cells0.style.textAlign = "left";
						cells0.style.border="1px solid black";
						cells0.style.height="20px";
						cells0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaSwrepPlanSiNo" readOnly="true" value="'+Sino+'" class="fpEditTblTxt" style="text-align:center" />';


						var cells1 = rowobjClnt.insertCell(1);
						cells1.style.textAlign = "left";
						cells1.style.border="1px solid black";
						cells1.innerHTML = '<input type = "text" size="3"  name="txtFldSwrepPpName" value="'+swrepplanData[swrpp]["txtFldSwrepPpName"]+'" class="fpEditTblTxt" style="width:150px;display:inline"/>'+
										   '<input type = "hidden"  name="txtFldFnaSwrepPlanRefId" value="'+tempRefId+'"/>'+
										   '<img src="images/addrow.png"   style="cursor: pointer;" height="16px" align="top" id="imgFnaArtuPlanAddRow" onclick="fnaSwrepPlanAddRow(&apos;fnaswrepplanTbl&apos;,&apos;Y&apos;,this)"/>';

						cells1.childNodes[0].onkeyup=function(){
							var tempRefId=getParentByTagName(this,'td').childNodes[1].value;
							var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");
							var curntVal=this.value,crntObj=this;

							for(var i=0;i<arrElm.length;i++){
								arrElm[i].parentNode.firstChild.value=curntVal;
							}
							this.value=curntVal
						};

						cells1.childNodes[0].onchange=function(){
							var arrPrntElm=$("tr.prntSwRepRowTbl:visible");
							var crntTrIndex=getParentByTagName(this,'tr').rowIndex;
							var curntElm=this;

							for(var i=0;i<arrPrntElm.length;i++){

								if(crntTrIndex!=arrPrntElm[i].rowIndex){
									if(arrPrntElm[i].childNodes[1].childNodes[0].value.trim().equalIgnoreCase(this.value.trim())){
										curntElm.value="";
										alert("Client(1)/Client(2) name already exist");
										this.focus();
										return false;
									}
								}
							}

						};

						if(!parentSts){
							removeClass(rowobjClnt,'prntSwRepRowTbl');

							var clientName=swrepplanData[swrpp]["txtFldSwrepPpName"];
							var clientRef=tempRefId;

							cells0.innerHTML="";
							cells1.innerHTML='<input type = "hidden" size="3"  name="txtFldSwrepPpName" readOnly="true" value="'+clientName+'" class="fpEditTblTxt" style="width:150px;display:inline"/>'+
											 '<input type = "hidden"  name="txtFldFnaSwrepPlanRefId" value="'+clientRef+'" style=""/>';

							reArrngCellBorder(clientRef,'fnaswrepplanTblss');


						}

						if(parentSts){
							cells0.firstChild.value=rowSino;
							parentSts=false;
							firstLpSts=false;
							rowSino++;
						}

						rowobjClnt.onclick = function() {
							var tempRefId=this.childNodes[1].childNodes[1].value;
							var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");
							var rows =getParentByTagName(this,'table').tBodies[0].rows;
							var subRows=document.getElementById('fnaswrepplanTbl').tBodies[0].rows;

							for (iCel=0; iCel < rows.length; iCel++)
							{
								 removeClass(rows[iCel],'fnaPlnSelected')
								 removeClass(subRows[iCel],'fnaPlnSelected')
							}

							for(var i=0;i<arrElm.length;i++){
								addClass(getParentByTagName(arrElm[i],'tr'),'fnaPlnSelected');
								var rowNo=getParentByTagName(arrElm[i],'tr').rowIndex;
								addClass(document.getElementById('fnaswrepplanTbl').tBodies[0].rows[rowNo-1],'fnaPlnSelected');
							}
						};


						strOldValue=swrepplanData[swrpp]["txtFldSwrepPpName"];

						tblResetScroll();

					}
				}
				if (tab == "ARCH_SWREPFUNDDET_DATA") {
					var swrepfData = value;

					var swrftblobj = document.getElementById("fnaSwrepFundTbl");
					var swrftbodyobj = swrftblobj.tBodies[0];

					for ( var swrpf in swrepfData) {

						var swrfrowlen = swrftbodyobj.rows.length;
						var swrfrowobj = swrftbodyobj.insertRow(swrfrowlen);

						//changed by Thulasy 16.03.2018 -->
						var planOptsf = swrepfData[swrpf]["txtFldFnaSwrepFundOpt"];
						var cell0 = swrfrowobj.insertCell(0);
						cell0.style.textAlign = "left";
						cell0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaSwrepFundSiNo" readonly="readonly" value="'
								+ (swrfrowlen + 1)
								+ '" class="fpEditTblTxt" style="text-align:center"	 /><input type = "hidden"  name="txtFldFnaSwrepFundMode" value="'
								+ INS_MODE
								+ '" /><input type = "hidden"  name="txtFldFnaSwrepFundId"  />';
						cell0.childNodes[2].value = swrepfData[swrpf]["txtFldFnaSwrepFundId"];

						/*
						 * var cell1 = swrfrowobj.insertCell(1);
						 * cell1.style.textAlign="left"; cell1.innerHTML ='<select
						 * name="selFnaSwrepFundPrdtType" class="kycSelect"></select>';
						 *
						 * var cell2 = swrfrowobj.insertCell(2);
						 * cell2.style.textAlign="left"; cell2.innerHTML ='<select
						 * name="selFnaSwrepFundCompName" class="kycSelect"></select>';
						 */
						var cell1 = swrfrowobj.insertCell(1);
						cell1.style.textAlign = "left";
						cell1.innerHTML = '<select name="selFnaSwrepFundPrdtType" class="kycSelect" onchange="/*loadPrinOrFundMgr(this)*/"></select>';

						var cell2 = swrfrowobj.insertCell(2);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<select name="selFnaSwrepFundCompName" class="kycSelect" onchange="getPrdtPlanName(this)" style="width:200px" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" ></select>'+
						'<input type="text"  name="txtFldFnaSwrepFundCompName" class="fpEditTblTxt" style="display:inline;width:200px"  maxlength="60">'+
						'<select name="selFnaSwrepFundProdCode" onchange="setPlanName(this)" onclick="hideTooltip();chkCompDets(this);" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" class="kycSelect" style="width:200px"><option value="">--SELECT--</option></select>'+
						'<input type="hidden" name="selFnaSwrepFundPlan"/>'+
						'<img src="images/modify.png" onclick="editSwrepFundName(this)"/>'+
						'<input type="text"  name="selFnaSwrepFundPlanTxt" class="fpEditTblTxt" style="display:none;width:185px"  maxlength="300" onclick="hideTooltip();" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>'+
						'<img src="images/history.png" onclick="resetSwrepFundName(this)" style="display:none"/>'+
						'<select name="selFnaSwrepFundCateg" class="kycSelect" style="width:80px;display:none"><option value="">--SELECT--</option><option value="B">Basic</option><option value="R">Rider</option></select>'+
						'<input type="hidden" name="selFnaSwrepFundOpt" value="CUSTOM"/>';

						var cell3 = swrfrowobj.insertCell(3);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundRisk" class="fpEditTblTxt" style=" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  />';
						cell3.childNodes[0].value = swrepfData[swrpf]["txtFldFnaSwrepFundRisk"];

						var cell4 = swrfrowobj.insertCell(4);
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<select name="selFnaSwrepFundPayment" class="kycSelect"></select>';

						var cell5 = swrfrowobj.insertCell(5);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSoUnit" class="fpEditTblTxt" style="width:120px" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
						cell5.childNodes[0].value = swrepfData[swrpf]["txtFldFnaSwrepFundSoUnit"];

						var cell6 = swrfrowobj.insertCell(6);
						cell6.style.textAlign = "left";//added by Thulasy 16.03.2018
						cell6.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSoPrc" class="fpEditTblTxt" style="width:120px"	maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell6.childNodes[0].value = swrepfData[swrpf]["txtFldFnaSwrepFundSoPrc"];

						var cell7 = swrfrowobj.insertCell(7);
						cell7.style.textAlign = "left";//added by Thulasy 16.03.2018
						cell7.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSiChrg" class="fpEditTblTxt" style="width:80px"	maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell7.childNodes[0].value = swrepfData[swrpf]["txtFldFnaSwrepFundSiChrg"];

						var cell8 = swrfrowobj.insertCell(8);
						cell8.style.textAlign = "left";//added by Thulasy 16.03.2018
						cell8.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSiAmt" class="fpEditTblTxt" style="width:75px" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
						cell8.childNodes[0].value = swrepfData[swrpf]["txtFldFnaSwrepFundSiAmt"];

						var cell9 = swrfrowobj.insertCell(9);
						cell9.style.textAlign = "left";
						cell9.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSiPrc" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';
						cell9.childNodes[0].value = swrepfData[swrpf]["txtFldFnaSwrepFundSiPrc"];

						comboMaker(cell1, fundPrdtArr,swrepfData[swrpf]["selFnaSwrepFundPrdtType"],	false);

						var prdtLob = swrepfData[swrpf]["selFnaSwrepFundPrdtType"];

						// comboMaker(cell2,fundPrdtArr,swrepfData[swrpf]["selFnaSwrepFundCompName"],true);
						if ( prdtLob == "ILP") {
							comboMakerById(cell2.childNodes[0],prinIdArr,swrepfData[swrpf]["selFnaSwrepFundCompName"],true);
						} else if (prdtLob == "UT") {
							comboMakerById( cell2.childNodes[0],fmArr,swrepfData[swrpf]["selFnaSwrepFundCompName"],	true);
						}
						cell2.childNodes[0].value = swrepfData[swrpf]["selFnaSwrepFundCompName"];
						cell2.childNodes[1].value = swrepfData[swrpf]["selFnaSwrepFundCompName"];
						cell2.childNodes[7].value = swrepfData[swrpf]["txtFldFnaSwrepFundBasRid"];
						cell2.childNodes[8].value = planOptsf;

						var planNamesf = swrepfData[swrpf]["selFnaSwrepFundPlan"];
						if(planOptsf == "CUSTOM"){
							cell2.childNodes[0].style.display="none";
							cell2.childNodes[2].style.display="none";
							cell2.childNodes[4].style.display="none";
							cell2.childNodes[5].value=planNamesf;
							cell2.childNodes[5].style.display="";
							cell2.childNodes[6].style.display="none";
						}else{
							cell2.childNodes[2].style.display="";
							cell2.childNodes[4].style.display="";
							cell2.childNodes[5].value="";
							cell2.childNodes[5].style.display="none";
							cell2.childNodes[6].style.display="none";

							if (prdtplanArr.length == 0) {
//								alert("going to call prdt plan name method...");
//								getPrdtPlanName(cell2.childNodes[0]);
							}

//							MAR_2018
//							var tmpArr = new Array();
//							tmpArr[0] = new Option(swrepfData[swrpf]["selFnaSwrepFundProdCode"],swrepfData[swrpf]["selFnaSwrepFundPlan"]);
//							comboMakerById(cell2.childNodes[1], tmpArr,	swrepfData[swrpf]["selFnaSwrepFundPlan"], true);


							cell2.childNodes[2].value = swrepfData[swrpf]["selFnaSwrepFundProdCode"];
							cell2.childNodes[3].value = planNamesf;

						}


						comboMaker(cell4, fundPayMethArr,swrepfData[swrpf]["selFnaSwrepFundPayment"],	true);

						$("#txtFldSwrepFundIaf").val(swrepfData[swrpf]["txtFldSwrepFundIaf"]);

						swrfrowobj.onclick = function() {
							selectSingleRow(this);
						};

						var totalcell = swrfrowobj.cells.length;
						for(var cel=0;cel<totalcell;cel++){
							if(swrfrowobj.cells[cel].childNodes[0]){
								swrfrowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
									e = e || event;
									checkChanges(this);
								},false);
							}
						}

					};

				}

				if (tab == "FNA_CKADET_DATA") {
					// var ckaData = value;
					jsonDataPopulate(value, tab);

				}

				if (tab == "ARCH_FNACLIENTFBT_DATA") {
					jsonDataPopulate(value, tab);
				}
				if (tab == "ARCH_FNACUSTFBREFERAL_DATA") {

					 $("#tblClientFbCustRef tbody tr").remove();
					 $("#tblClientFbAdviserRef tbody tr").remove();

					var fbrefdata = value;

					var fbtblobj = document.getElementById("tblClientFbCustRef");
					var fbtbodyobj = fbtblobj.tBodies[0];

					var fbadvtblobj = document.getElementById("tblClientFbAdviserRef");
					var fbadvtbodyobj = fbadvtblobj.tBodies[0];

					for ( var fb in fbrefdata) {

						var refFor = fbrefdata[fb]["txtFldFbRefCustRefFor"];

						if (refFor == "CUST") {

							var fbrowlenobj = fbtbodyobj.rows.length;

							var fbrowobj = fbtbodyobj.insertRow(fbrowlenobj);

							var cell0 = fbrowobj.insertCell(0);
							cell0.style.textAlign = "left";
							cell0.innerHTML = '<input type = "text"  name="txtFldFnaFbCustName" class="fpEditTblTxt"  maxlength="75"/>';
							cell0.childNodes[0].value = fbrefdata[fb]["txtFldFbRefCustName"];

							var cell1 = fbrowobj.insertCell(1);
							cell1.style.textAlign = "left";
							cell1.innerHTML = '<input type = "text"  name="txtFldFnaFbCustContact" class="fpEditTblTxt"  maxlength="20"/><input type = "hidden"  name="txtFldFnaFbCustMode" value="'+ INS_MODE + '" /><input type = "hidden"  name="txtFldFnaFbCustRefId"/><input type = "hidden"  name="txtFldFnaFbCustRefFor" value="CUST" />';
							cell1.childNodes[0].value = fbrefdata[fb]["txtFldFbRefCustCotact"];
							cell1.childNodes[2].value = fbrefdata[fb]["txtFldFnaFbCustRefId"];

							var cell2 = fbrowobj.insertCell(2);
							cell2.style.textAlign = "left";
							cell2.innerHTML = '<input type = "text"  name="txtFldFnaFbCustEmail" class="fpEditTblTxt kycEmail"  maxlength="80"/>';
							cell2.childNodes[0].value = fbrefdata[fb]["txtFldFbRefCustEmail"];

							var cell3 = fbrowobj.insertCell(3);
							cell3.style.textAlign = "left";
							cell3.innerHTML = '<select name="txtFldFnaFbCustRelation" class="fpSelect"/>';

							comboMaker(cell3, relArray, "", false);

							cell3.childNodes[0].value = fbrefdata[fb]["txtFldFbRefCustRel"];

							var cell4 = fbrowobj.insertCell(4);
							cell4.style.textAlign = "left";
							cell4.innerHTML = '<input type = "text"  name="txtFldFnaFbCustRemarks" class="fpEditTblTxt"  maxlength="300"/>';
							cell4.childNodes[0].value = fbrefdata[fb]["txtFldFbRefCustRemarks"];

							fbrowobj.onclick = function() {
								selectSingleRow(this);
							};

							var totalcell = fbrowobj.cells.length;
							for(var cel=0;cel<totalcell;cel++){
								if(fbrowobj.cells[cel].childNodes[0]){
									fbrowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
										e = e || event;
										checkChanges(this);
									},false);
								}
							}

						} else if (refFor == "ADVISER") {

							var fbadvrowlenobj = fbadvtbodyobj.rows.length;

							var fbadvrowobj = fbadvtbodyobj
									.insertRow(fbadvrowlenobj);

							var cell0 = fbadvrowobj.insertCell(0);
							cell0.style.textAlign = "left";
							cell0.innerHTML = '<input type = "text"  name="txtFldFnaFbAdvName" class="fpEditTblTxt"  maxlength="75"/>';
							cell0.childNodes[0].value = fbrefdata[fb]["txtFldFbRefCustName"];

							var cell1 = fbadvrowobj.insertCell(1);
							cell1.style.textAlign = "left";
							cell1.innerHTML = '<input type = "text"  name="txtFldFnaFbAdvContact" class="fpEditTblTxt"  maxlength="20"/><input type = "hidden"  name="txtFldFnaFbAdvMode" value="'
									+ INS_MODE
									+ '" /><input type = "hidden"  name="txtFldFnaFbCustAdvRefId"  /><input type = "hidden"  name="txtFldFnaFbCustAdvRefFor" value="ADVISER" />';
							cell1.childNodes[0].value = fbrefdata[fb]["txtFldFbRefCustCotact"];
							cell1.childNodes[2].value = fbrefdata[fb]["txtFldFnaFbCustRefId"];

							var cell2 = fbadvrowobj.insertCell(2);
							cell2.style.textAlign = "left";
							cell2.innerHTML = '<input type = "text"  name="txtFldFnaFbAdvEmail" class="fpEditTblTxt kycEmail"  maxlength="80"/>';
							cell2.childNodes[0].value = fbrefdata[fb]["txtFldFbRefCustEmail"];

							fbadvrowobj.onclick = function() {
								selectSingleRow(this);
							};

							var totalcell = fbadvrowobj.cells.length;
							for(var cel=0;cel<totalcell;cel++){
								if(fbadvrowobj.cells[cel].childNodes[0]){
									fbadvrowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
										e = e || event;
										checkChanges(this);
									},false);
								}
							}

						};

					};

					$(".kycEmail").off("change.email").on("change.email",function(){
						validateEmailComm(this);
				    });
				};

				if(tab == "ARCH_FNADISCLOSURE_DATA"){

					var discdata = value;

					for ( var dd in discdata) {

						var chklistid = discdata[dd]["txtFldFnaDisChkLiId"];
						var chflg = discdata[dd]["txtFldFnaDisChkLiFlg"];

						for(var i=1;i<=13;i++){

							if(document.getElementById("chkDis"+i).getAttribute("datavalue")== chklistid && chflg == "Y"){
								document.getElementById("chkDis"+i).checked=true;
								document.getElementById("chkDis"+i).value="Y";
								document.getElementById("chkDis"+i).nextSibling.value=chklistid;
							}else{
								if(chflg != "Y"){
									document.getElementById("chkDis"+i).checked=false;
								};
							};
						}



					}
				}
				
				if(tab == "ARCH_RISKPREFDETS_DATA"){
					var riskdata = value;
					for ( var dd in riskdata) {
						
						var crid = riskdata[dd]["txtFldCrId"]; 
						
						var tblobjrp = document.getElementById("fnaRiskPrefTbl");
						var tbodyobjrp = tblobjrp.tBodies[0];
						
						var rowlenobjrp = tbodyobjrp.rows.length;
						var jqRwLen=$("#fnaRiskPrefTbl tbody tr:visible").length;
						var rowobjrp = tbodyobjrp.insertRow(rowlenobjrp);
						
						var cell0rp = rowobjrp.insertCell(0);
						cell0rp.innerHTML =  '<input type="text" readonly="readonly" name="txtFldFnaRiskSino" value="'+(Number(jqRwLen)+1)+'" style="width:92%;text-align:center;"/>'	
						cell0rp.style.textAlign = "center";
						
						var cell0a = rowobjrp.insertCell(1);
						cell0a.innerHTML =  '<select class="kycSelect"  name="selFnaRiskFor" disabled="disabled"><option value="">--SELECT--</option><option value="CLIENT">CLIENT</option><option value="SPOUSE">SPOUSE</option></select>';	
						cell0a.style.textAlign = "center";
						$(cell0a).find("select:first").val(riskdata[dd]["selCrFor"]);
						
						var cell1 = rowobjrp.insertCell(2);
						cell1.style.textAlign = "left";
						cell1.innerHTML ='<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskInvObj" value="'+riskdata[dd]["txtFldCrInvstobj"]+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:98%;"/>'
								+ '<input type="hidden" name="txtFldFnaRiskMode" value="'+ INS_MODE+ '"/>'
								+ '<input type="hidden" name="txtFldFnaRiskId"/>';	//crid
						
						
						var cell2 = rowobjrp.insertCell(3);
						cell2.style.textAlign = "left";
						cell2.innerHTML = '<input type = "text" class="fpEditTblTxt"  name="txtFldFnaRiskAmtInv" value="'+riskdata[dd]["txtFldCrInvstamt"]+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
						
						var cell3 = rowobjrp.insertCell(4);
						cell3.style.textAlign = "left";
						cell3.innerHTML = '<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskTimeHr" value="'+riskdata[dd]["txtFldCrInvsttimehorizon"]+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
						
						var cell4 = rowobjrp.insertCell(5); 
						cell4.style.textAlign = "left";
						cell4.innerHTML = '<select class="kycSelect"  name="selFnaRiskInvObj" style="width:60%;"><option value="">--SELECT--</option><option value="Y">Yes</option><option value="S">Some</option><option value="N">No</option></select>'
										+ '<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskRp"  onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:34%;"/>';
						$(cell4).find("select:first").val(riskdata[dd]["selCrRiskpref"]);
						$(cell4).find("input:first").val(riskdata[dd]["txtFldCrRiskclass"]);
						
						var cell5 = rowobjrp.insertCell(6);
						cell5.style.textAlign = "left";
						cell5.innerHTML = '<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskROI" value="'+riskdata[dd]["txtFldCrRoi"]+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:98%;"/>'			
						
						var cell6 = rowobjrp.insertCell(7);
						cell6.style.textAlign = "left";
						cell6.innerHTML = '<select class="fpEditTblTxt"   name="SelFnaRiskAdequateFnds" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" ><option value="">--select--</option><option value="Y">YES</option><option value="N">NO</option></select>';
						$(cell6).find("select:first").val(riskdata[dd]["selCrAdeqfund"]);
						
						var cell6 = rowobjrp.insertCell(8);
						cell6.style.textAlign = "left";
						cell6.innerHTML = '<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskOthrCon" value="'+riskdata[dd]["txtFldCrOthconcern"]+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
					

						
					}
				}
				
				if(tab == "ALL_SIGNATURE_DETS"){
					
//					var page1_clientsign1 = document.getElementById("page1_clientsign");
//					page1_clientsign1.setAttribute("src", 'images/esign-icon.png');
//					page1_clientsign1.setAttribute("class", "esignclientsign");
//					page1_clientsign1.setAttribute("onclick", "openSignaturePad('client')");
					
					reloadSignatureParam("page1_clientsign", "client","INDEXPAGE");
					reloadSignatureParam("taxpage_clientsign", "client","TAXPAGE");
					reloadSignatureParam("signpage_clientsign", "client","SIGNPAGE");
					
//					var page12_clientsign = document.getElementById("page12_clientsign");
//					page12_clientsign.setAttribute("src", 'images/esign-icon.png');
//					page12_clientsign.setAttribute("class", "esignclientsign");
//					page12_clientsign.setAttribute("onclick", "openSignaturePad('client')");
					
					reloadSignatureParam("page12_clientsign", "client","DECLAREPAGE");
					
//					var page1_spousesign = document.getElementById("page1_spousesign");
//					page1_spousesign.setAttribute("src", 'images/esign-icon.png');
//					page1_spousesign.setAttribute("class", "esignclientsign");
//					page1_spousesign.setAttribute("onclick", "openSignaturePad('spouse')");
					
					reloadSignatureParam("page1_spousesign", "spouse","INDEXPAGE");
					reloadSignatureParam("taxpage_spousesign", "spouse","TASXPAGE");
					reloadSignatureParam("signpage_spousesign", "spouse","SIGNPAGE");
					
//					var page12_spousesign = document.getElementById("page12_spousesign");
//					page12_spousesign.setAttribute("src", 'images/esign-icon.png');
//					page12_spousesign.setAttribute("class", "esignclientsign");
//					page12_spousesign.setAttribute("onclick", "openSignaturePad('spouse')");
					
					reloadSignatureParam("page12_spousesign", "spouse","DECLAREPAGE");
					
					
//					var page13_advisersign = document.getElementById("page13_advisersign");
//					page13_advisersign.setAttribute("src", 'images/esign-icon.png');
//					page13_advisersign.setAttribute("class", "esignclientsign");
//					page13_advisersign.setAttribute("onclick", "openSignaturePad('adviser')");
					
					reloadSignatureParam("page13_advisersign", "adviser","DECLAREPAGE");
					reloadSignatureParam("signpage_advisersign", "adviser","SIGNPAGE");
					
//					var page13_managersign = document.getElementById("page13_managersign");
//					page13_managersign.setAttribute("src", 'images/esign-icon.png');
//					page13_managersign.setAttribute("class", "esignclientsign");
//					page13_managersign.setAttribute("onclick", "openSignaturePad('manager')");
					
					reloadSignatureParam("page13_managersign", "manager","DECLAREPAGE");
					var signstatusfld="hTxtFldCustSignStatus";
					
					$.each(value,function(k,v){
						
						if(v.hasOwnProperty("CLIENT_SIGNED") && v.CLIENT_SIGNED =='Y'){
							
							console.log("------>"+v.CLIENT_SIGN_PAGEREF);
							
							if(v.CLIENT_SIGN_PAGEREF == "INDEXPAGE"){
								loadSignatureFromJSON(v, "page1_clientsign", "msg_client_sign_1", "txtFldClntSignDatePage1");
//								clnt_img_1
								$("#clnt_img_1").attr('src', 'images/ok.png');
								
							}
							
							
							if(v.CLIENT_SIGN_PAGEREF == "TAXPAGE"){
								loadSignatureFromJSON(v, "taxpage_clientsign", "msg_client_sign_tax", "txtFldClntSignDateTax");
								$("#clnt_img_11").attr('src', 'images/ok.png');
								
							}
							
							if(v.CLIENT_SIGN_PAGEREF == "SIGNPAGE"){
								loadSignatureFromJSON(v, "signpage_clientsign", "msg_client_sign_signpage", "txtFldClntSignDateSignpage");
								$("#clnt_img_12").attr('src', 'images/ok.png');
							}

							if(v.CLIENT_SIGN_PAGEREF == "DECLAREPAGE"){
								loadSignatureFromJSON(v, "page12_clientsign", "msg_client_sign_declarepage", "txtFldClntSignDateDeclare");
								$("#clnt_img_13").attr('src', 'images/ok.png');
							}
							
//							var page1_clientsign = document.getElementById("page1_clientsign");
//							page1_clientsign.setAttribute("src", 'data:image/png;base64,'+v.CLIENT_SIGN);
//							page1_clientsign.setAttribute("class", "esignclientsigndone");
//							page1_clientsign.setAttribute("onclick", "");
//							page1_clientsign1.style.display="block";
//							$("#msg_client_sign_1").hide();
//							
//							$("#page1_clientsign").bind('contextmenu', function(e) {
//							    return false;
//							}); 
//							
//							$("input[name='txtFldClntSignDate']").val(v.CLIENT_SIGN_DATE);
//							
//							page1_clientsign.setAttribute("disabled", true);
							
							
							
//							var page12_clientsign = document.getElementById("page12_clientsign");
//							page12_clientsign.setAttribute("src", 'data:image/png;base64,'+v.CLIENT_SIGN);
//							page12_clientsign.setAttribute("class", "esignclientsigndone");
//							page12_clientsign.setAttribute("onclick", "");
//							
//							$("#page12_clientsign").bind('contextmenu', function(e) {
//							    return false;
//							}); 
//							
//							$("input[name='txtFldClntSignDate']").val(v.CLIENT_SIGN_DATE);
//							
//							page12_clientsign.setAttribute("disabled", true);
							
//							$("#hTxtFldCustSignStatus").val("Y");//REPORT

							setSignatureStatus(signstatusfld,v.CLIENT_SIGN_PAGEREF);
							
						}
						
						
						if(v.hasOwnProperty("ADVISER_SIGNED") && v.ADVISER_SIGNED == 'Y'){
							
							if(v.ADVISER_SIGN_PAGEREF == "SIGNPAGE"){
								loadSignatureFromJSON(v, "signpage_advisersign", "msg_adviser_sign_signpage", "txtFldAdvSignDateSignpage");
								$("#adv_img_12").attr('src', 'images/ok.png');
							}
							
							if(v.ADVISER_SIGN_PAGEREF == "DECLAREPAGE"){
								loadSignatureFromJSON(v, "page13_advisersign", "msg_adviser_sign_declarepage", "txtFldAdvSignDateDeclarepage");
								$("#adv_img_13").attr('src', 'images/ok.png');
							}
							
							
//							Adviser						
//							var page13_advisersign = document.getElementById("page13_advisersign");
//							page13_advisersign.setAttribute("src", 'data:image/png;base64,'+v.ADVISER_SIGN);
//							page13_advisersign.setAttribute("class", "esignclientsigndone");
//							page13_advisersign.setAttribute("onclick", "");
//							$(".msg_adviser_sign").hide();
//							
//							$("#page13_advisersign").bind('contextmenu', function(e) {
//							    return false;
//							}); 
//							
//							$("input[name='txtFldAdvSignDate']").val(v.ADVISER_SIGN_DATE);
//							
//							page13_advisersign.setAttribute("disabled", true);

//							$("#hTxtFldAdvSignStatus").val("Y");
							
							setSignatureStatus("hTxtFldAdvSignStatus",v.ADVISER_SIGN_PAGEREF);
							
							
							
							
						}
						
						
						if(v.hasOwnProperty("MANAGER_SIGNED") && v.MANAGER_SIGNED == 'Y'){
							
							if(v.MANAGER_SIGN_PAGEREF == "DECLAREPAGE"){
								loadSignatureFromJSON(v, "page13_managersign", "msg_manager_sign_Declarepage", "txtFldMgrSignDateDeclarepage");
								$("#mgr_img_13").attr('src', 'images/ok.png');
							}
							
//							var page13_managerrsign= document.getElementById("page13_managersign");
//							page13_managerrsign.setAttribute("src", 'data:image/png;base64,'+v.MANAGER_SIGN);
//							page13_managerrsign.setAttribute("class", "esignclientsigndone");
//							page13_managerrsign.setAttribute("onclick", "");
//							$(".msg_manager_sign").hide();
//							
//							$("#page13_managerrsign").bind('contextmenu', function(e) {
//							    return false;
//							}); 
//							
//							$("input[name='txtFldMgrSignDate']").val(v.MANAGER_SIGN_DATE);
//							page13_managerrsign.setAttribute("disabled", true);
							
							$("#hTxtFldMgrSignStatus").val("Y");
							
							$("#btnUpdateStatusForMgr").show();
							
							$("#btnUpdateStatusForMgr").on("click",function(){
								updateManagerStatus('manager',true);
							})
							
							
							
						}
						
						if(v.hasOwnProperty("SPOUSE_SIGNED") && v.SPOUSE_SIGNED == 'Y'){
							
							
//							var page12_spousesign= document.getElementById("page12_spousesign");
//							page12_spousesign.setAttribute("src", 'data:image/png;base64,'+v.SPOUSE_SIGN);
//							page12_spousesign.setAttribute("class", "esignclientsigndone");
//							page12_spousesign.setAttribute("onclick", "");
//							$(".msg_spouse_sign").hide();
//							
//							$("#page12_spousesign").bind('contextmenu', function(e) {
//							    return false;
//							}); 
//							
//							
//							var page1_spousesign= document.getElementById("page1_spousesign");
//							page1_spousesign.setAttribute("src", 'data:image/png;base64,'+v.SPOUSE_SIGN);
//							page1_spousesign.setAttribute("class", "esignclientsigndone");
//							page1_spousesign.setAttribute("onclick", "");
//							page1_spousesign.style.display="block";
//							
//							$("#page1_spousesign").bind('contextmenu', function(e) {
//							    return false;
//							}); 
//							
//							$("input[name='txtFldSpsSignDate']").val(v.SPOUSE_SIGN_DATE);
//							page12_spousesign.setAttribute("disabled", true);
//							page1_spousesign.setAttribute("disabled", true);
							
							
							
							if(v.SPOUSE_SIGN_PAGEREF == "INDEXPAGE"){
								loadSignatureFromJSON(v, "page1_spousesign", "msg_spouse_sign_1", "txtFldSpsSignDatePage1");
								$("#sps_img_1").attr('src', 'images/ok.png');
								
							}
							
							
							if(v.SPOUSE_SIGN_PAGEREF == "TAXPAGE"){
								loadSignatureFromJSON(v, "taxpage_spousesign", "msg_spouse_sign_tax", "txtFldSpsSignDateTax");
								$("#sps_img_11").attr('src', 'images/ok.png');
							}
							
							if(v.SPOUSE_SIGN_PAGEREF == "SIGNPAGE"){
								loadSignatureFromJSON(v, "signpage_spousesign", "msg_spouse_sign_signpage", "txtFldSpsSignDateSignpage");
								$("#sps_img_12").attr('src', 'images/ok.png');
							}

							if(v.SPOUSE_SIGN_PAGEREF == "DECLAREPAGE"){
								loadSignatureFromJSON(v, "page12_spousesign", "msg_spouse_sign_declarepage", "txtFldSpsSignDateDeclare");
								$("#sps_img_13").attr('src', 'images/ok.png');
							}
							
//							$("#hTxtFldSpsSignStatus").val("Y");
							
							setSignatureStatus("hTxtFldSpsSignStatus",v.SPOUSE_SIGN_PAGEREF);
						}
						
						
					});
					
				}
				
				
			};
		};
	}

	if (FNA_CUSTCATEG == CUST_CATEG_COMPANY) {
		blockSpsForCompany();
	}

	// fnaForm.dfSelfGender.value = fnaForm.dfSelfGender.value == "M" ?
	// "MALE" : fnaForm.dfSelfGender.value == "F" ? "FEMALE" : "";
	// fnaForm.dfSpsGender.value = fnaForm.dfSpsGender.value == "M" ?
	// "MALE" : fnaForm.dfSpsGender.value == "F" ? "FEMALE" : "";

	fnaForm.dfSelfAge.value = calcAge(fnaForm.dfSelfDob.value);
	fnaForm.dfSpsAge.value = calcAge(fnaForm.dfSpsDob.value);

	 /*chkNextAgeBirthday(); commented by kavi 17/03/2018*/

	fnaForm.selKycArchStatus.value = obj.parentNode.parentNode.cells[4].childNodes[0].value;
	// fnaForm.selKycSentStatus.value =
	// obj.parentNode.parentNode.cells[5].childNodes[0].value;
	fnaForm.selKycSentStatus.value = "";
	fnaForm.selKycMgrApprStatus.value = obj.parentNode.parentNode.cells[6].childNodes[0].value;
	// fnaForm.selKycMgrApprStatus.value = "";
	// fnaForm.txtFldMgrStsRemarks.value =
	// obj.parentNode.parentNode.cells[7].childNodes[0].value;
	fnaForm.txtFldMgrStsRemarks.value = "";
	fnaForm.txtFldMgrApprDate_1.value = unescape(obj.parentNode.parentNode.cells[8].childNodes[0].value);
	fnaForm.chkArchClntConsent.value = selectedFnaClntCon == true ? "Y" : "N";

	fnaForm.txtFldfnaDate.value = currDate;

	// This element in layout page and not in form element, so getting id and
	// assigned value.
	document.getElementById("selArchSts").value = fnaForm.selKycArchStatus.value;
	document.getElementById("selArchNo").value = obj.parentNode.parentNode.cells[1].childNodes[1].value;
	document.getElementById("htxtFldDisFormType").value = obj.parentNode.parentNode.cells[11].childNodes[0].value;
	// document.getElementById("selArchSts").disabled = true;

//	chkforOthers(fnaForm.radCDClntMailngAddr_032018, 'txtFldCDMailAddOth_032018', 'OTH'); ;//MAR_2018
	chkforOthers(fnaForm.swrepConflg, 'swrepConfDets', 'Y');
	enabOrDisabTaxResid('fnaFatcaTbl');// added by johnson 26052015

	document.getElementById("txtFldKycForm").value = FNA_FORMTYPE;


	if(isEmpty($('input[name="txtFldSpouseName"]').val())){
    	$('input[name="txtFldSpouseName"]').removeClass('readOnlyText').addClass("writeModeTextbox")
    	.prop("disabled",false).prop('readonly',false);
    }else{
    	$('input[name="txtFldSpouseName"]').removeClass('writeModeTextbox').addClass("readOnlyText")
    	.prop("disabled",false).prop('readonly',true);
    }

	if(!isEmpty($("#selFnaSelectedMgrId").val())){
		$("#txtFldSupVName").val($("#selFnaSelectedMgrId option:selected").text());
	}else{
		$("#txtFldSupVName").val("");
	}

	if($("#dfSelfSameasregadd").is(":checked")){
		$("#dfSelfMailaddrflg,#dfSpsMailaddrflg").prop("disabled",true);
	}

	$(".kycDisabledFld").removeClass("writeModeTextbox").addClass("readOnlyText").prop("readonly",true);

	if(!isEmpty($("#ntucCaseId").val())){
		$("[name=txtFldFnaFatcaMode]").val(UPD_MODE);
		$("[name=txtFldFnaDepMode]").val(UPD_MODE);
		$("[name=hFnaExistPolMode]").val(UPD_MODE);
		$("[name=txtFldFnaExistPolMode]").val(UPD_MODE);
		$("[name=txtFldFnaArtuPlanMode]").val(UPD_MODE);
		$("[name=txtFldFnaArtuFundMode]").val(UPD_MODE);
		$("[name=txtFldFnaSwrepPlanMode]").val(UPD_MODE);
		$("[name=txtFldFnaSwrepFundMode]").val(UPD_MODE);
		$("[name=txtFldFnaFbCustMode]").val(UPD_MODE);
		$("[name=txtFldFnaFbAdvMode]").val(UPD_MODE);
	}
	
	
	slctDslectAll();
	
	var page2clientName = $("#dfSelfName").val();
	var page2SpouseName = $("#dfSpsName").val();
	
	$("#selCrRiskprefFor_Top option").each(function() {
		  if($(this).val() == "CLIENT") {
		    $(this).text("CLIENT - " + page2clientName);            
		  }                        
		});
	$("#lblPage6Client").text("CLIENT - " +page2clientName);
	$('.questions').accordion("refresh");    
	
	if(!isEmpty(page2SpouseName)){
		$("#divASpouseSign").show();
		$("#signsts_sps_div").removeClass("disabledivs");
	}else{
		$("#divASpouseSign").hide();
		$("#signsts_sps_div").addClass("disabledivs");
	}
	
	
	
	setTimeout(function(){
//		var mgrmailsent = $("#mgrEmailSentFlg").val();
//		var custsignstats = $("#hTxtFldCustSignStatus").val();
//		var ntuccaseid = $("#ntucCaseId").val();
//		var ntucexist = chkAnyNTUCExist();
//		var totalclientsign=4,clientsigned=0;
		
		var isClientSignDone = chkClientSignatureDone();
		var isAdviserSignDone = chkAdviserSignatureDone();
//		if(isEmpty(ntuccaseid) && !ntucexist && mgrmailsent != "Y"){
			
//			var val_=JSON.parse(custsignstats);
//			if (!isEmpty(custsignstats) && !jQuery.isEmptyObject(val_)) {
//				
//				$.each(val_,function(k,v){
//					if(k=='INDEXPAGE' && v=='Y'){
//						clientsigned++;
//					}
//					
//					if(k=='TAXPAGE' && v=='Y'){
//						clientsigned++;
//					}
//					
//					if(k=='SIGNPAGE' && v=='Y'){
//						clientsigned++;
//					}
//					
//					if(k=='DECLAREPAGE' && v=='Y'){
//						clientsigned++;
//					}
//				});
//				
//			}
//			console.log("client signaturer compare ->"+totalclientsign+","+clientsigned)
//			if(totalclientsign == clientsigned){
			if(isClientSignDone && isAdviserSignDone){
//				 if( window.confirm("Do you want to send this FNA Form to manager review?")){
//					  sendMailtoManger();
//				  }
			}	
//		}
		
		
		var servAdvId = $("#txtFldClntServAdv").val();
		var servAdvMgrId = $("#txtFldMgrId").val();
		var mgracs = $("#hTxtFldLoggedAdvMgrAcs").val();
//		var loggeduserid = $("#hTxtFldLoggedAdvUserId").val();
		var loggedadvstfid = $("#hTxtFldLoggedAdvStfId").val();
		
//		alert( LOG_USER_MGRACS +","+ FNA_CUSTSERVADVID +","+ LOG_USER_ADVID);		
//		LOG_USER_MGRACS == "Y" && FNA_CUSTSERVADVID == LOG_USER_ADVID && LOG_USER_DESIG == DESIG_GMR

		if (LOG_USER_MGRACS == "Y" && LOG_USER_ADVID == servAdvMgrId ) {
			$("#tr_managersection").find(":input").prop("disabled",false);
//			$("#tr_managersection").find(":input").val("");//dec2019s
//			$("#tr_managersection").find("input[type='radio']").prop("checked",false);//dec2019
			
			var agree = $("input[name=suprevMgrFlg]:eq(0)");
			var disagree = $("input[name=suprevMgrFlg]:eq(1)");
			
			var agreeflg = agree.prop("checked");var disagreeflg = disagree.prop("checked");
			
			if(agreeflg && !disagreeflg){
				$("input[name=suprevMgrFlg]").prop("disabled",true);
			}else if(disagreeflg && !agreeflg){
				$("input[name=suprevMgrFlg]").prop("disabled",false);
			}

			$("#tabs-arch-li").css("display", "none");
			$("#imgMgrSign").click(function(){
				openSignaturePad('manager');
			});
			
		}else{
			$("#tr_managersection").find(":input").prop("disabled",true);
			$("#page13_managersign").css({"cursor": "not-allowed","pointer-events": "none"});
			$("#page13_managersign").closest("div").css({"cursor": "not-allowed"});
		}
		
//		button rework
		
//		var advisersignstats = $("#hTxtFldAdvSignStatus").val();
//		var mgrmailsent = $("#mgrEmailSentFlg").val();
//		var custsignstats = $("#hTxtFldCustSignStatus").val();
//		var ntuccaseid = $("#ntucCaseId").val();
//		var ntucexist = chkAnyNTUCExist();
		
//		var clientsigned=sofarClientSignatureDone(),totalclientsign=4;
//		
//		var advisersigned = chkAdviserSignatureDone();
//		
//		if(clientsigned != totalclientsign){
//			$("#btnMgrApprResend").prop("disabled",true);	
//		}
//		
//		if(!advisersigned){
//			$("#btnMgrApprResend").prop("disabled",true);	
//		}
//		var sentsts = $("#hTxtFldSentToMgr").val();
//		if(clientsigned == totalclientsign && advisersigned ){//&& sentsts == "Not Sent" && !ntucexist
//			$("#btnMgrApprResend").prop("disabled",false);
//			$("#btnMgrApprResend").prop("value","Send Now!");
//		}else{
//			$("#btnMgrApprResend").prop("disabled",true);	
//		}
		
		
	}, 200);
}

function allPageValidation() {

//	alert("allPageValidation")

	if (!validatePageOne())
		return;
//	 changed the functionality calling place by johnson 20160314
	if (FNA_FORMTYPE == SIMPLIFIED_VER) {
		if (!validateFnaSelfSpsDets(false))
			return;
	}

//	The following lines are Commentted MAR_2018

	


//	alert("validateFnaDepentDets finished")

//	if (FNA_FORMTYPE == ALL_VER) {
//
//		if (!validateFnaInvGoal())
//			return;
//
//		if (!validateFnaFlowDets())
//			return;
//
//		if (!validateFnaDepntCont())
//			return;
//
//		if (!validateFnaHlthInsDet())
//			return;
//
//		if (!validateFnaPropOwnDet())
//			return;
//
//		if (!validateFnaVehiOwnDet())
//			return;
//
//		// if(!validateFnaAdvRecPrdtDet())return;
//
//	}// end of if FNA_FORMTYPE

	/*if (!validateMailAddr())
		return;
//Commentted on MAR_2018
	*/

	if (!validIntrPreFun(false))
		return;
	
	if (!validateTPPBenifPEP(false))
		return;

	if (!validateFnaDepentDets(false))
		return;

	if(!validateFnaWellnesReview(false))
		return;

	
//	Sep_2019
//	if (!validateFnaExistPolDets(false))
//		return;
	
//	Not required
//	if (!validateFnaRiskPrefDets(false))
//		return;
	
	

	if (!validateFnaArtuPlantDet(false))
		return;

	if (!validateFnaArtuFundtDet(false))
		return;

	if (!validateFnaSwrepPlantDet(false))
		return;

	if (!validateFnaSwrepFundtDet(false))
		return;

	if (!validateReasonforRecomm(false))
		return;
	
	if (!validateUSTaxPrsn(false))
		return;

	if (!validateFnaFatcaTaxDets(false))
		return;


	if (!advDeclrAndSupReview(false))
		return;

	if (!validateFnaCustFeedback(false))
		return;

	if (!validateFnaCustFeedbackAdv(false))
		return;

	return true;

}// end of allPageValidation

function insertkycdets() {

	if (LOG_USER_STFTYPE == STFTYPE_STAFF) {
		alert("Access Denied. STAFF cannot register the KYC. ");
		return false;
	}

//	if (!allPageValidation())
//		return;
	
//		var flag = false;
	
//		var advisersignstats = $("#hTxtFldAdvSignStatus").val();
//		var custsignstats = $("#hTxtFldCustSignStatus").val();
		
		var clientsigned=sofarClientSignatureDone();
		if( clientsigned > 0 ){
			
//			var message = " You have modified the current FNA Form.<br/>This FNA Form is already signed by Client(1)/Client(2).<br/>Please sign the FNA Form again!.";
//			var messageNoModify = " This FNA Form is already signed by Client(1)/Client(2).<br/>Please sign the FNA Form again!.";
			
			var checkChange=$("#chkanychange").val();

			if(!isEmpty(checkChange) && !$.isEmptyObject($.parseJSON(checkChange)) && NTUCChkChngSts){
				
				
				 flg = chkAllDataSaved("",false,FNA_CONS_MODIFY_SIGN_INFO);
				 
				 $("#reportvaliationPopup table:first tbody>tr").not(":first").hide();
					$( "#reportvaliationPopup" ).dialog( "option", "height",280);
					$( "#reportvaliationPopup" ).dialog( "option", "width",500);

				 
				 $('#reportvaliationPopup .ui-dialog-buttonset button:first').remove();

				 $( "#reportvaliationPopup" ).dialog( "option", "buttons",
							[
							    {
							      text: "Proceed to Save",
							      click: function() {
											$('#reportvaliationPopup').dialog("close");
//											$("#saveBtn-button").click();
											
											saveOrUpdate();
							      }

							    },
							    {
							    	text : "Cancel",
							    	click : function(){
							    		$('#reportvaliationPopup').dialog("close");
//									  	$("#tabs-template-li").hide();
							    	}
							    }
							 ]
					);
				 
				
				
			}else{
				
				
				var dialog = $("#reportvaliationPopup").dialog({
					autoOpen : false,
					height : '550',
					width : '900',
					modal : true,
					resizable : false,
					// screen center
					my : "center",
					at : "center",
					of : window,
					// screen center
//					buttons :[{ " Proceed to Save " : function() { dialog.dialog("close"); saveOrUpdate(); }},
//					          { " Cancel " : function() { dialog.dialog("close"); }}], 
					close : function() {
						dialog.dialog("close");
					}
				});
				dialog.dialog("open")
				
				$( "#reportvaliationPopup" ).dialog( "option", "buttons",
						[
						    {
						      text: "Proceed to Save",
						      click: function() {
										$('#reportvaliationPopup').dialog("close");
										saveOrUpdate();
						      }

						    },
						    
						    
						    {
							      text: "Cancel",
							      click: function() {
										$('#reportvaliationPopup').dialog("close");
							      }

							    } 
						 ]
				);

				 $('#reportvaliationPopup').dialog('option', 'title', 'eKYC - Signature Notification');
					$( "#reportvaliationPopup" ).dialog( "option", "height",280);
					$( "#reportvaliationPopup" ).dialog( "option", "width",650);
				$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();
				$("#page16msg").html("");
				$("#page16msg").html( '<h3 style="color: red;font-size: 15pt;"></h3>');
				$("#reportvaliationPopup table:first tbody tr:first h3:first").html(FNA_CONS_SIGN_CLEAR_CONF)

				dialog.dialog("open");
				
			}
		}else{
			saveOrUpdate();
		}
	
	
	

}

function saveOrUpdate(){
	
	
	if (!allPageValidation())
		return;
	
	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");

	loadMsg.style.display = "block";
	// alert(loadMsg.style.display)
	loadingLayer.style.display = "block";
	
	
	var strArchSts = fnaForm.selKycArchStatus.value;
	var isntuccaseLock =  $("#ntucCaseLock").val();
	var ntuccaseLockno =  $("#ntucLockedCase").val();
	var ntuccaseid =  $("#ntucCaseId").val();
	var ntuccasestatus =  $("#ntucCaseStatus").val();
	
//	alert("Going to update")
	var kycupdaction = "KycPostUpdate.action";
	var kycinsaction = "KycPost.action";

	if(!isEmpty(ntuccaseid)){

		if (FNA_FORMTYPE == SIMPLIFIED_VER) {
			copyValuesClntDecltoPers();
		}

		enableAllFields();

		$(".kycFinCommitFlds").attr("readonly",false);
		$(".kycFinCommitFldsTot").attr("readonly",false);

		if(ntuccasestatus.toUpperCase() == "APPROVED" || ntuccasestatus.toUpperCase() == "REJECTED"){
			
			$("#ntucCaseId").val("");
			$("#ntucCaseStatus").val("");
			$(".workflowfields").val("");
			$("input[name=suprevMgrFlg]").prop('checked',false).val("")
			document.forms[0].action = kycinsaction;
			document.forms[0].submit();
		}else{
			document.forms[0].action = kycupdaction;
			document.forms[0].submit();		
		}
	

	}else{


//		if (strArchSts == STR_ARCHSTS_FINAL) {
//		if(isntuccaseLock.toUpperCase() == "LOCKED" && !isEmpty(ntuccaseLockno)){-- this scenarion never happens
		if(isntuccaseLock.toUpperCase() == "LOCKED" && !isEmpty(ntuccaseLockno)){

			enableAllFields();
			/*$("select[name='selFnaArtuPlanName']").css("display","");
			$("select[name='selFnaArtuFundProdCode']").css("display","");
			$("select[name='selFnaSwrepPlanName']").css("display","");
			$("select[name='selFnaSwrepFundProdCode']").css("display","");
			$("select[name='selFnaSwrepFundCateg']").css("display","");
			$("select[name='selFnaArtuFundCatage']").css("display","");*/

			document.forms[0].action = "KycPostUpd.action";
			document.forms[0].submit();

		} else {
//			 alert("else");
			if (isEmpty(fnaForm.selKycArchStatus.value))
				fnaForm.selKycArchStatus.value = STR_ARCHSTS_OPEN;

//			alert(fnaForm.selKycArchStatus.value);

			fnaForm.selKycMgrApprStatus.value = "";
			fnaForm.txtFldMgrStsRemarks.value = "";
			
			$(".workflowfields").val("");
			$("input[name=suprevMgrFlg]").prop('checked',false).val("")

			if (FNA_FORMTYPE == SIMPLIFIED_VER) {
				copyValuesClntDecltoPers();
			}
//			alert("values are copied!")

			enableAllFields();

		/*	$("select[name='selFnaArtuPlanName']").css("display","");
			$("select[name='selFnaArtuFundProdCode']").css("display","");
			$("select[name='selFnaSwrepPlanName']").css("display","");
			$("select[name='selFnaSwrepFundProdCode']").css("display","");
			$("select[name='selFnaSwrepFundCateg']").css("display","");
			$("select[name='selFnaArtuFundCatage']").css("display","");
		*/
			document.forms[0].action = kycinsaction;
			document.forms[0].submit();
		}

	}


	
}

function genreportkyc() {

	var fnaform = document.fnaForm;
	var fnaid = fnaform.txtFldFnaId.value;
	var custid = fnaform.txtFldCustId.value;
	var formtype = document.getElementById("txtFldKycForm").value;
	
	
	 

	if(isEmpty(fnaid)){

	}else{
//		 $("#loadingMessage").show();

		var machine = "";
		machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC //
				+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaid+"&__format=pdf"
				+ "&P_PRDTTYPE=" + custLobDets
				+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + formtype
				+ "&P_DISTID=" + LOGGED_DISTID;

		if(!chkAllDataSaved(machine,true,FNA_CONS_MODIFY_PDF)){
			return;
		}
//		 $("#loadingMessage").hide();
	}





//	creatSubmtDynaReportPDF(machine);

}

function removeTblRows(tblId) {

	var tBodyObj = document.getElementById(tblId).tBodies[0];
	var len = tBodyObj.rows.length;
	if (len > 0) {
		for (var i = len; i > 0; i--) {
			tBodyObj.removeChild(tBodyObj.rows[0]);
		}
	}
}

function EmailModalPopup(txtHtml, EmailContent, EmailId, ServMailId, Remarks,
		subject, filename, dispFileName) {

	if (ServMailId == "null") {
		ServMailId = "";
	}
	if (EmailId == "null") {
		EmailId = "";
	}

	txtHtml = "<form id='hiddenFormAttch' method='POST' target='TheAttachmentWindow' enctype='multipart/form-data'>"
			+ "<table cellspacing='0' cellpadding='0' width='95%' align='center'>"
			+ "<tr><td><fieldset class='fieldsetBorder'><legend class='legendTxt'>Send Email</legend>"
			+ "<table cellspacing='0' cellpadding='0' width='100%' align='center'>"
			+ "<tr valign='middle'><td align='right'>Email&nbsp;To:</td>"
			+ "<td><input type='text' size='100'  id='txtFldEmailTo' maxlength='150' value='"
			+ ServMailId
			+ "' name='txtFldEmailTo'/><img	src='images/staffEmail.jpg' height='25px' width='' style='cursor:pointer' onclick='showEmailIdLayout(this)' title='Staff EmailIds'/></td>"
			+ "</tr>"
			+ "<tr valign='middle'><td align='right'>Email&nbsp;To&nbsp;CC:</td>"
			+ "<td><input type='text' size='100' maxlength='500' id='txtFldEmailToCC' value='"
			+ EmailId
			+ "' name='txtFldEmailToCC'/><img src='images/staffEmail.jpg' height='25px' width='' style='cursor:pointer' onclick='showEmailIdLayout(this)' title='Staff EmailIds'/></td></tr>"
			+ "<tr><td align='right' > Subject:</td><td width='90%' ><input type='text'	 size='104' id='txtFldSubject' maxlength='75' value='"
			+ subject
			+ "' name='txtFldSubject'/></td></tr>"
			// + "<tr><td align='right' > Attachment : </td><td width='90%'
			// valign='top' ><img src='images/pdf.jpg' height='25px'
			// id='mailAttachpdf' width='25px' /><input type='text'
			// class='fptransText' name='attachmentName' id='attachmentName'
			// size='100' readOnly='true' value='"+dispFileName+"'/><input
			// type='hidden' id='txtFldMailAttach' size='90'
			// value='"+filename+"'/></td></tr>"
			+ "<tr><td align='right' > Attachment : </td><td width='90%' valign='top' ><input type='file' class='fptransText' name='fileFnaAttachment' id='fileFnaAttachment' size='100' style='width:90%' readOnly='true' value=''/><input type='hidden' id='txtFldMailAttach' size='90' value='"
			+ filename
			+ "'/><input type='hidden' name='mailAttachDispName'/></td></tr>"
			+ "<tr>"
			+ "<td colspan='2'><fieldset class='fieldsetBorder' ><legend class='legendTxt'>Email&nbsp;Content:</legend><textarea rows='8' id='EmailContent' name='EmailContent' cols='115' readonly='true'>"
			+ EmailContent
			+ "</textarea></fieldset></td></tr>"
			+ "<tr valign='middle'>"
			+ "<td colspan='2' ><fieldset class='fieldsetBorder' ><legend class='legendTxt'>Email&nbsp;Note:</legend><textarea rows='8' id='EmailNote' name='EmailNote' cols='115' readonly='true'>"
			+ Remarks
			+ "</textarea></fieldset>"
			+ "<input type='hidden' name='txtFldFnaMailCustId' id='txtFldFnaMailCustId'><input type='hidden' name='txtFldFnaMailFnaId' id='txtFldFnaMailFnaId'></td></tr>"
			+ "</table></td></tr></table>" + "</form>";
	// +"</form>";

	return txtHtml;
}// End EmailModalPopup

function showemailpopup(btnobj) {


	var archradbtn = btnobj.parentNode.parentNode.cells[0].childNodes[1];
	var fnaId = btnobj.parentNode.parentNode.cells[0].childNodes[0].value;

	var servAdvEmail = btnobj.parentNode.parentNode.cells[3].childNodes[1];
	var servAdvMgrEmail = btnobj.parentNode.parentNode.cells[3].childNodes[2];
	var servAdvName = btnobj.parentNode.parentNode.cells[3].childNodes[3];

	var advMgrMailCont1 = document.getElementById("hTxtFldAdvMgrEmailCont1").value;
	var advMgrMailCont2 = document.getElementById("hTxtFldAdvMgrEmailCont2").value;

	advMgrMailCont1 = advMgrMailCont1.replace("<br>", "\n");
	advMgrMailCont2 = advMgrMailCont2.replace("<br>", "\n");

	var mailContent = advMgrMailCont1 + " " + servAdvName.value + " \n "
			+ advMgrMailCont2;

	// StAFF ALSO NEED TO OPEN
	// if(LOG_USER_STFTYPE == "STAFF"){
	// //servAdvEmail.;
	// //servAdvMgrEmail.;
	// }
	// else{

	// }

	// var genFileName = fnaGenReport(btnobj, true);

	// alert("genFileName--->"+genFileName);

	archradbtn.checked = true;

	var htmltext = EmailModalPopup("txtHtml", mailContent, servAdvEmail.value,
			servAdvMgrEmail.value, "", "", "", "");// genFileName
	// var htmltext = EmailModalPopup("txtHtml", "",
	// servAdvEmail.value,servAdvMgrEmail.value, "", "", "", "");
	// var htmltext = EmailModalPopup("txtHtml", emailcont,
	// 'asikraja@firstprincipal.com',
	// 'asikraja@firstprincipal.com',"",subjformat,fileName,dispFileName);

	$("#emailPopup").html(htmltext);

	document.getElementById("txtFldFnaMailCustId").value = document
			.getElementById("txtFldCustId").value;
	document.getElementById("txtFldFnaMailFnaId").value = fnaId;

	var dialog = $("#emailPopup").dialog({
		autoOpen : false,
		height : 'auto',
		width : 'auto',
		modal : true,
		resizable : false,
		// screen center
		my : "center",
		at : "center",
		of : window,
		// screen center
		buttons : {
			" Send " : function() {
				sendMail(dialog, btnobj);
			},
			Cancel : function() {
				dialog.dialog("close");
			}
		},
		close : function() {
		}
	});

	dialog.dialog("open");

}

function sendMail(dialog, btnobj) {

	var mailTo = document.getElementById("txtFldEmailTo").value;
	var mailCC = document.getElementById("txtFldEmailToCC").value;
	var mailSubj = document.getElementById("txtFldSubject").value;
	var mailContent = document.getElementById("EmailContent").value;
	var mailAttach = document.getElementById("txtFldMailAttach").value;
	// var dispFileName = document.getElementById("attachmentName").value;
	var dispFileName = "";
	var custId = document.getElementById("txtFldCustId").value;
	var fnaId = btnobj.parentNode.parentNode.cells[0].childNodes[0].value;
	var mgrMailSentFlagObj = btnobj.parentNode.parentNode.cells[2].childNodes[5];

	// alert(document.getElementById("fileFnaAttachment").type)
	var fileFnaAttach = document.getElementById("fileFnaAttachment").value;

	if (!checkMandatoryField(document.getElementById("txtFldEmailTo"),
			FNA_EMAILTO_VALMSG))
		return;

	if (!checkMandatoryField(document.getElementById("txtFldSubject"),
			FNA_EMAILSUB_VALMSG))
		return;

	if (!checkMandatoryField(document.getElementById("EmailContent"),
			FNA_EMAILCONTENT_VALMSG))
		return;

	if (!validateEmailId("txtFldEmailTo"))
		return;

	if (!validateEmailId("txtFldEmailToCC"))
		return;

	if (window.confirm("Do you want to sent mail to manager?")) {
		mgrMailSentFlagObj.value = "Y";
	} else {
		mgrMailSentFlagObj.value = "";
		return false;
	}

	var mgrMailSentFlg = mgrMailSentFlagObj.value;

	var parameter = "dbcall=EMAIL" + "&txtFldEmailToCC=" + mailCC
			+ "&txtFldEmailTo=" + mailTo + "&txtFldSubject=" + mailSubj
			+ "&txtFldMailContent=" + mailContent + "&txtFldMailAttach="
			+ mailAttach + "&txtFldFnaMailCustId=" + custId
			+ "&txtFldFnaMailFnaId=" + fnaId + "&mailAttachDispName="
			+ dispFileName + "&mgrMailSentFlg=" + mgrMailSentFlg
			+ "&fileFnaAttachment=" + fileFnaAttach;

	// document.getElementById('hiddenFormAttch').appendChild(document.getElementById("fileFnaAttachment"));
	// creatSubmtDynaAttchment(baseUrl+"/KYCServletFormSubmit?"+parameter);

	document.forms["hiddenFormAttch"].action = baseUrl
			+ "/KYCServletFormSubmit?" + parameter;
	document.forms["hiddenFormAttch"].method = "POST";

	window.open('', 'TheAttachmentWindow',
			"channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
	document.getElementById("hiddenFormAttch").submit();

	// var jsnResponse = "";
	//
	// $.ajax({
	// type : "POST",
	// url : "KYCServletFormSubmit",
	// data : parameter,
	// dataType : "json",
	// contentType: 'multipart/form-data',
	// async : false,
	// processData: false,
	//
	// beforeSend : function() {
	// $("#loadingLayer").show();
	// $("#loadingMessage").show();
	// },
	//
	// success : function(data) {
	// jsnResponse = data;
	// },
	// complete : function() {
	// // alert("Loaded Success...");
	// $("#loadingMessage").hide();
	// },
	// error : function() {
	// // alert("Loading Error...");
	// $("#loadingMessage").hide();
	// }
	// });
	//
	// alert(jsnResponse)
	//
	// return jsnResponse;

	// alert(parameter);

	// var respText = ajaxCall(parameter);
	//
	// var retval = respText;// eval('(' + respText + ')');
	//
	// for ( var val in retval) {
	//
	// var tabdets = retval[val];
	//
	// if (tabdets["SESSION_EXPIRY"]) {
	// window.location = baseUrl + SESSION_EXP_JSP;
	// return;
	// }
	//
	// for ( var tab in tabdets) {
	//
	// if (tabdets.hasOwnProperty(tab)) {
	// var key = tab;
	// var value = tabdets[tab];
	//
	// if (key == "SEND_MAIL_SUCCESS") {
	// alert(value);
	// dialog.dialog("close");
	// btnobj.parentNode.parentNode.cells[5].childNodes[0].value = "YES";
	// // document.getElementById("selKycSentStatus").value="YES";
	// }
	// if (key == "SEND_MAIL_FAILURE") {
	// // alert(value);
	//
	// }
	// }
	// }
	// }

}

function fnaDepnAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaDepentDets(true))
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text" class="fpNonEditTblTxt" name="txtFldFnaDepntName" maxlength="60"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"class="fpEditTblTxt" />'
			+ '<input type="hidden" name="txtFldFnaDepMode" value="'+ INS_MODE+ '"/>' + '<input type="hidden" name="txtFldFnaDepId"/>';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<select name="selFnaDepntRel" class="kycSelect" ></select>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" class="fpNonEditTblTxt"  name="txtFldFnaDepntAge" maxlength="3" class="fpEditTblTxt"  size="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<select name="selFnaDepntSex" class="kycSelect" ></select>';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type = "text" class="fpNonEditTblTxt"  name="txtFldFnaDepntYr2Sup" maxlength="3" class="fpEditTblTxt"   onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text" name="txtFldFnaDepntMonthContr" maxlength="22"  onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" class="fpEditTblTxt"   onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text" name= "txtFldFnaDepntMonthSelf" maxlength="22"  onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" class="fpEditTblTxt" />';

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type  = "text" name= "txtFldFnaDepntMonthSps" maxlength="22"  onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" class="fpEditTblTxt" />';

	comboMaker(cell1, relArray, "", false);
	comboMakerByString(STR_SEX, cell3.childNodes[0], "", true);

	cell0.childNodes[0].focus();

	rowobj.onclick = function() {
		selectSingleRow(this);
	};
	// tblIncTxtFldClass(tblid);
}

function fnaDepnModRow() {
}

function fnaDepnDelRow(tblid) {

	deleteRowInTbl(tblid, false);
}

function fnaDepnExpndRow(tblid) {
}

function validateFnaDepentDets(flag) {

	var tblobj = document.getElementById("fnaDepnTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if(rowlenobj > 0 && flag){
	for (var rl = 0; rl < rowlenobj; rl++) {
		if(isEmpty(tbodyobj.rows[rl].cells[1].childNodes[0].value)){
			changedarrGlobal5.push(FNA_DEPNTNAME_VALMSG)
		}
//		if (!checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[0],FNA_DEPNTNAME_VALMSG)){
//			setPageFocus(27, FNA_FORMTYPE, custLobArr);
//			return false;
//		}
	}
	}

	document.getElementById("hTxtFldFnaDepntCount").value = rowlenobj;

	return true;
}

function fnaInvGoalAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaInvGoal())
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaInvGoalPur"  class="fpEditTblTxt"  maxlength="30" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type = "hidden"  name="txtFldFnaInvGoalMode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaInvGoalId"  />';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaInvGoalAmt"   class="fpEditTblTxt"  maxlength="22"  onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"/>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" name = "txtFldFnaInvGoalYrs" size="3"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	cell0.childNodes[0].focus();

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

	// tblIncTxtFldClass(tblid);
}

function fnaInvGoalModRow() {

}

function fnaInvGoalDelRow(tblid) {

	deleteRowInTbl(tblid, false);
}

function fnaInvGoalExpndRow() {

}

function validateFnaInvGoal() {

	var tblobj = document.getElementById("fnaInvGoalTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],		FNA_INVGOALPUR_VALMSG)) {
				return false;
			}

		}
	}

	document.getElementById("hTxtFldFnaInvGoalCnt").value = rowlenobj;
	return true;

}

function validateFnaFlowDets() {

	var tblobj = document.getElementById("fnaFlowDetTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],FNA_FLOWTYPE_VALMSG))
				return;
		}
	}

	document.getElementById("hTxtFldFnaFlowDetCnt").value = rowlenobj;
	return true;

}

function validateFnaDepntCont() {

	var tblobj = document.getElementById("fnadepntfinanceTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],	FNA_DEPNTCONTNAME_VALMSG))
				return;
		}
	}

	document.getElementById("hTxtFldFnaDepntContCnt").value = rowlenobj;
	return true;

}

function validateFnaHlthInsDet() {

	var tblobj = document.getElementById("fnahlthinsTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],	FNA_HLTHINSPOLTYPE_VALMSG))
				return;
		}

	}
	document.getElementById("hTxtFldFnaHlthInsCnt").value = rowlenobj;
	return true;

}

function validateFnaPropOwnDet() {

	var tblobj = document.getElementById("fnaPropOwnTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],	FNA_PROPOWNOWNSHIP_VALMSG))
				return;
		}
	}

	document.getElementById("hTxtFldFnaPropOwnCnt").value = rowlenobj;
	return true;
}

function validateFnaVehiOwnDet() {

	var tblobj = document.getElementById("fnaVehiOwnTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],FNA_VEHIOWNOWNER_VALMSG))
				return;
		}
	}

	document.getElementById("hTxtFldFnaVehiOwnCnt").value = rowlenobj;

	return true;

}

function validateFnaAdvRecPrdtDet() {

	var tblobj = document.getElementById("fnaadvrecTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[0],	FNA_ADVRECPRDTTYPE_VALMSG))
				return;
		}
	}

	document.getElementById("hTxtFldFnaAdvRecPrdtCnt").value = rowlenobj;

	return true;
}

function validateFnaArtuPlantDet(flag) {

	var tblobj = document.getElementById("fnaartuplanTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0 && flag) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			var chkPro=false;

			if(isEmpty(tbodyobj.rows[rl].cells[1].childNodes[0].value) &&
					isEmpty(tbodyobj.rows[rl].cells[1].childNodes[5].value))chkPro=true;

//	Poovathi add on 10-11-2019.		
			var prodType = tbodyobj.rows[rl].cells[0].childNodes[0].value;
			
			
			if(prodType == "ILP"){
//				if (!checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[1],FNA_ARTUPLANPRDTTYPE_VALMSG)) {
				if (!checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[0],FNA_ARTUPLANPRDTTYPE_VALMSG)) {
					setPageFocus(14, FNA_FORMTYPE, custLobArr);
					return;
					}
				}
			
			if((prodType == "Insurance")||(prodType == "H&S")||(prodType == "PA")){
					if (!checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[0],FNA_ARTUPLANPRDTTYPE_VALMSG)) {
						setPageFocus(14, FNA_FORMTYPE, custLobArr);
						return;
					}
			   }
			if(isEmpty(tbodyobj.rows[rl].cells[0].childNodes[0].value)){
				changedarrGlobal7.push(FNA_ARTUPLANPRDTTYPE_VALMSG);
			}

			if (!checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[5],FNA_ARTUPLAN_COMP_VALMSG)) {
					setPageFocus(14, FNA_FORMTYPE, custLobArr);
					return;
			}
			
			if(prodType == "ILP"){
				if (!checkMandatoryField(tbodyobj.rows[rl].cells[7].childNodes[0],FNA_ARTUPLAN_RISKRATE_VALMSG)) {
					setPageFocus(14, FNA_FORMTYPE, custLobArr);
					return;
				}	
				
				if(isEmpty(tbodyobj.rows[rl].cells[0].childNodes[0].value)){
					changedarrGlobal7.push(FNA_ARTUPLANPRDTTYPE_VALMSG);
				}
			}
//			rowobj.cells[7].childNodes[0].disabled = false;

//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[5],FNA_ARTUPLAN_PT_VALMSG)) {
//				setPageFocus(14, FNA_FORMTYPE, custLobArr);
//				return;
//			}

			if(isEmpty(tbodyobj.rows[rl].cells[1].childNodes[5].value)){
				changedarrGlobal7.push(FNA_ARTUPLAN_PT_VALMSG);
			}

		}
	}
	document.getElementById("hTxtFldFnaArtuPrdtCnt").value = rowlenobj;

	var tblSubObj = document.getElementById("fnaartuplanTblss");
	var tbodySubobj = tblSubObj.tBodies[0];
	var rowlenSubobj = tbodySubobj.rows.length;

	if (rowlenSubobj > 0) {
		for (var rl = 0; rl < rowlenSubobj; rl++) {

			if(flag){
				
				if (!checkMandatoryField(tbodySubobj.rows[rl].cells[1].firstChild,FNA_ARTUPLANCLNT_VALMSG,14,FNA_FORMTYPE)){
					return;
				}
				
				if(isEmpty(tbodySubobj.rows[rl].cells[1].firstChild.value) ){
					changedarrGlobal7.push(FNA_ARTUPLANCLNT_VALMSG);
				}
				
			}else{
				
				
				
			}
			

		}

	}

	return true;

}

function validateFnaArtuFundtDet(flag) {

	var tblobj = document.getElementById("fnaartufundTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0 && flag) {
		for (var rl = 0; rl < rowlenobj; rl++) {

//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[0],FNA_ARTUFUNDPRDTTYPE_VALMSG)) {
//				setPageFocus(25, FNA_FORMTYPE, custLobArr);
//				return;
//			}
			if(isEmpty(tbodyobj.rows[rl].cells[1].childNodes[0].value)){
				changedarrGlobal8.push(FNA_ARTUFUNDPRDTTYPE_VALMSG);
			}

/*			if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[1],FNA_ARTUFUND_COMP_VALMSG)) {
				setPageFocus(25, FNA_FORMTYPE, custLobArr);
				return;
			}*/


//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[5],FNA_ARTUFUND_PT_VALMSG)) {
//				setPageFocus(25, FNA_FORMTYPE, custLobArr);
//				return;
//			}

			if(isEmpty(tbodyobj.rows[rl].cells[2].childNodes[5].value)){
				changedarrGlobal8.push(FNA_ARTUFUND_PT_VALMSG);
			}
		}
	}

	document.getElementById("hTxtFldFnaArtuFundCnt").value = rowlenobj;
	return true;

}

function validateFnaSwrepPlantDet(flag) {

	var tblobj = document.getElementById("fnaswrepplanTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0 && flag) {
		for (var rl = 0; rl < rowlenobj; rl++) {

			var chkPro=false;

			if(isEmpty(tbodyobj.rows[rl].cells[1].childNodes[1].value) &&
					isEmpty(tbodyobj.rows[rl].cells[1].childNodes[5].value))chkPro=true;
			
			
//			Poovathi add on 10-11-2019.	   FNA_SWREPPLANPRDTTYPE_VALMSG	
			var prodType = tbodyobj.rows[rl].cells[0].childNodes[0].value;
			
			
			if(prodType == "ILP"){
				if (chkPro && !checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[1],FNA_SWREPPLANPRDTTYPE_VALMSG)) {
					setPageFocus(16, FNA_FORMTYPE, custLobArr);
					return;
				}
				}
			
			if((prodType == "Insurance")||(prodType == "H&S")||(prodType == "PA")){
				if (chkPro && !checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[0],FNA_SWREPPLANPRDTTYPE_VALMSG)) {
					setPageFocus(16, FNA_FORMTYPE, custLobArr);
					return;
				}
					
			   }
			

			/*if (chkPro && !checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[1],FNA_SWREPPLANPRDTTYPE_VALMSG)) {
				setPageFocus(16, FNA_FORMTYPE, custLobArr);
				return;
			}*/

			if(isEmpty(tbodyobj.rows[rl].cells[0].childNodes[0].value)){
				changedarrGlobal10.push(FNA_SWREPPLANPRDTTYPE_VALMSG);
			}

			if (chkPro && !checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[5],FNA_SWREPPLAN_COMP_VALMSG)) {
				setPageFocus(16, FNA_FORMTYPE, custLobArr);
				return;
			}


//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[5],	FNA_SWREPPLAN_PT_VALMSG)) {
//				setPageFocus(16, FNA_FORMTYPE, custLobArr);
//				return;
//			}
			if(isEmpty(tbodyobj.rows[rl].cells[1].childNodes[5].value)){
				changedarrGlobal10.push(FNA_SWREPPLAN_PT_VALMSG);
			}

		}
	}

	document.getElementById("hTxtFldFnaSwrepPlanCnt").value = rowlenobj;

	var tblSubObj = document.getElementById("fnaswrepplanTblss");
	var tbodySubobj = tblSubObj.tBodies[0];
	var rowlenSubobj = tbodySubobj.rows.length;

	if (rowlenSubobj > 0) {
		for (var rl = 0; rl < rowlenSubobj; rl++) {

			if (!checkMandatoryField(tbodySubobj.rows[rl].cells[1].firstChild,FNA_SWPLANCLNT_VALMSG,16,FNA_FORMTYPE))
				return;

		}

	}

	return true;

}

function validateFnaSwrepFundtDet(flag) {

	var tblobj = document.getElementById("fnaSwrepFundTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0 && flag) {
		for (var rl = 0; rl < rowlenobj; rl++) {

//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[1].childNodes[0],FNA_SWREPFUNDPRDTTYPE_VALMSG)) {
//				setPageFocus(16, FNA_FORMTYPE, custLobArr);
//				return;
//			}

			if(isEmpty(tbodyobj.rows[rl].cells[1].childNodes[0].value)){
				changedarrGlobal10.push(FNA_SWREPFUNDPRDTTYPE_VALMSG);
			}

			/*if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[1],FNA_SWREPFUND_COMP_VALMSG)) {
				setPageFocus(16, FNA_FORMTYPE, custLobArr);
				return;
			}*/





//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[5],FNA_SWREPFUND_PT_VALMSG)) {
//				setPageFocus(16, FNA_FORMTYPE, custLobArr);
//				return;
//			}

			if(isEmpty(tbodyobj.rows[rl].cells[2].childNodes[5].value)){
				changedarrGlobal10.push(FNA_SWREPFUND_PT_VALMSG);
			}
		}
	}

	document.getElementById("hTxtFldFnaSwrepFundCnt").value = rowlenobj;

	return true;
}
function validateFnaFatcaTaxDets(flag) {

	var clientFatcaFlag = $("#dfSelfUsnotifyflg").prop("checked");

//	alert("clientFatcaFlag ->"+clientFatcaFlag)
//	if(clientFatcaFlag){

		var tblobj = document.getElementById("fnaFatcaTbl");
		var tbodyobj = tblobj.tBodies[0];
		var rowlenobj = tbodyobj.rows.length;


		var tblobjs = document.getElementById("fnaFatcaTblSps");
		var tbodyobjs = tblobjs.tBodies[0];
		var rowlenobjs = tbodyobjs.rows.length;


		if (rowlenobj > 0 && flag) {
			for (var rl = 0; rl < rowlenobj; rl++) {


				if(isEmpty(tbodyobj.rows[rl].cells[0].childNodes[0].value)){
					changedarrGlobal4.push(FNA_FATCACONTRY_VALMSG + " for Client(1)/Client(2)");
				}
//				if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],FNA_FATCACONTRY_VALMSG,20,FNA_FORMTYPE)){
//					return;
//				}



				if (isEmpty(tbodyobj.rows[rl].cells[1].childNodes[0].value)) {
					if(isEmpty(tbodyobj.rows[rl].cells[2].childNodes[0].value)){
						changedarrGlobal4.push(FNA_NOTAXREASON + " for Client(1)/Client(2)" );
					}
//					if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[0],FNA_NOTAXREASON,20,FNA_FORMTYPE))
//						return;
				}

				if (!isEmpty(tbodyobj.rows[rl].cells[2].childNodes[0].value)
						&& tbodyobj.rows[rl].cells[2].childNodes[0].value === "Reason B") {
					if(isEmpty(tbodyobj.rows[rl].cells[3].childNodes[0].value)){
						changedarrGlobal4.push(FNA_REASONDETAILS + " for Client(1)/Client(2)" );
					}
//					if (!checkMandatoryField(tbodyobj.rows[rl].cells[3].childNodes[0],FNA_REASONDETAILS,20,FNA_FORMTYPE))
//						return;
				}
			}
		}

		if (rowlenobjs > 0 && flag) {
			for (var rls = 0; rls < rowlenobjs; rls++) {

//				if (!checkMandatoryField(tbodyobjs.rows[rls].cells[0].childNodes[0],FNA_FATCACONTRY_VALMSG,20,FNA_FORMTYPE))
//					return;

				if(isEmpty(tbodyobjs.rows[rls].cells[0].childNodes[0].value)){
					changedarrGlobal4.push(FNA_FATCACONTRY_VALMSG + ' for Client(1)/Client(2)');
				}


				if (isEmpty(tbodyobjs.rows[rls].cells[1].childNodes[0].value)) {
//					if (!checkMandatoryField(tbodyobjs.rows[rls].cells[2].childNodes[0],FNA_NOTAXREASON,20,FNA_FORMTYPE))
//						return;

					if(isEmpty(tbodyobjs.rows[rls].cells[2].childNodes[0].value)){
						changedarrGlobal4.push(FNA_NOTAXREASON + ' for Client(1)/Client(2)');
					}
				}

				if (!isEmpty(tbodyobjs.rows[rls].cells[2].childNodes[0].value)
						&& tbodyobjs.rows[rls].cells[2].childNodes[0].value === "Reason B") {
//					if (!checkMandatoryField(tbodyobjs.rows[rls].cells[3].childNodes[0],FNA_REASONDETAILS,20,FNA_FORMTYPE))
//						return;
					if(isEmpty(tbodyobjs.rows[rls].cells[3].childNodes[0].value)){
						changedarrGlobal4.push(FNA_REASONDETAILS + ' for Client(1)/Client(2)');
					}
				}
			}
		}

		document.getElementById("hTxtFldFnaFatcaTaxCnt").value = Number(rowlenobj)+Number(rowlenobjs);

//	}


	return true;
}


function validateFnaFatcaSpsTaxDets() {

	var clientFatcaFlag = $("#dfSpsUsnotifyflg").prop("checked");

	if(clientFatcaFlag){

		var tblobj = document.getElementById("fnaFatcaTblSps");
		var tbodyobj = tblobj.tBodies[0];
		var rowlenobj = tbodyobj.rows.length;

		if (rowlenobj > 0) {
			for (var rl = 0; rl < rowlenobj; rl++) {
				if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],FNA_FATCACONTRY_VALMSG,20,FNA_FORMTYPE))
					return;


				if (isEmpty(tbodyobj.rows[rl].cells[1].childNodes[0].value)) {
					if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[0],FNA_NOTAXREASON,20,FNA_FORMTYPE))
						return;
				}

				if (!isEmpty(tbodyobj.rows[rl].cells[2].childNodes[0].value)
						&& tbodyobj.rows[rl].cells[2].childNodes[0].value === "Reason B") {
					if (!checkMandatoryField(tbodyobj.rows[rl].cells[3].childNodes[0],FNA_REASONDETAILS,20,FNA_FORMTYPE))
						return;
				}
			}
		}

		document.getElementById("hTxtFldFnaFatcaTaxCntSps").value = rowlenobj;

	}


	return true;
}

function validateFnaLifeInsPlanDets() {

	var tblobj = document.getElementById("fnaLIPlanTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],	FNA_LIFEINSOWNER_VALMSG))
				return;
		}
	}

	document.getElementById("hTxtFldFnaLifeInsCnt").value = rowlenobj;

	return true;

}

function validateFnaInvPlanDets() {

	var tblobj = document.getElementById("fnaInvsetPlanTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],FNA_INVPLANINVTYPE_VALMSG))
				return;
		}
	}

	document.getElementById("hTxtFldFnaInvInsCnt").value = rowlenobj;

	return true;
}

function fnaAssgnFlg(obj, id) {

	if (id == "chk") {
		if (obj.checked) {
			// alert(obj.parentNode.childNodes[1].name)
			// obj.parentNode.childNodes[1].value="Y";
			obj.value = "Y";
		} else if (!obj.checked) {
			// obj.parentNode.childNodes[1].;
			obj.value = "";
		}
		// alert(obj.parentNode.childNodes[1].value)
	} else {
		if (obj.checked) {
			document.getElementById(id).value = obj.value;
		}
	}
}

function validateFnaRepCC() {

}

function showOutlook() {

	// This is for prototype alone

	// alert("adv Email : "+$("#txtFldAdvEmailId").val());
	// alert("mgr Email : "+$("#txtFldAdvMgrEmailId").val());

	var email = $("#txtFldAdvEmailId").val();
	var cc = $("#txtFldAdvMgrEmailId").val();
	var subject = 'KYC Form';
	var emailBody = 'KYC Form Archive Detail';
	window.location = 'mailto:' + email + '?subject=' + subject + '&body='
			+ emailBody + '&cc=' + cc;

}

function mgrApprveSts(btnobj) {

	var archradbtn = btnobj.parentNode.parentNode.cells[0].childNodes[1];
	archradbtn.checked = true;

	var fnaId = btnobj.parentNode.parentNode.cells[0].childNodes[0].value;

	var archStsFld = btnobj.parentNode.parentNode.cells[4].childNodes[0];
	var archSts = archStsFld.value;
	var clntCons = btnobj.parentNode.parentNode.cells[9].childNodes[1].value;
	var custAttchFlg = btnobj.parentNode.parentNode.cells[9].childNodes[2].value;

	var mgrApprveSts = btnobj.parentNode.parentNode.cells[6].childNodes[0];
	var mgrRemarks = btnobj.parentNode.parentNode.cells[7].childNodes[0];

	var parameter = "";

	var custId = $("#txtFldCustId").val();
	zipcustId = $("#txtFldCustId").val();
	var strAdvName = $("#txtFldAdvNameAttach").val();

	// FNA_FORM~CUST_NAME~ARCHIVENO
	var emailD = String(emailDets).split(",");
	var emailsubj = String(emailD[0]);
	var dispFileName = emailsubj.replace(/~/g, "_");
	dispFileName = dispFileName.replace(/CUST_NAME/g, FNA_CUSTNAME);
	dispFileName = dispFileName.replace(/ARCHIVENO/g,
			btnobj.parentNode.parentNode.cells[1].childNodes[1].value)
			.toUpperCase();
	dispFileName += ".PDF";

	if (LOG_USER_MGRACS == "Y" && FNA_CUSTSERVADVID == LOG_USER_ADVID
			&& LOG_USER_DESIG == DESIG_GMR) {

		if (!checkMandatoryField(archStsFld, FNA_ARCHSTS_VALMSG))
			return;
		if (!checkMandatoryField(mgrApprveSts, FNA_MGRAPPRSTS_VALMSG))
			return;

	} else if (LOG_USER_MGRACS == "Y" && FNA_CUSTSERVADVID != LOG_USER_ADVID) {

		if (!checkMandatoryField(archStsFld, FNA_ARCHSTS_VALMSG))
			return;
		if (!checkMandatoryField(mgrApprveSts, FNA_MGRAPPRSTS_VALMSG))
			return;

	} else {
		if (!checkMandatoryField(archStsFld, FNA_ARCHSTS_VALMSG))
			return;
	}

	parameter = "dbcall=MGRAPPR&txtFldFnaId=" + fnaId + "&txtFldMgrApprSts="
			+ mgrApprveSts.value + "&txtFldRemarks=" + mgrRemarks.value
			+ "&txtFldArchSts=" + archSts + "&txtFldClntCons=" + clntCons
			+ "&txtFldCustId=" + custId + "&genFileName=" + dispFileName
			+ "&strAdvName=" + strAdvName + "&txtFldCustAttachFlg="
			+ custAttchFlg + "&paramBirtUrl=" + BIRT_URL;
	// alert(parameter)

	var respText = ajaxCall(parameter);

	var retval = respText;// eval('(' + respText + ')');

	for ( var val in retval) {

		var tabdets = retval[val];

		if (tabdets["SESSION_EXPIRY"]) {
			window.location = baseUrl + SESSION_EXP_JSP;
			return;
		}

		for ( var tab in tabdets) {

			if (tabdets.hasOwnProperty(tab)) {
				var key = tab;
				var value = tabdets[tab];

				if (key == "CUST_ATTACH_ID") {
					if (value.length > 0)
						custattachId = value[0].txtFlsCustAttachId;
				}

				if (key == "MGR_APPR_STS") {

					if (value == "SUCCESS") {

						if (LOG_USER_MGRACS == "Y"
								&& FNA_CUSTSERVADVID == LOG_USER_ADVID
								&& LOG_USER_DESIG == DESIG_GMR) {

						} else if (LOG_USER_MGRACS == "Y"
								&& FNA_CUSTSERVADVID != LOG_USER_ADVID) {

							alert(FNA_MGRAPPRSTSUPD_MSG);

						} else {
							// alert(FNA_STFUPD_MSG);
							btnobj.parentNode.parentNode.cells[4].childNodes[0].disabled = true;
						}

						btnobj.parentNode.parentNode.cells[8].childNodes[0].value = currDate;
						btnobj.parentNode.parentNode.cells[8].childNodes[0].readOnly = true;

						// var custId = $("#txtFldCustId").val();
						// var user = $("#txtFldCrtdUser").val();

						// var url =
						// "KycPre.action?txtFldCustId="+custId+"&txtFldCrtdUser="+user+"&txtFldKycForm=ALL";

						// document.forms[0].action=url;
						// document.forms[0].submit();

					}
				}
			}
		}
	}
}

function fnaFlowDetAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaFlowDets())
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<select name="selFlowType" class="kycSelect"></select><input type = "hidden"  name="txtFldFnaFlowMode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaFlowId"  />';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaFundSrc"  class="fpEditTblTxt"  maxlength="30" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" name = "txtFldFnaFundWhen" size="10"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaFundAmt"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type = "text"  name="txtFldFnaFundRemark"  class="fpEditTblTxt"   maxlength="300" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	cell0.childNodes[0].focus();

	comboMaker(cell0, flowTypeArr, "", true);

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

}

function fnaFlowDetModRow() {
}
function fnaFlowDetDelRow(tblid) {
	deleteRowInTbl(tblid, false);
}
function fnaFlowDetExpndRow() {
}

function fnaContDepAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaDepntCont())
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaContDepName" class="fpEditTblTxt"  maxlength="60" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"	/><input type = "hidden"  name="txtFldFnaContDepMode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaContDepId"  />';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaContDepAge"  onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" class="fpEditTblTxt"  maxlength="3"	/>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" name = "txtFldFnaContDepYrs" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"	/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaContLiving" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<select name="selFnaContDepRel" class="kycSelect" ></select>';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaContDepTerYr" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaContDepTerYrEdn" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaContDepAnnlCost" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';

	var cell8 = rowobj.insertCell(8);
	cell8.style.textAlign = "left";
	cell8.innerHTML = '<input type="text" name = "txtFldFnaContDepFund" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';

	var cell9 = rowobj.insertCell(9);
	cell9.style.textAlign = "left";
	cell9.innerHTML = '<input type = "text"  name="txtFldFnaContDepRb" class="fpEditTblTxt"  maxlength="20" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	var cell10 = rowobj.insertCell(10);
	cell10.style.textAlign = "left";
	cell10.innerHTML = '<input type = "text"  name="txtFldFnaContDepWard" class="fpEditTblTxt"  maxlength="20" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	var cell11 = rowobj.insertCell(11);
	cell11.style.textAlign = "left";
	cell11.innerHTML = '<select name="selFnaContDepHosp" class="kycSelect"></select>';

	cell0.childNodes[0].focus();

	comboMaker(cell4, relArray, "", false);
	comboMaker(cell11, hospTypeArr, "", false);

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

}

function fnaContDepModRow() {

}

function fnaContDepDelRow(tblid) {
	deleteRowInTbl(tblid, false);
}

function fnaContDepExpndRow() {
}

function fnaHlthInsAddRow(tblid, extFlg) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaHlthInsDet())
		return;

	/*
	 * 'rowlenobj' varible is added in the following if condition by johnson on
	 * 02/11/2015 in order to allow to check the existing policy does the client
	 * have or not for only one time
	 */

	if (extFlg && rowlenobj == 0) {
		if (!loadHSPExistingPlan())
			return;
	}

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaHiPolType" class="fpEditTblTxt"  maxlength="20" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type = "hidden"  name="txtFldFnaHiMode" value="'
			+ INS_MODE + '" /><input type = "hidden"  name="txtFldFnahiId"/>';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaHiInsured" class="fpEditTblTxt"  maxlength="60" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" /><input type="hidden" name="txtFldHiAppId"/>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" name = "txtFldFnaHiBenfType" class="fpEditTblTxt"  maxlength="150" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaHiAmt" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);") />';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	// cell4.innerHTML ='<input type = "text" name="txtFldFnaHiExpiry"
	// class="fpEditTblTxt"  maxlength="10" />';
	cell4.innerHTML = '<input type = "text"  name="txtFldFnaHiExpiry" id="txtFldFnaHiExpiry" maxlength="10" onblur="CheckDate(this,NULL);"  class="fpEditTblTxt" style="width:75%" />'
			+ '<input type="hidden" id="hFnaLiCalMode" name="hFnaHiExp" value="'
			+ INS_MODE
			+ '" />'
			+ '<img src="images/cal.png" title="Date" alt="Date" height="16px" width="16px" onclick="getCalendarInTbl(this,event)" style="cursor:pointer">';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaHiAnnlPrem" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);") />';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaHiRemark" class="fpEditTblTxt"  maxlength="300" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	cell0.childNodes[0].focus();

	rowobj.onclick = function() {
		selectSingleRow(this);
	};
}

function fnaHlthInsModRow() {
}
function fnaHlthInsDelRow(tblId) {
	deleteRowInTbl(tblId, false);
}
function fnaHlthInsExpndRow() {
}

function fnaPropAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaPropOwnDet())
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaPropOwner" class="fpEditTblTxt"  maxlength="60"onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type = "hidden"  name="txtFldFnaPropOwnMode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaPropOwnId"/>';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaPropMktVal" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" name = "txtFldFnaPropOutsd" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaPropObj" class="fpEditTblTxt" value="Res"  maxlength="60" />';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type = "text"  name="txtFldFnaPropApprc" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaPropOsLoan" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaPropLoanCash" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaPropLoanCpf" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell8 = rowobj.insertCell(8);
	cell8.style.textAlign = "left";
	cell8.innerHTML = '<input type="text" name = "txtFldFnaPropYrs2pay" onchange=getTotalValue(this,"fnaPropOwnTbl") class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell9 = rowobj.insertCell(9);
	cell9.style.textAlign = "left";
	cell9.innerHTML = '<input type = "text"  name="txtFldFnaPropSoldRet" onchange=getTotalValue(this,"fnaPropOwnTbl") size="10" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

}

function fnaPropModRow() {
}

function fnaPropDelRow(tblid) {
	deleteRowInTbl(tblid, false);

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlen = tbodyobj.rows.length;

	if (rowlen > 0) {
		getTotalValue(tbodyobj.rows[0].cells[1].childNodes[0], tblid);
		getTotalValue(tbodyobj.rows[0].cells[2].childNodes[0], tblid);
		getTotalValue(tbodyobj.rows[0].cells[6].childNodes[0], tblid);
		getTotalValue(tbodyobj.rows[0].cells[7].childNodes[0], tblid);
		getTotalValue(tbodyobj.rows[0].cells[8].childNodes[0], tblid);
		getTotalValue(tbodyobj.rows[0].cells[9].childNodes[0], tblid);
	} else {
		var propFooterObj = tblobj.tFoot;
		propFooterObj.rows[0].cells[1].childNodes[0].value = 0;
		propFooterObj.rows[0].cells[2].childNodes[0].value = 0;
		propFooterObj.rows[0].cells[6].childNodes[0].value = 0;
		propFooterObj.rows[0].cells[7].childNodes[0].value = 0;
		propFooterObj.rows[0].cells[8].childNodes[0].value = 0;
		propFooterObj.rows[0].cells[9].childNodes[0].value = 0;
	}

}
function fnaPropExpndRow() {
}

function fnaVehiAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaVehiOwnDet())
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaVehiOwner" class="fpEditTblTxt"  maxlength="60"onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type = "hidden"  name="txtFldFnaVehiMode" value="'
			+ INS_MODE + '"/><input type="hidden" name="txtFldFnaVehiId"/>';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaVehiMktval" onchange=getTotalValue(this,"fnaVehiOwnTbl")  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"/>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" name = "txtFldFnaVehiLoanVal" onchange=getTotalValue(this,"fnaVehiOwnTbl")  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaVehiPerd" onchange=getTotalValue(this,"fnaVehiOwnTbl")  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type = "text"  name="txtFldFnaVehiRemarks" class="fpEditTblTxt"  maxlength="300"onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

}

function fnaVehiModRow() {
}

function fnaVehiDelRow(tblid) {

	deleteRowInTbl(tblid, false);

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlen = tbodyobj.rows.length;

	if (rowlen > 0) {
		getTotalValue(tbodyobj.rows[0].cells[1].childNodes[0], tblid);
		getTotalValue(tbodyobj.rows[0].cells[2].childNodes[0], tblid);
		getTotalValue(tbodyobj.rows[0].cells[3].childNodes[0], tblid);

	} else {
		var propFooterObj = tblobj.tFoot;
		propFooterObj.rows[0].cells[1].childNodes[0].value = 0;
		propFooterObj.rows[0].cells[2].childNodes[0].value = 0;
		propFooterObj.rows[0].cells[3].childNodes[0].value = 0;

	}
}
function fnaVehiExpndRow() {
}

function fnaLiPlanAddRow(tblid, exsflg) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaLifeInsPlanDets())
		return;

	/*
	 * 'rowlenobj' varible is added in the following if condition by johnson on
	 * 02/11/2015 in order to allow to check the existing policy does the client
	 * have or not for only one time
	 */
	if (exsflg && rowlenobj == 0) {
		if (!loadExistingPlan())
			return;
	}

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaLiOwner"  class="fpEditTblTxt"  maxlength="60"onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type = "hidden"  name="txtFldFnaLiMode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaLiId"  />';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaLiAssured"  class="fpEditTblTxt"  maxlength="75" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<select name = "txtFldFnaLiComp" class="kycSelect" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"/>';
	// cell2.childNodes[0] has been changed text field to select combo box by
	// johnson on 28042015

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaLiPolNo"   class="fpEditTblTxt"  maxlength="20" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type="hidden" name="txtFldFnaLiAppId"/>';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type="text" name="selFnaLiPolType"   class="fpEditTblTxt"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"maxlength="12" />';
	// '<select name="selFnaLiPolType" class="kycSelect" ></select>';
	// //commented by johnson on 07052015

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaLiDthBenf"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaLiMiCover"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaLiCashVal"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell8 = rowobj.insertCell(8);
	cell8.style.textAlign = "left";
	cell8.innerHTML = '<input type="text" name = "txtFldFnaLiMatrVal" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell9 = rowobj.insertCell(9);
	cell9.style.textAlign = "left";
	cell9.innerHTML = '<input type = "text"  name="txtFldFnaLiOsLoan"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell10 = rowobj.insertCell(10);
	cell10.style.textAlign = "left";
	cell10.innerHTML = '<input type = "text"  size="10" id="txtFldFnaLiMatrDate"  name="txtFldFnaLiMatrDate" maxlength="10" onblur="CheckDate(this,NULL);"  class="fpEditTblTxt" style="width:75%" />'
			+ '<input type="hidden" id="hCalMode" name="hCalMode" value="'
			+ INS_MODE
			+ '"/>'
			+ '<img src="images/cal.png" title="Date" alt="Date" height="16px" width="16px" onclick="getCalendarInTbl(this,event)" style="cursor:pointer">';
	// datePickerCall(cell10.childNodes[0].name);

	var cell11 = rowobj.insertCell(11);
	cell11.style.textAlign = "left";
	cell11.innerHTML = '<input type = "text"  name="txtFldFnaLiValEdnCash"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	// ---

	var cell12 = rowobj.insertCell(12);
	cell12.style.textAlign = "left";
	cell12.innerHTML = '<input type = "text"  name="txtFldFnaLiValRpCash"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell13 = rowobj.insertCell(13);
	cell13.style.textAlign = "left";
	cell13.innerHTML = '<input type = "text"  name="txtFldFnaLiValCpf"  class="fpEditTblTxt"  onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell14 = rowobj.insertCell(14);
	cell14.style.textAlign = "left";
	cell14.innerHTML = '<input type = "text"  name="txtFldFnaLiAnnlPrem"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell15 = rowobj.insertCell(15);
	cell15.style.textAlign = "left";
	cell15.innerHTML = '<input type = "text"  name="txtFldFnaLiTpdPay1"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"/>';

	var cell16 = rowobj.insertCell(16);
	cell16.style.textAlign = "left";
	cell16.innerHTML = '<input type = "text"  name="txtFldFnaLiYrsto1"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell17 = rowobj.insertCell(17);
	cell17.style.textAlign = "left";
	cell17.innerHTML = '<input type = "text"  name="txtFldFnaLiYr1"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';

	var cell18 = rowobj.insertCell(18);
	cell18.style.textAlign = "left";
	cell18.innerHTML = '<input type = "text"  name="txtFldFnaLiTpdPay2"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell19 = rowobj.insertCell(19);
	cell19.style.textAlign = "left";
	cell19.innerHTML = '<input type = "text"  name="txtFldFnaLiYrto2"   class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell20 = rowobj.insertCell(20);
	cell20.style.textAlign = "left";
	cell20.innerHTML = '<input type = "text"  name="txtFldFnaLiYr2"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell21 = rowobj.insertCell(21);
	cell21.style.textAlign = "left";
	cell21.innerHTML = '<input type = "text"  name="txtFldFnaLiTpdPay3"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell22 = rowobj.insertCell(22);
	cell22.style.textAlign = "left";
	cell22.innerHTML = '<input type = "text"  name="txtFldFnaLiYrto3"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell23 = rowobj.insertCell(23);
	cell23.style.textAlign = "left";
	cell23.innerHTML = '<input type = "text"  name="txtFldFnaLiYr3"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell24 = rowobj.insertCell(24);
	cell24.style.textAlign = "left";
	cell24.innerHTML = '<input type = "text"  name="txtFldFnaLiCompTpd"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell25 = rowobj.insertCell(25);
	cell25.style.textAlign = "left";
	cell25.innerHTML = '<input type = "text"  name="txtFldFnaLiRem"  class="fpEditTblTxt"  maxlength="300" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	comboMakerById(cell2.childNodes[0], prinIdArr, '', true);// added by
	// johnson on
	// 28042015
	// comboMaker(cell4,poltypeArr,"",true); // commented by johnson on 07052015

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

}

function fnaLiPlanModRow() {
}

function fnaLiPlanDelRow(tblid) {
	deleteRowInTbl(tblid, false);
}
function fnaLiPlanExpndRow() {
}

function fnaInvstPlanAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaInvPlanDets())
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaInvType"  class="fpEditTblTxt"  maxlength="20" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/><input type = "hidden"  name="txtFldFnaInvmode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaInvId"  />';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<select name="txtFldFnaInvInstName" class="kycSelect" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" ></select>';

	// cell1.innerHTML childNodes[0] hasbeen changed from textfield to select
	// element.
	// '<input type = "text" name="txtFldFnaInvInstName"
	// class="fpEditTblTxt"  maxlength="60"
	// onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);"
	// onmouseout="hideTooltip();"/>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" name = "txtFldFnaInvDesc" class="fpEditTblTxt"  maxlength="150"onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaInvAmt"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<select name="selFnaInvPayMeth" class="kycSelect" ></select>';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaInvCurrBid"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaInvDate"  maxlength="10" onblur="CheckDate(this,NULL);" class="fpEditTblTxt" style="width:75%;" />'
			+ '<input type="hidden" name="hMode" id="hMode" value="'
			+ INS_MODE
			+ '"/>'
			+ '<img title="Date" alt="Date" src="images/cal.png" height="16px" width="16px" onclick="getCalendarInTbl(this,event)" style="cursor:pointer">';
	// datePickerCall(cell6.childNodes[0].name);

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaInnUnitAlloc"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell8 = rowobj.insertCell(8);
	cell8.style.textAlign = "left";
	cell8.innerHTML = '<input type="text" name = "txtFldFnaIncCurrNav" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell9 = rowobj.insertCell(9);
	cell9.style.textAlign = "left";
	cell9.innerHTML = '<input type = "text"  name="txtFldFnaInvAnnlTopup"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell10 = rowobj.insertCell(10);
	cell10.style.textAlign = "left";
	cell10.innerHTML = '<input type = "text"  name="txtFldFnaInvCpfYrtoTopup"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell11 = rowobj.insertCell(11);
	cell11.style.textAlign = "left";
	cell11.innerHTML = '<input type = "text"  name="txtFldFnaInvFvTrantoCpf"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	// --

	var cell12 = rowobj.insertCell(12);
	cell12.style.textAlign = "left";
	cell12.innerHTML = '<input type = "text"  name="txtFldFnaInvRpYrtoTopup"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell13 = rowobj.insertCell(13);
	cell13.style.textAlign = "left";
	cell13.innerHTML = '<input type = "text"  name="txtFldFnaInvCashRetPrcnt"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell14 = rowobj.insertCell(14);
	cell14.style.textAlign = "left";
	cell14.innerHTML = '<input type = "text"  name="txtFldFnaInvFvSrsRet"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell15 = rowobj.insertCell(15);
	cell15.style.textAlign = "left";
	cell15.innerHTML = '<input type = "text"  name="txtFldFnaInvEdnCashtoTopup"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);" />';

	var cell16 = rowobj.insertCell(16);
	cell16.style.textAlign = "left";
	cell16.innerHTML = '<input type = "text"  name="txtFldFnaInvCashEdnPrcnt"  class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell17 = rowobj.insertCell(17);
	cell17.style.textAlign = "left";
	cell17.innerHTML = '<input type = "text"  name="txtFldFnaInvFvChildEdn"  class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell18 = rowobj.insertCell(18);
	cell18.style.textAlign = "left";
	cell18.innerHTML = '<input type = "text"  name="txtFldFnaInvChilName"  class="fpEditTblTxt"  maxlength="60" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	var cell19 = rowobj.insertCell(19);
	cell19.style.textAlign = "left";
	cell19.innerHTML = '<input type = "text"  name="txtFldFnaInvRem"  class="fpEditTblTxt"  maxlength="300" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	comboMakerById(cell1.childNodes[0], fmArr, '', true);// added by johnson
	// on 07052015
	comboMaker(cell4, fundPayMethArr, "", true);
	// datePickerCall(cell6.childNodes[0].name);//added by johnson on 09032015

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

}

function fnaInvstPlanModRow() {
}
function fnaInvstPlanDelRow(tblid) {
	deleteRowInTbl(tblid, false);
}
function fnaInvstPlanExpndRow() {
}

function fnaAdvRecAddRow(tblid) {

	if (prdtplanArr.length == 0) {
		// getPrdtPlanName();
	}

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaAdvRecPrdtDet())
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaAdvRecSiNo" readOnly="true" value="'
			+ (rowlenobj + 1)
			+ '" class="fpEditTblTxt" />'
			+ '<input type = "hidden"  name="txtFldFnaAdvRecMode" value="'
			+ INS_MODE
			+ '" />'
			+ '<input type = "hidden"  name="txtFldFnaAdvRecId"  />'
			+ '<select name="selFnaAdvRecRel" style="display:none" class="kycSelect" ><option ></option></select>'
			+ '<input type = "hidden"  name="txtFldFnaAdvRecName"  class="fpEditTblTxt"  maxlength="60" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';

	/**
	 * cell0.childNodes[3] and cell0.childNodes[4] are added by Johnson on
	 * 29042015 in order to commend the next two cells and the elements from
	 * those cells are added in the cell0 and these element are not visible in
	 * the screen
	 */

	// var cell1 = rowobj.insertCell(1);
	// cell1.style.textAlign="left";
	// cell1.innerHTML ='<select name="selFnaAdvRecRel" class="kycSelect"
	// ></select>';
	// var cell2 = rowobj.insertCell(2);
	// cell2.style.textAlign="left";
	// cell2.innerHTML ='<input type = "text" name="txtFldFnaAdvRecName"
	//  class="fpEditTblTxt"
	// maxlength="60"onclick="hideTooltip()"
	// onmouseover="showTooltip(event,this.value);"
	// onmouseout="hideTooltip();"/>';
	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<select name="selFnaAdvRecPrdtType" class="kycSelect" onchange="clearPlanNames(this)"></select>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<select name="selFnaAdvRecCompName" class="kycSelect" style="width:250px" onchange="getPrdtPlanName(this)" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" ></select><select name="selFnaAdvRecPrdtName" onchange="setPlanName(this)" class="kycSelect" style="width:250px" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"></select><input type="hidden" name="selFnaAdvRecPrdtPlan"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaAdvRecSumAssr" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<select name="selFnaAdvRecPayment" class="kycSelect" ></select>';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaAdvRecPlanTerm" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaAdvRecPremTerm" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaAdvRecPrem" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	// comboMaker(cell1,relArray,"",false); commented by Johnson on 29042015

//	comboMaker(cell1, prdtArray, "", true);
	comboMakerByKeyVal(STR_PRODTYPE,cell1.childNodes[0], "",true);

	
	comboMakerById(cell2.childNodes[0], prinIdArr, "", true);
	comboMakerById(cell0.childNodes[3], [], "", true);
	addSelectOption(cell2.childNodes[1]);
	comboMaker(cell4, paymentModeArr, "", true);

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

}

function fnaAdvRecModRow() {
}
function fnaAdvRecDelRow(tblid) {
	deleteRowInTbl(tblid, true);
}
function fnaAdvRecExpndRow() {
}

function getPrdtPlanName(prinobj) {

	var prdtTypeObj = prinobj.parentNode.previousSibling.childNodes[0];
	var prdtType = prdtTypeObj.value;
	var planNameObj = prinobj.parentNode.childNodes[2];
	var prinId = prinobj.value;

	if (!checkMandatoryField(prdtTypeObj, "Select the product type!")) {
		prinobj.value = "";
		return;
	}

	if (!isEmpty(prinId)) {
		
		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");

		loadMsg.style.display = "block";
		// alert(loadMsg.style.display)
		loadingLayer.style.display = "block";


		var parameter = "dbcall=PRDT_PLAN&selPrinId=" + prinId
				+ "&selPrdtType=" + encodeURIComponent(prdtType);

		var respText = ajaxCall(parameter);
		var retval = respText;
		prdtNamesArr.length = 0;
		fundRateArr.length=0;
		prodtypeArr.length=0;
		
		loadMsg.style.display = "none";

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}

			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var key = tab;
					var value = tabdets[tab];

					if (key == "MAST_PRDTPLANDET_ARR") {
						for ( var val in value) {
							var prdtdata = value[val];

							var planName = prdtdata["txtFldPlanName"];
							var fundRating = prdtdata["txtFldFundRating"];
							var prodType=prdtdata["txtFldProdType"];

							if (!isEmpty(prdtdata["txtFldPlanCode"])) {
								planName += "--->" + prdtdata["txtFldPlanCode"];
							}

							if (!isEmpty(prdtdata["txtFldCompCode"])) {
								planName += "[" + prdtdata["txtFldCompCode"]
										+ "]";
							}

							prdtNamesArr[prdtNamesArr.length++] = new Option(prdtdata["txtFldProdCode"], planName);
							fundRateArr[fundRateArr.length++]= fundRating;
							prodtypeArr[prodtypeArr.length++]=prodType;

						}

					}

				}
			}
		}

		clearSelectBoxOptions(planNameObj);

		if (prdtNamesArr.length > 0) {
			comboMakerById(planNameObj, prdtNamesArr, "", false);
		} else {
//			alert('No Product for ' + prdtType + ' and '+ prinobj.options[prinobj.selectedIndex].text );
//			Commentted on MAR_2018

		}
	} else {
		clearSelectBoxOptions(planNameObj);
	}
}

function setPlanName(selobj) {

	selobj.nextSibling.value = selobj.options[selobj.selectedIndex].text;

	if(selobj.parentNode.parentNode.childNodes[1].childNodes[0].value == "UT"){
		selobj.parentNode.parentNode.childNodes[3].childNodes[0].value=fundRateArr[selobj.selectedIndex-1];
		selobj.parentNode.parentNode.childNodes[3].childNodes[0].readOnly=true;
	}

	selobj.parentNode.parentNode.childNodes[2].childNodes[6].value=prodtypeArr[selobj.selectedIndex-1];



}

function clearPlanNames(selobj) {

	var planNameObj = selobj.parentNode.nextSibling.childNodes[1];
	var planNameHidden = selobj.parentNode.nextSibling.childNodes[2];

	var cmpnyNameSelObj = selobj.parentNode.nextSibling.childNodes[0];// aded by johnson on 24042015

	if (!isEmpty(cmpnyNameSelObj.value)) {
		cmpnyNameSelObj.selectedIndex = 0;
	}// end of if

	clearSelectBoxOptions(planNameObj);
	planNameHidden.value = "";
}

function editPrdtPlanName(imgobj){

	var planNameObj = imgobj.parentNode.childNodes[1];
	var planNameHidden = imgobj.parentNode.childNodes[2];
	var planNameText = imgobj.parentNode.childNodes[4];
	var image2 = imgobj.parentNode.childNodes[5];
	var optObj = imgobj.parentNode.childNodes[7];

	planNameObj.value="";
	planNameHidden.value="";
	optObj.value="";

	planNameObj.style.display="none";
	planNameText.style.display="inline";
	imgobj.style.display="none";
	image2.style.display="";
	optObj.value="CUSTOM";

}

function resetPrdtPlanName(imgobj){

	var planNameObj = imgobj.parentNode.childNodes[1];
	var planNameHidden = imgobj.parentNode.childNodes[2];
	var image1 = imgobj.parentNode.childNodes[3];
	var planNameText = imgobj.parentNode.childNodes[4];
	var optObj = imgobj.parentNode.childNodes[7];

	planNameObj.value="";
	planNameHidden.value="";
	planNameText.value = "";
	optObj.value="";


	planNameObj.style.display="inline";
	planNameText.style.display="none";
	imgobj.style.display="none";
	image1.style.display="";
	optObj.value="PRE";
}


function fnaArtuPlanAddRow(tblid,ParentSts,obj) {

	var tblobj    = document.getElementById(tblid);
	var tbodyobj  = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaArtuPlantDet(true))
		return;

	if(ParentSts.toUpperCase()=='Y'){
//		rowlenobj=(getParentByTagName(obj,'tr').rowIndex)-1;
		var clntGrpName=getParentByTagName(obj,'td').childNodes[1].value;
		var crntFld=$('[value="'+clntGrpName+'"]');
		var crntLth=crntFld.length;
		rowlenobj=getParentByTagName(crntFld.get(crntLth-1),'tr').rowIndex;
	}

	var rowobj = tbodyobj.insertRow(rowlenobj);

//	var cell0 = rowobj.insertCell(0);
//	cell0.style.textAlign = "left";
//	cell0.rowSpan='1';
//	cell0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaArtuPlanSiNo" readOnly="true" value="'	+ (rowlenobj + 1)+ '" class="fpEditTblTxt" style="text-align:center" />';

	/*var cell0a = rowobj.insertCell(1);//kavi
	cell0a.rowSpan='1';
	cell0a.innerHTML="<input type='text' value='' style='width:100px;display:inline-block;'>"+
					 "<img src='images/addrow.png'   style='cursor: pointer;display:inline-block;' height='16px' align='top' id='' onclick='fnaArtuPlanAddSubRow(this)'/>"; */

	var cell1 = rowobj.insertCell(0);
	cell1.style.textAlign = "left";
	cell1.style.border="1px solid black";
	cell1.innerHTML = '<select name="selFnaArtuPlanPrdtType" class="kycSelect" onchange="/*clearPlanNames(this)*/"></select>'+
		'<input type = "hidden"  name="txtFldFnaArtuPlanMode" value="'	+ INS_MODE+ '" />';
//		'<input type = "hidden"  name="txtFldFnaArtuPlanId"  />';

	var cell2 = rowobj.insertCell(1);
	cell2.style.textAlign = "left";
	cell2.style.border="1px solid black";
	cell2.innerHTML ='<select name="selFnaArtuPlanCompName" class="kycSelect" onchange="getPrdtPlanName(this)" style="width:200px;display:inline;" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"/></select>'+
	'<input type="text"  name="txtFldFnaArtuPlanCompName" class="fpEditTblTxt" style="width:200px;display:none"  maxlength="60">'+
	'<select name="selFnaArtuPlanName" onchange="setPlanName(this)" onclick="hideTooltip();chkCompDets(this);" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" class="kycSelect" style="width:200px;display:none"></select>'+
	'<input type="hidden" name="selFnaArtuPlan"/>'+
	'<img src="images/modify.png" onclick="editPrdtPlanName(this)" style="display:none"/>'+
	'<input type="text"  name="txtFldFnaArtuPlanNameTxt" class="fpEditTblTxt" style="display:inline;width:200px"  maxlength="300"/>'+
	'<img src="images/history.png" onclick="resetPrdtPlanName(this)" style="display:none"/>'+
	'<select name="selFnaArtuPrdtCatage" class="kycSelect" style="width:80px;display:inline"><option value="">--SELECT--</option><option value="B">Basic</option><option value="R">Rider</option></select>'+
	'<input type="hidden" name="selFnaArtuOpt" value="CUSTOM"/>';

	var cell3 = rowobj.insertCell(2);
	cell3.style.textAlign = "left";
	cell3.style.border="1px solid black";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaArtuPlanSumAssr" class="fpEditTblTxt"   maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell4 = rowobj.insertCell(3);
	cell4.style.textAlign = "left";
	cell4.style.border="1px solid black";
	cell4.innerHTML = '<select name="selFnaArtuPlanPayment" class="kycSelect"></select>';

	var cell5 = rowobj.insertCell(4);
	cell5.style.textAlign = "left";
	cell5.style.border="1px solid black";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaArtuPlanPlanTerm" class="fpEditTblTxt"  maxlength="30"/>';

	var cell6 = rowobj.insertCell(5);
	cell6.style.textAlign = "left";
	cell6.style.border="1px solid black";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaArtuPlanPremTerm" class="fpEditTblTxt"  maxlength="30"/>';

	var cell7 = rowobj.insertCell(6);
	cell7.style.textAlign = "left";
	cell7.style.border="1px solid black";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaArtuPlanPrem" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	
	var cell8 = rowobj.insertCell(7);
	cell8.style.textAlign = "left";
	cell8.style.border="1px solid black";
	cell8.innerHTML = '<select name="selFnaArtuPlanRiskRate" class="kycSelect"></select>';

	
//	comboMaker(cell1, prdtArray,"Insurance", false);
	comboMakerByKeyVal(STR_PRODTYPE,cell1.childNodes[0], "Insurance",false);

	comboMakerById(cell2.childNodes[0], prinIdArr, "", true);
	addSelectOption(cell2.childNodes[2]);
	comboMaker(cell4,paymentModeArr, "", true);
	comboMakerByKeyVal(STR_ILP_RISK_RATE,cell8.childNodes[0], "",true);
	
// Poovathi Add on 18-10-2019.	
// Poovathi Add on 06-11-2019.		
	
	 var cell0PrdType = rowobj.cells[0].childNodes[0].value;
	 
	   if((cell0PrdType == "Insurance")||(cell0PrdType == "H&S")||(cell0PrdType == "PA") ) {
		rowobj.cells[7].childNodes[0].disabled = true;
		rowobj.cells[7].childNodes[0].value="";
		rowobj.cells[7].childNodes[0].options[0].innerHTML = "-N/A-";
//		rowobj.cells[1].childNodes[0].style.display="";
//		rowobj.cells[1].childNodes[1].style.display="none";
		
       }else{
		rowobj.cells[7].childNodes[0].disabled = false;
		rowobj.cells[7].childNodes[0].options[0].innerHTML = "--SELECT--";
//		rowobj.cells[1].childNodes[0].style.display="none";
//		rowobj.cells[1].childNodes[1].style.display="";
		}
	
	
	
// Poovathi Add on 17-10-2019.
	
	rowobj.cells[0].childNodes[0].onchange = function(){
     var curtval = $(this).val();
		
     if((curtval == "Insurance")||(curtval == "H&S")||(curtval == "PA"))
		{
    	 rowobj.cells[7].childNodes[0].disabled = true;
    	 rowobj.cells[7].childNodes[0].value="";
    	 rowobj.cells[7].childNodes[0].options[0].innerHTML = "-N/A-";
//    	 rowobj.cells[1].childNodes[0].style.display="";
// 		 rowobj.cells[1].childNodes[1].style.display="none";
 		

        
		}else{
    	 rowobj.cells[7].childNodes[0].disabled = false;
    	 rowobj.cells[7].childNodes[0].options[0].innerHTML = "--SELECT--";
//    	 rowobj.cells[1].childNodes[0].style.display="none";
// 		 rowobj.cells[1].childNodes[1].style.display="";
       }		
		
	};
	

	
	

	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}

	document.getElementById("page7addrow").value="CHANGED";
	checkChanges(document.getElementById("page7addrow"));
	cell2.childNodes[1].focus()

	
	//	 poovathi Add Autocomplete  to insert function in page 6  on 29-10-2019.
		
		var list_of_principal = [];
		 list_of_principal =  copyTextArrFromOpt(rowobj.cells[1].childNodes[0]);						 
		 var prinobj_ = $(rowobj).find("td:eq(1)").find("input:eq(0)");						 
		 prinobj_.autocomplete({
			 source:list_of_principal
			 
		 });
	
	rowobj.onclick = function() {
//		var tempRefId=this.childNodes[1].childNodes[1].value;
//		var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");
		var rows =getParentByTagName(this,'table').tBodies[0].rows;
		var subRows=document.getElementById('fnaartuplanTblss').tBodies[0].rows;

		for (iCel=0; iCel < rows.length; iCel++)
		{
			 removeClass(rows[iCel],'fnaPlnSelected');
			 removeClass(subRows[iCel],'fnaPlnSelected');
		}

		addClass(this,'fnaPlnSelected');
	};

	//below parent client table added by kavi on 12/06/2018
	var tblobjClnt = document.getElementById("fnaartuplanTblss");
	var tbodyobjClnt = tblobjClnt.tBodies[0];
	var rowlenobjClnt = tbodyobjClnt.rows.length;

	if(ParentSts.toUpperCase()=='Y'){
		var clntGrpName=getParentByTagName(obj,'td').childNodes[1].value;
		var crntFld=$('[value="'+clntGrpName+'"]');
		var crntLth=crntFld.length;
		rowlenobjClnt=getParentByTagName(crntFld.get(crntLth-1),'tr').rowIndex;
	}

	var rowobjClnt = tbodyobjClnt.insertRow(rowlenobjClnt);
	var Sino=$("input[name='txtFldFnaArtuPlanSiNo']").length
	var crntDate=new Date();
	var tempRefId =	"TEMP"+crntDate.getDay()+crntDate.getMonth()+crntDate.getFullYear()+crntDate.getHours()+crntDate.getMinutes()+crntDate.getSeconds()+((Number(Sino)+1));

	addClass(rowobjClnt,'prntRowTbl');

	var cells0 = rowobjClnt.insertCell(0);
	cells0.style.textAlign = "left";
	cells0.style.border="1px solid black";
	cells0.style.height="20px";
	cells0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaArtuPlanSiNo" readOnly="true" value="'+((Number(Sino)+1))+ '" class="fpEditTblTxt" style="text-align:center" />';


	var cells1 = rowobjClnt.insertCell(1);
	cells1.style.textAlign = "left";
	cells1.style.border="1px solid black";
	cells1.innerHTML = '<input type = "text" size="3"  name="txtFldRecomPpName" value="" class="fpEditTblTxt" style="width:150px;display:inline"/>'+
					   '<input type = "hidden"  name="txtFldPrntTblRefId" value="'+tempRefId+'"/>'+
					   '<img src="images/addrow.png"   style="cursor: pointer;" height="16px" align="top" id="imgFnaArtuPlanAddRow" onclick="fnaArtuPlanAddRow(&apos;fnaartuplanTbl&apos;,&apos;Y&apos;,this)"/>';

	cells1.childNodes[0].onkeyup=function(){
		var tempRefId=getParentByTagName(this,'td').childNodes[1].value;
		var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");

		for(var i=0;i<arrElm.length;i++){
			arrElm[i].parentNode.firstChild.value=this.value;
		}
	};

	cells1.childNodes[0].onchange=function(){
		var arrPrntElm=$('tr.prntRowTbl:visible');
		var crntTrIndex=getParentByTagName(this,'tr').rowIndex;

		for(var i=0;i<arrPrntElm.length;i++){
			if(crntTrIndex!=arrPrntElm[i].rowIndex){
				if(arrPrntElm[i].childNodes[1].childNodes[0].value.trim().equalIgnoreCase(this.value.trim())){
					this.value="";
					alert("Client(1)/Client(2) name already exist");
					this.focus();
					return false;
				}
			}
		}

	};

	if(ParentSts.toUpperCase()=='Y'){
		removeClass(rowobjClnt,'prntRowTbl');

		var clientName=obj.parentNode.firstChild.value;
		var clientRef=obj.parentNode.childNodes[1].value;

		cells0.innerHTML="";
		cells1.innerHTML='<input type = "hidden" size="3"  name="txtFldRecomPpName" readOnly="true" value="'+clientName+'" class="fpEditTblTxt" style="width:150px;display:inline"/>'+
						 '<input type = "hidden"  name="txtFldPrntTblRefId" value="'+clientRef+'" style=""/>';

		reArrngCellBorder(clientRef,'fnaartuplanTblss');
	}

	if(ParentSts.toUpperCase()=='N'){
		rowobjClnt.childNodes[1].childNodes[0].focus();
	}

	rowobjClnt.onclick = function() {
		var tempRefId=this.childNodes[1].childNodes[1].value;
		var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");
		var rows =getParentByTagName(this,'table').tBodies[0].rows;
		var subRows=document.getElementById('fnaartuplanTbl').tBodies[0].rows;

		for (iCel=0; iCel < rows.length; iCel++)
		{
			 removeClass(rows[iCel],'fnaPlnSelected')
			 removeClass(subRows[iCel],'fnaPlnSelected')
		}

		for(var i=0;i<arrElm.length;i++){
			addClass(getParentByTagName(arrElm[i],'tr'),'fnaPlnSelected');
			var rowNo=getParentByTagName(arrElm[i],'tr').rowIndex;
			addClass(tbodyobj.rows[rowNo-1],'fnaPlnSelected');
		}
	};


	tblResetScroll();
}

function fnaArtuPlanModRow() {
}

function fnaArtuPlancDelRow(tblid) {
//	deleteRowInTbl(tblid, true);
//	document.getElementById("page7addrow").value="CHANGED";
//	checkChanges(document.getElementById("page7addrow"));
	var strDelSts=false;
	var strBothTblDel=false;

	var clntGrpTbl=document.getElementById("fnaartuplanTblss");
	var clntTblBdy=clntGrpTbl.tBodies[0];
	var clntTblLnth=clntTblBdy.rows.length;

	var fnaPlnTbl=document.getElementById("fnaartuplanTbl");
	var fnaPlnTblBdy=fnaPlnTbl.tBodies[0];
	var fnaPlnTblLnth=fnaPlnTblBdy.rows.length;
	var RowIndex;

	for (var i=0;i<clntTblLnth;i++){
		if(hasClass(clntTblBdy.rows[i],'fnaPlnSelected')){
			strBothTblDel=true;
			break;
		}
	}


	if(strBothTblDel){

		if (clntGrpTbl.getElementsByClassName('fnaPlnSelected'))
		{
				var sltRow=clntGrpTbl.getElementsByClassName('fnaPlnSelected').length;
				var clientName=clntGrpTbl.getElementsByClassName('fnaPlnSelected')[0].childNodes[1].firstChild.value;

				if(sltRow > 1 && !confirm(FNA_ADV_REC_TBLDEL_CONFIRM.replace("~CLIENT~",clientName)))return false;

				strDelSts=true;
		}

		for (var i=fnaPlnTblLnth;i>0;i--){
			RowIndex=i-1;
			if(hasClass(fnaPlnTblBdy.rows[RowIndex],'fnaPlnSelected')){

				var mode=fnaPlnTblBdy.rows[RowIndex].firstChild.childNodes[1].value

				if(mode != UPD_MODE){
				$(fnaPlnTblBdy.rows[RowIndex]).remove();
//				removeClass(fnaPlnTblBdy.rows[RowIndex],"fnaPlnSelected");

				var clntRefId=clntTblBdy.rows[RowIndex].childNodes[1].childNodes[1].value;
				$('[value="'+clntRefId+'"]').closest("tr").attr("row-ref",clntRefId);

				var crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);
				getParentByTagName(crntFld,'tr').parentNode.removeChild(getParentByTagName(crntFld,'tr'));
//				removeClass(clntTblBdy.rows[RowIndex],"fnaPlnSelected");

				crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);
					if(crntFld){
						getParentByTagName(crntFld,'tr').childNodes[0].style.borderBottom='1px solid black'
						getParentByTagName(crntFld,'tr').childNodes[1].style.borderBottom='1px solid black'
					}

				}else{
					$(fnaPlnTblBdy.rows[RowIndex]).hide();
					removeClass(fnaPlnTblBdy.rows[RowIndex],"fnaPlnSelected");

					var clntRefId=clntTblBdy.rows[RowIndex].childNodes[1].childNodes[1].value;
					$('[value="'+clntRefId+'"]').closest("tr").attr("row-ref",clntRefId);
					fnaPlnTblBdy.rows[RowIndex].firstChild.childNodes[1].value=DEL_MODE;

					var crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);
					$(getParentByTagName(crntFld,'tr')).hide();
					removeClass(clntTblBdy.rows[RowIndex],"fnaPlnSelected");

					crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);

					if(crntFld){
						getParentByTagName(crntFld,'tr').childNodes[0].style.borderBottom='1px solid black'
						getParentByTagName(crntFld,'tr').childNodes[1].style.borderBottom='1px solid black'
					}
				}

			}
		}

	}else{
		for (var i=0;i<fnaPlnTblLnth;i++){
			if(hasClass(fnaPlnTblBdy.rows[i],'fnaPlnSelected')){
				RowIndex=i;
//				fnaPlnTblBdy.rows[i].parentNode.removeChild(fnaPlnTblBdy.rows[i]);
				strDelSts=true;
				break;
			}
		}

		if(strDelSts && confirm(FNA_PROD_REC_TBLROWDEL_CONFIRM)){

		var mode=fnaPlnTblBdy.rows[RowIndex].firstChild.childNodes[1].value

		if(mode != UPD_MODE){
		fnaPlnTblBdy.rows[RowIndex].parentNode.removeChild(fnaPlnTblBdy.rows[i]);

		var clntRefId=clntTblBdy.rows[RowIndex].childNodes[1].childNodes[1].value;
		var crntFld=$('[value="'+clntRefId+'"]:last').get(0);
		getParentByTagName(crntFld,'tr').parentNode.removeChild(getParentByTagName(crntFld,'tr'));

		crntFld=$('[value="'+clntRefId+'"]:last').get(0);
			if(crntFld){
				getParentByTagName(crntFld,'tr').childNodes[0].style.borderBottom='1px solid black'
				getParentByTagName(crntFld,'tr').childNodes[1].style.borderBottom='1px solid black'
			}

		}else{
			$(fnaPlnTblBdy.rows[RowIndex]).hide();
			removeClass(fnaPlnTblBdy.rows[RowIndex],"fnaPlnSelected");

			var clntRefId=clntTblBdy.rows[RowIndex].childNodes[1].childNodes[1].value;
			$('[value="'+clntRefId+'"]').closest("tr").attr("row-ref",clntRefId);
			fnaPlnTblBdy.rows[RowIndex].firstChild.childNodes[1].value=DEL_MODE;

			var crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);
			$(getParentByTagName(crntFld,'tr')).hide();

			crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);

			if(crntFld){
				getParentByTagName(crntFld,'tr').childNodes[0].style.borderBottom='1px solid black'
				getParentByTagName(crntFld,'tr').childNodes[1].style.borderBottom='1px solid black'
			}
		}


		}

	}

	if(!strDelSts){
		alert('No row Selected');
	}else{
		var PrntTr=$('tr.prntRowTbl:visible');
		for(i=0;i<PrntTr.length;i++){
			PrntTr[i].childNodes[0].childNodes[0].value=i+1;
		}
	}

}

function fnaArtuPlancExpndRow() {
}



function editFundPlanName(imgobj){

	var planNameObj = imgobj.parentNode.childNodes[1];
	var planNameHidden = imgobj.parentNode.childNodes[2];
	var planNameText = imgobj.parentNode.childNodes[4];
	var image2 = imgobj.parentNode.childNodes[5];
	var planOpt = imgobj.parentNode.childNodes[7];

	planNameObj.value="";
	planNameHidden.value="";
	planOpt.value="";

	planNameObj.style.display="none";
	planNameText.style.display="inline";
	imgobj.style.display="none";
	image2.style.display="";
	planOpt.value="CUSTOM";

}

function resetFundPlanName(imgobj){

	var planNameObj = imgobj.parentNode.childNodes[1];
	var planNameHidden = imgobj.parentNode.childNodes[2];
	var image1 = imgobj.parentNode.childNodes[3];
	var planNameText = imgobj.parentNode.childNodes[4];
	var planOpt = imgobj.parentNode.childNodes[7];

	planNameObj.value="";
	planNameHidden.value="";
	planNameText.value = "";
	planOpt.value="";

	planNameObj.style.display="inline";
	planNameText.style.display="none";
	imgobj.style.display="none";
	image1.style.display="";
	planOpt.value="PRE";
}


function fnaArtuFundAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;
	var jqRowlenobj=$("#"+tblid+" tbody tr:visible").length;

	if (!validateFnaArtuFundtDet(true))
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaArtuFundSiNo" readOnly="true" value="'+ (jqRowlenobj + 1)+ '" class="fpEditTblTxt" style="text-align:center;" />'+
	'<input type = "hidden"  name="txtFldFnaArtuFundMode" value="'+ INS_MODE+'"/>'+
	'<input type = "hidden"  name="txtFldFnaArtuFundId"  />';
	//changed by Thulasy 16.03.2018 -->
	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<select name="selFnaArtuFundPrdtType" class="kycSelect" onchange="/*loadPrinOrFundMgr(this)*/" ></select>';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<select name="selFnaArtuFundCompName" class="kycSelect" onchange="getPrdtPlanName(this)" style="width:200px;display:none;" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" ></select>'+
	'<input type="text"  name="txtFldFnaArtuFundCompName" class="fpEditTblTxt" style="display:inline;width:200px"  maxlength="60">'+
	'<select name="selFnaArtuFundProdCode" onchange="setPlanName(this)" onclick="hideTooltip();chkCompDets(this);" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" class="kycSelect" style="width:200px;display:none"></select>'+
	'<input type="hidden" name="selFnaArtuFundPlan"/>'+
	'<img src="images/modify.png" onclick="editFundPlanName(this)"  style="display:none"/>'+
	'<input type="text"  name="txtFnaArtuFundPlanTxt" class="fpEditTblTxt" style="display:inline;width:200px"  maxlength="150"/>'+
	'<img src="images/history.png" onclick="resetFundPlanName(this)" style="display:none" />'+
	'<select name="selFnaArtuFundCatage" class="kycSelect" style="width:80px;display:none"><option value="">--SELECT--</option><option value="B">Basic</option><option value="R">Rider</option></select>'+
	'<input type="hidden" name="selFnaArtuFundOpt" value="CUSTOM"/>';


	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaArtuFundRisk" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<select name="selFnaArtuFundPayment" class="kycSelect"></select>';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaArtuFundPurSale" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaArtuFundPurAmt" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaArtuFundPurPrct" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	comboMaker(cell1, fundPrdtArr,"UT",false);
	addSelectOption(cell2.childNodes[0]);
	addSelectOption(cell2.childNodes[2]);
	// comboMaker(cell2,fundPrdtArr,"",true);
	comboMaker(cell4, fundPayMethArr, "", true);

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}

	document.getElementById("page8addrow").value="CHANGED";
	checkChanges(document.getElementById("page8addrow"));
	cell2.childNodes[1].focus()
}
function fnaArtuFundModRow() {
}
function fnaArtuFundcDelRow(tblid) {
	deleteRowInTbl(tblid, true);
	document.getElementById("page8addrow").value="CHANGED";
	checkChanges(document.getElementById("page8addrow"));
}

function fnaArtuFundcExpndRow() {
}



function editSwrepPlanName(imgobj){

	var planNameObj = imgobj.parentNode.childNodes[1];
	var planNameHidden = imgobj.parentNode.childNodes[2];
	var planNameText = imgobj.parentNode.childNodes[4];
	var image2 = imgobj.parentNode.childNodes[5];
	var planOpt = imgobj.parentNode.childNodes[7];

	planNameObj.value="";
	planNameHidden.value="";
	planOpt.value="";

	planNameObj.style.display="none";
	planNameText.style.display="inline";
	imgobj.style.display="none";
	image2.style.display="";
	planOpt.value="CUSTOM";

}

function resetSwrepPlanName(imgobj){

	var planNameObj = imgobj.parentNode.childNodes[1];
	var planNameHidden = imgobj.parentNode.childNodes[2];
	var image1 = imgobj.parentNode.childNodes[3];
	var planNameText = imgobj.parentNode.childNodes[4];
	var planOpt = imgobj.parentNode.childNodes[7];

	planNameObj.value="";
	planNameHidden.value="";
	planNameText.value = "";
	planOpt.value="";

	planNameObj.style.display="inline";
	planNameText.style.display="none";
	imgobj.style.display="none";
	image1.style.display="";
	planOpt.value="PRE";
}

function fnaSwrepPlanAddRow(tblid,ParentSts,obj) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaSwrepPlantDet(true))
		return;

	if(ParentSts.toUpperCase()=='Y'){
//		rowlenobj=(getParentByTagName(obj,'tr').rowIndex)-1;
		var clntGrpName=getParentByTagName(obj,'td').childNodes[1].value;
		var crntFld=$('[value="'+clntGrpName+'"]');
		var crntLth=crntFld.length;
		rowlenobj=getParentByTagName(crntFld.get(crntLth-1),'tr').rowIndex;
	}


	var rowobj = tbodyobj.insertRow(rowlenobj);

	/*var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaSwrepPlanSiNo" readonly="readonly" value="'
			+ (rowlenobj + 1)
			+ '" class="fpEditTblTxt" style="text-align:center"/><input type = "hidden"  name="txtFldFnaSwrepPlanMode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaSwrepPlanId"  />';*/

	var cell1 = rowobj.insertCell(0);
	cell1.style.textAlign = "left";
	cell1.style.border="1px solid black";
	cell1.innerHTML = '<select name="selFnaSwrepPlanPrdtType" class="kycSelect" onchange="/*clearPlanNames(this)*/"></select>'+
				      '<input type = "hidden"  name="txtFldFnaSwrepPlanMode" value="'+INS_MODE+'" />';

	//changed by Thulasy 16.03.2018 -->
	var cell2 = rowobj.insertCell(1);
	cell2.style.textAlign = "left";
	cell2.style.border="1px solid black";
	cell2.innerHTML = '<select name="selFnaSwrepPlanCompName" class="kycSelect" style="width:200px;display:inline" onchange="getPrdtPlanName(this)"onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"></select>'+
	'<input type="text"  name="txtFldFnaSwrepPlanCompName" class="fpEditTblTxt" style="display:none;width:200px"  maxlength="60">'+
	'<select name="selFnaSwrepPlanName" onchange="setPlanName(this)" onclick="hideTooltip();chkCompDets(this)" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();"class="kycSelect" style="width:200px;display:none"></select>'+
	'<input type="hidden" name="selFnaSwrepPlan"/>'+
	'<img src="images/modify.png" onclick="editSwrepPlanName(this)"  style="display:none"/>'+
	'<input type="text"  name="selFnaSwrepPlanNameTxt" class="fpEditTblTxt" style="display:inline;width:200px"  maxlength="300"/>'+
	'<img src="images/history.png" onclick="resetSwrepPlanName(this)"  style="display:none"/>'+
	'<select name="selFnaSwrepCateg" class="kycSelect" style="width:80px;display:inline"><option value="">--SELECT--</option><option value="B">Basic</option><option value="R">Rider</option></select>'+
	'<input type="hidden" name="selFnaSwrepOpt" value="CUSTOM"/>';


	var cell3 = rowobj.insertCell(2);
	cell3.style.textAlign = "left";
	cell3.style.border="1px solid black";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaSwrepPlanSA" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell4 = rowobj.insertCell(3);
	cell4.style.textAlign = "left";
	cell4.style.border="1px solid black";
	cell4.innerHTML = '<select name="selFnaSwrepPlanPayment" class="kycSelect"></select>';

	var cell5 = rowobj.insertCell(4);
	cell5.style.textAlign = "left";
	cell5.style.border="1px solid black";
	cell5.innerHTML = '<select name="selFnaSwrepPlanSwitch" class="kycSelect"></select>';

	var cell6 = rowobj.insertCell(5);
	cell6.style.textAlign = "left";
	cell6.style.border="1px solid black";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaSwrepPlanTerm" class="fpEditTblTxt"  maxlength="30"/>';

	var cell7 = rowobj.insertCell(6);
	cell7.style.textAlign = "left";
	cell7.style.border="1px solid black";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaSwrepPlanPayTerm" class="fpEditTblTxt" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell8 = rowobj.insertCell(7);
	cell8.style.textAlign = "left";
	cell8.style.border="1px solid black";
	cell8.innerHTML = '<input type = "text"  name="txtFldFnaSwrepPlanPrem" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';
	
	var cell9 = rowobj.insertCell(8);
	cell9.style.textAlign = "left";
	cell9.style.border="1px solid black";
	cell9.innerHTML = '<select name="selFnaSwrepPlanRiskRate" class="kycSelect"></select>';


//	comboMaker(cell1, prdtArray,"Insurance", false);
	comboMakerByKeyVal(STR_PRODTYPE,cell1.childNodes[0], "Insurance",false);	
	comboMakerByKeyVal(STR_ILP_RISK_RATE,cell9.childNodes[0], "",true);
	
//	Poovathi Add on 18-10-2019.
	
	var cell0PrdType = rowobj.cells[0].childNodes[0].value;
	
    if(cell0PrdType == "ILP" ){
    	
		 rowobj.cells[8].childNodes[0].disabled = false;
		 rowobj.cells[8].childNodes[0].options[0].innerHTML = "--SELECT--";
//		 rowobj.cells[1].childNodes[0].style.display = "none";
//		 rowobj.cells[1].childNodes[1].style.display = "";
		 

		 
	 }else{
    	 rowobj.cells[8].childNodes[0].disabled = true;
    	 rowobj.cells[8].childNodes[0].value="";
    	 rowobj.cells[8].childNodes[0].options[0].innerHTML = "-N/A-";
//	     rowobj.cells[1].childNodes[1].style.display = "none";
//		 rowobj.cells[1].childNodes[0].style.display = "";

	 }
	
	
//	 Poovathi Add on 17-10-2019.
	
	    rowobj.cells[0].childNodes[0].onchange = function(){
		
        var curtval = $(this).val();
        
        if((curtval == "Insurance")||(curtval == "H&S")||(curtval == "PA") || (isEmpty(curtval))){
        	rowobj.cells[8].childNodes[0].disabled = true;
        	rowobj.cells[8].childNodes[0].value="";
        	 rowobj.cells[8].childNodes[0].options[0].innerHTML = "-N/A-";
//        	rowobj.cells[1].childNodes[1].style.display = "none";
//        	rowobj.cells[1].childNodes[0].style.display = "";
            
        
		}else{
			 rowobj.cells[8].childNodes[0].disabled = false;
			 rowobj.cells[8].childNodes[0].options[0].innerHTML = "--SELECT--";
//			 rowobj.cells[1].childNodes[0].style.display = "none";
//             rowobj.cells[1].childNodes[1].style.display = "";
             
           }
		
	};
	
	


	// comboMaker(cell2,prdtArray,"",true);
	comboMakerById(cell2.childNodes[0], prinIdArr, "", true);
	addSelectOption(cell2.childNodes[2]);

	comboMaker(cell4, paymentModeArr, "", true);
	comboMaker(cell5, swtypeArr, "", true);

//	 poovathi Add Autocomplete  to insert function in page 9 on 29-10-2019.
	
	var list_of_principal = [];
	 list_of_principal =  copyTextArrFromOpt(rowobj.cells[1].childNodes[0]);						 
	 var prinobj_ = $(rowobj).find("td:eq(1)").find("input:eq(0)");						 
	 prinobj_.autocomplete({
		 source:list_of_principal
		 
	 });
	
	
	rowobj.onclick = function() {
		var rows =getParentByTagName(this,'table').tBodies[0].rows;
		var subRows=document.getElementById('fnaswrepplanTblss').tBodies[0].rows;

		for (iCel=0; iCel < rows.length; iCel++)
		{
			 removeClass(rows[iCel],'fnaPlnSelected');
			 removeClass(subRows[iCel],'fnaPlnSelected');
		}

		addClass(this,'fnaPlnSelected');
	};

	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}

	document.getElementById("page10addrow").value="CHANGED";
	checkChanges(document.getElementById("page10addrow"));
	cell2.childNodes[1].focus()

	var tblobjClnt = document.getElementById("fnaswrepplanTblss");
	var tbodyobjClnt = tblobjClnt.tBodies[0];
	var rowlenobjClnt = tbodyobjClnt.rows.length;

	if(ParentSts.toUpperCase()=='Y'){
		var clntGrpName=getParentByTagName(obj,'td').childNodes[1].value;
		var crntFld=$('[value="'+clntGrpName+'"]');
		var crntLth=crntFld.length;
		rowlenobjClnt=getParentByTagName(crntFld.get(crntLth-1),'tr').rowIndex;
	}

	var rowobjClnt = tbodyobjClnt.insertRow(rowlenobjClnt);
	var Sino=$("input[name='txtFldFnaSwrepPlanSiNo']").length
	var crntDate=new Date();
	var tempRefId =	"TEMP"+crntDate.getDay()+crntDate.getMonth()+crntDate.getFullYear()+crntDate.getHours()+crntDate.getMinutes()+crntDate.getSeconds()+((Number(Sino)+1));

	addClass(rowobjClnt,'prntSwRepRowTbl');

	var cells0 = rowobjClnt.insertCell(0);
	cells0.style.textAlign = "left";
	cells0.style.border="1px solid black";
	cells0.style.height="20px";
	cells0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaSwrepPlanSiNo" readOnly="true" value="'+((Number(Sino)+1))+ '" class="fpEditTblTxt" style="text-align:center" />';


	var cells1 = rowobjClnt.insertCell(1);
	cells1.style.textAlign = "left";
	cells1.style.border="1px solid black";
	cells1.innerHTML = '<input type = "text" size="3"  name="txtFldSwrepPpName" value="" class="fpEditTblTxt" style="width:150px;display:inline"/>'+
					   '<input type = "hidden"  name="txtFldFnaSwrepPlanRefId" value="'+tempRefId+'"/>'+
					   '<img src="images/addrow.png"   style="cursor: pointer;" height="16px" align="top" id=""/>';

	cells1.childNodes[2].onclick=function(){
		fnaSwrepPlanAddRow("fnaswrepplanTbl","Y",this);
	};

	cells1.childNodes[0].onkeyup=function(){
		var tempRefrId=getParentByTagName(this,'td').childNodes[1].value;
		var arrElm=document.querySelectorAll("input[value="+tempRefrId+"]");
		var curntVal=this.value,crntObj=this;

		for(var i=0;i<arrElm.length;i++){
			arrElm[i].parentNode.firstChild.value=curntVal;
		}
		this.value=curntVal;
	};

	cells1.childNodes[0].onchange=function(){
		var arrPrntElm=$("tr.prntSwRepRowTbl:visible");
		var crntTrIndex=getParentByTagName(this,'tr').rowIndex;

		for(var i=0;i<arrPrntElm.length;i++){
			if(crntTrIndex!=arrPrntElm[i].rowIndex){
				if(arrPrntElm[i].childNodes[1].childNodes[0].value.trim().equalIgnoreCase(this.value.trim())){
					this.value="";
					alert("Client(1)/Client(2) name already exist");
					this.focus();
					return false;
				}
			}
		}

	};

	if(ParentSts.toUpperCase()=='Y'){
		removeClass(rowobjClnt,'prntSwRepRowTbl');

		var clientName=obj.parentNode.firstChild.value;
		var clientRef=obj.parentNode.childNodes[1].value;

		cells0.innerHTML="";
		cells1.innerHTML='<input type = "hidden" size="3"  name="txtFldSwrepPpName" readOnly="true" value="'+clientName+'" class="fpEditTblTxt" style="width:150px;display:inline"/>'+
						 '<input type = "hidden"  name="txtFldFnaSwrepPlanRefId" value="'+clientRef+'" style=""/>';

		reArrngCellBorder(clientRef,'fnaswrepplanTblss');
	}

	if(ParentSts.toUpperCase()=='N'){
		rowobjClnt.childNodes[1].childNodes[0].focus();
	}

	rowobjClnt.onclick = function() {
		var tempRefId=this.childNodes[1].childNodes[1].value;
		var arrElm=document.querySelectorAll("input[value="+tempRefId+"]");
		var rows =getParentByTagName(this,'table').tBodies[0].rows;
		var subRows=document.getElementById('fnaswrepplanTbl').tBodies[0].rows;

		for (iCel=0; iCel < rows.length; iCel++)
		{
			 removeClass(rows[iCel],'fnaPlnSelected')
			 removeClass(subRows[iCel],'fnaPlnSelected')
		}

		for(var i=0;i<arrElm.length;i++){
			addClass(getParentByTagName(arrElm[i],'tr'),'fnaPlnSelected');
			var rowNo=getParentByTagName(arrElm[i],'tr').rowIndex;
			addClass(tbodyobj.rows[rowNo-1],'fnaPlnSelected');
		}
	};


	tblResetScroll();
}

function fnaSwrepPlanModRow() {
}
function fnaSwrepPlanDelRow(tblid) {
//	deleteRowInTbl(tblid, true);
//	document.getElementById("page10addrow").value="CHANGED";
//	checkChanges(document.getElementById("page10addrow"));
	var strDelSts=false;
	var strBothTblDel=false;

	var clntGrpTbl=document.getElementById("fnaswrepplanTblss");
	var clntTblBdy=clntGrpTbl.tBodies[0];
	var clntTblLnth=clntTblBdy.rows.length;

	var fnaPlnTbl=document.getElementById("fnaswrepplanTbl");
	var fnaPlnTblBdy=fnaPlnTbl.tBodies[0];
	var fnaPlnTblLnth=fnaPlnTblBdy.rows.length;
	var RowIndex;

	for (var i=0;i<clntTblLnth;i++){
		if(hasClass(clntTblBdy.rows[i],'fnaPlnSelected')){
			strBothTblDel=true;
			break;
		}
	}


	if(strBothTblDel){

		if (clntGrpTbl.getElementsByClassName('fnaPlnSelected'))
		{
				var sltRow=clntGrpTbl.getElementsByClassName('fnaPlnSelected').length;
				var clientName=clntGrpTbl.getElementsByClassName('fnaPlnSelected')[0].childNodes[1].firstChild.value;

				if(sltRow > 1 && !confirm(FNA_ADV_REC_TBLDEL_CONFIRM.replace("~CLIENT~",clientName)))return false;

				strDelSts=true;
		}

		for (var i=fnaPlnTblLnth;i>0;i--){
			RowIndex=i-1;
			if(hasClass(fnaPlnTblBdy.rows[RowIndex],'fnaPlnSelected')){

				var mode=fnaPlnTblBdy.rows[RowIndex].firstChild.childNodes[1].value

				if(mode != UPD_MODE){
				$(fnaPlnTblBdy.rows[RowIndex]).remove();
//				removeClass(fnaPlnTblBdy.rows[RowIndex],"fnaPlnSelected");

				var clntRefId=clntTblBdy.rows[RowIndex].childNodes[1].childNodes[1].value;
				$('[value="'+clntRefId+'"]').closest("tr").attr("row-ref",clntRefId);

				var crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);
				getParentByTagName(crntFld,'tr').parentNode.removeChild(getParentByTagName(crntFld,'tr'));
//				removeClass(clntTblBdy.rows[RowIndex],"fnaPlnSelected");

				crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);
					if(crntFld){
						getParentByTagName(crntFld,'tr').childNodes[0].style.borderBottom='1px solid black'
						getParentByTagName(crntFld,'tr').childNodes[1].style.borderBottom='1px solid black'
					}

				}else{
					$(fnaPlnTblBdy.rows[RowIndex]).hide();
					removeClass(fnaPlnTblBdy.rows[RowIndex],"fnaPlnSelected");

					var clntRefId=clntTblBdy.rows[RowIndex].childNodes[1].childNodes[1].value;
					$('[value="'+clntRefId+'"]').closest("tr").attr("row-ref",clntRefId);
					fnaPlnTblBdy.rows[RowIndex].firstChild.childNodes[1].value=DEL_MODE;

					var crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);
					$(getParentByTagName(crntFld,'tr')).hide();
					removeClass(clntTblBdy.rows[RowIndex],"fnaPlnSelected");

					crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);

					if(crntFld){
						getParentByTagName(crntFld,'tr').childNodes[0].style.borderBottom='1px solid black'
						getParentByTagName(crntFld,'tr').childNodes[1].style.borderBottom='1px solid black'
					}
				}

			}
		}


	}else{
		for (var i=0;i<fnaPlnTblLnth;i++){
			if(hasClass(fnaPlnTblBdy.rows[i],'fnaPlnSelected')){
				RowIndex=i;
//				fnaPlnTblBdy.rows[i].parentNode.removeChild(fnaPlnTblBdy.rows[i]);
				strDelSts=true;
				break;
			}
		}

		if(strDelSts && confirm(FNA_PROD_REC_TBLROWDEL_CONFIRM)){

		var mode=fnaPlnTblBdy.rows[RowIndex].firstChild.childNodes[1].value

		if(mode != UPD_MODE){
		fnaPlnTblBdy.rows[RowIndex].parentNode.removeChild(fnaPlnTblBdy.rows[i]);

		var clntRefId=clntTblBdy.rows[RowIndex].childNodes[1].childNodes[1].value;
		var crntFld=$('[value="'+clntRefId+'"]:last').get(0);
		getParentByTagName(crntFld,'tr').parentNode.removeChild(getParentByTagName(crntFld,'tr'));

		crntFld=$('[value="'+clntRefId+'"]:last').get(0);
			if(crntFld){
				getParentByTagName(crntFld,'tr').childNodes[0].style.borderBottom='1px solid black'
				getParentByTagName(crntFld,'tr').childNodes[1].style.borderBottom='1px solid black'
			}

		}else{
			$(fnaPlnTblBdy.rows[RowIndex]).hide();
			removeClass(fnaPlnTblBdy.rows[RowIndex],"fnaPlnSelected");

			var clntRefId=clntTblBdy.rows[RowIndex].childNodes[1].childNodes[1].value;
			$('[value="'+clntRefId+'"]').closest("tr").attr("row-ref",clntRefId);
			fnaPlnTblBdy.rows[RowIndex].firstChild.childNodes[1].value=DEL_MODE;

			var crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);
			$(getParentByTagName(crntFld,'tr')).hide();

			crntFld=$('[row-ref="'+clntRefId+'"]:visible:last td:eq(1)').get(0);

			if(crntFld){
				getParentByTagName(crntFld,'tr').childNodes[0].style.borderBottom='1px solid black'
				getParentByTagName(crntFld,'tr').childNodes[1].style.borderBottom='1px solid black'
			}
		}


		}

	}

	if(!strDelSts){
		alert('No row Selected');
	}else{
		var PrntTr=$('tr.prntSwRepRowTbl:visible');
		for(i=0;i<PrntTr.length;i++){
			PrntTr[i].childNodes[0].childNodes[0].value=i+1;
		}
	}

}
function fnaSwrepPlanExpndRow() {
}


function editSwrepFundName(imgobj){

	var planNameObj = imgobj.parentNode.childNodes[1];
	var planNameHidden = imgobj.parentNode.childNodes[2];
	var planNameText = imgobj.parentNode.childNodes[4];
	var image2 = imgobj.parentNode.childNodes[5];
	var planOpt = imgobj.parentNode.childNodes[7];

	planNameObj.value="";
	planNameHidden.value="";
	planOpt.value="";

	planNameObj.style.display="none";
	planNameText.style.display="inline";
	imgobj.style.display="none";
	image2.style.display="";
	planOpt.value="CUSTOM";

}

function resetSwrepFundName(imgobj){

	var planNameObj = imgobj.parentNode.childNodes[1];
	var planNameHidden = imgobj.parentNode.childNodes[2];
	var image1 = imgobj.parentNode.childNodes[3];
	var planNameText = imgobj.parentNode.childNodes[4];
	var planOpt = imgobj.parentNode.childNodes[7];

	planNameObj.value="";
	planNameHidden.value="";
	planNameText.value = "";
	planOpt.value="";

	planNameObj.style.display="inline";
	planNameText.style.display="none";
	imgobj.style.display="none";
	image1.style.display="";
	planOpt.value="PRE";
}

function fnaSwrepFundAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;
	var jqRowlenobj = $("#"+tblid+" tbody tr:visible").length;

	if (!validateFnaSwrepFundtDet(true))
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text" size="3"  name="txtFldFnaSwrepFundSiNo" readonly="readonly" value="'
			+ (jqRowlenobj + 1)
			+ '" class="fpEditTblTxt" style="text-align:center" /><input type = "hidden"  name="txtFldFnaSwrepFundMode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaSwrepFundId"  />';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<select name="selFnaSwrepFundPrdtType" class="kycSelect" onchange="/*loadPrinOrFundMgr(this)*/"></select>';
	//changed by Thulasy 16.03.2018 -->
	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<select name="selFnaSwrepFundCompName" class="kycSelect" onchange="getPrdtPlanName(this)" style="width:200px;display:none" onclick="hideTooltip()" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" ></select>'+
	'<input type="text"  name="txtFldFnaSwrepFundCompName" class="fpEditTblTxt" style="display:inline;width:200px"  maxlength="60">'+
	'<select name="selFnaSwrepFundProdCode" onchange="setPlanName(this)" onclick="hideTooltip();chkCompDets(this);" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" class="kycSelect" style="width:200px;display:none"></select>'+
	'<input type="hidden" name="selFnaSwrepFundPlan"/>'+
	'<img src="images/modify.png" onclick="editSwrepFundName(this)"  style="display:none"/>'+
	'<input type="text"  name="selFnaSwrepFundPlanTxt" class="fpEditTblTxt" style="display:inline;width:185px"  maxlength="300"/>'+
	'<img src="images/history.png" onclick="resetSwrepFundName(this)"  style="display:none" />'+
	'<select name="selFnaSwrepFundCateg" class="kycSelect" style="width:80px;display:none"><option value="">--SELECT--</option><option value="B">Basic</option><option value="R">Rider</option></select>'+
	'<input type="hidden" name="selFnaSwrepFundOpt" value="CUSTOM"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundRisk" class="fpEditTblTxt"  maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<select name="selFnaSwrepFundPayment" class="kycSelect" ></select>';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSoUnit" class="fpEditTblTxt" style="width:120px" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSoPrc" class="fpEditTblTxt" style="width:120px" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSiChrg" class="fpEditTblTxt" style="width:85px" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	var cell8 = rowobj.insertCell(8);
	cell8.style.textAlign = "left";
	cell8.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSiAmt" class="fpEditTblTxt" style="width:75px" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  />';

	var cell9 = rowobj.insertCell(9);
	cell9.style.textAlign = "left";
	cell9.innerHTML = '<input type = "text"  name="txtFldFnaSwrepFundSiPrc" class="fpEditTblTxt"  maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';

	comboMaker(cell1, fundPrdtArr,"UT", false);
	// comboMaker(cell2, fundPrdtArr, "", true);
	addSelectOption(cell2.childNodes[0]);
	addSelectOption(cell2.childNodes[2]);
	comboMaker(cell4, fundPayMethArr, "", true);

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}
	document.getElementById("page10addrow").value="CHANGED";
	checkChanges(document.getElementById("page10addrow"));
	cell2.childNodes[1].focus()
}
function fnaSwrepFundModRow() {
}
function fnaSwrepFundDelRow(tblid) {
	deleteRowInTbl(tblid, true);

	document.getElementById("page10addrow").value="CHANGED";
	checkChanges(document.getElementById("page10addrow"));

}
function fnaSwrepFundExpndRow() {
}

function fnaFatcaAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;


	var fatcaFor = "CLIENT";
	if(tblid == "fnaFatcaTblSps"){
		fatcaFor ="SPOUSE";
	}

	if (!validateFnaFatcaTaxDets(true))
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<select name="txtFldFnaFatcaCntry" class="kycSelect"></select>';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaFatcaTaxNo" class="fpEditTblTxt"  maxlength="20"/><input type = "hidden"  name="txtFldFnaFatcaMode" value="'
			+ INS_MODE
			+ '" /><input type = "hidden"  name="txtFldFnaFatcaId"  />';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<select name="txtFldFnaFatcaTaxReason" class="kycSelect" onchange="enableReasonDets(this)"></select>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text"  name="txtFldFnaFatcaReasonBDets" class="fpEditTblTxt"  readonly="true" maxlength="800"/>'
		+'<input type="hidden" name="txtFldFldFatcaFor" value="'+fatcaFor+'"/>';

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

	comboMaker(cell0, contryArr, "", false);
	comboMaker(cell2, tax_reasonArr, "", true);

	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}

	$("#page4addrow").val("CHANGED");
	checkChanges(document.getElementById("page4addrow"));
	cell0.childNodes[0].focus()

}

function enableReasonDets(obj) {

	if (obj.value == "Reason B") {
		obj.parentNode.parentNode.childNodes[3].childNodes[0].readOnly = false;
	} else {
		obj.parentNode.parentNode.childNodes[3].childNodes[0].value = "";
		obj.parentNode.parentNode.childNodes[3].childNodes[0].readOnly = true;
	}
}

function fnaFatcaDelRow(tblId) {
	deleteRowInTbl(tblId, false);
	$("#page4addrow").val("CHANGED");
	checkChanges(document.getElementById("page4addrow"));
}

function jsonDataPopulate(contData, tab) {

	var i=tab;

	for ( var cont in contData) {

		if (contData.hasOwnProperty(cont)) {

			var contvalue = contData[cont];

			var elemObj = eval('fnaForm.' + cont)	|| document.getElementsByName(cont);

			if (elemObj) {

				/*if(tab == "ARCH_FNA_TABLE_DATA"){
					if(cont == "txtFldMgrId"){
						$("#selFnaSelectedMgrId").val(contvalue);
					}
				} MAR_2018*/

				if (elemObj.type == "radio" || elemObj.type == "checkbox") {

					if (elemObj.value == contvalue || contvalue == "on" ) {
						elemObj.checked = true;
					}
				} else {

					elemObj.value = contvalue;
				}
				//added by kavi 16/03/2018
				if(tab=='ARCH_FNA_TABLE_DATA'){
					if(cont == 'advdecOptions'){
                       var data=contData[cont];
						var val=JSON.stringify(data);
						$('input[name="advdecOptions"]').val("");

						if (!isEmpty(data) && !jQuery.isEmptyObject(val)) {
							$('input[name="advdecOptions"]').val(val);
							$.each(data,function(obj,val){
								
								if(obj=='NO1A' && val=='Y')$('input[name="htfrepdecno1a"]').prop('checked',true);
								if(obj=='NO1B' && val=='Y')$('input[name="htfrepdecno1b"]').prop('checked',true);
								if(obj=='NO2' && val=='Y')$('input[name="htfrepdecno2"]').prop('checked',true);
							
								if(obj=='NO3' && val=='Y')$('input[name="htfrepdecno3"]').prop('checked',true);
								if(obj=='NO4' && val=='Y')$('input[name="htfrepdecno4"]').prop('checked',true);
								if(obj=='NO5' && val=='Y')$('input[name="htfrepdecno5"]').prop('checked',true);
								
								if(obj=='GIPROD' && val=='Y')$('input[name="htfrepdecgi"]').prop('checked',true);
								if(obj=='AHPROD' && val=='Y')$('input[name="htfrepdecah"]').prop('checked',true);
								
								if(obj == "NO1ALL" && val =='Y'){
									$('input[name="htfrepdecno1all"]').prop('checked',true);
									
								}
								
						});
						}
					}

					if(cont == 'apptypesClient'){
	                       var data=contData[cont];
	                       setJsonObjToElm(cont,data);
					}

					if(cont == 'prodrecObjectives'){
	                       var data=contData[cont];
	                       setJsonObjToElm(cont,data);
					}

					if(cont == 'cdLanguages'){
	                       var data=contData[cont];
	                       setJsonObjToElm(cont,data);
	                       if( $("#chkCDLanOth").is(":checked")){
		                    	  $("input[name='cdLanguageOth']").removeClass('readOnlyText').addClass('writeModeTextbox').prop('readonly',false)
		                    	  .prop('disabled',false).attr("disabled",false);
		                   }
					}

					if(cont ==  "fwrDepntflg" && contvalue == "Y"){
						showDepntSection(document.getElementById("fwrDepntflg"));
					}
					if(cont == "fwrFinassetflg" && contvalue == "Y"){
						showFinSituSection(document.getElementById("fwrFinassetflg"));
					}

					if(cont=='callbackAdviser' || cont=='callbackClient'){
						if(contvalue=='Y')document.getElementsByName(cont)[0].checked=true;
					}

					if(cont=='intprtRelat'){
						document.getElementsByName('drcIntRel')[0].value=contvalue;
					}

					if(cont=='mgrName'){
						var sltTxt=$("#selFnaSelectedMgrId option:selected").text();

						if(!isEmpty($("#selFnaSelectedMgrId").val()) &&  sltTxt != contvalue){
							$("#txtFldSupVName").val(contvalue);
							$("input[name='mgrName']").val(contvalue)
						}
					}

					if(cont=='ccRepexistinvestflg'){
						$("[name=ccRepexistinvestflgYN]").each(function(){
							if(this.value==contvalue)this.checked=true;
						})
					}
					
					if(cont == "mgrEmailSentFlg"){
						if(contvalue == "Y"){
							$("#hTxtFldSentToMgr").val("Sent");	
						}else{
							$("#hTxtFldSentToMgr").val("Not Sent");
						}
						
					}
					
					if(cont == "mgrApproveStatus"){
						
						
						
						if(contvalue == "APPROVE"){
							$("#hTxtFldMgrAppSts").val("Approved");
							$("#btnMgrApprResend").prop("disabled",true);
						}else if(contvalue == "REJECT"){
							$("#hTxtFldMgrAppSts").val("Rejected");
							$("#btnMgrApprResend").prop("disabled",false);
						}else{
							if(isEmpty(contvalue)){
								$("#hTxtFldMgrAppSts").val("");
//								var ntucexist = chkAnyNTUCExist();
//								alert(ntucexist)
//								var sentsts = $("#hTxtFldSentToMgr").val();
//								if(sentsts == "Not Sent" && !ntucexist){
//									$("#btnMgrApprResend").prop("disabled",false);
//									$("#btnMgrApprResend").prop("value","Send Now!");
//								}else{
									$("#btnMgrApprResend").prop("disabled",true);	
//								}
								
									
							}
							
						}
					}
					
					

				}


				if(tab == "ARCH_FNASSDET_TABLE_DATA"){


					if(cont == 'dfSelfFundsrc'){
	                       var data=contData[cont];
	                       setJsonObjToElm(cont,data);
	                      if( $("#chkCDClntSrcIncmOthrs").is(":checked")){
	                    	  $("input[name='dfSelfFundsrcDets']").removeClass('readOnlyText').addClass('writeModeTextbox').prop('readonly',false)
	                    	  .prop('disabled',false);
	                      }
					}

					if(cont == 'dfSpsFundsrc'){
	                       var data=contData[cont];
	                       setJsonObjToElm(cont,data);
	                       if( $("#chkCDSpsSrcIncmOthrs").is(":checked")){
		                    	  $("input[name='dfSpsFundsrcDets']").removeClass('readOnlyText').addClass('writeModeTextbox').prop('readonly',false)
		                    	  .prop('disabled',false);
		                   }
					}


					if ((cont == "dfSelfNationality" || cont == "dfSpsNationality")) {

						for(var cnt=0;cnt<3;cnt++){
							if(document.getElementsByName(cont))
								document.getElementsByName(cont)[cnt].checked=false;
						}

						switch (contvalue) {
							case "SG":document.getElementsByName(cont)[0].checked = true;;break;
							case "SGPR":document.getElementsByName(cont)[1].checked = true;break;
							case "OTH":document.getElementsByName(cont)[2].checked = true;break;
						}

						chkForContry(cont,contvalue);
					}//MAR_2018

					if(cont == "dfSelfEdulevel" || cont == "dfSpsEdulevel"){

						if(document.getElementsByName(cont+"Rad")){
							for(var cnt1=0;cnt1<document.getElementsByName(cont+"Rad").length;cnt1++){
								if(document.getElementsByName(cont+"Rad")[cnt1].value ==contvalue){
									document.getElementsByName(cont+"Rad")[cnt1].checked=true;
								}
							}
						}
						document.getElementsByName(cont)[0].value ==contvalue;
						if(cont == "dfSelfEdulevel"){
							$("input[name='dfSelfEdulevel']").val(contvalue);
						}else{
							$("input[name='dfSpsEdulevel']").val(contvalue);
						}
					}


					if(cont ==  "dfSelfReplExistpol" || cont == "dfSpsReplExistpol"){
						showWithdrawPol(null);
					}

					if(cont=="txtFldSpouseName"){
						$('input[name="txtFldSpouseName"]').val(contvalue);
					}

					if(cont=="dfSelfBusinatr" || cont=="dfSpsBusinatr"){
						if(contvalue.toUpperCase()=='OTHERS'){
							document.getElementById(cont).onchange();
						}
					}

					if(cont == "dfSelfMailaddrflg" && contvalue=='true'){
						$("#dfSelfMailaddr").removeClass("readOnlyText").addClass("writeModeTextbox").prop("readonly",false);
					}

					if(cont == 'dfSpsMailaddrflg' && contvalue=='true'){
						$("#dfSpsMailaddr").removeClass("readOnlyText").addClass("writeModeTextbox").prop("readonly",false);
					}

//						alert(cont + "="+contvalue)

					/*if(cont == "dfSelfNatyDets"){
						var selfNat = document.getElementsByName("dfSelfNationality").value;
						for(var cnt1=0;cnt1<3;cnt1++){
							if(document.getElementsByName(cont1))
								if(document.getElementsByName(cont)[cnt1].checked){
									selfNat=document.getElementsByName(cont)[cnt1].value;
								}
						}
						switch (selfNat) {
						case "SG":

							chkForContry(cont, contvalue);
							break;
						case "SGPR":

							chkForContry(cont, contvalue);
							break;
						case "OTH":

							chkForContry(cont, contvalue);
							break;
						}
					}mar_2018*/


//					}

					/*

					if(cont == "dfSelfUspersonflg" && contvalue == "Y"){
						document.getElementById(cont).checked=true;
						enabOrDisbTxtFld(document.getElementById(cont), 'dfSelfUstaxno', 'tdUSClntLblTxt');
					}

					if(cont == "dfSpsUspersonflg" && contvalue == "Y"){
						document.getElementById(cont).checked=true;
						enabOrDisbTxtFld(document.getElementById(cont), 'dfSpsUstaxno', 'tdUSSpsLblTxt');
					}

					if (cont == 'dfSelfUstaxno') {
						(!isEmpty(contvalue) ? clntUSTaxRefNum = contvalue : clntUSTaxRefNum = '');

					}// end of if

					if (cont == 'dfSpsUstaxno') {
						(!isEmpty(contvalue) ? spsUSTaxRefNum = contvalue : spsUSTaxRefNum = '');
					}// end of if
					mar_2018
					*/


					if(cont == 'dfSelfTaxres'){
	                       var data=contData[cont];
	                       setJsonObjToElm(cont,data);
	                      if( $("#chkCDClntOth").is(":checked")){
	                    	  $("textarea[name='dfSelfTaxresOth']").removeClass('readOnlyText').addClass('writeModeTextbox').prop('readonly',false)
	                    	  .prop('disabled',false);
	                      }
					}

					if(cont == 'dfSpsTaxres'){
	                       var data=contData[cont];
	                       setJsonObjToElm(cont,data);
	                       if( $("#chkCDSpsOth").is(":checked")){
		                    	  $("textarea[name='dfSpsfTaxresOth']").removeClass('readOnlyText').addClass('writeModeTextbox').prop('readonly',false)
		                    	  .prop('disabled',false);
		                   }
					}
					
					if(cont == 'dfSelfHeight'){						
						$("#dfSelfHeight").val(removeDecimalPoint(contData[cont]));
					}
					
					if(cont == 'dfSpsHeight'){						
						$("#dfSpsHeight").val(removeDecimalPoint(contData[cont]));
					}
				}

				popuPersPrioComBox(elemObj, contvalue, cont);// added by johnson on 29042015



			}
			if (cont == 'swrepConfDets')
				(!isEmpty(contvalue)) ? advRecommOthrs = contvalue	: advRecommOthrs = '';// added by johnson on 22052015

			if (cont == 'txtFldCDMailAddOth_032018')
				(!isEmpty(contvalue)) ? clntMailAddrOthrsSpcy = contvalue: clntMailAddrOthrsSpcy = '';// added by johnson on 22052015

			if (elemObj && elemObj.type == "checkbox" && contvalue == "Y") {
				elemObj.checked = true;
				fnaAssgnFlg(elemObj, 'chk');
				// elemeObj.value=cvalue;
			}

			if (cont == "cdackAgree" || cont == "ccRepexistinvestflg"
					|| cont == "swrepProceedflg"
					|| cont == "swrepDisadvgflg" || cont == "swrepAdvbyflg"
					|| cont == "swrepConflg" || cont == "cdIntrprtflg"
					|| cont == "cdBenfownflg" || cont == "cdTppflg"
					|| cont == "cdPepflg" || cont == "htfcdpaymentmode"
					|| cont == "radCDClntMailngAddr_032018" || cont == "dfSelfMailaddrflg"
					|| cont == "crRiskclass" || cont == "educatFlg" || cont == "crRiskclassILP" 
					|| cont == "educatDet" || cont == "financeCrseFlg"
					|| cont == "investExpFlg" || cont == "workExpFlg"
					|| cont == "outcomeCkaFlg" || cont == "suprevMgrFlg"
					|| cont == "txtFldDfSelfAgenext"
					|| cont == "txtFldDfSpsAgenext_032018"
					|| cont == "txtFldDfSelfEngProf"
					|| cont == "txtFldDfSpsEngProf" || cont == "dfSelfEngSpoken" || cont == "dfSelfEngWritten"
					|| cont == "dfSpsEngSpoken" || cont == "dfSpsEngWritten") {

				for (var elem = 0; elem < document.getElementsByName(cont).length; elem++) {

					if (document.getElementsByName(cont)[elem].value == contvalue) {
						document.getElementsByName(cont)[elem].checked = true;
						break;
					}
				}
				//
				if (cont == "cdIntrprtflg") {
					getrdIntprtFlg(contvalue);
				}

				if(cont == "cdBenfownflg" && contvalue == "Y"){
//					fnaForm.cdBenfownflg;
					chkTPPBenifPEP(fnaForm.cdBenfownflgY,'BENF');
				}
				if(cont == "cdTppflg" && contvalue == "Y"){
//					fnaForm.cdTppflg;
					chkTPPBenifPEP(fnaForm.cdTppflgY,'TPP');
				}
				if(cont == "cdPepflg" && contvalue == "Y"){
//					fnaForm.cdPepflg;
					chkTPPBenifPEP(fnaForm.cdPepflgY,'PEP');
				}
				//
			}

			if (tab == "ARCH_FNASSDET_TABLE_DATA") {
				if (cont == "txtFldFnaClntName") {
					for (var ele = 0; ele < document.getElementsByName("txtFldClientName").length; ele++) {
						document.getElementsByName("txtFldClientName")[ele].value = contvalue;
					}
				}
//				copyRegAddrToMailAddr(fnaForm.dfSelfMailaddrflg);
			}

			if (tab == "FNA_CKADET_DATA") {
				if (cont == "passDonotwishFlg" || cont == "passWishFlg") {
					if (contvalue == "Y") {
						document.getElementById("txtFldClntAcknDate").value = currDate;
					}
				}
			}

			/*
			 * if(elemObj && elemObj.type == "radio"){ elemObj.value=cvalue;
			 * elemObj.checked=true; }
			 */


//			var inputtype = elemObj.type;
//			if(inputtype ==  "text" || inputtype == "hidden" || inputtype == "select" || inputtype == "textarea"){
//				if(hasClass(elemObj,"checkdb")){
////					changedarrGlobal.push(JSON.stringify("{"+elemObj.name+":"+elemObj.value+"}"));
//					changedarrGlobal.push(new Array(elemObj.name,elemObj.value));
//				}
//			}else if(inputtype == "radio" || inputtype == "checkbox"){
//				if($(this).prop("checked")){
//					if(hasClass(elemObj,"checkdb")){
////						changedarrGlobal.push(JSON.stringify("{"+elemObj.name+":"+elemObj.value+"}"));
//						changedarrGlobal.push(new Array(elemObj.name,elemObj.value));
//					}
//				}
//			}

		}
	}
}

function copyValuesPerstoClntDecl() {
	// Page3 = page14
//alert("copyValuesPerstoClntDecl")
	/*
	 * txtFldFnaClntName = dfSelfName selFnaClntSex = dfSelfGender
	 * txtFldFnaClntDOB = dfSelfDob txtFldFnaClntNric = dfSelfNric
	 * selFnaClntMartSts = dfSelfMartsts txtFldFnaClntHome =
	 * dfSelfHome//home txtFldFnaClntMobile =
	 * txtFldCDClntHndPhne//mobile txtFldFnaClntOffice = dfSelfOffice//office
	 * txtFldFnaClntEmail = dfSelfPersemail//email-p txtFldFnaClntOccp =
	 * dfSelfOccpn txtFldFnaClntHomeAddr = dfSelfHomeaddr
	 */

	var sourceElemArr = [ "txtFldFnaClntName", "selFnaClntSex",
			"txtFldFnaClntDOB", "txtFldFnaClntNric", "selFnaClntMartSts",
			"txtFldFnaClntHome", "txtFldFnaClntMobile", "txtFldFnaClntOffice",
			"txtFldFnaClntEmail", "txtFldFnaClntOccp", "txtFldFnaClntHomeAddr",
			"txtFldFnaSpsName", "selFnaSpsSex", "txtFldFnaSpsDob",
			"txtFldFnaSpsNric", "selFnaSpsMartSts"// ,"dfSpsHome"
			, "txtFldFnaSpsHP", "txtFldFnaSpsEmail", "txtFldFnaSpsOccpn",
			"txtFldFnaSpsOffice" ];// ,,"dfSpsHomeaddr"

	var destElemArr = [ "dfSelfName", "dfSelfGender",
			"dfSelfDob", "dfSelfNric", "dfSelfMartsts",
			"dfSelfHome", "dfSelfMobile", "dfSelfOffice",
			"dfSelfPersemail", "dfSelfOccpn", "dfSelfHomeaddr",
			"dfSpsName", "dfSpsGender", "dfSpsDob",
			"dfSpsNric", "dfSpsMartsts",// "txtFldCDSpsCntDets",
			"dfSpsHp", "dfSpsPersemail", "dfSpsOccpn",
			"dfSpsOffice" ];// ,

	for (var elem = 0; elem < sourceElemArr.length; elem++) {

		var srcElem = eval('fnaForm.' + sourceElemArr[elem]);
		var destElem = eval('fnaForm.' + destElemArr[elem]);

		destElem.value = srcElem.value;

		if (destElem.type == "text") {
			destElem.readOnly = true;
			destElem.className = "readOnlyText";
		} else if (destElem.type == "select-one") {
			destElem.disabled = true;
		}

	}

	// 2015_05_25
	var multiElem_src = [ "txtFldFnaClntName", "dfSpsName" ];
	var multiElem_dest = [ "txtFldClientName", "txtFldSpouseName" ];

	for (var el = 0; el < multiElem_dest.length; el++) {

		for (var ele = 0; ele < document.getElementsByName(multiElem_dest[el]).length; ele++) {
			document.getElementsByName(multiElem_dest[el])[ele].value = document
					.getElementById(multiElem_src[el]).value;
		}
	}

	// fnaForm.dfSelfName.value=fnaForm.txtFldFnaClntName.value

}

function datePickerCall(inputfld) {

	$("input[name='" + inputfld + "']")
			.datepicker(
					{
						inline : true,
						changeMonth : true,
						changeYear : true,
						showOn : "button",
						buttonImage : "images/cal.png",
						buttonImageOnly : true,
						buttonText : "Select date",
						onselect : function() {
							if (inputfld == "txtFldFnaClntDOB")
								fnaForm.dfSelfAge.value = calcAge(fnaForm.txtFldFnaClntDOB.value);
							if (inputfld == "txtFldFnaSpsDob")
								fnaForm.dfSpsAge.value = calcAge(fnaForm.txtFldFnaSpsDob.value);

							/*chkNextAgeBirthday(); commented by kavi 17/03/2018*/

						}
					});
}

function chkforOthers(radbtn, txtfldid, value) {

	if (radbtn.value == value) {

		$("#" + txtfldid).focus();
		$("#" + txtfldid).attr('readonly', false);

		$("#" + txtfldid).attr('class', '');
		$("#" + txtfldid).removeClass('hidden');
		$("#" + txtfldid).addClass("writeModeTextbox");

		// added by johnson on 22052015
		if (txtfldid == 'txtFldCDMailAddOth_032018' && !isEmpty(clntMailAddrOthrsSpcy))
			$('#' + txtfldid).val(clntMailAddrOthrsSpcy);

		if (txtfldid == 'swrepConfDets' && !isEmpty(advRecommOthrs))
			$('#' + txtfldid).val(advRecommOthrs);

	} else {
		$("#" + txtfldid).val("");
		$("#" + txtfldid).attr('readonly', true);

		$("#" + txtfldid).attr('class', '');

		$("#" + txtfldid).addClass("readOnlyText");
	}
}

function calcAge(dob) {

	var age = 0;
	if (!isEmpty(dob)) {

		var birthday = dob.split("/")[2] + "-" + dob.split("/")[1] + "-"+ dob.split("/")[0];
		var now = new Date();
		var past = new Date(birthday);

		age = now.getFullYear() - past.getFullYear();


	}
	return age;
}

function loadExistCustDet(strDataCust) {
//	 alert("loadexisting...")

	var custDets = strDataCust;

	for ( var cval in custDets) {

		var ctabdets = custDets[cval];

		for ( var ctab in ctabdets) {

			if (ctabdets.hasOwnProperty(ctab)) {
				var ckey = ctab;
				var cvalue = ctabdets[ctab];
				// / alert(ckey + "="+cvalue);

				var elemObj = eval('fnaForm.' + ckey);
				if (elemObj)
					elemObj.value = cvalue;

				if (elemObj && elemObj.type == "checkbox" && cvalue == "Y") {
					// alert("cvalue ----->"+cvalue)
					elemObj.checked = true;
					fnaAssgnFlg(elemObj, 'chk');
					// elemeObj.value=cvalue;
				}

				if (elemObj && elemObj.type == "radio") {
					elemObj.value = cvalue;
					elemObj.checked = true;
				}

				if (ckey == "dfSelfNationality" || ckey == "dfSpsNationality") {
//					alert(ckey +"="+ cvalue)

					for(var cnt=0;cnt<3;cnt++){
							if(document.getElementsByName(ckey))
								document.getElementsByName(ckey)[cnt].checked=false;
						}

						switch (cvalue) {
							case "SG":document.getElementsByName(ckey)[0].checked = true;;break;
							case "SGPR":document.getElementsByName(ckey)[1].checked = true;break;
							case "OTH":document.getElementsByName(ckey)[2].checked = true;break;
						}


					chkForContry(ckey, cvalue);
				}

//added by kavi on 23/03/2018

				if(ckey=='dfSelfBusinatr' && cvalue.toUpperCase()=='OTHERS'){
					document.getElementById('dfSelfBusinatr').onchange();
				}

				for (var elem = 0; elem < document.getElementsByName(ckey).length; elem++) {
					document.getElementsByName(ckey)[elem].value = cvalue;
				}
				
				if(ckey == 'dfSelfHeight'){
					$("#dfSelfHeight").val(removeDecimalPoint(cvalue));
				}
				
				if(ckey == 'dfSpsHeight'){
					$("#dfSpsHeight").val(removeDecimalPoint(cvalue));
				}

			}
		}
	}
}

function chkSelectArch() {

	var isAnySelected = false;
	for (var elem = 0; elem < document.getElementsByName("version").length; elem++) {
		if (document.getElementsByName("version")[elem].checked) {
			isAnySelected = true;
			break;
		}
	}
	if (!isAnySelected) {
		alert("Please select any archive to load!");
		$("#tabs").tabs("load", 0);
	}

}

function page3fixedHeader() {

	if (!page3Flag) {

		if (document.getElementById("page3")) {

			$('#fnaInvGoalTbl').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 500, 250, 150 ],
				height : 200,
				ieHeight : 200, // johnson on 15042015
				width : 900,
				zebra : false,
				sortable : false,
				sortType : [ 'string', 'string', 'string' ],
				dateFormat : 'dd/mm/yyyy',
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'fnaInvGoalTbl'
			});

			document.getElementById("fnaInvGoalTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaInvGoalTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaInvGoalTblmainwrapper").style.height = "228px";
			document.getElementById("fnaInvGoalTbltampon").style.height = "235px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				var Tbl = document.getElementById("fnaInvGoalTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaInvGoalTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnaInvGoalTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

//			fixedHeader('fnaDepnTbl', '1', '100%', '0px', '100%', '15.8em','2', '0px', '98.4%', '4.8em', '98.4%', true, true, 4, 2);

			page3Flag = true;
		}

	}
}

function page4fixedHeader() {

	if (!page4Flag) {

		if (document.getElementById("page4")) {

			/*$('#fnaFatcaTbl').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 200, 200, 200, 200 ],
				height : 199,
				ieHeight : 200, // johnson on 15042015
				width : 820,
				zebra : false,
				sortable : false,
				sortType : [ 'string', 'string', 'string', 'string' ],
				dateFormat : 'dd/mm/yyyy',
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'fnaFatcaTbl'
			});

			document.getElementById("fnaFatcaTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaFatcaTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaFatcaTblmainwrapper").style.height = "228px";
			document.getElementById("fnaFatcaTbltampon").style.height = "235px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				var Tbl = document.getElementById("fnaFatcaTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaFatcaTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document.getElementById("fnaFatcaTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}
//			MAR_2018
			*/


			fixedHeader('fnaFatcaTbl','1','100%','0px','100%','18em','2','0px','100%','3.1em','100%',false,false,0,0);
			fixedHeader('fnaFatcaTblSps','1','100%','0px','100%','18em','2','0px','100%','3.1em','100%',false,false,0,0);

			page4Flag = true;
		}

	}
}

function page5fixedHeader() {

	if (!page5Flag) {

		if (document.getElementById("page5")) {

			$('#fnaFlowDetTbl').fixheadertable(
					{
						// caption : 'Alterations',
						colratio : [ 150, 150, 150, 150, 500 ],
						height : 200,
						ieHeight : 200, // johnson on 15042015
						width : '100%',
						zebra : false,
						sortable : false,
						sortType : [ 'string', 'string', 'string', 'string',
								'string' ],
						dateFormat : 'dd/mm/yyyy',
						pager : false,
						rowsPerPage : 10,
						resizeCol : true,
						tblId : 'fnaFlowDetTbl'
					});

			// added by johnson on 28042015 regarding horizontal scroll bar
			// issue
			document.getElementById("fnaFlowDetTblmainwrapper").style.height = "227px";
			document.getElementById("fnaFlowDetTbltampon").style.height = '235px';// added
			// by
			// johnson
			// on
			// 29042015
			// fixedHeader('fnaFlowDetTbl','1','100%','0px','100%','15.8em','2','0px','98.6%','5em','98.6%',false,false,4,2);
			fixedHeader('fnadepntfinanceTbl', '1', '100%', '0px', '100%',
					'15.8em', '2', '0px', '98.4%', '5em', '98.4%', true, true,
					4, 2);

			document.getElementById("fnadepntfinanceTbltableContainer").style.width = "897px";

		}
	}
}

function page6fixedHeader() {

	if (!page6Flag) {

		if (document.getElementById("page6")) {

			$('#fnahlthinsTbl').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 150, 150, 150, 150, 150, 150, 150 ],
				height : 182,
				ieHeight : 172,// johnson on 15042015
				width : 900,
				zebra : false,
				sortable : false,
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'fnahlthinsTbl'
			});

			document.getElementById("fnahlthinsTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnahlthinsTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnahlthinsTblmainwrapper").style.height = "228px";
			document.getElementById("fnahlthinsTbltampon").style.height = "235px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				var Tbl = document.getElementById("fnahlthinsTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnahlthinsTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnahlthinsTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

			page6Flag = true;
		}

	}
}

function page8fixedHeader() {
	if (!page8Flag) {

		if (document.getElementById("page8")) {

			$('#fnaPropOwnTbl').fixheadertable(
					{
						// caption : 'Alterations',
						colratio : [ 150, 150, 150, 150, 150, 150, 150, 150,
								150, 150 ],
						height : 200,
						ieHeight : 200,
						width : 900,
						zebra : false,
						sortable : false,
						pager : false,
						rowsPerPage : 10,
						resizeCol : true,
						tblId : 'fnaPropOwnTbl'
					});

			document.getElementById("fnaPropOwnTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaPropOwnTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaPropOwnTblmainwrapper").style.height = "228px";
			document.getElementById("fnaPropOwnTbltampon").style.height = "235px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				var Tbl = document.getElementById("fnaPropOwnTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaPropOwnTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnaPropOwnTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

			$('#fnaVehiOwnTbl').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 250, 150, 150, 150, 350 ],
				height : 200,
				ieHeight : 200,
				width : 900,
				zebra : false,
				sortable : false,
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'fnaVehiOwnTbl'
			});

			document.getElementById("fnaVehiOwnTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaVehiOwnTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaVehiOwnTblmainwrapper").style.height = "228px";
			document.getElementById("fnaVehiOwnTbltampon").style.height = "235px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				var Tbl = document.getElementById("fnaVehiOwnTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaVehiOwnTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnaVehiOwnTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

			page8Flag = true;
		}

	}
}

function page10fixedHeader() {

	if (!page10Flag) {

		if (document.getElementById("page10")) {

			$('#fnaLIPlanTbl').fixheadertable(
					{
						// caption : 'Alterations',
						colratio : [ 150, 150, 150, 150, 150, 150, 150, 150,
								150, 150, 150, 150, 150, 150, 150, 150, 150,
								150, 150, 150, 150, 150, 150, 150, 150, 150 ],
						height : 220,
						ieHeight : 200,// johnson on 15042015
						width : 880,
						zebra : false,
						sortable : false,
						pager : false,
						rowsPerPage : 10,
						resizeCol : true,
						tblId : 'fnaLIPlanTbl'
					});

			document.getElementById("fnaLIPlanTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaLIPlanTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaLIPlanTblmainwrapper").style.height = "248px";
			document.getElementById("fnaLIPlanTbltampon").style.height = "255px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				document.getElementById("fnaLIPlanTblheaderTbl").style.marginRight = "15px";
				var Tbl = document.getElementById("fnaLIPlanTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaLIPlanTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnaLIPlanTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

			page10Flag = true;
		}
	}
}

function page11fixedHeader() {
	if (!page11Flag) {

		if (document.getElementById("page11")) {

			$('#fnaInvsetPlanTbl').fixheadertable(
					{
						// caption : 'Alterations',
						colratio : [ 150, 150, 150, 150, 150, 150, 150, 150,
								150, 150, 150, 160, 150, 150, 150, 150, 150,
								150, 150, 150 ],
						height : 204,
						ieHeight : 204,// johnson on 15042015
						width : 890,
						zebra : false,
						sortable : false,
						pager : false,
						rowsPerPage : 10,
						resizeCol : true,
						tblId : 'fnaInvsetPlanTbl'
					});

			document.getElementById("fnaInvsetPlanTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaInvsetPlanTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaInvsetPlanTblmainwrapper").style.height = "248px";
			document.getElementById("fnaInvsetPlanTbltampon").style.height = "255px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				document.getElementById("fnaInvsetPlanTblheaderTbl").style.marginRight = "15px";
				var Tbl = document.getElementById("fnaInvsetPlanTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaInvsetPlanTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnaInvsetPlanTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

			page11Flag = true;
		}
	}
}
function page16fixedHeader() {
	if (!page16Flag) {

		if (document.getElementById("page16")) {

			$('#fnaadvrecTbl').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 55, 150, 500, 150, 150, 150, 150, 150 ],
				height : 220,
				ieHeight : 200,// johnson on 15042015
				width : 900,
				zebra : false,
				sortable : false,
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'fnaadvrecTbl'
			});

			document.getElementById("fnaadvrecTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaadvrecTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaadvrecTblmainwrapper").style.height = "248px";
			document.getElementById("fnaadvrecTbltampon").style.height = "255px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				document.getElementById("fnaadvrecTblheaderTbl").style.marginRight = "15px";
				var Tbl = document.getElementById("fnaadvrecTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaadvrecTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnaadvrecTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

			page16Flag = true;
		}
	}
}
function page17fixedHeader() {
	if (!page17Flag) {

		if (document.getElementById("page17")) {

			/*
			 $('#fnaartuplanTbl').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 50, 120, 450, 150, 150, 150, 150, 150 ],
				height : 220,
				ieHeight : 200,// johsnon on 15042015
				width : 1430,
				zebra : false,
				sortable : false,
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'fnaartuplanTbl'
			});

			document.getElementById("fnaartuplanTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaartuplanTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaartuplanTblmainwrapper").style.height = "248px";
			document.getElementById("fnaartuplanTbltampon").style.height = "255px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				document.getElementById("fnaartuplanTblheaderTbl").style.marginRight = "15px";
				var Tbl = document.getElementById("fnaartuplanTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document.getElementById("fnaartuplanTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document.getElementById("fnaartuplanTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

			*/

			// fixedHeader('fnaartufundTbl','1','100%','0px','100%','15.8em','2','0px','98.6%','8em','98.6%',true,true,4,2);
			fixedHeader('fnaartuplanTbl', '1', '100%', '0px', '100%', '25.2em', '2', '0px', '100.8%', '3.1em', '100.8%', false,false, 0, 0);
			fixedHeader('fnaartufundTbl', '1', '100%', '0px', '100%', '25.2em', '2', '0px', '98.4%', '5.8em', '98.4%', true, true, 4, 2);

			fixedHeader('fnaartuplanTblss', '1', '100%', '0px', '100%', '25.2em', '2', '0px', '95%', '3.1em', '95%', false,false, 0, 0);
			document.getElementById("fnaartuplanTblfixedHead").style.backgroundColor="white";
			/*
			 *
			$('#tblFnaExistPoldet').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 150, 700 ],
				height : 110,
				ieHeight : 90,
				width : 900,
				zebra : false,
				sortable : false,
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'tblFnaExistPoldet'
			});

			document.getElementById("tblFnaExistPoldetheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("tblFnaExistPoldetheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("tblFnaExistPoldetmainwrapper").style.height = "120px";
			document.getElementById("tblFnaExistPoldettampon").style.height = "125px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				document.getElementById("tblFnaExistPoldetheaderTbl").style.marginRight = "15px";
				var Tbl = document.getElementById("tblFnaExistPoldetheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("tblFnaExistPoldetbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("tblFnaExistPoldetheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}
			*/

			// fixedHeader('fnaartufundTbl','1','100%','0px','100%','15.8em','2','0px','98.6%','8em','98.6%',true,true,4,2);
//			fixedHeader('fnaartufundTbl', '1', '100%', '0px', '100%', '15.8em', '2', '0px', '98.4%', '8em', '98.4%', true, true, 4, 2);


			page17Flag = true;
		}
	}
}
function page19fixedHeader() {
	if (!page19Flag) {

		if (document.getElementById("page17")) {
/*
			$('#fnaswrepplanTbl').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 55, 150, 500, 150, 150, 150, 150, 150, 150 ],
				height : 220,
				ieHeight : 220, // johnson on 15042015
				width : 915,
				zebra : false,
				sortable : false,
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'fnaswrepplanTbl'
			});

			document.getElementById("fnaswrepplanTblheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaswrepplanTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaswrepplanTblmainwrapper").style.height = "248px";
			document.getElementById("fnaswrepplanTbltampon").style.height = "255px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				document.getElementById("fnaswrepplanTblheaderTbl").style.marginRight = "15px";
				var Tbl = document.getElementById("fnaswrepplanTblheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaswrepplanTblbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnaswrepplanTblheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}*/

			//changed by Thulasy 16.03.2018 -->
			fixedHeader('fnaswrepplanTbl', '1', '100%', '0px', '100%', '15.8em', '2', '0px', '100%', '3.1em', '100%', false,false, 0, 0);
			fixedHeader('fnaswrepplanTblss', '1', '100%', '0px', '100%', '15.8em', '2', '0px', '95%', '3.1em', '95%', false,false, 0, 0);
			document.getElementById("fnaswrepplanTblfixedHead").style.backgroundColor="white";

			fixedHeader('fnaSwrepFundTbl', '1', '100%', '0px', '100%',	'15.8em', '2', '0px', '100%', '5.7em', '100%', true, true,	4, 2);
			page19Flag = true;
		}
	}
}

function loadjscssfile(filename, filetype) {
	if (filetype == "js") { // if filename is a external JavaScript file
		var fileref = document.createElement('script');
		fileref.setAttribute("type", "text/javascript");
		fileref.setAttribute("src", filename);
	} else if (filetype == "css") { // if filename is an external CSS file
		var fileref = document.createElement("link");
		fileref.setAttribute("rel", "stylesheet");
		fileref.setAttribute("type", "text/css");
		fileref.setAttribute("href", filename);
	}
	if (typeof fileref != "undefined")
		document.getElementsByTagName("head")[0].appendChild(fileref);
}

function loadJsCssFiles() {
	// loadjscssfile(YUIPATH+"/js-lib/yahoo-dom-event/yahoo-dom-event.js","js");
	// loadjscssfile(YUIPATH+"/js-lib/element/element-min.js","js");
	// loadjscssfile(YUIPATH+"/js-lib/button/button-min.js","js");
	//
	// loadjscssfile(YUIPATH+"/js-lib/tabview/tabview-min.js","js");
	// loadjscssfile(YUIPATH+"/js-lib/container/container-min.js","js");
	// loadjscssfile(YUIPATH+"/js-lib/container/container_core-min.js","js");
	// loadjscssfile(YUIPATH+"/js-lib/container/container_core.js","js");
	//
	// loadjscssfile(YUIPATH+"/js-lib/button/assets/skins/sam/button.css","css");
	// loadjscssfile(YUIPATH+"/js-lib/container/assets/skins/sam/container.css","css");
	// loadjscssfile(YUIPATH+"/js-lib/fonts/fonts-min.css","css");
	// loadjscssfile(YUIPATH+"/js-lib/tabview/assets/skins/sam/tabview.css","css");

	loadjscssfile("scripts/toolbar.js", "js");
	// document.getElementById("footer").innerHTML =
	// (decodeURIComponent(footer));

}

function copyRegAddrToMailAddr(rdobj) {

	var clntregaddr = fnaForm.dfSelfHomeaddr;
	var spsregaddr = fnaForm.dfSpsHomeaddr;

	var clntmailaddr = fnaForm.dfSelfMailaddr;
	var spsmailaddr = fnaForm.dfSpsMailaddr;

	var spsName = fnaForm.dfSpsName.value;


	var chkd = rdobj.checked;
	var addr = rdobj.value;
//	alert(addr)
//	 Page -14 related
	if(chkd){

		clntmailaddr.value = clntregaddr.value;
//		clntmailaddr.readOnly = true;//dec_2019
//		clntmailaddr.className = "readOnlyText";//dec_2019
		clntmailaddr.readOnly = false;//dec_2019
		clntmailaddr.className = "writeModeTextbox";//dec_2019

		if(!isEmpty(spsName)){
			spsmailaddr.value = spsregaddr.value;
//			spsmailaddr.readOnly = true;//dec_2019
//			spsmailaddr.className = "readOnlyText";//dec_2019
			spsmailaddr.readOnly = false;//dec_2019
			spsmailaddr.className = "writeModeTextbox";//dec_2019
			// enabOrDisabResnMailAddr('radCDClntMailngAddr_032018','DISABLE');
			}else{
				spsmailaddr.value = "";
				spsmailaddr.readOnly = false;
				spsmailaddr.className = "writeModeTextbox";
			}

		$("#radCDClntMailngAddr_OFFADDR").attr('checked',false);
		$("input[name='radCDClntMailngAddr_032018']").attr('disabled',true);
		$("input[name='radCDClntMailngAddr_032018']").attr('checked',false);
		$("#radCDClntMailngAddr_OFFADDR").attr('disabled',true);

		$("#dfSelfMailaddrflg").prop("disabled",true).prop("checked",false);
		$("#dfSpsMailaddrflg").prop("disabled",true).prop("checked",false);

		/*switch (addr) {

		case "Clnt": {
			clntmailaddr.value = clntregaddr.value;
			clntmailaddr.readOnly = true;
			clntmailaddr.className = "readOnlyText";

			if(!isEmpty(spsName)){
			spsmailaddr.value = clntregaddr.value;
			spsmailaddr.readOnly = true;
			spsmailaddr.className = "readOnlyText";
			// enabOrDisabResnMailAddr('radCDClntMailngAddr_032018','DISABLE');
			}else{
				spsmailaddr.value = "";
				spsmailaddr.readOnly = false;
				spsmailaddr.className = "writeModeTextbox";
			}
			$("#radCDClntMailngAddr_OFFADDR").attr('checked',false);
			$("input[name='radCDClntMailngAddr_032018']").attr('disabled',true);
			$("input[name='radCDClntMailngAddr_032018']").attr('checked',false);
			$("#radCDClntMailngAddr_OFFADDR").attr('disabled',true);
			break;
		}
		case "Sps": {

			if(isEmpty(spsName)){
				alert("Spouse name is empty!.");
			}

			clntmailaddr.value = spsregaddr.value;
			clntmailaddr.readOnly = true;
			clntmailaddr.className = "readOnlyText";

			if(!isEmpty(spsName)){
			spsmailaddr.value = spsregaddr.value;
			spsmailaddr.readOnly = true;
			spsmailaddr.className = "readOnlyText";
			// enabOrDisabResnMailAddr('radCDClntMailngAddr_032018','DISABLE');
			}else{
				spsmailaddr.value = "";
				spsmailaddr.readOnly = false;
				spsmailaddr.className = "writeModeTextbox";
			}
			$("#radCDClntMailngAddr_OFFADDR").attr('checked',false);
			$("input[name='radCDClntMailngAddr_032018']").attr('disabled',true);
			$("input[name='radCDClntMailngAddr_032018']").attr('checked',false);
//			$("#radCDClntMailngAddr_OFFADDR").attr('disabled',true);
			break;
		}
		case "None":{

			clntmailaddr.value = "";
			clntmailaddr.readOnly = false;
			clntmailaddr.className = "writeModeTextbox";

			if (FNA_CUSTCATEG != "COMPANY") {
				spsmailaddr.value = "";
				spsmailaddr.readOnly = false;
				spsmailaddr.className = "writeModeTextbox";

			}// end of if

			$("input[name='radCDClntMailngAddr_032018']").attr('disabled',false);
			$("#radCDClntMailngAddr_OFFADDR").attr('disabled',false);

//			alert('Attach a supporting document in client attachments screen.(e.g.) bank statement/bill (Dated within 3 months)');
//			 enabOrDisabResnMailAddr('radCDClntMailngAddr_032018','ENABLE');

			break;
		}

		case "NoneSps": {


			if (FNA_CUSTCATEG != "COMPANY") {
				spsmailaddr.value = "";
				spsmailaddr.readOnly = false;
				spsmailaddr.className = "writeModeTextbox";

			}else{
				if(!isEmpty(spsName)){
					spsmailaddr.value = spsregaddr.value;
					spsmailaddr.readOnly = true;
					spsmailaddr.className = "readOnlyText";
					// enabOrDisabResnMailAddr('radCDClntMailngAddr_032018','DISABLE');
					}else{
						spsmailaddr.value = "";
						spsmailaddr.readOnly = false;
						spsmailaddr.className = "writeModeTextbox";
					}
			}// end of if

			$("input[name='radCDClntMailngAddr_032018']").attr('disabled',false);
			$("#radCDClntMailngAddr_OFFADDR").attr('disabled',false);

//			alert('Attach a supporting document in client attachments screen.(e.g.) bank statement/bill (Dated within 3 months)');
//			 enabOrDisabResnMailAddr('radCDClntMailngAddr_032018','ENABLE');

			break;
		}
		}
//		Commentted on MAR_2018
		*/

	}else{

		$("#dfSelfMailaddrflg").prop("disabled",false);
		$("#dfSpsMailaddrflg").prop("disabled",false);

		/*clntmailaddr.value = "";
		clntmailaddr.readOnly = false;
		clntmailaddr.className = "writeModeTextbox";*/

		/*if (FNA_CUSTCATEG != "COMPANY") {*/
		/*	spsmailaddr.value = "";
			spsmailaddr.readOnly = false;
			spsmailaddr.className = "writeModeTextbox";
		}*/
		// end of if

		$("input[name='radCDClntMailngAddr_032018']").prop('disabled',false);
		$("#radCDClntMailngAddr_OFFADDR").prop('disabled',false);

		/*
		clntmailaddr.value = clntregaddr.value;
		clntmailaddr.readOnly = true;
		clntmailaddr.className = "readOnlyText";

		if(!isEmpty(spsName)){
		spsmailaddr.value = clntregaddr.value;
		spsmailaddr.readOnly = true;
		spsmailaddr.className = "readOnlyText";
		// enabOrDisabResnMailAddr('radCDClntMailngAddr_032018','DISABLE');
		}else{
			spsmailaddr.value = "";
			spsmailaddr.readOnly = false;
			spsmailaddr.className = "writeModeTextbox";
		}
		$("#radCDClntMailngAddr_OFFADDR").attr('checked',false);
		$("input[name='radCDClntMailngAddr_032018']").attr('disabled',true);
		$("input[name='radCDClntMailngAddr_032018']").attr('checked',false);
		$("#radCDClntMailngAddr_OFFADDR").attr('disabled',true);

//		Commentted on MAR_2018
		*/
	}


}

function mailAddrQueryMode(rdobj){

	var addr = rdobj.value;

	switch (addr) {

	case "Clnt": {
		$("#radCDClntMailngAddr_OFFADDR").attr('checked',false);
		$("input[name='radCDClntMailngAddr_032018']").attr('disabled',true);
		$("#radCDClntMailngAddr_OFFADDR").attr('disabled',true);
	}
	case "Sps": {
		$("#radCDClntMailngAddr_OFFADDR").attr('checked',false);
		$("input[name='radCDClntMailngAddr_032018']").attr('disabled',true);
	}
	case "None": {
		$("input[name='radCDClntMailngAddr_032018']").attr('disabled',false);
		$("#radCDClntMailngAddr_OFFADDR").attr('disabled',false);
	}

	}

}

function mailAddrOth(rdobj){
	var addr = rdobj.value;


	var clntmailaddr = fnaForm.dfSelfMailaddr;
	var spsmailaddr = fnaForm.dfSpsMailaddr;

	var spsName = fnaForm.dfSpsName.value;

//	if(addr == "None"){////	 commentted on MAR_2018

	if(rdobj.checked==true){

		alert('Attach a supporting document in client attachments screen.(e.g.) bank statement/bill (Dated within 3 months)');

		rdobj.parentNode.childNodes[7].value="";
		rdobj.parentNode.childNodes[7].readOnly=false;
		rdobj.parentNode.childNodes[7].className="writeModeTextbox";

		/*clntmailaddr.value = "";
		clntmailaddr.readOnly = false;
		clntmailaddr.className = "writeModeTextbox";*/

/*		if (FNA_CUSTCATEG != "COMPANY") {
			spsmailaddr.value = "";
			spsmailaddr.readOnly = false;
			spsmailaddr.className = "writeModeTextbox";

		}*/

		$("input[name='radCDClntMailngAddr_032018']").attr('disabled',false);
		$("#radCDClntMailngAddr_OFFADDR").attr('disabled',false);


	}else{
//		else block added on MAR_2018

		rdobj.parentNode.childNodes[7].value = "";
		rdobj.parentNode.childNodes[7].readOnly = true;
		rdobj.parentNode.childNodes[7].className = "readOnlyText";

/*		if (FNA_CUSTCATEG != "COMPANY") {
			spsmailaddr.value = "";
			spsmailaddr.readOnly = true;
			spsmailaddr.className = "readOnlyText";

		}*/

		$("input[name='radCDClntMailngAddr_032018']").attr('disabled',true);
		$("#radCDClntMailngAddr_OFFADDR").attr('disabled',true);

	}

}

function validateFnaSelfSpsDets(flag) {
//	3a)For Clientâ€™s Declaration section, all fields are to be made mandatory.


	var oneContOnly = document.getElementById("dfSelfOnecontflg");
	var clientSameasRegAddr = document.getElementById("dfSelfSameasregadd");
	var alertMessageArr = new Array();

	if (FNA_FORMTYPE == SIMPLIFIED_VER) {
		

		alertMessageArr = [
				[ 'dfSelfName', NAMEOFCLIENT, '12', 'TEXT' ],
				[ 'dfSelfGender', GENDEROFCLIENT, '12', 'TEXT' ],
				[ 'dfSelfDob', DOBOFCLIENT, '12', 'TEXT' ],
				[ 'dfSelfNric', NRICOFCLIENT, '12', 'TEXT' ],
				[ 'dfSelfBirthcntry', BIRTHCOUNTRYCLIENT, '12', 'TEXT' ],
				[ 'radselfSG^radselfSGPR^radselfOTH', NATLYCLIENT, '12', 'RADIO' ],
				[ 'dfSelfNatyDets^radselfSGPR', NATLYCLIENTOTH, '12', 'RADIOOTHERS' ],
				[ 'dfSelfNatyDets^radselfOTH', NATLYCLIENTOTH, '12', 'RADIOOTHERS' ]
				

		];
		
		if(flag){
			alertMessageArr.push([ 'dfSelfMartsts', MARTIALSTATUSOFCLIENT, '12', 'TEXT' ]);
		}

		if(oneContOnly.checked == true){
			alertMessageArr.push([ 'dfSelfHome^dfSelfMobile^dfSelfOffice^dfSelfPersemail',CONTACTDETAILS1, '12', 'OR' ]);
		}else{
			alertMessageArr.push([ 'dfSelfHome^dfSelfMobile^dfSelfOffice^dfSelfPersemail',CONTACTDETAILS, '12', 'OR' ]);
		}
		alertMessageArr.push([ 'dfSelfHomeaddr', REGADDRESSCLIENT, '12', 'TEXT' ]);
//		alertMessageArr.push([ 'radCDClntAddrClnt^radCDClntAddrSps^radCDClntAddrNone', MAILINGADDRESS, '12', 'RADIO' ]);

		if(!clientSameasRegAddr.checked){
//			alertMessageArr.push([ 'dfSelfMailaddrflg', MAILINGADDRESSSELF, '12', 'CHECKBOX' ]);
//			alertMessageArr.push([ 'radCDClntMailngAddr_032018', REASONMAILINGADDRESS, '12', 'RAD' ]);
//			alertMessageArr.push([ 'txtFldCDMailAddOth_032018^radCDClntMailngAddr_032018', REASONMAILINGADDRESSOTHERS, '12', 'RADOTHERS' ]);
//			if(document.getElementById("dfSelfMailaddrflg").checked){
				alertMessageArr.push([ 'dfSelfMailaddr', MAILINGADDRESSOTHERS, '12', 'TEXT' ]);
//			}
		}

//		alertMessageArr.push([ 'dfSelfCompname', EMPLOYEEROFCLIENT, '12', 'TEXT' ]);
//		alertMessageArr.push([ 'dfSelfBusinatr', NATUREBUSINESSOFCLIENT, '12', 'TEXT' ]);
//		alertMessageArr.push([ 'dfSelfBusinatrDets^dfSelfBusinatr', NATUREBUSINESSOTHERSOFCLIENT, '12', 'TEXTOTHERS' ]);
//		alertMessageArr.push([ 'dfSelfOccpn', OCCUPATIONOFCLIENT, '12', 'TEXT' ]);
//		alertMessageArr.push([ 'dfSelfAnnlincome', ANNUALINCOMEOFCLIENT, '12', 'TEXTDouble' ]);
		// [ 'txtFldSelfResCountry_032018',CNTRYOFRESIOFCLIENT, '12', 'TEXT' ],
		
		if(flag){
			
			alertMessageArr.push(['chkCDClntErndIncm^chkCDClntInvIncm^chkCDClntPrsnlIncm^chkCDClntCPFSvngs^chkCDClntSrcIncmOthrs', SOURCEOFFFUNDCLIENT, '12', 'CHECKBOX' ]);
			alertMessageArr.push([ 'dfSelfFundsrcDets^chkCDClntSrcIncmOthrs', SRCFUNDOTHCLIENT, '12', 'RADIOOTHERS' ]);
			alertMessageArr.push([ 'dfSelfEngSpoken', LANGSPOKCLIENT, '12', 'RAD' ]);
			alertMessageArr.push([ 'dfSelfEngWritten', LANGWRITECLIENT, '12', 'RAD' ]);
			alertMessageArr.push([ 'dfSelfEdulevelRad', EDULVLCLIENT, '12', 'RAD' ]);

			
		}

		if (!isEmpty(trim(document.getElementById("dfSpsName").value))) {
			alertMessageArr.push([ 'dfSpsName', NAMEOFSPOUSECLIENT, '12', 'TEXT' ]);
			alertMessageArr.push([ 'dfSpsGender', NAMEOFSPOUSEGENDER, '12', 'TEXT' ]);
			alertMessageArr.push([ 'dfSpsDob', DOBOFSPOUSE, '12', 'TEXT' ]);
			// alertMessageArr.push([ 'txtFldDfSpsAgenext',AGESPOUSE, '12','RADIO' ]);
			alertMessageArr.push([ 'dfSpsNric',NRICOFSPOUSE, '12','TEXT' ]);
			alertMessageArr.push([ 'dfSpsBirthcntry',BIRTHCOUNTRYSPOUSE, '12','TEXT' ]);
			alertMessageArr.push([ 'radSpsSG^radSpsSGPR^radSpsOTH', NATLYSPOUSE, '12', 'RADIO' ]);
			alertMessageArr.push([ 'dfSpsNatyDets^radSpsSGPR', NATLYSPOUSEOTH, '12', 'RADIOOTHERS' ]);
			alertMessageArr.push([ 'dfSpsNatyDets^radSpsOTH', NATLYSPOUSEOTH, '12', 'RADIOOTHERS' ]);
			if(flag){
				alertMessageArr.push([ 'dfSpsMartsts', MARTIALSTATUSOFSPOUSE, '12', 'TEXT' ]);
			}

			if(oneContOnly.checked == true){
				alertMessageArr.push([ 'dfSpsHome^dfSpsHp^dfSpsOffice^dfSpsPersemail', CONTACTDETAILSSPOUSE1, '12', 'OR' ]);
			}else{
				alertMessageArr.push([ 'dfSpsHome^dfSpsHp^dfSpsOffice^dfSpsPersemail', CONTACTDETAILSSPOUSE, '12', 'OR' ]);
			}

			alertMessageArr.push([ 'dfSpsHomeaddr', REGADDRESSSPOUSE, '12', 'TEXT' ]);

			if(!clientSameasRegAddr.checked){
//				alertMessageArr.push([ 'dfSpsMailaddrflg', MAILINGADDRESSPOUSE, '12', 'CHECKBOX' ]);
//				if(document.getElementById("dfSpsMailaddrflg").checked){
					alertMessageArr.push([ 'dfSpsMailaddr', MAILINGADDRESSOTHERS, '12', 'TEXT' ]);
//				}
			}

//			alertMessageArr.push([ 'dfSpsCompname', EMPLOYEEROFSPOUSE, '12', 'TEXT' ]);
//			alertMessageArr.push([ 'dfSpsBusinatr', NATUREBUSINESSOFSPOUSE, '12', 'TEXT' ]);
//			alertMessageArr.push([ 'dfSpsBusinatrDets^dfSpsBusinatr', NATUREBUSINESSOTHERSOFSPOUSE, '12', 'TEXTOTHERS' ]);
//			alertMessageArr.push([ 'dfSpsOccpn', OCCUPATIONOFSPOUSE, '12', 'TEXT' ]);
//			alertMessageArr.push([ 'dfSpsAnnlincome', ANNUALINCOMEOFSPOUSE, '12', 'TEXTDouble' ]);

			/*alertMessageArr.push([ 'radCDClntAddrClnt^radCDClntAddrSps^radCDClntAddrNone', MAILINGADDRESSSPOUSE, '12', 'RADIO' ]); commented buy kavi 17/03/2018*/
//			alertMessageArr.push([ 'dfSpsMailaddr^radCDClntAddrNone', MAILINGADDRESSOTHERSSPOUSE, '12', 'RADIOOTHERS' ]);
			//added by kavi 17/03/2018

			if(flag){
				
				alertMessageArr.push(['chkCDSpsErndIncm^chkCDSpsInvIncm^chkCDSpsPrsnlIncm^chkCDSpsCPFSvngs^chkCDSpsSrcIncmOthrs', SOURCEOFFFUNDSPOUSE, '12', 'CHECKBOX']);
				alertMessageArr.push([ 'dfSpsFundsrcDets^chkCDSpsSrcIncmOthrs', SRCFUNDOTHSPOUSE, '12', 'RADIOOTHERS' ]);
				alertMessageArr.push([ 'dfSpsEngSpoken', LANGSPOKSPOUSE, '12', 'RAD' ]);
				alertMessageArr.push([ 'dfSpsEngWritten', LANGWRITESPOUSE, '12', 'RAD' ]);
				alertMessageArr.push([ 'dfSpsEdulevelRad', EDULVLSPOUSE, '12', 'RAD' ]);

				
			}


		}

	}
//	This else part nor required; DEC2019
	else {
		alertMessageArr = [ [ 'txtFldFnaClntName', NAMEOFCLIENT, '2', 'TEXT' ],
				[ 'txtFldFnaClntNric', NRICOFCLIENT, '2', 'TEXT' ],
				[ 'dfSelfName', NAMEOFCLIENT, '12', 'TEXT' ],
				[ 'dfSelfNric', NRICOFCLIENT, '12', 'TEXT' ]
		// ,['fnaadvrecTbl',FNA_ADVRECPRDTTYPE_VALMSG,'14','TABLE']
		];
	}
//alert(alertMessageArr)

	var alertMessage = new Array();
	var le = 0;
	for (var len = 0; len < alertMessageArr.length; len++) {

//		alert(alertMessageArr[len][0])

		if (alertMessageArr[len][3] == "TEXT") {

			if (isEmpty(trim(document.getElementById(alertMessageArr[len][0]).value))) {
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}
		} else if (alertMessageArr[len][3] == "TABLE") {
			var Tbl = document.getElementById(alertMessageArr[len][0]);
			var Tbody = Tbl.tBodies[0];
			var rowLength = Tbody.rows.length;
			for (var ro = 0; ro < rowLength; ro++) {
				if (isEmpty(trim(Tbody.rows[ro].cells[1].childNodes[0].value))) {
					alertMessage[le] = new Array(4);
					alertMessage[le][0] = alertMessageArr[len][0];
					alertMessage[le][1] = alertMessageArr[len][1];
					alertMessage[le][2] = alertMessageArr[len][2];
					alertMessage[le][3] = alertMessageArr[len][3] + "^" + ro+ "^" + "2";
					changedarrGlobal2.push(alertMessageArr[len][1])
					le++;
				}
			}
		} else if (alertMessageArr[len][3] == "OR") {



			var ORArray = alertMessageArr[len][0].split("^");
			var lengh = 0;
			for (var lenValue = 0; lenValue < ORArray.length; lenValue++) {
				if (isEmpty(trim(document.getElementById(ORArray[lenValue]).value))) {
					lengh++;
				}
			}


			if ((oneContOnly.checked == true && lengh > 3) || (oneContOnly.checked == false && lengh >2)) {
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}

		} else if (alertMessageArr[len][3] == "TEXTDouble") {
			if (isEmpty(trim(document.getElementById(alertMessageArr[len][0]).value))) {// ||
																						// document.getElementById(alertMessageArr[len][0]).value
																						// ==
																						// "0"
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}
		} else if (alertMessageArr[len][3] == "TEXTOTHERS") {

			var OthArray = alertMessageArr[len][0].split("^");
			var lengh = 0;

			if (document.getElementById(OthArray[1]).value.toUpperCase() == "OTHERS"
					&& isEmpty(trim(document.getElementById(OthArray[0]).value))) {
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}

		} else if (alertMessageArr[len][3] == "CHECKBOX") {

			var CheckBoxArray = alertMessageArr[len][0].split("^");
			var lengh = 0;
			for (var lenValue = 0; lenValue < CheckBoxArray.length; lenValue++) {

				if (document.getElementById(CheckBoxArray[lenValue]).checked == true) {
					lengh++;
				}
			}

			if (lengh == 0) {
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}

		} else if (alertMessageArr[len][3] == "RADIO") {

			var RadArray = alertMessageArr[len][0].split("^");
			var lengh = 0;
			for (var lenValue = 0; lenValue < RadArray.length; lenValue++) {
				if (document.getElementById(RadArray[lenValue]).checked == true) {
					lengh++;
				}
			}

			if (lengh == 0) {
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}

		} else if (alertMessageArr[len][3] == "RADIOOTHERS") {

			var OthArray = alertMessageArr[len][0].split("^");
			var lengh = 0;

			if (document.getElementById(OthArray[1]).checked == true
					&& isEmpty(trim(document.getElementById(OthArray[0]).value))) {
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}

		} else if (alertMessageArr[len][3] == "RAD") {

			var mailAddr = document.getElementsByName(alertMessageArr[len][0]);
			var lengh = 0;
			for (var i = 0; i < mailAddr.length; i++) {
				if (mailAddr[i].checked) {
					mail_value = mailAddr[i].value;
					lengh++;
				}
			}

			if (lengh == 0) {
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}

		} else if (alertMessageArr[len][3] == "RADOTHERS") {

			var RadArray = alertMessageArr[len][0].split("^");

			var mailAddr = document.getElementsByName(RadArray[1]);
			var lengh = 0;
			var mail_value = "";
			for (var i = 0; i < mailAddr.length; i++) {
				if (mailAddr[i].checked) {
					mail_value = mailAddr[i].value;
					lengh++;
				}
			}

			if (mail_value == "OTH"
					&& isEmpty(trim(document.getElementById(RadArray[0]).value))) {
				alertMessage[le] = new Array(4);
				alertMessage[le][0] = alertMessageArr[len][0];
				alertMessage[le][1] = alertMessageArr[len][1];
				alertMessage[le][2] = alertMessageArr[len][2];
				alertMessage[le][3] = alertMessageArr[len][3];
				changedarrGlobal2.push(alertMessageArr[len][1])
				le++;
			}

		}

	}

//	alert("Len->"+alertMessage.length)
	if (alertMessage.length > 0 && !flag)
		ModalPopups.MandatoryPopup("KYCLoadingLayer", alertMessage, {
			width : 500,
//			height : "auto",
			onOk : "",
			onCancel : "ModalPopupsPromptCancel()"
		});

	if (alertMessage.length > 0 && !flag) {
		return false;
	}

	return true;

}

function showEmailIdLayout(forfld) {

	var emailIdFld = forfld.parentNode.parentNode.cells[1].childNodes[0];

	var availableName = [];
	var emailIdArr = EMAILID_FOR_MAILSCR.split("^");
	var rr = 0;
	var mgr = 0;
	var stf = 0;

	removeTblRows("emailIdTbl");
	removeTblRows("emailIdTblMgr");
	removeTblRows("emailIdTbl_Para");

	for (var em = 0; em < emailIdArr.length; em++) {
		var emdet = emailIdArr[em].split("~");
		availableName[availableName.length++] = emdet[1];
	}

	if (isEmpty(emailIdFld.value)) {
		consEmailIdArr.length = 0;
	} else {
		consEmailIdArr = emailIdFld.value.split(",");
	}

	if (!emailpopupFlag) {

		$('#emailIdTbl').fixheadertable({
			// caption : 'Alterations',
			colratio : [ 60, 80, 275, 275 ],
			height : 212,
			ieHeight : 210,
			width : 710,
			zebra : true,
			sortable : false,
			sortType : [ 'string', 'string', 'string', 'string' ],
			dateFormat : 'dd/mm/yyyy',
			pager : false,
			rowsPerPage : 20,
			resizeCol : true,
			tblId : 'emailIdTbl'
		});

		$('#emailIdTblMgr').fixheadertable({
			// caption : 'Alterations',
			colratio : [ 50, 70, 260, 260 ],
			height : 250,
			ieHeight : 210,
			width : 650,
			zebra : true,
			sortable : false,
			sortType : [ 'string', 'string', 'string', 'string' ],
			dateFormat : 'dd/mm/yyyy',
			pager : false,
			rowsPerPage : 20,
			resizeCol : true,
			tblId : 'emailIdTblMgr'
		});

		$('#emailIdTbl_Para').fixheadertable({
			// caption : 'Alterations',
			colratio : [ 50, 70, 260, 260 ],
			height : 250,
			ieHeight : 210,
			width : 650,
			zebra : true,
			sortable : false,
			sortType : [ 'string', 'string', 'string', 'string' ],
			dateFormat : 'dd/mm/yyyy',
			pager : false,
			rowsPerPage : 20,
			resizeCol : true,
			tblId : 'emailIdTbl_Para'
		});

		document.getElementById("emailIdTblheaderTbl").style.marginRight = "13px";
		// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
		document.getElementById("emailIdTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
		document.getElementById("emailIdTblmainwrapper").style.height = "240px";
		document.getElementById("emailIdTbltampon").style.height = "250px";

		if (getBrowserApp == "Microsoft Internet Explorer") {
			var Tbl = document.getElementById("emailIdTblheaderTbl").childNodes[0].childNodes[0];
			var bodyContainer = document
					.getElementById("emailIdTblbodyContainer");
			var headerTbl = document.getElementById("emailIdTblheaderTbl");
			bodyContainer.onscroll = function(e) {
				Tbl.style.left = '-' + this.scrollLeft + 'px';
			};
		}

		document.getElementById("emailIdTblMgrheaderTbl").style.marginRight = "13px";
		// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
		document.getElementById("emailIdTblMgrheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
		document.getElementById("emailIdTblMgrmainwrapper").style.height = "240px";
		document.getElementById("emailIdTblMgrtampon").style.height = "250px";

		if (getBrowserApp == "Microsoft Internet Explorer") {
			var Tbl = document.getElementById("emailIdTblMgrheaderTbl").childNodes[0].childNodes[0];
			var bodyContainer = document
					.getElementById("emailIdTblMgrbodyContainer");
			var headerTbl = document.getElementById("emailIdTblMgrheaderTbl");
			bodyContainer.onscroll = function(e) {
				Tbl.style.left = '-' + this.scrollLeft + 'px';
			};
		}

		document.getElementById("emailIdTbl_ParaheaderTbl").style.marginRight = "13px";
		// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
		document.getElementById("emailIdTbl_ParaheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
		document.getElementById("emailIdTbl_Paramainwrapper").style.height = "240px";
		document.getElementById("emailIdTbl_Paratampon").style.height = "250px";

		if (getBrowserApp == "Microsoft Internet Explorer") {
			var Tbl = document.getElementById("emailIdTbl_ParaheaderTbl").childNodes[0].childNodes[0];
			var bodyContainer = document
					.getElementById("emailIdTbl_ParabodyContainer");
			var headerTbl = document.getElementById("emailIdTbl_ParaheaderTbl");
			bodyContainer.onscroll = function(e) {
				Tbl.style.left = '-' + this.scrollLeft + 'px';
			};
		}

		emailpopupFlag = true;

		if (LOG_USER_STFTYPE == STFTYPE_PARA) {
			document.getElementById("paraplanner_tab").style.display = "";
		}
	}

	var emailId = "";
	var whichTBody = "", whichrw = "";

	var tbl = document.getElementById("emailIdTbl");
	var tbody = tbl.tBodies[0];
	var chkdCnt = 0;

	var mgrTbl = document.getElementById("emailIdTblMgr");
	var mgrTbody = mgrTbl.tBodies[0];

	var paraTbl = document.getElementById("emailIdTbl_Para");
	var paraTBody = paraTbl.tBodies[0];

	if (LOG_USER_STFTYPE == STFTYPE_PARA) {

		for ( var para in paraAdvEmailObj) {

			var paraAdvDets = paraAdvEmailObj[para];
			// paraAdvEmailIds += paraAdvDets["paraAdvStfEmailId"]+",";

			var rc = rr++;

			var prow = paraTBody.insertRow(rc);

			whichTBody = paraTBody;
			whichrw = prow;

			var cell0 = prow.insertCell(0);
			cell0.innerHTML = '<input type="text" name="emsino" readOnly="true" class="fpNonEditTblTxt" style="width:100%"/>';
			cell0.childNodes[0].value = rc + 1;
			cell0.style.textAlign = "center";

			var cell1 = prow.insertCell(1);
			cell1.innerHTML = '<input type="checkbox" onclick="getEmailId(this)"/>';
			cell1.style.textAlign = "center";

			var cell2 = prow.insertCell(2);
			cell2.innerHTML = '<input type="text" name="emadvstfname" readOnly="true" class="fpNonEditTblTxt" style="width:100%" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" /><input type="hidden" name="emadvstfid"/>';
			cell2.childNodes[1].value = paraAdvDets["paraAdvStfId"];
			cell2.childNodes[0].value = paraAdvDets["paraAdvStfName"];

			var cell3 = prow.insertCell(3);
			cell3.innerHTML = '<input type="text" name="emid" readOnly="true" class="fpNonEditTblTxt" style="width:100%" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';
			cell3.childNodes[0].value = paraAdvDets["paraAdvStfEmailId"];
			emailId = paraAdvDets["paraAdvStfEmailId"];

			if (consEmailIdArr.length > 0) {

				for ( var exs in consEmailIdArr) {
					if (consEmailIdArr[exs] == emailId) {
						whichrw.cells[1].childNodes[0].checked = true;
						chkdCnt++;
					}
				}
			}

		}

	}

	// else{

	for (var em = 0; em < emailIdArr.length; em++) {

		var emdet = emailIdArr[em].split("~");

		if (emdet[2] == STFTYPE_STAFF) {

			var rcs = stf++;

			var crow = tbody.insertRow(rcs);

			whichTBody = tbody;
			whichrw = crow;

			var cell0 = crow.insertCell(0);
			cell0.innerHTML = '<input type="text" name="emsino" readOnly="true" class="fpNonEditTblTxt" style="width:100%"/>';
			cell0.childNodes[0].value = rcs + 1;
			cell0.style.textAlign = "center";

			var cell1 = crow.insertCell(1);
			cell1.innerHTML = '<input type="checkbox" onclick="getEmailId(this)"/>';
			cell1.style.textAlign = "center";

			var cell2 = crow.insertCell(2);
			cell2.innerHTML = '<input type="text" name="emadvstfname" readOnly="true" class="fpNonEditTblTxt" style="width:100%" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" /><input type="hidden" name="emadvstfid"/>';
			cell2.childNodes[1].value = emdet[0];
			cell2.childNodes[0].value = emdet[1];

			var cell3 = crow.insertCell(3);
			cell3.innerHTML = '<input type="text" name="emid" readOnly="true" class="fpNonEditTblTxt" style="width:100%" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"/>';
			cell3.childNodes[0].value = emdet[4];
			emailId = emdet[4];

		}

		if (emdet[3] == "Y") {

			var mg = mgr++;

			var mrow = mgrTbody.insertRow(mg);

			whichTBody = mgrTbody;
			whichrw = mrow;

			var cell0 = mrow.insertCell(0);
			cell0.innerHTML = '<input type="text" name="emsino" readOnly="true" class="fpNonEditTblTxt" style="width:100%"/>';
			cell0.childNodes[0].value = mg + 1;
			cell0.style.textAlign = "center";

			var cell1 = mrow.insertCell(1);
			cell1.innerHTML = '<input type="checkbox" onclick="getEmailId(this)"/>';
			cell1.style.textAlign = "center";

			var cell2 = mrow.insertCell(2);
			cell2.innerHTML = '<input type="text" name="emadvstfname" readOnly="true" class="fpNonEditTblTxt" style="width:100%"/><input type="hidden" name="emadvstfid"/>';
			cell2.childNodes[1].value = emdet[0];
			cell2.childNodes[0].value = emdet[1];

			var cell3 = mrow.insertCell(3);
			cell3.innerHTML = '<input type="text" name="emid" readOnly="true" class="fpNonEditTblTxt" style="width:100%"/>';
			cell3.childNodes[0].value = emdet[4];
			emailId = emdet[4];

		}

		if (consEmailIdArr.length > 0) {

			for ( var exs in consEmailIdArr) {
				if (consEmailIdArr[exs] == emailId) {
					whichrw.cells[1].childNodes[0].checked = true;
					chkdCnt++;
				}
			}
		}

	}

	// }

	var dialog = $("#emailIdsPopup").dialog({
		autoOpen : false,
		height : 'auto',
		width : '70%',
		modal : true,
		resizable : false,

		buttons : {
			" Assign " : function() {
				assignEmailIds(dialog, emailIdFld);
			},
			Cancel : function() {
				dialog.dialog("close");
			}
		},
		close : function() {
		}
	});

	dialog.dialog("open");

}

function assignEmailIds(dialog, fld) {

	var tblId = document.getElementById("emailIdTbl");
	var tblBody = tblId.tBodies[0];
	var rowLen = tblBody.rows.length;

	var mgrTbl = document.getElementById("emailIdTblMgr");
	var mgrTbody = mgrTbl.tBodies[0];
	var mgrRowLen = mgrTbody.rows.length;

	var paraTbl = document.getElementById("emailIdTbl_Para");
	var paraTbody = paraTbl.tBodies[0];
	var paraRowLen = paraTbody.rows.length;

	var msgFlg = false;
	var selCount = 0;

	var allEmailIds = "";

	if (rowLen > 0) {
		for (var row = 0; row < rowLen; row++) {
			if (tblBody.rows[row].cells[1].childNodes[0].checked) {
				selCount++;
				break;
				// allEmailIds +=
				// tblBody.rows[row].cells[3].childNodes[0].value+",";
			}
		}

	} else {
		msgFlg = true;
	}

	if (mgrRowLen > 0) {
		for (var row = 0; row < mgrRowLen; row++) {
			if (mgrTbody.rows[row].cells[1].childNodes[0].checked) {
				selCount++;
				break;
				// allEmailIds +=
				// tblBody.rows[row].cells[3].childNodes[0].value+",";
			}
		}
	} else {
		msgFlg = true;
	}

	if (paraRowLen > 0) {
		for (var row = 0; row < paraRowLen; row++) {
			if (paraTbody.rows[row].cells[1].childNodes[0].checked) {
				selCount++;
				break;
				// allEmailIds +=
				// tblBody.rows[row].cells[3].childNodes[0].value+",";
			}
		}
	} else {
		msgFlg = true;
	}

	if (msgFlg || selCount == 0) {
		alert(COMM_NOEMAIL_VALMSG);
		return false;
	}

	consEmailIdArr.sort();

	for ( var mail in consEmailIdArr) {
		allEmailIds += consEmailIdArr[mail] + ",";
	}
	// alert(allEmailIds)
	// allEmailIds += currTxtVal+",";
	;
	fld.value = "";
	fld.value = allEmailIds.substring(0, allEmailIds.length - 1);

	dialog.dialog("close");

}

function emailIdSelAll(chkSelAll, autocomptbl) {

	var tblId = "";

	var stfTbl = document.getElementById("emailIdTbl");
	var mgrTbl = document.getElementById("emailIdTblMgr");
	var paraTbl = document.getElementById("emailIdTbl_Para");

	if (autocomptbl == "STF") {
		tblId = stfTbl;
	} else if (autocomptbl == "MGR") {
		tblId = mgrTbl;
	} else if (autocomptbl == "PARA") {
		tblId = paraTbl;
	}

	var tblBody = tblId.tBodies[0];
	var rowLen = tblBody.rows.length;

	if (rowLen > 0) {

		if (chkSelAll.checked) {
			for (var rc = 0; rc < rowLen; rc++) {
				tblBody.rows[rc].cells[1].childNodes[0].checked = true;
				getEmailId(tblBody.rows[rc].cells[1].childNodes[0]);
			}
		} else {
			for (var rc = 0; rc < rowLen; rc++) {
				tblBody.rows[rc].cells[1].childNodes[0].checked = false;
				getEmailId(tblBody.rows[rc].cells[1].childNodes[0]);
			}
		}

	} else {
		alert("No Rows to select");
		chkSelAll.checked = false;
	}
}

function getEmailId(chkbox) {

	var tableId = chkbox.parentNode.parentNode.parentNode.parentNode.id;

	var tblId = document.getElementById(tableId);
	var tblBody = tblId.tBodies[0];
	var tblRowLen = tblBody.rows.length;
	var chkdCnt = 0;

	var emailObj = chkbox.parentNode.parentNode.childNodes[3].childNodes[0];

	if (chkbox.checked) {
		if (!contains(consEmailIdArr, emailObj.value)) {
			consEmailIdArr[consEmailIdArr.length++] = emailObj.value;
		}
	} else {
		// alert('else');
		if (contains(consEmailIdArr, emailObj.value)) {
			removeItem(consEmailIdArr, emailObj.value);
		}
	}

	for (var chk = 0; chk < tblRowLen; chk++) {
		if (tblBody.rows[chk].cells[1].childNodes[0].checked) {
			chkdCnt++;
		}
	}

	if (chkdCnt > 0 && chkdCnt == tblRowLen) {
		document.getElementById("chkEmailIdSelAll").checked = true;
	} else {
		document.getElementById("chkEmailIdSelAll").checked = false;
	}

	// for(var ss in consEmailIdArr){
	// alert('array elems -->'+consEmailIdArr[ss]);
	// }

}// End of getEmailId

function printBlankForm() {

	var fnaform = document.fnaForm;
	// var fnaid=fnaform.txtFldFnaId.value;
	var tblId = document.getElementById("verdettable");
	var tbody = tblId.tBodies[0];
	var rlen = tbody.rows.length;

	var fnaid = "";

	if(rlen > 0)
		fnaid = tbody.rows[rlen - 1].cells[0].childNodes[0].value;


	var custid = fnaform.txtFldCustId.value;
	var formtype = document.getElementById("txtFldKycForm").value;

	var machine = "";

	var strBirtFormat = "/frameset?__report=";
	var params = "&P_CUSTID=" + custid + "&P_FNAID=" + fnaid+"&__format=pdf"
			+ "&P_PRDTTYPE=" + custLobDets + "&P_CUSTOMERNAME="
			+ encodeURIComponent(FNA_CUSTNAME) + "&P_CUSTNRIC="
			+ encodeURIComponent(FNA_CUSTNRIC) + "&P_FORMTYPE=" + formtype
			+ "&P_DISTID=" + LOGGED_DISTID;

	machine = BIRT_URL + strBirtFormat + FNA_RPT_FILELOC + params;


	creatSubmtDynaReportPDF(machine);

}

function hidePagingDiv() {

	/*$("#tabs-template-li").css("display", "none");*/

	var tblId = document.getElementById("verdettable");
	var chktbody = tblId.tBodies[0];
	var rl = chktbody.rows.length;

	for (var r = 0; r < rl; r++) {
		/*chktbody.rows[r].cells[0].childNodes[1].checked = false; commented by kavi on 4/4/2018*/
	}

	// alert("1/11122222222")

	$("#paginationdiv > #curr_pageval").val("");
	$("#simplepaginationdiv > #curr_pageval_simple").val("");
	$('.pagination').jqPagination("destroy", "");
	$('.simplepagination').jqPagination("destroy", "");
	// alert("1/23333333333")

	$("#paginationdiv").hide();
	$("#simplepaginationdiv").hide();

	/*$("#tblArchDets").hide();*/
	// $("#btngroup").hide();
	saveBtnObj.setStyle('display', 'none');
	reportBtnObj.setStyle('display', 'none');
	ntucBtnObj.setStyle('display', 'none');
	clientSignBtnObj.setStyle('display', 'none');
	$("#esignaturepopbox").hide();
}

function showPagingDiv() {

	$("#tabs").tabs("option", "active", 1);
	LoadPageWithPaging(FNA_FORMTYPE);

	saveBtnObj.setStyle('display', '');
	reportBtnObj.setStyle('display', '');
	if(!($("#verdettable tbody tr:first td:eq(3) select:first").val()=="Submitted"))
	ntucBtnObj.setStyle('display', '');
//	clientSignBtnObj.setStyle('display', '');
	$("#esignaturepopbox").show();
	// This functionality not required,

	/*
	 * var tblId = document.getElementById("verdettable"); var chktbody =
	 * tblId.tBodies[0]; var rl = chktbody.rows.length; var showflg = false; //
	 * var chkFinal = true;
	 *
	 * var formType = FNA_FORMTYPE;
	 *
	 * for (var r = 0; r < rl; r++) {
	 *
	 *
	 *
	 * var selectedFnaArchSts = chktbody.rows[r].cells[4].childNodes[0].value;
	 * var selectedFnaClntCon = chktbody.rows[r].cells[9].childNodes[0].checked;
	 * var strLastCrtdArchFlg = chktbody.rows[r].cells[7].childNodes[1].value;
	 *
	 *
	 * if (strLastCrtdArchFlg == "Y") { if(selectedFnaArchSts ==
	 * STR_ARCHSTS_FINAL){ if(selectedFnaClntCon){ showflg = true; break; }
	 * }else if(selectedFnaArchSts != STR_ARCHSTS_FINAL){ showflg = true; break; } //
	 * //// if( selectedFnaArchSts == STR_ARCHSTS_FINAL && //
	 * !selectedFnaClntCon && strLastCrtdArchFlg == "Y" ){ // ////
	 * chkFinal=false; // //// //$("#btngroup").hide(); // //// } // //
	 * if(strLastCrtdArchFlg == "Y"){ // // if(selectedFnaArchSts ==
	 * STR_ARCHSTS_FINAL && // !selectedFnaClntCon){ // // chkFinal=false; // //
	 * //$("#btngroup").hide(); // // }else{ // // chkFinal=true; // //
	 * //$("#btngroup").show(); // // } // // }else{ // // chkFinal=false; // //
	 * //$("#btngroup").hide(); // // } // // if(selectedFnaArchSts ==
	 * STR_ARCHSTS_FINAL){ // // chkFinal=false; // // }else{ // //
	 * chkFinal=true; // // }
	 *  } } // if(showflg){ // $("#tblArchDets").show(); //
	 * $("#paginationdiv").show(); // $("#btngroup").show(); // } // //
	 * if(showflg && chkFinal){ // // $("#btngroup").show(); // // }
	 *
	 * $("#tblArchDets").show();
	 *
	 *
	 * for (var r = 0; r < rl; r++) {
	 * if(chktbody.rows[r].cells[0].childNodes[1].checked) formType =
	 * chktbody.rows[r].cells[11].childNodes[0].value; }
	 *
	 * if(formType == SIMPLIFIED_VER){ $("#paginationdiv").hide();
	 * $("#simplepaginationdiv").show(); } if(formType == ALL_VER){
	 * $("#paginationdiv").show(); $("#simplepaginationdiv").hide(); }
	 *
	 *
	 *
	 * if (showflg) { // $("#btngroup").show(); saveBtnObj.setStyle('display',
	 * ''); reportBtnObj.setStyle('display', ''); }
	 *
	 */

}

function loadExistingPlan() {

	var custId = fnaForm.txtFldCustId.value;

	var tblobj = document.getElementById("fnaLIPlanTbl");
	var tbodyobj = tblobj.tBodies[0];

	if (!loadExistPolFlg) {

		// if(window.confirm("Do you want to load the existing policy(FPMS)
		// details?")){

		var parameter = "dbcall=FPMS_POLICY&txtFldCustId=" + custId;

		var respText = ajaxCall(parameter);

		var retval = respText;// eval('(' + respText + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}

			for ( var tab in tabdets) {

				if (tabdets.hasOwnProperty(tab)) {
					var key = tab;
					var value = tabdets[tab];

					if (key == "NO_MAST_FPMSPOLDETS") {
						alert(NO_EXIST_POL_REC);
						return true;
					}

					if (value.length > 0) {

						if (key == "MAST_FPMSPOLDETS_ARR") {
							var populatedAppIds = [];

							var rlen = tbodyobj.rows.length;
							if (rlen > 0) {
								for (var rc = 0; rc < rlen; rc++) {
									populatedAppIds[populatedAppIds.length++] = tbodyobj.rows[rc].cells[3].childNodes[1].value;
								}
							}

							var anyNewPolFlg = false;
							for ( var val in value) {
								var prdtdata = value[val];
								if (!checkValueInArray(populatedAppIds,
										prdtdata["txtFldAppId"])) {
									anyNewPolFlg = true;
								}
							}

							if (anyNewPolFlg) {

								if (window.confirm("Do you want to load the existing policy(FPMS) details?")) {

									// if(key == "MAST_FPMSPOLDETS_ARR"){

									for ( var val in value) {

										var prdtdata = value[val];

										if (!checkValueInArray(populatedAppIds,
												prdtdata["txtFldAppId"])) {

											fnaLiPlanAddRow('fnaLIPlanTbl',
													false);
											var rowlenobj = tbodyobj.rows.length;

											tbodyobj.rows[rowlenobj - 1].cells[0].childNodes[0].value = prdtdata["txtFldPolProposed"];
											tbodyobj.rows[rowlenobj - 1].cells[1].childNodes[0].value = prdtdata["txtFldPolAssured"];
											tbodyobj.rows[rowlenobj - 1].cells[2].childNodes[0].value = prdtdata["txtFldPolPrinId"];
											tbodyobj.rows[rowlenobj - 1].cells[3].childNodes[0].value = prdtdata["txtFldPolNo"];
											tbodyobj.rows[rowlenobj - 1].cells[3].childNodes[1].value = prdtdata["txtFldAppId"];
											tbodyobj.rows[rowlenobj - 1].cells[4].childNodes[0].value = prdtdata["txtFldPolCateg"];
											tbodyobj.rows[rowlenobj - 1].cells[10].childNodes[0].value = prdtdata["txtFldMatrDate"];

											tbodyobj.rows[rowlenobj - 1].cells[14].childNodes[0].value = prdtdata["txtFldAnnualPremium"];

											tbodyobj.rows[rowlenobj - 1].cells[0].childNodes[0].readOnly = true;
											tbodyobj.rows[rowlenobj - 1].cells[1].childNodes[0].readOnly = true;
											tbodyobj.rows[rowlenobj - 1].cells[2].childNodes[0].readOnly = true;
											tbodyobj.rows[rowlenobj - 1].cells[3].childNodes[0].readOnly = true;
											tbodyobj.rows[rowlenobj - 1].cells[4].childNodes[0].readOnly = true;
											tbodyobj.rows[rowlenobj - 1].cells[10].childNodes[0].readOnly = true;

											tbodyobj.rows[rowlenobj - 1].cells[14].childNodes[0].readOnly = true;

										}
									}
									// loadExistPolFlg=true;
									// }
									return false;
								}
							}
						}
					}
				}
			}
		}
	}
	return true;
}

function archDefaultVal(selobj) {
	glblArchDefltVal = selobj.value;
}

function chkMgrApprSts(selobj) {

	var mgrApprSts = selobj.parentNode.parentNode.childNodes[6].childNodes[0].value;
	// var mgrApprSts = fnaForm.selKycMgrApprStatus.value;

	if (isEmpty(mgrApprSts) || mgrApprSts != MGRAPPRSTS_APPROVE) {
		alert(FNA_YETTOAPPR_BYMGR);
		selobj.value = glblArchDefltVal;
		return false;
	}
	return true;
}

function fnaViewOnly() {

	// for(var i=0;i<fnaForm.elements.length;i++){
	// var e = fnaForm.elements[i];
	// e.disabled=true;
	// }

	for (var i = 0; i < fnaForm.elements.length; i++) {
		var e = fnaForm.elements[i];
		if (!(e.name == "verArchFnaId" || e.name == "version"
				|| e.name == "verCretdBy" || e.name == "verCretdDate"
				|| e.name == "selverKycArchStatus"
				// || e.name == "selverKycSentStatus" //disabled by johnson
				// 20160226
				|| e.name == "selverKycMgrApprSts"
				|| e.name == "txtFldVerMgrApprRem"
				|| e.name == "txtFldVerMgrApprDate"
				|| e.name == "txtFldTblFnaType"
				|| e.name == "hTxtFldLastCrtdArch" || e.name == "curr_pageval"
				|| e.id == "curr_pageval" || e.name == "curr_pageval_simple" || e.id == "curr_pageval_simple")) {

			if (e.type == "text" || e.type == "textArea") {
				e.readOnly = true;
			}
			if (e.type == "checkbox" || e.type == "radio"
					|| e.type == "select-one") {
				e.disabled = true;
			}

		}
	}
	// for(var tbl in kycDhtmlTblId){removeTblRows(kycDhtmlTblId[tbl]);}
}

function assignArchSts(selobj) {

	var archStsval = fnaForm.selKycArchStatus.value;

	if (selobj.value == STR_ARCHSTS_FINAL) {

		if ((window
				.confirm("Do you want to change the archive status as FINAL for selected archive?"))) {
			// var genFileName = fnaGenReport(null,false);
			fnaForm.selKycArchStatus.value = document.getElementById("selArchSts").value;
		} else {
			document.getElementById("selArchSts").value = fnaForm.selKycArchStatus.value;
		}
	} else {

		if (selobj.value != archStsval)
			fnaForm.selKycArchStatus.value = document.getElementById("selArchSts").value;
		else
			document.getElementById("selArchSts").value = archStsval;
	}

}

function chkClntConsent(chkbox) {

	if (chkbox.checked) {
		chkbox.nextSibling.value = "Y";
	} else {
		chkbox.nextSibling.value = "N";
	}
}

function goToPageNumKYC(pageNum, IdName, FieldName, formType) {

	var totalpage, alltotalpage;

	if (formType == ALL_VER) {
		totalpage = pageDetsArr[0].ALL_VER;
		alltotalpage = pageDetsArr[0].TOTAL;
	} else {
		totalpage = pageDetsArr[1].SIMPLIFIED_VER;

		if (!checkValueInArray(custLobArr, LIFE_INS)) {
			totalpage = 14;
		}
		alltotalpage = pageDetsArr[1].TOTAL;
	}

	if (FieldName == "TABLE") {
		var Obj = FieldName.split("^");
		var ObjName = Obj[0];
		var rowNum = Obj[1];
		var cellNum = Obj[2];

		ModalPopupsPromptCancel();
		// $('.pagination').jqPagination('option', 'current_page', pageNum);

		for (var p = 1; p <= alltotalpage; p++) {
			$("#page" + p).hide();
		}
		$("#page23").hide();// EMpty page for simplified version
		$("#page31").hide();//ClientSignature Form
		$("#tabs").tabs("option", "active", 1);
		return false;
		$("#page" + pageNum).show();
		setPageValue(pageNum, formType);// added by johnson on 21012016_1110
		var Tbl = document.getElementById(IdName);
		var Tbody = Tbl.tBodies[0];
		Tbody.rows[rowNum].cells[cellNum].childNodes[0].focus();
		// break;
	} else if (FieldName == "OR" || FieldName == "CHECKBOX"
			|| FieldName == "RADIO") {

		ModalPopupsPromptCancel();
		// $('.pagination').jqPagination('option', 'current_page', pageNum);

		for (var p = 1; p <= alltotalpage; p++) {
			$("#page" + p).hide();
		}
		$("#page23").hide();// EMpty page for simplified version
		$("#page31").hide();//ClientSignature Form
		$("#tabs").tabs("option", "active", 1);
		$("#page" + pageNum).show();

		var idArr = IdName.split("^");
		document.getElementById(idArr[0]).focus();
		setPageValue(pageNum, formType);// added by johnson on 21012016_1110
		// break;
	} else if (FieldName == "TEXTOTHERS" || FieldName == "RADIOOTHERS") {
		ModalPopupsPromptCancel();
		// $('.pagination').jqPagination('option', 'current_page', pageNum);

		for (var p = 1; p <= alltotalpage; p++) {
			$("#page" + p).hide();
		}
		$("#page23").hide();// EMpty page for simplified version
		$("#page31").hide();//ClientSignature Form
		$("#tabs").tabs("option", "active", 1);
		$("#page" + pageNum).show();

		var idArr = IdName.split("^");

		document.getElementById(idArr[0]).focus();
		setPageValue(pageNum, formType);// added by johnson on 21012016_1110
		// break;
	} else {
		ModalPopupsPromptCancel();
		// $('.pagination').jqPagination('option', 'current_page', pageNum);

		for (var p = 1; p <= alltotalpage; p++) {
			$("#page" + p).hide();
		}
		$("#page23").hide();// EMpty page for simplified version
		$("#page31").hide();//ClientSignature Form
		$("#tabs").tabs("option", "active", 1);
		$("#page" + pageNum).show();

		setPageValue(pageNum, formType);// added by johnson on 21012016_1110
		// alert(IdName)
		if (document.getElementById(IdName))
			document.getElementById(IdName).focus();
		// break;
	}

}

function ModalPopupsPromptCancel() {
	ModalPopups.Cancel("KYCLoadingLayer");
}

function fnaGenReport(btnobj, setEmailFlg) {

	var fileName = "";
	var dispFileName = "";
	var insflg = "";
	var servAdvEmail = "";
	var servAdvMgrEmail = "";

	var fnaId = $("#txtFldFnaId").val();
	var custId = $("#txtFldCustId").val();
	var rptloc = $("#txtFldRptFileLoc").val();
	var pdfloc = $("#txtFldPdfGenLoc").val();

	var strCustName = document.getElementsByName("txtFldClientName")[0].value;
	var strAdvName = document.getElementsByName("txtFldAdvName")[0].value;

	// FNA_FORM~CUST_NAME~ARCHIVENO
	var emailD = String(emailDets).split(",");
	var emailsubj = String(emailD[0]);
	var emailcont = String(emailD[1]);

	var subjformat = emailsubj.replace(/~/g, "_");
	dispFileName = subjformat;

	subjformat = subjformat.replace(/CUST_NAME/g, FNA_CUSTNAME);
	dispFileName = subjformat;

	if (btnobj) {

		subjformat = subjformat.replace(/ARCHIVENO/g,
				btnobj.parentNode.parentNode.cells[1].childNodes[1].value)
				.toUpperCase();
		dispFileName = subjformat;

		insflg = "N";

		fnaId = btnobj.parentNode.parentNode.cells[0].childNodes[0].value;

		servAdvEmail = btnobj.parentNode.parentNode.cells[3].childNodes[1];
		servAdvMgrEmail = btnobj.parentNode.parentNode.cells[3].childNodes[2];
	} else {

		subjformat = subjformat.replace(/ARCHIVENO/g,
				document.getElementById("selArchNo").value).toUpperCase();
		dispFileName = subjformat;
		insflg = "Y";
	}

	dispFileName += ".PDF";

	var parameter = "dbcall=GENREPORT&txtFldFnaId=" + fnaId + "&txtFldCustId="
			+ custId + "&genFileName=" + dispFileName + "&strAdvName="
			+ strAdvName + "&strCustName=" + strCustName + "&strBirtUrl="
			+ BIRT_URL;

	var respText = ajaxCall(parameter);

	var retval = respText;// eval('(' + respText + ')');

	for ( var val in retval) {
		var tabdets = retval[val];
		if (tabdets["SESSION_EXPIRY"]) {
			window.location = baseUrl + SESSION_EXP_JSP;
			return;
		}
		for ( var tab in tabdets) {
			if (tabdets.hasOwnProperty(tab)) {
				var key = tab;
				var value = tabdets[tab];

				if (key == "GEN_REPORT_FILENAME") {
					if (!isEmpty(value))
						fileName = value;
				}
			}
		}
	}

	if (isEmpty(fileName)) {
		alert("Error in generating attachment...");
	}

	if (setEmailFlg) {

		// if(LOG_USER_STFTYPE == "PARAPLANNER"){
		// document.getElementById("txtFldEmailTo").value =
		// paraAdvEmailIds.substr(0,paraAdvEmailIds.length-1);

		// }else{
		document.getElementById("txtFldEmailTo").value = servAdvMgrEmail.value;
		document.getElementById("txtFldEmailToCC").value = servAdvEmail.value;
		// }

		document.getElementById("EmailContent").value = emailcont;

		document.getElementById("txtFldSubject").value = subjformat;

		document.getElementById("txtFldMailAttach").value = fileName;
		document.getElementById("attachmentName").value = dispFileName;

	}
	return fileName;
}

// johnson on 17042015
function showAutoCompEmailId(txtfld, autocomptbl) {

	var value = txtfld.value.toUpperCase(), emlIdTbl = "";

	var tblId = document.getElementById("emailIdTbl");
	var mgrTbl = document.getElementById("emailIdTblMgr");
	var paraTbl = document.getElementById("emailIdTbl_Para");

	if (autocomptbl == "STF") {
		emlIdTbl = tblId;
	} else if (autocomptbl == "MGR") {
		emlIdTbl = mgrTbl;
	} else if (autocomptbl == "PARA") {
		emlIdTbl = paraTbl;
	}
	// emlIdTbl = document.getElementById("emailIdTbl"),
	var emlIdTBody = emlIdTbl.tBodies[0], slno = 1;

	for (var em = 0; em < emlIdTBody.rows.length; em++) {
		var advName = emlIdTBody.rows[em].cells[2].childNodes[0].value
				.toUpperCase(), row = emlIdTBody.rows[em];

		if (advName.indexOf(value) != -1) {
			row.style.display = '';
			emlIdTBody.rows[em].cells[0].childNodes[0].value = slno;
			slno++;
		} else
			row.style.display = 'none';
	}// end of for

}// end of showAutoCompEmailId

// added by johnson on 23042015
function chkCompDets(selObj) {
	var compSelObj = selObj.parentNode.childNodes[0];
	if (compSelObj.options[compSelObj.selectedIndex].text == '--SELECT--'
			&& compSelObj.options[compSelObj.selectedIndex].value == '') {
		alert(SELOBJ_CMPNYNAME_ALERT);
		compSelObj.focus();
		return;
	}// end of if
}// end of chkCompDets

/*
 * added by johnson on 29042015 for populating values in the combo box of
 * personal priorities
 */
function popuPersPrioComBox(elemObj, contvalue, cont) {

	if (cont == 'selFnaPPHospExp' || cont == 'selFnaPPOutPat'
			|| cont == 'selFnaPPMajIll' || cont == 'selFnaPPDental'
			|| cont == 'selFnaPPOldAgeDis' || cont == 'selFnaPPLossIncome') {
		contvalue == '01' ? elemObj.value = '1'
				: contvalue == '02' ? elemObj.value = '2'
						: contvalue == '03' ? elemObj.value = '3' : '';
	}// end of if
}// end of popuPersPrioComBox

function getTotalValue(selObj, tblId) {
	var proptbl = document.getElementById(tblId);
	var proptbodyobj = proptbl.tBodies[0];
	var propRowLen = proptbodyobj.rows.length;

	var cellInd = selObj.parentNode.cellIndex;

	var total = "0";
	for (var len = 0; len < propRowLen; len++) {
		if (!IsEmpty(proptbodyobj.rows[len].cells[cellInd].childNodes[0].value)) {
			total = parseFloat(total)
					+ parseFloat(proptbodyobj.rows[len].cells[cellInd].childNodes[0].value);
		}

	}
	var propFooterObj = proptbl.tFoot;

	propFooterObj.rows[0].cells[cellInd].childNodes[0].value = total;
}

function chkFinalSts(selobj) {
	var archsts = selobj.value;

	if (archsts == "FINAL") {

		if (window
				.confirm(" By Changing to FINAL, Cannot modify the Archive,Do you want to continue?"))
			return true;
		else {
			selobj.value = glblArchDefltVal;
			return false;
		}
	}
	//

}

function loadPrinOrFundMgr(selobj) {

	var prdttype = selobj.value;
	var compselobj = selobj.parentNode.parentNode.childNodes[2].childNodes[0];
	var planselobj = selobj.parentNode.parentNode.childNodes[2].childNodes[1];
	var planhidden = selobj.parentNode.parentNode.childNodes[2].childNodes[2];

	if (isEmpty(prdttype)) {
		clearSelectBoxOptions(compselobj);
		clearSelectBoxOptions(planselobj);
		planhidden.value = "";
	} else {
		if (prdttype == "UT") {
			clearSelectBoxOptions(compselobj);
			clearSelectBoxOptions(planselobj);
			planhidden.value = "";
			comboMaker(selobj.parentNode.parentNode.childNodes[2], fmArr, "",
					false);
		} else if (prdttype == "ILP") {
			clearSelectBoxOptions(compselobj);
			clearSelectBoxOptions(planselobj);
			planhidden.value = "";
			comboMaker(selobj.parentNode.parentNode.childNodes[2], prinIdArr,
					"", false);
		}
	}

}

function calcSum(elemObj, type) {

	var elemId = '', sum = 0, zeroVal = '0.0000';
	if (!elemObj)
		elemId = 'null';
	else
		elemId = elemObj.id;

	// Annual Expenditure
	if (type == Ann_Exp.SUMOF_ANNEXP_SELF) {
		for ( var tot in Ann_Exp.Ann_Exp_Self) {
			var id = Ann_Exp.Ann_Exp_Self[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for
		document.getElementById('txtFldFnaExpAnnlSelf').value = sum.toFixed(4);

		if (sum == zeroVal)
			document.getElementById('txtFldFnaExpAnnlSelf').value = '';
	}// end of if

	if (type == Ann_Exp.SUMOF_ANNEXP_SPS) {
		for ( var tot in Ann_Exp.Ann_Exp_Sps) {
			var id = Ann_Exp.Ann_Exp_Sps[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for
		document.getElementById('txtFldFnaExpAnnlSps').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldFnaExpAnnlSps').value = '';
	}// end of if

	if (type == Ann_Exp.SUMOF_ANNEXP_FAM) {
		for ( var tot in Ann_Exp.Ann_Exp_Fam) {
			var id = Ann_Exp.Ann_Exp_Fam[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldFnaExpAnnlFam').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldFnaExpAnnlFam').value = '';
	}// end of if

	// Source of income
	if (type == tot_SrcOfIncm.SRCOFINCM_SELF) {
		for ( var tot in tot_SrcOfIncm.tot_SrcOfIncm_Self) {
			var id = tot_SrcOfIncm.tot_SrcOfIncm_Self[tot], othrElems = $('#'
					+ id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtTotSrcOfIncmSelf').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtTotSrcOfIncmSelf').value = '';
	}// end of if

	// commented by johnson on 26052015
	// if(type == tot_SrcOfIncm.SRCOFINCM_SELF_INCR){

	// for(var tot in tot_SrcOfIncm.tot_SrcOfIncm_Self_Incr){
	// var id = tot_SrcOfIncm.tot_SrcOfIncm_Self_Incr[tot],othrElems =
	// $('#'+id);
	// if(elemId == id)if(!isEmpty(elemObj.value)) sum +=
	// parseFloat(elemObj.value);
	// if(elemId != 'null')if(elemId != id)if(!isEmpty(othrElems.val()))sum +=
	// parseFloat(othrElems.val());
	// if(elemId == 'null')if(elemId != id)if(!isEmpty(othrElems.val()))sum +=
	// parseFloat(othrElems.val());
	// }//end of for
	// document.getElementById('txtTotSrcOfIncmSelfIncr').value =
	// sum.toFixed(4);
	// }//end of if

	// added by johnson on 26052015
	if (type == tot_SrcOfIncm.SRCOFINCM_SELF_PRD) {

		for ( var tot in tot_SrcOfIncm.tot_SrcOfIncm_Self_Prd) {
			var id = tot_SrcOfIncm.tot_SrcOfIncm_Self_Prd[tot], othrElems = $('#'
					+ id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtTotSrcOfIncmSelfPrd').value = sum
				.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtTotSrcOfIncmSelfPrd').value = '';
	}// end of if

	if (type == tot_SrcOfIncm.SRCOFINCM_SPS) {
		for ( var tot in tot_SrcOfIncm.tot_SrcOfIncm_Sps) {
			var id = tot_SrcOfIncm.tot_SrcOfIncm_Sps[tot], othrElems = $('#'
					+ id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtTotSrcOfIncmSps').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtTotSrcOfIncmSps').value = '';

	}// end of if

	// commented by johnson on 26052015
	// if(type == tot_SrcOfIncm.SRCOFINCM_SPS_INCR){
	// for(var tot in tot_SrcOfIncm.tot_SrcOfIncm_Sps_Incr){
	// var id = tot_SrcOfIncm.tot_SrcOfIncm_Sps_Incr[tot],othrElems = $('#'+id);
	// if(elemId == id)if(!isEmpty(elemObj.value))sum +=
	// parseFloat(elemObj.value);
	// if(elemId != 'null')if(elemId != id)if(!isEmpty(othrElems.val()))sum +=
	// parseFloat(othrElems.val());
	// if(elemId == 'null')if(elemId != id)if(!isEmpty(othrElems.val()))sum +=
	// parseFloat(othrElems.val());
	// }//end of for
	// document.getElementById('txtTotSrcOfIncmSpsIncr').value = sum;

	// }//end of if

	// added by johnson on 26052015
	if (type == tot_SrcOfIncm.SRCOFINCM_SPS_PRD) {
		for ( var tot in tot_SrcOfIncm.tot_SrcOfIncm_Sps_Prd) {
			var id = tot_SrcOfIncm.tot_SrcOfIncm_Sps_Prd[tot], othrElems = $('#'
					+ id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtTotSrcOfIncmSpsPrd').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtTotSrcOfIncmSpsPrd').value = '';
	}// end of if

	// CPF
	if (type == tot_CPF.SUMOF_CPF_SELFCR) {
		for ( var tot in tot_CPF.tot_CPFSelf_CurrBal) {
			var id = tot_CPF.tot_CPFSelf_CurrBal[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldTotCPFSelfCurrBal').value = sum
				.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotCPFSelfCurrBal').value = '';
	}// end of if

	if (type == tot_CPF.SUMOF_CPF_SELFAC) {
		for ( var tot in tot_CPF.tot_CPFSelf_AnnContr) {
			var id = tot_CPF.tot_CPFSelf_AnnContr[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldTotCPFSelfAnnCtr').value = sum
				.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotCPFSelfAnnCtr').value = '';
	}// end of if

	if (type == tot_CPF.SUMOF_CPF_SPSCR) {
		for ( var tot in tot_CPF.tot_CPFSps_CurrBal) {
			var id = tot_CPF.tot_CPFSps_CurrBal[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldTotCPFSpsCurrBal').value = sum
				.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotCPFSpsCurrBal').value = '';
	}// end of if

	if (type == tot_CPF.SUMOF_CPF_SPSAC) {
		for ( var tot in tot_CPF.tot_CPFSps_AnnContr) {
			var id = tot_CPF.tot_CPFSps_AnnContr[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldTotCPFSpsAnnCtr').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotCPFSpsAnnCtr').value = '';
	}// end of if

	// financial Liablities
	if (type == tot_FincLiab.SUMOF_FINLIAB_SELF) {
		for ( var tot in tot_FincLiab.tot_FincLiab_Self) {
			var id = tot_FincLiab.tot_FincLiab_Self[tot], othrElems = $('#'
					+ id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldTotFincLiabSelf').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotFincLiabSelf').value = '';
	}// end of if

	if (type == tot_FincLiab.SUMOF_FINLIAB_SPS) {
		for ( var tot in tot_FincLiab.tot_FincLiab_Sps) {
			var id = tot_FincLiab.tot_FincLiab_Sps[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldTotFincLiabSps').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotFincLiabSps').value = '';
	}// end of if

	/* CASH ASSET */

	if (type == tot_CashAsst.SUMOF_CASHASST_SELF) {
		for ( var tot in tot_CashAsst.tot_CashAsstSelf) {
			var id = tot_CashAsst.tot_CashAsstSelf[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldTotCashAsstSelf').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotCashAsstSelf').value = '';
	}// end of if

	if (type == tot_CashAsst.SUMOF_CASHASST_SPS) {
		for ( var tot in tot_CashAsst.tot_CashAsstSps) {
			var id = tot_CashAsst.tot_CashAsstSps[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldTotCashAsstSps').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotCashAsstSps').value = '';
	}// end of if

	if (type == tot_CashAsst.SUMOF_CASHASST_JOIN) {
		for ( var tot in tot_CashAsst.tot_CashAsstJoin) {
			var id = tot_CashAsst.tot_CashAsstJoin[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for
		document.getElementById('txtFldTotCashAsstJoin').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldTotCashAsstJoin').value = '';
	}// end of if

	/* OTHER ASSET */

	if (type == tot_OthrAsst.SUMOF_OTHASST_SELF) {
		for ( var tot in tot_OthrAsst.tot_OthrAsstSelf) {
			var id = tot_OthrAsst.tot_OthrAsstSelf[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldOATotSelf').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldOATotSelf').value = '';
	}// end of if

	if (type == tot_OthrAsst.SUMOF_OTHASST_SPS) {
		for ( var tot in tot_OthrAsst.tot_OthrAsstSps) {
			var id = tot_OthrAsst.tot_OthrAsstSps[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldOATotSps').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldOATotSps').value = '';
	}// end of if

	if (type == tot_OthrAsst.SUMOF_OTHASST_JOIN) {
		for ( var tot in tot_OthrAsst.tot_OthrAsstJoin) {
			var id = tot_OthrAsst.tot_OthrAsstJoin[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for
		document.getElementById('txtFldOATotJoint').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldOATotJoint').value = '';
	}// end of if

	if (type == tot_OthrAsst.SUMOF_OTHASST_LOAN) {
		for ( var tot in tot_OthrAsst.tot_OthrAsstLoan) {
			var id = tot_OthrAsst.tot_OthrAsstLoan[tot], othrElems = $('#' + id);
			if (elemId == id)
				if (!isEmpty(elemObj.value))
					sum += parseFloat(elemObj.value);
			if (elemId != 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
			if (elemId == 'null')
				if (elemId != id)
					if (!isEmpty(othrElems.val()))
						sum += parseFloat(othrElems.val());
		}// end of for

		document.getElementById('txtFldOATotLoans').value = sum.toFixed(4);
		if (sum == zeroVal)
			document.getElementById('txtFldOATotLoans').value = '';
	}// end of if

}// end of calcSum

function getrdIntprtFlg(selObj) {

//	kycLanguage
//	kycInterprtDets


	var intrName = document.getElementById('intprtName'),
		intrNric = document.getElementById('intprtNric'),
		intrContNo = document.getElementById('intprtContact'),
		intrRel = document.getElementById('intprtRelat'),
		intrUpload = document.getElementById('fileIntrPrtAttchCopy');

	if (selObj == "Y") {

		$(".kycInterprtDets").prop("disabled",false);
		$(".kycInterprtDets").prop("readOnly",false);
		$(".kycInterprtDets").addClass("writeModeTextbox");

		/*intrName.readOnly = false;
		intrNric.readOnly = false;
		intrContNo.readOnly = false;
		intrRel.disabled = false;
		intrUpload.disabled = false;


		intrName.className = "writeModeTextbox";
		intrNric.className = "writeModeTextbox";
		intrContNo.className = "writeModeTextbox";*/

		document.getElementById('divIntrLblName').style.color = 'maroon';
		document.getElementById('divIntrLblNRIC').style.color = 'maroon';
		document.getElementById('divIntrLblContNo').style.color = 'maroon';
		document.getElementById('divIntrLblRel').style.color = 'maroon';
		document.getElementById('divIntrLblSign').style.color = 'maroon';
		document.getElementById('divIntrLblUpload').style.color = 'maroon';

	} else {

		intrName.value = "";
		intrNric.value = "";
		intrContNo.value = "";
		intrRel.value = "";
		intrUpload.value = "";

		/*intrName.readOnly = true;
		intrNric.readOnly = true;
		intrContNo.readOnly = true;
		intrRel.disabled = true;
		intrUpload.disabled = true;

		intrName.className = "readOnlyText";
		intrNric.className = "readOnlyText";
		intrContNo.className = "readOnlyText";*/

		$(".kycInterprtDets").prop("disabled",true);
		$(".kycInterprtDets").prop("readOnly",true);
		$(".kycInterprtDets").addClass("readOnlyText");

		document.getElementById('divIntrLblName').style.color = 'black';
		document.getElementById('divIntrLblNRIC').style.color = 'black';
		document.getElementById('divIntrLblContNo').style.color = 'black';
		document.getElementById('divIntrLblRel').style.color = 'black';
		document.getElementById('divIntrLblSign').style.color = 'black';
		document.getElementById('divIntrLblUpload').style.color = 'black';

	}
}

function loadHSPExistingPlan(tblid) {

	var custId = fnaForm.txtFldCustId.value;

	var tblobj = document.getElementById("fnahlthinsTbl");
	var tbodyobj = tblobj.tBodies[0];

	if (!loadExistHsaPolFlg) {

		// if(window.confirm("Do you want to load the existing policy(FPMS)
		// details?")){

		var parameter = "dbcall=FPMS_POLICY_HSP&txtFldCustId=" + custId;

		var respText = ajaxCall(parameter);

		var retval = respText;// eval('(' + respText + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			for ( var tab in tabdets) {

				if (tabdets.hasOwnProperty(tab)) {
					var key = tab;
					var value = tabdets[tab];

					if (value.length == 0) {
						alert("No policy record found!");
						// return false;
					}

					if (value.length > 0) {
						// if(key == "MAST_FPMSPOLDETS_ARR"){

						var populatedAppIdsHSP = [];

						var rlen = tbodyobj.rows.length;
						if (rlen > 0) {
							for (var rc = 0; rc < rlen; rc++) {
								populatedAppIdsHSP[populatedAppIdsHSP.length++] = tbodyobj.rows[rc].cells[1].childNodes[1].value;
							}
						}

						var confirmCnt = 0;

						for ( var val in value) {

							var prdtdata = value[val];

							if (checkValueInArray(populatedAppIdsHSP,
									prdtdata["txtFldBasOrRidId"])) {
								confirmCnt++;
							}
						}

						if (value.length != confirmCnt) {

							if (window
									.confirm("Do you want to load the existing policy(FPMS) details?")) {

								for ( var val in value) {

									var prdtdata = value[val];

									if (!checkValueInArray(populatedAppIdsHSP,
											prdtdata["txtFldBasOrRidId"])) {
										// anyNewPolFlg=true;

										// if(anyNewPolFlg){

										// if(window.confirm("Do you want to
										// load the existing policy(FPMS)
										// details?")){

										// for(var val in value){

										// var prdtdata = value[val];

										fnaHlthInsAddRow('fnahlthinsTbl', false);

										var rowlenobj = tbodyobj.rows.length;
										tbodyobj.rows[rowlenobj - 1].cells[0].childNodes[0].value = prdtdata["txtFldPolCateg"];
										tbodyobj.rows[rowlenobj - 1].cells[1].childNodes[0].value = prdtdata["txtFldAssuredName"];
										tbodyobj.rows[rowlenobj - 1].cells[2].childNodes[0].value = prdtdata["txtFldPlanName"];
										tbodyobj.rows[rowlenobj - 1].cells[3].childNodes[0].value = prdtdata["txtFldAmount"];
										tbodyobj.rows[rowlenobj - 1].cells[4].childNodes[0].value = prdtdata["txtFldExpiry"];
										tbodyobj.rows[rowlenobj - 1].cells[5].childNodes[0].value = prdtdata["txtFldAnnualPremium"];
										// tbodyobj.rows[rowlenobj-1].cells[1].childNodes[1].value=prdtdata["txtFldAppId"];
										tbodyobj.rows[rowlenobj - 1].cells[1].childNodes[1].value = prdtdata["txtFldBasOrRidId"];

										tbodyobj.rows[rowlenobj - 1].cells[0].childNodes[0].readOnly = true;
										tbodyobj.rows[rowlenobj - 1].cells[1].childNodes[0].readOnly = true;
										tbodyobj.rows[rowlenobj - 1].cells[2].childNodes[0].readOnly = true;
										tbodyobj.rows[rowlenobj - 1].cells[3].childNodes[0].readOnly = true;
										tbodyobj.rows[rowlenobj - 1].cells[4].childNodes[0].readOnly = true;
										tbodyobj.rows[rowlenobj - 1].cells[5].childNodes[0].readOnly = true;

										// loadExistHsaPolFlg = true;
										// }
										// return false;
										// }

										// }
									}

								}
								return false;
							}

						}
						// }
						return true;
						// }
					}
				}
			}
		}

		// }
	}

	return true;
}

function enabOrDisabTaxResid(tblId) {

	var tbl = document.getElementById(tblId), tblBody = tbl.tBodies[0], tblLen = tblBody.rows.length;

	if (tblLen <= 0) {

		$('#chkFatcaNoContFlg').prop({'disabled' : false,'checked' : true});
	} else if (tblLen > 0) {
		$('#chkFatcaNoContFlg').prop({'disabled' : true,'checked' : false});
	}
}// end of enabDisabTaxResid

function copyValuesClntDecltoPers() {
//	alert("copyValuesClntDecltoPers")

	// THis is for Simplified version to ALL version

	// Page3 = page14

	/*
	 * txtFldFnaClntName = dfSelfName selFnaClntSex = dfSelfGender
	 * txtFldFnaClntDOB = dfSelfDob txtFldFnaClntNric = dfSelfNric
	 * selFnaClntMartSts = dfSelfMartsts txtFldFnaClntHome =
	 * dfSelfHome//home txtFldFnaClntMobile =
	 * txtFldCDClntHndPhne//mobile txtFldFnaClntOffice = dfSelfOffice//office
	 * txtFldFnaClntEmail = dfSelfPersemail//email-p txtFldFnaClntOccp =
	 * dfSelfOccpn txtFldFnaClntHomeAddr = dfSelfHomeaddr
	 */

	var sourceElemArr = [ "txtFldFnaClntName", "selFnaClntSex",
			"txtFldFnaClntDOB", "txtFldFnaClntNric", "selFnaClntMartSts",
			"txtFldFnaClntHome", "txtFldFnaClntMobile", "txtFldFnaClntOffice",
			"txtFldFnaClntEmail", "txtFldFnaClntOccp", "txtFldFnaClntHomeAddr",
			"txtFldFnaSpsName", "selFnaSpsSex", "txtFldFnaSpsDob",
			"txtFldFnaSpsNric", "selFnaSpsMartSts"// ,"dfSpsHome"
			, "txtFldFnaSpsHP", "txtFldFnaSpsEmail", "txtFldFnaSpsOccpn",
			"txtFldFnaSpsOffice" ];// ,,"dfSpsHomeaddr"

	var destElemArr = [ "dfSelfName", "dfSelfGender",
			"dfSelfDob", "dfSelfNric", "dfSelfMartsts",
			"dfSelfHome", "dfSelfMobile", "dfSelfOffice",
			"dfSelfPersemail", "dfSelfOccpn", "dfSelfHomeaddr",
			"dfSpsName", "dfSpsGender", "dfSpsDob",
			"dfSpsNric", "dfSpsMartsts",// "txtFldCDSpsCntDets",
			"dfSpsHp", "dfSpsPersemail", "dfSpsOccpn",
			"dfSpsOffice" ];// ,

	for (var elem = 0; elem < sourceElemArr.length; elem++) {

		var srcElem = eval('fnaForm.' + sourceElemArr[elem]);
		var destElem = eval('fnaForm.' + destElemArr[elem]);

		srcElem.value = destElem.value;

	}

	// 2015_05_25
	var multiElem_src = [ "dfSelfName", "dfSpsName" ];
	var multiElem_dest = [ "txtFldClientName", "txtFldSpouseName" ];

	for (var el = 0; el < multiElem_dest.length; el++) {

		for (var ele = 0; ele < document.getElementsByName(multiElem_dest[el]).length; ele++) {
			document.getElementsByName(multiElem_dest[el])[ele].value = document.getElementById(multiElem_src[el]).value;
		}
	}

}

// added by johnson on 18062015
function enableInsPolTxtArea(selObj, txtFldId) {

	var selObjVal = selObj.options[selObj.selectedIndex].value;

	if (!isEmpty(selObjVal) && selObjVal == 'Y') {

		document.getElementById(txtFldId).className = '';
		document.getElementById(txtFldId).readOnly = false;
		document.getElementById(txtFldId).focus();

	} else if (isEmpty(selObjVal) || selObjVal == 'N') {

		document.getElementById(txtFldId).className = 'readOnlyText';
		document.getElementById(txtFldId).readOnly = true;
		document.getElementById(txtFldId).value = '';

	} // end of if
}// end of enableInsPolTxtArea

function enablOrDisblCKAContrls(radObj, txtFldId) {

	if (radObj.checked && radObj.value == 'Y') {

		document.getElementById(txtFldId).className = '';
		document.getElementById(txtFldId).readOnly = false;
		document.getElementById(txtFldId).focus();
		document.getElementById(txtFldId).style.width = '100%';
		document.getElementById(txtFldId).style.borderStyle = 'inset';

	} else if (radObj.checked && radObj.value == 'N') {

		document.getElementById(txtFldId).className = 'readOnlyText';
		document.getElementById(txtFldId).readOnly = true;
		document.getElementById(txtFldId).value = '';
	}// end of if

	if (radObj.name == 'educatFlg') {
		var eduDets = document.getElementsByName('educatDet');
		for (var edu = 0; edu < eduDets.length; edu++) {
			if (radObj.value == 'Y') {
				eduDets[edu].disabled = false;
			}
			if (radObj.value == 'N') {
				eduDets[edu].disabled = true;
				eduDets[edu].checked = false;
			}// end of if

		}// end of for

	}// end of if

	if (radObj.name == 'investExpFlg') {

		if (radObj.checked && radObj.value == 'Y') {

			document.getElementById('fna_investCisFlg').disabled = false;
			document.getElementById('fna_investIlpFlg').disabled = false;

		} else if (radObj.checked && radObj.value == 'N') {

			document.getElementById('fna_investCisFlg').checked = false;
			document.getElementById('fna_investCisFlg').disabled = true;

			document.getElementById('fna_investIlpFlg').checked = false;
			document.getElementById('fna_investIlpFlg').disabled = true;

		}// end of if
	}// end of if

	if (radObj.name == 'workExpFlg') {

		if (radObj.checked && radObj.value == 'Y') {

			document.getElementById('fna_workExpDevprdt').disabled = false;
			document.getElementById('fna_workExpInvprdt').disabled = false;
			document.getElementById('fna_workExpAcc').disabled = false;

		} else if (radObj.checked && radObj.value == 'N') {

			document.getElementById('fna_workExpDevprdt').checked = false;
			document.getElementById('fna_workExpDevprdt').disabled = true;

			document.getElementById('fna_workExpInvprdt').checked = false;
			document.getElementById('fna_workExpInvprdt').disabled = true;

			document.getElementById('fna_workExpAcc').checked = false;
			document.getElementById('fna_workExpAcc').disabled = true;

		}// end of if

	}// end of if

}// end of enablOrDisblContrls

function validateInsPol() {

	var selObj = document.getElementById('fna_selFnaPPInsPolFlg');
	selObj.options[selObj.selectedIndex].value;

	if (!isEmpty(selObj.options[selObj.selectedIndex])
			&& selObj.options[selObj.selectedIndex].value == 'Y') {
		if (!checkMandatoryField(document.getElementById('selFnaPPInsPolRem'),
				HLTH_INS_POL))
			return;
	}// end of if

	return true;

}// end of

function validateMailAddr() {

	var mailAddrRadio = document.getElementsByName('dfSelfMailaddrflg');
	var mailAddrResn = document.getElementById('radCDClntMailngAddr_OTH');

	if (mailAddrRadio && mailAddrRadio[2].checked == true) {
		if (!checkMandatoryField(document.getElementById('dfSelfMailaddr'), MAIL_ADDR_SELF))
			return;

		var spsName = fnaForm.dfSpsName.value;
		if(!isEmpty(spsName)){
			if (!checkMandatoryField(document.getElementById('dfSpsMailaddr'), MAIL_ADDR_SPS))
				return;
		}
	}// end of if

	// comment has opened for mandatory validation of
	// mailing address of others
	// by johnson 20160303
	if (mailAddrResn && mailAddrResn.checked == true) {
		if (!checkMandatoryField(document.getElementById('txtFldCDMailAddOth_032018'),
				MAIL_ADDR_RESN))
			return;
	}// end of if

	return true;
}// end of validateMailAddr

function validProfSection(){

//	validProfSection
//	alert("validProfSection")

	if(document.getElementsByName("txtFldDfSelfAgenext_032018")[1].checked == true){

		var langProfObj = document.getElementsByName("txtFldDfSelfEngProf");

		if(!langProfObj[0].checked && !langProfObj[1].checked){
			alert(SELF_LANG_PROFLEVEL);
			setPageFocus(14, FNA_FORMTYPE);
			return false;
		}

		if (!checkMandatoryField(document.getElementById('dfSelfEdulevel'),SELF_PROF_EDULEVEL, 14, FNA_FORMTYPE))
			return;

	}


	if(document.getElementsByName("txtFldDfSpsAgenext")[1].checked == true){

		var langProfObj = document.getElementsByName("txtFldDfSpsEngProf");

		if(!langProfObj[0].checked && !langProfObj[1].checked){
			alert(SPS_LANG_PROFLEVEL);
			setPageFocus(14, FNA_FORMTYPE);
			return false;
		}

		if (!checkMandatoryField(document.getElementById('dfSpsEdulevel'),SPS_PROF_EDULEVEL, 14, FNA_FORMTYPE))
			return;

	}

	return true;
}

function validIntrPreFun(flag) {



	var intrPreRad = document.getElementsByName('cdIntrprtflg');
	var relshipSelObj = document.getElementById('intprtRelat');

	if (intrPreRad[0] && intrPreRad[0].checked) {

		var atLeast1Lang = false;
		$(".kycLanguage").each(function(){
			if($(this).prop("checked")){
				atLeast1Lang=true;
			}
		});

		

		if(flag){

//			if (!checkMandatoryField(document.getElementById('intprtName'),INTEPRE_NAME, 13, FNA_FORMTYPE)){
//				return;
//			}
			
			if(!atLeast1Lang){
//				alert("Select Language Use in Client's Declaration Section");
//				setPageFocus(13,FNA_FORMTYPE);
//				scrollToView("tblClientDeclLanguage");
	//
//				return false;

				changedarrGlobal3.push("Select Any 1 Language Use in Personal Details Section^"+document.getElementById("htflaneng"));

			}

			if(isEmpty(document.getElementById('intprtName').value)){
				changedarrGlobal3.push(INTEPRE_NAME+"^"+document.getElementById('intprtName'));
			}

//			if (!checkMandatoryField(document.getElementById('intprtNric'),INTEPRE_NRIC, 13, FNA_FORMTYPE))
//				return;

//			if(isEmpty(document.getElementById('intprtNric').value)){
//				changedarrGlobal3.push(INTEPRE_NRIC+"^"+document.getElementById('intprtNric'));
//			}

//			if (!checkMandatoryField(document.getElementById('intprtContact'),	INTEPRE_CONTACT, 13, FNA_FORMTYPE))
//				return;

			if(isEmpty(document.getElementById('intprtContact').value)){
				changedarrGlobal3.push(INTEPRE_CONTACT+"^"+document.getElementById('intprtContact'));
			}

//			if (!checkMandatoryField(relshipSelObj,INTEPRE_SEL_RELSHIP, 13, FNA_FORMTYPE))
//				return;

			if(isEmpty(relshipSelObj.value)){
				changedarrGlobal3.push(INTEPRE_SEL_RELSHIP+"^"+relshipSelObj);
			}

			// if(!checkMandatoryField(document.getElementById('fileIntrPrtAttchCopy'),INTEPRE_SEL_UPLOAD,13,FNA_FORMTYPE))
			// return true;

		}

	}// end of if

	return true;
}// end of validIntrPreFun

function validAdvAndRecmChkboxes() {

	// var advAndRemmRad = document.getElementsByName('swrepConflg')[0];
	//
	// if (advAndRemmRad && advAndRemmRad.checked){
	// if
	// (!checkMandatoryField(document.getElementById('swrepConfDets'),ADV_RECMM_Q1,15,FNA_FORMTYPE))return;
	// }

	var famIncmProtect = document.getElementById('htfarfamincpro');
	var medIncmProtect = document.getElementById('htfardisincpro');
	var ednIncmProtect = document.getElementById('htfarednincpro');
	var invIncmProtect = document.getElementById('htfarinv');
	var retIncmProtect = document.getElementById('htfarret');
	var othIncmProtect = document.getElementById('htfarother');

	if (!(famIncmProtect).checked && !(medIncmProtect).checked
			&& !(ednIncmProtect).checked && !(invIncmProtect).checked
			&& !(retIncmProtect).checked && !(othIncmProtect).checked) {

//		alert(ADNT_INCMPROTECT_VAL_MSG);
//		setPageFocus(14, FNA_FORMTYPE, custLobArr);
//		famIncmProtect.focus();
//		return false;
		changedarrGlobal7.push(ADNT_INCMPROTECT_VAL_MSG);
	}

	return true;
}// end of validAdvAndRecmChkboxes

function validateCKAKnwldgeAcqrd() {

	var radEduQual = document.getElementById('fna_educatFlgY');
	var radFinCrse = document.getElementById('fna_financeCrseFlgY');
	var radInvstExp = document.getElementById('fna_investExpFlgY');
	var radWrkExp = document.getElementById('fna_workExpFlgY');

	if (radEduQual && radEduQual.checked == true) {

		var eduDets = document.getElementsByName('educatDet'), eduFlag = false;

		for (var edu = 0; edu < eduDets.length; edu++)
			if (eduDets[edu] && eduDets[edu].checked == true)
				eduFlag = true;

		if (!eduFlag) {
			alert('Mention the field of Higer Qualification');
			return;
		}
		if (!checkMandatoryField(document.getElementById('educatInstitution'),
				CKA_EDU_DETS))
			return;
	}// end of if

	if (radFinCrse && radFinCrse.checked == true)
		if (!checkMandatoryField(document.getElementById('financeInstitution'),
				CKA_FIN_DETS))
			return;

	if (radInvstExp && radInvstExp.checked == true) {

		if (document.getElementById('fna_investCisFlg').checked == false
				&& document.getElementById('fna_investIlpFlg').checked == false) {
			alert("Select any of the investment product");
			return;
		}// end of if

		if (!checkMandatoryField(document.getElementById('investFinanceInst'),
				CKA_INVST_DETS))
			return;
	}// end of if

	if (radWrkExp && radWrkExp.checked == true) {
		if (document.getElementById('fna_workExpDevprdt').checked == false
				&& document.getElementById('fna_workExpInvprdt').checked == false
				&& document.getElementById('fna_workExpAcc').checked == false) {
			alert("Select any areas in which you have experience");
			return;
		}// end of if

		if (!checkMandatoryField(document.getElementById('workOrga'),
				CKA_WRK_EXP_DETS))
			return;
	}// end of if

	return true;
}// end of validateCKAKnwldgeAcqrd

function blockSpsForCompany() {

	var spsElemArr = [
	// page3
	"txtFldFnaSpsName", "selFnaSpsSex", "txtFldFnaSpsDob", "txtFldFnaSpsNric",
			"selFnaSpsMartSts", "dfSpsHome", "dfSpsHomeaddr",
			"txtFldFnaSpsHP", "txtFldFnaSpsEmail", "txtFldFnaSpsOccpn",
			"txtFldFnaSpsOffice", "selFnaSpsNatly", "selFnaSpsEmpSts",
			"chkFnaSpsSmoke", "selFnaSpsMartSts",

			// page3- un-related to company
			"selFnaClntMartSts",

			// page5
			'txtFldFnaExpRentSps', 'txtFldFnaExpUtilSps',
			'txtFldFnaExpGrocSps', 'txtFldFnaExpEatSps',
			'txtFldFnaExpClothSps', 'txtFldFnaExpTranSps',
			'txtFldFnaExpMedSps', 'txtFldFnaExpPersSps',
			'txtFldFnaExpDepntSps', 'txtFldFnaExpTaxSps', 'txtFldFnaExpEntSps',
			'txtFldFnaExpFesSps', 'txtFldFnaExpVocaSps',
			'txtFldFnaExpChrtySps', 'txtFldFnaExpPropSps',
			'txtFldFnaExpVehiSps', 'txtFldFnaExpInsSps', 'txtFldFnaExpOthSps',

			'txtFldFnaExpRentFam', 'txtFldFnaExpUtilFam',
			'txtFldFnaExpGrocFam', 'txtFldFnaExpEatFam',
			'txtFldFnaExpClothFam', 'txtFldFnaExpTranFam',
			'txtFldFnaExpMedFam', 'txtFldFnaExpPersFam',
			'txtFldFnaExpMaintFam', 'txtFldFnaExpDomsFam',
			'txtFldFnaExpChildFam', 'txtFldFnaExpTaxFam', 'txtFldFnaExpEntFam',
			'txtFldFnaExpFesFam', 'txtFldFnaExpVocaFam',
			'txtFldFnaExpChrtyFam', 'txtFldFnaExpPropFam',
			'txtFldFnaExpVehiFam', 'txtFldFnaExpInsFam', 'txtFldFnaExpOthFam',

			'txtFldFnaContNeedSps', 'txtFldFnaContDeathSps',
			'txtFldFnaContTPDYrSps', 'txtFldFnaContRemSps',
			'txtFldFnaContRBSps', 'txtFldFnaContWardSps', 'selFnaContHospSps',
			'txtFldFnaContNeedFam', 'txtFldFnaContDeathFam',
			'txtFldFnaContTPDYrFam', 'txtFldFnaContRemFam',

			// page6
			"txtFldFnaContAnIncomDthSps", "txtFldFnaContAnIncomTpdSps",
			"txtFldFnaContAnIncomMedSps", "txtFldFnaContMISps",
			"txtFldFnaContLumpIllSps", "txtFldFnaContOthIllSps",
			"txtFldFnaContContPrctSps", "txtFldFnaContLiaPrctSps",
			"txtFldFnaContLastExpSps",
			// page6 SrcOfIncm
			"txtFldIncSrcEmpSps", "txtFldIncSrcIncrSps",
			"txtFldIncSrcPerdrSps", "txtFldIncSrcAddWageSps",
			"txtFldIncSrcNEAmtSps", "txtFldIncSrcNEIncSps",
			"txtFldIncSrcNEPrdSps", "txtFldIncSrcDivSps",
			"txtFldIncSrcDivIncSps", "txtFldIncSrcDivIncSps",
			"txtFldIncSrcDivPrdSps", "txtFldIncSrcRentSps",
			"txtFldIncSrcReIncSps", "txtFldIncSrcRePrdSps",
			"txtFldIncSrcOthAmtSps", "txtFldIncSrcOthIncrSps",
			"txtFldIncSrcOthPrdSps",

			// page7
			// CPF
			"txtFldCPFOrdAcCurSps", "txtFldCPFOrdAcContSps",
			"txtFldCPFSplAcCurSps", "txtFldCPFSplAcContSps",
			"txtFldCPFMedCurSps", "txtFldCPFMedContSps",
			// Fin Liab
			"txtFldFLOverdraftSps", "txtFldFLShortLoanSps", "txtFldFLTaxSps",
			"txtFldFLOthersSps",
			// CA
			"txtFldCARoiSps", "txtFldCARoiFam",
			// Retr Plan Sps
			"fna_txtFldRPIntAgeSps", "txtFldRPProjAgeSps",
			"fna_txtFldRPLivingYrSps", "txtFldRPProjRoiSps",
			"txtFldRPLivingAmtRetSps", "txtFldRPLivingYrRetSps",
			"txtFldRPLumpAmtRetSps", "txtFldRPFv1AmtSps", "txtFldRPFv1YrSps",
			"txtFldRPFv2AmtSps", "txtFldRPFv2YrSps", "txtFldRPMssAmtSps",
			"txtFldRPMssYrSps", "txtFldRPAvaIncRetSps", "txtFldRPAvaYrRetSps",
			"txtFldRPOthIncRetSps", "txtFldRPOthYrRetSps",
			"txtFldRPAssetValSps",
			// Retr Plan Fam
			"fna_txtFldRPLivingYrFam", "txtFldRPProjRoiFam",
			"txtFldRPLivingAmtRetFam", "txtFldRPLivingYrRetFam",
			"txtFldRPLumpAmtRetFam", "txtFldRPFv1AmtFam", "txtFldRPFv1YrFam",
			"txtFldRPFv2AmtFam", "txtFldRPFv2YrFam", "txtFldRPMssAmtFam",
			"txtFldRPMssYrFam", "txtFldRPAvaIncRetFam", "txtFldRPAvaYrRetFam",
			"txtFldRPOthIncRetFam", "txtFldRPOthYrRetFam",

			// page8
			// Invst Sps
			"txtFldINVBondCurSps", "txtFldINVBondRegSps", "txtFldINVIlpCurSps",
			"txtFldINVIlpRegSps", "txtFldINVShrCurSps", "txtFldINVShrRegSps",
			"txtFldINVOthCurSps", "txtFldINVOthRegSps",
			// Invst Jnt
			"txtFldINVBondCurFam", "txtFldINVBondRegFam", "txtFldINVIlpCurFam",
			"txtFldINVIlpRegFam", "txtFldINVShrCurFam", "txtFldINVShrRegFam",
			"txtFldINVOthCurFam", "txtFldINVOthRegFam",
			// Cash Asst Sps
			"txtFldCAFdSps", "txtFldCASavSps", "txtFldCACashSps",
			"txtFldCASrsSps",
			// Cash Asst Jnt
			"txtFldCAFdJoint", "txtFldCASavJoint", "txtFldCACashJoint",
			// Cash Asst RP
			"txtFldCAFdRp", "txtFldCASavRp", "txtFldCACashRp", "txtFldCASrsRp",
			// Oth Asst Sps
			"txtFldOAPersSps", "txtFldOAClubSps", "txtFldOABusiSps",
			"txtFldOAOthSps",
			// Oth Asst Jnt
			"txtFldOAPersJoint", "txtFldOAClubJoint", "txtFldOABusiJoint",
			"txtFldOAOthJoint",

			// page13
			// Need Anals Sumry Sps
			"txtFldOARFamIncSps", "txtFldOARDisIncSps", "txtFldOARIllNeedSps",
			"fna_txtFldOARHospTypeSps", "txtFldOARWardTypeSps",
			"txtFldOARExsHospSps", "txtFldOARExsPolSps", "txtFldOARRetFundSps",
			"txtFldOARChildEdnSps", "txtFldOARCurSavSps", "txtFldOAREstSavSps",
			"txtFldOARTotTgtSps", "txtFldOAROthPurSps", "txtFldOARSavNeedSps",

			// page14
			"dfSpsName", "dfSpsGender", "dfSpsDob",
			"dfSpsNric", "dfSpsMartsts", "txtFldCDSpsCntDets",
			"dfSpsHp", "dfSpsPersemail", "dfSpsOccpn",
			"dfSpsOffice", "dfSpsCompname", "dfSpsAnnlincome"

	];

	for (var el = 0; el < spsElemArr.length; el++) {

		var destElem = eval('fnaForm.' + spsElemArr[el]);
		if (destElem) {

			if (destElem.type == "text") {
				destElem.readOnly = true;
				destElem.className = "readOnlyText";
			} else { // if(destElem.type == "select-one")
				destElem.disabled = true;
			}

		}

	}

	$("#page3_spsdob").hide();
	$("#imgPage14SpsDob").hide();
	$("#fnaDepnAddRow").hide();
	$("#fnaDepnDelRow").hide();
	$("#fnaContDepAddRow").hide();
	$("#fnaContDepDelRow").hide();

}

function enableAllFields() {

	for (var i = 0; i < fnaForm.elements.length; i++) {
		fnaForm.elements[i].disabled = false;
	}
}

function getTotPages(lobArr, formType) {

	if (checkValueInArray(lobArr, LIFE_INS) && formType == SIMPLIFIED_VER) {
		totalpage = 11;
	} else if (!checkValueInArray(lobArr, LIFE_INS)
			&& formType == SIMPLIFIED_VER) {
		totalpage = 14;
	}
	if (checkValueInArray(lobArr, LIFE_INS) && formType == ALL_VER) {
		totalpage = 19;
	}
	if (!checkValueInArray(lobArr, LIFE_INS) && formType == ALL_VER) {
		totalpage = 22;
	}

}

function LoadPageWithPaging(formType) {

	var totalpage, alltotalpage;

	if (formType == ALL_VER) {
		totalpage = pageDetsArr[0].ALL_VER;
		alltotalpage = pageDetsArr[0].TOTAL;
	} else {

		totalpage = pageDetsArr[1].SIMPLIFIED_VER;

		if (!checkValueInArray(custLobArr, LIFE_INS)) {
			totalpage = 14;
		}
		alltotalpage = pageDetsArr[1].TOTAL;
	}

	if (formType == SIMPLIFIED_VER) {

		$("#paginationdiv").hide();
		$("#simplepaginationdiv").show();

		$('.simplepagination').jqPagination({

			link_string : '/?page={page_number}',
			max_page : totalpage,
			paged : function(page) {
//page is jqpagination parameter; based on the parameter set the div content in kyctabs
				switch (page) {
					case 1: {page = 1;break;}//index page
					case 2: {page = 12;break;}/*Client's Declaration*/
					case 3: {page = 13;break;}/*Client's Declaration (AML)*/
					case 4: {page = 27;break;}/*Financial Wellness Review*/
					case 5: {page = 10;break;}/*Client's Risk Preference &amp; Investment Objectives*/
					case 6: {page = 14;break;}/*Product & Recommendations (New Purchase &amp; Top Up)*/
					case 7: {page = 25;break;}/*Advice &amp; Recommendations (New Purchase &amp; Top Up)*/
					case 8: {page = 15;break;}/*Advice &amp; Recommendations (Switching &amp; Replacement)*/
					case 9:{page = 16;break;}/*Advice &amp; Recommendations (Switching &amp; Replacement)*/
					case 10:{page = 17;break;}/*Advice &amp; Recommendations*/
					case 11: {page = 20;break;}/*Client's Tax Residency Declaration*/					
					/*case 12:{page = 28;break;}*//*Disclosure Requirements and Checklist (Investment / Life and Health Insurance)*/
					case 12:{page = 31;break;}/*Client's Signature Form*/
					case 13:{page = 18;break;}/*Client's Declaration &amp; Acknowledgement*/
					case 14:{page = 19;break;}/*Representative's Declaration*/
					/*case 15:{page = 26;break;}Client's Feedback Form*/
				}

				for (var p = 1; p <= alltotalpage; p++) {
					$("#page" + p).hide();
				}
				$("#page23").hide();// EMpty page for simplified version
				$("#page31").hide();//ClientSignature Form
				$("#tabs").tabs("option", "active", 1);
				$("#page" + page).show();
				$('#tabs-template').animate({//added by kavi 16/03/2018 with animation scroll
		            scrollTop: 0
		        },150);
				/*scrollTop(0);*/
			}
		});

		$('.simplepagination').jqPagination('option', 'current_page', 1);

	}

	if (formType == ALL_VER) {

		$("#paginationdiv").show();
		$("#simplepaginationdiv").hide();

		$('.pagination').jqPagination({

			link_string : '/?page={page_number}',
			max_page : totalpage,
			paged : function(page) {

				// commended by johnson 20160311
				// if (!allPageValidation()) {
				// return;
				// }

				for (var p = 1; p <= alltotalpage; p++) {
					$("#page" + p).hide();
				}
				$("#page23").hide();// EMpty page for simplified version
				$("#page31").hide();//ClientSignature Form
				$("#tabs").tabs("option", "active", 1);
				$("#page" + page).show();

			}
		});

		$('.pagination').jqPagination('option', 'current_page', 1);

	}

	$("#loadingMessage").hide();

}

function logoutKyc() {

	if (window.confirm("Do you want to close?")) {
		// <%if (request.getSession(false) == null)
		// //response.sendRedirect(response.encodeRedirectURL("Login.jsp"));
		// %>

		// <%//session.invalidate();%>
		window.close();
		return true;
	} else {
		return false;
	}

}
function callWebServices() {

	document.forms[0].action = baseUrl + "/avaservices/";
	document.forms[0].submit();
}

function callWebServicesClient() {
	document.forms[0].action = "FileUploadPre.action";
	document.forms[0].submit();
}

// function logoutKyc() {
//
// if (window.confirm("Do you want to close?")) {
// // <%if (request.getSession(false) == null)
// // //response.sendRedirect(response.encodeRedirectURL("Login.jsp"));
// // %>
//
// // <%//session.invalidate();%>
// window.close();
// return true;
// } else {
// return false;
// }
//
// }

function exportFnaDets() {

	// var fnaForm = document.forms[0];

	var clntName = document.getElementById('dfSelfName').value;
	var clntSex = document.getElementById('dfSelfGender').value;
	var clntDOB = document.getElementById('dfSelfDob').value;
	var clntNRIC = document.getElementById('dfSelfNric').value;
	var clntNatn = document.getElementById('dfSelfNationality').value;
	var clntMrtSts = document.getElementById('dfSelfMartsts').value;
	var clntCntDets = document.getElementById('dfSelfHome').value;
	var clntHndPhne = document.getElementById('dfSelfMobile').value;
	var clntOffPhne = document.getElementById('dfSelfOffice').value;
	var clntEmail = document.getElementById('dfSelfPersemail').value;
	var clntAnnIncm = document.getElementById('dfSelfAnnlincome').value;
	var clntOccpn = document.getElementById('dfSelfOccpn').value;
	var clntEmplr = document.getElementById('dfSelfCompname').value;
	var clntAddr = document.getElementById('dfSelfHomeaddr').value;
	var clntHeight = document.getElementById('htxtFldClntHeight').value;
	var clntWeight = document.getElementById('htxtFldClntWeight').value;
	var clntRace = document.getElementById('htxtFldClntRace').value;

	var spsName = document.getElementById('dfSpsName').value;
	var spsSex = document.getElementById('dfSpsGender').value;
	var spsDOB = document.getElementById('dfSpsDob').value;
	var spsNRIC = document.getElementById('dfSpsNric').value;
	var spsNatn = document.getElementById('selFnaSpsNatly').value;
	var spsMrtSts = document.getElementById('dfSpsMartsts').value;
	var spsHome = document.getElementById('dfSpsHome').value;
	var spsHndPhne = document.getElementById('dfSpsHp').value;
	var spsOffCnt = document.getElementById('dfSpsOffice').value;
	var spsEmail = document.getElementById('dfSpsPersemail').value;
	var spsAnnIncm = document.getElementById('dfSpsAnnlincome').value;
	var spsOccpn = document.getElementById('dfSpsOccpn').value;
	var spsEmplr = document.getElementById('dfSpsCompname').value;
	var spsHomeAddr = document.getElementById('dfSpsHomeaddr').value;

	var tbl = document.getElementById('fnaartuplanTbl'),
	tblBody = tbl.tBodies[0], tblLen = tblBody.rows.length;

	var action = 'KycExportAction.action?';
	var param = '';
	var selfDets = 'dfSelfName=' + clntName + '&' + 'dfSelfGender='
			+ clntSex + '&' + 'dfSelfDob=' + clntDOB + '&'
			+ 'dfSelfNric=' + clntNRIC + '&' + 'dfSelfNationality='
			+ clntNatn + '&' + 'dfSelfMartsts=' + clntMrtSts + '&'
			+ 'dfSelfHome=' + clntCntDets + '&'
			+ 'dfSelfMobile=' + clntHndPhne + '&' + 'dfSelfOffice='
			+ clntOffPhne + '&' + 'dfSelfPersemail=' + clntEmail + '&'
			+ 'dfSelfAnnlincome=' + clntAnnIncm + '&'
			+ 'dfSelfOccpn=' + clntOccpn + '&' + 'dfSelfCompname='
			+ clntEmplr + '&' + 'dfSelfHomeaddr=' + clntAddr + '&'
			+ 'htxtFldClntHeight=' + clntHeight + '&' + 'htxtFldClntWeight='
			+ clntWeight + '&' + 'htxtFldClntRace=' + clntRace + '&';

	var spsDets = 'dfSpsName=' + spsName + '&' + 'dfSpsGender='
			+ spsSex + '&' + 'dfSpsDob=' + spsDOB + '&'
			+ 'dfSpsNric=' + spsNRIC + '&' + 'selFnaSpsNatly=' + spsNatn
			+ '&' + 'dfSpsMartsts=' + spsMrtSts + '&'
			+ 'dfSpsHome=' + spsHome + '&' + 'dfSpsHp='
			+ spsHndPhne + '&' + 'dfSpsOffice=' + spsOffCnt + '&'
			+ 'dfSpsPersemail=' + spsEmail + '&' + 'dfSpsAnnlincome='
			+ spsAnnIncm + '&' + 'dfSpsOccpn=' + spsOccpn + '&'
			+ 'dfSpsCompname=' + spsEmplr + '&' + 'dfSpsHomeaddr='
			+ spsHomeAddr;

	param += selfDets + spsDets + '&hfnaartuplanTblLen=' + tblLen;

	// var kycDynaForm = document.createElement("FORM");

}// end of exportArchiveDets

function exportArchiveDets(selObj) {

	// show Loader
	// $("#loadingMessage").show();
	// $("#sucmsg").html("");
	var fna_id = selObj.parentNode.parentNode.cells[0].childNodes[0].value;
	var param = '';

	param += getExptDets(fna_id, param);
	param += loadCustDets(strExistDataCust);

	// alert('Parameter----->'+param);
	// $("#loadingMessage").hide();

}// end of exportFnaDets

function loadCustDets(strExistDataCust) {

	var ssFlds = [ 'dfSelfAnnlincome', 'dfSelfCompname',
			'htxtFldClntHeight', 'htxtFldClntWeight', 'htxtFldClntRace',
			'dfSpsAnnlincome', 'dfSpsCompname' ];

	var custDets = strExistDataCust;
	var param = '';

	for ( var cval in custDets) {

		var ctabdets = custDets[cval];

		for ( var ctab in ctabdets) {

			if (ctabdets.hasOwnProperty(ctab)) {

				var ckey = ctab;
				var cvalue = ctabdets[ctab];

				for ( var ssFld in ssFlds) {
					if (ssFlds[ssFld] == ckey) {
						param += '&' + ckey + '=' + cvalue;
					}// end of if
				}// end of for
			}// end of if
		}// end of for{ctab}
	}// end of for{cval}
	return param;
}// end of loadCustDets

function getExptDets(fna_id, param) {

	var ajaxParam = 'dbcall=EXPTDETS&FNA_ID=' + fna_id;
	var ajaxResponse = ajaxCall(ajaxParam);
	var param = '';
	var retval = ajaxResponse;

	for ( var val in retval) {

		var tabdets = retval[val];
		if (tabdets["SESSION_EXPIRY"]) {
			window.location = baseUrl + SESSION_EXP_JSP;
			return;
		}

		for ( var tab in tabdets) {
			if (tabdets.hasOwnProperty(tab)) {
				var value = tabdets[tab];

				if (tab == "EXPT_FNASSDETS") {

					for ( var ckey in value) {
						var ssvalue = value[ckey];

						for ( var ssFlds in selfSpsFlds) {

							if ((param.indexOf(ssvalue) == -1)) {
								if (selfSpsFlds[ssFlds] == ckey) {

									param += '&' + ckey + '=' + ssvalue;
								}// end of if
							}// end ofif
						}// end of for{ssFlds}
					}// end of for{ckey}
				}// end of if
			}// end of if
		}// end of for{tab}
	}// end of for{val}
	return param;
}// end of getExptDets

function downloadZipFile(selObj) {
	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	var zipcustId = document.getElementById("txtFldCustId").value;
	var attachFlg = document.getElementById("chkCustAttachFlg").value;
	if (rowIndex >= 0) {

		var fnaId = tbody.rows[rowIndex].cells[0].childNodes[0].value;
		var strAdvName = "";
		var servAdvStfId = tbody.rows[rowIndex].cells[6].childNodes[2].value;
		window.open('DownloadFileAttachments.action?&txtFldAttachCltId='
				+ zipcustId + '&txtFldClientName=' + FNA_CUSTNAME
				+ '&selDistId=' + LOGGED_DISTID + "&txtFldCustAttachId="
				+ custattachId + "&txtFldFnaId=" + fnaId + "&strAdvName="
				+ strAdvName + "&strAdvStfId=" + servAdvStfId, '_blank',
				'toolbar=0,location=0,menubar=0');
	} else {
		alert(NO_ATTACHID_FOUND);
		return false;
	}

}

function loaderBlock() {

	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");

	loadMsg.style.display = "block";
	// alert(loadMsg.style.display)
	loadingLayer.style.display = "block";
}


function loaderHide() {

	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");

	loadMsg.style.display = "none";
	// alert(loadMsg.style.display)
//	loadingLayer.style.display = "block";
}

function insertCustomerAttachments(selObj) {

	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	var hNTUCPolicyId = tbody.rows[rowIndex].cells[1].childNodes[2].value;

	if (isEmpty(hNTUCPolicyId)) {
		if (LOG_USER_STFTYPE == "ADVISER") {
			custSelObj = selObj;
			setTimeout(function() {
				loaderBlock()
			}, 0);
			setTimeout(function() {
				insertAttachments()
			}, 1000);
			// setTimeout(function() { chkNRICDocumentDetails() },1000);

		} else {
			alert(ADVISER_ALLOW_EXPORT);
			return false;
		}

	} else {
		alert(POLICYID_RECEIVED);
		return false;
	}

}

function insertAttachments() {

	selObj = custSelObj;

	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	if (rowIndex >= 0) {

		var fnaId = tbody.rows[rowIndex].cells[0].childNodes[0].value;
		var zipcustId = document.getElementById("txtFldCustId").value;
		var servAdvStfId = tbody.rows[rowIndex].cells[6].childNodes[2].value;
		var strAdvName = $("#txtFldAdvNameAttach").val();
		var emailD = String(emailDets).split(",");
		var emailsubj = String(emailD[0]);
		var dispFileName = emailsubj.replace(/~/g, "_");
		dispFileName = dispFileName.replace(/CUST_NAME/g, FNA_CUSTNAME);
		dispFileName = dispFileName.replace(/ARCHIVENO/g,
				selObj.parentNode.parentNode.cells[1].childNodes[1].value)
				.toUpperCase();
		dispFileName += ".PDF";

		dispFileName = "KYC.PDF";

		// alert(parameter)

		var ajaxParam = 'dbcall=INSERT_CUSTOMER_ATTACHMENTS&txtFldFnaId='
				+ fnaId + "&txtFldCustId=" + zipcustId + "&genFileName=''"
				+ "&paramBirtUrl=" + BIRT_URL + "&strAdvName=" + strAdvName;
		var ajaxResponse = callAjax(ajaxParam, false);
		retval = jsonResponse = eval('(' + ajaxResponse + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];

					if (tab == "CUST_ATTACH_ID") {
						custattachId = value[0].txtFlsCustAttachId;
					}

					/*
					 * if(tab == "CUST_ATTACH_NRIC_DETAILS"){
					 * alert(value[0].txtFldNricCount); var loadMsg =
					 * document.getElementById("loadingMessage"); var
					 * loadingLayer = document.getElementById("loadingLayer");
					 * loadMsg.style.display="none"; return false; }
					 */
				}
			}
		}

		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");
		loadMsg.style.display = "none";

		downloadZipFile(custSelObj);

	}
}

function callNTUCWS(selObj) {

	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	if (selObj.value == "APPROVE") {
		var conf = window.confirm(COMPLIANCE_APPROVESTATUS_CONFIRM);
		if (!conf) {
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display = "none";
			selObj.value = "";
			return false;
		}
	}

	if (rowIndex >= 0 && !isEmpty(selObj.value)) {

		var fnaId = tbody.rows[rowIndex].cells[0].childNodes[0].value;
		var ntucPolicyId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var approvalRemark = tbody.rows[rowIndex].cells[19].childNodes[0].value;
		var approvalDate = tbody.rows[rowIndex].cells[20].childNodes[0].value;
		var approvalStatus = selObj.value;
		var ntucPolicyNum = tbody.rows[rowIndex].cells[2].childNodes[1].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var hCompApproveStatus = tbody.rows[rowIndex].cells[18].childNodes[1].value;

		// if(isEmpty(hNTUCPolicyId) && (isEmpty(ntucPolicyNum) ||
		// !(hCompApproveStatus =="APPROVE"))){
		// if(isEmpty(ntucPolicyNum) && !isEmpty(hNTUCPolicyId)){
		/*
		 * var loadMsg = document.getElementById("loadingMessage"); var
		 * loadingLayer = document.getElementById("loadingLayer");
		 *
		 * loadMsg.style.display="block"; loadingLayer.style.display="block";
		 */

		var ajaxParam = 'dbcall=CALL_NTUCWS&txtFldFnaId=' + fnaId
				+ "&txtFldNTUCPolicyId=" + ntucPolicyId
				+ "&txtFldNTUCApprovalRemarks=" + approvalRemark
				+ "&strApprovalDate=" + approvalDate + "&approvalStatus="
				+ approvalStatus;
		var ajaxResponse = callAjax(ajaxParam, false);
		retval = jsonResponse = eval('(' + ajaxResponse + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];

					if (tab == "NTUC_STATUS_DETAILS_FAILED") {
						alert(value[0].txtFldNTUCStatusDescription);
						var loadMsg = document.getElementById("loadingMessage");
						var loadingLayer = document
								.getElementById("loadingLayer");
						loadMsg.style.display = "none";
						return false;
					}

					if (tab == "NTUC_STATUS_DETAILS") {

						tbody.rows[rowIndex].cells[2].childNodes[1].value = value[0].txtFldNTUCPolNumber;
						tbody.rows[rowIndex].cells[2].childNodes[2].value = value[0].txtFldNTUCStatusCode;
						tbody.rows[rowIndex].cells[2].childNodes[3].value = value[0].txtFldNTUCStatusDescription;

						alert("Policy Number:" + value[0].txtFldNTUCPolNumber
								+ "\nStatus Code:"
								+ value[0].txtFldNTUCStatusCode
								+ "\nStatus Description:"
								+ value[0].txtFldNTUCStatusDescription)
						/* if(value[0].txtFldNTUCStatusCode == 0){ */
						compApproveStatus(selObj);
						/*
						 * }else{ var loadMsg =
						 * document.getElementById("loadingMessage"); var
						 * loadingLayer =
						 * document.getElementById("loadingLayer");
						 * loadMsg.style.display="none"; //selObj.value = ""; }
						 */

					}
				}
			}
		}

		/*
		 * }else{ alert("Policy Id has been received"); var loadMsg =
		 * document.getElementById("loadingMessage"); var loadingLayer =
		 * document.getElementById("loadingLayer");
		 * loadMsg.style.display="none"; return false; }
		 */

	}
}

/*
 * function callExportViaAjax(){
 *
 * var templateDiv = document.getElementById("page1");
 *
 *
 * var page2 = document.getElementById("page2"); var kycfrm = document.fna;
 *
 * var csimport = '<link rel="stylesheet" href="css/tblstyle.css"
 * media="print"/><link rel="stylesheet" href="css/kycstyles.css"
 * media="print"/>'+ '<link rel="stylesheet" href="css/toolbar.css"
 * media="print"/>';
 *
 * document.getElementById("exportPdfForm").innerHTML = "<html><head>"+csimport +"</head>" +
 * templateDiv.innerHTML + "</html>"; var exportForm = document.exportPdfForm;
 * var pdfArr = new Array(); var rd=0; for(var ele=0;ele<exportForm.elements.length;ele++){
 * if(exportForm.elements[ele].type=="text"){
 * pdfArr[rd]=exportForm.elements[ele].name; rd++; }
 *
 * if(exportForm.elements[ele].type=="checkbox"){
 * pdfArr[rd]=exportForm.elements[ele].name; rd++; }
 *
 * if(exportForm.elements[ele].type=="radio"){
 * pdfArr[rd]=exportForm.elements[ele].id; rd++; } }
 *
 * var fnafrm = document.fnaForm; for(var len=0;len<pdfArr.length;len++){ var
 * elemObj = eval('document.fnaForm.'+pdfArr[len]); if(isValidObject(elemObj)) {
 * var fieldname = pdfArr[len]; var fieldobj =
 * eval('document.exportPdfForm.'+pdfArr[len]); var fieldtype = fieldobj.type;
 * if(fieldtype == "text"){
 * eval('document.exportPdfForm.'+pdfArr[len]).parentNode.innerHTML =
 * elemObj.value; }else if (fieldtype == "checkbox"){
 * eval('document.exportPdfForm.'+pdfArr[len]).parentNode.innerHTML = ""; }else
 * if (fieldtype == "radio"){
 * eval('document.exportPdfForm.'+pdfArr[len]).parentNode.innerHTML = ""; len++; } } }
 * mapForms = document.mapForm;
 *
 * document.getElementById("txtFld").value=
 * encodeURIComponent(document.getElementById("exportPdf").innerHTML) map =
 * window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");
 *
 *
 * if (map) { mapForms.submit(); } else { alert('You must allow popups for this
 * map to work.'); } }
 *
 * function isValidObject(objToTest) {
 *
 * if (objToTest == null || objToTest == undefined) {
 *
 * return false; }
 *
 * return true; }//end isValidObject
 */

function adminApproveStatus(selObj) {
	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	if (rowIndex >= 0 && !isEmpty(selObj.value)) {
		var mngrStatus = tbody.rows[rowIndex].cells[6].childNodes[0].value;
		if (!(mngrStatus == "APPROVE")) {
			alert(MANAGER_APPROVESTATUS);
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display = "none";
			selObj.value = "";
			return false;
		}

		var fnaId = tbody.rows[rowIndex].cells[0].childNodes[0].value;
		var mangrEmailId = tbody.rows[rowIndex].cells[3].childNodes[2].value;
		var remarks = tbody.rows[rowIndex].cells[15].childNodes[0].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var polNo = tbody.rows[rowIndex].cells[2].childNodes[1].value;

		if (selObj.value == "REJECT" && isEmpty(remarks)) {
			alert(ADMIN_KEYIN_REMARKS);
			selObj.value = tbody.rows[rowIndex].cells[14].childNodes[1].value;
			if (tbody.rows[rowIndex].cells[14].childNodes[1].value == undefined
					|| isEmpty(tbody.rows[rowIndex].cells[14].childNodes[1].value)
					|| tbody.rows[rowIndex].cells[14].childNodes[1].value == "NOT YET APPROVED") {
				selObj.value = "";
			}
			tbody.rows[rowIndex].cells[15].childNodes[0].focus();
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display = "none";
			return false;
		}

		if (selObj.value == "REJECT") {
			var conf = window.confirm(ADMIN_REJECTSTATUS_CONFIRM);
			if (!conf) {
				var loadMsg = document.getElementById("loadingMessage");
				var loadingLayer = document.getElementById("loadingLayer");
				loadMsg.style.display = "none";
				selObj.value = "";
				return false;
			}
		} else if (selObj.value == "APPROVE") {
			var conf = window.confirm(ADMIN_APPROVESTATUS_CONFIRM);
			if (!conf) {
				var loadMsg = document.getElementById("loadingMessage");
				var loadingLayer = document.getElementById("loadingLayer");
				loadMsg.style.display = "none";
				selObj.value = "";
				return false;
			}
		}

		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");

		var ajaxParam = 'dbcall=ADMIN_APPROVE_STATUS&txtFldFnaId=' + fnaId
				+ "&txtFldAdminAppStatus=" + selObj.value
				+ "&txtFldManagerEmailId=" + mangrEmailId
				+ "&txtFldAdminRemarks=" + remarks + "&hNTUCPolicyId="
				+ hNTUCPolicyId + "&txtFldPolNum=" + polNo;
		var ajaxResponse = callAjax(ajaxParam, false);
		retval = jsonResponse = eval('(' + ajaxResponse + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];

					if (tab == "TAB_ADMIN_APPROVE_DETAILS") {
						tbody.rows[rowIndex].cells[13].childNodes[0].value = value[0].ADMIN_KYC_SENT_STATUS;
						tbody.rows[rowIndex].cells[14].childNodes[0].value = value[0].ADMIN_APPROVE_STATUS;
						tbody.rows[rowIndex].cells[15].childNodes[0].value = value[0].ADMIN_APPROVE_REMARKS;
						tbody.rows[rowIndex].cells[16].childNodes[0].value = currDate;

						tbody.rows[rowIndex].cells[13].childNodes[0].disabled = true;
						tbody.rows[rowIndex].cells[14].childNodes[0].disabled = true;
						tbody.rows[rowIndex].cells[15].childNodes[0].readOnly = true;
						tbody.rows[rowIndex].cells[16].childNodes[0].readOnly = true;

						alert(value[0].ADMIN_POLICY_STATUS);
					}
				}
			}
		}

		// selObj.disabled = true;
		tbody.rows[rowIndex].cells[16].childNodes[0].value = currDate;
		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");
		loadMsg.style.display = "none";

	}

}

function managerApproveStatus(selObj) {

	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	if (rowIndex >= 0 && !isEmpty(selObj.value)) {
		var fnaId = tbody.rows[rowIndex].cells[0].childNodes[0].value;
		var mangrEmailId = tbody.rows[rowIndex].cells[3].childNodes[2].value;
		var remarks = tbody.rows[rowIndex].cells[7].childNodes[0].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var polNo = tbody.rows[rowIndex].cells[2].childNodes[1].value;

		if (selObj.value == "REJECT" && isEmpty(remarks)) {
			alert(MANAGER_KEYIN_REMARKS);

			selObj.value = tbody.rows[rowIndex].cells[8].childNodes[1].value;
			if (tbody.rows[rowIndex].cells[8].childNodes[1].value == undefined
					|| isEmpty(tbody.rows[rowIndex].cells[8].childNodes[1].value)
					|| tbody.rows[rowIndex].cells[8].childNodes[1].value == "NOT YET APPROVED") {
				selObj.value = "";
			}
			tbody.rows[rowIndex].cells[7].childNodes[0].focus();
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display = "none";
			return false;
		}

		if (selObj.value == "APPROVE") {
			var conf = window.confirm(CONFIRM_APPROVE_MANAGER);
			if (!conf) {
				var loadMsg = document.getElementById("loadingMessage");
				var loadingLayer = document.getElementById("loadingLayer");
				loadMsg.style.display = "none";
				selObj.value = "";
				return false;
			}
		}

		if (selObj.value == "REJECT") {
			var conf = window.confirm(CONFIRM_REJECT_MANAGER);
			if (!conf) {
				var loadMsg = document.getElementById("loadingMessage");
				var loadingLayer = document.getElementById("loadingLayer");
				loadMsg.style.display = "none";
				selObj.value = "";
				return false;
			}
		}

		var ajaxParam = 'dbcall=MANAGER_APPROVE_STATUS&txtFldFnaId=' + fnaId
				+ "&txtFldMgrAppStatus=" + selObj.value
				+ "&txtFldManagerEmailId=" + mangrEmailId
				+ "&txtFldManagerRemarks=" + remarks + "&hNTUCPolicyId="
				+ hNTUCPolicyId + "&txtFldPolNum=" + polNo;
		var ajaxResponse = callAjax(ajaxParam, false);
		retval = jsonResponse = eval('(' + ajaxResponse + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];

					if (tab == "TAB_MANAGER_APPROVE_DETAILS") {
						tbody.rows[rowIndex].cells[5].childNodes[0].value = value[0].MANAGER_KYC_SENT_STATUS;
						tbody.rows[rowIndex].cells[6].childNodes[0].value = value[0].MANAGER_APPROVE_STATUS;
						tbody.rows[rowIndex].cells[7].childNodes[0].value = value[0].MANAGER_APPROVE_REMARKS;
						tbody.rows[rowIndex].cells[8].childNodes[0].value = currDate;
						tbody.rows[rowIndex].cells[5].childNodes[0].disabled = true;
						tbody.rows[rowIndex].cells[6].childNodes[0].disabled = true;
						tbody.rows[rowIndex].cells[7].childNodes[0].readOnly = true;
						tbody.rows[rowIndex].cells[8].childNodes[0].readOnly = true;
						alert(value[0].MANAGER_POLICY_STATUS);
					}
				}
			}
		}

		// selObj.disabled = true;
		tbody.rows[rowIndex].cells[8].childNodes[0].value = currDate;

		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");
		loadMsg.style.display = "none";

	}

}

function compApproveStatus(selObj) {
	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	/*
	 * var conf = window.confirm("After Changing Approve Status is not allowed
	 * to change. Do You Want to Continue"); if(!conf){ return false; }
	 */

	if (rowIndex >= 0 && !isEmpty(selObj.value)) {

		var fnaId = tbody.rows[rowIndex].cells[0].childNodes[0].value;
		var mangrEmailId = tbody.rows[rowIndex].cells[3].childNodes[2].value;
		var remarks = tbody.rows[rowIndex].cells[19].childNodes[0].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var polNo = tbody.rows[rowIndex].cells[2].childNodes[1].value;

		if (selObj.value == "REJECT" && isEmpty(remarks)) {
			alert(COMPLIANCE_KEYIN_REMARKS);
			selObj.value = "";
			tbody.rows[rowIndex].cells[19].childNodes[0].focus();
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display = "none";
			return false;
		}

		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");

		// setTimeout(function() { loaderBlock() }, 15);

		var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId
				+ "&txtFldCompAppStatus=" + selObj.value
				+ "&txtFldManagerEmailId=" + mangrEmailId
				+ "&txtFldCompRemarks=" + remarks + "&hNTUCPolicyId="
				+ hNTUCPolicyId + "&txtFldPolNum=" + polNo;
		var ajaxResponse = callAjax(ajaxParam, false);
		retval = jsonResponse = eval('(' + ajaxResponse + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];

					if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {

						tbody.rows[rowIndex].cells[17].childNodes[0].value = value[0].COMP_KYC_SENT_STATUS;
						tbody.rows[rowIndex].cells[18].childNodes[0].value = value[0].COMP_APPROVE_STATUS;
						tbody.rows[rowIndex].cells[18].childNodes[1].value = value[0].COMP_APPROVE_STATUS;
						tbody.rows[rowIndex].cells[19].childNodes[0].value = value[0].COMP_APPROVE_REMARKS;
						tbody.rows[rowIndex].cells[20].childNodes[0].value = currDate;

						tbody.rows[rowIndex].cells[17].childNodes[0].disabled = true;

						if (selObj.value == "APPROVE") {
							tbody.rows[rowIndex].cells[18].childNodes[0].disabled = true;
						}

						tbody.rows[rowIndex].cells[19].childNodes[0].readOnly = true;
						tbody.rows[rowIndex].cells[20].childNodes[0].readOnly = true;
						alert(value[0].COMP_POLICY_STATUS);

					}
				}
			}
		}

		// selObj.disabled = true;
		tbody.rows[rowIndex].cells[20].childNodes[0].value = currDate;
		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");
		loadMsg.style.display = "none";

	}

}
function enabOrDisbOthrs(selObj, txtFldID, selSelfSpsObjId) {

	var txtFldObj = document.getElementById(txtFldID);
	var selSelfSpsObj = document.getElementById(selSelfSpsObjId);

	if (selObj.value == 'Others') {

		txtFldObj.className = 'writeModeTextbox';
		txtFldObj.readOnly = false;
		txtFldObj.focus();
		// document.getElementById('othrsLblTxt').style.display = '';

	} else {

		txtFldObj.value = '';
		txtFldObj.className = 'readOnlyText';
		txtFldObj.readOnly = true;
		// document.getElementById('othrsLblTxt').style.display = 'none';

	}// end of if else

	if (!(selObj.value == 'Others') && !(selSelfSpsObj.value == 'Others')) {
		document.getElementById('othrsLblTxt').style.display = 'none';
	} else {
		document.getElementById('othrsLblTxt').style.display = '';
	}// end of if

}// end of enabOrDisbOthrs

function enabOrDisbTxtFld(chkObj, txtFldID, lblFldID) {

	var txtFldObj = document.getElementById(txtFldID);
	var lblFldObj = document.getElementById(lblFldID);

	if (chkObj.checked) {

		lblFldObj.style.color = 'maroon';
		txtFldObj.className = 'writeModeTextbox';
		txtFldObj.readOnly = false;
		txtFldObj.focus();

		/*
		if (chkObj.id == 'dfSelfUspersonflg') {

			if (!isEmpty(clntUSTaxRefNum)) {
				txtFldObj.value = clntUSTaxRefNum;
			}// end of if

		} else if (chkObj.id == 'dfSpsUspersonflg') {

			if (!isEmpty(clntUSTaxRefNum)) {
				txtFldObj.value = spsUSTaxRefNum;
			}// end of if

		}// end of if
//		MAR_2018
		*/

	} else if (!(chkObj.checked)) {

		lblFldObj.style.color = 'black';

		txtFldObj.value = '';
		txtFldObj.className = 'readOnlyText';
		txtFldObj.readOnly = true;
	}// end of if else

}// end of enabOrDisbTxtFld

function chkNRICDocumentDetails() {

	selObj = custSelObj;

	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	if (rowIndex >= 0) {

		var zipcustId = document.getElementById("txtFldCustId").value;

		// alert(parameter)

		var ajaxParam = 'dbcall=chkNRICDocumentDetails&txtFldCustId='
				+ zipcustId;

		var ajaxResponse = callAjax(ajaxParam, false);
		retval = eval('(' + ajaxResponse + ')');

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];

					if (tab == "CUST_ATTACH_NRIC_DETAILS") {
						alert(value[0].txtFldNricCount);
						var loadMsg = document.getElementById("loadingMessage");
						var loadingLayer = document.getElementById("loadingLayer");
						loadMsg.style.display = "none";
						return false;
					}
				}
			}
		}

		/*
		 * var loadMsg = document.getElementById("loadingMessage"); var
		 * loadingLayer = document.getElementById("loadingLayer");
		 * loadMsg.style.display="none";
		 */

		setTimeout(function() {
			insertAttachments()
		}, 1000);

	}
}

function callCompilanceDetails(selObj) {

	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;

	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex - 2;

	if (rowIndex >= 0 && !isEmpty(selObj.value)) {

		var ntucPolicyId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var ntucPolicyNum = tbody.rows[rowIndex].cells[2].childNodes[1].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var remarks = tbody.rows[rowIndex].cells[19].childNodes[0].value;
		var hCompApproveStatus = tbody.rows[rowIndex].cells[18].childNodes[1].value;

		if (selObj.value == "REJECT" && isEmpty(remarks)) {
			alert(COMPLIANCE_KEYIN_REMARKS);
			selObj.value = "";
			tbody.rows[rowIndex].cells[19].childNodes[0].focus();
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display = "none";
			return false;
		}

		var adminStatus = tbody.rows[rowIndex].cells[14].childNodes[0].value;

		if (!(adminStatus == "APPROVE")) {
			alert(ADMIN_APPROVE_STATUS);
			selObj.value = tbody.rows[rowIndex].cells[18].childNodes[1].value;
			if (tbody.rows[rowIndex].cells[18].childNodes[1].value == undefined
					|| isEmpty(tbody.rows[rowIndex].cells[18].childNodes[1].value)
					|| tbody.rows[rowIndex].cells[18].childNodes[1].value == "NOT YET APPROVED") {
				selObj.value = "";
			}
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display = "none";
			return false;
		}

		if (!isEmpty(hNTUCPolicyId) && (hCompApproveStatus != "APPROVE")) {
			callNTUCWS(selObj);
			// compApproveStatus(selObj);
		} else {
			compApproveStatus(selObj);
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display = "none";
			return false;
		}
	}

}

function callManagerApproveStatus(selObj) {
	if (!isEmpty(selObj.value)) {
		setTimeout(function() {
			loaderBlock()
		}, 0);
		setTimeout(function() {
			managerApproveStatus(selObj)
		}, 1000);
	}

}

function callAdminApproveStatus(selObj) {
	if (!isEmpty(selObj.value)) {
		setTimeout(function() {
			loaderBlock()
		}, 0);
		setTimeout(function() {
			adminApproveStatus(selObj)
		}, 1000);
	}
}

function callComplianceApproveStatus(selObj) {
	if (!isEmpty(selObj.value)) {
		setTimeout(function() {
			loaderBlock()
		}, 0);
		setTimeout(function() {
			callCompilanceDetails(selObj)
		}, 1000);

	}
}

function showKycToolTip(evt, obj) {
	var toolStr = "";
	var rowObj = obj.parentNode.parentNode;
	var tblObj = rowObj.parentNode.parentNode;
	var tblId = tblObj.id;
	rowObj = obj.parentNode.parentNode;

	var mgrApprBy = rowObj.cells[11].childNodes[1].value;
	var adminApprBy = rowObj.cells[11].childNodes[2].value;
	var compApprBy = rowObj.cells[11].childNodes[3].value;

	toolStr = " FNA ID : " + rowObj.cells[0].childNodes[0].value

	// if (!isEmpty(mgrApprBy))
	// toolStr += "<br>" + " Manager Status Approved By :" + mgrApprBy;
	// else if (isEmpty(mgrApprBy))
	// toolStr += "<br>" + " Manager Status Yet to Approve!";
	//
	// if (!isEmpty(adminApprBy))
	// toolStr += "<br>" + " Admin Status Approved By :" + adminApprBy;
	// else if (isEmpty(adminApprBy))
	// toolStr += "<br>" + " Admin Status Yet to Approve!";
	//
	// if (!isEmpty(compApprBy))
	// toolStr += "<br>" + " Compliance Status Approved By :" + compApprBy;
	// else if (isEmpty(compApprBy))
	// toolStr += "<br>" + " Compliance Status Yet to Approve!";

	showTooltip(evt, toolStr);
}

function makeReadOnlyNameNric() {

	// 2015_12_17 - MAKING READONLY, NAME & NRIC FIELDS

	// Page14 Elems - FOR SIMPLIFIED OPTION
	// dfSelfName
	// dfSelfNric

	if (!isEmpty(fnaForm.dfSelfName.value)) {
		fnaForm.dfSelfName.readOnly = true;
		fnaForm.dfSelfName.className = "readOnlyText";

	}

	if (!isEmpty(fnaForm.dfSelfNric.value)) {
		fnaForm.dfSelfNric.readOnly = true;
		fnaForm.dfSelfNric.className = "readOnlyText";
	}

	// 2016_02_08

	if (!isEmpty(fnaForm.resAddr1.value)) {
		// fnaForm.resAddr1.readOnly=true;
		// fnaForm.resAddr2.readOnly=true;
		// fnaForm.resAddr3.readOnly=true;
		// fnaForm.resCity.readOnly=true;
		// fnaForm.resState.readOnly=true;
		// fnaForm.resCountry.disabled=true;
		// fnaForm.resPostalcode.readOnly=true;

	}

	if (!isEmpty(fnaForm.dfSelfDob.value)) {
		fnaForm.dfSelfDob.readOnly = true;
		fnaForm.dfSelfDob.className = "readOnlyText";
		document.getElementById("imgPage14ClntDob").style.display = "none";
	}

	if (!isEmpty(fnaForm.dfSelfHomeaddr.value)) {
		fnaForm.dfSelfHomeaddr.readOnly = true;
		fnaForm.dfSelfHomeaddr.className = "readOnlyText";
	}

	// 2016_02_08

	// Page03 Elems - FOR ALL OPTION
	// txtFldFnaClntName
	// txtFldFnaClntNric

	if (!isEmpty(fnaForm.txtFldFnaClntName.value)) {
		fnaForm.txtFldFnaClntName.readOnly = true;
		fnaForm.txtFldFnaClntName.className = "readOnlyText";
	}

	if (!isEmpty(fnaForm.txtFldFnaClntNric.value)) {
		fnaForm.txtFldFnaClntNric.readOnly = true;
		fnaForm.txtFldFnaClntNric.className = "readOnlyText";
	}

	if (!isEmpty(fnaForm.txtFldFnaClntHomeAddr.value)) {
		fnaForm.txtFldFnaClntHomeAddr.readOnly = true;
		fnaForm.txtFldFnaClntHomeAddr.className = "readOnlyText";
	}
}

function openDialog(selObj) {
	ModalPopups.AddressPopup("AddressLayer", {
		width : 750,
		height : 150,
		onOk : "ModalPopupsPromptAddressSubmit()",
		onCancel : "ModalPopupsPromptAddressCancel()"
	});

	comboMakerById(document.getElementById("selResCountry"), CountryAllArray,
			document.getElementById("resCountry").value, true);
	document.getElementById("selResCountry").value = document
			.getElementById("resCountry").value;

	// 2016_02_08
	if (isEmpty(document.getElementById("resAddr1").value)) {
		enabledisableCountry(document.getElementById("selResCountry"))
	}

}

function ModalPopupsPromptAddressCancel() {
	ModalPopups.Cancel("AddressLayer");
}

function ModalPopupsPromptAddressSubmit() {

	document.getElementById("resAddr1").value = document
			.getElementById("txtFldResAddr1").value;
	document.getElementById("resAddr2").value = document
			.getElementById("txtFldResAddr2").value;
	document.getElementById("resAddr3").value = document
			.getElementById("txtFldResAddr3").value;
	document.getElementById("resCity").value = document
			.getElementById("txtFldResCity").value;
	document.getElementById("resPostalcode").value = document
			.getElementById("txtFldResPostalCode").value;
	document.getElementById("resState").value = document
			.getElementById("txtFldResState").value;
	document.getElementById("resCountry").value = document
			.getElementById("selResCountry").value;

	document.getElementById("dfSelfHomeaddr").value = document
			.getElementById("txtFldResAddr1").value
			+ " , "
			+ document.getElementById("txtFldResAddr2").value
			+ " , "
			+ document.getElementById("txtFldResAddr3").value
			+ " , "
			+ document.getElementById("txtFldResCity").value
			+ " , "
			+ document.getElementById("txtFldResPostalCode").value
			+ " , "
			+ document.getElementById("txtFldResState").value
			+ " , "
			+ document.getElementById("selResCountry").value;

	ModalPopups.Cancel("AddressLayer");
}

function enabledisableCountry(selObj) {

	if (selObj.value == "SINGAPORE") {

		document.getElementById("txtFldResState").value = "";
		document.getElementById("txtFldResState").readOnly = true;
		document.getElementById("txtFldResCity").value = "";
		document.getElementById("txtFldResCity").readOnly = true;

	} else {

		document.getElementById("txtFldResState").readOnly = false;
		document.getElementById("txtFldResCity").readOnly = false;
	}
}

function setUnSaveData(strUnSavedCustData) {

	var namearr = [ "txtFldClientName", "txtFldFnaClntName", "htxtFldFnaClntName", "dfSelfName", "txtFldClntAcknClientNameDets" ]
	var icarr = [ "txtFldClntNric", "txtFldFnaClntNric", "htxtFldFnaClntNric",	"dfSelfNric", "txtFldClntAcknClientNRIC" ]
	var dobarr = [ "txtFldClntDOB", "txtFldFnaClntDOB", "dfSelfDob" ]
	var addrarr = [ "txtFldClntResAddress", "txtFldFnaClntHomeAddr","htxtFldFnaClntHomeAddr", "dfSelfHomeaddr", "txtFldSelfRegAddr" ]

	var icvalue = "";
	var nric = "";
	var fin = "";
	var passport = "";
	var custAddr = "";

	for ( var unsavekey in strUnSavedCustData) {

		var unsavevalue = strUnSavedCustData[unsavekey];

		if (unsavekey == "US_CUSTNAME") {
			for (var elem = 0; elem < namearr.length; elem++) {
				var uselem = eval('fnaForm.' + namearr[elem]) || document.getElementById(namearr[elem]);
				if (uselem)
					uselem.value = unsavevalue;
			}
			for (var ele = 0; ele < document.getElementsByName("txtFldClientName").length; ele++) {
				document.getElementsByName("txtFldClientName")[ele].value = unsavevalue;
			}
		}
		if (unsavekey == "US_CUSTNRIC") {
			nric = unsavevalue;
			icvalue = !isEmpty(nric) ? nric : !isEmpty(fin) ? fin : !isEmpty(passport) ? passport : "";
		}
		if (unsavekey == "US_CUSTFIN") {
			fin = unsavevalue;
			icvalue = !isEmpty(nric) ? nric : !isEmpty(fin) ? fin : !isEmpty(passport) ? passport : "";
		}
		if (unsavekey == "US_CUSTPP") {
			passport = unsavevalue;
			icvalue = !isEmpty(nric) ? nric : !isEmpty(fin) ? fin : !isEmpty(passport) ? passport : "";
		}

		if (unsavekey == "US_CUSTDOB") {
			for (var elem = 0; elem < dobarr.length; elem++) {
				var uselem = eval('fnaForm.' + dobarr[elem]);
				if (uselem)
					uselem.value = unsavevalue;
			}
		}

		if (unsavekey == "US_CUSTADDR1") {
			fnaForm.resAddr1.value = unsavevalue;
			custAddr = unsavevalue;
		}
		if (unsavekey == "US_CUSTADDR2") {
			fnaForm.resAddr2.value = unsavevalue;
			if (!isEmpty(unsavevalue))
				custAddr += "," + unsavevalue;
		}
		if (unsavekey == "US_CUSTADDR3") {
			fnaForm.resAddr3.value = unsavevalue;
			if (!isEmpty(unsavevalue))
				custAddr += "," + unsavevalue;
		}
		if (unsavekey == "US_CUSTCITY") {
			fnaForm.resCity.value = unsavevalue;
			if (!isEmpty(unsavevalue))
				custAddr += "," + unsavevalue;
		}
		if (unsavekey == "US_CUSTSTATE") {
			fnaForm.resState.value = unsavevalue;
			if (!isEmpty(unsavevalue))
				custAddr += "," + unsavevalue;
		}
		if (unsavekey == "US_CUSTCOUNTRY") {
			fnaForm.resCountry.value = unsavevalue;
			if (!isEmpty(unsavevalue))
				custAddr += "," + unsavevalue;
		}
		if (unsavekey == "US_CUSTPOSTAL") {
			fnaForm.resPostalcode.value = unsavevalue;
			if (!isEmpty(unsavevalue))
				custAddr += "," + unsavevalue + ".";
		}

	}

	for (var elem = 0; elem < addrarr.length; elem++) {
		var uselem = eval('fnaForm.' + addrarr[elem]);
		if (uselem)
			uselem.value = custAddr;
	}

	copyRegAddrToMailAddr(fnaForm.dfSelfMailaddrflg);

	for (var elem = 0; elem < icarr.length; elem++) {
		var uselem = eval('fnaForm.' + icarr[elem]);
		if (uselem)
			uselem.value = icvalue;

	}
}

function showMgrInfo(e, obj, tt_for) {

	var toolStr = "";
	var rowObj = obj.parentNode.parentNode;
	var tblObj = rowObj.parentNode.parentNode;
	var tblId = tblObj.id;
	rowObj = obj.parentNode.parentNode;

	var mgrApprBy = rowObj.cells[11].childNodes[1].value;
	var adminApprBy = rowObj.cells[11].childNodes[2].value;
	var compApprBy = rowObj.cells[11].childNodes[3].value;

	// var tt_txt = " Manager : "+advmgrname ; //+ " [" +advmgrid + "] ";
	var tt_txt = "";

	if (tt_for == "mgr") {
		if (!isEmpty(mgrApprBy))
			tt_txt += "<br>" + " KYC Approved By :" + mgrApprBy;
		else if (isEmpty(mgrApprBy))
			tt_txt += "<br>" + " KYC Yet to Approve by Manager!";
	}

	if (tt_for == "admin") {
		if (!isEmpty(adminApprBy))
			tt_txt += "<br>" + " KYC Approved By :" + adminApprBy;
		else if (isEmpty(adminApprBy))
			tt_txt += "<br>" + " KYC Yet to Approve by Admin!";
	}

	if (tt_for == "comp") {
		if (!isEmpty(compApprBy))
			tt_txt += "<br>" + " KYC Approved By :" + compApprBy;
		else if (isEmpty(compApprBy))
			tt_txt += "<br>" + " KYC Yet to Approve by Compliance!";
	}

	showTooltip(e, tt_txt);

}

function validatePageOne() {

//	v2)Representativeâ€™s Declaration, at least 1 of the following options must be checked in Representativeâ€™s
//	alert("validatePageOne")
	var optnOneA = document.getElementById('htfrepdecno1a');
	var optnOneB = document.getElementById('htfrepdecno1b');
	var AndHPrdts = document.getElementById('htfrepdecah');
	var optnTwo = document.getElementById('htfrepdecno2');
	var optnThree = document.getElementById('htfrepdecno3');
	var optnGIPrdts = document.getElementById('htfrepdecgi');


	if (!(optnOneA.checked) && !(optnOneB.checked) && !(AndHPrdts.checked) && !(optnTwo.checked) && !(optnThree.checked) && !(optnGIPrdts.checked)) {

		alert(AGNT_DECLR_VAL_MSG);
		setPageFocus(1, FNA_FORMTYPE, custLobArr);

		scrollToView("tblAdviserDeclSec");
//	End of v2)


		return false;
	}// end of if

	// for Application Types(Client's Choice)
	var chkOptOneLandN = document.getElementById('htfcclife1');
	var chkOptOneAandH = document.getElementById('htfccah1');
	var chkOptTwoLandN = document.getElementById('htfcclife2');
	var chkOptTwoAandH = document.getElementById('htfccah2');
	// var spnOptTwoLandN = document.getElementById('spnOptnTwoLandN');
	// var spnOptTwoAandH = document.getElementById('spnOptnTwoLyfAandH');

	if (!(chkOptOneLandN.checked) && !(chkOptOneAandH.checked)
			&& !(chkOptTwoLandN.checked) && !(chkOptTwoAandH.checked)) {

		alert(CLNT_CHOICE_VAL_MSG);
		setPageFocus(1, FNA_FORMTYPE, custLobArr);

		$('.spnAppchoicCls').each(function() {
			var $this = $(this);
			$this.css({ 'background-color' : 'yellow'});
		});

		chkOptOneLandN.focus();
		return false;
	}// end of if

	return true;
}// end of validatePageOne

function validateApplcntType() {

	var chkOptOneLandN = document.getElementById('htfcclife1');
	var chkOptOneAandH = document.getElementById('htfccah1');
	var chkOptTwoLandN = document.getElementById('htfcclife2');
	var chkOptTwoAandH = document.getElementById('htfccah2');
	// var spnOptTwoLandN = document.getElementById('spnOptnTwoLandN');
	// var spnOptTwoAandH = document.getElementById('spnOptnTwoLyfAandH');

	if ((chkOptOneLandN.checked) || (chkOptOneAandH.checked)
			|| (chkOptTwoLandN.checked) || (chkOptTwoAandH.checked)) {

		$('.spnAppchoicCls').each(function() {
			var $this = $(this);
			$this.css({
				'background-color' : 'transparent'
			});
		});

		// spnOptTwoLandN.style.backgroundColor = 'transparent'
		// spnOptTwoAandH.style.backgroundColor = 'transparent'

	} else if (!(chkOptOneLandN.checked) && !(chkOptOneAandH.checked)
			&& !(chkOptTwoLandN.checked) && !(chkOptTwoAandH.checked)) {

		alert(CLNT_CHOICE_VAL_MSG);

		$('.spnAppchoicCls').each(function() {
			var $this = $(this);
			$this.css({
				'background-color' : 'yellow'
			});
		});

		// spnOptTwoLandN.style.backgroundColor = 'yellow'
		// spnOptTwoAandH.style.backgroundColor = 'yellow'
		chkOptOneLandN.focus();

	}// end of if//end of if

	// to enable or disable the
	// client risk preference controls
	// added by johnson 20160303
	// enabOrDisabClntRiskPref(chkOptTwoLandN);
	// commented by johnson 20160316

}// end of validateApplcntType

function validateUSTaxPrsn(flag) {


//	$("input[name='dfSelfUsnotifyflg'], input[name='dfSelfUspersonflg']").attr("checked",false);
//	$("input[name='dfSpsUsnotifyflg'], input[name='dfSpsUspersonflg']").attr("checked",false);
	var spsName = fnaForm.dfSpsName.value;


	var chkSelfUSPrsn = document.getElementById('dfSelfUsnotifyflg');
	var chkSpsUSPrsn = document.getElementById('dfSpsUsnotifyflg');

	var chkSelfUSTaxPrsn = document.getElementById('dfSelfUspersonflg');
	var chkSpsUSTaxPrsn = document.getElementById('dfSpsUspersonflg');

	var txtFldSelfUSTaxPrsn = document.getElementById('dfSelfUstaxno');
	var txtFldSpsUSTaxPrsn = document.getElementById('dfSpsUstaxno');
	var clntfatca=false;
	var spsfatca=false;


	if(chkSelfUSPrsn.checked || chkSelfUSTaxPrsn.checked){
		clntfatca = true;
	}

	if(!clntfatca && flag){
//		alert(FNA_FATCA_USORNOT + " for client");
		changedarrGlobal4.push(FNA_FATCA_USORNOT + " for Client(1)");
//		setPageFocus(20,FNA_FORMTYPE);
//		scrollToView("tblFatcaDecl");
//		return false;
	}

	if(!isEmpty(spsName)){
		if(chkSpsUSPrsn.checked || chkSpsUSTaxPrsn.checked){
			spsfatca=true;
		}

		if(!spsfatca && flag){
//			alert(FNA_FATCA_USORNOT + " for spouse");
			changedarrGlobal4.push(FNA_FATCA_USORNOT + " for Client(2)");
//			setPageFocus(20,FNA_FORMTYPE);
//			scrollToView("tblFatcaDecl");
//			return false;
		}
	}

	if (chkSelfUSTaxPrsn.checked && flag) {
		if(isEmpty(txtFldSelfUSTaxPrsn.value)){
			changedarrGlobal4.push(USTAX_PRSNCLNT_VAL_MSG);
		}
//		if (!checkMandatoryField(txtFldSelfUSTaxPrsn, USTAX_PRSNCLNT_VAL_MSG,20, FNA_FORMTYPE))
//			return;
	}// end of if

	if (chkSpsUSTaxPrsn.checked && flag) {
//		if (!checkMandatoryField(txtFldSpsUSTaxPrsn, USTAX_PRSNSPS_VAL_MSG, 20,FNA_FORMTYPE))
//			return;
		if(isEmpty(txtFldSpsUSTaxPrsn.value)){
			changedarrGlobal4.push(USTAX_PRSNSPS_VAL_MSG);
		}
	}// end of if

	if(chkSelfUSPrsn.checked && flag) { //condition added by kavi 15/03/2018
	var tblobj = document.getElementById("fnaFatcaTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj == 0) {


		changedarrGlobal4.push(FNA_FATCA_ATLEAST_1TAX +" for Client(1)");

//		alert(FNA_FATCA_ATLEAST_1TAX +" for Client");

//		setPageFocus(20,FNA_FORMTYPE);
//		scrollToView("fnaFatcaTbl");
//		return false;
	}
	}

	if(chkSpsUSPrsn.checked && flag){
		var tblobj = document.getElementById("fnaFatcaTblSps");
		var tbodyobj = tblobj.tBodies[0];
		var rowlenobj = tbodyobj.rows.length;

		if (rowlenobj == 0) {

			changedarrGlobal4.push(FNA_FATCA_ATLEAST_1TAX +" for Client(1)/Client(2)");

//			alert(FNA_FATCA_ATLEAST_1TAX+" for Spouse/Client");
//
//			setPageFocus(20,FNA_FORMTYPE);
//			scrollToView("fnaFatcaTblSps");
//
//			return false;
		}
	}

	return true;
}// end of validateUSTaxPrsn


function chkTPPBenifPEP(radObj, fldVal) {

	var radVal = radObj.value;
	var radBenfObj, radTPPObj, radPEPObj;
	var idObj;

	radBenfObj = document.getElementsByName('cdBenfownflg');
	radTPPObj = document.getElementsByName('cdTppflg');
	radPEPObj = document.getElementsByName('cdPepflg');

	if (fldVal == 'BENF')
		idObj = cdBenfTppPepObj.BENF;
	if (fldVal == 'TPP')
		idObj = cdBenfTppPepObj.TPP;
	if (fldVal == 'PEP')
		idObj = cdBenfTppPepObj.PEP;


	if (radObj.checked && radObj.value == 'Y') {

		$('.mandLblTxtBenTP').each(function(index, elem) {
			$(elem).css('color', 'maroon');
		});

		$(idObj).each(function(index, elem) {
			var $elem = $('#' + elem);

			if ($elem.prop('type') == 'select-one')
				$elem.prop('disabled', false);
			else
				$elem.prop('readonly', false);

			if ($elem.hasClass('readOnlyText')) {
				$elem.removeClass('readOnlyText')
				$elem.addClass('writeModeTextbox')
			}// end of if

		});

		if (fldVal == 'TPP') {

			$('.mandLblTxtBenTPP').each(function(index, elem) {
				$(elem).css('color', 'maroon');
			});

			$(cdBenfTppPepObj.OTH).each(function(index, elem) {

				var $elem = $('#' + elem);

				if ($elem.prop('type') == 'radio')
					$elem.prop('disabled', false);
				else
					$elem.prop('readonly', false);

				if ($elem.hasClass('readOnlyText')) {
					$elem.removeClass('readOnlyText')
					$elem.addClass('writeModeTextbox')
				}// end of if

			});

			$('input[name^="htfcdpaymentmode"]').each(function() {
				$(this).prop('disabled', false)
			})

			$('#filecdTppNricCopy').prop('disabled', false);

		}// end of if

		// $('input[name^="htfcdpaymentmode"]').each(function(){$(this).prop('disabled',false)})

		// if(fldVal == 'TPP') $('#filecdTppNricCopy').prop('disabled',false);

		// if(fldVal == 'BENF') {benfFlg = true; tppFlg = false;pepFlg = false;}
		// else if(fldVal == 'TPP') {benfFlg = false;tppFlg = true; pepFlg =
		// false;}
		// else if(fldVal == 'PEP') {benfFlg = false; tppFlg = false;pepFlg =
		// true;}

	} else if (radObj.checked && radObj.value == 'N') {

		$(idObj).each(function(index, elem) {

			if(!isEmpty($("#ntucCaseId").val()) && (elem=="txtFldCDBenfId" || elem=="txtFldCDTppId" || elem=="txtFldCDPepId"))
			{
			return true;
			}

			var $elem = $('#' + elem);

			if ($elem.prop('type') == 'select-one')
				$elem.prop('disabled', true).val('');
			else
				$elem.prop('readonly', true).val('');

			if ($elem.hasClass('writeModeTextbox')) {
				$elem.removeClass('writeModeTextbox')
				$elem.addClass('readOnlyText')
			}// end of if
			
			

		});
		$("#filecdTppNricCopy").val("");
		$('#filecdTppNricCopy').prop('disabled', true);

		// if(fldVal == 'BENF') {benfFlg = tppFlg = pepFlg = false;}
		// else if(fldVal == 'TPP') {benfFlg = tppFlg = pepFlg = false;}
		// else if(fldVal == 'PEP') {benfFlg = tppFlg = pepFlg = false;}

	}// end of if

	if (!radBenfObj[0].checked && !radTPPObj[0].checked
			&& !radPEPObj[0].checked) {

		$('.mandLblTxtBenTP').each(function(index, elem) {
			$(elem).css('color', 'black');
		});

	}// end of if

	if (radTPPObj[1].checked) {

		$('.mandLblTxtBenTPP').each(function(index, elem) {
			$(elem).css('color', 'black');
		});

		$(cdBenfTppPepObj.OTH).each(function(index, elem) {

			var $elem = $('#' + elem);

			$elem.prop('readonly', true).val('');

			if ($elem.hasClass('writeModeTextbox')) {
				$elem.removeClass('writeModeTextbox')
				$elem.addClass('readOnlyText')
			}// end of if

		});

		$('input[name^="htfcdpaymentmode"]').each(function() {
			$(this).prop({
				'disabled' : true,
				'checked' : false
			})
		})

	}// end of if

}// end of

function validateTPPBenifPEP(flag) {


		var benfArr = new Array();
		var tppArr = new Array();
		var pepArr = new Array();
		var benfTppPepOthIdArr;

		var BENF = 'Beneficial Owner';
		var TPP = 'Third Party Payer';
		var PEP = 'Political Exposed Person (PEP)';

		var type = '', chkFlg = false;

		var MSG = [ 'Key in Name of the ', 'Key in NRIC / Incorp. No of the ',
				'Key in NRIC / Incorp. No  of the ', 'Key in Address of the ',
				'Key in Position of the ', 'Select Country of the ',
				'Select in Relationship of the ' ];
		var OTH = [ 'Key in Name of the Bank',
				'Key in Contact No. of the Bank' ];

		var radBenfObj = document.getElementsByName('cdBenfownflg');
		var radTPPObj = document.getElementsByName('cdTppflg');
		var radPEPObj = document.getElementsByName('cdPepflg');

		var benfFlg = false, tppFlg = false, pepFlg = false;// added by johnson
															// 20160303

		if (radBenfObj && radBenfObj[0].checked) {
			benfFlg = true;
		}
		if (radTPPObj && radTPPObj[0].checked) {
			tppFlg = true;
		}
		if (radPEPObj && radPEPObj[0].checked) {
			pepFlg = true;
		}

		if (benfFlg) {
			benfArr = cdBenfTppPepObj.BENF;
			type = BENF;
		}
		if (tppFlg) {
			tppArr = cdBenfTppPepObj.TPP;
			type = TPP;
		}
		if (pepFlg) {
			pepArr = cdBenfTppPepObj.PEP;
			type = PEP;
		}

		if (tppFlg) {
			benfTppPepOthIdArr = cdBenfTppPepObj.OTH;
		}

		if (benfArr) {
			$.each(benfArr, function(index, elemId) {
				var $elem = $('#' + elemId);
				if (isEmpty($elem.val()) && flag) {

					if(elemId == "txtFldCDBenfNric" || elemId == "txtFldCDBenfIncNo"){
						var nric = $("#txtFldCDBenfNric").val();
						var incorp = $("#txtFldCDBenfIncNo").val();

						if(isEmpty(nric) && isEmpty(incorp)){
//							alert(MSG[index] + BENF);
//							setPageFocus(13, FNA_FORMTYPE, custLobArr);
//							$elem.focus();
							changedarrGlobal3.push(MSG[index] + BENF +"^"+$elem);
//							chkFlg = true;
//							return false;
//							return true;
						}


					}else{
//						alert(MSG[index] + BENF);
//						setPageFocus(13, FNA_FORMTYPE, custLobArr);
//						$elem.focus();
						changedarrGlobal3.push(MSG[index] + BENF+"^"+$elem);
//						chkFlg = true;
//						return false;
//						return true;
					}


				}// end of if
			});

//			if(!flag)return true;
//			else if (chkFlg && flag)
//				return false;

		}// end of if
		if (tppArr) {
			$.each(tppArr, function(index, elemId) {
				var $elem = $('#' + elemId);
				if (isEmpty($elem.val()) && flag) {

					if(elemId == "txtFldCDTppNric" || elemId == "txtFldCDTppIncNo"){
						var nric = $("#txtFldCDTppNric").val();
						var incorp = $("#txtFldCDTppIncNo").val();

						if(isEmpty(nric) && isEmpty(incorp)){
//							alert(MSG[index] + TPP);
//							setPageFocus(13, FNA_FORMTYPE, custLobArr);
//							$elem.focus();
							changedarrGlobal3.push(MSG[index] + TPP+"^"+$elem);
//							chkFlg = true;
//							return false;
//							return true;
						}


					}else{
//						alert(MSG[index] + TPP);
//						setPageFocus(13, FNA_FORMTYPE, custLobArr);
//						$elem.focus();
						changedarrGlobal3.push(MSG[index] + TPP+"^"+$elem);
//						chkFlg = true;
//						return false;
//						return true;
					}

				}// end of if
			});

//			if(!flag)return true;
//			else if (chkFlg && flag)return false;
//			else if (!chkFlg && flag)return true;
		}// end of if

		if (pepArr) {
			$.each(pepArr, function(index, elemId) {
				var $elem = $('#' + elemId);
				if (isEmpty($elem.val()) && flag) {

					if(elemId == "txtFldCDPepNric" || elemId == "txtFldCDPepIncNo"){
						var nric = $("#txtFldCDPepNric").val();
						var incorp = $("#txtFldCDPepIncNo").val();

						if(isEmpty(nric) && isEmpty(incorp)){
//							alert(MSG[index] + PEP);
//							setPageFocus(13, FNA_FORMTYPE, custLobArr);
//							$elem.focus();
							changedarrGlobal3.push(MSG[index] + PEP+"^"+$elem);
//							chkFlg = true;
//							return false;
//							return true;
						}


					}else{
//						alert(MSG[index] + PEP);
//						setPageFocus(13, FNA_FORMTYPE, custLobArr);
//						$elem.focus();
						changedarrGlobal3.push(MSG[index] + PEP+"^"+$elem);
//						chkFlg = true;
//						return false;
//						return true;
					}

				}// end of if
			});
//			if(!flag)return true;
//			else if (chkFlg && flag)return false;
//			else if (!chkFlg && flag)return true;

		}// end of if

		if (benfTppPepOthIdArr) {

			$.each(benfTppPepOthIdArr, function(index, elemId) {

				var $elem = $('#' + elemId);

				if (isEmpty($elem.val()) && flag) {
//					alert(OTH[index]);
//					setPageFocus(13, FNA_FORMTYPE, custLobArr);
//					$elem.focus();
					changedarrGlobal3.push(OTH[index]+"^"+$elem);
//					chkFlg = true;
//					return false;
//					return true;
				}// end of if
			});

//			if(!flag)return true;
//			else if (chkFlg && flag)return false;
//			else if (!chkFlg && flag)return true;


			var radCashObj = document.getElementsByName('htfcdpaymentmode');
			// var tppUploadFld = document.getElementById('filecdTppNricCopy');

			if (!radCashObj[0].checked && !radCashObj[1].checked
					) {

//				alert('select Cash/Cheque/Credit Card');
				changedarrGlobal3.push("select Cash/Cheque/Credit Card"+"^"+radCashObj[0]);
//				setPageFocus(13, FNA_FORMTYPE, custLobArr);
//				radCashObj[0].focus();
//				return false;
//				return true;
			}// end of if

			// if(!checkMandatoryField(tppUploadFld,
			// TPP_UPLOAD_VAL_MSG,13,FNA_FORMTYPE))return true;
		}// end of if




	return true;
}// end of validateTPPBenifPEPs

function enabOrDisabClntRiskPref(chkObj, chkVal) {

	var riskIdArr = clntRiskPrefArr.ID;

	if ((chkObj) && (chkObj.checked) || (chkVal) && (!isEmpty(chkVal))) {

		$('.clntRiskPrefLblCls').each(function(index, elem) {
			$(elem).css('color', 'maroon');
		});

		$(riskIdArr).each(function(index, elemId) {

			var $elem = $('#' + elemId);

			if ($elem.prop('type') == 'select-one')
				$elem.prop('disabled', false);
			else
				$elem.prop('readonly', false);

			if ($elem.hasClass('readOnlyText')) {
				$elem.removeClass('readOnlyText')
				$elem.addClass('writeModeTextbox')
			}// end of if

		});

		$('input[name^="crRiskclass"]').each(function() {
			$(this).prop('disabled', false)
		})
		// lyfAndNIFlg = true;

	} else if ((chkObj) && (!chkObj.checked)) {

		$('.clntRiskPrefLblCls').each(function(index, elem) {
			$(elem).css('color', 'black');
		});

		$(riskIdArr).each(function(index, elemId) {

			var $elem = $('#' + elemId);

			if ($elem.prop('type') == 'select-one')
				$elem.prop('disabled', true).val('');
			else
				$elem.prop('readonly', true).val('');

			if ($elem.hasClass('writeModeTextbox')) {
				$elem.removeClass('writeModeTextbox')
				$elem.addClass('readOnlyText')
			}// end of if

		});

		$('input[name^="crRiskclass"]').each(function() {
			$(this).prop({
				'disabled' : true,
				'checked' : false
			})
		})
		// lyfAndNIFlg = false;
	}// end of if

}// end of enabOrDisabClntRiskPref

function validateClntRiskPref() {

	// $('input[name^="crRiskclass"]').each(function(){$(this).prop('disabled',false)})
	var clntRiskPrefIdArr, clntRiskPrefMsgArr;
	var chkFlg = false;
	var lyfAndNIFlg = false;
	var htfcclife2 = document.getElementById("htfcclife2");

	var isLifePrdt = checkValueInArray(custLobArr, LIFE_INS);
	var isInvestPrdt = checkValueInArray(custLobArr, INVEST);

	if (htfcclife2.checked || isInvestPrdt || (!isLifePrdt && !isInvestPrdt)) {
		clntRiskPrefIdArr = clntRiskPrefArr.ID;
		clntRiskPrefMsgArr = clntRiskPrefArr.MSG;
	}

	if (clntRiskPrefIdArr) {
		$.each(clntRiskPrefIdArr, function(index, elemId) {
			var $elem;
			if (elemId == 'crRiskclass') {
				var cnt = 0, maxLen = $('input[name^="crRiskclass"]').length;
				$('input[name^="crRiskclass"]').each(function() {
					if (!($(this).prop('checked')))
						cnt++;
				});

				if (cnt == maxLen) {
					alert(clntRiskPrefMsgArr[index]);
					setPageFocus(10, FNA_FORMTYPE, custLobArr);
					window.location.hash = '#divClntRiskClsfictn';
					$('.spnClntRiskPref').each(function() {
						var $radObj = $(this);
						$radObj.css({
							'background-color' : 'yellow'
						});
					});

					chkFlg = true;
					return false;
				}// end of if

			} else {
				$elem = $('#' + elemId);
			}// end of if else

			if ($elem) {

				if (isEmpty($elem.val())) {
					alert(clntRiskPrefMsgArr[index]);
					setPageFocus(10, FNA_FORMTYPE, custLobArr);
					$elem.focus();
					chkFlg = true;
					return false;
				}// end of if

			}// end of if

		});

		if (chkFlg)
			return false;
	}// end of if
	/*
	 * if(clntRiskPrefIdArr){ var cnt=0,maxLen =
	 * $('input[name^="crRiskclass"]').length;
	 * $('input[name^="crRiskclass"]').each(function(){
	 * if(!($(this).prop('checked')))cnt++; }); if(cnt == maxLen){ alert('Select
	 * any one from the General Risk Classification');
	 * setPageFocus(10,FNA_FORMTYPE,custLobArr); return false; }//end of if
	 * }//end of if
	 */
	return true;
}// end of validateClntRiskPref

// this funcionality is included to validate the
// four advise and recommandation tables during update
// added by johnson 20160303
function validateAdvRecommTbls() {

	var tblArrId = [ 'fnaartuplanTbl', 'fnaartufundTbl', 'fnaswrepplanTbl','fnaSwrepFundTbl' ];
	var page, chkObj;
	var message = '';

	// radio check related
	var radInvOrAHProd = document.getElementsByName('ccRepexistinvestflgYN');
	var radFlgY = false, radFlgN = false;
	var chkObjLife = document.getElementById('htfcclife2');
	var chkObjAh = document.getElementById('htfccah2');

	// if(checkValueInArray(custLobArr, LIFE_AND_INVEST)){}
	// if (checkValueInArray(custLobArr, LIFE_INS)){}
	// if (checkValueInArray(custLobArr, INVEST)){}

	if (!validateFnaArtuPlantDet(true))
		return;

	if (!validateFnaArtuFundtDet(true))
		return;

	if (!validateFnaSwrepPlantDet(true))
		return;

	if (!validateFnaSwrepFundtDet(true))
		return;

	if (radInvOrAHProd[0].checked && radInvOrAHProd[0].value == 'Y')
		radFlgY = true;
	if (radInvOrAHProd[1].checked && radInvOrAHProd[1].value == 'N')
		radFlgN = true;
	var msgArr = new Array(), cnt = 0;

	var message = 'Representative must include one or more Product(s) recommended to the Client(1)/Client(2)\n ';

	var tbl1message = message
			+ ' in Product Recommendations (New Purchase & Top Up) section - Life, A&H, PL and ILP';

	var tbl2message = message
			+ ' in Product Recommendations (New Purchase & Top Up) section - UT';

	var tbl3message = message
			+ ' in Advice & Recommendations (Switching & Replacement) section - Life, A&H, PL and ILP';

	var tbl4message = message
			+ ' in Advice & Recommendations (Switching & Replacement) section - UT';

	var eitherfundorprdt = message
			+ ' in Product Recommendations (New Purchase & Top Up) section,either For  - (Life, A&H, PL and ILP) or  UT ';

	if (!checkValueInArray(custLobArr, INVEST)
			&& !checkValueInArray(custLobArr, LIFE_INS)) {

		var life = false,inv=false;

		var tblobj1 = document.getElementById("fnaartuplanTbl");
		var tbodyobj1 = tblobj1.tBodies[0];
		var rowlenobj1 = tbodyobj1.rows.length;
		if(rowlenobj1 == 0)
			life=true;
		else if(rowlenobj1 > 0)
			life=false;

		var tblobj2 = document.getElementById("fnaartufundTbl");
		var tbodyobj2 = tblobj2.tBodies[0];
		var rowlenobj2 = tbodyobj2.rows.length;
		if(rowlenobj2 == 0)
			inv=true;
		else if(rowlenobj2 > 0)
			inv=false;


//		var life = chklifeuttbl("fnaartuplanTbl", null, 14);
//		var inv = chklifeuttbl("fnaartufundTbl", null, 25);

		if ((life)) {
//			alert(eitherfundorprdt);
//			setPageFocus(14, FNA_FORMTYPE, custLobArr);
//			return false;
			changedarrGlobal7.push(eitherfundorprdt);
		}
		if(!life){
			if(inv)
				changedarrGlobal8.push(eitherfundorprdt);
		}
	} else {
		if (!checkValueInArray(custLobArr, INVEST)){

			var tblobj = document.getElementById("fnaartuplanTbl");
			var tbodyobj = tblobj.tBodies[0];
			var rowlenobj = tbodyobj.rows.length;

			if(rowlenobj == 0)
				changedarrGlobal7.push(tbl1message);

		}

		if ((radFlgN) && !checkValueInArray(custLobArr, LIFE_INS)){
//			&& !chklifeuttbl("fnaartufundTbl", tbl2message, 14)

			var tblobj = document.getElementById("fnaartufundTbl");
			var tbodyobj = tblobj.tBodies[0];
			var rowlenobj = tbodyobj.rows.length;

			if(rowlenobj == 0)
				changedarrGlobal8.push(tbl2message);
		}
			//return;// || chkObjLife.checked == true
	}

//	14032018


	if (radFlgY) {

		var radQArr = [ 'swrepConflg', 'swrepAdvbyflg','swrepDisadvgflg', 'swrepProceedflg' ];

		for (var qRad = 0; qRad < radQArr.length; qRad++) {

			var rad = document.getElementsByName(radQArr[qRad]);

			if (qRad == 0 && rad[0].checked) {
//				if (!checkMandatoryField(document.getElementById('swrepConfDets'), ADV_RECMM_Q1,15, FNA_FORMTYPE))
//					return;
				if(isEmpty(document.getElementById('swrepConfDets').value)){
					changedarrGlobal9.push(ADV_RECMM_Q1)
				}



			}

			if (!rad[0].checked && !rad[1].checked) {
				var msg = '';

				if (qRad > 0) {
					msg = 'Representative must select Yes/No for the Question '	+ (qRad + 1) + '.'
				} else {
					msg = 'Representative must select Yes for the Question '	+ (qRad + 1) + '.'
				}


				changedarrGlobal9.push(msg)


//				alert(msg);
//				setPageFocus(15, FNA_FORMTYPE, custLobArr);
//				rad[0].focus();
//				return false;
			}// end of if
		}// end of for

		if (!checkValueInArray(custLobArr, INVEST)	){
			var tblobj = document.getElementById("fnaswrepplanTbl");
			var tbodyobj = tblobj.tBodies[0];
			var rowlenobj = tbodyobj.rows.length;
			if(rowlenobj == 0)
				changedarrGlobal10.push(tbl3message);
		}//&& !chklifeuttbl("fnaswrepplanTbl", tbl3message, 16)
			//return;// chkObjLife.checked == true &&   //return comented 15/11/2018 -kavi
//		Commentted on may_2017
//		if (!checkValueInArray(custLobArr, LIFE_INS)
//				&& !chklifeuttbl("fnaSwrepFundTbl", tbl4message, 16))
//			return;// chkObjLife.checked == true &&

	}// end of if

	// for(var tbl = 0;tbl<tblArrId.length;tbl++){
	//
	// // if((tblId == 'fnaartufundTbl' && chkObjLife.checked == true &&
	// rowlenobj == 0) ){
	// // message += 'Representative must include one or more Product(s)
	// recommended to the client \n ';
	// // message += 'for UT and ILP (FUND DETAILS) in the New Purchase & Top Up
	// ';
	// //// page = 14;
	// // alert(message);
	// // setPageFocus(14,FNA_FORMTYPE);
	// // return false;
	// // }
	//
	//
	//
	// // if(tblId == 'fnaswrepplanTbl' && radFlgY && rowlenobj == 0)
	//
	//
	// if((tblId == 'fnaartuplanTbl' && !radFlgN && rowlenobj == 0) ||
	// (tblId == 'fnaartufundTbl' && !radFlgN && rowlenobj == 0) ||
	// (tblId == 'fnaartuplanTbl' && radFlgN && rowlenobj == 0) ||
	// (tblId == 'fnaartufundTbl' && radFlgN && rowlenobj == 0)){
	//
	// message += 'Representative must include one or more Product(s)
	// recommended to the client \n ';
	// chkObj = document.getElementById('htfcclife2');
	// page = 14;
	//
	// if(tblId == 'fnaartuplanTbl')
	// message += 'For LIFE & HEALTH INSURANCE (PLAN DETAILS) in the New
	// Purchase & Top Up ';
	//
	// else if(tblId == 'fnaartufundTbl' && rowlenobj == 0)
	// message += 'For UT and ILP (FUND DETAILS) in the New Purchase & Top Up ';
	//
	// }else if((tblId == 'fnaswrepplanTbl' && !radFlgY && rowlenobj == 0) ||
	// (tblId == 'fnaSwrepFundTbl' && !radFlgY && rowlenobj == 0) ||
	// (tblId == 'fnaswrepplanTbl' && radFlgY && rowlenobj == 0) ||
	// (tblId == 'fnaSwrepFundTbl' && radFlgY && rowlenobj == 0)){
	// message = '';
	// message += 'Representative must include one or more Product(s)
	// recommended to the client \n ';
	// chkObj = document.getElementById('htfccah2');
	// page = 16;
	//
	// if(tblId == 'fnaswrepplanTbl')
	// message += 'For LIFE & HEALTH INSURANCE (PLAN DETAILS) in the Switching &
	// Replacement ';
	//
	// else if(tblId == 'fnaSwrepFundTbl')
	// message += 'For UT and ILP (FUND DETAILS) in the Switching & Replacement
	// ';
	//
	// msgArr[cnt] = message;
	// cnt++;
	// }//end of if else
	// // if(chkObj && page){
	// // if(chkObj.checked && rowlenobj == 0){
	// // alert(message);
	// // setPageFocus(page,FNA_FORMTYPE);
	// // return false;
	// // }//end of if
	// // }//end of if
	//
	// }//end of for

	// if(msgArr.length >0){
	// for(var val =0;val<msgArr.length;msg++){
	// alert(msgArr[val]);
	// setPageFocus(16,FNA_FORMTYPE);
	// return false;
	// }//end of for
	// }//end of if

	return true;

}// end of validateAdvRecommTbls

function validateReasonforRecomm(flag) {

	var resnForRecom = document.getElementById('advrecReason');
	var resnForRecom1 = document.getElementById('advrecReason1');
	var resnForRecom2 = document.getElementById('advrecReason2');

	if(flag){
//	 SIMPLIFIED_VER ---> PAGE 8
		

//		if (!checkMandatoryField(resnForRecom, ADNT_RESNRECOM_VAL_MSG, 17,FNA_FORMTYPE))
//			return false;

		if(isEmpty(resnForRecom.value)){
			changedarrGlobal11.push(ADNT_RESNRECOM_VAL_MSG);
		}
		
		if(isEmpty(resnForRecom1.value)){
			changedarrGlobal11.push(ADNT_RESNRECOM1_VAL_MSG);
		}
		
		if(isEmpty(resnForRecom2.value)){
			changedarrGlobal11.push(ADNT_RESNRECOM2_VAL_MSG);
		}

	}
		

	
	return true;
}

function validateClntConsent() {

	var radClntAckObj = document.getElementsByName('cdackAgree');
	var txtFldClntChoice = document.getElementById('cdackAdvrecomm');

	if (!radClntAckObj[0].checked && !radClntAckObj[1].checked) {
//		alert(CLNT_ACKN_VAL_MSG);
//		setPageFocus(18, FNA_FORMTYPE, custLobArr);
//		radClntAckObj[0].focus();
//		return false;
		changedarrGlobal12.push(CLNT_ACKN_VAL_MSG);

	}// end of if

//	if (!checkMandatoryField(txtFldClntChoice, CLNT_CHOICE_REPRECOMM_VAL_MSG,18, FNA_FORMTYPE))
//		return false;
	if(isEmpty(txtFldClntChoice.value)){
		changedarrGlobal12.push(CLNT_CHOICE_REPRECOMM_VAL_MSG);
	}

	return true;
}// end of validateClntConsent

function advDeclrAndSupReview(flag) {

	var servAdvId = $("#txtFldClntServAdv").val();
	var servAdvMgrId = $("#txtFldMgrId").val();

	if (LOG_USER_MGRACS == "Y" && LOG_USER_ADVID == servAdvMgrId && flag) {


		var radSupReview = document.getElementsByName('suprevMgrFlg');

		if (!radSupReview[0].checked && !radSupReview[1].checked) {
//			alert(SUPR_REVIEW_VAL_MSG);
//			setPageFocus(19, FNA_FORMTYPE, custLobArr);
//			radSupReview[0].focus();
//			return false;

			changedarrGlobal13.push(SUPR_REVIEW_VAL_MSG);

		}// end of if

		if (radSupReview[1].checked) {
			var suprRevFld = document.getElementById('suprevFollowReason');
//			if (!checkMandatoryField(suprRevFld, SUPRREVW_RESN_VAL_MSG, 19,	FNA_FORMTYPE))
//				return false;

			if(isEmpty(suprRevFld.value)){
				changedarrGlobal13.push(SUPRREVW_RESN_VAL_MSG);
			}
		}// end of if
	}

	return true;
}// end of advDeclrAndSupReview

function chkClntRiskPref(radObj) {

	if (radObj.checked) {

		$('.spnClntRiskPref').each(function() {
			var $radObj = $(this);
			$radObj.css({
				'background-color' : 'transparent'
			});
		});
	}// end of if
}// end of chkClntRiskPref

function enabOrDisabResnMailAddr(radObjName, enabOrDisab) {

	// if(enabOrDisab == 'ENABLE')
	// alert('Attach a supporting document.(e.g.) bank statement/bill (Dated
	// within 3 months)');

	$('input[name^="' + radObjName + '"]').each(function() {

		var $radObj = $(this);

		if (enabOrDisab == 'ENABLE') {

			$radObj.prop({
				'disabled' : false
			});
			$('#divResnMailAddrLbl').css('color', 'maroon');

		} else if (enabOrDisab == 'DISABLE') {

			$radObj.prop({
				'disabled' : true,
				'checked' : false
			});
			$('#divResnMailAddrLbl').css({
				'color' : 'maroon'
			});

		}// end of if

	});

}// end of enableResnMailAddr

function validateAttachmentFlds() {

	var msgArr = "";
	;
	var cdAddrObj = document.getElementById('dfSelfMailaddrflg');//document.getElementsByName('dfSelfMailaddrflg')[2];
	if (cdAddrObj.checked) {

//		msgArr += 'Attach a supporting document.(e.g.) bank statement/bill (Dated within 3 months) \n';
		// setPageFocus(12, FNA_FORMTYPE);
		// return true;
	}// end of if

	var intrPreRad = document.getElementsByName('cdIntrprtflg');

	if (intrPreRad[0] && intrPreRad[0].checked) {
		msgArr += INTEPRE_SEL_UPLOAD + "\n";

		changedarrGlobal14.push(INTEPRE_SEL_UPLOAD);

		// if
		// (!checkMandatoryField(document.getElementById('fileIntrPrtAttchCopy'),INTEPRE_SEL_UPLOAD,13,FNA_FORMTYPE))
		// return true;
	}

	var radTPPObj = document.getElementsByName('cdTppflg');

	if (radTPPObj[0].checked) {
		// var tppUploadFld = document.getElementById('filecdTppNricCopy');
		// if(!checkMandatoryField(tppUploadFld,
		// TPP_UPLOAD_VAL_MSG,13,FNA_FORMTYPE));//return true;
		msgArr += TPP_UPLOAD_VAL_MSG + "\n";
		changedarrGlobal14.push(TPP_UPLOAD_VAL_MSG);
	}// end of if

//	if (!isEmpty(msgArr))
//		alert(msgArr);


	showFatcaAttach(document.getElementById("dfSelfUspersonflg"));
	showFatcaAttach(document.getElementById("dfSpsUspersonflg"));

	return true;
}// end of validateAttachmentFlds

function chklifeuttbl(tblId, message, page) {
	// var tblId = "fnaartuplanTbl";
	var tblobj = document.getElementById(tblId);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if ((rowlenobj == 0)) {
//		if (message)
//			alert(message);
//		setPageFocus(page, FNA_FORMTYPE, custLobArr);
//		return false;
	}
	return true;
}

function generatePDF(imgobj) {

	var fnaIdObj = imgobj.parentNode.childNodes[0];
	var custIdObj = imgobj.parentNode.parentNode.cells[2].childNodes[4];

	var fnaform = document.fnaForm;
	var fnaid = fnaIdObj.value;
	var custid = custIdObj.value;
	var formtype = document.getElementById("txtFldKycForm").value;

	var machine = "";
	machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC
			+ "&__format=pdf" + "&P_CUSTID=" + custid + "&P_FNAID=" + fnaid
			+ "&P_PRDTTYPE=" + custLobDets
			+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + formtype
			+ "&P_DISTID=" + LOGGED_DISTID;

	// alert(machine);

	creatSubmtDynaReportPDF(machine);


}

function chkNextAgeBirthday() {

//	fnaForm.dfSelfAge.value = calcAge(fnaForm.txtFldFnaClntDOB.value);
//	fnaForm.dfSpsAge.value = calcAge(fnaForm.txtFldFnaSpsDob.value);

	var clntAge = fnaForm.dfSelfAge.value;
	var spsAge = fnaForm.dfSpsAge.value;

	if (Number(clntAge) == 0 && isEmpty(fnaForm.txtFldFnaClntDOB.value)) {
		document.getElementsByName("txtFldDfSelfAgenext_032018")[0].checked = false;
		document.getElementsByName("txtFldDfSelfAgenext_032018")[1].checked = false;

//		document.getElementsByName("txtFldDfSelfAgenext")[0].disabled = false;
//		document.getElementsByName("txtFldDfSelfAgenext")[1].disabled = false;

//		$("input[name='dfSelfEdulevel']").val("");
		$("input[name='txtFldDfSelfEngProf']").attr("checked",false);
//		$("input[name='txtFldDfSelfEngProf'],input[name='dfSelfEdulevel']").attr('disabled',true);//COMMENTTED ON MAR_2018
//		$("input[name='dfSelfEdulevel']").attr("class","readOnlyText");//COMMENTTED ON MAR_2018


	} else if (Number(clntAge) >= 0 && Number(clntAge) < 62) {
		document.getElementsByName("txtFldDfSelfAgenext_032018")[0].checked = true;
		document.getElementsByName("txtFldDfSelfAgenext_032018")[0].disabled = true;
		document.getElementsByName("txtFldDfSelfAgenext_032018")[1].checked = false;
		document.getElementsByName("txtFldDfSelfAgenext_032018")[1].disabled = true;

		$("input[name='dfSelfEdulevel']").val("");
		$("input[name='txtFldDfSelfEngProf']").attr("checked",false);
//		$("input[name='txtFldDfSelfEngProf'],input[name='dfSelfEdulevel']").attr('disabled',true);////COMMENTTED ON MAR_2018
//		$("input[name='dfSelfEdulevel']").attr("class","readOnlyText");////COMMENTTED ON MAR_2018

	} else {
		document.getElementsByName("txtFldDfSelfAgenext_032018")[0].checked = false;
		document.getElementsByName("txtFldDfSelfAgenext_032018")[0].disabled = true;
		document.getElementsByName("txtFldDfSelfAgenext_032018")[1].checked = true;
		document.getElementsByName("txtFldDfSelfAgenext_032018")[1].disabled = true;

		$("input[name='txtFldDfSelfEngProf'],input[name='dfSelfEdulevel']").attr('disabled',false).attr('readOnly',false);;
//		$("input[name='dfSelfEdulevel']").attr("class","writeModeTextbox");
	}

	if (Number(spsAge) == 0 && isEmpty(fnaForm.txtFldFnaSpsDob.value)) {
		document.getElementsByName("txtFldDfSpsAgenext_032018")[0].checked = false;
		document.getElementsByName("txtFldDfSpsAgenext_032018")[1].checked = false;
//		document.getElementsByName("txtFldDfSpsAgenext")[0].disabled = false;
//		document.getElementsByName("txtFldDfSpsAgenext")[1].disabled = false;

//		$("input[name='dfSpsEdulevel']").val("");
		$("input[name='txtFldDfSpsEngProf']").attr("checked",false);
//		$("input[name='txtFldDfSpsEngProf'],input[name='dfSpsEdulevel']").attr('disabled',true);//COMM MAR_2018
//		$("input[name='dfSpsEdulevel']").attr("class","readOnlyText");//COMM MAR_2018


	} else if (Number(spsAge) >= 0 && Number(spsAge) < 62) {
		document.getElementsByName("txtFldDfSpsAgenext_032018")[0].checked = true;
		document.getElementsByName("txtFldDfSpsAgenext_032018")[1].checked = false;

		document.getElementsByName("txtFldDfSpsAgenext_032018")[0].disabled = true;
		document.getElementsByName("txtFldDfSpsAgenext_032018")[1].disabled = true;

//		$("input[name='dfSpsEdulevel']").val("");
		$("input[name='txtFldDfSpsEngProf']").attr("checked",false);
//		$("input[name='txtFldDfSpsEngProf'],input[name='dfSpsEdulevel']").attr('disabled',true);///COMM MAR_2018
//		$("input[name='dfSpsEdulevel']").attr("class","readOnlyText");///COMM MAR_2018

	} else {
		document.getElementsByName("txtFldDfSpsAgenext_032018")[0].checked = false;
		document.getElementsByName("txtFldDfSpsAgenext_032018")[1].checked = true;
		document.getElementsByName("txtFldDfSpsAgenext_032018")[0].disabled = true;
		document.getElementsByName("txtFldDfSpsAgenext_032018")[1].disabled = true;


		$("input[name='txtFldDfSpsEngProf'],input[name='dfSpsEdulevel']").attr('disabled',false).attr('readOnly',false);
//		$("input[name='dfSpsEdulevel']").attr("class","writeModeTextbox");
	}
}

function chkForContry(objname, country) {

	// var objname = contryobj.name;

	if (objname == "dfSelfNationality") {
		$("#hTxtFldFnaSelfNatlty").val(country);
	} else if (objname == "dfSpsNationality") {
		$("#hTxtFldFnaSpsNatlty").val(country);
	}

	// if(contryobj.checked){

	if (country == "SG") {

		if (objname == "dfSelfNationality") {
//			$("#spanclntnatloth").hide();
			$("#dfSelfNatyDets").val('');
			$("#dfSelfNatyDets").prop("disabled",true);
		} else if(objname == "dfSpsNationality"){
//			$("#spanspsnatloth").hide();
			$("#dfSpsNatyDets").val('');//changed by kavi 15/03/2018
			$("#dfSpsNatyDets").prop("disabled",true);
		}

	} else if(country == "SGPR" || country == "OTH"){

		if (objname == "dfSelfNationality") {
//			$("#spanclntnatloth").prop("disabled",false);
			$("#dfSelfNatyDets").prop("disabled",false);
		} else if (objname =="dfSpsNationality"){
//			$("#spanspsnatloth").show().prop("display","inline");
			$("#dfSpsNatyDets").prop("disabled",false);//changed by kavi 15/03/2018
		}

	}
	// }

}

function fnaExistPolAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;
	var jqRowlenobj = $("#"+tblid+" tbody tr:visible").length;

	if (!validateFnaExistPolDets(true))
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);


	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "center";
	cell0.style.width="55px";
	cell0.innerHTML ='<input type = "text" size="3" name="txtFldFnaWithdrawSiNo" readOnly="true" class="fpEditTblTxt" style="text-align:center" value="'+ (jqRowlenobj+1)	+ '" />';//changed by kavi on 27/09/2018

	var cell01 = rowobj.insertCell(1);
	cell01.style.textAlign = "left";
	cell01.innerHTML = '<select name="selFnaExistPolFor" class="kycSelect" style="width:85px"><option value="Self">Self</option><option value="Spouse">Spouse/Client</option></select>';

	var cell02 = rowobj.insertCell(2);
	cell02.style.textAlign = "left";
	cell02.innerHTML = '<input type = "text"  name="txtFldFnaExistPolDate"  maxlength="10" onblur="CheckDate(this,NULL);"  style="width:78px" />'
		+ '<input type="hidden" id="hFnaLiCalMode" name="hFnaExistPolMode" value="'+ INS_MODE+ '" />'
		+ '<img src="images/cal.png" title="Date" alt="Date" height="16px" width="16px" onclick="getCalendarInTbl(this,event)" style="cursor:pointer">';

	var cell03 = rowobj.insertCell(3);
	cell03.style.textAlign = "left";
	cell03.innerHTML = '<input type="text" name="selFnaExistPolPrin" class="fpEditTblTxt" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" maxlength="60"/>';

	var cell1 = rowobj.insertCell(4);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaExistPolProdName" class="fpEditTblTxt"  maxlength="300"/>'+
			'<input type = "hidden"  name="txtFldFnaExistPolMode" value="'+ INS_MODE+ '" /><input type = "hidden"  name="txtFldFnaExistPolId"  />'+
			'<input type = "hidden"  name="txtFldFnaExistPolNo" class="fpEditTblTxt"  maxlength="40"/>';


//	comboMakerById(cell03.childNodes[0], prinIdArr,"",true);

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}

	$("#page5addrow").val("CHANGED");
	checkChanges(document.getElementById("page5addrow"))
	cell02.childNodes[0].focus()
}

function fnaExistPolDelRow(tblId) {
	deleteRowInTbl(tblId,true);
	document.getElementById("page5addrow").value="CHANGED";
	checkChanges(document.getElementById("page5addrow"));
}

function validateFnaExistPolDets(flag) {

	var tblobj = document.getElementById("fnaWithdrawPolTble");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0 && flag) {
		for (var rl = 0; rl < rowlenobj; rl++) {
//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0], "Key in Policy No."))
//				return;

//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0], "Key in Date"))
//				return;

//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[2].childNodes[0], "Key in Date"))
//				return;

//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[4].childNodes[0], FNA_EXSTPOLREP_POLDETMAND))
//				return;

			if(isEmpty(tbodyobj.rows[rl].cells[4].childNodes[0].value)){
				changedarrGlobal5.push(FNA_EXSTPOLREP_POLDETMAND);
			}
		}
	}

	document.getElementById("hTxtFldFnaExistPolCnt").value = rowlenobj;

	return true;
}

function fnaCustFeedbackAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaCustFeedback(true))
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaFbCustName" class="fpEditTblTxt"  maxlength="75"/>';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaFbCustContact" class="fpEditTblTxt"  maxlength="20"/><input type = "hidden"  name="txtFldFnaFbCustMode" value="'+ INS_MODE+ '" /><input type = "hidden"  name="txtFldFnaFbCustRefId" /><input type = "hidden"  name="txtFldFnaFbCustRefFor" value="CUST" />';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type = "text"  name="txtFldFnaFbCustEmail" class="fpEditTblTxt kycEmail"  maxlength="80"/>';

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<select name="txtFldFnaFbCustRelation" class="fpSelect"/>';
	comboMaker(cell3, relArray, "", false);

	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type = "text"  name="txtFldFnaFbCustRemarks" class="fpEditTblTxt"  maxlength="300"/>';

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}

	$(".kycEmail").off("change.email").on("change.email",function(){
		validateEmailComm(this);
    });

	document.getElementById("pagefbaddrow").value="CHANGED";
	checkChanges(document.getElementById("pagefbaddrow"));
}

function validateFnaCustFeedback(flag) {

	var tblobj = document.getElementById("tblClientFbCustRef");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0 && flag) {
		for (var rl = 0; rl < rowlenobj; rl++) {
			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],"Key in Client Name"))
				return;

			if (isEmpty(tbodyobj.rows[rl].cells[1].childNodes[0].value)	&& isEmpty(tbodyobj.rows[rl].cells[2].childNodes[0].value)) {
//				alert("Key in any contact details.")
//				return false;

				changedarrGlobal15.push("Key in any contact details in feedback section.");
			}
		}
	}

	document.getElementById("hTxtFldFnaCustFeedbackCnt").value = rowlenobj;

	return true;
}

function fnaCustFeedbackDelRow(tblId) {
	deleteRowInTbl(tblId, false);

	document.getElementById("pagefbaddrow").value="CHANGED";
	checkChanges(document.getElementById("pagefbaddrow"));
}

function fnaCustFeedbackAdvAddRow(tblid) {

	var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (!validateFnaCustFeedbackAdv(true))
		return;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text"  name="txtFldFnaFbAdvName" class="fpEditTblTxt"  maxlength="75"/>';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text"  name="txtFldFnaFbAdvContact" class="fpEditTblTxt"  maxlength="20"/><input type = "hidden"  name="txtFldFnaFbAdvMode" value="'+ INS_MODE+'" /><input type = "hidden"  name="txtFldFnaFbCustAdvRefId"/><input type = "hidden"  name="txtFldFnaFbCustAdvRefFor" value="ADVISER" />';

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type = "text"  name="txtFldFnaFbAdvEmail" class="fpEditTblTxt kycEmail"  maxlength="80"/>';

	rowobj.onclick = function() {
		selectSingleRow(this);
	};

	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}

	$(".kycEmail").off("change.email").on("change.email",function(){
		validateEmailComm(this);
    });


	document.getElementById("pagefbaddrow").value="CHANGED";
	checkChanges(document.getElementById("pagefbaddrow"));
}

function validateFnaCustFeedbackAdv(flag) {

	var tblobj = document.getElementById("tblClientFbAdviserRef");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
		for (var rl = 0; rl < rowlenobj; rl++) {
//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],"Key in Adviser Name"))
//				return;

			if(isEmpty(tbodyobj.rows[rl].cells[0].childNodes[0].value)){
				changedarrGlobal15.push("Key in Adviser Name in feedback section")
			}

			if (isEmpty(tbodyobj.rows[rl].cells[1].childNodes[0].value)
					&& isEmpty(tbodyobj.rows[rl].cells[2].childNodes[0].value)) {
//				alert("Key in any contact details.")
//				return false;

				changedarrGlobal15.push("Key in any contact details in feedback section")
			}
		}
	}

	document.getElementById("hTxtFldFnaCustFeedbackAdvRefCnt").value = rowlenobj;

	return true;
}

function fnaCustFeedbackAdvDelRow(tblId) {
	deleteRowInTbl(tblId, false);

	document.getElementById("pagefbaddrow").value="CHANGED";
	checkChanges(document.getElementById("pagefbaddrow"));
}

function setExistPolDets(obj) {

	var radInvOrAHProd = document.getElementsByName('ccRepexistinvestflgYN');
	var radFlgY = false, radFlgN = false;

	if (radInvOrAHProd[0].checked && radInvOrAHProd[0].value == 'Y')
		radFlgY = true;
	if (radInvOrAHProd[1].checked && radInvOrAHProd[1].value == 'N')
		radFlgN = true;

	if (radFlgY) {
		document.getElementById("txtFldDfSelfReplExistpolY").checked = true;
		document.getElementById("txtFldDfSelfReplExistpolN").disabled = true;
	}
	if (radFlgN) {
		document.getElementById("txtFldDfSelfReplExistpolY").checked = false;
		document.getElementById("txtFldDfSelfReplExistpolN").checked = false;
		document.getElementById("txtFldDfSelfReplExistpolN").disabled = false;
	}
}

function showClientFeedback(){



	var dialog = $( "#page26" ).dialog({
		cache: false,
	      autoOpen: false,
	      title : "Client's Feedback Form",
	      height: 500,
	      width: 800,
	      modal: true,
	      buttons: { " OK ": function(){validateFeedbackDialog(dialog) }},
	      close: function(){validateFeedbackDialog(dialog)}

	    });

	if(fnaForm.clientFeedbackFlag.value != "Y" && window.confirm("Client want to give feedback?")){

		fnaForm.clientFeedbackFlag.value="Y";
		$( "#page26" ).show();
		dialog.dialog( "open" );

	}else{
		$( "#page26" ).show();
		dialog.dialog( "open" );
	}
}

function validateFeedbackDialog(dialog){
	var valid = true;

	if(!validateFnaCustFeedback(true))return;
	if(!validateFnaCustFeedbackAdv(true))return;
	dialog.dialog( "destroy" );
	$( "#page26" ).insertAfter($("#page25"));
	$( "#page26" ).hide();
	return valid;
}

function chngMailAddrRad(chkbox){
	var chkval=chkbox.value
	if(chkbox.checked){
		$("input[name='radCDClntMailngAddr_032018'").attr("checked",false);
		chkbox.checked=true;
	}else{
		if(chkval == "OTH"){
			$("#txtFldCDMailAddOth_032018").val("");
			$("#txtFldCDMailAddOth_032018").attr('readonly', true);
			$("#txtFldCDMailAddOth_032018").attr('class', '');
			$("#txtFldCDMailAddOth_032018").addClass("readOnlyText");
		}
	}
}



function changeMgrDets(selobj){
	if(window.confirm("Do you want to change the manager for this archive?")){
		document.getElementById("txtFldMgrId").value=selobj.value;
	}else{
		selobj.value = document.getElementById("txtFldMgrId").value;
		return false;
	}

	if(!isEmpty(selobj.value)){
	document.getElementById("txtFldSupVName").value=selobj.options[selobj.selectedIndex].innerHTML;
	}else{
		document.getElementById("txtFldSupVName").value="";
	}

}

function chngChktoRadFATCA(chkbox,clnttype){

	if(clnttype == "client"){

		if(chkbox.checked){
			$("input[name='dfSelfUsnotifyflg'], input[name='dfSelfUspersonflg']").attr("checked",false);
			chkbox.checked=true;
			
			var chkboxname = $(chkbox).prop("name");
			if(chkboxname == 'dfSelfUsnotifyflg' && $("input[name='dfSelfUsnotifyflg']").is(":checked")){
				
				
				 
					var tblobj = document.getElementById("fnaFatcaTbl");
					var tbodyobj = tblobj.tBodies[0];
					var rowlenobj = tbodyobj.rows.length;
					if(rowlenobj == 0){
						var selfic = $("#dfSelfNric").val();
						fnaFatcaAddRow("fnaFatcaTbl") ;
						tbodyobj.rows[0].cells[0].childNodes[0].value="Singapore";
						tbodyobj.rows[0].cells[1].childNodes[0].value=selfic;
						
					}
				
			}
		
			

			enabOrDisbTxtFld(document.getElementById("dfSelfUspersonflg"),'dfSelfUstaxno','tdUSClntLblTxt');
		}
	}

	if(clnttype == "spouse"){
		if(chkbox.checked){
			$("input[name='dfSpsUsnotifyflg'], input[name='dfSpsUspersonflg']").attr("checked",false);
			chkbox.checked=true;
			
			var chkboxname = $(chkbox).prop("name");
			if(chkboxname == 'dfSpsUsnotifyflg' && $("input[name='dfSpsUsnotifyflg']").is(":checked")){
				
				var tblobjSps = document.getElementById("fnaFatcaTblSps");
				var tbodyobjSps = tblobjSps.tBodies[0];
				var rowlenobjSps = tbodyobjSps.rows.length;
				if(rowlenobjSps == 0){
					var spsic = $("#dfSpsNric").val();
					fnaFatcaAddRow("fnaFatcaTblSps") ;
					tbodyobjSps.rows[0].cells[0].childNodes[0].value="Singapore";
					tbodyobjSps.rows[0].cells[1].childNodes[0].value=spsic;
					
				}
			
		}

			enabOrDisbTxtFld(document.getElementById("dfSpsUspersonflg"),'dfSpsUstaxno','tdUSSpsLblTxt');
		}
	}
}

function setOnlyContactChkBox(){

	var spsName = fnaForm.dfSpsName.value;

//dfSelfHome^dfSelfMobile^dfSelfOffice^dfSelfPersemail
//	dfSpsHome^dfSpsHp^dfSpsOffice^dfSpsPersemail
	var clntarr = ["dfSelfHome","dfSelfMobile","dfSelfOffice","dfSelfPersemail"];
	var spsarr = ["dfSpsHome","dfSpsHp","dfSpsOffice","dfSpsPersemail"];

	var clnttot=0,spstot=0;


	$(clntarr).each(function(index) {
		if(!isEmpty($("#"+clntarr[index]).val())){
			clnttot=clnttot+1;
		}
	});

	$(spsarr).each(function(index) {
		if(!isEmpty($("#"+spsarr[index]).val())){
			spstot=spstot+1;
		}
	});

	if(clnttot == 1 || spstot == 1){
		$("#dfSelfOnecontflg").attr("checked",true);
	}


}

//added by kavi 02/03/2018

function page26fixedHeader() {
	if (!page26Flag) {

		if (document.getElementById("page29")) {

			$('#fnaartuplanTbls').fixheadertable({
				// caption : 'Alterations',
				colratio : [ 55, 150, 500, 150, 150, 150, 150, 150 ],
				height : 220,
				ieHeight : 200,// johsnon on 15042015
				width : 915,
				zebra : false,
				sortable : false,
				pager : false,
				rowsPerPage : 10,
				resizeCol : true,
				tblId : 'fnaartuplanTbls'
			});

			document.getElementById("fnaartuplanTblsheaderTbl").style.marginRight = "13px";
			// document.getElementById("headerTbl").parentNode.childNodes[0].style.overflow="scroll";
			document.getElementById("fnaartuplanTblsheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
			document.getElementById("fnaartuplanTblsmainwrapper").style.height = "248px";
			document.getElementById("fnaartuplanTblstampon").style.height = "255px";

			if (getBrowserApp == "Microsoft Internet Explorer") {
				document.getElementById("fnaartuplanTblsheaderTbl").style.marginRight = "15px";
				var Tbl = document.getElementById("fnaartuplanTblsheaderTbl").childNodes[0].childNodes[0];
				var bodyContainer = document
						.getElementById("fnaartuplanTblsbodyContainer");
				bodyContainer.style.overflowY = 'scroll';
				var headerTbl = document
						.getElementById("fnaartuplanTblsheaderTbl");
				// bodyContainer.onscroll = function(e){
				// Tbl.style.left = '-' + this.scrollLeft +'px';
				// };
			}

			// fixedHeader('fnaartufundTbl','1','100%','0px','100%','15.8em','2','0px','98.6%','8em','98.6%',true,true,4,2);
//			fixedHeader('fnaartuplanTbls', '1', '100%', '0px', '100%', '15.8em',
//					'2', '0px', '98.4%', '8em', '98.4%', true, true, 4, 2);

			page26Flag = true;
		}
	}
}



function pageDepfixedHeader() {
	if (!pageDepFlag) {

		if (document.getElementById("page27")) {

			fixedHeader('fnaDepnTbl','1','100%','0px','100%','18em','2','0px','100%','3.3em','100%',false,false,0,0);
			fixedHeader('fnaWithdrawPolTble','1','100%','0px','100%','18em','2','0px','100%','3.3em','100%',false,false,0,0);

//			$('#fnaDepnTbl').tableFixedHeader({	scrollContainer: $("#fnaDepnTblContainer")});

//			$('#fnaWithdrawPolTble').tableFixedHeader();


//			$('#fnaDepnTbl').fixheadertable({
//				colratio : [50,240,100,50,150,100,200 ],
//				height : 320,
//				ieHeight : 300,
//				width : 1050,
//				zebra : false,
//				sortable : false,
//				pager : false,
//				rowsPerPage : 10,
//				resizeCol : true,
//				tblId : 'fnaDepnTbl'
//			});
//
//			document.getElementById("fnaDepnTblheaderTbl").style.marginRight = "13px";
//			document.getElementById("fnaDepnTblheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
//			document.getElementById("fnaDepnTblmainwrapper").style.height = "248px";
//			document.getElementById("fnaDepnTbltampon").style.height = "255px";
//
//			if (getBrowserApp == "Microsoft Internet Explorer") {
//				document.getElementById("fnaDepnTblheaderTbl").style.marginRight = "15px";
//				var Tbl = document.getElementById("fnaDepnTblheaderTbl").childNodes[0].childNodes[0];
//				var bodyContainer = document.getElementById("fnaDepnTblbodyContainer");
//				bodyContainer.style.overflowY = 'scroll';
//				var headerTbl = document.getElementById("fnaDepnTblheaderTbl");
//			}
//			alert("")
//
//			$('#fnaWithdrawPolTble').fixheadertable({
//				colratio : [50,120,280],
//				height : 320,
//				ieHeight : 300,
//				width : 908,
//				zebra : false,
//				sortable : false,
//				pager : false,
//				rowsPerPage : 10,
//				resizeCol : true,
//				tblId : 'fnaWithdrawPolTble'
//			});
//
//			document.getElementById("fnaWithdrawPolTbleheaderTbl").style.marginRight = "13px";
//			document.getElementById("fnaWithdrawPolTbleheaderTbl").parentNode.childNodes[1].style.overflow = "scroll";
//			document.getElementById("fnaWithdrawPolTblemainwrapper").style.height = "248px";
//			document.getElementById("fnaWithdrawPolTbletampon").style.height = "255px";
//
//			if (getBrowserApp == "Microsoft Internet Explorer") {
//				document.getElementById("fnaWithdrawPolTbleheaderTbl").style.marginRight = "15px";
//				var Tbl = document.getElementById("fnaWithdrawPolTbleheaderTbl").childNodes[0].childNodes[0];
//				var bodyContainer = document.getElementById("fnaWithdrawPolTblebodyContainer");
//				bodyContainer.style.overflowY = 'scroll';
//				var headerTbl = document.getElementById("fnaWithdrawPolTbleheaderTbl");
//			}

//			fixedHeader('fnaDepnTbl','1','100%','0px','26.8%','92px','2','0px','25.9%','2.8em','96.0%',false,false,0,0);
//			fixedHeader('fnaWithdrawPolTble','1','100%','0px','26.8%','92px','2','0px','25.9%','2.8em','96.0%',false,false,0,0);


			pageDepFlag = true;
		}
	}
}



function fnaDepAddRow(){


	var tblobj = document.getElementById("fnaDepnTbl");
	var tbodyobj = tblobj.tBodies[0];


	if(!validateFnaDepentDets(true))
		return;

	var rowlenobj = tbodyobj.rows.length;
	var jqRwLen=$("#fnaDepnTbl tbody tr:visible").length;
	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell = rowobj.insertCell(0);
	cell.innerHTML =  '<input type="text" name="txtFldFnaDepSino" readonly="true" value="'+(Number(jqRwLen)+1)+'" style="width:52px;text-align:center;"/>'
	cell.style.textAlign = "center";

	var cell0 = rowobj.insertCell(1);
	cell0.innerHTML = '<input type = "text" class="fpNonEditTblTxt"   name="txtFldFnaDepntName" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />'
			+ '<input type="hidden" name="txtFldFnaDepMode" value="'+ INS_MODE+ '"/>'
			+ '<input type="hidden" name="txtFldFnaDepId"/>';

	var cell3 = rowobj.insertCell(2);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<select name="selFnaDepntSex"  class="kycSelect"></select>';

	var cell2 = rowobj.insertCell(3);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type="text" name="txtFldFnaDepntAge" maxlength="3"  class="fpNonEditTblTxt"  size="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"   onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';


	var cell1 = rowobj.insertCell(4);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<select name="selFnaDepntRel" class="kycSelect" ></select>';

	var cell4 = rowobj.insertCell(5);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type = "text"  class="fpNonEditTblTxt"  name="txtFldFnaDepntYr2Sup" maxlength="3" onkeyup="chkDecimal(this,3,0);" onpaste="PasteOnlyNum(this,0);"    onkeydown="chkDecimal(this,3,0);" onkeypress="return blockNonNumbers(this,event,false,false);"/>';


	var cell5 = rowobj.insertCell(6);
	cell5.style.textAlign = "left";
	cell5.innerHTML ='<input type = "text" class="fpNonEditTblTxt"  name="txtFldFnaDepntOccp" maxlength="60"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />'+
		'<input type = "hidden" class="fpNonEditTblTxt"  name="txtFldFnaDepntMonthContr" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />'+
		 '<input type = "hidden" class="fpNonEditTblTxt"  name="txtFldFnaDepntMonthSelf" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />'+
		 '<input type = "hidden" class="fpNonEditTblTxt"  name="txtFldFnaDepntMonthSps" maxlength="22" onkeyup="chkDecimal(this,16,4);" onpaste="PasteOnlyNum(this,4);"   onkeydown="chkDecimal(this,16,4);" onkeypress="return blockNonNumbers(this,event,true,false);" />';


	comboMaker(cell1, relArray, "", false);

	comboMakerByString(STR_SEX, cell3.childNodes[0], "",true);

	rowobj.onclick = function() {
		selectSingleRow(this);
	};


	var totalcell = rowobj.cells.length;
	for(var cel=0;cel<totalcell;cel++){
		if(rowobj.cells[cel].childNodes[0]){
			rowobj.cells[cel].childNodes[0].addEventListener("change",function(e){
				e = e || event;
				checkChanges(this);
			},false);
		}
	}

	$("#page5addrow").val("CHANGED");
	checkChanges(document.getElementById("page5addrow"))
	cell0.childNodes[0].focus()

}
function fnaDepDelRow(tblId){
	deleteRowInTbl(tblId, true);
	$("#page5addrow").val("CHANGED");
	checkChanges(document.getElementById("page5addrow"))
}

function fwrDepDelRow(){

	$('#fwrDepTble tbody tr').each(function(){

		var mode=$(this).find('td:first').find('input:eq(0)').val();
		if($(this).hasClass('active')){
			if(mode.toUpperCase()=='I'){
				$(this).remove();
			}

			if(mode.toUpperCase()=='Q'){
				$(this).hide()
				$(this).find('td:first').find('input:eq(0)').val('D');
				$(this).removeClass('active')
			}
		}
	});

	reorderSerialNo('fwrDepTble');
}

function reorderSerialNo(tbl){
	var count=1
	$('#'+tbl+' tbody tr').each(function(){
		if($(this).is(':visible')){
		  $(this).find('td:first').find('span').text(count);
		  count++;
		}
	});
}

function showDepntSection(yesorno){
	if(!isEmpty(yesorno.value) && yesorno.value =="Y"){
		$("#fnaDepntBtnGrps").css({"visibility":"visible"});
	}else{
		$("#fnaDepntBtnGrps").css({"visibility":"hidden"});

		var table = document.getElementById("fnaDepnTbl");
		var rowCount = table.tBodies[0].rows.length;
		if(rowCount>0){
			/*for(var r=0;r<rowCount;r++){*/
			/*for(var r=rowCount-1;r>=0;r--){	*/
				/*document.getElementById("hSelectRow").value=table.tBodies[0].rows[r].rowIndex-1;*/
				deleteAllRows("fnaDepnTbl")
			/*}		*/
		}
	}
}



function showFinSituSection(yesorno){
	if(!isEmpty(yesorno.value) && yesorno.value == "Y"){
		$(".kycFinCommitFlds").removeClass("readOnlyText").addClass("writeModeTextbox")
		.prop("readOnly",false)
	}else{
		$(".kycFinCommitFlds").addClass("readOnlyText")
		.prop({"readOnly":true,"background-color":"#DAE0D6"}).val("")

		$(".kycFinCommitFldsTot").val("");
	}
}
function showWithdrawPol(objs){

	var withdrawClient = $("#dfSelfReplExistpol");
	var withdrawSps = $("#dfSpsReplExistpol");

	if((!isEmpty(withdrawClient.val()) || !isEmpty(withdrawSps.val())) && ((withdrawClient.val() == "Y" || withdrawSps.val() =="Y"))){
		$("#fnaWithdTerBtnGrps").css({"visibility":"visible"})
	}else{
		$("#fnaWithdTerBtnGrps").css({"visibility":"hidden"});
		var table = document.getElementById("fnaWithdrawPolTble");
		var rowCount = table.tBodies[0].rows.length;
		if(rowCount>0){
		/*	for(var r=rowCount-1;r>=0;r--){
				document.getElementById("hSelectRow").value=table.tBodies[0].rows[r].rowIndex-1;*/
			deleteAllRows("fnaWithdrawPolTble")
		/*	}	*/
		}
//		deleteAllRows("fnaWithdrawPolTble");
	}

}

function validateFnaWellnesReview(flag){

	var depntSec = $("#fwrDepntflg").val();
	var finCommSec = $("#fwrFinassetflg").val();
	var clntPolRepSec = $("#dfSelfReplExistpol").val();
	var spPolRepSec = $("#dfSpsReplExistpol").val();

	if(!isEmpty(depntSec) && depntSec == "Y" && flag ){

		var tblobj = document.getElementById("fnaDepnTbl");
		var tbodyobj = tblobj.tBodies[0];
		var rowlenobj = tbodyobj.rows.length;
		if (rowlenobj == 0){
//			setPageFocus(27,FNA_FORMTYPE); //added by kavi 15/13/2018
//			alert(FNA_DEPENT_NODET);
//			return false;
			changedarrGlobal5.push(FNA_DEPENT_NODET);
		}

	}
	if(!isEmpty(finCommSec) && finCommSec == "Y"){
		$(".kycFinCommitFlds").removeClass("readOnlyText").addClass("writeModeTextbox")
		.prop("readOnly",false)
	}else{
		$(".kycFinCommitFlds").addClass("readOnlyText")
		.prop({"readOnly":true,"background-color":"#DAE0D6"}).val("")
	}

	if((!isEmpty(clntPolRepSec) || !isEmpty(spPolRepSec)) && ( (clntPolRepSec == "Y" || spPolRepSec =="Y"))){//condition changed by kavi 15/03/2018

		$("#fnaWithdTerBtnGrps").css({"visibility":"visible"});


		var tblobj = document.getElementById("fnaWithdrawPolTble");
		var tbodyobj = tblobj.tBodies[0];
		var rowlenobj = tbodyobj.rows.length;
		if(rowlenobj == 0){

			changedarrGlobal5.push(FNA_EXSTPOLREP_POLDET);

		}
	}


	return true;
}

function chkAdvDecOptions(chkbox){

	var advDecOpt = $("#advdecOptions");
	var jsonvalues = JSON.parse(isEmpty(advDecOpt.val()) ? "{}" : advDecOpt.val());
	var chkd = chkbox.checked;
	var val = chkbox.value;

	var newobj=jsonvalues;
	newobj[val]=(chkd == true ? "Y" :"N");
	advDecOpt.val(JSON.stringify(newobj));


}

function chkAdvAppTypeOptions(chkbox){

	var advAppTypeOpt = $("#apptypesClient");
	var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	var chkd = chkbox.checked;
	var val = $(chkbox).attr("data");

	var newobj=jsonvalues;
	newobj[val]=(chkd == true ? "Y" :"N");
	advAppTypeOpt.val(JSON.stringify(newobj));


}

function chkJsnOptions(chkbox,jsnFld){

	var advAppTypeOpt = $("#"+jsnFld+"");
	var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	var chkd = chkbox.checked;
	var val = $(chkbox).attr("data");

	var newobj=jsonvalues;
	newobj[val]=(chkd == true ? "Y" :"N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}

function chkJsnOptionsSingle(chkbox,jsnFld){

	$(chkbox).closest("tr").find("input:checkbox").each(function(){
		var chkName=$(this).attr("name");

		if(!$(this).is($(chkbox))){
			this.checked=false
		}

		if(chkName=="chkCDClntOth" &&  this.checked==false){
			$("#dfSelfTaxresOth").removeClass("writeModeTextbox").addClass("readOnlyText")
			.attr('readOnly',true).val("");
		}

		if(chkName=="chkCDSpsOth" &&  this.checked==false){
			$("#dfSpsfTaxresOth").removeClass("writeModeTextbox").addClass("readOnlyText")
			.attr('readOnly',true).val("");
		}
	});

	/*if(chkName!="chkCDClntOth" || chkName!="chkCDSpsOth"){
		if(chkName)
		$("#"+Ele+"").removeClass("writeModeTextbox").addClass("readOnlyText")
		.attr('readOnly',true).val("");
	}*/

//	if(!chkbox.checked)chkbox.checked=true;

	var advAppTypeOpt = $("#"+jsnFld+"");
	advAppTypeOpt.val("");
	var jsonvalues = JSON.parse(isEmpty(advAppTypeOpt.val()) ? "{}" : advAppTypeOpt.val());
	var chkd = chkbox.checked;
	var val = $(chkbox).attr("data");

	var newobj=jsonvalues;
	newobj[val]=(chkd == true ? "Y" :"N");
	advAppTypeOpt.val(JSON.stringify(newobj));
}

function chkSelfSpsOthrValid(chkbox,Ele){
	var chkd = chkbox.checked;
	if(chkd){
		$("#"+Ele+"").removeClass("readOnlyText").addClass("writeModeTextbox")
		.attr('readOnly',false).prop('disabled',false).attr('disabled',false);
	}else{
		$("#"+Ele+"").removeClass("writeModeTextbox").addClass("readOnlyText")
		.attr('readOnly',true).val("");
	}
}

//added by kavi 15/03/2018
function calcAgeFld(dob,retnelm) {

	var age = 0;
	if (!isEmpty(dob)) {

		var birthday = dob.split("/")[2] + "-" + dob.split("/")[1] + "-"+ dob.split("/")[0];
		var now = new Date();
		var past = new Date(birthday);

		age = now.getFullYear() - past.getFullYear();


	}
	if(dob.length>0)
	$('input[name="'+retnelm+'"]').val(age);
}

//added by kavi 16/3/2018
function setJsonObjToElm(cont, data) {
	var val = JSON.stringify(data);
	$('input[name="' + cont + '"]').val("");
	if (!isEmpty(data) && !jQuery.isEmptyObject(val)) {
		$('input[name="' + cont + '"]').val(val);
		$.each(data, function(obj, val) {
			if (val == 'Y')
				$('input[data="' + obj + '"]').prop("checked", true);
			else
				$('input[data="' + obj + '"]').prop("checked", false);
		});
	}
}

function setEduLevel(rad,txtName){
	if(rad.checked){
		document.getElementsByName(txtName)[0].value=rad.value;
	}
	checkChanges(document.getElementsByName(txtName)[0])
}

function setDisCloseVal(chkobj){
	if(chkobj.checked){
		chkobj.nextSibling.value=chkobj.getAttribute("datavalue");
		chkobj.value="Y";
	}else{
		chkobj.nextSibling.value="";
		chkobj.value="N";
	}
}


function calcAgeFld(dob,retnelm) {
    var age = 0;
    if (!isEmpty(dob)) {
        var birthday = dob.split("/")[2] + "-" + dob.split("/")[1] + "-"+ dob.split("/")[0];
        var now = new Date();
        var past = new Date(birthday);

        age = now.getFullYear() - past.getFullYear();
    }
    if(dob.length>0)
    $('input[name="'+retnelm+'"]').val(age);
}

function showFatcaAttach(chkbox){
	if(chkbox.checked){

		if(chkbox.getAttribute("id")=='dfSelfUspersonflg'){
//			alert(FNA_FATCA_ATTACH+" for Client.");
//			changedarrGlobal14.push(FNA_FATCA_ATTACH+" for Client(1)");//dec2019
		}else{
//			alert(FNA_FATCA_ATTACH+" for Spouse.");
//			changedarrGlobal14.push(FNA_FATCA_ATTACH+" for Client(2)");//dec2019
		}

	}
	return true
}
function calcFinCommit(txtobj,op){


	var txtval = Number(txtobj.value);

	var sa  = $("#cfSelfAnnlinc").val()
	var sb = $("#cfSelfCpfcontib").val();
	var sc = $("#cfSelfEstexpense").val();
	var stot = Number(sa)-Number(sb)-Number(sc);

	$("#cfSelfSurpdef").val(stot);

	var spa  = $("#cfSpsAnnlinc").val()
	var spb = $("#cfSpsCpfcontib").val();
	var spc = $("#cfSpsEstexpense").val();
	var sptot = Number(spa)-Number(spb)-Number(spc);

	$("#cfSpsSurpdef").val(sptot);

	var sfa  = $("#cfFamilyAnnlinc").val()
	var sfb = $("#cfFamilyCpfcontib").val();
	var sfc = $("#cfFamilyEstexpense").val();
	var sftot = Number(sfa)-Number(sfb)-Number(sfc);

	$("#cfFamilySurpdef").val(sftot);





}

function calcAsset(txtobj){
	var txtval = Number(txtobj.value)

	var sa  = $("#alSelfTotasset").val()
	var sa1  = $("#alSpsTotasset").val()
	var sa2  = $("#alFamilyTotasset").val()

	var sb  = $("#alSelfTotliab").val()
	var sb1  = $("#alSpsTotliab").val();
	var sb2 = $("#alFamilyTotliab").val()

	$("#alSelfNetasset").val(Number(sa)-Number(sb))
	$("#alSpsNetasset").val(Number(sa1)-Number(sb1))
	$("#alFamilyNetasset").val(Number(sa2)-Number(sb2))

}
function calcCombined(txtobj){

	var parrow= txtobj.parentNode.parentNode;

	var client = parrow.cells[1].childNodes[0];

	var spouse = parrow.cells[2].childNodes[0]
	var combined = parrow.cells[3].childNodes[0]
	combined.value = Number(client.value)+Number(spouse.value)


	var sfa  = $("#cfFamilyAnnlinc").val()
	var sfb = $("#cfFamilyCpfcontib").val();
	var sfc = $("#cfFamilyEstexpense").val();
	var sftot = Number(sfa)-Number(sfb)-Number(sfc);

	$("#cfFamilySurpdef").val(sftot);


	var sa2  = $("#alFamilyTotasset").val()
	var sb2 = $("#alFamilyTotliab").val()
	$("#alFamilyNetasset").val(Number(sa2)-Number(sb2))




}

function showAttachIntprt(rad){

	var intrPreRad = document.getElementsByName('cdIntrprtflg');

	if (intrPreRad[0] && intrPreRad[0].checked) {
		alert(INTEPRE_SEL_UPLOAD);
	}

}

function isAllValidationDone(){
	
	
	if(
		changedarrGlobal1.length == 0 &&
		changedarrGlobal2.length == 0 &&
		changedarrGlobal3.length == 0 &&
		changedarrGlobal4.length == 0 &&
		changedarrGlobal5.length == 0 &&
		changedarrGlobal6.length == 0 &&
		changedarrGlobal7.length == 0 &&
		changedarrGlobal8.length == 0 &&
		changedarrGlobal9.length == 0 &&
		changedarrGlobal10.length == 0 &&
		changedarrGlobal11.length == 0 &&
		changedarrGlobal12.length == 0 &&
		changedarrGlobal13.length == 0 &&
		changedarrGlobal14.length == 0 &&
		changedarrGlobal15.length == 0 ){
		return true;
	}else{
		return false;
	}
	
}


function chkAllDataSaved(machine,pdfSts,message){
	closePopupBox();
	var allpassed = true;

	changedarrGlobal1.length=0;changedarrGlobal2.length=0;changedarrGlobal3.length=0;
	changedarrGlobal4.length=0;changedarrGlobal5.length=0;changedarrGlobal6.length=0;
	changedarrGlobal7.length=0;changedarrGlobal8.length=0;changedarrGlobal9.length=0;
	changedarrGlobal10.length=0;changedarrGlobal11.length=0;changedarrGlobal12.length=0;
	changedarrGlobal13.length=0;changedarrGlobal14.length=0;changedarrGlobal15.length=0;
	
	
var pendingmsg = '<h3 style="color: red;"><img src="images/close.png" style="width:15px;height:13px;vertical-align:middle" />PENDING</h3>'
	$("#page1msg").html("");$("#page2msg").html("");$("#page3msg").html("");
	$("#page4msg").html("");$("#page5msg").html("");$("#page6msg").html("");
	$("#page7msg").html("");$("#page8msg").html("");$("#page9msg").html("");
	$("#page10msg").html("");$("#page11msg").html("");$("#page12msg").html("");
	$("#page13msg").html("");$("#page14msg").html("");$("#page15msg").html("");
	$("#page16msg").html("");$("#pagesignmsg").html("");

	if(changedarrGlobal1.length ==0){
		$("#page1msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page1msg").append('<h3 style="color: red;">PENDING<img src="images/close.png" width="20px" /></h3><br/>');
		allpassed = false;
	}
	
	
	
	if (FNA_FORMTYPE == SIMPLIFIED_VER) {
		if (!validateFnaSelfSpsDets(true))
			return;
	}
	
	if(changedarrGlobal2.length ==0){
		$("#page2msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page2msg").append(pendingmsg);
		for(var i2=0;i2<changedarrGlobal2.length;i2++){
			var str = changedarrGlobal2[i2];
			var splt = str.split("^")
			$("#page2msg").append((i2+1)+"."+splt[0] +"<br/>");
		}
		allpassed = false;
	}

	if (!validIntrPreFun(true)){return;}
	if (!validateTPPBenifPEP(true)){return;}
//	changedarrGlobal3.length =0;

	if(changedarrGlobal3.length ==0){
		$("#page3msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{

		$("#page3msg").append(pendingmsg);
		for(var i3=0;i3<changedarrGlobal3.length;i3++){
			var str = changedarrGlobal3[i3];
			var splt = str.split("^")
			$("#page3msg").append((i3+1)+"."+splt[0] +"<br/>");
		}

		allpassed = false;

	}
	if (!validateUSTaxPrsn(true)){return;}
	if (!validateFnaFatcaTaxDets(true)){return;}
//	changedarrGlobal4.length =0;
	if(changedarrGlobal4.length ==0){
		$("#page4msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page4msg").append(pendingmsg);
		for(var i4=0;i4<changedarrGlobal4.length;i4++){
			var str = changedarrGlobal4[i4];
	//		var splt = str.split("^")
			$("#page4msg").append((i4+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}
	if(!validateFnaWellnesReview(true)){return;}
	if (!validateFnaDepentDets(true)){return;}
	if (!validateFnaExistPolDets(true)){return;}
//	changedarrGlobal5.length=0;
	if(changedarrGlobal5.length ==0){
		$("#page5msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page5msg").append(pendingmsg);
		for(var i5=0;i5<changedarrGlobal5.length;i5++){
			var str = changedarrGlobal5[i5];
	//		var splt = str.split("^")
			$("#page5msg").append((i5+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}
//	changedarrGlobal6.length =0;
	if(changedarrGlobal6.length ==0){
		$("#page6msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page6msg").append(pendingmsg);
		for(var i6=0;i6<changedarrGlobal6.length;i6++){
			var str = changedarrGlobal6[i6];
	//		var splt = str.split("^")
			$("#page6msg").append((i6+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}

	if (!validAdvAndRecmChkboxes()){return;}
	if (!validateAdvRecommTbls()){return;}
//	changedarrGlobal7.length=0;
	if(changedarrGlobal7.length ==0){
		$("#page7msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page7msg").append(pendingmsg);
		for(var i7=0;i7<changedarrGlobal7.length;i7++){
			var str = changedarrGlobal7[i7];
	//		var splt = str.split("^")
			$("#page7msg").append((i7+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}
//	changedarrGlobal8.length=0;
	if(changedarrGlobal8.length ==0){
		$("#page8msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page8msg").append(pendingmsg);
		for(var i8=0;i8<changedarrGlobal8.length;i8++){
			var str = changedarrGlobal8[i8];
	//		var splt = str.split("^")
			$("#page8msg").append((i8+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}
//	changedarrGlobal9.length=0;
	if(changedarrGlobal9.length ==0){
		$("#page9msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page9msg").append(pendingmsg);
		for(var i9=0;i9<changedarrGlobal9.length;i9++){
			var str = changedarrGlobal9[i9];
	//		var splt = str.split("^")
			$("#page9msg").append((i9+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}
	changedarrGlobal10.length=0;
	if(changedarrGlobal10.length ==0){
		$("#page10msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page10msg").append(pendingmsg);
		for(var i10=0;i10<changedarrGlobal10.length;i10++){
			var str = changedarrGlobal10[i10];
	//		var splt = str.split("^")
			$("#page10msg").append((i10+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}


	if (!validateReasonforRecomm(true)){return;}
//	changedarrGlobal11.length =0;

	if(changedarrGlobal11.length ==0){
		$("#page11msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page11msg").append(pendingmsg);
		for(var i11=0;i11<changedarrGlobal11.length;i11++){
			var str = changedarrGlobal11[i11];
	//		var splt = str.split("^")
			$("#page11msg").append((i11+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}


	if (!validateClntConsent(true)){return;}
//	changedarrGlobal12.length=0;

	if(changedarrGlobal12.length ==0){
		$("#page12msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page12msg").append(pendingmsg);
		for(var i12=0;i12<changedarrGlobal12.length;i12++){
			var str = changedarrGlobal12[i12];
	//		var splt = str.split("^")
			$("#page12msg").append((i12+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}



	if (!advDeclrAndSupReview(true)){return;}
//	changedarrGlobal13.length=0;
	if(changedarrGlobal13.length ==0){
		$("#page13msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page13msg").append(pendingmsg);
		for(var i13=0;i13<changedarrGlobal13.length;i13++){
			var str = changedarrGlobal13[i13];
	//		var splt = str.split("^")
			$("#page13msg").append((i13+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}
	if (!validateAttachmentFlds()){return;}
//	changedarrGlobal14.length=0;
	if(changedarrGlobal14.length ==0){
		$("#page14msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page14msg").append('<h3 style="color: red;">Info<img src="images/info.png" width="20px" /></h3><br/>');
		for(var i14=0;i14<changedarrGlobal14.length;i14++){
			var str = changedarrGlobal14[i14];
	//		var splt = str.split("^")
			$("#page14msg").append((i14+1)+"."+str +"<br/>");
		}
		/*allpassed = false;*/
	}
	if (!validateFnaCustFeedback(true)){return;}
	if (!validateFnaCustFeedbackAdv(true)){return}
//	changedarrGlobal15.length=0;

	if(changedarrGlobal15.length ==0){
		$("#page15msg").html('<h3>Completed.</h3><img src="images/historyOk.png" width="20px" />');
	}else{
		$("#page15msg").append(pendingmsg);
		for(var i15=0;i15<changedarrGlobal15.length;i15++){
			var str = changedarrGlobal15[i15];
	//		var splt = str.split("^")
			$("#page15msg").append((i15+1)+"."+str +"<br/>");
		}
		allpassed = false;
	}
var chkanychange = $("#chkanychange");
var jsonvalues = JSON.parse(isEmpty(chkanychange.val()) ? "{}" : chkanychange.val());
if(!isEmpty(chkanychange.val())){
	$("#page16msg").html( '<h3 style="color: red;font-size: 15pt;"> '+FNA_NONVALIDATION_FIELDS+' Please save and then generate PDF.</h3>');
	allpassed = false;
}else{
	$("#page16msg").html('&nbsp;')
}

console.log("allpassed---------------------->"+allpassed)
if(!allpassed){
var dialog = $("#reportvaliationPopup").dialog({
	autoOpen : false,
	height : '600',
	width : '90%',
	modal : true,
	resizable : false,
	// screen center
	my : "center",
	at : "center",
	of : window,
	// screen center
	buttons : {
		" OK " : function() {
				dialog.dialog("close");
			}
	},
	close : function() {
	}
});

var isAllKeyedin = isAllValidationDone();

//alert(isAllKeyedin)
console.log("isAllKeyedin----------->"+isAllKeyedin)

if(isAllKeyedin){
	
	var checkChange=$("#chkanychange").val();
	
	
	
	if(!isEmpty(checkChange) && !$.isEmptyObject($.parseJSON(checkChange)) && NTUCChkChngSts){

//		if(!chkAllDataSaved("",false," You have modified the current FNA Form.<br/> Click OK to save and proceed.<br/>Click Cancel to ignore changes."))return;
		
		
		$('#reportvaliationPopup').dialog('option', 'title', 'E-Submit validation');
		$("#page16msg").html("");
		$("#page16msg").html( '<h3 style="color: red;font-size: 15pt;"></h3>');
		
		var clientsigned=sofarClientSignatureDone();
		if( clientsigned > 0 ){
			$("#reportvaliationPopup table:first tbody tr:first h3:first").html(FNA_CONS_MODIFY_SIGN_INFO)
			$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();
			$( "#reportvaliationPopup" ).dialog( "option", "height",280);
			$( "#reportvaliationPopup" ).dialog( "option", "width",500);

		}else{
			$("#reportvaliationPopup table:first tbody tr:first h3:first").html(FNA_CONS_MODIFY_INFO)
			$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();
			$( "#reportvaliationPopup" ).dialog( "option", "height",280);
			$( "#reportvaliationPopup" ).dialog( "option", "width",500);

		}
		
		if(pdfSts){
			$("#reportvaliationPopup table:first tbody tr:first h3:first").html(FNA_CONS_MODIFY_PDF);
			$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();
			$( "#reportvaliationPopup" ).dialog( "option", "height",280);
			$( "#reportvaliationPopup" ).dialog( "option", "width",500);

		}
		
//		$("#reportvaliationPopup table:first tbody>tr").not(":first").show();
		
		$( "#reportvaliationPopup" ).dialog( "option", "buttons",
				[
				    {
				      text: "Proceed to Save",
				      click: function() {
								$('#reportvaliationPopup').dialog("close");
								saveOrUpdate();
				      }

				    },
				    
				    
				    {
					      text: "Cancel",
					      click: function() {
								$('#reportvaliationPopup').dialog("close");
					      }

					    } 
				 ]
		);
		
	}
	
}else{
	console.log("inside data ------------------------------>"+message)
	
	var checkChange=$("#chkanychange").val();
	
	if(!isEmpty(checkChange) && !$.isEmptyObject($.parseJSON(checkChange)) && NTUCChkChngSts){
		
		
		
		if(pdfSts){
			$("#reportvaliationPopup table:first tbody tr:first h3:first").html(message);
			$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();
			$( "#reportvaliationPopup" ).dialog( "option", "height",280);
			$( "#reportvaliationPopup" ).dialog( "option", "width",500);

		}else{
			
			$('#reportvaliationPopup').dialog('option', 'title', 'E-Submit validation');
			$("#page16msg").html("");
			$("#page16msg").html( '<h3 style="color: red;font-size: 15pt;"></h3>');
			$("#reportvaliationPopup table:first tbody tr:first h3:first").html(message)
			$("#reportvaliationPopup table:first tbody>tr").not(":first").show();
			
		}
		
	}else{
		
		if(pdfSts){
//			$("#reportvaliationPopup table:first tbody tr:first h3:first").html(message);
//			$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();
//			return true;
			
			if(!IsEmpty(machine)){
				creatSubmtDynaReportPDF(machine)
			}
			return false;
		}else{
			
			$('#reportvaliationPopup').dialog('option', 'title', 'E-Submit validation');
			$("#page16msg").html("");
			$("#page16msg").html( '<h3 style="color: red;font-size: 15pt;"></h3>');
			$("#reportvaliationPopup table:first tbody tr:first h3:first").html(message)
			$("#reportvaliationPopup table:first tbody>tr").not(":first").show();

			
		}
		
		
	}
	
	

}



dialog.dialog("open");
return false;

}else{
	if(pdfSts && !IsEmpty(machine)){
		creatSubmtDynaReportPDF(machine)
	}
	else{
		return true;
	}
	
}



//	return true;


}

function callsetPageFocus(pageno,formtype){
	$("#reportvaliationPopup").dialog('close');
	setPageFocus(pageno,formtype)
}

function checkChanges(tblcellelem){
	var chkanychange = $("#chkanychange");
	var jsonvalues = JSON.parse(isEmpty(chkanychange.val()) ? "{}" : chkanychange.val());
	var newobj=jsonvalues;
	newobj[tblcellelem.name]=tblcellelem.value;
	chkanychange.val(JSON.stringify(newobj));

}
function chkOneCont(othis){
	document.getElementById('htxtOneAddr').value=othis.checked;
	checkChanges(document.getElementById('htxtOneAddr'))
}

function tblCommonScroll(tbl){

	$("#fnaartuplanTblfixedHead").off('scroll').on('scroll', function () {
        $('#fnaartuplanTblssfixedHead').scrollTop($(this).scrollTop());
    });

    $("#fnaartuplanTblssfixedHead").off('scroll').on('scroll', function () {
        $('#fnaartuplanTblfixedHead').scrollTop($(this).scrollTop());
    });

    $("#"+tbl).off('scroll');

}

function tblResetScroll(){
	$('#fnaartuplanTblssfixedHead').scrollTop($('#fnaartuplanTblfixedHead').scrollTop());
}

function fnaArtuPlanAddSubRow(cellObj){

	var rowIndex=$(cellObj).closest("tr").index();
	$("#fnaartuplanTbl tbody tr").eq(rowIndex).after("<tr><td style='width:20px;'>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td></tr>");
	$("#fnaartuplanTblss tbody tr").eq(rowIndex).after("<tr><td style='width:20px;'></td><td>2</td>");
}


function reArrngCellBorder(Clnt,tbl){
	var length=$("#"+tbl+" input[value='"+Clnt+"']").length;
	var obj=$("input[value='"+Clnt+"']");

	$("input[value='"+Clnt+"']").each(function(i){
		if(i==0){
			$(this).closest('td').css('border-bottom','1px solid transparent');
			$(this).closest('tr').find("td:first").css('border-bottom','1px solid transparent');
			return;
		}

		if(i==(length-1)){
			$(this).closest('td').css('border-top','1px solid transparent');
			$(this).closest('tr').find("td:first").css('border-top','1px solid transparent');
			return false;
		}

		$(this).closest('td').css({'border-top':'1px solid transparent','border-bottom':'1px solid transparent'});
		$(this).closest('tr').find("td:first").css({'border-top':'1px solid transparent','border-bottom':'1px solid transparent'});
	});
}


//function fetchPdfAndSavePolicy(obj){
//	var paramRefField=$(obj).closest('tr').find("input,select").serializeArray();
//	paramRefField.push({name: "dbcall", value:"POLICYPDFSAVE"});
//
//	$.ajax({
//		  type: "POST",
//		  url: "/KYC/KycUtilsServlet",
//		  data:paramRefField,
//		  cache: false,
//		  success: function(data){
//
//			  var saveResp=$.parseJSON(data);
//
//			  if(saveResp[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
//				  window.location = baseUrl + SESSION_EXP_JSP;
//				  return;
//			  }
//
//			  if(saveResp[0].STATUS=='SUCCESS'){
//				  alert("Sucessfully created policy");
//			  }
//
//			  if(saveResp[0].STATUS=='FAIL'){
//				  alert("Something went wrong.");
//			  }
//		  }
//		});
//}


function validateClntsDets(obj,spsChkSts){
	var $row=$(obj).closest("tr");
	var modalShwSts=false;

	var custId = $row.find("[name='htxtFldNFnaCustId']").val();
	var rtrnJsn = chkForNRICAttachment("chkNRICDocumentDetails",custId);

	if(!rtrnJsn[0].self){
		modalShwSts=true;
		slfAttachAddStatus=true;
	}

	if(!rtrnJsn[0].spouse && spsChkSts && confirm("Do you want to add Client(2)/Dependent details in DSF ?")){
		modalShwSts=true;
		spsAttachAddStatus=true;
	}

	clntAtchList=rtrnJsn[0].ATTACHED_LIST;

	if(clntAtchList.length == 0){
		alert(NO_NRIC_DOC_FOUND_CLNT + " Upload the NRIC Document(s)");	
	}
	if(clntAtchList.length > 0){
		if(!rtrnJsn[0].self){
			alert(NO_NRIC_DOC_FOUND_CLNT)
		}

		if(!rtrnJsn[0].spouse && spsChkSts){
			alert(NO_NRIC_DOC_FOUND_SPS)
		}
	}

	return modalShwSts;
}//end of

//function formJsonText(fna_id,agentcode){
//
//	//var fna_id = $(obj).closest('tr').find('td:eq(0)').find('input:first').val();
//
//
//	var loadMsg = document.getElementById("loadingMessage");
//	var loadingLayer = document.getElementById("loadingLayer");
//
//
//	var facode = $("#hTxtFldAFaCode").val();
//	var faURL = $("#hTxtFldAFaURL").val();
//
//
//	loadMsg.style.display="block";
//	//alert(loadMsg.style.display)
//	loadingLayer.style.display="block";
//
//	var parameter = "dbcall=VER&FNA_ID=" + fna_id;
//
//
//	var respText = ajaxCall(parameter);
//	var retval = respText;
//
//
//	loadMsg.style.display="none";
//	//alert(loadMsg.style.display)
//	loadingLayer.style.display="none";
//
//	var jsonStr={};
//	jsonStr['KYCID']=fna_id;
//
//	var JsnAgntIntro={}
//	JsnAgntIntro['AgentCode']=agentcode;//$(obj).closest('tr').find("td:eq(2) input:eq(7)").val();
//	JsnAgntIntro['LifeAdvisor']=true;
//	JsnAgntIntro['HealthAdvisor']=true;
//	JsnAgntIntro['ILPAdvisor']=true;
//
//
//	jsonStr['AgentIntroForm']=JsnAgntIntro;
//
//	var JsnPrsnlGenrlForm={};
//	var JsnContactNo={};
//	var JsnAddress={};
//	var JsnDepForm={};
//	var JsnDepDetArr=[];
//	var JsnDepDetObj={};
//	var JsnContactSpsNo={};
//	var JsnSpsAddress={};
//
//	var isspousenameexist=false;
//
//	for ( var val in retval) {
//		var tabdets = retval[val];
//		if (tabdets["SESSION_EXPIRY"]) {
//			window.location = baseUrl + SESSION_EXP_JSP;
//			return;
//		}
//
//
//		for ( var tab in tabdets) {
//			if (tabdets.hasOwnProperty(tab)) {
//				var value = tabdets[tab];
//
//				if (tab == "ARCH_FNASSDET_TABLE_DATA") {
//
//
//					var fnsDetl=value;//refarr['ARCH_FNASSDET_TABLE_DATA'];
//					console.log(fnsDetl)
//
//					JsnPrsnlGenrlForm['DateOfBirth']=fnsDetl.dfSelfDob.replace(/\//g, '-');
//					JsnPrsnlGenrlForm['EducationLevel']=chkValueExistInList(educationList,fnsDetl.dfSelfEdulevel);
//
//					//alert(fnsDetl.dfSelfGender)
//
//					JsnPrsnlGenrlForm['GenderCode']=fnsDetl.dfSelfGender;
//					JsnPrsnlGenrlForm['LanguageSpoken']=(isEmpty(fnsDetl.dfSelfEngSpoken))?"":(fnsDetl.dfSelfEngSpoken=='Y')?'Proficient':'Not Proficient';
//					JsnPrsnlGenrlForm['LanguageWritten']=(isEmpty(fnsDetl.dfSelfEngWritten))?"":(fnsDetl.dfSelfEngWritten=='Y')?'Proficient':'Not Proficient';
//					JsnPrsnlGenrlForm['MaritalStatus']=chkValueExistInList(maritalList,fnsDetl.txtFldCDClntMrtSts);
//					JsnPrsnlGenrlForm['NRIC_Passport']=fnsDetl.txtFldCDClntNRIC;
//					JsnPrsnlGenrlForm['Name']=fnsDetl.txtFldCDClntName;
//					JsnPrsnlGenrlForm['NationalityCode']=isEmpty(fnsDetl.dfSelfNationality)?"-1":
//							fnsDetl.dfSelfNationality=='OTH' ? chkValueExistInListOthr(nationalityList,(isEmpty(fnsDetl.dfSelfNatyDets)?"-1":fnsDetl.dfSelfNatyDets))
//								:chkValueExistInList(nationalityList,fnsDetl.dfSelfNationality);
//					JsnPrsnlGenrlForm['Occupation']=(isEmpty(fnsDetl.dfSelfOccpn))?"D999":chkValueExistInListOthr(occupationList,fnsDetl.dfSelfOccpn);
//					JsnPrsnlGenrlForm['Smoker']=fnsDetl.dfSelfSmoker;
//					JsnPrsnlGenrlForm['CPFNo']="";//Dont know which Field
//
//					JsnContactNo['Office']="";//fnsDetl.dfSelfOffice
//					JsnContactNo['Mobile']=fnsDetl.dfSelfMobile;
//					JsnContactNo['Home']="";//fnsDetl.dfSelfOffice
//
//					JsnPrsnlGenrlForm['ContactNo']=JsnContactNo;
//					JsnPrsnlGenrlForm['Race']=chkValueExistInListOthr(raceList,fnsDetl.dfSelfRace);
//					JsnPrsnlGenrlForm['Height']=fnsDetl.dfSelfHeight;//Dont know which Field
//					JsnPrsnlGenrlForm['NatureOfWork']=(fnsDetl.dfSelfBusinatr=='')?'':(fnsDetl.dfSelfBusinatr=='Others')?fnsDetl.dfSelfBusinatrDets:fnsDetl.dfSelfBusinatr;
//					JsnPrsnlGenrlForm['Weight']=fnsDetl.dfSelfWeight;//Dont know which Field
//					JsnPrsnlGenrlForm['EmailAddress']=fnsDetl.dfSelfPersemail;
//					JsnPrsnlGenrlForm['NameOfCompanySchool']=fnsDetl.dfSelfCompname;
//
//					JsnAddress["Address1"]=fnsDetl.htxtFldClntResAddr1;
//					JsnAddress["Address2"]=fnsDetl.htxtFldClntResAddr2;
//					JsnAddress["Address3"]=fnsDetl.htxtFldClntResAddr3;
//					JsnAddress["Address4"]="";
//					JsnAddress["PostalCode"]=(fnsDetl.resCountry.toUpperCase()=='SINGAPORE')?fnsDetl.htxtFldClntResPostalCode:'000000';
//					JsnAddress["AddressType"]=(fnsDetl.resCountry.toUpperCase()=='SINGAPORE')?'Singapore':'Non Singapore';
//					JsnAddress["Unit"]="";//Dont know which Field
//
//
//					JsnPrsnlGenrlForm['Address']=JsnAddress;
//
//					if(!isEmpty(fnsDetl.dfSpsName)){
//						isspousenameexist=true;
//					}
//
//					JsnDepDetObj['Bday']=fnsDetl.dfSpsDob.replace(/\//g, '-');
//					JsnDepDetObj['Gender']=fnsDetl.dfSpsGender;
//					JsnDepDetObj['Name']=fnsDetl.dfSpsName;
//					JsnDepDetObj['Occupation']=(isEmpty(fnsDetl.dfSpsOccpn))?"D999":chkValueExistInListOthr(nationalityList,fnsDetl.dfSpsOccpn);
//					JsnDepDetObj['Relationship']="SP"
//					JsnDepDetObj['Smoker']=fnsDetl.chkFnaSpsSmoke;
//					JsnDepDetObj['NRIC_Passport']=fnsDetl.dfSpsNric;
//					JsnDepDetObj['NationalityCode']=(isEmpty(fnsDetl.dfSpsNationality))?"-1":(fnsDetl.dfSpsNationality=='OTH')?chkValueExistInListOthr(nationalityList,(isEmpty(fnsDetl.dfSpsNatyDets)?"-1":fnsDetl.dfSpsNatyDets)):chkValueExistInList(nationalityList,fnsDetl.dfSelfNationality);
//					JsnDepDetObj['CPFNo']='';
//
//					JsnContactSpsNo['Office']="";//fnsDetl.dfSpsOffice;
//					JsnContactSpsNo['Mobile']=fnsDetl.dfSpsHp;
//					JsnContactSpsNo['Home']="";//fnsDetl.dfSpsHome;
//
//					JsnDepDetObj['ContactNo']=JsnContactSpsNo;
//					JsnDepDetObj['Race']='';//no field available
//					JsnDepDetObj['LanguageWritten']=(isEmpty(fnsDetl.dfSpsEngWritten))?"":(fnsDetl.dfSpsEngWritten=='Y')?'Proficient':'Not Proficient';
//					JsnDepDetObj['LanguageSpoken']=(isEmpty(fnsDetl.dfSpsEngSpoken))?"":(fnsDetl.dfSpsEngSpoken=='Y')?'Proficient':'Not Proficient';
//					JsnDepDetObj['Height']='';//no field available
//					JsnDepDetObj['NatureOfWork']=(fnsDetl.dfSpsBusinatr=='')?'':(fnsDetl.dfSpsBusinatr=='Others')?fnsDetl.dfSpsBusinatrDets:fnsDetl.dfSpsBusinatr;
//					JsnDepDetObj['Weight']='';//no field available
//					JsnDepDetObj['EmailAddress']=fnsDetl.dfSpsPersemail;
//					JsnDepDetObj['NameOfCompanySchool']=fnsDetl.dfSpsCompname;
//					JsnDepDetObj['MaritalStatus']=chkValueExistInList(maritalList,fnsDetl.dfSpsMartsts);
//
//					JsnSpsAddress['Address4']='';//no field available
//					JsnSpsAddress['Address3']=fnsDetl.resAddr3;
//					JsnSpsAddress['Address2']=fnsDetl.resAddr2;
//					JsnSpsAddress['Address1']=fnsDetl.resAddr1;
//					JsnSpsAddress['PostalCode']=(fnsDetl.resCountry.toUpperCase()=='SINGAPORE')?fnsDetl.resPostalcode:'000000';
//					JsnSpsAddress['AddressType']=(fnsDetl.resCountry.toUpperCase()=='SINGAPORE')?'Singapore':'Non Singapore';
//					JsnSpsAddress['Unit']='';//no field available
//
//					JsnDepDetObj['Address']=JsnSpsAddress;
//
//				}
//
//			}
//		}
//
//
//	}
//
//	jsonStr['PersonalGeneralForm']=JsnPrsnlGenrlForm;
//
//	JsnDepDetArr.push(JsnDepDetObj);
//	JsnDepForm['Dependents']=JsnDepDetArr;
//
//	if(isspousenameexist){
//		jsonStr['DependentsForm']=JsnDepForm;
//	}
////
////	jsonStr['faCode']="Avallis";
////	jsonStr['faURL']="www.ekyc.avallis.com";
//
//	console.log(JSON.stringify({"personInfo":jsonStr,"faCode":facode,"faURL":faURL}));
//
//	return JSON.stringify({"personInfo":jsonStr,"faCode":facode,"faURL":faURL});
//}

function chkForNRICAttachment(dbCall,zipcustId){

	 var returnVal='';

		$.ajax({
			  type: "POST",
			  url: "KycUtilsServlet",
			  data:{
				  dbcall:dbCall,
				  txtFldCustId:zipcustId
			  },
			  cache: false,
			  // async:false,
			  success: function(data){
				  var retAjxval=$.parseJSON(data);

				  if(retAjxval[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
					  window.location = baseUrl + SESSION_EXP_JSP;
					  return;
				  }

				  returnVal=retAjxval;

			  }
        });

		return returnVal;

}

function ModalPopupsPromptMandCancel(){
	 ModalPopups.Cancel("ClntDetsMandLayer");
	 spsAttachAddStatus=false;
	 slfAttachAddStatus=false;
}

/*function assingClntMandVal(nricAttchCount,row){

	var $row=$(row).closest("tr");


	if(document.getElementById("txtFldPopupAttDocUpload") && document.getElementById("txtFldPopupAttDocUpload").files.length == 0 ){
		document.getElementById("txtFldPopupAttDocUpload").focus();
		alert("Select Self NRIC Document.")

	   return false;
	}

	if(document.getElementById("txtFldSpsPopupAttDocUpload") && document.getElementById("txtFldSpsPopupAttDocUpload").files.length == 0 ){
		document.getElementById("txtFldSpsPopupAttDocUpload").focus();
		alert("Select Spouse NRIC Document.")

	   return false;
	}

	var parameter = "dbcall=CLIENTNRICUPLOAD"
			+"&txtFldPopUpDocTitle="+getParamValue($("#txtFldPopUpDocTitle"))
			+"&txtFldPopupPageNo="+getParamValue($("#txtFldPopupPageNo"))
			+"&txtFldPopUpChkList="+getParamValue($("#txtFldPopUpChkList"))
			+"&txtFldPopUpAttRem="+getParamValue($("#txtFldPopUpAttRem"))
			+"&txtFldPopUpDocCode="+getParamValue($("#txtFldPopUpDocCode"))
			+"&htxtFldNtucCaseNo="+getParamValue($row.find("td:eq(2) input:eq(6)"))
			+"&txtFldSpsPopUpDocTitle="+getParamValue($("#txtFldSpsPopUpDocTitle"))
			+"&txtFldSpsPopupPageNo="+getParamValue($("#txtFldSpsPopupPageNo"))
			+"&txtFldSpsPopUpChkList="+getParamValue($("#txtFldSpsPopUpChkList"))
			+"&txtFldSpsPopUpAttRem="+getParamValue($("#txtFldSpsPopUpAttRem"))
			+"&txtFldSpsPopUpDocCode="+getParamValue($("#txtFldSpsPopUpDocCode"))
			+"&txtFldSpsPopupAttRef="+getParamValue($("#txtFldSpsPopupAttRef"));

	 var form = $('#hiddenUtilFormAttch')[0];
     var data = new FormData(form);
     var ret="";

	    $.ajax({
	        url : baseUrl+"/KycUtilsServlet?"+parameter,
	        type: "POST",
	        data :data,
	        cache: false,
	        enctype: 'multipart/form-data',
	        contentType: false,
	        processData: false,
	        // async:false

	    }).done(function(response){
	    	var saveResp=$.parseJSON(response);


			  if(saveResp[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
				  window.location = baseUrl + SESSION_EXP_JSP;
				  return;
			  }

			  if(saveResp[0].STATUS =='SUCCESS'){

				  alert("Client NRIC document uploaded successfully.");
//				  ModalPopupsPromptMandCancel();
				  ModalPopups.Cancel("ClntDetsMandLayer");
//				  return true;
				  ret=true

			  }

			  if(saveResp[0].STATUS=='FAIL'){
				  alert("Client NRIC document upload failed.");
//				  ModalPopupsPromptMandCancel();
				  ModalPopups.Cancel("ClntDetsMandLayer");
				  ret= false;
			  }
	    });

	    return ret;

//	fetchPdfAndSavePolicy(globalRowObj);


}*/

function assingClntMandVal(nricAttchCount,row){

	var $row=$(row).closest("tr");

//	if(!chkClntMandtryDoc())return;

	var custId = $row.find("[name='htxtFldNFnaCustId']").val();
	var rtrnJsn = chkForNRICAttachment("chkNRICDocumentDetails",custId);
	$("#loadingLayer").show();
	$("#loadingMessage").show();
	var parameter = "dbcall=CLIENTNRICUPLOAD&"+$("#fnaClntAtchTbl .DbInsertRow").find("input:text,input:hidden,select").serialize();

	$("#fnaClntAtchTbl tbody tr").each(function(){
		var mode=$(this).find("td:eq(0) input:eq(1)").val();
		if(mode==QRY_MODE){
			$(this).find("[type=file]").remove();
		}

	});
	
	
	
	

	 var form = $('#hiddenUtilFormAttch')[0];
     var data = new FormData(form);
     var ret="";
     
     var loadMsg = document.getElementById("loadingMessage");
	 var loadingLayer = document.getElementById("loadingLayer");
	 
	 loadMsg.style.display = "block";
		// alert(loadMsg.style.display)
		loadingLayer.style.display = "block";

	    $.ajax({
	        url : baseUrl+"/KycUtilsServlet?"+parameter,
	        type: "POST",
	        data :data,
	        cache: false,
	        enctype: 'multipart/form-data',
	        contentType: false,
	        processData: false,
	        // async:false

	    }).done(function(response){
	    	var saveResp=$.parseJSON(response);

	    	 if(saveResp[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
				  window.location = baseUrl + SESSION_EXP_JSP;
				  return;
			 }

	    	 if(saveResp[0].STATUS =='SUCCESS'){

				  
//				  ModalPopupsPromptMandCancel();
				 
//				  return true;
//				  $("#loadingMessage").hide();
				 setTimeout(function(){ 
						 var loadMsg = document.getElementById("loadingMessage");
						 var loadingLayer = document.getElementById("loadingLayer");
						 loadMsg.style.display="none";
						 alert("Client(1)/Client(2) NRIC document uploaded successfully.");
						 ModalPopups.Cancel("ClntDetsMandLayer");
					 },500);
				  return true;
//				  ret=true

			  }

	    	 if(saveResp[0].STATUS=='FAIL'){
				 
//				  ModalPopupsPromptMandCancel();
				 
//				  $("#loadingMessage").hide();
//				  loadMsg.style.display="none";
				  setTimeout(function(){ 
						 var loadMsg = document.getElementById("loadingMessage");
						 var loadingLayer = document.getElementById("loadingLayer");
						 loadMsg.style.display="none";
						 alert("Client(1)/Client(2) NRIC document upload failed.");
						 ModalPopups.Cancel("ClntDetsMandLayer");
					 },500);
				  return false;
//				  ret= false;
			 }

//	    	ret=true;
	    });

	    setTimeout(function(){ 
			 var loadMsg = document.getElementById("loadingMessage");
			 var loadingLayer = document.getElementById("loadingLayer");
			 loadMsg.style.display="none";
			 ModalPopups.Cancel("ClntDetsMandLayer");
		 },500)
	    return true;

//	fetchPdfAndSavePolicy(globalRowObj);


}

function showPopupCalendar(cal,ev){
	getCalendarInTbl(cal,ev);
	document.getElementById("ClntDetsMandLayer_popup").appendChild(document.getElementById("scw"));
	document.getElementById("ClntDetsMandLayer_popup").appendChild(document.getElementById("scwIframe"));
	document.getElementById("scw").style.zIndex='18000';
	document.getElementById("scw").style.left="380px";
	document.getElementById("scw").style.top="70px";
	document.getElementById("scwIframe").style.left="380px";
	document.getElementById("scwIframe").style.top="70px";
	return true;

}

function checkPolicyExist(caseid){

	var retarr=[];
	var appid="",policyno="";

//	var $row=$(row).closest("tr");
//	var strNtucCaseNo=$row.find("td:eq(2) input:eq(6)").val();
//	var caseid = $row.find('td:eq(3) input:eq(3)').val();
	var existSts="";

	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");



	loadMsg.style.display="block";
	//alert(loadMsg.style.display)
	loadingLayer.style.display="block";


	$.ajax({
		  type: "POST",
		  url: "KycUtilsServlet",
		  data:{
			  dbcall:"CHKPOLICYEXIST",
			  strCaseId:caseid
		  },
		  cache: false,
		  // async:false,
		  success: function(data){
			  var retAjxval=$.parseJSON(data);




			  if(retAjxval[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
				  window.location = baseUrl + SESSION_EXP_JSP;
			  }

			  if(retAjxval[0].STATUS=='EXIST'){
//				  ModalPopupsPromptMandCancel();
//				  alert("Policy already exist");

//				  existSts=false;

				  appid =  retAjxval[0].CASEID_EQU_APPID;
				  policyno=retAjxval[0].CASEID_EQU_POLNO;

				  retarr = new Array(appid,policyno);

			  }else{

				  retarr.length=0;
				  retarr = new Array()

//				  existSts=true;
			  }


			  loadMsg.style.display="none";

		  }
  });

	return retarr;
}


function callDSF(fnaid,agentcode,lockimg){

	var row = $(lockimg).closest("tr");
	var orgfile = row.find("td:eq(0) img:eq(2)").attr('src');

	if(!(orgfile == "images/e-submission.png")){
		return false;
	}
	
	var jsonstring = formJsonText(fnaid,agentcode);
//	alert("json----->"+jsonstring);

setTimeout(function(){
	
	
	var encodesaml ="",caseid="",dsf_url="";
	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");
	var casestatus = row.find("td:eq(3)").find("input:eq(6)").val();
	var caseid = row.find("td:eq(3)").find("input:eq(3)").val();
//	row.find("td:eq(0) img:eq(2)").css('visibility', 'hidden');


	row.find("td:eq(0) img:eq(2)").attr('src', 'images/ajax-loader.gif');
//	$("loadingMessage").show();
	
	loadMsg.style.display="block";
//	alert(loadMsg.style.display)
	loadingLayer.style.display="block";



//	setTimeout(function(){
//		loadMsg.style.display="block";
////		alert(loadMsg.style.display)
//		loadingLayer.style.display="block";
//	}, 100);
	
	
	
	setTimeout(function(){
		

		$.ajax({

		    url:"OAuthServlet",

		    type:"POST",

		    data:$.param(

		    		{CALLFOR: "OAUTHVALIDATE",
		    			KYCJSON : jsonstring,
		    			ADVSTFCODE:agentcode,
		    			FNAID:fnaid,
		    			CASESTATUSDB:casestatus,
		    			CASEID:caseid
		    		}),

		      success:function(data) {

//		    	  loadMsg.style.display="none";

					var response = $.parseJSON(data);

					if (response["SESSION_EXPIRY"]) {
						window.location = baseUrl + SESSION_EXP_JSP;
						return;
					}

					$.each(response, function(count, arr) {

						$.each(arr,function(tab,data){

							$.each(data,function(key,val){

//								console.log(key +" >> " +val);

								if(key == "UPDATESTATUS_B4_CREATEMF2"){
//									alert(val);
								}
								if(key == "UPDATESTATUS_B4_CREATEMF2_MSG"){
									alert(val);
									loadMsg.style.display="none";
								}

								if(key == "OAUTH_UNAUTHORIZE"){
									alert(val["error_description"])
									loadMsg.style.display="none";
								}

								if(key == "OAUTH_STATUSBASED_FAIL"){
									alert(val["EKYC_ERROR"])
									loadMsg.style.display="none";
								}

//								if(key == "OAUTH_SUCCESS"){
//									callCreateMF2(jsonstring);
//								}

								if(key == "CREATE_POLICY_SUCCESS"){
									//alert(val["caseId"])

//									alert("DSF CaseId Generated success.\nCase ID :"+val["caseId"] +"\n");

									//lockimg.show();
									$(lockimg).closest('tr').find("td:eq(3) select:eq(0)").val("In-Progress");

									$(lockimg).closest('tr').find("td:eq(2) input:eq(8)").val(val["caseId"]);
									$(lockimg).closest('tr').find('td:eq(3) input:eq(3)').val(val["caseId"]);

									caseid =val["caseId"];

									if(!isEmpty(caseid)){

//										$(lockimg).closest('tr').find("td:eq(0) img:eq(3)").show();
										$(lockimg).closest('tr').find("td:eq(3) img:eq(2)").show();

//										var retarr = checkPolicyExist(caseid);
//										if(retarr.length == 0){
//											$(lockimg).closest('tr').find('td:eq(0) img:eq(3)').show();

//										}else{
//											$(lockimg).closest('tr').find('td:eq(3) input:eq(4)').val(retarr[0][0]);
//											$(lockimg).closest('tr').find('td:eq(3) input:eq(5)').val(retarr[0][1]);
//										}

										$("#hTxtDsfStatusChk").val("Y");

									}

									loadMsg.style.display="none";
								}

								if(key == "CREATE_POLICY_ERROR"){
									if(val["errorMsg"]){
										alert(val["errorMsg"])
									}else{
										alert(val)
									}
//									loadMsg.style.display="none";
									row.find("td:eq(0) img:eq(2)").attr('src', orgfile);
									loadMsg.style.display="none";
								}

								if(key == "IDP_RESPONSE_TO_DSF"){
//									alert(val)
								}
								if(key == "DSF_RESPONSE"){
									/*
									var $dialog = $('<div></div>')
						               .html('<iframe style="border: 0px; " src="' + val + '" width="100%" height="100%"></iframe>')
						               .dialog({
						                   autoOpen: false,
						                   modal: true,
						                   height: 700,
						                   width: 950,
						                   title: "DSF Login"
						               });

									$dialog.dialog('open');
						*/
								}

								if(key == "ENCODED_SAML"){encodesaml=val;}
								if(key == "CASEID"){caseid =val;}
								if(key == "NTUC_DSF_URL"){

//									dsf_url=val;
									creatSubmtDynaNewWindow(val);
//									creatSubmtDynaNewWindow(baseUrl+ "/testdsf.jsp");
//
//									row.find("td:eq(0) img:eq(2)").css('visibility', 'visible');
									row.find("td:eq(0) img:eq(2)").attr('src', orgfile);
									loadMsg.style.display="none";

								}
								if(key == "UPDATEDSFCASE"){
//									alert("Case Return From DSF ->"+val)
								}

							})

						});
					});

//					if(!isEmpty(caseid)){

//						var retarr = checkPolicyExist(caseid);

//						if(retarr.length == 0){
//							$(lockimg).closest('tr').find('td:eq(0) img:eq(3)').show();

//						}else{
//							$(lockimg).closest('tr').find('td:eq(3) input:eq(4)').val(retarr[0][0]);
//							$(lockimg).closest('tr').find('td:eq(3) input:eq(5)').val(retarr[0][1]);
//						}

//					}
//					loadMsg.style.display="none";

		      }
		});
		
	}, 100);


	
}, 100);






}

function callIDPTest(){


	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");

	loadMsg.style.display="block";
	//alert(loadMsg.style.display)
	loadingLayer.style.display="block";

	$.ajax({

	    url:"IdPRequestHandler",

	    type:"POST",

	    data:$.param(

	    		{CALLFOR: "VALIDATEDSFREQ"}),

	      success:function(data) {


	    	  loadMsg.style.display="none";
	    	  alert(data)

				var response = $.parseJSON(data);

				$.each(response, function(tab, data) {

				});


	      }
	});
}

function lockCase(obj){



	if(window.confirm("Do you want to lock the case?.No changes are allowed after lock")){


		var caseNo = $(obj).closest('tr').find("td:eq(2) input:eq(8)").val();
		var fnaid= $(obj).closest('tr').find('td:eq(0)').find('input:first').val();


		parameter = "dbcall=UPDATELOCKEDCASE&strFNAId=" + fnaid + "&caseNo="+ caseNo;

		var retval = ajaxCall(parameter);

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}

			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var key = tab;
					var value = tabdets[tab];

					if(key == "NTUC_LOCK_CASE"){

						if(value == "SUCCESS"){

						}

						if(value == "FAIL"){

						}
					}
				}
			}
		}

	}
}


function cancelCase(obj){

	if(window.confirm("Do you want to cancel the case?")){
		setnCallUpdateDSFParams(obj,'Cancel');
	}
}


function validateKycMandField(fna_id){

var parameter = "dbcall=VER&FNA_ID=" + fna_id;

var respText = ajaxCall(parameter);
var retval = respText;

//console.log(JSON.stringify(retval));
var errSelfSts=false;
var errSpsSts=false;
var retrnSelfTxt=''
var retrnSpsTxt=''

$.each(respText,function(i,refarr){

	$.each(refarr,function(tab,obj){

		if(tab=="ARCH_FNASSDET_TABLE_DATA"){

			if(isEmpty(obj.dfSelfName) || isEmpty(obj.dfSelfNric) || isEmpty(obj.dfSelfNric) || isEmpty(obj.dfSelfNationality)
					|| isEmpty(obj.dfSelfDob) || isEmpty(obj.dfSelfGender))
				errSelfSts=true;

			if(isEmpty(obj.dfSelfName))
				retrnSelfTxt="Self Name \n";

			if(isEmpty(obj.dfSelfNric))
				retrnSelfTxt+="NRIC_Passport \n";

			if(isEmpty(obj.dfSelfNationality))
				retrnSelfTxt+="NationalityCode \n";

			if(isEmpty(obj.dfSelfDob))
				retrnSelfTxt+="DateOfBirth \n";

			if(isEmpty(obj.dfSelfGender))
				retrnSelfTxt+="GenderCode \n";

			if(!isEmpty(obj.dfSpsName)){

				if(isEmpty(obj.dfSpsNric) || isEmpty(obj.dfSpsDob) || isEmpty(obj.dfSpsGender) || isEmpty(obj.dfSpsNric))
					errSpsSts=true;

				if(isEmpty(obj.dfSpsNric))
					retrnSpsTxt+="NRIC_Passport \n";

				if(isEmpty(obj.dfSpsDob))
					retrnSpsTxt+="DateOfBirth \n";

				if(isEmpty(obj.dfSpsGender))
					retrnSpsTxt+="GenderCode \n";

				if(isEmpty(obj.dfSpsNric))
					retrnSpsTxt+="NationalityCode \n";

			}

		}

	});

});


var returnAlrtMsg="";

if(errSelfSts){
	returnAlrtMsg+="Following Client(1) details are empty : \n";
	returnAlrtMsg+=retrnSelfTxt+" \n";
}

if(errSpsSts){
	returnAlrtMsg+="Following Client(2) details are empty : \n";
	returnAlrtMsg+=retrnSpsTxt;
}

return returnAlrtMsg;
}

function setnCallUpdateDSFParams(rowobj,cancelstatus){


	var agentcode = $(rowobj).closest('tr').find("td:eq(2) input:eq(7)").val();
	var fnaid = $(rowobj).closest('tr').find('td:eq(0)').find('input:first').val();// cell0.childNodes[0].value;

	var pdfparam = "";
	var casestatus= $(rowobj).closest('tr').find('td:eq(3) select:eq(0)').val();
	var casestatusFinal= $(rowobj).closest('tr').find('td:eq(3) input:eq(6)').val();
	var caseid =  $(rowobj).closest('tr').find('td:eq(3) input:eq(3)').val();
//	cell3.childNodes[5].value
	var jsonparam =	 formJsonText(fnaid,agentcode);
	var custid = "",custnric ="",servadvid="";

//	alert(caseid)
//	alert(casestatus +": Final Status is "+casestatusFinal)
	if(!isEmpty(casestatus)){

		casestatus = isEmpty(cancelstatus)?casestatus:cancelstatus;

//		updateStatusToDSF(fnaid, agentcode, caseid, pdfparam, casestatus);


		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");

		loadMsg.style.display="block";
		//alert(loadMsg.style.display)
		loadingLayer.style.display="block";

		$.ajax({

		    url:"OAuthServlet",

		    type:"POST",

		    data:$.param(

		    		{CALLFOR: "UPDATESTATUSTODSF",

		    			ADVSTFCODE:agentcode,
		    			FNAID:fnaid,
		    			CASEID:caseid,
		    			PDFPARAM:pdfparam,
		    			CASESTATUS: casestatus ,
		    			KYCJSON:jsonparam,
		    			CUSTID:custid,
		    			CUSTNRIC:custnric,
		    			SERVADVID:servadvid


		    		}),

		      success:function(data) {

		    	  loadMsg.style.display="none";

					var response = $.parseJSON(data);
					$.each(response, function(count, arr) {
						$.each(arr,function(tab,data){
							$.each(data,function(key,val){

								if(key == "UPDATEKYC2_STATUS"){
									alert(val)
//									$(rowobj).closest('tr').find('td:eq(3) select:eq(0)').val("In-Progress");
								}
								if(key == "UPDATEKYC2_DSF_STATUS"){
									if(val.toLowerCase() == "success"){
										if(casestatus.toLowerCase() == "cancel"){
											$(rowobj).closest('tr').find('td:eq(3) select:eq(0)').append("<option value='Cancel'>Cancelled</option>")
											$(rowobj).closest('tr').find("td:eq(3) img:eq(2)").hide();
											$(rowobj).closest('tr').find('td:eq(3) select:eq(0)').val("Cancel");
											$(rowobj).closest('tr').find("td:eq(3) img:eq(2)").hide();
											$(rowobj).closest('tr').find("td:eq(0) img:eq(2)").hide();
											$(rowobj).closest('tr').find("td:eq(0) img:eq(3)").hide();
										}else if(casestatus.toLowerCase() == "submitted"){
											$(rowobj).closest('tr').find("td:eq(3) img:eq(2)").hide();
											$(rowobj).closest('tr').find("td:eq(0) img:eq(2)").hide();
											if(ntuc_sftp_access_flag){
												$(rowobj).closest('tr').find("td:eq(0) img:eq(3)").show();
											}
										}
									}
								}
								if(key == "UPDATEKYC2_STATUS_PARAM"){
									$(rowobj).closest('tr').find('td:eq(3) select:eq(0)').val(val);

								}
								/*if(key == "UPDATEKYC2_STATUS"){
									alert(val)
									$(rowobj).closest('tr').find('td:eq(3) select:eq(0)').val("Submitted");
								}*/

								if(key == "OAUTH_STATUSBASED_FAIL"){
									alert(val["EKYC_ERROR"])
								}


							})

						});
					});


		      }
		});

	}



}

function updateStatusToDSF(fnaid,agentcode,caseid,pdfparam,casestatus){


//	var jsonstring = formJsonText(fnaid,agentcode);
//	alert("json----->"+jsonstring);


}

function createPolicywithAttach(fnaid,agentcode,lockimg,caseid){


	var jsonstring = formJsonText(fnaid,agentcode);

	var row = $(lockimg).closest("tr");

	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");



	loadMsg.style.display="block";
	loadingLayer.style.display="block";



//	var paramRefField=$(obj).closest('tr').find("input,select").serializeArray();

	$.ajax({
		  type: "POST",
		  url: "KycUtilsServlet",
		  data:$.param(
		    		{
		    			dbcall:"POLICYPDFSAVE",
		    			FNAID:fnaid,
		    			KYCJSON:jsonstring,
		    			ADVSTFCODE:agentcode,
		    			CASEID:caseid

		    		}),
		  cache: false,
		  success: function(data){

			loadMsg.style.display="none";
//			loadingLayer.style.display="none";


			  var saveResp=$.parseJSON(data);


			  if(saveResp[0].SESSION_EXPIRY == 'SESSION_EXPIRY'){
				  window.location = baseUrl + SESSION_EXP_JSP;
				  return;
			  }



			  $.each(saveResp, function(count, arr) {

					$.each(arr,function(tab,data){

						if(tab == "STATUS_IN_DETAILS"){
							if("FILE_NOT_FOUND" == data){
								alert("File not found for the caseId:"+caseid);
							}
						}

						if(tab == "STATUS"){
							alert("Policy attachment download status : "+data);
						}

					});
			  });


		  },
		  fail:function(data){
			  loadMsg.style.display="none";
//				loadingLayer.style.display="none";

		  }
		});


}

function fnaClntAtchAdd(tbl,mode){

	if(!validateClntAtchDoc())return;

	var strLoggedUser=COMMON_SESS_VALS[0].STR_LOGGEDUSER;
	var strCurrDate=COMMON_SESS_VALS[0].TODAY_DATE;
	var selfname = $("#hTxtFldFnaDsfSelfName").val();
	var spsname = $("#hTxtFldFnaDsfSpsName").val();

	var tblobj = document.getElementById(tbl);
	var tbodyobj = tblobj.tBodies[0];

	var rowlenobj = tbodyobj.rows.length;
	var jqRwLen=$("#"+tbl+" tbody tr:visible").length;
	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.innerHTML =  '<input type="text" readonly="readonly" name="txtFldClntDocSino" value="'+(Number(jqRwLen)+1)+'" style="width:100%;text-align:center;background:transparent;border:0"/>'+
					   '<input type="hidden" name="txtFldClntDocMode" value="'+ mode+ '"/>';
	cell0.style.textAlign = "center";

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML ='<select class="kycSelect"  name="selClntDocFor" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  style="width:98%;"></select>'+
	'<input type="hidden" name="txtFldDocApplcntName" id="txtFldDocApplcntName" class="kycSelect" style="background-color: rgb(233, 244, 235);width:98%;background:transparent;border:1 solid gray"/>'
	comboCloneOption($("#selHdnAtchFor"),cell1.firstChild);

	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
//	cell2.innerHTML = '<input type = "text" readOnly="true" class="fplblText" style="background-color: rgb(233, 244, 235);background:transparent;border:0"  name="txtFldClntDocChkList" value="'+APP_ATTACH_CATEGORY_FINPLAN+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:100%"/>';
	cell2.innerHTML = '<select class="kycSelect" name="txtFldClntDocChkList" ><option value="'+APP_ATTACH_CATEGORY_FINPLAN+'">'+APP_ATTACH_CATEGORY_FINPLAN+'</option><option value="AT076">Others</option></select>';
	cell2.childNodes[0].value=APP_ATTACH_CATEGORY_FINPLAN;

	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
//	cell3.innerHTML = '<input type = "text" readOnly="true" class="fplblText" style="background-color: rgb(233, 244, 235);background:transparent;border:0"  name="txtFldClntDocTitle" value="'+APP_ATTACH_CATEGORY_NRICPP+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:100%"/>'+
//					  '<input type="hidden" name="txtFldPopUpDocCode" id="txtFldPopUpDocCode" value="AT050"/>';
	cell3.innerHTML = '<select class="kycSelect" name="txtFldClntDocTitle" ><option value="'+APP_ATTACH_CATEGORY_NRICPP+'">'+APP_ATTACH_CATEGORY_NRICPP+'</option><option value="Others">Others</option></select>'+
	  '<input type="hidden" name="txtFldPopUpDocCode" id="txtFldPopUpDocCode" value="AT050"/>';
	
	cell3.childNodes[0].value=APP_ATTACH_CATEGORY_NRICPP;
	cell3.childNodes[0].onchange = function(){
		var doctitle = cell3.childNodes[0].value;
		if(doctitle == APP_ATTACH_CATEGORY_NRICPP){
			cell3.childNodes[1].value="AT050";
		}else{
			cell3.childNodes[1].value="AT076";
		}
	}


	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type = "file"  name="txtFldClntDocFile" accept=".jpg,.pdf,.tif,.tiff,.png,.bmp,.jpeg,.gif"  class="cssValidateNRICFile" style="width:100%"/>';

	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text" readOnly="true" class="fplblText" style="background-color: rgb(233, 244, 235);background:transparent;border:0"  name="txtFldClntDocFileType" value="" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:100%"/>';

	var cell6 = rowobj.insertCell(6);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text" class="fpEditTblTxt"  name="txtFldClntDocPgNo" value="" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:60%"/>';

	var cell7 = rowobj.insertCell(7);
	cell7.style.textAlign = "left";
	cell7.innerHTML = '<input type = "text" class="fpEditTblTxt"  name="txtFldClntDocRmk"  onclick="hideTooltip()" maxlength="300" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:98%"/>';

	var cell8 = rowobj.insertCell(8);
	cell8.style.textAlign = "left";
	cell8.innerHTML = '<input type = "text" readOnly="true" class="fplblText" style="background-color: rgb(233, 244, 235);background:transparent;border:0"  name="txtFldClntDocCrtdBy" value="'+strLoggedUser+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:98%"/>';

	var cell9 = rowobj.insertCell(9);
	cell9.style.textAlign = "left";
	cell9.innerHTML = '<input type = "text" readOnly="true" class="fplblText" style="background-color: rgb(233, 244, 235);width:95%;background:transparent;border:0"  name="txtFldClntDocAtchDte" value="'+strCurrDate+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:98%"/>';


	$(rowobj).on("click",function(event){
		selectSingleRow(this);
	});

	if(mode==INS_MODE){
		$(rowobj).addClass("DbInsertRow")
	}

	$(cell1.firstChild).on("change",function(event){
		event.stopPropagation();
		var crntElemt=event.target;
		var crntVal=this.value;
		if(!isEmpty(crntVal)){
			if(crntVal == "CLIENT"){
				cell1.childNodes[1].value=selfname;
				cell1.childNodes[1].readOnly=true;
			}else if(crntVal == "SPOUSE/CLIENT"){
				if(isEmpty(spsname)){
					/*alert("Since Spouse details are not found in selected archive. To upload document, Spouse Name is required!");
					$(cell1.firstChild).val("")
					$(cell1).find("input:eq(0)").val("");
					return false;*/
					
					openApplcantNameDlg(crntVal, rowobj)
					depname = cell1.childNodes[1].value;
					
				}else{
					cell1.childNodes[1].value=spsname;
					cell1.childNodes[1].readOnly=true;
				}
				
			}else {
				openApplcantNameDlg(crntVal, rowobj)
				depname = cell1.childNodes[1].value;
				
			}
		}else{
			
		}

		
		if(!isEmpty(crntVal)){
		if(clntAtchList.length>0){
			/*if($.inArray(crntVal,clntAtchList)> -1 ){
				alert("NRIC Document already uploaded for "+crntVal);
				this.value="";
				return false;
			}*/
		}
		
		$(this).closest("table").find("[name=selClntDocFor]").each(function(){
			/*if(crntElemt!=this && this.value==crntVal){
				alert("NRIC Document already uploaded for "+crntVal);
				crntElemt.value="";
			}*/
		});
		}
	});
}

function fnaClntAtchDel(tblId) {
	deleteRowInTbl(tblId,true);
}

function validateClntAtchDoc(){
	var tblRowLength=$("#fnaClntAtchTbl tbody tr").length;
	var status=true;

	if(tblRowLength>0){
		$("#fnaClntAtchTbl tbody tr").each(function(){
			if($(this).find("td:eq(0) input:eq(1)").val()!=QRY_MODE){
			if(isEmpty($(this).find("td:eq(1) select:first").val())){
				alert("Select in Attachment for");
				$(this).find("td:eq(1) select:first").focus();
				status=false;
				return false;
			}

			if($(this).find("td:eq(4) input:eq(0)")[0].files.length==0){
				alert("Select a file.");
				$(this).find("td:eq(4) input:first").focus();
				status=false;
				return false;
			}
			}
		});
	}

	return status;
}

function chkClntMandtryDoc(){

	var tblRowLength=$("#fnaClntAtchTbl tbody tr").length;
	if(tblRowLength>0){
		if(!validateClntAtchDoc())return;
	}else{
		alert("No rows to insert.")
	}

	if(slfAttachAddStatus){

		var existSts=false;

		$("#fnaClntAtchTbl tbody tr").each(function(){

			if($(this).find("td:eq(1) select:first").val()==ATTACH_FOR_PROPOSER){
				existSts=true;
				slfAttachAddStatus=false;
				return false;
			}

		});

		if(!existSts){alert("Upload the NRIC/PASSPORT Document for "+ ATTACH_FOR_PROPOSER); return false;}

	}

	if(spsAttachAddStatus){
		var existSts=false;

		$("#fnaClntAtchTbl tbody tr").each(function(){
		if($(this).find("td:eq(1) select:first").val()==ATTACH_FOR_ASSURED){
			existSts=true;
			spsAttachAddStatus=false;
			return false;
		}
		});


		if (!existSts) {alert("Upload the NRIC/PASSPORT Document for  "+ATTACH_FOR_ASSURED); return false;}
	}

	return true;
}

function reloadDSFUpdatedCases(){

	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");

	loadMsg.style.display="block";
	//alert(loadMsg.style.display)
	loadingLayer.style.display="block";

	var custid= $("#hTxtFldFnaCustId").val();
	var loguserid = $("#hTxtFldFnaLoggedUserId").val();

		parameter = "dbcall=loadFnaDetsForArchList&strFnaCustId="+custid+"&strFnaLogUserId="+loguserid;
		console.log(parameter);

		var retval = ajaxCall(parameter);




		var tabdets = retval[0];

		for ( var tab in tabdets) {

			if (tabdets.hasOwnProperty(tab)) {

				var key = tab;
				var value = tabdets[tab];


				if (key == "VERSION_DETAILS") {

					var archcnt = value.length;
					var tmpcnt = value.length;
					var cnt=1;

//					for (var cnt = 1; cnt <= value.length; cnt++) {

						var ntuccasestatus = value[cnt - 1]["txtFldNtucCaseSts"];
						var ntuccaseid = value[cnt - 1]["txtFldNtucCaseId"];
						var fnaid = value[cnt - 1]["fna_id"];

						var rowobj = getArchiveRowObj(fnaid);

						if(ntuccasestatus.toUpperCase() == 'COMPLETED'){

							var cnfrm ="The Case Id :" + ntuccaseid +" is completed all the process at DSF side.\nDo you want to agree all the changes are completed.\n\n Click OK to Lock the case (No More changes are allowed after Lock).\n\n Click Cancel to need some more changes in KYC archive.";

							if(window.confirm(cnfrm)){
								ntuccasestatus="Submitted";
								rowobj.cells[3].childNodes[4].value = ntuccasestatus;
								rowobj.cells[3].childNodes[4].disabled=true;
								rowobj.cells[3].childNodes[8].value = ntuccasestatus;
								setnCallUpdateDSFParams(rowobj.cells[3],null);

//								$("#hTxtDsfStatusChk").val("");
							}else{

								ntuccasestatus="In-Progress";
								rowobj.cells[3].childNodes[4].value = ntuccasestatus;
								rowobj.cells[3].childNodes[8].value = "Submitted";
								rowobj.cells[3].childNodes[4].disabled=true;
								$(rowobj).closest('tr').find("td:eq(0) img:eq(3)").hide();
							}
//							Always clear
							$("#hTxtDsfStatusChk").val("");
						}
//					}
				}
			}
		}
}


function getArchiveRowObj(fnaid){

	var rowobj = null;
	var tblId = document.getElementById("verdettable");
	var tbody = tblId.tBodies[0];
	var rowlen = tbody.rows.length;
	for(var r=0;r<rowlen;r++){

		var rowfaid = tbody.rows[r].cells[0].childNodes[0].value;
		if(rowfaid==fnaid){
			rowobj = tbody.rows[r];
			break;
		}
	}

	return rowobj;
}


function fetchExistNtucDoc(obj){
	var $row=$(obj).closest("tr");
	var custId = $row.find("[name='htxtFldNFnaCustId']").val();
	var fnaid = $row.find('td:eq(0)').find('input:first').val();
	
	if(isEmpty($("#hTxtFldFnaDsfSelfName").val())){
		
		$("#hTxtFldFnaDsfSelfName").val("");
		$("#hTxtFldFnaDsfSpsName").val("");

		var parameter = "dbcall=VER&FNA_ID=" + fnaid;
		var respText = ajaxCall(parameter);

		$.each(respText,function(i,refarr){
			if(i==2){
				var fnsDetl=refarr['ARCH_FNASSDET_TABLE_DATA'];
				$("#hTxtFldFnaDsfSelfName").val(fnsDetl.dfSelfName);
				$("#hTxtFldFnaDsfSpsName").val(fnsDetl.dfSpsName);
				
			}

		});
		
	}	
	
	

	var rtrnJsn = chkForNRICAttachment("chkNRICDocumentDetails",custId);

	clntAtchList=rtrnJsn[0].ATTACHED_LIST;
	if(clntAtchList.length == 0){
//		alert(NO_NRIC_DOC_FOUND)
		if(!window.confirm(NO_NRIC_DOC_FOUND_CLNT +"\nDo you want to upload the NRIC Document(s)?")){
			return false;
		}
	}
	parameter = "dbcall=FETCHCLINTNRICATTACH&txtFldCustId";
	var rtrnJsn = chkForNRICAttachment("FETCHCLINTNRICATTACH",custId);


	 if(rtrnJsn[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
		  window.location = baseUrl + SESSION_EXP_JSP;
		  return;
	 }

	 if(!(rtrnJsn[0].NRIC_REC.length>0)){
//		 alert(NO_NRIC_DOC_FOUND);
	 }

//	 if(rtrnJsn[0].NRIC_REC.length>0){
		 ModalPopups.MandatoryPopupFPMS("ClntDetsMandLayer", {width:960,height:520,onOk:"",onCancel:"ModalPopupsPromptMandCancel()"});
//	 }
			$("#ClntDetsMandLayer_okButton").click(function(){
				if(!chkClntMandtryDoc())return;
//				if(!chkClntMandtryDoc())return false;
				
				var loadMsg = document.getElementById("loadingMessage");
				var loadingLayer = document.getElementById("loadingLayer");

				loadMsg.style.display = "block";
				// alert(loadMsg.style.display)
				loadingLayer.style.display = "block";
				
				if(!assingClntMandVal("+isCustNricAttached+",this)){
//					alert("Attachment upload failed");
					loadMsg.style.display = "none";
					return;
				}

			});

	 $.each(rtrnJsn[0].NRIC_REC,function(i,obj){
		 fnaClntAtchAdd("fnaClntAtchTbl",QRY_MODE);
		 var $lstRow=$("#fnaClntAtchTbl tbody tr:last");
		 $.each(obj,function(elm,val){
			 if(elm=="txtFldClntDocFile"){
//				 $lstRow.find("[name='"+elm+"']").attr("type","text");
				 var txtFld='<input type="text" class="fplblText" value="'+val+'" readonly="readonly" class="fplblText" style="background-color:rgb(233, 244, 235);background:transparent;border:0;width:100%" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();">'
				 $lstRow.find("[name='"+elm+"']").hide().closest("td").append(txtFld);
				 return;
			 }

			 $lstRow.find("[name='"+elm+"']").val(val).prop("readonly",true)//.attr("class","").addClass("fplblText kycselect").
			 .css({"background-color":"rgb(233, 244, 235)"});

			 if( $lstRow.find("[name='"+elm+"']").is("select")) {
				 $lstRow.find("[name='"+elm+"']").attr("disabled",true);
			 }
		 });
	 });
	 if(clntAtchList.length==(Number($("#selHdnAtchFor option").length)-1)){
//		 dec_2019
//		 $("#ClntDetsMandLayer_okButton").hide();
//		 $("#fnaClntAtchAddRow").hide();
//		 $("#fnaClntAtchDelRow").hide();
	 }
}


//function dsfESubmit(elm){
//
//	var agentcode = $(elm).closest('tr').find("td:eq(2) input:eq(7)").val();
//	var fnaid = $(elm).closest('tr').find('td:eq(0)').find('input:first').val();// cell0.childNodes[0].value;
//	var advstfcode = $(elm).closest('tr').find("td:eq(2) input:eq(7)").val();
//	var lockimg = $(elm).closest('tr').find("td:eq(3) img:eq(1)");
//	var jsonstring ;
//	var spsNricChkSts=false;
//	var blnAtlstOneNtuc=false;
//
//	var parameter = "dbcall=VER&FNA_ID=" + fnaid;
//	var respText = ajaxCall(parameter);
//
////	console.log(JSON.stringify(respText));
//	$.each(respText,function(i,refarr){
//		if(i==2){
//			var fnsDetl=refarr['ARCH_FNASSDET_TABLE_DATA'];
//
//			if(!isEmpty(fnsDetl.dfSpsName)){
//				spsNricChkSts=true;
//			}
//
//		}
//
//		if(i==22){
//			var fnsPlnDets=refarr['ARCH_ADVRECPRDTPLAN_DATA'];
//
//			if(typeof fnsPlnDets !== "undefined" && fnsPlnDets.length>0){
//			$.each(fnsPlnDets,function(i,obj){
//				if(obj.selFnaAdvRecCompName=='PRIN000065'){
//					blnAtlstOneNtuc=true;
//					return false;
//				}
//
//			});
//			}
//
//		}
//
//	});
//
//
//
//	if(!isEmpty(advstfcode)){
//
//		var validtnArr = [];
//
////		validtnArr = validateClntsDets(this,spsNricChkSts);
//
////		console.log("temporarily returned here");
//
////		ModalPopups.MandatoryPopupFPMS("ClntDetsMandLayer", {width:920,onOk:"",onCancel:"ModalPopupsPromptMandCancel()"});
//
//		//for testing
//		/*$("#ClntDetsMandLayer_okButton").click(function(){
//
//
//			assingClntMandVal("+isCustNricAttached+",this);
//
////			callDSF(fnaid,agentcode,lockimg);
//
//
//
//		});*/
//
//		if( validateClntsDets(elm,spsNricChkSts)){
//			ModalPopups.MandatoryPopupFPMS("ClntDetsMandLayer", {width:920,onOk:"",onCancel:"ModalPopupsPromptMandCancel()"});
//
//
//
//		}else {
//
//
//			var mandField=validateKycMandField(fnaid);
//
//
//			if(mandField.length>0){
//				alert(mandField);
//				$(elm).closest("td").find("[name=version]").click();
//				setPageFocus(12, FNA_FORMTYPE);
//				return false;
//			}
//
//			//For At least NTUC Product
//			if(!blnAtlstOneNtuc){
//				alert("At least one NTUC Product should be available.");
//				$(elm).closest("td").find("[name=version]").click();
//				setPageFocus(14, FNA_FORMTYPE);
//				return false;
//			}
//
//			callDSF(fnaid,agentcode,lockimg);
//		}
//
//		$("#ClntDetsMandLayer_okButton").click(function(){
//
//			if(!chkClntMandtryDoc())return false;
//
//			if(!assingClntMandVal("+isCustNricAttached+",elm)){
//				alert("Attachment upload failed");
//				return;
//			}
//
//			var mandField=validateKycMandField(fnaid);
//
//
//			if(mandField.length>0){
//				alert(mandField);
////				$(this).closest("td").find("[name=version]").click();
//				$(lockimg).closest("tr").find("td:eq(0)").find("[name=version]").click();
//				setPageFocus(12, FNA_FORMTYPE);
//				return false;
//			}
//
//			//For At least NTUC Product
//			if(!blnAtlstOneNtuc){
//				alert("At least one NTUC Product should be available.");
////				$(this).closest("td").find("[name=version]").click();
//				$(lockimg).closest("tr").find("td:eq(0)").find("[name=version]").click();
//				setPageFocus(14, FNA_FORMTYPE);
//				return false;
//			}
//
//			callDSF(fnaid,agentcode,lockimg);
//
//		});
//
//
//	}else{
//
//		alert("Adviser are not registered with NTUC Income");
//
//	}
//
//}

function toggleExistInvest(obj){

	if(!obj.checked){
		$("#ccRepexistinvestflg").val("");
		return;
	}

	$("[name=ccRepexistinvestflgYN]").attr("checked",false)

	obj.checked=true;
	$("#ccRepexistinvestflg").val(obj.value);
}

function slctDslectAll(){
	var blnSlctAllSts=true; 
	
	$("#repDecTbl [type=checkbox]").each(function(){
		var chkid = $(this).prop("id");
		if(chkid != "htfrepdecno1all"){
			if(!this.checked){blnSlctAllSts=false;return false;}
		}
		
	});
	
	if(blnSlctAllSts)$("#htfrepdecno1all").prop("checked",true);
	if(!blnSlctAllSts)$("#htfrepdecno1all").prop("checked",false);
}


function openApplcantNameDlg(val,rowobj){	
	
	var depname = isDependantExists(val,rowobj);
	if(!isEmpty(depname)){
		 $(rowobj).find("td:eq(1)").find("input:eq(0)").val(depname);
	}else{
		$("#attachappllegend").html(val + " name is mandatory to upload document ");
		
		$("#txtFldAttachApplName").val("");
		$("#txtFldAttachApplName").focus();
		
//		 ModalPopups.attachApplName("ATTACHAPPLNAME",{width: 450,height: 50,onOk: "" ,onCancel:""} );
		 
		 $("#ATTACHAPPLNAME_okButton").click(function(){
			 closeApplcantNameDlg(rowobj);
		 })
		 
		  $("#ATTACHAPPLNAME_cancelButton").click(function(){
			 cancelApplcantNameDlg(rowobj);
		 })
	}
	

}


function closeApplcantNameDlg(rowobj){
	

	 var name = document.getElementById("txtFldAttachApplName").value;
	 if(isEmpty(name)){
		 alert("Name is required")
		 return false;
	 }
	 $(rowobj).find("td:eq(1)").find("input:eq(0)").val(name);
	
	 
	 document.getElementById("attachapplnamediv").appendChild(document.getElementById("attachapplspan"))
	 ModalPopups.Cancel("ATTACHAPPLNAME");
}


function cancelApplcantNameDlg(rowobj){
	

	 var name = document.getElementById("txtFldAttachApplName").value;
//	 if(isEmpty(name)){
//		 alert("Name is required")
//		 return false;
//	 }
	 $(rowobj).find("td:eq(1)").find("select:eq(0)").val("");
	 $(rowobj).find("td:eq(1)").find("input:eq(0)").val("");
	
	 
	 document.getElementById("attachapplnamediv").appendChild(document.getElementById("attachapplspan"))
	 ModalPopups.Cancel("ATTACHAPPLNAME");
}

function isDependantExists(depentfor,rowobj){
	var depname="";
	var rindex =  $(rowobj).index();
	
	$("#fnaClntAtchTbl > tbody > tr").each(function(i){
		
		if($(this).index() != rindex ){
			
			var dep = $(this).find("td:eq(1) select:eq(0)").val();
			
			if(dep == depentfor){
				depname = $(this).find("td:eq(1) input:eq(0)").val();
			}
			
			return depname
		}
		
	});
	return depname;
}

function addNTUCRow(){
	$("#imgFnaArtuPlanAddRow").trigger("click");
	
//	$("#fnaartuplanTblss tbody tr:last [name=txtFldRecomPpName]").val($("#txtFldClientName").val());
	
	$("#fnaartuplanTbl tbody tr:last [name=selFnaArtuPlanCompName]").val("NTUC Income");
}


function ntucArchiveClick(){
	
	if (!allPageValidation())
		return;
	
	
	if(!chkAllDataSaved("",false,FNA_CONS_MODIFY_INFO)){
		closePopupBox();
		return;
	}
		
	
	var agentcode = $("#verdettable tbody tr:first").find("td:eq(2) input:eq(7)").val();
	var fnaid = $("#verdettable tbody tr:first").find('td:eq(0)').find('input:first').val();// cell0.childNodes[0].value;
	var advstfcode =$("#verdettable tbody tr:first").find("td:eq(2) input:eq(7)").val();
	var lockimg =$("#verdettable tbody tr:first").find("td:eq(3) img:eq(1)");
	var jsonstring ;
	var spsNricChkSts=false;
	var blnAtlstOneNtuc=false;
	var blnSignatureSts=false;
	
	$("#hTxtFldFnaDsfSelfName").val("");
	$("#hTxtFldFnaDsfSpsName").val("");

	var parameter = "dbcall=VER&FNA_ID=" + fnaid;
	var respText = ajaxCall(parameter);

	$.each(respText,function(i,refarr){
		if(i==2){
			var fnsDetl=refarr['ARCH_FNASSDET_TABLE_DATA'];
			$("#hTxtFldFnaDsfSelfName").val(fnsDetl.dfSelfName);
			$("#hTxtFldFnaDsfSpsName").val(fnsDetl.dfSpsName);

			if(!isEmpty(fnsDetl.dfSpsName)){
				spsNricChkSts=true;
			}

		}

		if(i==22){
			var fnsPlnDets=refarr['ARCH_ADVRECPRDTPLAN_DATA'];

			if(typeof fnsPlnDets !== "undefined" && fnsPlnDets.length>0){
			$.each(fnsPlnDets,function(i,obj){
				if(obj.selFnaAdvRecCompName=='NTUC Income'){
					blnAtlstOneNtuc=true;
					return false;
				}

			});
			}

		}
		
		if(i == 35){
			var totalclientsign=4,clientsinged=0;
			
			var fnaSignature = refarr["ALL_SIGNATURE_DETS"];
			if(typeof fnaSignature !== "undefined" && fnaSignature.length>0){
				
				$.each(fnaSignature,function(k,v){
					
					if(v.hasOwnProperty("CLIENT_SIGNED") && v.CLIENT_SIGNED =='Y'){
						/*if(v.CLIENT_SIGN_PAGEREF == "INDEXPAGE"){							
						}*/
						
						clientsinged++;
					}
				});
				
				if(totalclientsign == clientsinged){
					blnSignatureSts=true;
//					return false;
				}
				
				/*$.each(fnaSignature,function(i,obj){
					if(obj.selFnaAdvRecCompName=='NTUC Income'){
						blnAtlstOneNtuc=true;
						return false;
					}

				});*/
				
				console.log("totalclientsign == clientsinged" + totalclientsign +","+ clientsinged)
			}
		}

	});



	if(!isEmpty(advstfcode)){

		var validtnArr = [];

		if( validateClntsDets($("#verdettable tbody tr:first"),spsNricChkSts)){
			ModalPopups.MandatoryPopupFPMS("ClntDetsMandLayer", {width:920,onOk:"",onCancel:"ModalPopupsPromptMandCancel()"});



		}else {


			var mandField=validateKycMandField(fnaid);


			if(mandField.length>0){
				alert(mandField);
				setPageFocus(12, FNA_FORMTYPE);
				return false;
			}

			//For At least NTUC Product
			if(!blnAtlstOneNtuc){
				var ntucRowSts=false;
				
				$("#fnaartuplanTbl tbody tr").each(function(){
					if($(this).find("[name=selFnaArtuPlanCompName]").val()=="NTUC Income"){
						ntucRowSts=true;
						return false;
					}	
				});
				
				if(!ntucRowSts){
					setPageFocus(14, FNA_FORMTYPE);
					alert(NTUC_PRODUCT_ALERT);
					addNTUCRow();
				}
				
				return false;
			}
			
			
//			alert("blnSignatureSts=====>"+blnSignatureSts);
			
			//									Signature validation
			if(!blnSignatureSts){
				alert(CLIENT_SIGN_B4_DSF);
				$(this).closest("td").find("[name=version]").click();
				setPageFocus(1, FNA_FORMTYPE);
				setTimeout(function(){openPopupBox();},700);
				return false;
			}
			
			
			var checkChange=$("#chkanychange").val();

			if(!isEmpty(checkChange) && !$.isEmptyObject($.parseJSON(checkChange)) && NTUCChkChngSts){

				if(!chkAllDataSaved("",false,FNA_CONS_MODIFY_INFO))return;
//				chkAllDataSaved("",false);
//				hidePagingDiv();
				$( "#reportvaliationPopup" ).dialog( "option", "height",280);
				$( "#reportvaliationPopup" ).dialog( "option", "width",500);
//				$("#reportvaliationPopup table:first tbody tr:first h3:first").html(" You have changed some data in current FNA.<br/> Click OK to save and proceed.<br/>Click Cancel to ignore changes.");
				$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();


				var buttons = $( ".reportvaliationPopup" ).dialog( "option", "buttons" );

				$('#reportvaliationPopup .ui-dialog-buttonset button:first').remove();

				$( "#reportvaliationPopup" ).dialog( "option", "buttons",
						[
						    {
						      text: "OK",
						      click: function() {
										$('#reportvaliationPopup').dialog("close");
										$("#saveBtn-button").click();
						      }

						    },
						    {
						    	text : "Cancel",
						    	click : function(){
						    		$('#reportvaliationPopup').dialog("close");
//								  	$("#tabs-template-li").hide();
						    	}
						    }
						 ]
				);


//				NTUCChkChngSts=false;
			}
			

			callDSF(fnaid,agentcode,lockimg);
		}

		$("#ClntDetsMandLayer_okButton").click(function(){

			if(!chkClntMandtryDoc())return;
			
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");

			loadMsg.style.display = "block";
			// alert(loadMsg.style.display)
			loadingLayer.style.display = "block";

			if(!assingClntMandVal("+isCustNricAttached+",this)){
//				alert("Attachment upload failed");
				loadMsg.style.display = "none";
				return;
			}

			var mandField=validateKycMandField(fnaid);


			if(mandField.length>0){
				alert(mandField);
				setPageFocus(12, FNA_FORMTYPE);
				return false;
			}

			//For At least NTUC Product
			if(!blnAtlstOneNtuc){
				alert(NTUC_PRODUCT_ALERT);
				setPageFocus(14, FNA_FORMTYPE);
				return false;
			}

			callDSF(fnaid,agentcode,lockimg);

		});


	}else{

		alert("Adviser is not registered with NTUC Income. Please contact compliance team.");

	}
}

function setSpouseCombo(arrObj,keyOpt,$elmobj){
	$elmobj.find("option:not(:first)").remove();
		
		$.each(arrObj,function(i,obj){
//			console.log(obj.txtFldSpouseName);
			if(isEmpty(obj[keyOpt]))return;
			
			var optTxt='<option data-nric="'+obj['dfSpsNric']+'" value="'+obj[keyOpt]+'">'+obj[keyOpt]+'</option>';
			
			$elmobj.append(optTxt);
		});
	}

	function clearSpsfld(){
		$("#tblSelfSps tbody #radSpsSG").prop("checked",false);
		
		if($("#tblSelfSps tbody #radSpsOTH")[0].checked){
			$("#tblSelfSps tbody #radSpsSG").trigger("click").prop("checked",false);
		}
		
		if($("#tblSelfSps tbody #radSpsSGPR")[0].checked){
			$("#tblSelfSps tbody #radSpsSG").trigger("click").prop("checked",false);
		}
		
		
		$.each(spsClearArr,function(i,obj){
			$("[name="+obj+"]").val("");
		});
	}
   
  //-------------------------------RISK PREF RELATED
  function page12fixedHeader(){
	if (!page12Flag) {

		if (document.getElementById("page12")) {
//			fixedHeader('fnaRiskPrefTbl','1','100%','0px', '100%', '15em', '2', '0px', '98.2%', '5em', '98.2%',false,false,0,0);
//			fixedHeader('fnaRiskPrefTbl', '1', '100%', '0px', '100%', '25.2em', '2', '0px', '95%','3.1em', '95%', false,false, 0, 0);
			page12Flag = true;
		}
	}
}


function fnaRiskPrefView(obj){
	
	var rowLen=$("#fnaRiskPrefTbl tbody tr").length;
/*	if(rowLen>=2){
		alert("Only two Risk Preference are allowed.");
		return;
	}*/
	
	$( "#riskPrefRowView" ).dialog({
    	autoOpen: false,
    	title:"Risk Preference",
    	height:'550', 
    	width:'900',
    	modal: true,
    	resizable: false,
        buttons: {
          "Add": function() {
        	 
        	  
        	  
        	  var ta = ["Retirement","Education","Accumlation","Others"]
        	  var amta = [100,200,300,400]
        	  
        	  for(var temp = 0;temp<4;temp++){
        		  
        		  fnaRiskPrefAddRow($(this)); 
        		  
            	  var tblobj = document.getElementById("fnaRiskPrefTbl");
            		var tbodyobj = tblobj.tBodies[0];

            		var rowlenobj = tbodyobj.rows.length;
            		
            		
            		tbodyobj.rows[rowlenobj - 1 ].cells[1].childNodes[0].value='CLIENT';
            		tbodyobj.rows[rowlenobj - 1 ].cells[2].childNodes[0].value=ta[temp];
            		tbodyobj.rows[rowlenobj - 1 ].cells[3].childNodes[0].value=amta[temp];
            		tbodyobj.rows[rowlenobj - 1 ].cells[4].childNodes[0].value="2";
            		tbodyobj.rows[rowlenobj - 1 ].cells[5].childNodes[0].value="Y";
            		
        		  
        	  }
        	
        	  $( this ).dialog( "close" );
        		
          },
          "Close": function() {
            $( this ).dialog( "close" );
          }
        },
    	close: function( event, ui ) { $(this).dialog( "destroy" );$(this).find("select,textarea,input").val("");$(this).find("input[type=checkbox]").attr("checked",false);},
    	open: function( event, ui ) {console.log("open");}
    });
	
	$( "#riskPrefRowView" ).dialog( "open");
}

function fnaRiskPrefAddRow(obj){
	/*var tblobj = document.getElementById(tblid);
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	var rowobj = tbodyobj.insertRow(rowlenobj);

	var cell0 = rowobj.insertCell(0);
	cell0.style.textAlign = "left";
	cell0.innerHTML = '<input type = "text" class="fpNonEditTblTxt" name="txtFldFnaDepntName" maxlength="60"  onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"class="fpEditTblTxt" />'
					+ '<input type="hidden" name="txtFldriskPrefMod" value="'+ INS_MODE+ '"/>' + '<input type="hidden" name="riskPrefId"/>';

	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<select name="selFnaDepntRel" class="kycSelect" ></select>';
	
	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.style.border="1px solid black";
	cell1.innerHTML = '<input type = "hidden"  name="txtFldFnaSwrepPlanMode" value="'+INS_MODE+'" />';
	
	*/
	
	var tblobj = document.getElementById("fnaRiskPrefTbl");
	var tbodyobj = tblobj.tBodies[0];

	var rowlenobj = tbodyobj.rows.length;
	var jqRwLen=$("#fnaRiskPrefTbl tbody tr:visible").length;
	var rowobj = tbodyobj.insertRow(rowlenobj);
	
	var cell0 = rowobj.insertCell(0);
	cell0.innerHTML =  '<input type="text" readonly="readonly" name="txtFldFnaRiskSino" value="'+(Number(jqRwLen)+1)+'" style="width:92%;text-align:center;"/>'	
	cell0.style.textAlign = "center";
	
	var cell0a = rowobj.insertCell(1);
	cell0a.innerHTML =  '<select class="kycSelect"  name="selFnaRiskFor" disabled="disabled"><option value="">--SELECT--</option><option value="CLIENT">CLIENT</option><option value="SPOUSE">SPOUSE</option></select>';	
	cell0a.style.textAlign = "center";
	$(cell0a).find("select:first").val($(obj).find("#selCrRiskprefFor").val());
	
	var cell1 = rowobj.insertCell(2);
	cell1.style.textAlign = "left";
	cell1.innerHTML ='<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskInvObj" value="'+$(obj).find("#txtFldCrInvstobj").val()+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:98%;"/>'
			+ '<input type="hidden" name="txtFldFnaRiskMode" value="'+ INS_MODE+ '"/>'
			+ '<input type="hidden" name="txtFldFnaRiskId"/>';	
	
	var cell2 = rowobj.insertCell(3);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type = "text" class="fpEditTblTxt"  name="txtFldFnaRiskAmtInv" value="'+$(obj).find("#txtFldCrInvstamt").val()+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
	
	var cell3 = rowobj.insertCell(4);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskTimeHr" value="'+$(obj).find("#txtFldCrInvsttimehorizon").val()+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
	 
	var cell4 = rowobj.insertCell(5); 
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<select class="kycSelect"  name="selFnaRiskInvObj" style="width:60%;"><option value="">--SELECT--</option><option value="Y">Yes</option><option value="S">Some</option><option value="N">No</option></select>'
					+ '<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskRp"  onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:34%;"/>';
	$(cell4).find("select:first").val($(obj).find("#selCrRiskpref").val());
	$(cell4).find("input:first").val(getSelectedValue($(obj).find("[name=chkCrRiskclass]")));
	
	var cell5 = rowobj.insertCell(6);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskROI" value="'+$(obj).find("#txtFldCrRoi").val()+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:98%;"/>'			
	
	var cell6 = rowobj.insertCell(7);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<select class="fpEditTblTxt"   name="SelFnaRiskAdequateFnds" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" ><option value="">--select--</option><option value="Y">YES</option><option value="N">NO</option></select>';
	$(cell6).find("select:first").val($(obj).find("#selCrAdeqfund").val());
	
	var cell6 = rowobj.insertCell(8);
	cell6.style.textAlign = "left";
	cell6.innerHTML = '<input type = "text" class="fpEditTblTxt"   name="txtFldFnaRiskOthrCon" value="'+$(obj).find("#txtFldCrOthconcern").val()+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" />';
	
//	$("#riskPrefRowView").dialog( "close" );
}

function chkClntSpsRiskPref(obj){
	var strRefVal=obj.value;
	var rowLen=$("#fnaRiskPrefTbl tbody tr").length;
	
	if(rowLen>0){
		$("#fnaRiskPrefTbl tbody tr").each(function(){
			var strCrntVal=$(this).find("td:eq(1) select:first").val();
			if(strCrntVal==strRefVal){
				if ($("#riskPrefRowView").hasClass('ui-dialog-content')) {
				$("#riskPrefRowView").dialog( "close" );
				}
				alert("Client Risk Preference already inserted.");
			}
		});
	}
}

function validateFnaRiskPrefDets() {

	var tblobj = document.getElementById("fnaRiskPrefTbl");
	var tbodyobj = tblobj.tBodies[0];
	var rowlenobj = tbodyobj.rows.length;

	if (rowlenobj > 0) {
//		for (var rl = 0; rl < rowlenobj; rl++) {
//			if (!checkMandatoryField(tbodyobj.rows[rl].cells[0].childNodes[0],FNA_INVPLANINVTYPE_VALMSG))
//				return;
//		}
	}

	document.getElementById("hTxtFldFnaRiskPrefCnt").value = rowlenobj;

	return true;
}
  //-------------------------------RISK PREF RELATED
	
//	----------------- GOJS RELATED

function openSignaturePage(signfor,page,liobject){
	
	switch(page){
	case "INDEXPAGE":
		setPageFocus(1, FNA_FORMTYPE);		
		break;
	case "TAXPAGE":
		setPageFocus(20, FNA_FORMTYPE);
		break;
	case "SIGNPAGE":
		setPageFocus(31, FNA_FORMTYPE);
		break;
	case "DECLAREPAGE":
		if(signfor.toUpperCase() == "MANAGER"){
			setPageFocus(19, FNA_FORMTYPE);	
		}else if(signfor.toUpperCase() == "MANAGER"){
			setPageFocus(19, FNA_FORMTYPE);	
		}else{
			setPageFocus(18, FNA_FORMTYPE);	
		}
		
		break;
	}
	
	openSignaturePad(signfor, page)
	
	
}
	
	function openSignaturePad(signfor,page){
		
		if(signfor != "manager"){
			
			if(!chkAllDataSaved("",false,FNA_CONS_REQ_INFO)){
				
//				$('#reportvaliationPopup').dialog('option', 'title', 'eKYC - Signature Validation');
//				$("#page16msg").html("");
//				$("#page16msg").html( '<h3 style="color: red;font-size: 15pt;"></h3>');
//				$("#reportvaliationPopup table:first tbody tr:first h3:first").html(FNA_CONS_REQ_INFO)
					
					return;
				}
			
		}
		
		var fnaId = $("#txtFldFnaId").val();
		
		 var custSignsts = $("#hTxtFldCustSignStatus").val();
		 
		 var mgrmailsent = $("#mgrEmailSentFlg").val();
			var custsignstats = $("#hTxtFldCustSignStatus").val();
			var ntuccaseid = $("#ntucCaseId").val();
			var ntucexist = chkAnyNTUCExist();
			
		 if(signfor.toLowerCase() == "adviser"){

			 var isClientSignDone = chkClientSignatureDone();
			 
			 if(!isClientSignDone){
				 alert("Please sign the FNA Form then proceed to esubmission!");
				 return false;
			 }	
			 
			 var page2spouseName = $("#dfSpsName").val();
			 if(!isEmpty(page2spouseName)){
				 
				 var isClient2SignDone = chkClient2SignatureDone();
				 
				 if(!isClient2SignDone){
					 alert("Please sign the FNA Form then proceed to esubmission!");
					 return false;
				 }	
				 
			 }
			 
			 
			 

			 
			 
		 }
		 
		 var agree=false ,disagree=false,reason="";
		 var agreefld ,disagreefld;
		 
		 if(signfor == "manager"){
			 
				agreefld  =  $("input[name='suprevMgrFlg']:eq(0)");
				disagreefld =$("input[name='suprevMgrFlg']:eq(1)");
				
				agree = $("input[name='suprevMgrFlg']:eq(0)").prop("checked");
				disagree = $("input[name='suprevMgrFlg']:eq(1)").prop("checked");	
				reason = $("textarea[name='suprevFollowReason']").val();
				
				
				
				
				if(!agree && !disagree){
					closePopupBox();
					alert(FNA_APPR_MGR_AGREE )
					$("#tr_mgr_agreesec").css({"border":"1px solid red"});
					$("input[name='suprevMgrFlg']").closest("td").css({"background-color":"yellow"});
					return false;
				}
				
				if(!agree && disagree){
					if(isEmpty(reason)){
						alert(MANAGER_KEYIN_REMARKS);
						return false;
					}
				}
				
				if(agree){
					var conf = window.confirm(CONFIRM_APPROVE_MANAGER);
					if(!conf){
						var loadMsg = document.getElementById("loadingMessage");
						var loadingLayer = document.getElementById("loadingLayer");
						loadMsg.style.display="none";
//						selObj.value = "";
						
						agreefld.prop("checked",false);
						disagreefld.prop("checked",false);
						
						return false;
					}else{
						agreefld.prop("checked",true).prop("disabled",true);
						disagreefld.prop("checked",false).prop("disabled",true);
						
					}	
				}
				
				if(disagree && !agree){
					var conf = window.confirm(CONFIRM_REJECT_MANAGER);
					if(!conf){
						var loadMsg = document.getElementById("loadingMessage");
						var loadingLayer = document.getElementById("loadingLayer");
						loadMsg.style.display="none";
						agreefld.prop("checked",false);
						disagreefld.prop("checked",false);
//						selObj.value = "";
						return false;
					}
				}
				
				
		 }
		
		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");
		loadMsg.style.display="";
		
		var jsnResponse = "";
		   
		  $.ajax({
		  url: "ESignatureServlet",
		  // async:false,
		  type: "post",
		  data: {
			  fnaid:fnaId,
			  param:"fetch",
			  pageref:page
		  	},
		  success : function(data){
			  var response = $.parseJSON(data);
			  
			  
			  if (response["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
			  }
			//  alert("response->"+response)
			  $.each(response, function(count, arr) {
				  
				  $.each(arr,function(tab,data){
					  
//					  alert(tab + "="+JSON.stringify(data));
					  console.log(tab + "="+JSON.stringify(data));
					  
					  if(tab == "ADVISER"){						  
						  $("#adviserSignSavedDiagram").val(JSON.stringify(data));
					  }
					  
					  if(tab == "MANAGER"){						  
						  $("#managerSignSavedDiagram").val(JSON.stringify(data));
					  }
					  
					  if(tab == "CLIENT"){
						  $("#clientSignSavedDiagram").val(JSON.stringify(data));
						  
					  }
					  
					  
				  })
				  
			  })

			  
			  
			  showeSign(signfor,page,agreefld,disagreefld);
				
			  loadESign('client',page);
			  loadESign('adviser',page);
			  loadESign('manager',page);
			  loadESign('spouse',page);
			  
			  if(signfor == "client"){
				  $("#divClientSign").show().addClass("active");
				  $("#divAdviserSign").hide().removeClass("active");
				  $("#divManagerSign").hide().removeClass("active");
				  $("#divASpouseSign").hide().removeClass("active");
				  
				  $(".naccs .menu div#divClientSign").trigger("click");
			  }
			  
			  if(signfor == "adviser"){
				  
				  var custsignstats = $("#hTxtFldCustSignStatus").val();
				  
				  $("#divClientSign").hide().removeClass("active");;
				  $("#divAdviserSign").show().addClass("active");
				  $("#divManagerSign").hide().removeClass("active");
				  $("#divASpouseSign").hide().removeClass("active");
				  $(".naccs .menu div#divAdviserSign").trigger("click");
			  }
			  
			  if(signfor == "manager"){
				  
				  var advisersignstats = $("#hTxtFldAdvSignStatus").val();
				  
				  $("#divClientSign").hide().removeClass("active");;
				  $("#divAdviserSign").hide().removeClass("active");
				  $("#divManagerSign").show().addClass("active");
				  $("#divASpouseSign").hide().removeClass("active");
				  $(".naccs .menu div#divManagerSign").trigger("click");
			  }
			  
			  if(signfor == "spouse"){
				  
				  var spousesignstats = $("#hTxtFldSpsSignStatus").val();
				  
				  $("#divClientSign").hide().removeClass("active");;
				  $("#divAdviserSign").hide().removeClass("active");
				  $("#divManagerSign").hide().removeClass("active");
				  $("#divASpouseSign").show().addClass("active");
				  $(".naccs .menu div#divASpouseSign").trigger("click");
			  }
			  
			  loadMsg.style.display="none";
	  	  }
		  });
		
		
		
	}


	function showeSign(signfor,page,agreefld,disagreefld){
		
		
		var dialog = $( "#eSignJSP" ).dialog({
			cache: false,
		      autoOpen: false,
		      title : "eKYC : e-Signature",
		      height: 550,
		      width: 980,
		      modal: true,
		      resizable: false,
		      draggable: false,
		      buttons: { 
		    	  " SAVE ": function(){ saveClientSign (dialog,signfor,page); },
		    	  " CLOSE ": function(){ 
		    		  dialog.dialog( "close" );
		    		  if(agreefld)
		    			  agreefld.prop("disabled",false);
		    		  if(disagreefld)
		    			  disagreefld.prop("disabled",false);
		      }},
		      close: function(){}

		    });

		 
			$( "#eSignJSP" ).show();
			dialog.dialog( "open" );
			
		 
	}
	
	function saveClientSign(dialog,signfor,page){
		
		console.log("saveClientSign  signfor -->"  +  signfor +",->"+page);

		var signData=null;
		var clientimage,adviserimage,managerimage,spouseimage;
		
		var fnaId = $("#txtFldFnaId").val();
		
		var  parameter = "fnaid:"+fnaId;
		
		var jsnResponse = "";
		
		var ac = 0;
		
		
		var emptycanvas = '{"position": "0 0","model": { "class": "GraphLinksModel","nodeDataArray": [ {"stroke":"blue","strokeWidth":3,"category":"FreehandDrawing","key":-1, "loc":"0 0", "geo":""}],"linkDataArray": []}}';
		
		
	if(signfor == "adviser"){
		
		 var str = '{ "position": "' + go.Point.stringify(adviserSignDiagram.position) + '",\n  "model": ' + adviserSignDiagram.model.toJson() + ' }';
    	 document.getElementById("adviserSignSavedDiagram").value = str;
			
			var actualsign = document.getElementById("adviserSignSavedDiagram").value.trim();
			
			var a1 = JSON.parse(emptycanvas);
			var a2 = JSON.parse(actualsign);
			
			var emptygeo = a1.model.nodeDataArray[0].geo, actualgeo="";
			var totgeo = a2.model.nodeDataArray.length
			
			for(var g=0;g<totgeo;g++){
				actualgeo += a2.model.nodeDataArray[g].geo;	
			}
			console.log("actualgeo->"+actualgeo +"\nemptygeo->" + emptygeo);
			
			if(actualgeo == emptygeo){
				alert(FNA_EMPTY_SIGN);
				return false;
			}			

		}
	
	if(signfor == "client"){
		
		 var str = '{ "position": "' + go.Point.stringify(clientSignDiagram.position) + '",\n  "model": ' + clientSignDiagram.model.toJson() + ' }';
    	 document.getElementById("clientSignSavedDiagram").value = str;
		
		var actualsign = document.getElementById("clientSignSavedDiagram").value.trim();
		
		console.log("1->"+actualsign)
		console.log("2->"+emptycanvas)
		
		var a1 = JSON.parse(emptycanvas);
		var a2 = JSON.parse(actualsign);
		
		var emptygeo = a1.model.nodeDataArray[0].geo, actualgeo="";
		var totgeo = a2.model.nodeDataArray.length
		
		for(var g=0;g<totgeo;g++){
			actualgeo += a2.model.nodeDataArray[g].geo;	
		}
		console.log("actualgeo->"+actualgeo +"\nemptygeo->" + emptygeo);
		
		if(actualgeo == emptygeo){
			alert(FNA_EMPTY_SIGN);
			return false;
		}			

	}
	
	if(signfor == "manager"){
		
		
		
		var str = '{ "position": "' + go.Point.stringify(managerSignDiagram.position) + '",\n  "model": ' + managerSignDiagram.model.toJson() + ' }';
   	 document.getElementById("managerSignSavedDiagram").value = str;
		
		var actualsign = document.getElementById("managerSignSavedDiagram").value.trim();
		
		var a1 = JSON.parse(emptycanvas);
		var a2 = JSON.parse(actualsign);
		
		var emptygeo = a1.model.nodeDataArray[0].geo, actualgeo="";
		var totgeo = a2.model.nodeDataArray.length
		
		for(var g=0;g<totgeo;g++){
			actualgeo += a2.model.nodeDataArray[g].geo;	
		}
		console.log("actualgeo->"+actualgeo +"\nemptygeo->" + emptygeo);
		
		if(actualgeo == emptygeo){
			alert(FNA_EMPTY_SIGN);
			return false;
		}			

	}
		
		if(signfor == "spouse"){
			
			
			 var str = '{ "position": "' + go.Point.stringify(spouseSignDiagram.position) + '",\n  "model": ' + spouseSignDiagram.model.toJson() + ' }';
        	 document.getElementById("spouseSignSavedDiagram").value = str;
			
			var actualsign = document.getElementById("spouseSignSavedDiagram").value.trim();
			
			var a1 = JSON.parse(emptycanvas);
			var a2 = JSON.parse(actualsign);
			
			var emptygeo = a1.model.nodeDataArray[0].geo, actualgeo="";
			var totgeo = a2.model.nodeDataArray.length
			
			for(var g=0;g<totgeo;g++){
				actualgeo += a2.model.nodeDataArray[g].geo;	
			}
			console.log("actualgeo->"+actualgeo +"\nemptygeo->" + emptygeo);
			
			if(actualgeo == emptygeo){
				alert(FNA_EMPTY_SIGN);
				return false;
			}			

		}
		
		
		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");

		loadMsg.style.display = "block";
		// alert(loadMsg.style.display)
		loadingLayer.style.display = "block";
		
		$("#clientSignDiagramDiv,#adviserSignDiagramDiv,#managerSignDiagramDiv,#spouseSignDiagramDiv").each(function(){
			var imgid = $(this).prop("id")
			
//			console.log($(this).html());
//			clientSignDiagram
//			adviserSignDiagram
//			managerSignDiagram
				
				html2canvas($(this), {
		             onrendered: function (canvas) {
		            	 
		            	 ac++;
		            	 
		            		if(imgid == "clientSignDiagramDiv"){
		            			
		            			  var imgageData = canvas.toDataURL("image/png");
				                    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
				                    clientimage = newData;
				                    
		            			 var str = '{ "position": "' + go.Point.stringify(clientSignDiagram.position) + '",\n  "model": ' + clientSignDiagram.model.toJson() + ' }';
				            	 document.getElementById("clientSignSavedDiagram").value = str;
				            	 
				            	 parameter += ",clientSignSavedDiagram:"+$('#clientSignSavedDiagram').val()
				            	 parameter += ",clientpageref:"+page
		            		}
		            		
		            		if(imgid == "adviserSignDiagramDiv"){
		            			
		            			  var imgageData = canvas.toDataURL("image/png");
				                    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
				                    adviserimage = newData;
		            			
		            			 var str = '{ "position": "' + go.Point.stringify(adviserSignDiagram.position) + '",\n  "model": ' + adviserSignDiagram.model.toJson() + ' }';
				            	 document.getElementById("adviserSignSavedDiagram").value = str;
				            	 parameter += ",adviserSignSavedDiagram:"+$('#adviserSignSavedDiagram').val()
				            	 parameter += ",adviserpageref:"+page
		            		}
		            		
		            		if(imgid == "managerSignDiagramDiv"){
		            			
		            			  var imgageData = canvas.toDataURL("image/png");
				                    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
				                    managerimage = newData;
		            			
		            			 var str = '{ "position": "' + go.Point.stringify(managerSignDiagram.position) + '",\n  "model": ' + managerSignDiagram.model.toJson() + ' }';
				            	 document.getElementById("managerSignSavedDiagram").value = str;
				            	 parameter += ",managerSignSavedDiagram:"+$('#managerSignSavedDiagram').val()
				            	 parameter += ",managerpageref:"+page
		            		}

		            		if(imgid == "spouseSignDiagramDiv"){
		            			
		            			  var imgageData = canvas.toDataURL("image/png");
				                    var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
				                    spouseimage = newData;
		            			
		            			 var str = '{ "position": "' + go.Point.stringify(spouseSignDiagram.position) + '",\n  "model": ' + spouseSignDiagram.model.toJson() + ' }';
				            	 document.getElementById("spouseSignSavedDiagram").value = str;
				            	 parameter += ",spouseSignSavedDiagram:"+$('#spouseSignSavedDiagram').val()
				            	 parameter += ",spousepageref:"+page
		            		}
		            		
		            		
		            		if(ac == 4){
		            			
		            			
		            			
		            			
		            			  $.ajax({
		            	    		  url: "ESignatureServlet",
		            	    		  type: "post",
		            	    		  data: {
		            	    			  fnaid:fnaId,
		            	    			  signfor :signfor,
		            	    			  pageref:page,
		            	    			  clientSignSavedDiagram:$('#clientSignSavedDiagram').val(),
		            	    			  clientbase64signdata:clientimage,
		            	    			  
		            	    			  adviserSignSavedDiagram:$('#adviserSignSavedDiagram').val(),
		            	    			  adviserbase64signdata:adviserimage,
		            	    			  
		            	    			  managerSignSavedDiagram:$('#managerSignSavedDiagram').val(),
		            	    			  managerbase64signdata:managerimage,
		            	    			  
		            	    			  spouseSignSavedDiagram:$('#spouseSignSavedDiagram').val(),
		            	    			  spousebase64signdata:spouseimage
		            	    			  
		            	    			  
		            	    		  	},
		            	    		  success : function(data){
		            	    			  
		            	    			  var response = $.parseJSON(data);
		            	    			  
		            	    			  $.each(response,function(key,val){
		            	    				  
		            	    				 if(val.client){
		            	    					 
		            	    					 
			            	    				 alert("Client(1) Signature captured successfully");
			            	    				  
//			            	    				 var page1_clientsign1 = document.getElementById("page1_clientsign");
//			            	 					page1_clientsign1.setAttribute("src", 'images/lock.png');
//			            	 					page1_clientsign1.setAttribute("class", "");
//			            	 					page1_clientsign1.setAttribute("onclick", "");
//			            	 					$(".msg_client_sign").show();
//			            	 					$("input[name='txtFldClntSignDate']").val(currDate);
//			            	 					page1_clientsign1.style.display="block"
//			            	 					
//			            	 					
//			            	 					 var page12_clientsign = document.getElementById("page12_clientsign");
//			            	 					page12_clientsign.setAttribute("src", 'images/lock.png');
//			            	 					page12_clientsign.setAttribute("class", "");
//			            	 					page12_clientsign.setAttribute("onclick", "");
//				            	 					$(".msg_client_sign").show();
			            	    				
			            	    				var imageid = "",imgmsgid="",imgdatefldid="",popup_imgid="";
			            	    				
			            	    				if(page == "INDEXPAGE"){
			            	    					imageid = "page1_clientsign";
			            	    					imgmsgid = "msg_client_sign_1";
			            	    					imgdatefldid = "txtFldClntSignDatePage1";
			            	    					popup_imgid="clnt_img_1"
			            	    				}
			            	    				
			            	    				
			            	    				if(page == "TAXPAGE"){
			            	    					imageid = "taxpage_clientsign";
			            	    					imgmsgid = "msg_client_sign_tax";
			            	    					imgdatefldid = "txtFldClntSignDateTax";
			            	    					popup_imgid="clnt_img_11";
			            	    				}
			            	    				
			            	    				if(page == "SIGNPAGE"){
			            	    					imageid = "signpage_clientsign";
			            	    					imgmsgid = "msg_client_sign_signpage";
			            	    					imgdatefldid = "txtFldClntSignDateSignpage";
			            	    					popup_imgid="clnt_img_12";
			            	    				}
			            	    				
			            	    				if(page == "DECLAREPAGE"){
			            	    					imageid = "page12_clientsign";
			            	    					imgmsgid = "msg_client_sign_declarepage";
			            	    					imgdatefldid = "txtFldClntSignDateDeclare";
			            	    					popup_imgid="clnt_img_13";
			            	    				}
			            	    				 
			            	    				afterSuccessSignature(imageid, imgmsgid, imgdatefldid,page,"hTxtFldCustSignStatus",popup_imgid);
			            	    				
			            	    				
//			            	    				$("#hTxtFldCustSignStatus").val("Y");
				            	 					
				            	 					
				            	 				$('#clientSignSavedDiagram').val('{"position": "0 0","model": { "class": "GraphLinksModel","nodeDataArray": [ {"stroke":"blue","strokeWidth":3,"category":"FreehandDrawing","key":-1, "loc":"0 0", "geo":""}],"linkDataArray": []}}')	            	    			  
						            	    			
		            	    				 }
		            	    				 
		            	    				 if(val.adviser){
		            	    					 
		            	    					
		            	    					 var custsignstat = $("#hTxtFldCustSignStatus").val();
		            	    					 
		            	    					 
			            	    				 alert("Adviser Signature captured successfully");
			            	    				 
//			            	    				 var page13_advisersign = document.getElementById("page13_advisersign");
//			            	 					page13_advisersign.setAttribute("src", 'images/lock.png');
//			            	 					page13_advisersign.setAttribute("class", "");
//			            	 					page13_advisersign.setAttribute("onclick", "");
//			            	 					$("#msg_adviser_sign").show();
//			            	 					$("input[name='txtFldAdvSignDate']").val(currDate);
			            	    					
			            	    					var imageid = "",imgmsgid="",imgdatefldid="";
			            	    					
			            	    					
			            	    					if(page == "SIGNPAGE"){
				            	    					imageid = "signpage_advisersign";
				            	    					imgmsgid = "msg_adviser_sign_signpage";
				            	    					imgdatefldid = "txtFldAdvSignDateSignpage";
				            	    					popup_imgid="adv_img_12";
				            	    				}
				            	    				
				            	    				if(page == "DECLAREPAGE"){
				            	    					imageid = "page13_advisersign";
				            	    					imgmsgid = "msg_adviser_sign_declarepage";
				            	    					imgdatefldid = "txtFldAdvSignDateDeclarepage";
				            	    					popup_imgid="adv_img_13";
				            	    				}
			            	    					
			            	    					afterSuccessSignature(imageid, imgmsgid, imgdatefldid,page,"hTxtFldAdvSignStatus",popup_imgid);
			            	    					
			            	    					
			            	    				$('#adviserSignSavedDiagram').val('{"position": "0 0","model": { "class": "GraphLinksModel","nodeDataArray": [ {"stroke":"blue","strokeWidth":3,"category":"FreehandDrawing","key":-1, "loc":"0 0", "geo":""}],"linkDataArray": []}}')

			            	    				
			            	    				var isClientSignDone = chkClientSignatureDone();
			            	    				var isClient2SignDone  = true;
			            	    				 var page2spouseNameTmp = $("#dfSpsName").val();
		            	    					 if(!isEmpty(page2spouseNameTmp)){
		            	    						 isClient2SignDone = chkClient2SignatureDone();
		            	    					 }
			            	    				var isAdviserSignDone = chkAdviserSignatureDone();
			            	    				 
			            	    				
			            	    				 var mgrmailsent = $("#mgrEmailSentFlg").val();
		            	    					var custsignstats = $("#hTxtFldCustSignStatus").val();
		            	    					var ntuccaseid = $("#ntucCaseId").val();
		            	    					var ntucexist = chkAnyNTUCExist();
//		            	    					if(isEmpty(ntuccaseid) && !ntucexist && mgrmailsent != "Y"){
//		            	    						if(custsignstats == "Y"){
//		            	    							 if( window.confirm("Do you want to send this FNA Form to manager review?")){
//		            	    								  sendMailtoManger()
//		            	    							  }
//		            	    						}	
//		            	    					}
		            	    					
		            	    					
		            	    					
		            	    					if(isClientSignDone && isClient2SignDone && isAdviserSignDone){
		            	    						 if( window.confirm("Do you want to send this FNA Form to manager review?")){
	            	    								  sendMailtoManger();
	            	    							  }
		            	    					}
			            	    				
//			            	    				 $("#hTxtFldAdvSignStatus").val("Y");
		            	    				 }
		            	    				 
		            	    				 if(val.manager){
		            	    					 
		            	    					
			            	    				  alert("Manager Signature captured successfully");
			            	    				 
			            	    				 
			            	    				  
			            	    				  
//			            	    				  var page13_managersign = document.getElementById("page13_managersign");
//			            	  					page13_managersign.setAttribute("src", 'images/lock.png');
//			            	  					page13_managersign.setAttribute("class", "");
//			            	  					page13_managersign.setAttribute("onclick", "");
//			            	  					$("#msg_manager_sign").show();
//			            	  					$("input[name='txtFldMgrSignDate']").val(currDate);
			            	    				  
			            	    				  
			            	    				  if(page == "DECLAREPAGE"){
			            	    					  popup_imgid="mgr_img_13";
			            	    					  afterSuccessSignature( "page13_managersign", "msg_manager_sign_Declarepage", "txtFldMgrSignDateDeclarepage","DECLAREPAGE",null,popup_imgid);
			          							}
			            	    				  
			            	    				  $("#hTxtFldMgrSignStatus").val("Y");
			            	  					 
			            	  					$('#managerSignSavedDiagram').val('{"position": "0 0","model": { "class": "GraphLinksModel","nodeDataArray": [ {"stroke":"blue","strokeWidth":3,"category":"FreehandDrawing","key":-1, "loc":"0 0", "geo":""}],"linkDataArray": []}}')	            	    			  
				            	    			  
				            	    			  
//			            	  					 setTimeout(function(){
//			            	  						 
//				            	  					  alert("Click OK to continue...");
//				            	  					
////				            	  					$("#btnEKYCOK").trigger("click")
//			            	  						 
//			            	  					 }, 300);
			            	  					
			            	  					
//			            	  					Manager Auto trigger
			            	  					
			            	  					updateManagerStatus('manager',false);
			            	  					
			            	  				  
			            	    				  
		            	    				 }
		            	    				 
		            	    				 if(val.spouse){
		            	    					 
//		            	    					 $("#hTxtFldSpsSignStatus").val("Y");
			            	    				  alert("Client(2) Signature captured successfully");
			            	    				  
//			            	    				  var page1_spousesign = document.getElementById("page1_spousesign");
//			            	    				  page1_spousesign.setAttribute("src", 'images/lock.png');
//			            	    				  page1_spousesign.setAttribute("class", "");
//			            	    				  page1_spousesign.setAttribute("onclick", "");
//				            	 					$(".msg_spouse_sign").show();
//				            	 					
//				            	 					page1_spousesign.style.display="block";
//				            	 					
//				            	 					
//				            	 					 var page12_spousesign = document.getElementById("page12_spousesign");
//				            	 					page12_spousesign.setAttribute("src", 'images/lock.png');
//				            	 					page12_spousesign.setAttribute("class", "");
//				            	 					page12_spousesign.setAttribute("onclick", "");
//					            	 				$(".msg_spouse_sign").show();
//			            	  					
//					            	 				$("input[name='txtFldSpsSignDate']").val(currDate);
			            	    				  
			            	    					var imageid = "",imgmsgid="",imgdatefldid="";
				            	    				
				            	    				if(page == "INDEXPAGE"){
				            	    					imageid = "page1_spousesign";
				            	    					imgmsgid = "msg_spouse_sign_1";
				            	    					imgdatefldid = "txtFldSpsSignDatePage1";
				            	    					popup_imgid = "sps_img_1";
				            	    				}
				            	    				
				            	    				
				            	    				if(page == "TAXPAGE"){
				            	    					imageid = "taxpage_spousesign";
				            	    					imgmsgid = "msg_spouse_sign_tax";
				            	    					imgdatefldid = "txtFldSpsSignDateTax";
				            	    					popup_imgid = "sps_img_11";
				            	    				}
				            	    				
				            	    				if(page == "SIGNPAGE"){
				            	    					imageid = "signpage_spousesign";
				            	    					imgmsgid = "msg_spouse_sign_signpage";
				            	    					imgdatefldid = "txtFldSpsSignDateSignpage";
				            	    					popup_imgid = "sps_img_12";
				            	    				}
				            	    				
				            	    				if(page == "DECLAREPAGE"){
				            	    					imageid = "page12_spousesign";
				            	    					imgmsgid = "msg_spouse_sign_declarepage";
				            	    					imgdatefldid = "v";
				            	    					popup_imgid = "sps_img_13";
				            	    				}
				            	    				 
				            	    				afterSuccessSignature(imageid, imgmsgid, imgdatefldid,page,"hTxtFldSpsSignStatus",popup_imgid);
				            	    				
				            	    				
					            	 				$('#spouseSignSavedDiagram').val('{"position": "0 0","model": { "class": "GraphLinksModel","nodeDataArray": [ {"stroke":"blue","strokeWidth":3,"category":"FreehandDrawing","key":-1, "loc":"0 0", "geo":""}],"linkDataArray": []}}')
					            	    			 
		            	    				 }

		            	    				 
		            	    			  });
		            	    			  
		            	    			  
		            	    			  loadMsg.style.display = "none";
		            	    			  dialog.dialog( "close" );
		            	    			  
		            	    			 

		            	    	  	  }
		            	    		  });
		            			
		            		}
		            		   
		            	
		                    
		                 }
		             });    
				
				
			
			
		});
		
		
		
		  

	}
//	----------------- GOJS RELATED
	
	function getRecomProdTblToPopup(){
		
		var fnaartuplanTbl = document.getElementById("fnaartuplanTbl");
		var fnaartuplantbodyobj = fnaartuplanTbl.tBodies[0];
		var rowlen = fnaartuplantbodyobj.rows.length;
		
		var prevClientName = "" , prevPrin = "";
		
		for(var r = 0;r<rowlen;r++){
			
			var clientName = fnaartuplantbodyobj.rows[r].cells[1].childNodes[0].value;
			var prin = fnaartuplantbodyobj.rows[r].cells[3].childNodes[0].value;
			
			clientName = isEmpty(clientName) ?"":clientName.toUpperCase();
			prin = isEmpty(prin) ?"":prin.toUpperCase();
//			alert(clientName + "," +prin)
			
			if(prevClientName != clientName){
				
				addNewProdRecomClient(clientName);
				
				console.log(clientName+","+prevPrin+","+prin)
				if(prevPrin != prin){
					
					var pdiv = $(".tab_in").find("input[name=tabgroup_rp][value='"+clientName+"']").closest("div");
					
					var obj = $(pdiv).find("input.prinbtn");
					addNewProdRecomPrin(obj)
				}
				
				
				
				
			}else{
				
				
				console.log(clientName+","+prevPrin+","+prin)
				if(prevPrin != prin){
					
					var pdiv = $(".tab_in").find("input[name=tabgroup_rp][value='"+clientName+"']").closest("div");
					
					var obj = $(pdiv).find("input.prinbtn");
//					addNewProdRecomPrin(obj)
				}
				
				

				
			}
			
			
			prevClientName= clientName;
			prevPrin = prin;
		}

		
	}	
	
	function showProdRecomSingleRow(){
		
//		getRecomProdTblToPopup();
		
		 $('#produtRecomSingleRow').dialog({
		        modal: true,
		        width: 900, height: 550, resizable: true,
		        title: "Product Recommendation",
		        open: function() {  },
		        buttons: {
		          Ok: function() {
//		            $( this ).dialog( "close" );
		        	  prodrecompopuptotable($( this ));
		          },
		          Close : function(){
		        	  $( this ).dialog( "close" );
		          }
		        }
		      })
		
	}
	
	
	function addNewProdRecomClient(tabName){
		
		
		var page2clientName = $("#dfSelfName").val();
		
		
		
		 var person = prompt("Client(2) Name", page2clientName);
		 
		 if(!isEmpty(person)){
			 
			 var totaltabs = $("input[name=tabgroup_rp]").length;
//				totaltabs+=1;
				
				 var newDiv = $("#tab-conts-default").html();
				 
				 newDiv = $(newDiv);
				 
				 
				 newDiv.find("input[name=tabgroup_rp]").prop("id","tab-recprod-client_"+totaltabs).val(isEmpty(tabName)?""+person:tabName);
				 newDiv.find("label").prop("for","tab-recprod-client_"+totaltabs).text(isEmpty(tabName)?""+person:tabName);
				 newDiv.find("input[name=clientName_sr]").prop("id","clientName_sr_"+totaltabs).val(isEmpty(tabName)?""+person:tabName);
				 
				 newDiv.find("table.maintable").prop("id","prinProdTbl_"+totaltabs);
				 
				 newDiv.find("h5").text(isEmpty(tabName)?person:tabName)
				 		 	 
			     $('#prodrecm-all-tabs').append($(newDiv));		 

//				 var total_table =  $('#prodrecm-all-tabs').find("#tab-recprod-client").find("table.maintable").length;
//				 alert(total_table + '<')
//				 if(total_table > 1){
//					 newDiv.find(".tab__content > table.maintable").find("table:gt(0)").remove();
					 
//					 $('#prodrecm-all-tabs').find("#tab-recprod-client").not('table.maintable:first').remove();
					 
//				 }
				 
			 
		 }
		

		 
		
	}
	
	function addNewProdRecomPrin(obj){
		
		var tab_content = $(obj).closest("div.dynatable");
		
		var total_table = tab_content.find("table.maintable").length;
		
		var clonedtable = $("#prinProdTbl_def").clone(true);
		
		clonedtable.prop("id","prinProdTbl_"+total_table);
		
		$(tab_content).append($(clonedtable));
		
	}
	
	function addNewProdRecomProds(obj){
		
		var tab_content = $(obj).closest(".tab__content");
		
		var total_table = tab_content.find("table.maintable").length;
		
		var clientname = $(obj).closest("table").find("input[name=clientName_sr]").val();
		
		
		
		var prinname = $(obj).closest("table").find("select[name=selMastPrincipal_sr]").val();
		
		
		
		var parenttable = $("#prinProdTbl_def");
		
		var tabletoappend = $(obj).closest("table.maintable")
		
		var clonedrows = [];
		
		$(parenttable).find("tr.prods_def").each(function(){
			clonedrows[clonedrows.length++] = $(this).clone(true);
		});
		
//		alert(clonedrows.length+"<-");
		
		$(clonedrows).each(function(){
			$(tabletoappend).append($(this));
		})		
		
		$(obj).hide();
	}
	

	function prodrecompopuptotable(dialog){
		
		
		var totalclient = 0 ;
		
		var totalprin = 0;
		
		var totalprod = 0;
		
		totalclient = $('#prodrecm-all-tabs').find("input[name=clientName_sr]").length;
		
		totalprin = $('#prodrecm-all-tabs').find("select[name=selMastPrincipal_sr]").length;
		
		totalprod = $('#prodrecm-all-tabs').find("input[name=txtFldProdName_sr]").length;
		
		console.log(totalclient+","+totalprin+","+totalprod)
		
		$("#hTxtFldProdCount").val(totalprod)
		
		var clientname = {};
		var prinname = {};
	
		var jsonObj = []
		var mergedObj  = {}
		
		var prevprin = "";
		
//		$('#prodrecm-all-tabs').find("input[name=clientName_sr]").each(function(){
//			var name = $(this).prop("name");
//			var value = $(this).val();
//			clientitem = {}
//			clientitem [name] = value;
			
//			$('#prodrecm-all-tabs').find("select[name=selMastPrincipal_sr]").each(function(){
				
				
//				var pkey  =  $(this).prop("name");
//				var pval =  $(this).val();
//				
//				console.log(pkey + "---->"+pval)
//				prinitem = {}
//				prinitem [pkey] = pval;
				 
				 $('#prodrecm-all-tabs').find("table.maintable").find(":input.iterate")
				 .not(':input[type=button], :input[type=submit], :input[type=reset]')
				 .each(function(){
					 
					 
					
					 
					 var prkey  =  $(this).prop("name");
					 var prval =  $(this).val();
					 
					 detsitem = {}
					 detsitem [prkey] = prval;
					 console.log(detsitem);
					 jsonObj.push(detsitem);
					 
//					 mergedObj = $.extend(prinitem,detsitem);
					 
//					 console.log( JSON.stringify( mergedObj ) );
					 
//					 if(prevprin != pval){
//						 
//						 mergedObj = $.extend(clientitem,prinitem,detsitem);
//						 jsonObj.push(JSON.stringify( mergedObj ));
//						 
//						 
//						 prevprin = pval;
//						 
//					 }
					 
					
					
					 
						
				 })//prod
				
				
//				 jsonObj.push(mergedObj);
				 
				 
//				 jsonObj.push(prinitem);
//			})//prin
		
			
//		})//client
		
		
//		$('#prodrecm-all-tabs').find(":input")
//			.not(':input[type=button], :input[type=submit], :input[type=reset]')
//				.each(function(){
//			
//			  	var name = $(this).prop("name");
//	        	var value = $(this).val();
//	        	
//	        	
//	        	
//	        	if(!isEmpty(name)){	        		
//		        	item = {}
//		            item [name] = value;
//		        	
//		        	if(jsonObj.length <= 10){
//		        		jsonObj.push(item);	
//		        	}
//		        	
//		        	        		
//	        	}
//	        	
//		});
	
		console.log(jsonObj);
		
//		console.log(sortJSON(jsonObj,'selMastPrincipal_sr','asc'));		
//		console.log(sortJSON(jsonObj,'txtFldProdName_sr','asc'));
		
		var prevClientName = "",prevprinName = "" ,prevProdName = "";
		var data_arr = [];
		
		
//		for (var data = 0 ; data <jsonObj.length ; data++ ){
//			
//			 
//			var prodname = jsonObj[data].txtFldProdName_sr;
//			var prinname = jsonObj[data].selMastPrincipal_sr;
//			var clientName = jsonObj[data].clientName_sr;
//			var prodtype = jsonObj[data].selProdType_sr
//			var sa = jsonObj[data].txtFldSA_sr
//			var paymeth = jsonObj[data].selPaymeth_sr			
//			var planterm = jsonObj[data].txtFldPlanTerm_sr
//			var premterm = jsonObj[data].txtFldPremTerm_sr
//			var premamt = jsonObj[data].txtFldPremAmt_sr;
//			
//			if(!isEmpty(clientName) && clientName != prevClientName){
//				
//				if(!isEmpty(prinname) && prinname != prevprinName){
//					data_arr[data_arr.length++] = new Array(clientName,prinname,prodname,prodtype,sa,paymeth,planterm,premterm,premamt)
//					prevprinName = prinname;
//				}else{
//					data_arr[data_arr.length++] = new Array(clientName,prevprinName,prodname,prodtype,sa,paymeth,planterm,premterm,premamt)
//				}
//				
//				
//			}
//			prevClientName=clientName
//		}
//		
//		console.log("---------->"+data_arr)
		var page2clientName = $("#dfSelfName").val();
		
		for (var i = 0; i < totalprod; i++) {

			fnaArtuPlanAddRow("fnaartuplanTbl", "", "", "");
			
			
			var fnaartuplanTbl = document.getElementById("fnaartuplanTbl");
			var fnaartuplantbodyobj = fnaartuplanTbl.tBodies[0];
			var rowlen = fnaartuplantbodyobj.rows.length;
			
			
			
			fnaartuplantbodyobj.rows[rowlen - 1].cells[0].childNodes[0].value = (i+1);	
			
			fnaartuplantbodyobj.rows[rowlen - 1].cells[3].childNodes[0].selectedIndex = (i+1)
			fnaartuplantbodyobj.rows[rowlen - 1].cells[3].childNodes[5].value = "PLAN"+(i+1);
			
			fnaartuplantbodyobj.rows[rowlen - 1].cells[1].childNodes[0].value = page2clientName;
			
			/*	for (var data = 0 ; data <jsonObj.length ; data++ ){
				
			 
				var prodname = jsonObj[data].txtFldProdName_sr;
				var prinname = jsonObj[data].selMastPrincipal_sr;
				var clientName = jsonObj[data].clientName_sr
				
				if(prevClientName != clientName){
					
					console.log(jsonObj[data].clientName_sr)
					if(!isEmpty(clientName)){					
						fnaartuplantbodyobj.rows[rowlen - 1].cells[1].childNodes[0].value = clientName;
						prevClientName = clientName;
					}
					
					if(!isEmpty(jsonObj[data].selMastPrincipal_sr)){
//						fnaartuplantbodyobj.rows[rowlen - 1].cells[3].childNodes[0].value = prinname;
						
						prevprinName = prinname;
					}
					
					if(!isEmpty(prodname)){
//						fnaartuplantbodyobj.rows[rowlen - 1].cells[3].childNodes[5].value = prodname;
						
						prevProdName = prodname;
					}
					
					break;
				}
				
//				
			}*/
        }
		
		
		dialog.dialog("close");
	}
	
	
	
	function sendMailtoManger(){
		
//		loaderBlock();
		
		var fnaid = $("#txtFldFnaId").val();
		var ntuccaseid  = $("#txtFldCustId").val();
		var ntucexist = chkAnyNTUCExist();
		var flag = false;
		
		if(window.confirm("Do you want to upload any policy documents now?")){
			openPolicyDocDlg(fnaid, ntuccaseid, ntucexist);					
			
		}else{
			
			setTimeout(function() { loaderBlock() ;}, 0);	
   		  	setTimeout(function() {  triggerMgrEmail(fnaid, ntuccaseid, ntucexist) ;}, 1000);	
//			triggerMgrEmail(fnaid, ntuccaseid, ntucexist);
			
		}
		
		
		
		
	}
	
	function openPolicyDocDlg(fnaid, ntuccaseid, ntucexist){
		
		$("#txtFldFNAID").val($("#txtFldFnaId").val());
		
		fixedHeader('kycapprovaluploadTbl','1','100%','0px', '100%', '27em', '2', '0px', '98.2%', '3em', '98.2%',false,false,0,0);
		
		var dialog = $( "#adviserupload" ).dialog({
			cache: false,
		      autoOpen: false,
		      title : "Upload Policy Document",
		      height: 600,
		      width: 950,
		    
		      modal : true,
		    
		    	
		      buttons: { " Upload Documents ": function(){
		    	  
		    	  var flag = false;
		    	  
		    	  var tblRowLength=$("#kycapprovaluploadTbl tbody tr").length;

		  			if(tblRowLength == 0){
		  				alert("Upload any policy document!");
		  				return false;
		  			}
		    	  
		    	  if(!validateAdviserUpload())return;
		    	  
		    	  setTimeout(function() { loaderBlock() ;}, 0);	
		    	  
		    	  setTimeout(function() { 
		    		  flag = uploadAdvApprovalDocs(dialog); 
			    	  
			    	  if(flag){
			    		  setTimeout(function() { loaderBlock() ;}, 0);	
			    		  setTimeout(function() {  triggerMgrEmail(fnaid, ntuccaseid, ntucexist) ;}, 1000);	
//			    		  triggerMgrEmail(fnaid, ntuccaseid, ntucexist);
			    	  }
			    	  
		    	  }, 1000);	
		    	  
		    	 
		    	  
		      }},
		      close: function(){
		    	  dialog.dialog("close");
		      }

		    });
		
		dialog.dialog("open");
		

	}
	
	function triggerMgrEmail(fnaid,ntuccaseid,ntucexist){
		
		
		var parameter = "dbcall=NONNTUCMGRMAIL&strFNAId=" + fnaid + "&ntuccaseid="+ ntuccaseid + "&ntucexist="+ntucexist;

		var retval = ajaxCall(parameter);

		for ( var val in retval) {

			var tabdets = retval[val];

			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}

			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var key = tab;
					var value = tabdets[tab];

					if(key == "SEND_MAIL_FAILURE" || key =="SEND_MAIL_SUCCESS"){

						alert(value);
						var loadMsg = document.getElementById("loadingMessage");
						loadMsg.style.display = "none";
					}
					
					if(key == "MGR_EMAIL_SENT_FLG"){
						$("#hTxtFldSentToMgr").val(value);
						$("#hTxtFldMgrAppSts").val("");
						var ntucexist = chkAnyNTUCExist();
						var sentsts = $("#hTxtFldSentToMgr").val();
						if(sentsts == "Not Sent" && !ntucexist){
							$("#btnMgrApprResend").prop("disabled",false);
							$("#btnMgrApprResend").prop("value","Send Now!");
						}else{
							$("#btnMgrApprResend").prop("disabled",true);	
						}
					}
				}
			}
		}
		
	}

	function chkAnyNTUCExist(){
		
		var ntucexist = false;
		$("#fnaartuplanTbl tbody tr").each(function(){
			var roww=$(this);
			
			var prin = roww.find(":input[name=selFnaArtuPlanCompName]").val();
			if(prin == "NTUC Income"){
				return ntucexist = true;
			}
			
		})
		
		return ntucexist;
	}
	
	function chkAnyNonNTUCExist(){
		
		var nonntucexist = false;
		
		$("#fnaartuplanTbl tbody tr").each(function(){
			var roww=$(this);
			
			var prin = roww.find(":input[name=selFnaArtuPlanCompName]").val();
			if(prin != "NTUC Income"){
				return nonntucexist = true;
			}
			
		})
		
		return nonntucexist;
	}
	
	function resendToManager(){
		
		var advisersignstats = $("#hTxtFldAdvSignStatus").val();
		var mgrmailsent = $("#mgrEmailSentFlg").val();
		var custsignstats = $("#hTxtFldCustSignStatus").val();
		var ntuccaseid = $("#ntucCaseId").val();
		var ntucexist = chkAnyNTUCExist();
		var nonntucexist = chkAnyNonNTUCExist();
		
//		if(!nonntucexist){
//			alert("Since there is no product recommended apart from NTUC principal/company\n.Please recommend atlease one principal/company other than NTUC Income!")
//			return false;
//		}

		
		var clientsigned=sofarClientSignatureDone(),
		totalclientsign=4;
		
		var advisersigned = chkAdviserSignatureDone();
		
		
		if(clientsigned != totalclientsign){
			alert(CLIENT_SIGN_B4_NONDSF );
			setTimeout(function(){openPopupBox();},700);
			return false;
		}
		
		if(!advisersigned){
			alert(CLIENT_SIGN_B4_NONDSF );
			setTimeout(function(){openPopupBox();},700);
			return false;
		}
		
		
//		if(isEmpty(ntuccaseid) && !ntucexist && mgrmailsent != "Y"){
		if( mgrmailsent != "Y"){
			if(clientsigned == totalclientsign){
				 if( window.confirm("Do you want to send this FNA Form to manager review?")){
					  sendMailtoManger();
//					  $("#btnMgrApprResend").hide();
				  }
			}	
		}else if (mgrmailsent == "Y"){
			
			if( window.confirm("Do you want to re-send this FNA Form to manager review?")){
				  sendMailtoManger();
//				  $("#btnMgrApprResend").hide();
			  }
			
		}
		
		
		
//		if( window.confirm("Do you want to send this FNA template to manager review?")){
//			  sendMailtoManger();
//			  $("#btnMgrApprResend").hide();
//		  }
	}
	
	
	function validateAdviserUpload(){
		var tblRowLength=$("#kycapprovaluploadTbl tbody tr").length;
		var status=true;

		if(tblRowLength>0){
			$("#kycapprovaluploadTbl tbody tr").each(function(){
				if($(this).find("td:eq(0) input:eq(1)").val()!=QRY_MODE){
				if(isEmpty($(this).find("td:eq(1) input:first").val())){
					alert("Select the Check List");
					$(this).find("td:eq(1) select:first").focus();
					status=false;
					return false;
				}
				
				
				if(isEmpty($(this).find("td:eq(2) input:first").val())){
					alert("Select the Document Title");
					$(this).find("td:eq(2) select:first").focus();
					status=false;
					return false;
				}

				if($(this).find("td:eq(3) input:eq(0)")[0].files.length==0){
					alert("Select a file.");
					$(this).find("td:eq(3) input:first").focus();
					status=false;
					return false;
				}
				}
			});
		}

		return status;
	}
	
	function fnaAdviesrDocAdd(tbl,mode){

		if(!validateAdviserUpload())return;
		
		var list_of_chklist = [];
		list_of_chklist =  copyTextArrFromOpt(document.getElementById("selAttachMastAttachCateg"));						 
		 

		var strLoggedUser=COMMON_SESS_VALS[0].STR_LOGGEDUSER;
		var strCurrDate=COMMON_SESS_VALS[0].TODAY_DATE;
		

		var tblobj = document.getElementById(tbl);
		var tbodyobj = tblobj.tBodies[0];

		var rowlenobj = tbodyobj.rows.length;
		var jqRwLen=$("#"+tbl+" tbody tr:visible").length;
		var rowobj = tbodyobj.insertRow(rowlenobj);

		var cell0 = rowobj.insertCell(0);
		cell0.innerHTML =  '<input type="text" readonly="readonly" name="txtFldKycApprAdvDocSino" value="'+(Number(jqRwLen)+1)+'" style="width:100%;text-align:center;background:transparent;border:0"/>'+
						   '<input type="hidden" name="txtFldKycApprAdvDocmode" value="'+ mode+ '"/>'
//						   +'<input type="checkbox" />';
		cell0.style.textAlign = "center";

		var cell1 = rowobj.insertCell(1);
		cell1.style.textAlign = "left";
//		cell1.innerHTML ='<select class="kycSelect"  name="selKycApprAdvChkList" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  style="width:98%;"></select>'
		cell1.innerHTML ='<input type="text"  name="selKycApprAdvChkList" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  style="width:98%;"/>'+
		'<select class="kycSelect"  name="selKycApprAdvChkListSel"  style="width:98%;display:none"></select>'

		comboCloneOption($("#selAttachMastAttachCateg"),cell1.childNodes[1]);
//		cell1.childNodes[0].onchange = function(){
//			loadAttachDocTitle(cell1.childNodes[1],cell2.childNodes[2]);
//		}
		
		var prinobj_ = $(rowobj).find("td:eq(1)").find("input:eq(0)");						 
		 prinobj_.autocomplete({
			 source:list_of_chklist
		 });

		var cell2 = rowobj.insertCell(2);
		cell2.style.textAlign = "left";
//		cell2.innerHTML ='<select class="kycSelect"  name="selKycApprAdvDocTitle" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  style="width:98%;"><option value="">--SELECT--</option></select>'+
//		 '<input type="hidden" name="txtFldPopUpDocText" id="txtFldPopUpDocText" value=""/>';
		cell2.innerHTML ='<input type="text" class="kycSelect"  name="selKycApprAdvDocTitle" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  style="width:98%;"/>'+
		 '<input type="hidden" name="txtFldPopUpDocText" id="txtFldPopUpDocText" value=""/>'+'<select class="kycSelect"  name="selKycApprAdvDocTitleSel" onclick="hideTooltip()" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();"  style="width:98%;display:none"><option value="">--SELECT--</option></select>';

		cell1.childNodes[1].selectedIndex = 1;
		loadAttachDocTitle(cell1.childNodes[1],cell2.childNodes[2]);
		var list_of_doctitle = [];
		list_of_doctitle =  copyTextArrFromOpt(cell2.childNodes[2]);						 
		
		var doctitle_ = $(rowobj).find("td:eq(2)").find("input:eq(0)");						 
		doctitle_.autocomplete({
			 source:list_of_doctitle
		 });
		
//		cell2.childNodes[0].onchange = function(){
//			
//			if(!isEmpty(cell2.childNodes[0].value)){
////				alert(cell2.childNodes[0].options[cell2.childNodes[0].selectedIndex].text)
//				cell2.childNodes[1].value= cell2.childNodes[0].options[cell2.childNodes[0].selectedIndex].text;	
//			}
//			
//		}
			
		var cell3 = rowobj.insertCell(3);
		cell3.style.textAlign = "left";
		cell3.innerHTML = '<input type = "file"  name="fileKycApprAdvDoc" accept=".jpg,.pdf,.tif,.tiff,.png,.bmp,.jpeg,.gif"  class="cssValidateNRICFile" style="width:100%"/>';

		
		
		var cell5 = rowobj.insertCell(4);
		cell5.style.textAlign = "left";
		cell5.innerHTML = '<input type = "text" readOnly="true"  name="txtFldKycApprAdvDocDate" class="fplblText" style="background-color: rgb(233, 244, 235);width:95%;background:transparent;border:0"  value="'+strCurrDate+'" style="width:98%"/>';
		cell5.style.display="none";
		
		var cell6 = rowobj.insertCell(5);
		cell6.style.textAlign = "left";
		cell6.innerHTML = '<input type = "text" class="fpEditTblTxt"  name="txtFldKycApprAdvDocPgNo" maxlength="10"  style="width:60%"/>';
		cell6.style.display="none";
		
		var cell7 = rowobj.insertCell(6);
		cell7.style.textAlign = "left";
		cell7.innerHTML = '<input type = "text" class="fpEditTblTxt"  name="txtFldKycApprAdvDocRem"  maxlength="300" onclick="hideTooltip()"  onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="width:98%"/>';
		cell7.style.display="none";
		

		rowobj.onclick = function() {
			selectSingleRow(this);
		};
//		$(rowobj).on("click",function(event){
//			selectSingleRow(this);
//		});

		if(mode==INS_MODE){
			$(rowobj).addClass("DbInsertRow")
		}

	}
	
	
	

	function loadAttachDocTitle(categoryFld,docTitleFld){


		for (var del = docTitleFld.length - 1; del>0; del--) {
			docTitleFld.remove(del);
		}

		docTitleFld.options[0]=new Option('--SELECT--','');
		var optCnt = docTitleFld.length;
		var txtFldAttachCategName = categoryFld.options[categoryFld.selectedIndex].text;
		var selectedval = "";

		for(var count=0;count<mastAttachCategArr.length;count++){

			if(mastAttachCategArr[count][1] == txtFldAttachCategName &&
			  (mastAttachCategArr[count][3] == "CLIENT" || mastAttachCategArr[count][3] == "GENERAL")){

				var docTitle = mastAttachCategArr[count][2];
				var attachmentId = mastAttachCategArr[count][0];
				docTitleFld.options[optCnt]=new Option(docTitle,attachmentId);
//				if(docTitle ==APP_ATTACH_CATEGORY_SERVICING ){
//					selectedval = attachmentId;
//				}
				optCnt++;

			}//End of mastAttachCategArr[count][1]==txtFldAttachCategName
		}//End for count;

		
	}
	
	function uploadAdvApprovalDocs(dialog){
		
		 var parameter = "dbcall=ADVISERAPPRODOCS&"+$("#frmKycApproval").find("input:text,input:hidden,select").serialize();

		$("#kycapprovaluploadTbl tbody tr").each(function(){
			var mode=$(this).find("td:eq(0) input:eq(1)").val();
			if(mode==QRY_MODE){
				$(this).find("[type=file]").remove();
			}

		});

		 var form = $('#frmKycApproval')[0];
	     var data = new FormData(form);
	     var ret="";

		    $.ajax({
		        url : baseUrl+"/KycUtilsServlet?"+parameter,
		        type: "POST",
		        data :data,
		        cache: false,
		        enctype: 'multipart/form-data',
		        contentType: false,
		        processData: false,
		        // async:false

		    }).done(function(response){
		    	var saveResp=$.parseJSON(response);

		    	 if(saveResp[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
					  window.location = baseUrl + SESSION_EXP_JSP;
					  return;
				 }

		    	 if(saveResp[0].STATUS =='SUCCESS'){

					  alert("Policy document(s) are uploaded successfully.");
					  
					  var loadMsg = document.getElementById("loadingMessage");
					  loadMsg.style.display = "none";
					  dialog.dialog("close");
					  ret=true;
					 

				  }

		    	 if(saveResp[0].STATUS=='FAIL'){
					  alert("Policy document(s) uploaded failed.");
					  var loadMsg = document.getElementById("loadingMessage");
					  loadMsg.style.display = "none";
					  dialog.dialog("close");
					  ret= false;
				 }

//		    	ret=true;
		    });
	
	return ret;
	}
	
	
	function delAdvApprDocRow(){
		
		var table = document.getElementById("kycapprovaluploadTbl");	  
		var rowCount = table.rows.length;
		
		table.deleteRow(document.getElementById("hSelectRow").value);
		
		incrIndex("kycapprovaluploadTbl", true)
		
//		deleteRowInTbl("kycapprovaluploadTbl", true);
	}
	
	function uploadPolicyDocs(){
		

		var fnaid = $("#txtFldFnaId").val();
		var ntuccaseid  = $("#txtFldCustId").val();
		var ntucexist = chkAnyNTUCExist();
	
		openPolicyDocDlg(fnaid, ntuccaseid, ntucexist);			
	}
	
	/*function chkForAnyChange(){
		
		
		var advisersignstats = $("#hTxtFldAdvSignStatus").val();
		var custsignstats = $("#hTxtFldCustSignStatus").val();
		
		var clientsigned=sofarClientSignatureDone();
		
//		if(custsignstats == 'Y'){
		if( clientsigned > 0 ){
			
			var checkChange=$("#chkanychange").val();

			if(!isEmpty(checkChange) && !$.isEmptyObject($.parseJSON(checkChange)) && NTUCChkChngSts){
				
				
				
				var flg = chkAllDataSaved("",false," You have modified the current FNA Form.<br/>This FNA Form is already signed by client.<br/>Please sign the FNA Form again is required.");
				
				
				if(!flg){
					return;
				}
				
				setTimeout(function(){
					
					if(!flg) {
					
						$('#reportvaliationPopup').dialog('option', 'title', 'eKYC - Signature Notification');
						$( "#reportvaliationPopup" ).dialog( "option", "height",280);
						$( "#reportvaliationPopup" ).dialog( "option", "width",500);
	//					$("#reportvaliationPopup table:first tbody tr:first h3:first").html(" You have changed some data in current FNA Form.<br/>This FNA Form is already signed by client.<br/>Need to get the signature from the client .");
						$("#reportvaliationPopup table:first tbody>tr").not(":first").hide();
	
	
						var buttons = $( ".reportvaliationPopup" ).dialog( "option", "buttons" );
	
						$('#reportvaliationPopup .ui-dialog-buttonset button:first').remove();
	
						$( "#reportvaliationPopup" ).dialog( "option", "buttons",
								[
								    {
								      text: "OK",
								      click: function() {
	//							    	  if(confirm("Do you want to save the changes now?")){
												$('#reportvaliationPopup').dialog("close");
	//											$("#saveBtn-button").click();
												saveOrUpdate();
	//											return true;
	//									  }
								      }
								    }
								 ]
						);
					
					}else{
						return true;
					}
				
					hidePagingDiv();
					
				}, 1000);

				

			}else{
				return true;
			}
		}else{
			return true;
		}

	
		return true;
	
		
	}*/
	
	function reloadSignatureParam(imageid,signfor,page){
		var imageid_loc = document.getElementById(imageid);
		imageid_loc.setAttribute("src", 'images/esign-icon.png');
		imageid_loc.setAttribute("class", "esignclientsign esign-icon");
		imageid_loc.setAttribute("onclick", "openSignaturePad('"+signfor+"','"+page+"')");
		console.log(imageid +" is triggered")
	}
	
	function afterSuccessSignature(imageid,imgmsgid,imgdatefldid,page,signstatusfld,popup_imgid){
		 var imageid_loc = document.getElementById(imageid);
		 imageid_loc.setAttribute("src", 'images/lock.png');
		 imageid_loc.setAttribute("class", "esign-icon");
		 imageid_loc.setAttribute("onclick", "");
		 
		 $("#"+imgmsgid).show();
		 $("#"+imgdatefldid).val(currDate);
		 imageid_loc.style.display="block";
		 $("#"+popup_imgid).attr('src','images/ok.png');
			 
			 
			 if(signstatusfld){
				/* var signstatus = $("#"+signstatusfld);
					var jsonvalues = JSON.parse(isEmpty(signstatus.val()) ? "{}" : signstatus.val());

					var newobj=jsonvalues;
					newobj[page]=("Y");
					signstatus.val(JSON.stringify(newobj));*/
				 
				 setSignatureStatus(signstatusfld,page);

			 }
			 
			 setTimeout(function(){openPopupBox();},700);
	}
	
	function setSignatureStatus(signstatusfld,page){
		 var signstatus = $("#"+signstatusfld);
			var jsonvalues = JSON.parse(isEmpty(signstatus.val()) ? "{}" : signstatus.val());

			var newobj=jsonvalues;
			newobj[page]=("Y");
			signstatus.val(JSON.stringify(newobj));
	}
	
	
	function loadSignatureFromJSON(v,imageid,imgmsgid,imgdatefldid){
		
		var SIGN_DATA = "",SIGN_DATE ="";
		
		
		if(v.hasOwnProperty("ADVISER_SIGNED") && v.ADVISER_SIGNED == 'Y'){
			SIGN_DATA = v.ADVISER_SIGN
			SIGN_DATE = v.ADVISER_SIGN_DATE
		}
		
		if(v.hasOwnProperty("CLIENT_SIGNED") && v.CLIENT_SIGNED == 'Y'){
			SIGN_DATA = v.CLIENT_SIGN
			SIGN_DATE = v.CLIENT_SIGN_DATE
		}
		
		if(v.hasOwnProperty("MANAGER_SIGNED") && v.MANAGER_SIGNED == 'Y'){
			SIGN_DATA = v.MANAGER_SIGN
			SIGN_DATE = v.MANAGER_SIGN_DATE
		}
		
		if(v.hasOwnProperty("SPOUSE_SIGNED") && v.SPOUSE_SIGNED == 'Y'){
			SIGN_DATA = v.SPOUSE_SIGN
			SIGN_DATE = v.SPOUSE_SIGN_DATE
		}
	
		var imageid_loc = document.getElementById(imageid);
		imageid_loc.setAttribute("src", 'data:image/png;base64,'+SIGN_DATA);
		imageid_loc.setAttribute("class", "esignclientsigndone esign-icon");
		imageid_loc.setAttribute("onclick", "");
		imageid_loc.style.display="block";
		$("#"+imgmsgid).hide();
		
		$("#"+imageid).bind('contextmenu', function(e) {
		    return false;
		}); 
		
		$("#"+imgdatefldid).val(SIGN_DATE);
		
		imageid_loc.setAttribute("disabled", true);
		
	}
	
	function chkClientSignatureDone(){
		
		/*var mgrmailsent = $("#mgrEmailSentFlg").val();
		var custsignstats = $("#hTxtFldCustSignStatus").val();
		var ntuccaseid = $("#ntucCaseId").val();
		var ntucexist = chkAnyNTUCExist();*/
		
//		alert(custsignstats)
		var totalclientsign=4,
		clientsigned=sofarClientSignatureDone(); 
		
		
		
		
			console.log("client signaturer compare ->"+totalclientsign+","+clientsigned)
			if(totalclientsign == clientsigned){
				
				return true;
			}else{
				return false;
			}	
		
		
}
	
function chkClient2SignatureDone(){
	 
		var totalclientsign=4,
		clientsigned=sofarClient2SignatureDone(); 
		
			console.log("client(2) signaturer compare ->"+totalclientsign+","+clientsigned)
			if(totalclientsign == clientsigned){
				
				return true;
			}else{
				return false;
			}	
		
		
}
	
function sofarClientSignatureDone(){
		
		var mgrmailsent = $("#mgrEmailSentFlg").val();
		var custsignstats = $("#hTxtFldCustSignStatus").val();
		var ntuccaseid = $("#ntucCaseId").val();
		var ntucexist = chkAnyNTUCExist();
//		alert(custsignstats)
		var totalclientsign=4,clientsigned=0; 
		
//		if(isEmpty(ntuccaseid) && !ntucexist && mgrmailsent != "Y"){
			
			var val_= JSON.parse(isEmpty(custsignstats) ? "{}" : custsignstats);
			if (!isEmpty(custsignstats) && !jQuery.isEmptyObject(val_)) {
				
				$.each(val_,function(k,v){
					if(k=='INDEXPAGE' && v=='Y'){
						clientsigned++;
					}
					
					if(k=='TAXPAGE' && v=='Y'){
						clientsigned++;
					}
					
					if(k=='SIGNPAGE' && v=='Y'){
						clientsigned++;
					}
					
					if(k=='DECLAREPAGE' && v=='Y'){
						clientsigned++;
					}
				});
				
			}
//		}
			/*console.log("client signaturer compare ->"+totalclientsign+","+clientsigned)
			if(totalclientsign == clientsigned){
				
				return true;
			}else{
				return false;
			}	*/
		
		return clientsigned;
		
}


function sofarClient2SignatureDone(){
		
		var mgrmailsent = $("#mgrEmailSentFlg").val();
		var custsignstats = $("#hTxtFldSpsSignStatus").val();
		var ntuccaseid = $("#ntucCaseId").val();
		var ntucexist = chkAnyNTUCExist();
//		alert(custsignstats)
		var totalclientsign=4,client2signed=0; 
		
//		if(isEmpty(ntuccaseid) && !ntucexist && mgrmailsent != "Y"){
			
			var val_= JSON.parse(isEmpty(custsignstats) ? "{}" : custsignstats);
			if (!isEmpty(custsignstats) && !jQuery.isEmptyObject(val_)) {
				
				$.each(val_,function(k,v){
					if(k=='INDEXPAGE' && v=='Y'){
						client2signed++;
					}
					
					if(k=='TAXPAGE' && v=='Y'){
						client2signed++;
					}
					
					if(k=='SIGNPAGE' && v=='Y'){
						client2signed++;
					}
					
					if(k=='DECLAREPAGE' && v=='Y'){
						client2signed++;
					}
				});
				
			}

		
		return client2signed;
		
}
	
	
	function chkSpouseSignatureDone(){
		
		var mgrmailsent = $("#mgrEmailSentFlg").val();
		var custsignstats = $("#hTxtFldCustSignStatus").val();
		var ntuccaseid = $("#ntucCaseId").val();
		var ntucexist = chkAnyNTUCExist();
//		alert(custsignstats)
		var totalclientsign=4,clientsigned=0; 
		
		if(isEmpty(ntuccaseid) && !ntucexist && mgrmailsent != "Y"){
			
			var val_=JSON.parse(custsignstats);
			if (!isEmpty(custsignstats) && !jQuery.isEmptyObject(val_)) {
				
				$.each(val_,function(k,v){
					if(k=='INDEXPAGE' && v=='Y'){
						clientsigned++;
					}
					
					if(k=='TAXPAGE' && v=='Y'){
						clientsigned++;
					}
					
					if(k=='SIGNPAGE' && v=='Y'){
						clientsigned++;
					}
					
					if(k=='DECLAREPAGE' && v=='Y'){
						clientsigned++;
					}
				});
				
			}
		}
			console.log("client(2) signaturer compare ->"+totalclientsign+","+clientsigned)
			if(totalclientsign == clientsigned){
				
				return true;
			}else{
				return false;
			}	
		
		
}
	
function chkAdviserSignatureDone(){
	
		var isClientSignDone = chkClientSignatureDone();
		
		var isClient2SignDone  = true;
		 var page2spouseNameTmp = $("#dfSpsName").val();
		 if(!isEmpty(page2spouseNameTmp)){
			 isClient2SignDone = chkClient2SignatureDone();
		 }
		
		if(!isClientSignDone){
//			alert("Client signature is not completed!")
			return false;
		}
		
		if(!isClient2SignDone){
			return false;
		}
		
		var mgrmailsent = $("#mgrEmailSentFlg").val();
//		var custsignstats = $("#hTxtFldCustSignStatus").val();
		var advsignsts = $("#hTxtFldAdvSignStatus").val();
		var ntuccaseid = $("#ntucCaseId").val();
		var ntucexist = chkAnyNTUCExist();
		var totaladvsign=2,advisersigned=0; 
//		if(isEmpty(ntuccaseid) && !ntucexist && mgrmailsent != "Y"){//03122019
//		if( mgrmailsent != "Y"){
//			if(advsignsts == "N"){advsignsts = '{"N":"N"}'}
		if(!isEmpty(advsignsts)){
			
			var val_=JSON.parse(advsignsts);
			if (!isEmpty(advsignsts) && !jQuery.isEmptyObject(val_)) {
				
				$.each(val_,function(k,v){
					
				
					
					if(k=='SIGNPAGE' && v=='Y'){
						advisersigned++;
					}
					
					if(k=='DECLAREPAGE' && v=='Y'){
						advisersigned++;
					}
				});
				
			}
			
		}
			
//		}
			console.log("adviser signaturer compare ->"+totaladvsign+","+advisersigned)
			if(totaladvsign == advisersigned){
//				btnMgrApprResend
//				alert("inside")
				$("#btnMgrApprResend").prop("disabled",false)
				
				return true;
			}else{
				return false;
			}	
		
		
}
	

function changeSpouseNames(thisobj){
	
	var thisval = $(thisobj).val();
//	alert(thisval)
	if(!isEmpty(thisval)){
		
		$("input[name=txtFldSpouseName]").val(thisval);
		$("#dfSpsName").val(thisval);
		

		if(!isEmpty(thisval)){
			$("#divASpouseSign").show();
			$("#signsts_sps_div").removeClass("disabledivs");
			
			$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").on("click");
			$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").css({"cursor": "auto"});
			$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").closest("div").css({"cursor": "auto"});
			
			
			reloadSignatureParam("page1_spousesign", "spouse","INDEXPAGE");
			reloadSignatureParam("taxpage_spousesign", "spouse","TASXPAGE");
			reloadSignatureParam("signpage_spousesign", "spouse","SIGNPAGE");
			reloadSignatureParam("page12_spousesign", "spouse","DECLAREPAGE");
			
		}else{
			$("#divASpouseSign").hide();

			$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").off("click");
			$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").css({"cursor": "not-allowed","pointer-events": "none"});
			$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").closest("div").css({"cursor": "not-allowed"});
			$("#signsts_sps_div").addClass("disabledivs");
			
		}
		
		
	

	}else{
		
		$("#txtFldSpouseNamePage2").attr("readOnly",false).removeClass("readOnlyText");
		
		$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").off("click");
		$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").css({"cursor": "not-allowed","pointer-events": "none"});
		$("#page1_spousesign,#taxpage_spousesign,#signpage_spousesign,#page12_spousesign").closest("div").css({"cursor": "not-allowed"});
		

		
	}
	
	
	
}

function getClient2Details(){
	
	removeTblRows("c2dynatable");
	
	var client2 = $("#txtFldSpouseNamePage2").val();
	if(!isEmpty(client2)){
		$("#searchclient2").val(client2)
	}
	
	var dialog = $("#client2SearchDialog").dialog({
		autoOpen : false,
		height : '700',
		width : '900',
		modal : true,
		resizable : true,

		buttons : {
			" OK " : function() {
				setClient2InfoToAllSection(dialog);
				
			},
			Cancel : function() {
				dialog.dialog("close");
			}
		},
		close : function() {
		}
	});

	dialog.dialog("open");


	
}


function searchClient2Dets(){
//$("#client2SearchDialog").find("#searchclient2").change(function(){
		var thisval = $("#client2SearchDialog").find("#searchclient2").val();
		var servadvid = $("#txtFldClntServAdv").val();
		var parameter = "dbcall=GETSPOUSEDETAILS&txtFldSpsNric=&txtFldSpsName="+thisval+"&txtFldServAdvId="+servadvid;
		var respText = ajaxCall(parameter);
//		clearSpsfld();
		
		var tbl_ = document.getElementById("c2dynatable");
		var tbody_ = tbl_.tBodies[0];
	    removeTblRows("c2dynatable");
		
		$.each(respText,function(i,objArr){
			$.each(objArr,function(tab,tabdata){		
				$.each(tabdata,function(elm,val){
					
					var data = JSON.stringify(tabdata[elm]) ;
					console.log(data)
					
					var row_ = tbody_.insertRow(elm);
					var cell0_ = row_.insertCell(0);
					cell0_.innerHTML = '<input type="checkbox" class="c2clientdynachk"  onchange="getSelectedClient2Info(this)"/><input type="hidden" value=\''+data+'\'/>'+tabdata[elm].dfSpsName;
					
					var cell1_ = row_.insertCell(1);
					cell1_.innerHTML = tabdata[elm].dfSpsNric;
					
					var cell2_ = row_.insertCell(2);
					cell2_.innerHTML = tabdata[elm].dfSpsDob;
					
					row_.onclick=function(){
						row_.cells[0].childNodes[0].checked=true;
						getSelectedClient2Info(row_.cells[0].childNodes[0]);
					}
					
					
				});
				
			})
			
		});
}

function setClient2InfoToAllSection(dialog){
	var tbl_ = document.getElementById("c2dynatable");
	var tbody_ = tbl_.tBodies[0];
	var trow_ = tbody_.rows.length;
	
	if(trow_ > 0){
		
		for(var dat = 0; dat<trow_ ; dat++){
			var selectedClient2 = tbody_.rows[dat].cells[0].childNodes[0].checked;
			if(selectedClient2){
				var client2info = tbody_.rows[dat].cells[0].childNodes[1].value;
				
				
				client2info = JSON.parse(client2info);
				
				
				for(var elm in client2info){
					
					
					if (client2info.hasOwnProperty(elm)) {
						
						 var val = client2info[elm];
						
						 
						 if(elm == "dfSpsName"){
		    					$('input[name="txtFldSpouseName"],input[name="dfSpsName"]').val(val);
		    					changeSpouseNames($("#dfSpsName"))
		    					$("#signsts_sps_div").removeClass("disabledivs");
		    					
		    					
		    				}
						 
	    				$("[name="+elm+"]").val(val);
	    				
	    				if(elm=='dfSpsDob'){
	    					$("[name="+elm+"]").trigger('onblur');
	    				}
	    				
	    				if(elm=='dfSpsNation'){
	    					for(var cnt=0;cnt<3;cnt++){
								if(document.getElementsByName('dfSpsNationality'))
									document.getElementsByName('dfSpsNationality')[cnt].checked=false;
							}

							switch (val) {
								case "SG":document.getElementsByName('dfSpsNationality')[0].checked = true;;break;
								case "SGPR":document.getElementsByName('dfSpsNationality')[1].checked = true;break;
								case "OTH":document.getElementsByName('dfSpsNationality')[2].checked = true;break;
							}

							chkForContry('dfSpsNationality',val);
	    					
	    				}
	    				
	    				if(elm=='dfSpsHeight'){
	    					$("[name="+elm+"]").val(removeDecimalPoint(val));
	    				}
	    				
	    				
	    				
	    				
						 }
					
				}
    				
			}
		}
		
		copyRegAddrToMailAddr(fnaForm.dfSelfSameasregadd);
		
	} 
		
	dialog.dialog("close");
    removeTblRows("c2dynatable");
	 
	
}

function getSelectedClient2Info(chk){
	var curchecked = $(chk).prop("checked");
	
	
	$('.c2clientdynachk').not($(chk)).prop('checked', false);
	
	$(chk).prop("checked",!curchecked);
	
}

function openPopupBox(){
	
	var isClientSignDone = chkClientSignatureDone();
	
	var isClient2SignDone  = true;
	 var page2spouseNameTmp = $("#dfSpsName").val();
	 if(!isEmpty(page2spouseNameTmp)){
		 isClient2SignDone = chkClient2SignatureDone();
	 }
	 
	var isAdviserSignDone = chkAdviserSignatureDone();
	var isManagerSignDone = $("input[id='hTxtFldMgrSignStatus']").val();
	
	if(!(isClientSignDone && isClient2SignDone && isAdviserSignDone)){
		
//		var e = e || event;
		openpopboxsrc($('#esignaturepopbox'));
//		$('#esignaturepopbox').find("div.box").css("display","block");
//		$('#esignaturepopbox').trigger("click");
	}
	
}

function closePopupBox(){
	$('#esignaturepopbox').find("div.box").css("display","none");
}


//This is for New Approval Screen
function updateManagerStatus(signfor,flag){
	var agree_m ="",disagree_m="",reason_m="",status_m="";
//	if(signfor == "manager"){
		agree_m= $("input[name='suprevMgrFlg']:eq(0)").prop("checked");
		disagree_m = $("input[name='suprevMgrFlg']:eq(1)").prop("checked");	
		reason_m = $("textarea[name='suprevFollowReason']").val();
		
		status_m = agree_m ? "APPROVE" : (disagree_m ? "REJECT" : "");
		
//	}
		
		var agreefld  =  $("input[name='suprevMgrFlg']:eq(0)");
		var disagreefld =$("input[name='suprevMgrFlg']:eq(1)");
		
		if(!agree_m && !disagree_m){
			closePopupBox();
			alert(FNA_APPR_MGR_AGREE )
			$("#tr_mgr_agreesec").css({"border":"1px solid red"});
			$("input[name='suprevMgrFlg']").closest("td").css({"background-color":"yellow"});
			return false;
		}
		
		if(!agree_m && disagree_m){
			if(isEmpty(reason_m)){
				alert(MANAGER_KEYIN_REMARKS);
				return false;
			}
		}
		if(flag){
			
			if(agree_m){
				var conf = window.confirm(CONFIRM_APPROVE_MANAGER);
				if(!conf){
					var loadMsg = document.getElementById("loadingMessage");
					var loadingLayer = document.getElementById("loadingLayer");
					loadMsg.style.display="none";
//					selObj.value = "";
					
					agreefld.prop("checked",false);
					disagreefld.prop("checked",false);
					
					return false;
				}else{
					agreefld.prop("checked",true).prop("disabled",true);
					disagreefld.prop("checked",false).prop("disabled",true);
					
				}	
			}
			
			if(disagree_m && !agree_m){
				var conf = window.confirm(CONFIRM_REJECT_MANAGER);
				if(!conf){
					var loadMsg = document.getElementById("loadingMessage");
					var loadingLayer = document.getElementById("loadingLayer");
					loadMsg.style.display="none";
					agreefld.prop("checked",false);
					disagreefld.prop("checked",false);
//					selObj.value = "";
					return false;
				}
			}
			
		}
		
		var mangrEmailId = $("#txtFldAdvMgrEmailId").val()
		var mangrId = $("#txtFldMgrId").val()
		var hNTUCPolicyId = $("#ntucCaseId").val();
		var polNo = "";
		
		var advstfEmailId = $("#txtFldAdvEmailId").val();			            	  					
		var advName = $("#txtFldAdvName").val();
		var custName = $("#dfSelfName").val();
		
		var approverAdvStfId = "";
		var approverUserId =$("#hTxtFldFnaLoggedUserId").val();
		var strLoggedUserEmailId = "";
		var fnaPrin = "";
		var fnaId= $("#txtFldFnaId").val();
		
		
		var ajaxParam = "dbcall=MANAGER_APPROVE_STATUS"
		+"&txtFldFnaId="+fnaId
		
		+"&txtFldMgrAppStatus="+status_m
		+"&txtFldManagerEmailId="+mangrEmailId
		+"&txtFldManagerId="+mangrId
		+"&txtFldManagerRemarks="+reason_m
		
		+"&hNTUCPolicyId="+hNTUCPolicyId
		+"&txtFldPolNum="+polNo
		
		+"&txtFldAdvEmailId="+advstfEmailId
		+"&txtFldAdviserName="+advName
		+"&txtFldCustName="+custName		
		 
		+"&txtFldApprAdvId="+approverAdvStfId
		+"&txtFldApprUserId="+approverUserId
		+"&txtFldApprUserEmailId="+strLoggedUserEmailId
		+"&txtFldFnaPrin="+fnaPrin
		;
		
		
		console.log("ajaxParam ------->"+ajaxParam);
		
		setTimeout(function() {
			window.parent.document.getElementById('cover-spin').style.display="block";
		}, 0);
		setTimeout(function() {
			var ajaxResponse = callAjax(ajaxParam,false);
			window.parent.document.getElementById('cover-spin').style.display="block";
			retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
			 
			
			for ( var val in retval) {
				
				var tabdets = retval[val];
				
				if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				}
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						var value = tabdets[tab];
						
						
						if (tab == "TAB_MANAGER_APPROVE_DETAILS") {
							
							alert(value[0].MANAGER_POLICY_STATUS);
							parent.location.reload(true);

//							 location.reload(true);
							 
						}
					}
				}
			}
		}, 1000);
		
		
}