/**
 * 
 */

var getBrowserApp=navigator.appName;

function isEmpty(str){
	if(str == null || str.length == 0 || str===undefined)return true;
	else return false;
}



function comboMakerByString(inputStr,selObj,selectedVal,addselectflg){
	
	var valArrayObj = [];
	var inArrObj = inputStr.split("^");
	
	
	for(var opt=0;opt<inArrObj.length;opt++){
		valArrayObj[opt] = new Option(inArrObj[opt],inArrObj[opt]);	
	}
	
	
	if(addselectflg){
		addSelectOption(selObj);
		selObj.value = "";
		
	}
	
    for(var k=0; k < valArrayObj.length; k++) {
    	
        if(valArrayObj[k].value.length>0){
        	selObj.options[selObj.options.length++] = new Option(valArrayObj[k].value,valArrayObj[k].text);
        } else    {
            selObj.options.length=0;
            return;
        }
        if(!isEmpty(selectedVal))    {
            if(selectedVal == valArrayObj[k].text)	        {
                selObj.options[k].selected=true;
            }//end if
        }//end if
    }//end for{k}

}
 
function comboMakerByKeyVal(inputStr,selObj,selectedVal,addselectflg){

	
	var valArrayObj = [];
	var inArrObj = inputStr.split("^");
	
	
	for(var opt=0;opt<inArrObj.length;opt++){
		valArrayObj[opt] = new Option(inArrObj[opt].split("=")[0],inArrObj[opt].split("=")[1]);	
	}
	
	
	if(addselectflg){
		addSelectOption(selObj);
		selObj.value = "";
		
	}
	
    for(var k=0; k < valArrayObj.length; k++) {
    	
        if(valArrayObj[k].value.length>0){
        	selObj.options[selObj.options.length++] = new Option(valArrayObj[k].value,valArrayObj[k].text);
        } else    {
            selObj.options.length=0;
            return;
        }
        if(!isEmpty(selectedVal))    {
            if(selectedVal == valArrayObj[k].text)	        {
                selObj.options[k].selected=true;
            }//end if
        }//end if
    }//end for{k}


}

function comboMaker(cellObj,valArrayObj,selectedVal,addselectflg){
	 
	
	if(addselectflg){
		addSelectOption(cellObj.children[0]);
		cellObj.children[0].value = "";
		
	}
    
    for(var k=0; k < valArrayObj.length; k++)
    {
        if(valArrayObj[k].value.length>0)    {
        	cellObj.children[0].options[cellObj.children[0].options.length++] = new Option(valArrayObj[k].value,valArrayObj[k].text);
        }
        else    {
            cellObj.children[0].options.length=0;
            return;
        }
       
    }//end for{k}
    
    if(!isEmpty(selectedVal))    {
    	cellObj.children[0].value=selectedVal;
    }else{
    	cellObj.children[0].value="";
    }
}//end comboMaker


function comboMakerById(cellObj,valArrayObj,selectedVal,addselectflg){
	
	if(addselectflg){
		addSelectOption(cellObj);
		cellObj.value = "";
	}
    
    for(var k=0; k < valArrayObj.length; k++)
    {
        if(valArrayObj[k].value.length>0)    {        	
            cellObj.options[cellObj.options.length++] = new Option(valArrayObj[k].value,valArrayObj[k].text);
        }
        else    {
            cellObj.options.length=0;
            return;
        }
       
        if(selectedVal != "")    {        	
            if(selectedVal == valArrayObj[k].value)	        {
                cellObj.options[k].selected=true;
            }//end if
        }//end if
    }//end for{k}
}//end comboMaker

function clearSelectBoxOptions(obj){
	for(var index=obj.options.length-1;index>=0;index--){
     obj.options[index]=null;
   }
  obj.options[0] = new Option("--SELECT--", "");
}

function addSelectOption(obj) {
	
    var option = document.createElement("option");
    option.text = "--SELECT--";
    option.value = "";
    obj.add(option,0);
    
}


function makeOptionArr(strValues,splitby){
	
	var retArr = [];
	
	var inArrObj = strValues.split(splitby);
	
	for(var opt=0;opt<inArrObj.length;opt++){
		
		retArr[opt] = new Option(inArrObj[opt],inArrObj[opt]);
		
	}
	
	return retArr;
	
}

function copyOptionsArrFromSel(sourceSelect){
	
	
	var retArr = [];
	
	
	for(var opt=0;opt<sourceSelect.options.length;opt++){
		
		retArr[opt] = new Option(sourceSelect.options[opt].value,sourceSelect.options[opt].text);
		
	}
	
	
	return retArr;
	
}

function chkDigitOnly(e){
	
	 var key = e.charCode || e.keyCode || 0;
	  
	   
	   //array of key to escape from validations
	   var myArray = [46,8, 9, 27, 13, 190];
	   
	   //used to restrict more than one decimal point
	   if($(this).val().indexOf('.') != -1){
		   var number = $(this).val().split('.'); 
		   if(number.length >1 && key == 46){
			   myArray.splice(0,46);
		   }//End of if
	   }//End of if
	   
	   
		   if ($.inArray(key, myArray) !== -1 ||
				   //arrow key to escape from checking
			        (key >= 35 && key <= 39)) {
			      return;       
			    }
		  
		   var charVal = String.fromCharCode(key);
			   valid = /^[0-9]+$/.test(charVal);
			   //valid = decimalValidation(this);
			   
		   if(!valid){
			   e.preventDefault();
		   }//End of if
}


function doValidation(numFldArr,dateFldArr,emailFldArr){
	var num,numArrLen = numFldArr.length;
	var dateArrLen = dateFldArr.length;
	var eml,emlArrLen = emailFldArr.length;
	
	
	for(num = 0;num<numArrLen;num++){
		doNumValidation(numFldArr[num]);
	}//end of for
	
	for(var dt=0;dt<dateArrLen;dt++){
		   var dtElem = document.getElementById(dateFldArr[dt]);
		   
			   $(dtElem).bind('change',function(){
				   var dtFld = $(this);
				   isValidDate(dtFld);
			   });  
	   }//end of for
	
	for(eml=0;eml<emlArrLen;eml++){   
		validateEmailId(emailFldArr[eml]);
	}//end of for
	//setDymcIdForTblTxtFlds();
}//end of validateNumber()
function validateNumber(event,obj){

	
	var e=event;
	var key = e.charCode || e.keyCode || 0;
	var excepKeyArr = [46,8, 9, 27, 13, 190];
	var val = obj.value;
	
	if(val.indexOf('.') != -1){
		
		var number = val.split('.');
		
		if(number.length >1 && key == 46){
			   excepKeyArr.splice(0,46);
		}//End of if
	 }else{
		 
		 if(val.length>12 && key != 190){				 
			 	if ($.inArray(key, excepKeyArr) !== -1 ||(key >= 35 && key <= 39)) {}
			 	else e.preventDefault();
		 }//else if(val.length>13 && key == 190){}
		   
	}
	if ($.inArray(key, excepKeyArr) !== -1 ||(key >= 35 && key <= 39)) {}
	var charVal = String.fromCharCode(key);
	valid = /^[0-9]+$/.test(charVal);
	if(!valid){e.preventDefault();}//End of if

}

function doNumValidation(e){
	
	var elem = $( "input[name='"+id+"']" );//document.getElementById(id);
	   var valid = '';
	   
//		   $(elem).bind('keypress',function(e){
			  // setDymcIdForTblTxtFlds();
			   var key = e.charCode || e.keyCode || 0;
			  
			   
			   //array of key to escape from validations
			   var myArray = [46,8, 9, 27, 13, 190];
			   
			   //used to restrict more than one decimal point
			   if($(this).val().indexOf('.') != -1){
				   var number = $(this).val().split('.'); 
				   if(number.length >1 && key == 46){
					   myArray.splice(0,46);
				   }//End of if
			   }//End of if
			   
			   
				   if ($.inArray(key, myArray) !== -1 ||
						   //arrow key to escape from checking
					        (key >= 35 && key <= 39)) {
					      return;       
					    }
				  
				   var charVal = String.fromCharCode(key);
					   valid = /^[0-9]+$/.test(charVal);
					   //valid = decimalValidation(this);
					   
				   if(!valid){
					   e.preventDefault();
				   }//End of if
//		   }
//		   );//end of function	
}//end of doNumValidation


function isValidDate(txtFld){

	var currFld = $(txtFld),currVal = $(txtFld).val();
	
	if(!isEmpty(currVal)){
		
		var message = {
				DATE_FORMAT:'date format must be dd/mm/yyyy',
				DATE_BET:'Date must between \'1\' to \'31\'',
				MON_BET:'Month must between \'1\' to \'12\'',
				INVD_DT:'Invalid date for this month',
				LEAP_YR:'This is not leap year'
		};
//		if(currVal == null)
//			return false;
	//	
//		if(!isEmpty(currVal)){
//			alert(message.DATE_FORMAT);
//			currFld.val('');
//			currFld.focus();
//			return ;
//		}
			
		
		var regexDate = /^(\d{1,2})(\/)(\d{1,2})(\/)(\d{4})$/;
		var dtArr = currVal.match(regexDate);
		
		
		if(dtArr == null){
			alert(message.DATE_FORMAT);
			currFld.val('');
			return false;
		}
		
		dtDay = dtArr[1];
		dtMonth = dtArr[3];
		dtYear = dtArr[5];
		
//		alert(dtDay + "," +dtMonth + ","+dtYear);
		
		if(dtMonth < 1 || 12 < dtMonth){alert(message.DATE_BET);clrFld(txtFld);return;}
		else if(dtDay < 1 || dtDay >31){alert(message.MON_BET);clrFld(txtFld);return false;}
		else if((dtMonth==4 || 
				 dtMonth==6 || 
				 dtMonth==9 ||
				 dtMonth==11) && dtDay == 31){alert(message.INVD_DT);clrFld(txtFld);return;}
			
		else if(dtMonth == 2){
			var isLeap = (dtYear%4 == 0 && (dtYear%100 !=0 || dtYear%400 ==0));
			if(dtDay>29 || (dtDay ==29 && !isLeap)){alert(message.LEAP_YR);clrFld(txtFld);return;}
				
		}//end of if
		
	}
	

}//end of validateEmailId

function validateEmailId(emlElemId){
	
	
//	var emlFldElem = $( "input[name='"+emlElemId+"']" ) || $("#"+emlElemId);
	
	var emlFldElem = document.getElementById(emlElemId) || $("#"+emlElemId) ;
	
	   
	//   $(emlFldElem).bind('change',function(){
	
	var emailidArr = emlFldElem.value.split(",");
	
	for(var em in emailidArr){
		
//		var value = emlFldElem.value.trim();
		if(!isEmpty(emailidArr[em])){
			
			var value = emailidArr[em].trim();
			
			  
			  var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			  
			  
			  if (!filter.test(value)  ) {
				  
				  alert('EmailId not valid');	
				  
				  emlFldElem.value = "";
				  
				  if (getBrowserApp=="Microsoft Internet Explorer"){
				  
					  emlFldElem.focus();
				  
				  }
				  return false;		  
			  }
			  
		}  
	}
	
	  
	  return  true;
//});
}//end of validateEmailId

function clrFld(obj){$(obj).val('');}//end of clrFld


function includeTxtFldClass(txtFldArr){
	if(txtFldArr.length >0){
		for(var cls=0;cls<txtFldArr.length;cls++){
			var tldFldElem = $("input[name='"+txtFldArr[cls]+"']");
			if(containClass(tldFldElem)){
				var _$cls = $(tldFldElem).attr('class');
				$(tldFldElem).addClass(_$cls+' kycEditText');
			}
		}//end of for
	}//end of if
}//end of 

function containClass(elem){
	if(!$(elem).attr('class'))return false;
	var _$len = $(elem).attr('class').length;
	if(_$len > 0)return true; else return false;
}//end of containClass

function tblIncTxtFldClass(tblId){
	var _$tblElem = $('#'+tblId),_$tblBody = _$tblElem.find('TBODY');
	_$tblElem.attr('border','1');
	var _$rows = _$tblBody.find('TR');
	_$rows.each(function(){
		var _$cells = $(this).find('TD');
		_$cells.each(function(){
			if($(this).find('INPUT')){
				var _$txtFld = $(this).find('INPUT[TYPE="TEXT"]');
				if(!containClass(_$txtFld)){
					$(_$txtFld).addClass('kycEditText');
				}else{
					var _$cls = _$txtFld.attr('class');
					$(_$txtFld).addClass(_$cls+' kycEditText');
				}
			}//end of if 
		});
	});
}//end of tblIncClass


function checkMandatoryField(obj,alertMsg,page,formType)	{
	 
//	var objval =  jQuery.type(obj) ? obj.val() : obj.value;
	
	
	
	var objval =    obj.value;
	
	
	if(isEmpty(objval)){
		
		alert(alertMsg);
		
		if(page && formType){
			setPageFocus(page,formType);	
		}
		obj.focus();	
		return false;
	}
	return true;
}

function textCounter( field,maxlimit ) {

	var len=field.value.length;
	  if ( len> maxlimit )
	  {
	    field.value = field.value.substring( 0, maxlimit );
	    donothing();
	    return false;
	  }

	}//end textCounter


function donothing()
{
document.returnValue=true;
}//end donothing

function selectSingleRow(currRow)
{

	document.getElementById('hSelectRow').value="";
	var row;
	if (window.event){
		e = window.event;
		row = e.srcElement;
	}else{
		row = currRow;
	}
	//var row = window.event.srcElement;

	if(row.tagName=='SPAN')
	{
		row=row.parentNode.parentNode;
	}
	else
	if(row.tagName=='TD')
	{
		row=row.parentNode;
	}
	else
	if(row.tagName=='TR')
	{
		row=row;
	}else if(row.tagName=='INPUT'){
		row=row.parentNode.parentNode;
	}
	else
		return false;

	while (row.tagName != "TR")
	row = row.parentNode;

	var table = row;
	while (table.tagName != "TABLE")
		table = table.parentNode;
	//alert("table id " +table.id);
	
	var rows = table.rows;
	// to set the selected row Index
	document.getElementById('hSelectRow').value = row.rowIndex;

	for (iCel=0; iCel < rows.length; iCel++)
	{
		color = "";
		if (row == rows[iCel]) {
		   color = "#c2e4b7";
		}
		 var cells = rows[iCel].cells;
		for (jCel = 0; jCel < cells.length; jCel++){
		    var e = cells[jCel];
		    e.style.backgroundColor =color;
		    //e.style.color=bgcolor
		}//end for(jCel=0;)
	}//end for(iCel=1;)
}//end of selectSingleRow()

//To deselect the row in a table on click
function deselectSingleRow(tblId){

	if(!IsEmpty(tblId)){
		var table = document.getElementById(tblId);
		var rows = table.rows;

		for (iCel=0; iCel < rows.length; iCel++)
		{
			color = "";
			 var cells = rows[iCel].cells;
			for (jCel = 0; jCel < cells.length; jCel++){
			    var e = cells[jCel];
			    e.style.backgroundColor =color;
			    // e.style.color=bgcolor
			}// end for(jCel=0;)
		}// end for(iCel=1;)
	}
}// end of selectSingleRow()

function deleteAllRows(tableID){
	/*var table = document.getElementById(tableID);	  
	var rowCount = table.tBodies[0].rows.length;
	if(rowCount>0){
		for(var row=0;row<rowCount;row++)
		table.tBodies[0].deleteRow(row);
	}*/
//	$("#"+tableID+" tbody tr").remove(); commented by kavi 28/09/2018
	
if(tableID=="fnaDepnTbl")	{
	$("#"+tableID+" tbody tr").each(function(i){
		var mode=$(this)[0].childNodes[1].childNodes[1].value;
		
		if(mode==UPD_MODE){
		$(this).hide();
		$(this)[0].childNodes[1].childNodes[1].value=DEL_MODE;
		}else{
		$(this).remove();
		}
		
	});
}
	
if(tableID=="fnaWithdrawPolTble"){
	$("#"+tableID+" tbody tr").each(function(i){
	var mode=$(this)[0].lastChild.childNodes[1].value;
	
	if(mode==UPD_MODE){
		$(this).hide();
		$(this)[0].lastChild.childNodes[1].value=DEL_MODE;
		$(this)[0].childNodes[2].childNodes[1].value=DEL_MODE;
	}else{
		$(this).remove();
	}
	});	
}


}

function deleteRowInTbl(tableID,reorderflg){
	
	var table = document.getElementById(tableID);	  
	var rowCount = table.rows.length;
	
 
	if(rowCount<1){
		alert("No rows to delete!");
		return;
	}
	
	if(isEmpty(document.getElementById("hSelectRow").value)){
		alert("Select a row to delete!");
		return;
	}
			 
	if(tableID=="fnaFatcaTbl" || tableID=="fnaFatcaTblSps" || tableID=="fnaDepnTbl" || tableID=="tblClientFbCustRef" || tableID=="tblClientFbAdviserRef"){
	var mode=table.rows[document.getElementById("hSelectRow").value].childNodes[1].childNodes[1].value;
	
	if(mode==UPD_MODE){
		$(table.rows[document.getElementById("hSelectRow").value]).hide();
		table.rows[document.getElementById("hSelectRow").value].childNodes[1].childNodes[1].value=DEL_MODE;
	}else{
		table.deleteRow(document.getElementById("hSelectRow").value);
	}
	
	}
	
	if(tableID=="fnaWithdrawPolTble"){
		var mode=table.rows[document.getElementById("hSelectRow").value].lastChild.childNodes[1].value;
		
		if(mode==UPD_MODE){
			$(table.rows[document.getElementById("hSelectRow").value]).hide();
			table.rows[document.getElementById("hSelectRow").value].lastChild.childNodes[1].value=DEL_MODE;
			table.rows[document.getElementById("hSelectRow").value].childNodes[2].childNodes[1].value=DEL_MODE;
			incrIndex(tableID,reorderflg);
		}else{
			table.deleteRow(document.getElementById("hSelectRow").value);
		}
	}
	
	if(tableID=="fnaartufundTbl" || tableID=="fnaSwrepFundTbl" || tableID=="fnaClntAtchTbl"){
		var mode=table.rows[document.getElementById("hSelectRow").value].firstChild.childNodes[1].value;
		
		if(tableID=='fnaClntAtchTbl' && mode==QRY_MODE){
			alert("Row cannot be deleted.");
			return false;
		}
		
		if(mode==UPD_MODE){
			$(table.rows[document.getElementById("hSelectRow").value]).hide();
			table.rows[document.getElementById("hSelectRow").value].firstChild.childNodes[1].value=DEL_MODE;
			incrIndex(tableID,reorderflg);
		}else{
			table.deleteRow(document.getElementById("hSelectRow").value);
		}
	}
	// to delete the rows selected	
	
		
	if(document.getElementById('hSelectRow')){
		document.getElementById('hSelectRow').value = '';
	}
				rowCount--;
	incrIndex(tableID,reorderflg);
} 

function incrIndex(tblName,reorderflg) {
	
	if(reorderflg){
	
	/*	var tblObj = document.getElementById(tblName);
		var rowLen = tblObj.tBodies[0].rows.length;	
		var tBodyObj = tblObj.tBodies[0];*/
		
		/*for(var i=0;i<rowLen;i++)
		{
	         var lclIndex = i+1;      
	         tBodyObj.rows[i].cells[0].firstChild.value =lclIndex;        	
	        
		} 	*/ //commented by kavi 27/09/2018
		
		$("#"+tblName+" tbody tr:visible").each(function(i){
			$(this).find("td:first input:eq(0)").val(i+1);
		});
	}
}//end incrIndex

function formFldEnableDisable(fldarr,prop){
	
	var readOnlyProp=false;
	var cssstyle="";
	
	if(prop == "disable"){
		readOnlyProp = true;
		cssstyle="readOnlyText";
	}else{
		readOnlyProp=false;
		cssstyle="";
	}
	
//	if(fldarr.length>0){
		
		for(var fld in fldarr){
			//alert(fldarr[fld]);
			document.getElementById(fldarr[fld]).readOnly=readOnlyProp;
			//alert(document.getElementById(fldarr[fld]).className)
			if(isEmpty(cssstyle)){
				cssstyle = document.getElementById(fldarr[fld]).className;
			}
			document.getElementById(fldarr[fld]).className=cssstyle;
		}
//	}
	
}


function tblCrudImgVisible(AllTabImageObj,visibleImageObj,primaryKeyValue){
	
//	alert("ss");

	//It Initially, Hide all the images to be hidden. 
	for(var allImgKey in AllTabImageObj){
		if(AllTabImageObj.hasOwnProperty(allImgKey)){
			var tabImg = AllTabImageObj[allImgKey];
			fnShowHideImg(tabImg[0],[],"hidden",primaryKeyValue);
		}
	}
	
	for (var allImgKey in AllTabImageObj) {
		
	    if (AllTabImageObj.hasOwnProperty(allImgKey)) {
	    	
	       var tabImg = AllTabImageObj[allImgKey];
	       //alert(allImgKey);	       
	       for(var visbImgKey in visibleImageObj){
	    	   
	    	   if(visibleImageObj.hasOwnProperty(visbImgKey)){
	    		   if(visbImgKey == allImgKey){
	    			   fnShowHideImg(tabImg[0],visibleImageObj[visbImgKey],"visible",primaryKeyValue);
	    		   }
	    	   }
	       }
	       
	    }
	}
}

/**
 * This method is used to visible/hide the images. This is meant to reduce the repeated code to the "tblCrudImgVisible" method
 * @param allImages -- This is from the FPMSConstantScript.js (According to module)
 * @param visibleImages -- This is from the Corresponding script, and it is populated based on some condition
 * @param strvisible -- Either "visible" or "Hidden"
 * @param primaryKeyValue -- This is meant for show/hide the edit icon.
 */

function fnShowHideImg(allImages,visibleImages,strvisible,primaryKeyValue){
	
	if(visibleImages.length >0){
		
		
		for(var allKey in allImages){
			if(allImages.hasOwnProperty(allKey)){
				for(var visi in visibleImages){					
					if(allKey ==  visibleImages[visi]){
						if(document.getElementById(allImages[allKey])){							
							document.getElementById(allImages[allKey]).style.visibility=strvisible;
							if(isEmpty(primaryKeyValue)){								
								if(allKey == "EDIT"){
									document.getElementById(allImages[allKey]).style.display="none";
								}	
							}else{
								document.getElementById(allImages[allKey]).style.display="inline";
							}
													   
						}
					}
				}
			}
		}
	}else{		
		for(var allKey in allImages){
			if(allImages.hasOwnProperty(allKey)){
				if(document.getElementById(allImages[allKey]))
					document.getElementById(allImages[allKey]).style.visibility=strvisible;
				}
			}
		}	
}



function checkValueInArray(valueArr,valuetocheck){
	
	var IsExists = false;
	
	for(var opt = 0; opt < valueArr.length; opt++){
		if( valueArr[opt] == valuetocheck ){   
	        IsExists = true; 
	        break;        
	    }    
	}    
	
	return IsExists;
}


function creatSubmtDynaReportPDF(href){

	var lex1 = href.split('?');
	var action= lex1[0];
	var qstr = lex1[1];
	var obj = " ";

	var formId = document.getElementById("hiddenForm");
	var divId = document.getElementById("dynamicFormDiv");

	if(formId){
		if(divId)
			formId.removeChild(divId);
	}

	var newdiv = document.createElement('div');
	newdiv.id = "dynamicFormDiv";

	if(qstr != null) {
		var params = qstr.split('&');
		for(var p=0;p< params.length;p++){
			var keyValue = params[p].split('=');
			var name = keyValue[0];
			var value = keyValue[1];

			if(value.indexOf("\'") != -1)
				value = value.replace("\'","\'");

			obj += '<input type="hidden" name="'+name+'" value="'+value+'"/>';

		}
	}
	newdiv.innerHTML = obj;

	if(document.getElementById("hiddenForm"))
			document.getElementById("hiddenForm").appendChild(newdiv);

	document.forms["hiddenForm"].action=action;
	document.forms["hiddenForm"].method="POST";

	var wind = window.open('', 'TheWindowBIRT',"channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
	document.getElementById("hiddenForm").submit();
	wind.document.title = 'FNA Form Preview/Print';

}//end creatSubmtDynaReport

function trim(stringToTrim) {
    return stringToTrim.replace(/^\s+|\s+$/g,"");
}//end trim 



//added on 14052015


/** Function to Check the Max Length for Whole Numbers and Decimal Values*/

function chkDecimal(cellObj,maxlen,decilen)	{
	
	//alert(decilen);	
	
	
	if(!IsEmpty(cellObj.value))		{
		var cellValue=cellObj.value;
		var cellValueLen=cellValue.length;
		var precisionLen=maxlen-decilen;
		
		if(cellValue.indexOf(".")==-1)	{			
			if(cellValueLen>precisionLen)	{
				cellObj.value=cellObj.value.substring(0,precisionLen);
				return false;
			}
		}
		else if(cellValue.indexOf(".")!=-1)		{			
			var splitDeci= cellValue.split(".");
			var splitInteger=splitDeci[0];
			var splitDecimal=splitDeci[1];
			var splitIntLen= splitInteger.length;
			var splitDeciLen= splitDecimal.length;
			
					
			if(splitIntLen>precisionLen)		{				
				cellObj.value=splitInteger.substring(0, precisionLen) + "."+splitDeci[1];
				return false;
			}
			
			if(splitDeciLen > decilen){ //changed the == symbol to > symbol on 11-10-2012.
				var dec;
				var temp = cellObj.value.split(".");
//				alert(temp);
				dec =  temp[1].substring(0,(decilen-1));
//				alert(precisionLen+","+temp[0].length+","+splitIntLen+","+temp[0]+"."+dec);
				cellObj.value="";
				if((temp[0].length > (precisionLen))){
//					alert("1111");
					cellObj.value=temp[0]+"."+dec;
				}else if((temp[0].length <= (precisionLen))){
//					alert("222");
					cellObj.value=splitInteger.substring(0, precisionLen)+"."+dec;
				}
				//cellObj.value=cellValue.substring(0, (decilen-1));
				return false;
			}
		}
	}
	
	return true;
}//end chkDecimal


function blockNonNumbers(obj, e, allowDecimal, allowNegative)
{
	var key;
	var isCtrl = false;
	var keychar;
	var reg;
		
	if(window.event) {
		key = e.keyCode;
		isCtrl = window.event.ctrlKey
	}
	else if(e.which) {
		key = e.which;
		isCtrl = e.ctrlKey;
	}
	
	if (isNaN(key)) return true;
	
	keychar = String.fromCharCode(key);
	
	// check for backspace or delete, or if Ctrl was pressed
	if (key == 8 || isCtrl)
	{
		return true;
	}

	reg = /\d/;
	var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
	var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;
	
	return isFirstN || isFirstD || reg.test(keychar);
}//end of blockNonNumbers



function getCalendar(cal, event, value){
	//var cal = document.getElementsByName("txtFldCustPersdob")[0];
	var cell  = cal.parentNode;
	if (getBrowserApp=="Netscape"){
		scwShow(cell.childNodes[value], event);
	}
	else if (getBrowserApp=="Microsoft Internet Explorer"){
		scwShow(cell.firstChild, event);
	}
} // End Function (getCalendar)


function getCalendarInTbl(cal,event){

	var code;
	if (event.keyCode) code = event.keyCode;
	else if (event.which) code = event.which;

	if(code!=9){ // To escape the tab key in firefox.
		var parentCel = cal.parentNode;

		var hDateValue = parentCel.childNodes[1].value;
		 if(hDateValue == INS_MODE){
			 	getCalendar(cal, event,'0');
		 }
		 else if(hDateValue == UPD_MODE){
			 	getCalendar(cal, event,'0');
		 }
		 else{
			 	return false;
		 }
		 cal.focus();
	}
}//End function getCalendarInTbl




//added by johnson on 21042015

function datevalidate(obj,alertid)		{
	//alert("this obj value"+obj.value);	alert("alertd obj"+alertid);
	if(!(isEmpty(obj)))	{    
		if(!(CheckDate(obj,alertid)))	{
			//obj.select();
			return false;
		}
	}
	return true;
}


function CheckDate(obj,alertid,currDateFlag)
{ 
		var currDtObj = document.getElementById("TODAYDT");
		var strFormat = "dd/mm/yyyy";
		
		if(!(isDate(obj.value)))
		{ 
			alert("Date format should be 'DD/MM/YYYY' ") ;
			//obj.value="";
			if (getBrowserApp=="Netscape"){
				obj.value="";
				return false;
			}
			else{
				obj.value="";
				obj.focus();			
				return false;
			}
		}
		
		obj.value = FormatDate(obj.value,strFormat); // change the date format as required
		
		if(!isEmpty(obj.value)){
			var objYear = obj.value.substring(6,8);
//			alert(objYear);
			if(objYear < 19 ){
				alert("Date is out of range");
				obj.value="";
				obj.focus();			
				return false;
			}
		}
		
		if(currDateFlag){
		if(!(doDateCheck(obj,currDtObj)))
		{
		 // If No need to show alert msgs even the date is greater than SysDate (e.g., Date of Resign), pass alertid as NULL

		if(!isEmpty(alertid)) { 
			alert(alertid);
			obj.select();
			obj.focus();
		} 
		return false;
			}
		}
return true;
} //end CheckDate

function isDate(DateToCheck){
	if(DateToCheck==""){return true;}
	var m_strDate = FormatDate(DateToCheck);
	if(m_strDate==""){
	return false;
	}
	var m_arrDate = m_strDate.split("/");
	var m_DAY = m_arrDate[0];
	var m_MONTH = m_arrDate[1];
	var m_YEAR = m_arrDate[2];
	if(m_YEAR.length > 4 || m_YEAR.length < 4){return false;}
	m_strDate = m_MONTH + "/" + m_DAY + "/" + m_YEAR;
	var testDate=new Date(m_strDate);
	if(testDate.getMonth()+1==m_MONTH){
	return true;
	} 
	else{
	return false;
	}
	}//end function


function FormatDate(DateToFormat,FormatAs){
	if(DateToFormat==""){return"";}
	if(!FormatAs){FormatAs="dd/mm/yyyy";}

	var strReturnDate;
	FormatAs = FormatAs.toLowerCase();
	DateToFormat = DateToFormat.toLowerCase();
	var arrDate
	var arrMonths = new Array("January","February","March","April","May","June","July","August","September","October","November","December");
	var strMONTH;
	var Separator;

	while(DateToFormat.indexOf("st")>-1){
	DateToFormat = DateToFormat.replace("st","");
	}

	while(DateToFormat.indexOf("nd")>-1){
	DateToFormat = DateToFormat.replace("nd","");
	}

	while(DateToFormat.indexOf("rd")>-1){
	DateToFormat = DateToFormat.replace("rd","");
	}

	while(DateToFormat.indexOf("th")>-1){
	DateToFormat = DateToFormat.replace("th","");
	}

	if(DateToFormat.indexOf(".")>-1){
	Separator = ".";
	}

	if(DateToFormat.indexOf("-")>-1){
	Separator = "-";
	}


	if(DateToFormat.indexOf("/")>-1){
	Separator = "/";
	}

	if(DateToFormat.indexOf(" ")>-1){
	Separator = " ";
	}

	arrDate = DateToFormat.split(Separator);
	DateToFormat = "";
		for(var iSD = 0;iSD < arrDate.length;iSD++){
			if(arrDate[iSD]!=""){
			DateToFormat += arrDate[iSD] + Separator;
			}
		}
	DateToFormat = DateToFormat.substring(0,DateToFormat.length-1);
	arrDate = DateToFormat.split(Separator);

	if(arrDate.length < 3){
	return "";
	}

	var DAY = arrDate[0];
	var MONTH = arrDate[1];
	var YEAR = arrDate[2];




	if(parseFloat(arrDate[1]) > 12){
	DAY = arrDate[1];
	MONTH = arrDate[0];
	}

	if(parseFloat(DAY) && DAY.toString().length==4){
	YEAR = arrDate[0];
	DAY = arrDate[2];
	MONTH = arrDate[1];
	}


	for(var iSD = 0;iSD < arrMonths.length;iSD++){
	var ShortMonth = arrMonths[iSD].substring(0,3).toLowerCase();
	var MonthPosition = DateToFormat.indexOf(ShortMonth);
		if(MonthPosition > -1){
		MONTH = iSD + 1;
			if(MonthPosition == 0){
			DAY = arrDate[1];
			YEAR = arrDate[2];
			}
		break;
		}
	}

	var strTemp = YEAR.toString();
	if(strTemp.length==2){

		if(parseFloat(YEAR)>40){
		YEAR = "19" + YEAR;
		}
		else{
		YEAR = "20" + YEAR;
		}

	}


		if(parseInt(MONTH)< 10 && MONTH.toString().length < 2){
		MONTH = "0" + MONTH;
		}
		if(parseInt(DAY)< 10 && DAY.toString().length < 2){
		DAY = "0" + DAY;
		}
		switch (FormatAs){
		case "dd/mm/yyyy":
		return DAY + "/" + MONTH + "/" + YEAR;
		case "mm/dd/yyyy":
		return MONTH + "/" + DAY + "/" + YEAR;
		case "dd/mmm/yyyy":
		return DAY + "-" + arrMonths[MONTH -1].substring(0,3) + "-" + YEAR; // hyphen included instead of space by selva for this format 30-07-2008
		case "mmm/dd/yyyy":
		return arrMonths[MONTH -1].substring(0,3) + "-" + DAY + "-" + YEAR;
		case "dd/mmmm/yyyy":
		return DAY + "-" + arrMonths[MONTH -1] + "-" + YEAR;	
		case "mmmm/dd/yyyy":
		return arrMonths[MONTH -1] + "-" + DAY + "-" + YEAR;
		case "dd/mmm/yy":																// case included by selva 30/07/2008 for employee search purpose
	    return DAY+"-"+arrMonths[MONTH -1].substring(0,3) + "-" + YEAR.substring(2,4);
		}

	return DAY + "/" + strMONTH + "/" + YEAR;

	} //End Function

function contains(array,val) {
    for(elem in array)
        if(array[elem] == val)
            return true;
    return false;
}//End of contains

function removeItem(array, val){
    for(var elem in array){
        if(array[elem]==val){
            array.splice(elem,1);
//        	array[elem]='';
            break;
        }
    }
}//End of removeItem





function creatSubmtDynaAttchment(href){

	var lex1 = href.split('?');
	var action= lex1[0];
	var qstr = lex1[1];
	var obj = " ";

	var formId = document.getElementById("hiddenFormAttch");
	var divId = document.getElementById("dynamicFormDiv");

	if(formId){
		if(divId)
			formId.removeChild(divId);
	}

	var newdiv = document.createElement('div');
	newdiv.id = "dynamicFormDiv";

	if(qstr != null) {
		var params = qstr.split('&');
		for(var p=0;p< params.length;p++){
			var keyValue = params[p].split('=');
			var name = keyValue[0];
			var value = keyValue[1];

			if(value.indexOf("\'") != -1)
				value = value.replace("\'","\'");

			obj += '<input type="text" name="'+name+'" value="'+value+'"/>';

		}
	}
	newdiv.innerHTML = obj;

	if(document.getElementById("hiddenFormAttch"))
			document.getElementById("hiddenFormAttch").appendChild(newdiv);

	document.forms["hiddenFormAttch"].action=action;
	document.forms["hiddenFormAttch"].method="POST";

	window.open('', 'TheAttachmentWindow',"channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
	document.getElementById("hiddenFormAttch").submit();

}//end creatSubmtDynaReport


function scrollToView(id){
	document.getElementById(id).scrollIntoView();
}

function validateEmailComm(obj){	

	var emailidval =$(obj).val();
	
//	for(var em in emailidArr){
		
		if(!isEmpty(emailidval)){
			
			  var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			  
			  if (!filter.test(emailidval)  ) {
				  alert('EmailId not valid');	
				  $(obj).val("").focus();
				  return false;		  
			  }
			  
		}  
//	}
	  return  true;
}

function getParentByTagName(node, tagname) {
	var parent;
	if (node === null || tagname === '') return;
	parent  = node.parentNode;
	tagname = tagname.toUpperCase();

	while (parent.tagName !== "HTML") {
		if (parent.tagName === tagname) {
			return parent;
		}
		parent = parent.parentNode;
	}

	return parent;
}

function removeClass(el, className)
{
   if (el.classList)
       el.classList.remove(className)
   else if (hasClass(el, className))
   {
       var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
       el.className = el.className.replace(reg, ' ');
   }
}

function addClass(el, className) {
	  if (el.classList)
	    el.classList.add(className)
	  else if (!hasClass(el, className)) el.className += " " + className
}

function hasClass(el, className) {
	  if (el.classList)
	    return el.classList.contains(className)
	  else
	    return !!el.className.match(new RegExp('(\\s|^)' + className + '(\\s|$)'))
}
//added by kavi
String.prototype.equalIgnoreCase = function( str )
{
  return this.toUpperCase() === str.toUpperCase();
}
//added by kavi 25/02/2019
Number.prototype.countDecimals = function () {
    if(Math.floor(this.valueOf()) === this.valueOf()) return 0;
    return this.toString().split(".")[0].length || 0; 
}

function PasteOnlyNum(obj,pre){
	setTimeout(function(){
//		console.log(obj.value);
		
		if(pre==0){
		 var regNoPre = /^\d+$/;
		 if(!regNoPre.test(obj.value.trim())){
				obj.value="";
				alert("Numbers only allowed.")
	     }
		}else{
		 var reg =new RegExp("^[0-9]+(\.[0-9]{1,"+pre+"})?$");
		 if(!reg.test(obj.value.trim())){
			obj.value="";
			alert("Numbers only allowed.")
		 } 
		
		}
		
		obj.value=obj.value.trim();
		
	},400);
}	

function chkValueExistInList(objList,val){
	var rtnval="";
	$.each(objList,function(i,key){
		if(key.value.toUpperCase()==val.toUpperCase()){
			rtnval=key.name;
		}
	});
	
	return rtnval;
}

function chkValueExistInListOthr(objList,val){
	var rtnval="";
	if(!isEmpty(val)){		
		$.each(objList,function(i,key){
			if(key.value.toUpperCase()==val.toUpperCase()){
				rtnval=key.name;
			}
		});
		
	}
	
	
	if(!isEmpty(val) && rtnval==""){
		rtnval=objList[objList.length-1].name;
	}
	
	return rtnval;
}

function creatSubmtDynaForm(href){	
//	alert(href)
		var lex1 = href.split('?');
		var action= lex1[0];
		var qstr = lex1[1];
		var obj = " ";

//		var insertion="<form name='dynamicPostForm' id='dynamicPostForm' method='POST' action='"+action+"'>";
		var newdiv = document.createElement('div');
//        insertion += "</form>";
        
//        var frm = document.createElement("form");
//        frm.setAttribute("name", "dynamicPostForm");
//        frm.setAttribute("id", "dynamicPostForm");
//        frm.setAttribute("method", "POST");
//        frm.setAttribute("action", action);        
		
		if(qstr != null) {
			var params = qstr.split('&');
			for(var p=0;p< params.length;p++){
				var keyValue = params[p].split('=');
				var name = keyValue[0];
				var value = keyValue[1];
				obj += "<input type='hidden' name='"+name+"' value='"+value+"'/>";
				
			}
		}
		newdiv.innerHTML = obj;
//		This is not working in Firefox.(14)
//		 document.createElement(insertion);
		if(document.forms[0])
				document.forms[0].appendChild(newdiv);		
		
document.forms[0].id="dynamicPostForm";
document.forms[0].action=action;
document.forms[0].method="POST";
document.forms[0].submit();	
}//end creatSubmtDynaForm

function creatSubmtDynaNewWindow(href){
	
    var lex1 = href.split('?');
    var action= lex1[0];
    var qstr = lex1[1];
    var obj = " ";

    var formId = document.getElementById("hiddenForm");
    var divId = document.getElementById("dynamicFormDiv");

    if(formId){
        if(divId)
            formId.removeChild(divId);
    }

    var newdiv = document.createElement('div');
    newdiv.id = "dynamicFormDiv";

    if(qstr != null) {
        var params = qstr.split('&');
        for(var p=0;p< params.length;p++){
            var keyValue = params[p].split('=');
            var name = keyValue[0];
            var value = keyValue[1];

            if(value.indexOf("\'") != -1)
                value = value.replace("\'","\'");
                value = unescape(value);

            //obj += '<input type="hidden" name="'+name+'" value="'+value+'"/>';
            obj += "<input type='hidden' name='"+name+"' value='"+value+"'/>";

        }
    }
    newdiv.innerHTML = obj;   

    if(document.getElementById("hiddenForm"))
    	document.getElementById("hiddenForm").appendChild(newdiv);

    document.forms[1].action=action;
    document.forms[1].method="POST";

  window.open('', 'TheWindowBIRT',"channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
  document.getElementById("hiddenForm").submit();

}


function uwAttachLoginVerify() {
	//alert();
	var frm = document.loginForm;
	var userId = frm.txtFldUserId;
	var pass = frm.txtFldpassword;
	//var dist = frm.distributor;//DIST_CHNG
	//alert("dist"+dist.value);

	if(isEmpty(userId.value)) {
		//alert('Enter the UserName! ');
		if(document.getElementById("userIdMsg")) {
			document.getElementById("userIdMsg").style.display="";
			loginForm.txtFldUserId.focus();
		}
		if(document.getElementById("passMsg")) {
			document.getElementById("passMsg").style.display="none";
		}

			return false;
	}	else if(isEmpty(pass.value)) {
		//alert('Enter the Password! ');
		if(document.getElementById("passMsg")) {
			document.getElementById("passMsg").style.display="";
			loginForm.txtFldpassword.focus();
		}
		if(document.getElementById("userIdMsg")) {
			document.getElementById("userIdMsg").style.display="none";
		}
		
		return false;
	}
	else {
		
		frm.submit();
		
		window.moveTo(0,0);
	    window.resizeTo(screen.width+2,screen.height+30)
	    window.moveTo(0,0);
	    window.offscreenBuffering=true;
	    	
	    	
	}
}

function clearForm() {
	//alert("clear form");
	if(document.getElementById("userIdMsg")) {
		document.getElementById("userIdMsg").style.display="none";
	}
	if(document.getElementById("passMsg")) {
		document.getElementById("passMsg").style.display="none";
	}
	if(document.getElementById("distMsg")) {
		document.getElementById("distMsg").style.display="none";
	}
}

function loginautoCompleteOff(){
	for(var frmNum=0;frmNum<document.forms.length;frmNum++) {
		for(var cv=0;cv<document.forms[frmNum].length;cv++) {
		  	var currEle =document.forms[frmNum].elements[cv];
		  	if(currEle.type=='text'){  
		  		currEle.setAttribute("autocomplete","off"); 
		  	 }
		}
	}

}


function getParamValue($obj){
	
	var returnStr="";
	
	if (!(typeof $obj.val() === "undefined")) {
		returnStr=$obj.val()
	}
	
	return returnStr;
}

function formJsonText(fna_id,agentcode){
	
	//var fna_id = $(obj).closest('tr').find('td:eq(0)').find('input:first').val();
	
	
	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");
	
	
	var facode = $("#hTxtFldAFaCode").val();
	var faURL = $("#hTxtFldAFaURL").val();
	
//		loadMsg.style.display="block";
		//alert(loadMsg.style.display)
//		loadingLayer.style.display="block"; 
		
	
	
	var parameter = "dbcall=VER&FNA_ID=" + fna_id;
	console.log(parameter);
	
	var respText = ajaxCall(parameter);
	var retval = respText;
	
	
//	loadMsg.style.display="none";	 
	
	var jsonStr={};
	jsonStr['KYCID']=fna_id;	
	
	var JsnAgntIntro={}
	JsnAgntIntro['AgentCode']=agentcode;//$(obj).closest('tr').find("td:eq(2) input:eq(7)").val();
	JsnAgntIntro['LifeAdvisor']=true;
	JsnAgntIntro['HealthAdvisor']=true;
	JsnAgntIntro['ILPAdvisor']=true;
	
	 
	jsonStr['AgentIntroForm']=JsnAgntIntro;
	
	var JsnPrsnlGenrlForm={};
	var JsnContactNo={};
	var JsnAddress={};
	var JsnDepForm={};
	var JsnDepDetArr=[];
	var JsnDepDetObj={};
	var JsnContactSpsNo={};
	var JsnSpsAddress={};
	
	var isspousenameexist=false;
	
	for ( var val in retval) {
		
		var tabdets = retval[val];
		if (tabdets["SESSION_EXPIRY"]) {
			window.location = baseUrl + SESSION_EXP_JSP;
			return;
		}
		
		
		for ( var tab in tabdets) {
			if (tabdets.hasOwnProperty(tab)) {
				var value = tabdets[tab];
				
				if (tab == "ARCH_FNASSDET_TABLE_DATA") {
					
//					loadMsg.style.display="none";	
					
					
					var fnsDetl=value;//refarr['ARCH_FNASSDET_TABLE_DATA'];
					console.log(fnsDetl)
					
					JsnPrsnlGenrlForm['DateOfBirth']=fnsDetl.dfSelfDob.replace(/\//g, '-');
					JsnPrsnlGenrlForm['EducationLevel']=chkValueExistInList(educationList,fnsDetl.dfSelfEdulevel);
					
					//alert(fnsDetl.dfSelfGender)
					
					JsnPrsnlGenrlForm['GenderCode']=fnsDetl.dfSelfGender;
					JsnPrsnlGenrlForm['LanguageSpoken']=(isEmpty(fnsDetl.dfSelfEngSpoken))?"":(fnsDetl.dfSelfEngSpoken=='Y')?'011':'99';
					JsnPrsnlGenrlForm['LanguageWritten']=(isEmpty(fnsDetl.dfSelfEngWritten))?"":(fnsDetl.dfSelfEngWritten=='Y')?'011':'99';
					JsnPrsnlGenrlForm['MaritalStatus']=chkValueExistInList(maritalList,fnsDetl.txtFldCDClntMrtSts);
					JsnPrsnlGenrlForm['NRIC_Passport']=fnsDetl.txtFldCDClntNRIC;
					JsnPrsnlGenrlForm['Name']=fnsDetl.txtFldCDClntName;					
					JsnPrsnlGenrlForm['NationalityCode']=isEmpty(fnsDetl.dfSelfNationality)?"-1":
							fnsDetl.dfSelfNationality=='OTH' ? chkValueExistInListOthr(nationalityList,(isEmpty(fnsDetl.dfSelfNatyDets)?"-1":fnsDetl.dfSelfNatyDets))
								:chkValueExistInList(nationalityList,fnsDetl.dfSelfNationality);
					JsnPrsnlGenrlForm['Occupation']=(isEmpty(fnsDetl.dfSelfOccpn))?"D999":chkValueExistInListOthr(occupationList,fnsDetl.dfSelfOccpn);
					JsnPrsnlGenrlForm['Smoker']=fnsDetl.dfSelfSmoker;
					JsnPrsnlGenrlForm['CPFNo']=fnsDetl.dfSelfNric;//Dont know which Field
					
					JsnContactNo['Office']=fnsDetl.dfSelfOffice;
					JsnContactNo['Mobile']=fnsDetl.dfSelfMobile;
					JsnContactNo['Home']=fnsDetl.dfSelfHome;
					
					JsnPrsnlGenrlForm['ContactNo']=JsnContactNo;
					JsnPrsnlGenrlForm['Race']=chkValueExistInListOthr(raceList,fnsDetl.dfSelfRace);
					JsnPrsnlGenrlForm['Height']=removeDecimalPoint(fnsDetl.dfSelfHeight);			
					JsnPrsnlGenrlForm['NatureOfWork']=(fnsDetl.dfSelfBusinatr=='')?'':(fnsDetl.dfSelfBusinatr=='Others')?fnsDetl.dfSelfBusinatrDets:fnsDetl.dfSelfBusinatr;				
					JsnPrsnlGenrlForm['Weight']=fnsDetl.dfSelfWeight;			
					JsnPrsnlGenrlForm['EmailAddress']=fnsDetl.dfSelfPersemail;
					JsnPrsnlGenrlForm['NameOfCompanySchool']=fnsDetl.dfSelfCompname;
			
					JsnAddress["Address1"]=fnsDetl.htxtFldClntResAddr1;
					JsnAddress["Address2"]=fnsDetl.htxtFldClntResAddr2;
					JsnAddress["Address3"]=fnsDetl.htxtFldClntResAddr3;
					JsnAddress["Address4"]="";
					JsnAddress["PostalCode"]=(fnsDetl.resCountry.toUpperCase()=='SINGAPORE')?fnsDetl.htxtFldClntResPostalCode:'000000';
					JsnAddress["AddressType"]=(fnsDetl.resCountry.toUpperCase()=='SINGAPORE')?'Singapore':'Non Singapore';
					JsnAddress["Unit"]="";//Dont know which Field
					
					
					JsnPrsnlGenrlForm['Address']=JsnAddress;
					
					if(!isEmpty(fnsDetl.dfSpsName)){
						isspousenameexist=true;
					}
					
					JsnDepDetObj['Bday']=fnsDetl.dfSpsDob.replace(/\//g, '-');
					JsnDepDetObj['Gender']=fnsDetl.dfSpsGender;
					JsnDepDetObj['Name']=fnsDetl.dfSpsName;
					JsnDepDetObj['Occupation']=(isEmpty(fnsDetl.dfSpsOccpn))?"D999":chkValueExistInListOthr(nationalityList,fnsDetl.dfSpsOccpn);
					JsnDepDetObj['Relationship']="";
					JsnDepDetObj['Smoker']=fnsDetl.chkFnaSpsSmoke;
					JsnDepDetObj['NRIC_Passport']=fnsDetl.dfSpsNric;
					JsnDepDetObj['NationalityCode']=(isEmpty(fnsDetl.dfSpsNationality))?"-1":(fnsDetl.dfSpsNationality=='OTH')?chkValueExistInListOthr(nationalityList,(isEmpty(fnsDetl.dfSpsNatyDets)?"-1":fnsDetl.dfSpsNatyDets)):chkValueExistInList(nationalityList,fnsDetl.dfSpsNationality);
					JsnDepDetObj['CPFNo']=fnsDetl.dfSpsNric;
					
					JsnContactSpsNo['Office']=fnsDetl.dfSpsOffice;
					JsnContactSpsNo['Mobile']=fnsDetl.dfSpsHp;
					JsnContactSpsNo['Home']=fnsDetl.dfSpsHome;
					
					JsnDepDetObj['ContactNo']=JsnContactSpsNo;
					JsnDepDetObj['Race']='';//no field available
					JsnDepDetObj['LanguageWritten']=(isEmpty(fnsDetl.dfSpsEngWritten))?"":(fnsDetl.dfSpsEngWritten=='Y')?'011':'99';
					JsnDepDetObj['LanguageSpoken']=(isEmpty(fnsDetl.dfSpsEngSpoken))?"":(fnsDetl.dfSpsEngSpoken=='Y')?'011':'99';
					JsnDepDetObj['Height']='';//no field available
					JsnDepDetObj['NatureOfWork']=(fnsDetl.dfSpsBusinatr=='')?'':(fnsDetl.dfSpsBusinatr=='Others')?fnsDetl.dfSpsBusinatrDets:fnsDetl.dfSpsBusinatr;
					JsnDepDetObj['Weight']='';//no field available
					JsnDepDetObj['EmailAddress']=fnsDetl.dfSpsPersemail;
					JsnDepDetObj['NameOfCompanySchool']=fnsDetl.dfSpsCompname;
					JsnDepDetObj['MaritalStatus']=chkValueExistInList(maritalList,fnsDetl.dfSpsMartsts);
					
					JsnSpsAddress['Address4']='';//no field available
					JsnSpsAddress['Address3']=fnsDetl.resAddr3;
					JsnSpsAddress['Address2']=fnsDetl.resAddr2;
					JsnSpsAddress['Address1']=fnsDetl.resAddr1;
					JsnSpsAddress['PostalCode']=(fnsDetl.resCountry.toUpperCase()=='SINGAPORE')?fnsDetl.resPostalcode:'000000';
					JsnSpsAddress['AddressType']=(fnsDetl.resCountry.toUpperCase()=='SINGAPORE')?'Singapore':'Non Singapore';
					JsnSpsAddress['Unit']='';//no field available
					
					JsnDepDetObj['Address']=JsnSpsAddress;
					
				}
				
			}
		}
		
		
	}	
	
	jsonStr['PersonalGeneralForm']=JsnPrsnlGenrlForm;
	
	JsnDepDetArr.push(JsnDepDetObj);
	JsnDepForm['Dependents']=JsnDepDetArr;
	
	if(isspousenameexist){
		jsonStr['DependentsForm']=JsnDepForm;
	}
//	
//	jsonStr['faCode']="Avallis";
//	jsonStr['faURL']="www.ekyc.avallis.com";
	
	console.log(JSON.stringify({"personInfo":jsonStr,"faCode":facode,"faURL":faURL}));
	
	return JSON.stringify({"personInfo":jsonStr,"faCode":facode,"faURL":faURL});
}

/*
 * This function is to add value alone to select option tag <option "value">value</option> 
 * if Flag true then a select option will be created <option value="">--select--</option>
 * 23/10/2018
 */
function comboCloneOption($Obj,ToObj){
	
	var options=$Obj.find(">option").clone();
	
	$(ToObj).append(options);
	
}

/*
 * Added by kavi 26/02/2018
 * arr format [{key:value},{key:value}]
 * Jquery based
 */

function comboAddOptArr(arrObj,keyOpt,$elmobj){
	$elmobj.find("option:not(:first)").remove();
	
	$.each(arrObj,function(i,obj){
//		console.log(obj.txtFldSpouseName);
		if(isEmpty(obj[keyOpt]))return;
		
		var optTxt='<option value="'+obj[keyOpt]+'">'+obj[keyOpt]+'</option>';
		
		$elmobj.append(optTxt);
	});
	
}


function removeDecimalPoint(val){
	var precisonCnt=Number(val).countDecimals();
	var retrnval=""; 
	
	if(isEmpty(val)){
		
	}else if(precisonCnt==3){
		retrnval=parseFloat(Math.round(Number(val) * 1).toFixed(0));
	}else if(precisonCnt==2){
		retrnval=parseFloat(Math.round(Number(val) * 10).toFixed(0));
	}else if(precisonCnt==1){
		retrnval=parseFloat(Math.round(Number(val) * 100).toFixed(0));
	}else if(precisonCnt==0){
		retrnval = Number(val);
	}
	
	return retrnval;
}


/*
 *Used for selecting checked value in group of checkbox 
 */
function getSelectedValue(objArr){
	var retrnValue='';
	
	$(objArr).each(function(){
		if($(this).is(":checked"))
			retrnValue=$(this).val();
	});
	
	return retrnValue;
}



function sortTable(tableid,cellindex,childindex,dir) {
  var table, rows, switching, i, x, y, shouldSwitch,  switchcount = 0;
  table = document.getElementById(tableid);
  switching = true;
  while (switching) {
    switching = false;
    rows = table.rows;
    for (i = 1; i < (rows.length - 1); i++) {
      shouldSwitch = false;
      x = rows[i].cells[cellindex].childNodes[childindex];//getElementsByTagName("TD")
      y = rows[i + 1].cells[cellindex].childNodes[childindex];
      if (dir == "asc") {
        if (x.value.toLowerCase() > y.value.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.value.toLowerCase() < y.value.toLowerCase()) {
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      switchcount ++; 
    } else {
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}


function sortJSON(data, key, way) {
    return data.sort(function(a, b) {
        var x = a[key]; var y = b[key];
        console.log(x+"="+y)
        if (way === 'asc' ) { return ((x < y) ? -1 : ((x > y) ? 1 : 0)); }
        if (way === 'desc') { return ((x > y) ? -1 : ((x < y) ? 1 : 0)); }
    });
}


function groupTable($rows, startIndex, total){
	if (total === 0){
		return;
	}
	var i , currentIndex = startIndex, count=1, lst=[];
	var tds = $rows.find('td:eq('+ currentIndex +')');
	var ctrl = $(tds[1]);
	lst.push($rows[0]);
	for (i=1;i<=tds.length;i++){
		console.log(ctrl.find("input:eq(0)").val() + " = " + $(tds[i]).find("input:eq(0)").val())
		if (ctrl.find("input:eq(0)").val() ==  $(tds[i]).find("input:eq(0)").val()){
			count++;
			$(tds[i]).addClass('deleted');
			lst.push($rows[i]);
		}
		else{
			if (count>1){
				ctrl.attr('rowspan',count);
				groupTable($(lst),startIndex+1,total-1)
			}
			count=1;
			lst = [];
			ctrl=$(tds[i]);
			lst.push($rows[i]);
		}
	}
}

function copyTextArrFromOpt(sourceSelect){
	
	
	var retArr = [];
	
	
	for(var opt=0;opt<sourceSelect.options.length;opt++){
		
		retArr[opt] = sourceSelect.options[opt].text;
		
	}
	
	
	return retArr;
	
}


function generateFNAPDF(fnaid,custid,custLobDets) {

	var formtype = "SIMPLIFIED";

	var machine = "";
	
	
	machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC + "&__format=pdf" 
			+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaid
			+ "&P_PRDTTYPE=" + custLobDets+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + formtype+ "&P_DISTID=" + LOGGED_DISTID;


	creatSubmtDynaReportPDF(machine);
	
	
//	alert(machine)
//	
//	var $dialog = $('#pdfPopup')
////    .html('<iframe style="border: 0px; " src="' + machine + '" width="100%" height="100%" id="dynaFramePDF"></iframe>')
//    .dialog({
//        autoOpen: false,
//        modal: true,
//        height: "750",
//        width: "1090",
//        title: "FNA - Printable View",
//        buttons : {
//    		" OK " : function() {
//    				$(this).dialog("close");
//    			}
//    	}
//    });
//$dialog.dialog('open');
//
//$("#pdfPopup").find("#dynaFramePDF").prop("src",machine)





}




function removeTblRows(tblId) {

	var tBodyObj = document.getElementById(tblId).tBodies[0];
	var len = tBodyObj.rows.length;
	if (len > 0) {
		for (var i = len; i > 0; i--) {
			tBodyObj.removeChild(tBodyObj.rows[0]);
		}
	}
}