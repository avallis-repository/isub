var strGlblUsrPrvlg="";

var strFnaMgrUnAppDets="";
var STR_FNA_MGRAPPRSTS_LIST = "APPROVE^REJECT";
var strAdminFNADets = "";
var strComplianceFNADets = "";
var strLoggedUserEmailId = "";
var archiveflg= false;
var baseUrl;
var currDate = "";
var strMngrFlg="",strAdminFlg="",strCompFlg="";
var strALLFNAIds="";


var BIRT_URL="",FNA_RPT_FILELOC="";
var COMMON_SESS_VALS="";
var LOGGED_DISTID="";//,FNA_RPT_FILELOC="",FNA_PDFGENLOC="",FNA_FORMTYPE="";
var strLoggedAdvStfId = "";
var strLoggedUserId = "";
var ntuc_policy_access_flag=false,ntuc_sftp_acs_flg=false;
var isFromMenu="";

var pendingdatatable,approveddatatable,rejecteddatatable;

function callBodyInit(){
	
//	 var spltArr = strGlblUsrPrvlg.split(",");
//	 for(var arrlen=0;arrlen<spltArr.length;arrlen++){
//		 if(spltArr[arrlen] == NTUC_POLICY_ACCESS){
//			 ntuc_policy_access_flag = true;
//		 }
//		  
//	 }	
	
	for ( var sess in COMMON_SESS_VALS) {
		
		var sessDets = COMMON_SESS_VALS[sess];	  
		for (var dets in sessDets) {		  
			if (sessDets.hasOwnProperty(dets)) {
			  
				var sesskey = dets;
				var sessvalue = sessDets[dets];
				
				console.log("---------->"+sesskey +" == "+sessvalue);

			    if(sesskey == "LOGGED_DISTID") {LOGGED_DISTID = sessvalue;}
			    
			    if(sesskey == "LOGGED_USER_EMAIL_ID"){strLoggedUserEmailId= sessvalue;}
			    
			    if(sesskey == "TODAY_DATE"){currDate = sessvalue;}
			    
			    if(sesskey == "MANAGER_ACCESSFLG"){strMngrFlg = sessvalue;}
			    
			    if(sesskey == "LOGGED_ADVSTFID"){strLoggedAdvStfId = sessvalue;}
			    
			    if(sesskey == "LOGGED_USERID" ){strLoggedUserId = sessvalue;}//|| sessKey == "STR_LOGGEDUSER" || || sessKey == "LOGGED_USER"
			    
			    if(sesskey == "RPT_FILE_LOC"){FNA_RPT_FILELOC = sessvalue;}
			    
			    if(sesskey == "ADMIN_ACCESSFLG"){strAdminFlg = sessvalue;}
			    
			    if(sesskey == "COMPLIANCE_ACCESSFLG"){strCompFlg = sessvalue;}
			    
			    if(sesskey == "NTUC_POLICY_ACCESS"){ntuc_policy_access_flag = sessvalue;}
			    
			    if(sesskey == "NTUC_SFTP_ACCESS"){ntuc_sftp_acs_flg = sessvalue;}
			    
			}
		}
	}
	
	openList('PENDING');
	
	 pendingdatatable = $('#tblPending').DataTable(
			 { "columnDefs": [{ "targets": [ 6,7,8 ],"visible": false,"searchable": false}]}		 
	 );

	  approveddatatable = $('#tblApproved').DataTable(
			  { "columnDefs": [{ "targets": [ 6,7,8,10,11,12,13,14,15 ],"visible": false,"searchable": false}]}
			  );

	  rejecteddatatable = $('#tblRejected').DataTable(
			  { "columnDefs": [{ "targets": [ 6,7,8,10,11,12,13,14,15 ],"visible": false,"searchable": false}]}
			  );
	
	
//	if(isFromMenu == true){
//		$('#cover-spin').hide(0);
//		$("#landingpage").find("li:eq(0) a").trigger("click");
//		
//	}else{
	  
	  if(isFromMenu == true){
		  $("#landingpage").find("li:eq(0) a").trigger("click");
	  }else{
		  
		  if(strFnaMgrUnAppDets != null){
				loadMgrUpApprovedRec();
			}
			if(strAdminFNADets != null){
				loadAdminUpApprovedRec();
			}
			
			if(strComplianceFNADets != null){
				loadCompUnApprovedRec();		
			}
			
		  $("#landingpage").find("li:eq(1) a").trigger("click");
		  
		  $('#cover-spin').hide(0);
	  }
		
		
		
		
		if(strALLFNAIds != null){
			loadALLFNAId();
		}
		
//	}
	
	if(strMngrFlg != "Y"){
		$("#appr-mgr-tab").hide();
		$("#appr-mgr-tab-cont").hide();
	}
	if(strAdminFlg != "Y"){		
		$("#appr-admin-tab").hide();
		$("#appr-admin-tab-cont").hide();		
	}
	if(strCompFlg != "Y"){		
		$("#appr-comp-tab").hide();
		$("#appr-comp-tab-cont").hide();		
	}

	$("#spanCrtdUser").text($("#txtFldCrtdUser").val());
	
	
	createPie(".pieID.legend", ".pieID.pie");
	
	
	
	
}

function loadTblValuesFromResultSet(resultSets,ACSLVL){
	if(ACSLVL =="APPROVE"){
		//$("#viewApprovHistory").modal();
		var tblId = document.getElementById("approveList");
	
	var tbody = tblId.tBodies[0]; 
	removeTblRows("approveList");
	
	var rowLen = tbody.rows.length;
	var ResultLength = resultSets.length;
//	alert(ResultLength)
	for(var len=0;len<ResultLength;len++){
		 
		var row = tbody.insertRow(len);
		
		var cell_ = row.insertCell(0);
		
//		poovathi add on 25-02-2020
		cell_.innerHTML = '<input type="radio" name ="" id ="" title="click to view details" readonly="readonly"  style="width:52px;text-align:center; background:none !important;"/>';
		
//	poovathi commented on 25-02-2020
//cell_.innerHTML = '<div class="custom-control custom-radio"><input type="radio" class="custom-control-input" id="123" name="example1"/><label class="custom-control-label" for="123"></label></div>';
		//cell_.innerHTML ='';
		/*$(cell_).find("button").on("click",function(){
			
			
			$('#kycapproval > tbody  > tr').each(function () { $(this).removeClass("focusedRow") });
			$(this).parents("tr").addClass("focusedRow");
			
			var fnaid = ($(this).parents("tr").find("td:eq(3)").text());
			var advisername = ($(this).parents("tr").find("td:eq(1)").text());
			
			$("#myModal").modal('show');
			
			_selectedFnaid = fnaid;
			_selectedFnaAdvName = advisername;
			
			
		})*/
		
		
		var cell0 = row.insertCell(1);
//		cell0.innerHTML = '<input type="text" value="'+(len+1)+'" readOnly="true" class="fpNonEditTblData" style="background:transparent;text-align:center;"><input type="hidden"  value="'+ACSLVL+'" class="fpNonEditTblData" readonly="true" style="background:transparent;text-align:center;"/>';
		cell0.innerHTML = resultSets[len].txtFldFnaId;
		
		var cell1 = row.insertCell(2);
//		cell1.innerHTML = '<input type="text" readOnly="true" class="fpNonEditTblData" style="background:transparent;width:90%">'+
//		'<input type="hidden" name="htxtFldAdvStfId"/>'+
//		'<input type="hidden" name="htxtFldAdvStfEmailId"/>'+
//		'<input type="hidden" name="htxtFldAdvStfMgrId"/>'+
//		'<input type="hidden" name="htxtFldAdvStfMgrName"/>'+
//		'<input type="hidden" name="htxtFldAdvStfMgrEmailId"/>';
		
		cell1.innerHTML =resultSets[len].txtFldCustName;
		
//		+'<img src="images/info.png" onmouseover="showMgrInfo(event,this);" onmouseout="hideTooltip();" style="height:18px;width:18px;vertical-align:middle" />';
//		cell1.childNodes[0].value = resultSets[len].txtFldAdvStfName;
//		cell1.childNodes[1].value = resultSets[len].txtFldAdvStfId;
//		cell1.childNodes[2].value = resultSets[len].txtFldAdvStfEmailId;
//		cell1.childNodes[3].value = resultSets[len].txtFldMgrId;
//		cell1.childNodes[4].value = resultSets[len].txtFldMgrName;
//		cell1.childNodes[5].value = resultSets[len].txtFldMgrEmailId;
		
		
		var cell2 = row.insertCell(3);
//		cell2.innerHTML = '<input type="text" readOnly="true" name="txtFldCustName" class="fpNonEditTblData" style=";" onmouseover="showMgrInfo(event,this,\'icinfo\');" onmouseout="hideTooltip();">'+'<input type="hidden" name="htxtFldCustId"/>'+'<input type="hidden" name="htxtFldCustNric"/>';
//		cell2.childNodes[0].value = resultSets[len].txtFldCustName;
//		cell2.childNodes[1].value = resultSets[len].txtFldCustId;
//		cell2.childNodes[2].value = resultSets[len].txtFldCustFnaIc;
		
		cell2.innerHTML =  resultSets[len].txtFldAdvStfName;
		
		var cell3 = row.insertCell(4);
//		cell3.innerHTML = '<input type="text" readOnly="true" class="fpNonEditTblData" style=";" name="txtFldNTUCPolNum">';
//		cell3.childNodes[0].value = resultSets[len].txtFldNTUCPolNum;
//		cell3.style.display="none";
		
		cell3.innerHTML = resultSets[len].txtFldMgrApproveStatus
		
		var cell4 = row.insertCell(5);
//		cell4.innerHTML = '<input type="text" readOnly="true" class="fpNonEditTblData" style=";" name="txtFldFnaId">'+
//		'<input type="hidden" name="htxtFldMgrStsApprBy"/>'+
//		'<input type="hidden" name="htxtFldMgrStsApprUserId"/>'+
//		'<input type="hidden" name="htxtFldIsPdfOpen"/>'+
//		'<img src="images/expand.png" title="View eKYC" width="18" height="18" onclick="openEKYC(this)"/>';
//		cell4.childNodes[0].value = resultSets[len].txtFldFnaId;
//		cell4.childNodes[1].value = strLoggedAdvStfId;
//		cell4.childNodes[2].value = resultSets[len].txtFldMgrStsApprBy;
		cell4.innerHTML =  resultSets[len].txtFldAdminApproveStatus;
		
		
		var cell5 = row.insertCell(6);

//		if(!isEmpty(resultSets[len].htxtFldNtucCaseId)){
			// For NTUC
//			cell5.innerHTML = "<a href='DownloadFile.action?txtFldNtucPolId="+resultSets[len].txtFldNTUCPolId+"' target=\'_new\' >"+'<img src="images/viewdocument.png" title="View KYC" width="18" height="18" onmouseover=""; onmouseout=""/></a>'+
//			cell5.innerHTML = '<img src="images/viewdocument.png" title="View Policy" width="18" height="18" onclick="downloadAndOpenFile(this)"/>'+
//			'<input type="hidden" name="htxtFldNtucPolId"/>'+
//			'<input type="hidden" name="htxtFldNtucCaseId"/>'+
//			'<input type="hidden" name="htxtFldNtucCasests"/>'+
//			'<input type="hidden" name="htxtFldFnaPolCrtdFlg"/>'+
//			'<input type="hidden" name="htxtFldAdvstfCode"/>'+
//			'<input type="hidden" name="htxtFldFnaPrin"/>'+
//			'<input type="hidden" name="htxtFldFnaClientSign"/>'+
//			'<input type="hidden" name="htxtFldFnaAdviserSign"/>'+
//			'<input type="hidden" name="htxtFldFnaManagerSign"/>'+
//			'<input type="text" class="fpNonEditTblData" style="display:none;text-align:center" value="-N/A-" readOnly="true"/>'+
//			'<img src="images/PDF.png" title="Print FNA" width="18" height="18" onclick="callGenerateFNAPDF(this)"/>';
//			cell5.style.textAlign = "center";
		
		cell5.innerHTML = resultSets[len].txtFldCompApproveStatus;
			
			if(resultSets[len].txtFldFnaPrin == "NON-NTUC"){
//				cell5.childNodes[0].style.display="none";
//				cell5.childNodes[10].style.display="";
			}else{
//				cell5.childNodes[0].style.display="";
//				cell5.childNodes[10].style.display="none";
			}
				
//		}else{
//			//For Non NTUC 
////			cell5.innerHTML = '<img src="images/viewdocument.png" title="View KYC" width="18" height="18" onclick="openFnaReportAsPDF(this)"/>'+
//			cell5.innerHTML = '<img src="images/viewdocument.png" title="View KYC" width="18" height="18" onclick="downloadAndOpenFile(this)"/>'+
//			'<input type="hidden" name="htxtFldNtucPolId"/>';
//			cell5.style.textAlign = "center";		
//		}
		
//		cell5.childNodes[1].value = resultSets[len].txtFldNTUCPolId;
//		cell5.childNodes[2].value = resultSets[len].txtFldFnaNtucCaseId;
//		cell5.childNodes[3].value = resultSets[len].txtFldFnaNtucCasests;
//		cell5.childNodes[4].value = resultSets[len].txtFldFnaPolCrtFlg;
//		cell5.childNodes[5].value = resultSets[len].txtFldAdvStfCode;
//		
//		cell5.childNodes[6].value = resultSets[len].txtFldFnaPrin;
//		cell5.childNodes[7].value = resultSets[len].txtFldFnaClientSign;
//		cell5.childNodes[8].value = resultSets[len].txtFldFnaAdviserSign;
//		cell5.childNodes[9].value = resultSets[len].txtFldFnaManagerSign;
		
//		var cell6 = row.insertCell(6);
//		cell6.innerHTML = '<select style="width:100%;" name="selKycMgrApprSts" class="kycSelect" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" onchange="callManagerApproveStatus(this);"></select>'+
//		'<input type="hidden" name="htxtFldMngrApproveStatus"/>';
		
		//addSelectOption(cell6.childNodes[0]);
//		var mgrapprovestatus = resultSets[len].txtFldMgrApproveStatus;
//		comboMakerByString(STR_FNA_MGRAPPRSTS_LIST,	cell6.childNodes[0],mgrapprovestatus, true);
//		cell6.childNodes[0].value = !isEmpty(mgrapprovestatus) ? mgrapprovestatus: "";
//		cell6.childNodes[1].value = !isEmpty(mgrapprovestatus) ? mgrapprovestatus: "";
		//cell6.childNodes[0].value = mgrapprovestatus;
	
//		var cell7 = row.insertCell(7);
//		cell7.innerHTML = '<input type="text"  class="fpEditTblTxt" name="txtFldMgrApproveRemarks" maxlength="300" style=";">';
//		cell7.childNodes[0].value = resultSets[len].txtFldMgrApproveRemarks;
//		
//		var cell8 = row.insertCell(8);
//		cell8.innerHTML = '<input type="text" readOnly="true" name="txtFldMgrApproveDate" class="fpNonEditTblData" style=";width:80%">'
//			+'<img src="images/info.png" onmouseover="showMgrInfo(event,this,\'mgr\');" onmouseout="hideTooltip();" style="height:18px;width:18px;vertical-align:middle" />';;															
//		cell8.childNodes[0].value = resultSets[len].txtFldMgrApproveDate;
//				
//		var cell9 = row.insertCell(9);
//		cell9.innerHTML = '<select style="width:100%;" name="selKycAdminApprSts" class="kycSelect" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" onchange="callAdminApproveStatus(this);"></select>'+
//		'<input type="hidden" name="htxtFldAdminApproveStatus"/>';
//		
//		var adminapprovestatus = resultSets[len].txtFldAdminApproveStatus;
//		comboMakerByString(STR_FNA_MGRAPPRSTS_LIST,	cell9.childNodes[0],adminapprovestatus, true);
//		//addSelectOption(cell9.childNodes[0]);
//		cell9.childNodes[0].value = !isEmpty(adminapprovestatus) ? adminapprovestatus: "";
//		cell9.childNodes[1].value = !isEmpty(adminapprovestatus) ? adminapprovestatus: "";
//		//cell9.childNodes[0].value = adminapprovestatus;
//		
//		var cell10 = row.insertCell(10);
//		cell10.innerHTML = '<input type="text" class="fpNonEditTblData" name="txtFldAdminApproveRemarks" maxlength="300" style=";">'
//			+'<input type="hidden" name="htxtFldAdminStsApprBy"/>'
//			+'<input type="hidden" name="htxtFldAdminStsApprUserId"/>';
//		cell10.childNodes[0].value = resultSets[len].txtFldAdminApproveRemarks;
//		cell10.childNodes[1].value = strLoggedAdvStfId;
//		cell10.childNodes[2].value = resultSets[len].txtFldAdminStsApprBy;
//	 
//		var cell11 = row.insertCell(11);																																																																																																																									
//		cell11.innerHTML = '<input type="text" class="fpNonEditTblData" readonly="true" style=";width:80%">'
//			+'<img src="images/info.png" onmouseover="showMgrInfo(event,this,\'admin\');" onmouseout="hideTooltip();" style="height:18px;width:18px;vertical-align:middle" />';;
//		cell11.childNodes[0].value = resultSets[len].txtFldAdminApproveDate;
//		
//		var cell12 = row.insertCell(12);
//		cell12.innerHTML = '<select style="width:100%;" name="selKycCompApprSts" class="kycSelect" onmouseover="showTooltipCombo(event,this);" onmouseout="hideTooltip();" onchange="callComplianceApproveStatus(this);"></select>'+
//							'<input type="hidden" name="htxtFldCompApproveStatus"/>'
//		
//		var compapprovestatus = resultSets[len].txtFldCompApproveStatus;
//		comboMakerByString(STR_FNA_MGRAPPRSTS_LIST,	cell12.childNodes[0],compapprovestatus, true);
//		//addSelectOption(cell12.childNodes[0]);
//		cell12.childNodes[0].value = !isEmpty(compapprovestatus) ? compapprovestatus: "";
//		cell12.childNodes[1].value = !isEmpty(compapprovestatus) ? compapprovestatus: "";
//		
//		var cell13 = row.insertCell(13);
//		cell13.innerHTML = '<input type="text"  class="fpNonEditTblData" name="txtFldCompApproveRemarks" maxlength="300" style=";">'
//			+'<input type="hidden" name="htxtFldCompStsApprBy"/>'
//			+'<input type="hidden" name="htxtFldCompStsApprUserId"/>';
//		cell13.childNodes[0].value = resultSets[len].txtFldCompApproveRemarks;
//		cell13.childNodes[1].value = strLoggedAdvStfId;
//		cell13.childNodes[2].value = resultSets[len].txtFldCompStsApprBy;
		
//		var cell14 = row.insertCell(14);
//		cell14.innerHTML = '<input type="text"  class="fpNonEditTblData" name="txtFldCompApproveDate" readonly="true" style=";width:60%">'
//			+'<img src="images/info.png" onmouseover="showMgrInfo(event,this,\'comp\');" onmouseout="hideTooltip();" style="height:18px;width:18px;vertical-align:middle" />'
//			+'<img src="images/policy-pdf.png" style="height:18px;width:18px;vertical-align:middle;cursor:pointer;display:none" title="Download Policy Details" onclick="callPolNoAPI(this)"/>';
//		cell14.childNodes[0].value = resultSets[len].txtFldCompApproveDate;
//			
////		Oct_2018 Always Disabled , as Mgr and Admin Approval is not required anymore
////		 if(!(strMngrFlg.toUpperCase() == "Y")){
//			 cell6.childNodes[0].disabled = true;
//			 cell7.childNodes[0].readOnly = true;
//			 cell8.childNodes[0].readOnly = true;
//		 }
		 
/*		 if(strMngrFlg.toUpperCase() == "Y" && cell6.childNodes[0].value == "APPROVE"){
			 cell6.childNodes[0].disabled = true;
			 cell7.childNodes[0].readOnly = true;
			 cell8.childNodes[0].readOnly = true;
		 }*/
			
//		 var spltArr = strGlblUsrPrvlg.split(",");
//		 var adminFlg=false;
//		 var compFlg=false;
//		 for(var arrlen=0;arrlen<spltArr.length;arrlen++){
//			 if(spltArr[arrlen] == ADMIN_KYC_APPROVAL){
//				 adminFlg = true;
//			 }
//			 
//			 if(spltArr[arrlen] == COMP_KYC_APPROVAL){
//				 compFlg = true;
//			 }
//		 }
//		 
//		 if(!adminFlg){
//			 cell9.childNodes[0].disabled = true;
//			 cell10.childNodes[0].readOnly = true;
//			 cell11.childNodes[0].readOnly = true;
//		 }else{
//			 if(cell9.childNodes[0].value == "APPROVE"){
//				 cell9.childNodes[0].disabled = true;
//				 cell9.childNodes[0].options[1].innerHTML = "APPROVED";
//				 cell10.childNodes[0].readOnly = true;
//				 cell11.childNodes[0].readOnly = true;
//			 }
//		 }
//			Oct_2018 Always Disabled , as Mgr and Admin Approval is not required anymore 
//		 aug_2019 enabled
//		 cell9.childNodes[0].disabled = true;
//		 cell10.childNodes[0].readOnly = true;
//		 cell11.childNodes[0].readOnly = true;

		 
//		 
//		 if(!compFlg){
//			 cell12.childNodes[0].disabled = true;
//			 cell13.childNodes[0].readOnly = true;
//			 cell14.childNodes[0].readOnly = true;
//		 }else{
////			 if(cell12.childNodes[0].value == "APPROVE"){
//			 
//			 if(cell9.childNodes[0].value == "APPROVE"){
//				 
//				 cell12.childNodes[0].disabled = false;
//				 cell13.childNodes[0].readOnly = false;
//				 cell14.childNodes[0].readOnly = false;
//				 
//			 }else{
//				 cell12.childNodes[0].disabled = true;
//				 cell12.childNodes[0].options[1].innerHTML = "APPROVED";
//				 cell13.childNodes[0].readOnly = true;
//				 cell14.childNodes[0].readOnly = true;
//				 
//			 }
//			 
//			 
//			 if(!isEmpty(cell12.childNodes[0].value) && cell12.childNodes[0].value == "APPROVE"){
//				 cell12.childNodes[0].disabled = true;
//				 cell12.childNodes[0].options[1].innerHTML = "APPROVED";
//				 cell13.childNodes[0].readOnly = true;
//				 cell14.childNodes[0].readOnly = true;
//				
//				 if(cell12.childNodes[0].value == "APPROVE" && ntuc_policy_access_flag)
//					 cell14.childNodes[2].style.display = "inline-block";
//			 }
//			 
//		 }
		 
//		 if(strMngrFlg == "Y" && strLoggedAdvStfId ==  resultSets[len].txtFldMgrId){
//			 
//			 if(mgrapprovestatus == "APPROVE"){// && adminapprovestatus == "APPROVE" && compapprovestatus == "APPROVE" 
//				 
//				 cell6.childNodes[0].disabled=true;
//				 cell6.childNodes[0].options[1].innerHTML = "APPROVED";
//				 cell6.childNodes[0].disabled=true;				 
//				 cell7.childNodes[0].readOnly=true;
//				 
//			 }else{
//				 
//				 cell6.childNodes[0].disabled=false;
//				 cell7.childNodes[0].readOnly=false;
//				 
//			 }
//			 
//		 }else{
//			 
//			 if(mgrapprovestatus == "APPROVE"){// && adminapprovestatus == "APPROVE" && compapprovestatus == "APPROVE" 
//				 
//				 cell6.childNodes[0].disabled=true;
//				 cell6.childNodes[0].options[1].innerHTML = "APPROVED";
//				 cell7.childNodes[0].readOnly=true;
//				 
//			 }
//			 
//		 }
		 
		 

//		 if(mgrapprovestatus == "APPROVE"){
//			 cell6.childNodes[0].options[1].innerHTML = "APPROVED";
//			 cell6.childNodes[0].disabled=true;	
//		 }
//		 
//		 if(cell9.childNodes[0].value == "APPROVE"){
//			 cell9.childNodes[0].disabled = true;
//			 cell9.childNodes[0].options[1].innerHTML = "APPROVED";
//			 
//		 }
//		 
//		 if(cell12.childNodes[0].value == "APPROVE"){
//			 cell12.childNodes[0].disabled = true;
//			 cell12.childNodes[0].options[1].innerHTML = "APPROVED";
//		 }
//		 
//		 
////		 if(ACSLVL=="M"){row.style.backgroundColor = "#99aac6";}
////		 if(ACSLVL=="A"){row.style.backgroundColor = "#d3d6a7";}
////		 if(ACSLVL=="C"){row.style.backgroundColor = "#e0c7d0";}
//		 
//		 row.onclick = function() {
//				selectSingleRow(this);
//			};
		 
		 
	}
}
	if(ACSLVL== "PENDING"){
		
var tblId = document.getElementById("pendingList");
		
		var tbody = tblId.tBodies[0]; 
		removeTblRows("pendingList");
		
		var rowLen = tbody.rows.length;
		var ResultLength = resultSets.length;

		for(var len=0;len<ResultLength;len++){
			 
			var row = tbody.insertRow(len);
			
			var cell_ = row.insertCell(0);
			
//			poovathi add on 25-02-2020
			cell_.innerHTML = '<input type="radio" name ="" id ="" title="click to view details" readonly="readonly"  style="width:52px;text-align:center; background:none !important;"/>';
			

			
			
			var cell0 = row.insertCell(1);

			cell0.innerHTML = resultSets[len].txtFldFnaId;
			
			var cell1 = row.insertCell(2);

			
			cell1.innerHTML =resultSets[len].txtFldCustName;
			

			
			
			var cell2 = row.insertCell(3);

			
			cell2.innerHTML =  resultSets[len].txtFldAdvStfName;
			
			var cell3 = row.insertCell(4);

			
			cell3.innerHTML = resultSets[len].txtFldMgrApproveStatus
			
			var cell4 = row.insertCell(5);

			cell4.innerHTML =  resultSets[len].txtFldAdminApproveStatus;
			
			
			var cell5 = row.insertCell(6);

			
			cell5.innerHTML = resultSets[len].txtFldCompApproveStatus;
				
				if(resultSets[len].txtFldFnaPrin == "NON-NTUC"){
//					cell5.childNodes[0].style.display="none";
//					cell5.childNodes[10].style.display="";
				}else{
//					cell5.childNodes[0].style.display="";
//					cell5.childNodes[10].style.display="none";
				}
					
           }
		
			 
			 
		}
	
if(ACSLVL == "REJECT"){
		
		var tblId = document.getElementById("rejectList");
		
		var tbody = tblId.tBodies[0]; 
		removeTblRows("rejectList");
		
		var rowLen = tbody.rows.length;
		var ResultLength = resultSets.length;

		for(var len=0;len<ResultLength;len++){
			 
			var row = tbody.insertRow(len);
			
			var cell_ = row.insertCell(0);
			
//			poovathi add on 25-02-2020
			cell_.innerHTML = '<input type="radio" name ="" id ="" title="click to view details" readonly="readonly"  style="width:52px;text-align:center; background:none !important;"/>';
			

			
			
			var cell0 = row.insertCell(1);

			cell0.innerHTML = resultSets[len].txtFldFnaId;
			
			var cell1 = row.insertCell(2);

			
			cell1.innerHTML =resultSets[len].txtFldCustName;
			

			
			
			var cell2 = row.insertCell(3);

			
			cell2.innerHTML =  resultSets[len].txtFldAdvStfName;
			
			var cell3 = row.insertCell(4);

			
			cell3.innerHTML = resultSets[len].txtFldMgrApproveStatus
			
			var cell4 = row.insertCell(5);

			cell4.innerHTML =  resultSets[len].txtFldAdminApproveStatus;
			
			
			var cell5 = row.insertCell(6);

			
			cell5.innerHTML = resultSets[len].txtFldCompApproveStatus;
				
				if(resultSets[len].txtFldFnaPrin == "NON-NTUC"){
//					cell5.childNodes[0].style.display="none";
//					cell5.childNodes[10].style.display="";
				}else{
//					cell5.childNodes[0].style.display="";
//					cell5.childNodes[10].style.display="none";
				}
		 
		}
		
	
	
}
		
	}

function callManagerApproveStatus(selObj){
	
	if(!isEmpty(selObj.value)){
		
		setTimeout(function() { loaderBlock() ;}, 0);
		setTimeout(function() { managerApproveStatusAppr(selObj,false,true); }, 1000);	
		
		
	}
	
}

function callAdminApproveStatus(selObj){
	if(!isEmpty(selObj.value)){
		setTimeout(function() { loaderBlock(); }, 0);
		setTimeout(function() { adminApproveStatus(selObj) ;}, 1000);
	}
}

function callComplianceApproveStatus(selObj){
	
//	ModalPopups.uploadKycApproveDoc("KycApprovalDoc",{width:410,onOk:""/*,onCancel:"ModalPopupsCancelKycDoc()"*/});
//	
//	$("#KycApprovalDoc_okButton").click(function(){
//		kycCompApproveDocBinary(); //base64 code
//		
//		if(!isEmpty(selObj.value)){
//			setTimeout(function() { loaderBlock() ;}, 0);
//			setTimeout(function() { callCompilanceDetails(selObj) ;}, 1000);		
//		}
//	});
	
	
	if(!isEmpty(selObj.value)){
		setTimeout(function() { loaderBlock(); }, 0);
		setTimeout(function() { callCompilanceDetails(selObj) ;}, 1000);
	}
	
	;
	
}

function loaderBlock(){
	
	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");
	
	loadMsg.style.display="block";
	//alert(loadMsg.style.display)
	loadingLayer.style.display="block"; 
}
 

function managerApproveStatusAppr(selObj,$dialog,confirmmsgflag){

	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex-2;
	

 	
	if(rowIndex>=0 && !isEmpty(selObj.value)){
		
		
		/*var isPdfOpen = tbody.rows[rowIndex].cells[4].childNodes[3].value;
		if(isPdfOpen != "Y"){
			alert("Please ensure you have verified all the pages in the FNA before your approval.");
			selObj.value= "";
			
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display="none";
			
			return false;
		}*///aug_2019
		
		var fnaId = tbody.rows[rowIndex].cells[4].childNodes[0].value;
//		var mangrEmailId = strLoggedUserEmailId;
		var remarks = tbody.rows[rowIndex].cells[7].childNodes[0].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var polNo = tbody.rows[rowIndex].cells[3].childNodes[0].value;
		var advName = tbody.rows[rowIndex].cells[1].childNodes[0].value;
		var custName = tbody.rows[rowIndex].cells[2].childNodes[0].value;
		var advstfEmailId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var mangrId  = tbody.rows[rowIndex].cells[1].childNodes[3].value;
		var mangrEmailId = tbody.rows[rowIndex].cells[1].childNodes[5].value;
		
		var approverAdvStfId = tbody.rows[rowIndex].cells[4].childNodes[1].value;
		var approverUserId = tbody.rows[rowIndex].cells[4].childNodes[2].value;
		
		var fnaPrin = tbody.rows[rowIndex].cells[5].childNodes[6].value ;
		var fnaClientSigned = tbody.rows[rowIndex].cells[5].childNodes[7].value ;
		var fnaAdviserSigned = tbody.rows[rowIndex].cells[5].childNodes[8].value ;
		var fnaManagerSigned = tbody.rows[rowIndex].cells[5].childNodes[9].value ;
		
		if(isEmpty(fnaClientSigned)){
			alert(CLIENT_SIGN_B4_NONDSF);
			return false;
		}
		
		if(isEmpty(fnaAdviserSigned)){
			alert(CLIENT_SIGN_B4_NONDSF);
			return false;
		}
		
		
		

		if(selObj.value == "REJECT" && isEmpty(remarks)){
			
			alert(MANAGER_KEYIN_REMARKS);
			
			selObj.value=tbody.rows[rowIndex].cells[6].childNodes[1].value;
			if(tbody.rows[rowIndex].cells[6].childNodes[1].value == undefined || 
					isEmpty(tbody.rows[rowIndex].cells[6].childNodes[1].value)){
				selObj.value= "";
			}
			tbody.rows[rowIndex].cells[7].childNodes[0].focus();
			
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display="none";	
			
			
			return false;
		}
		
		if(selObj.value == "APPROVE" ){
			
			
			if(isEmpty(fnaManagerSigned)){
				
				alert("Please ensure you have verified all the pages in the FNA before your approval. !");
				
				openEKYC(selObj);
				
				var loadMsg = document.getElementById("loadingMessage");
//				var loadingLayer = document.getElementById("loadingLayer");
				loadMsg.style.display="none";
				
				return false;
			}
			if(confirmmsgflag){
				var conf = window.confirm(CONFIRM_APPROVE_MANAGER);
				if(!conf){
					var loadMsg = document.getElementById("loadingMessage");
					var loadingLayer = document.getElementById("loadingLayer");
					loadMsg.style.display="none";
					selObj.value = "";
					return false;
				}
			}
			
		}
		
		if(selObj.value == "REJECT"  && confirmmsgflag){
			var conf = window.confirm(CONFIRM_REJECT_MANAGER);
			if(!conf){
				var loadMsg = document.getElementById("loadingMessage");
				var loadingLayer = document.getElementById("loadingLayer");
				loadMsg.style.display="none";
				selObj.value = "";
				return false;
			}
		}
		
		
		

		
		
		var ajaxParam = "dbcall=MANAGER_APPROVE_STATUS"
		+"&txtFldFnaId="+fnaId
		
		+"&txtFldMgrAppStatus="+selObj.value
		+"&txtFldManagerEmailId="+mangrEmailId
		+"&txtFldManagerId="+mangrId
		+"&txtFldManagerRemarks="+remarks
		
		+"&hNTUCPolicyId="+hNTUCPolicyId
		+"&txtFldPolNum="+polNo
		
		+"&txtFldAdvEmailId="+advstfEmailId
		+"&txtFldAdviserName="+advName
		+"&txtFldCustName="+custName		
		 
		+"&txtFldApprAdvId="+approverAdvStfId
		+"&txtFldApprUserId="+approverUserId
		+"&txtFldApprUserEmailId="+strLoggedUserEmailId
		+"&txtFldFnaPrin="+fnaPrin
		;
		
		
		
		
		var ajaxResponse = callAjax(ajaxParam,false);
				
		retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		 
		
		for ( var val in retval) {
			
			var tabdets = retval[val];
			
			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];
					
					
					if (tab == "TAB_MANAGER_APPROVE_DETAILS") {
						//tbody.rows[rowIndex].cells[5].childNodes[0].value  = value[0].MANAGER_KYC_SENT_STATUS;
						tbody.rows[rowIndex].cells[6].childNodes[0].value  = value[0].MANAGER_APPROVE_STATUS;
						tbody.rows[rowIndex].cells[7].childNodes[0].value  = value[0].MANAGER_APPROVE_REMARKS;
						tbody.rows[rowIndex].cells[8].childNodes[0].value  = currDate;
						//tbody.rows[rowIndex].cells[5].childNodes[0].disabled = true;
						
						//FOR BOTH APPROVE AND REJECT FIELD IS DISABLED
//						if(tbody.rows[rowIndex].cells[6].childNodes[0].value == "APPROVE"){
							tbody.rows[rowIndex].cells[6].childNodes[0].disabled = true;
							tbody.rows[rowIndex].cells[7].childNodes[0].readOnly = true;
							tbody.rows[rowIndex].cells[8].childNodes[0].readOnly = true;
//						}
							
							if(tbody.rows[rowIndex].cells[6].childNodes[0].value == "APPROVE"){
								tbody.rows[rowIndex].cells[6].childNodes[0].options[1].innerHTML = "APPROVED";
							}
						
						
						alert(value[0].MANAGER_POLICY_STATUS);
						
						 
					}
				}
			}
		}
	
		//selObj.disabled = true;
		tbody.rows[rowIndex].cells[8].childNodes[0].value = currDate; 
		
		getAllColumnData(selObj);
		

		
	}
	
	
	
}

function adminApproveStatus(selObj){
	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex-2;
	
	
	if(rowIndex>=0 && !isEmpty(selObj.value)){
		var mngrStatus = tbody.rows[rowIndex].cells[6].childNodes[0].value;
		if(!(mngrStatus == "APPROVE")){
			alert(MANAGER_APPROVESTATUS);
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display="none";
			selObj.value="";
			return false;
		}
		
		 
		var fnaId = tbody.rows[rowIndex].cells[4].childNodes[0].value;
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = tbody.rows[rowIndex].cells[10].childNodes[0].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var polNo = tbody.rows[rowIndex].cells[3].childNodes[0].value;
		var advName = tbody.rows[rowIndex].cells[1].childNodes[0].value;
		var custName = tbody.rows[rowIndex].cells[2].childNodes[0].value;
		var custId = tbody.rows[rowIndex].cells[2].childNodes[1].value;
		var advstfEmailId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		
		var approverAdvStfId = tbody.rows[rowIndex].cells[10].childNodes[1].value;
		var approverUserId = tbody.rows[rowIndex].cells[10].childNodes[2].value;
		
		var fnaPrin = tbody.rows[rowIndex].cells[5].childNodes[6].value ;
		var fnaClientSigned = tbody.rows[rowIndex].cells[5].childNodes[7].value ;
		var fnaAdviserSigned = tbody.rows[rowIndex].cells[5].childNodes[8].value ;
		var fnaManagerSigned = tbody.rows[rowIndex].cells[5].childNodes[9].value ;


		if(selObj.value == "REJECT" && isEmpty(remarks)){
			alert(ADMIN_KEYIN_REMARKS);
			selObj.value=tbody.rows[rowIndex].cells[9].childNodes[1].value;
			if(tbody.rows[rowIndex].cells[9].childNodes[1].value == undefined || 
					isEmpty(tbody.rows[rowIndex].cells[9].childNodes[1].value)){
				selObj.value= "";
			}
			tbody.rows[rowIndex].cells[10].childNodes[0].focus();
			var loadMsg = document.getElementById("loadingMessage");
			loadMsg.style.display="none";
			return false;
		}
		
		if(selObj.value == "REJECT"){
			var conf = window.confirm(ADMIN_REJECTSTATUS_CONFIRM);
			if(!conf){
				var loadMsg = document.getElementById("loadingMessage");
				loadMsg.style.display="none";
				selObj.value = "";
				return false;
			}
		}else if(selObj.value == "APPROVE"){
			var conf = window.confirm(ADMIN_APPROVESTATUS_CONFIRM);
			if(!conf){
				var loadMsg = document.getElementById("loadingMessage");
				loadMsg.style.display="none";
				selObj.value = "";
				return false;
			}
		}
		
		
		
		
		var ajaxParam = 'dbcall=ADMIN_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldAdminAppStatus="+selObj.value+
		"&txtFldManagerEmailId="+mangrEmailId+"&txtFldAdminRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
		"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
		+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldFnaPrin="+fnaPrin+"&txtFldCustId="+custId;
		
		var ajaxResponse = callAjax(ajaxParam,false);
		retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		
		for ( var val in retval) {
			
			var tabdets = retval[val];
			
			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];
					
					 
					if (tab == "TAB_ADMIN_APPROVE_DETAILS") {
						//tbody.rows[rowIndex].cells[13].childNodes[0].value  = value[0].ADMIN_KYC_SENT_STATUS;
						tbody.rows[rowIndex].cells[9].childNodes[0].value  = value[0].ADMIN_APPROVE_STATUS;
						tbody.rows[rowIndex].cells[10].childNodes[0].value  = value[0].ADMIN_APPROVE_REMARKS;
						tbody.rows[rowIndex].cells[11].childNodes[0].value  = currDate;
						
//						FOR BOTH APPROVE AND REJECT , FIELD IS DISABLED 
//						if(tbody.rows[rowIndex].cells[9].childNodes[0].value == "APPROVE"){
							tbody.rows[rowIndex].cells[9].childNodes[0].disabled = true;
							tbody.rows[rowIndex].cells[10].childNodes[0].readOnly = true;
							tbody.rows[rowIndex].cells[11].childNodes[0].readOnly = true;
//						}
							
						if(value[0].ADMIN_APPROVE_STATUS == "APPROVE"){
							
							
							 var spltArr = strGlblUsrPrvlg.split(",");
							 var adminFlg=false;
							 var compFlg=false;
							 for(var arrlen=0;arrlen<spltArr.length;arrlen++){
								 if(spltArr[arrlen] == ADMIN_KYC_APPROVAL){
									 adminFlg = true;
								 }
								 
								 if(spltArr[arrlen] == COMP_KYC_APPROVAL){
									 compFlg = true;
								 }
							 }
							 
							 if(adminFlg && compFlg){
								tbody.rows[rowIndex].cells[12].childNodes[0].disabled = false;
								tbody.rows[rowIndex].cells[13].childNodes[0].readOnly = false;
							 }
							
						}
						
						if(tbody.rows[rowIndex].cells[9].childNodes[0].value == "APPROVE"){
							tbody.rows[rowIndex].cells[9].childNodes[0].options[1].innerHTML = "APPROVED";
						}
						
						
						
						
						alert(value[0].ADMIN_POLICY_STATUS);
					}
				}	
			}
		}
	
		//selObj.disabled = true;
		
		tbody.rows[rowIndex].cells[11].childNodes[0].value = currDate;
		getAllColumnData(selObj);
		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");
		loadMsg.style.display="none";
	
		
	}
	
}



function callCompilanceDetails(selObj){ 
	
	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;

	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex-2;

	
	if(rowIndex>=0 && !isEmpty(selObj.value)){
		
	 
		
		var fnaId = tbody.rows[rowIndex].cells[4].childNodes[0].value;
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = tbody.rows[rowIndex].cells[13].childNodes[0].value;
		
		var ntucPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var ntucCaseId = tbody.rows[rowIndex].cells[5].childNodes[2].value;
		var ntucCasests = tbody.rows[rowIndex].cells[5].childNodes[3].value;
		var ntucPolCrtdFlg = tbody.rows[rowIndex].cells[5].childNodes[4].value;
		
		var polNo = tbody.rows[rowIndex].cells[3].childNodes[0].value;
		var advName = tbody.rows[rowIndex].cells[1].childNodes[0].value;
		var custName = tbody.rows[rowIndex].cells[2].childNodes[0].value;
		var advstfEmailId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var hCompApproveStatus = tbody.rows[rowIndex].cells[12].childNodes[1].value;
		
		var approverAdvStfId = tbody.rows[rowIndex].cells[13].childNodes[1].value;
		var approverUserId = tbody.rows[rowIndex].cells[13].childNodes[2].value;
		
		
		if(selObj.value == "REJECT" && isEmpty(remarks)){
			alert(COMPLIANCE_KEYIN_REMARKS);
			selObj.value="";
			tbody.rows[rowIndex].cells[13].childNodes[0].focus();
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display="none";
			return false;
		} 
		
		var adminStatus = tbody.rows[rowIndex].cells[9].childNodes[0].value;
		
		
		/*
//		 Oct_2018
		if(!(adminStatus == "APPROVE")){
			alert(ADMIN_APPROVE_STATUS);
			selObj.value=tbody.rows[rowIndex].cells[12].childNodes[1].value;
			if(tbody.rows[rowIndex].cells[12].childNodes[1].value == undefined){
				selObj.value= "";
			}
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display="none";
			return false;
		}
		*/
	 
		
//		 if(!isEmpty(hNTUCPolicyId) && (hCompApproveStatus !="APPROVE")){
		 if(!isEmpty(ntucCaseId) && (hCompApproveStatus !="APPROVE")){
			 
		 	//callNTUCWS(selObj);
			compApproveStatus(selObj);
		}else{
			
			compApproveStatus(selObj);
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display="none";
			return false;
		}
	}
	
		
}


function compApproveStatus(selObj){
	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex-2;
	
	/*var conf = window.confirm("After Changing Approve Status is not allowed to change. Do You Want to Continue");
	if(!conf){
		return false;
	}*/
	
	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");
	
 	
	if(rowIndex>=0 && !isEmpty(selObj.value)){
		 
		
		var fnaId = tbody.rows[rowIndex].cells[4].childNodes[0].value;
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = tbody.rows[rowIndex].cells[13].childNodes[0].value;
		var ntucPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var ntucCaseId = tbody.rows[rowIndex].cells[5].childNodes[2].value;
		var ntucCasests = tbody.rows[rowIndex].cells[5].childNodes[3].value;
		var ntucPolCrtdFlg = tbody.rows[rowIndex].cells[5].childNodes[4].value;
		var advstfcode = tbody.rows[rowIndex].cells[5].childNodes[5].value;
		
		var polNo = tbody.rows[rowIndex].cells[3].childNodes[0].value;
		var advName = tbody.rows[rowIndex].cells[1].childNodes[0].value;
		var custName = tbody.rows[rowIndex].cells[2].childNodes[0].value;
		var advstfId = tbody.rows[rowIndex].cells[1].childNodes[1].value;
		var advstfEmailId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		
		var custId = tbody.rows[rowIndex].cells[2].childNodes[1].value;
		var custNRIC = tbody.rows[rowIndex].cells[2].childNodes[2].value;
		
		var approverAdvStfId = tbody.rows[rowIndex].cells[13].childNodes[1].value;
		var approverUserId = tbody.rows[rowIndex].cells[13].childNodes[2].value;
//		var advstfcode="";
		
		var fnaPrin = tbody.rows[rowIndex].cells[5].childNodes[6].value ;
		var fnaClientSigned = tbody.rows[rowIndex].cells[5].childNodes[7].value ;
		var fnaAdviserSigned = tbody.rows[rowIndex].cells[5].childNodes[8].value ;
		var fnaManagerSigned = tbody.rows[rowIndex].cells[5].childNodes[9].value ;

		
		if(selObj.value == "REJECT" && isEmpty(remarks)){
			alert(COMPLIANCE_KEYIN_REMARKS);
			selObj.value="";
			tbody.rows[rowIndex].cells[13].childNodes[0].focus();
			loadMsg.style.display="none";
			return false;
		}
		
//		var loadMsg = document.getElementById("loadingMessage");
//		var loadingLayer = document.getElementById("loadingLayer");
		
//		setTimeout(function() { loaderBlock() }, 15);
		
		var jsontext = formJsonText(fnaId,advstfcode);
		
		if(selObj.value == "APPROVE" ){
			
			if(fnaPrin == "NTUC"){
				
				ModalPopups.uploadKycApproveDoc("KycApprovalDoc",{width:410,onOk:"",onCancel:""});
				
				$("#KycApprovalDoc_cancelButton").click(function(){
					ModalPopupsCancelKycDoc(selObj);
				});
				
				
				$("#KycApprovalDoc_okButton").click(function(){
					
					var file = document.getElementById("fileKycCompApprove").value;
					
					if(isEmpty(file)){
						alert("Select the document");
						return false;
					}else{
						
						
						var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
						selObj.value+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
						"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
						+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+ntucCaseId
						+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
						+"&CASEID="+ntucCaseId+"&CASESTATUS="+selObj.value+"&ADVSTFCODE="+advstfcode
						+"&FNAID="+fnaId+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
						+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin+"&KYCJSON="+jsontext
						console.log(ajaxParam)
						
						var form = $('#hdnFormKycApprDoc')[0];
					    var data = new FormData(form);
					    
//					    alert(ajaxParam)
					    
					    loadMsg.style.display="block";
					    //alert(loadMsg.style.display)
					    loadingLayer.style.display="block"; 
					    setTimeout(function() { loaderBlock(); }, 150);
					    
					    
					    $.ajax({
					        url : "KYCServlet?"+ajaxParam,
					        type: "POST",
					        data :data,
					        cache: false,
					        enctype: 'multipart/form-data',
					        contentType: false,
					        processData: false
					    }).done(function(response){ 
					    	
//					    	var saveResp=$.parseJSON(response);
//							  
//							  if(saveResp[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
//								  window.location = baseUrl + SESSION_EXP_JSP;
//								  return;
//							  }
//							  
//							  if(saveResp[0].STATUS=='SUCCESS'){
//								  
//								  ModalPopupsCancelKycDoc();
//							  }
//							  
//							  if(saveResp[0].STATUS=='FAIL'){
//								  alert("Failed to convert to base64.");
//							  }
//					    	alert(response)
					    	
					    	loadMsg.style.display="none";	 
					    	
					    	var retval =  $.parseJSON(response);
							
							for ( var val in retval) {
								
								var tabdets = retval[val];
								
								if (tabdets["SESSION_EXPIRY"]) {
									window.location = baseUrl + SESSION_EXP_JSP;
									return;
								}
								for ( var tab in tabdets) {
									if (tabdets.hasOwnProperty(tab)) {
										var value = tabdets[tab];
										
//										alert(tab +","+value[0]);
										 
										if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
//											alert(value)
											//tbody.rows[rowIndex].cells[17].childNodes[0].value  = value[0].COMP_KYC_SENT_STATUS;
											tbody.rows[rowIndex].cells[12].childNodes[0].value  = value[0].COMP_APPROVE_STATUS;
											tbody.rows[rowIndex].cells[12].childNodes[1].value  = value[0].COMP_APPROVE_STATUS;
											tbody.rows[rowIndex].cells[13].childNodes[0].value  = value[0].COMP_APPROVE_REMARKS;
											tbody.rows[rowIndex].cells[14].childNodes[0].value  = currDate;
											
											if(tbody.rows[rowIndex].cells[12].childNodes[0].value == "APPROVE"){
												tbody.rows[rowIndex].cells[12].childNodes[0].disabled = true;
												tbody.rows[rowIndex].cells[13].childNodes[0].readOnly = true;
												tbody.rows[rowIndex].cells[14].childNodes[0].readOnly = true;
												tbody.rows[rowIndex].cells[14].childNodes[2].style.display = "inline-block";
											}
											
											
											alert(value[0].COMP_POLICY_STATUS);
											
										}
										
										if(tab == "UPDATEKYC2_STATUS"){
											alert(value);
										}
										
										
										if(tab == "UPDATEKYC2_DSF_STATUS" ){
//											alert(value.toLowerCase())
											if(value.toLowerCase() == "success"){
											
												tbody.rows[rowIndex].cells[12].childNodes[0].disabled = true;
												tbody.rows[rowIndex].cells[13].childNodes[0].readOnly = true;
												tbody.rows[rowIndex].cells[14].childNodes[0].readOnly = true;
												
												getAllColumnData(selObj);
//												var loadMsg = document.getElementById("loadingMessage");
//												var loadingLayer = document.getElementById("loadingLayer");
												loadMsg.style.display="none";
											}else if(value.toLowerCase() == "failure"){
												
												tbody.rows[rowIndex].cells[12].childNodes[0].disabled = false;
												tbody.rows[rowIndex].cells[13].childNodes[0].readOnly = false;
												tbody.rows[rowIndex].cells[14].childNodes[0].readOnly = false;
												
												
												tbody.rows[rowIndex].cells[12].childNodes[0].value  = "";
												tbody.rows[rowIndex].cells[12].childNodes[1].value  = "";
												tbody.rows[rowIndex].cells[13].childNodes[0].value  = "";
												tbody.rows[rowIndex].cells[14].childNodes[0].value  = "";

												
											}
											
										}
										
										
										if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
											alert(value);
											
										}
									}
								}
							}
					    });
						
					}
					
				
				    
				    ModalPopupsCancelKycDoc(selObj);
					
					
				});
				
				
				
			}else{
				

				
				var conf = window.confirm(COMPLIANCE_APPROVESTATUS_CONFIRM);
				if(!conf){
					var loadMsg = document.getElementById("loadingMessage");
					var loadingLayer = document.getElementById("loadingLayer");
					loadMsg.style.display="none";
					selObj.value = "";
					return false;
				}
				
				var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
				selObj.value+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
				"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
				+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+ntucCaseId
				+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
				+"&CASEID="+ntucCaseId+"&CASESTATUS="+selObj.value+"&ADVSTFCODE="+advstfcode
				+"&FNAID="+fnaId+"&KYCJSON="+jsontext+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
				+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin;
				
				
				console.log(ajaxParam);
				
//				var form = $('#hdnFormKycApprDoc')[0];
//			    var data = new FormData(form);
			    
//			    alert(ajaxParam)
			    
			    loadMsg.style.display="block";
			    //alert(loadMsg.style.display)
			    loadingLayer.style.display="block"; 
			    setTimeout(function() { loaderBlock() }, 1000);
			    
			    
			    $.ajax({
			        url : "KYCServlet?"+ajaxParam,
			        type: "POST",
//			        data :data,
			        cache: false,
//			        enctype: 'multipart/form-data',
			        contentType: false,
			        processData: false
			    }).done(function(response){ 
			    	
			    	var loadMsgs = document.getElementById("loadingMessage");
					var loadingLayers = document.getElementById("loadingLayer");
					
					loadMsgs.style.display="none";	 
			    	
			    	var retval =  $.parseJSON(response);
					
					for ( var val in retval) {
						
						var tabdets = retval[val];
						
						if (tabdets["SESSION_EXPIRY"]) {
							window.location = baseUrl + SESSION_EXP_JSP;
							return;
						}
						for ( var tab in tabdets) {
							if (tabdets.hasOwnProperty(tab)) {
								var value = tabdets[tab];
								
//								alert(tab +","+value[0]);
								 
								if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
//									alert(value)
									//tbody.rows[rowIndex].cells[17].childNodes[0].value  = value[0].COMP_KYC_SENT_STATUS;
									tbody.rows[rowIndex].cells[12].childNodes[0].value  = value[0].COMP_APPROVE_STATUS;
									tbody.rows[rowIndex].cells[12].childNodes[1].value  = value[0].COMP_APPROVE_STATUS;
									tbody.rows[rowIndex].cells[13].childNodes[0].value  = value[0].COMP_APPROVE_REMARKS;
									tbody.rows[rowIndex].cells[14].childNodes[0].value  = currDate;
									
									if(tbody.rows[rowIndex].cells[12].childNodes[0].value == "APPROVE"){
										tbody.rows[rowIndex].cells[12].childNodes[0].disabled = true;
										tbody.rows[rowIndex].cells[13].childNodes[0].readOnly = true;
										tbody.rows[rowIndex].cells[14].childNodes[0].readOnly = true;
									}
									
									if(tbody.rows[rowIndex].cells[12].childNodes[0].value == "APPROVE"){
										tbody.rows[rowIndex].cells[12].childNodes[0].options[1].innerHTML = "APPROVED";
									}
									
									alert(value[0].COMP_POLICY_STATUS);
									
								}
								
								
								
								
								
								if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
									alert(value);
									
								}
							}
						}
					}
			    });
				
			
				
				
				
			
				
			}
			
			
			
			
			
			
		}else if(selObj.value == "REJECT" ){
			
			
			
			var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
			selObj.value+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
			"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
			+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+ntucCaseId
			+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
			+"&CASEID="+ntucCaseId+"&CASESTATUS="+selObj.value+"&ADVSTFCODE="+advstfcode
			+"&FNAID="+fnaId+"&KYCJSON="+jsontext+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
			+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin;
			
			
			console.log(ajaxParam);
			
//			var form = $('#hdnFormKycApprDoc')[0];
//		    var data = new FormData(form);
		    
//		    alert(ajaxParam)
		    
		    loadMsg.style.display="block";
		    //alert(loadMsg.style.display)
		    loadingLayer.style.display="block"; 
		    setTimeout(function() { loaderBlock() }, 150);
		    
		    
		    $.ajax({
		        url : "KYCServlet?"+ajaxParam,
		        type: "POST",
//		        data :data,
		        cache: false,
//		        enctype: 'multipart/form-data',
		        contentType: false,
		        processData: false,
		        // async:false
		    }).done(function(response){ 
		    	
		    	var loadMsgs = document.getElementById("loadingMessage");
				var loadingLayers = document.getElementById("loadingLayer");
				
				loadMsgs.style.display="none";	 
		    	
		    	var retval =  $.parseJSON(response);
				
				for ( var val in retval) {
					
					var tabdets = retval[val];
					
					if (tabdets["SESSION_EXPIRY"]) {
						window.location = baseUrl + SESSION_EXP_JSP;
						return;
					}
					for ( var tab in tabdets) {
						if (tabdets.hasOwnProperty(tab)) {
							var value = tabdets[tab];
							
//							alert(tab +","+value[0]);
							 
							if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
//								alert(value)
								//tbody.rows[rowIndex].cells[17].childNodes[0].value  = value[0].COMP_KYC_SENT_STATUS;
								tbody.rows[rowIndex].cells[12].childNodes[0].value  = value[0].COMP_APPROVE_STATUS;
								tbody.rows[rowIndex].cells[12].childNodes[1].value  = value[0].COMP_APPROVE_STATUS;
								tbody.rows[rowIndex].cells[13].childNodes[0].value  = value[0].COMP_APPROVE_REMARKS;
								tbody.rows[rowIndex].cells[14].childNodes[0].value  = currDate;
								
								if(tbody.rows[rowIndex].cells[12].childNodes[0].value == "APPROVE"){
									tbody.rows[rowIndex].cells[12].childNodes[0].disabled = true;
									tbody.rows[rowIndex].cells[13].childNodes[0].readOnly = true;
									tbody.rows[rowIndex].cells[14].childNodes[0].readOnly = true;
								}
								
								
								alert(value[0].COMP_POLICY_STATUS);
								
							}
							
							if(tab == "UPDATEKYC2_STATUS"){
								alert(value);
							}
							
							if(tab == "UPDATEKYC2_DSF_STATUS" ){
//								alert(value.toLowerCase())
								if(value.toLowerCase() == "success"){
								
									tbody.rows[rowIndex].cells[12].childNodes[0].disabled = true;
									tbody.rows[rowIndex].cells[13].childNodes[0].readOnly = true;
									tbody.rows[rowIndex].cells[14].childNodes[0].readOnly = true;
									
									getAllColumnData(selObj);
									loadMsgs.style.display="none";
								}else if(value.toLowerCase() == "failure"){
									
									tbody.rows[rowIndex].cells[12].childNodes[0].disabled = false;
									tbody.rows[rowIndex].cells[13].childNodes[0].readOnly = false;
									tbody.rows[rowIndex].cells[14].childNodes[0].readOnly = false;
									
									
									tbody.rows[rowIndex].cells[12].childNodes[0].value  = "";
									tbody.rows[rowIndex].cells[12].childNodes[1].value  = "";
									tbody.rows[rowIndex].cells[13].childNodes[0].value  = "";
									tbody.rows[rowIndex].cells[14].childNodes[0].value  = "";

									
								}
								
							}
							
							if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
								alert(value);
								
							}
						}
					}
				}
		    });
			
		
			
			
			
		}
		
		
		
		
	
		
		
		
//		var ajaxResponse = callAjax(ajaxParam,false);
		
		//getAllColumnData(selObj); will be added in model popup 1/11/2018
	//	selObj.disabled = true;
		tbody.rows[rowIndex].cells[14].childNodes[0].value = currDate; 
//		var loadMsg = document.getElementById("loadingMessage");
//		var loadingLayer = document.getElementById("loadingLayer");
		loadMsg.style.display="none";
	
		
	}
	
}


function callNTUCWS(selObj){ 

	var TblId = selObj.parentNode.parentNode.parentNode.parentNode.id;
	var tbody = document.getElementById(TblId).tBodies[0];
	var rowIndex = selObj.parentNode.parentNode.rowIndex-2;
	 
	
	if(selObj.value == "APPROVE"){
		var conf = window.confirm(COMPLIANCE_APPROVESTATUS_CONFIRM);
		if(!conf){
			var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			loadMsg.style.display="none";
			selObj.value = "";
			return false;
		}
	}
	
	
	if(rowIndex>=0 && !isEmpty(selObj.value)){
		
		
		
		var fnaId = tbody.rows[rowIndex].cells[4].childNodes[0].value;
		var ntucPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var approvalRemark = tbody.rows[rowIndex].cells[13].childNodes[0].value;
		var approvalDate = tbody.rows[rowIndex].cells[14].childNodes[0].value;
		var approvalStatus = selObj.value;
		//var ntucPolicyNum = tbody.rows[rowIndex].cells[2].childNodes[1].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var hCompApproveStatus = tbody.rows[rowIndex].cells[12].childNodes[1].value;
		
	/*	var fnaId = tbody.rows[rowIndex].cells[4].childNodes[0].value;
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = tbody.rows[rowIndex].cells[13].childNodes[0].value;
		var ntucPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var hNTUCPolicyId = tbody.rows[rowIndex].cells[5].childNodes[1].value;
		var polNo = tbody.rows[rowIndex].cells[3].childNodes[0].value;*/
		var advName = tbody.rows[rowIndex].cells[1].childNodes[0].value;
		var custName = tbody.rows[rowIndex].cells[2].childNodes[0].value;
		var advstfEmailId = tbody.rows[rowIndex].cells[1].childNodes[2].value;
		var advId = tbody.rows[rowIndex].cells[1].childNodes[1].value;
		
		
		//if(isEmpty(hNTUCPolicyId) && (isEmpty(ntucPolicyNum) || !(hCompApproveStatus =="APPROVE"))){
			//if(isEmpty(ntucPolicyNum) && !isEmpty(hNTUCPolicyId)){
			/*var loadMsg = document.getElementById("loadingMessage");
			var loadingLayer = document.getElementById("loadingLayer");
			
			loadMsg.style.display="block";
			loadingLayer.style.display="block"; */
			
			var ajaxParam = "dbcall=CALL_NTUCWS" +
					"&txtFldFnaId=" + fnaId +
					"&txtFldNTUCPolicyId="+ntucPolicyId +
					"&txtFldNTUCApprovalRemarks=" + approvalRemark+
					"&strApprovalDate="+approvalDate+
					"&approvalStatus="+approvalStatus+
					"&txtFldAdvStfId="+advId;
			
		//	alert(ajaxParam);
			
			
			var ajaxResponse = callAjax(ajaxParam,false);
			retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
			
			for ( var val in retval) {
				
				var tabdets = retval[val];
				
				if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				}
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						var value = tabdets[tab];
						
						if (tab == "NTUC_STATUS_DETAILS_FAILED") {
							alert(value[0].txtFldNTUCStatusDescription);
							var loadMsg = document.getElementById("loadingMessage");
							var loadingLayer = document.getElementById("loadingLayer");
							loadMsg.style.display="none";
							return false;
						}
						
						if (tab == "NTUC_STATUS_DETAILS") {
							
							/*tbody.rows[rowIndex].cells[2].childNodes[1].value = value[0].txtFldNTUCPolNumber;
							tbody.rows[rowIndex].cells[2].childNodes[2].value = value[0].txtFldNTUCStatusCode;
							tbody.rows[rowIndex].cells[2].childNodes[3].value = value[0].txtFldNTUCStatusDescription; */
							
							alert("Policy Number:"+value[0].txtFldNTUCPolNumber+"\nStatus Code:"+value[0].txtFldNTUCStatusCode+"\nStatus Description:"+value[0].txtFldNTUCStatusDescription)
							compApproveStatus(selObj);
							 
								
						}
					}
				}
			}
			
		 
		
	}
}

function openFnaReportAsPDF(imageobj){
	
	 var url ="";   
	 var fnaid = imageobj.parentNode.parentNode.childNodes[4].childNodes[0].value;
	 var custid = imageobj.parentNode.parentNode.childNodes[2].childNodes[1].value;
	 var isPdfOpenFld = imageobj.parentNode.parentNode.childNodes[4].childNodes[3];
	 
	 params = "&P_CUSTID="+custid;
	 params+= "&P_FNAID="+fnaid;
	 params+= "&P_PRDTTYPE=LIFE";
	 params+= "&P_CUSTOMERNAME=";
	 params+= "&P_CUSTNRIC=";
	 params+= "&P_FORMTYPE=SIMPLIFIED";
	 params+= "&P_DISTID="+LOGGED_DISTID;
	 
	 isPdfOpenFld.value="Y";

//	 url = BIRT_URL+'/frameset?__report=report/FNA/Financial Needs Analysis V 3.9.rptdesign&__format=pdf'+params;
	 if(!IsEmpty(url)){
	  creatSubmtDynaReportPDF(url);
	 }
	 
	 
}


function getAllColumnData(selObj){
	 
	
	var rowInd = selObj.parentNode.parentNode.rowIndex-2;
	
	var tbody = selObj.parentNode.parentNode.parentNode;
	
	var adviserId =tbody.rows[rowInd].cells[1].childNodes[1].value;
	var clntId =tbody.rows[rowInd].cells[2].childNodes[1].value;
	var fnaId =tbody.rows[rowInd].cells[4].childNodes[0].value;
	var mangrapprovestatus =tbody.rows[rowInd].cells[6].childNodes[0].value;
	var mangrapproveremarks =tbody.rows[rowInd].cells[7].childNodes[0].value;
	var mangrapprovedate =tbody.rows[rowInd].cells[8].childNodes[0].value;
	var adminapprovestatus =tbody.rows[rowInd].cells[9].childNodes[0].value;
	var adminapproveremarks =tbody.rows[rowInd].cells[10].childNodes[0].value;
	var adminapprovedate =tbody.rows[rowInd].cells[11].childNodes[0].value;
	var compapprovestatus =tbody.rows[rowInd].cells[12].childNodes[0].value;
	var compapproveremarks =tbody.rows[rowInd].cells[13].childNodes[0].value;
	var compapprovedate =tbody.rows[rowInd].cells[14].childNodes[0].value;
	var hhtcPolId = tbody.rows[rowInd].cells[5].childNodes[1].value;
	var mgrStsApprBy = tbody.rows[rowInd].cells[4].childNodes[2].value;
	var adminStsApprBy = tbody.rows[rowInd].cells[10].childNodes[2].value;
	var compStsApprBy = tbody.rows[rowInd].cells[13].childNodes[2].value;
	
	var strQueryString = "";
	
	if(selObj.name == "selKycMgrApprSts"){
		mangrapprovedate = currDate;
	}
	
	if(selObj.name == "selKycAdminApprSts"){
		adminapprovedate = currDate;
	}

	if(selObj.name == "selKycCompApprSts"){
		compapprovedate = currDate;
	}
	 
//	setTimeout(function() { loaderBlock() }, 150);
	
	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");

	loadMsg.style.display = "block";
	// alert(loadMsg.style.display)
	loadingLayer.style.display = "block";
	
	strQueryString = "dbcall=INSERT_AUXUILLARY_DATA&txtFldAdviserId="+adviserId+"&txtFldClientId="+clntId+
		"&txtFldFnaId="+fnaId+"&txtFldMngrApproveStatus="+mangrapprovestatus+"&txtFldMngrRemarks="+mangrapproveremarks+"&txtFldMngrApproveDate="+mangrapprovedate+
		"&txtFldAdminApproveStatus="+adminapprovestatus+"&txtFldAdminRemarks="+adminapproveremarks+"&txtFldAdminApproveDate="+adminapprovedate+
		"&txtFldComplianceApproveStatus="+compapprovestatus+"&txtFldComplianceRemarks="+compapproveremarks+"&txtFldComplianceApproveDate="+compapprovedate+
		"&txtFldNTUCPolId="+hhtcPolId +"&txtFldMgrStsApprBy="+mgrStsApprBy+"&txtFldAdminStsApprBy="+adminStsApprBy+"&txtFldCompStsApprBy="+compStsApprBy;
	 
	var ajaxResponse = callAjax(strQueryString,false);// uncomment on 1/11/2018
	var retval = jsonResponse = eval('(' + ajaxResponse + ')');
	
	for ( var val in retval) {

		var tabdets = retval[val];

		if (tabdets["SESSION_EXPIRY"]) {
			window.location = baseUrl + SESSION_EXP_JSP;
			return;
		}
		for ( var tab in tabdets) {
			if (tabdets.hasOwnProperty(tab)) {
				var value = tabdets[tab];

				if (tab == "INSERT_AUXUILLARY_STATUS") {
					if(value =="insert_error"){
						alert("History details insert error");
						document.getElementById("loadingMessage").style.display="none";
					}else{
						alert("History details saved success");
						document.getElementById("loadingMessage").style.display="none";
					}
				}

			}
		}
	}
		
	
	
	
}
/*$("#btnApprovedHistory,#btnPendHistory").click(function(){
	//alert();
	getHistoryDetails();
});*/

function getHistoryDetails(btnid){
    
     var selKycApprovalStatus = btnid.value;
//	commented by poovathi on 25 feb 2020
//	$("#kycapproval tbody tr").remove();
	
//	var loadMsg = document.getElementById("loadingMessage");
//	var loadingLayer = document.getElementById("loadingLayer");
	
//commented by poovathi on 25-02-2020
	//var selKycApprovalStatus = document.getElementById("selKycApprovalStatus").value
	
//	POOVATHI ADD ON 25-02-2020	
	/*var selKycApprovalStatus = document.getElementById("btnApprovedHistory").value ="APPROVED";
	var selKycPendingStatus = document.getElementById("btnPendHistory").value ="PENDING";
	var selKycRejectedStatus = document.getElementById("btnRejectHistory").value ="REJECT";*/
	//alert(selKycPendingStatus);
	console.log( selKycApprovalStatus);
	
	//FOR APPROVE 
	

	if(!isEmpty(trim(selKycApprovalStatus)) &&  selKycApprovalStatus == "APPROVE"){
		
		//document.getElementById("history").style.display="none";
		//document.getElementById("Approval").style.display="";
		
		var strQueryString = "dbcall=getAuxuillaryDetails" +
				"&selKycApprovalStatus="+selKycApprovalStatus;
				 
		
		 
		
		var ajaxResponse = callAjax(strQueryString,false);
		retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		 
		
		for ( var val in retval) {
			
			console.log("val"+val);
			
			//var loadMsg = document.getElementById("loadingMessage");
			//var loadingLayer = document.getElementById("loadingLayer");
			//loadMsg.style.display="none";
			
			var tabdets = retval[val];
			
			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];
					
					if (tab == "FNA_HISTORY_DETAILS_NO_RECORDS") {
						alert("Tab details"+ tab)
						//$("#viewApprovHistory").modal();
						//removeTblRows("kycapproval");
						alert(value[0].NO_RECORDS);
						//var loadMsg = document.getElementById("loadingMessage");
						//var loadingLayer = document.getElementById("loadingLayer");
						//loadMsg.style.display="none";
						return false;
					}
					
					if (tab == "FNA_HISTORY_DETAILS") {
						alert("tab" + tab);
						//if(value[0].txtFldMgrApproveStatus =="APPROVE"){
						$("#viewApprovHistory").modal();
							loadTblValuesFromResultSet(value,"APPROVE");
						//}
						
					}
				}
			}
		}
		
		/*var tblId = document.getElementById("kycapproval");
		 var tbody = tblId.tBodies[0];
		 var rowLen = tbody.rows.length;
		 
		 for(var len=0;len<rowLen;len++){
			 for(var cell=0;cell<tbody.rows[len].cells.length;cell++){
				
				 if(tbody.rows[len].cells[cell].childNodes[0].type == "text"){
					 tbody.rows[len].cells[cell].childNodes[0].readOnly = true;
					 tbody.rows[len].cells[cell].childNodes[0].className = "fpNonEditTblData";
					 tbody.rows[len].cells[cell].childNodes[0].style.background="";
				 }else if(tbody.rows[len].cells[cell].childNodes[0].type == "select-one"){
					 tbody.rows[len].cells[cell].childNodes[0].disabled = true;
					 tbody.rows[len].cells[cell].childNodes[0].style.background="";
					 
				 }
			 }
		 }*/
		
	}
	//FOR PENDING
	

	if(!isEmpty(trim(selKycApprovalStatus)) &&  selKycApprovalStatus == "PENDING"){
		
		//document.getElementById("history").style.display="none";
		//document.getElementById("Approval").style.display="";
		
		var strQueryString = "dbcall=getAuxuillaryDetails" +
				"&selKycApprovalStatus="+selKycApprovalStatus;
				 
		console.log("strQueryString" + strQueryString);
		 
		
		var ajaxResponse = callAjax(strQueryString,false);
		
		//console.log("ajaxResponse"+ ajaxResponse);
		
		retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		console.log("retval"+retval);
		
		for ( var val in retval) {
			
			console.log("val"+val);
			
			//var loadMsg = document.getElementById("loadingMessage");
			//var loadingLayer = document.getElementById("loadingLayer");
			//loadMsg.style.display="none";
			
			var tabdets = retval[val];
			
			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];
					
					if (tab == "FNA_HISTORY_DETAILS_NO_RECORDS") {
						alert("Tab details"+ tab)
						//$("#viewApprovHistory").modal();
						//removeTblRows("kycapproval");
						alert(value[0].NO_RECORDS);
						//var loadMsg = document.getElementById("loadingMessage");
						//var loadingLayer = document.getElementById("loadingLayer");
						//loadMsg.style.display="none";
						return false;
					}
					
					if (tab == "FNA_HISTORY_DETAILS") {
						alert("tab" + tab);
						//if(value[0].txtFldMgrApproveStatus =="APPROVE"){
						$("#viewPendHistory").modal();
							loadTblValuesFromResultSet(value,"PENDING");
						//}
						
					}
				}
			}
		}
		
		/*var tblId = document.getElementById("kycapproval");
		 var tbody = tblId.tBodies[0];
		 var rowLen = tbody.rows.length;
		 
		 for(var len=0;len<rowLen;len++){
			 for(var cell=0;cell<tbody.rows[len].cells.length;cell++){
				
				 if(tbody.rows[len].cells[cell].childNodes[0].type == "text"){
					 tbody.rows[len].cells[cell].childNodes[0].readOnly = true;
					 tbody.rows[len].cells[cell].childNodes[0].className = "fpNonEditTblData";
					 tbody.rows[len].cells[cell].childNodes[0].style.background="";
				 }else if(tbody.rows[len].cells[cell].childNodes[0].type == "select-one"){
					 tbody.rows[len].cells[cell].childNodes[0].disabled = true;
					 tbody.rows[len].cells[cell].childNodes[0].style.background="";
					 
				 }
			 }
		 }*/
		
	}
	
	
	// FOR REJECT
	
	if(!isEmpty(trim(selKycApprovalStatus)) &&  selKycApprovalStatus == "REJECT"){
		
		//document.getElementById("history").style.display="none";
		//document.getElementById("Approval").style.display="";
		
		var strQueryString = "dbcall=getAuxuillaryDetails" +
				"&selKycApprovalStatus="+selKycApprovalStatus;
				 
		console.log("strQueryString" + strQueryString);
		 
		
		var ajaxResponse = callAjax(strQueryString,false);
		
		//console.log("ajaxResponse"+ ajaxResponse);
		
		retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		console.log("retval"+retval);
		
		for ( var val in retval) {
			
			console.log("val"+val);
			
			//var loadMsg = document.getElementById("loadingMessage");
			//var loadingLayer = document.getElementById("loadingLayer");
			//loadMsg.style.display="none";
			
			var tabdets = retval[val];
			
			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];
					
					if (tab == "FNA_HISTORY_DETAILS_NO_RECORDS") {
						alert("Tab details"+ tab)
						//$("#viewApprovHistory").modal();
						//removeTblRows("kycapproval");
						alert(value[0].NO_RECORDS);
						//var loadMsg = document.getElementById("loadingMessage");
						//var loadingLayer = document.getElementById("loadingLayer");
						//loadMsg.style.display="none";
						return false;
					}
					
					if (tab == "FNA_HISTORY_DETAILS") {
						alert("tab" + tab);
						//if(value[0].txtFldMgrApproveStatus =="APPROVE"){
						$("#viewRejectHistory").modal();
							loadTblValuesFromResultSet(value,"REJECT");
						//}
						
					}
				}
			}
		}
		
		/*var tblId = document.getElementById("kycapproval");
		 var tbody = tblId.tBodies[0];
		 var rowLen = tbody.rows.length;
		 
		 for(var len=0;len<rowLen;len++){
			 for(var cell=0;cell<tbody.rows[len].cells.length;cell++){
				
				 if(tbody.rows[len].cells[cell].childNodes[0].type == "text"){
					 tbody.rows[len].cells[cell].childNodes[0].readOnly = true;
					 tbody.rows[len].cells[cell].childNodes[0].className = "fpNonEditTblData";
					 tbody.rows[len].cells[cell].childNodes[0].style.background="";
				 }else if(tbody.rows[len].cells[cell].childNodes[0].type == "select-one"){
					 tbody.rows[len].cells[cell].childNodes[0].disabled = true;
					 tbody.rows[len].cells[cell].childNodes[0].style.background="";
					 
				 }
			 }
		 }*/
		
	}
	
// Poovathi add on 26-02-2020 [SHOWS /LOAD FNA DETAILS !PENDING] 
	
/*if(!isEmpty(trim(selKycApprovalStatus)) && selKycApprovalStatus  == ""){
	
		getApprovalDetails();
	}
	*/

//commented by poovathi
	/*if(!isEmpty(trim(selKycApprovalStatus)) &&  selKycApprovalStatus == "PENDING"){
		
		if(strFnaMgrUnAppDets != null){
			loadMgrUpApprovedRec();	
		}
		
		if(strAdminFNADets != null){
			loadAdminUpApprovedRec();
		}
		
		if(strComplianceFNADets != null){
			loadCompUnApprovedRec();
		}
		
		getApprovalDetails();
		
		
	}
	*/
	 
	
}

function getApprovalDetails(){
	
	// comment by poovathi 
	//$("#kycapproval tbody tr").remove();
	
//	document.getElementById("history").style.display="";
//	document.getElementById("Approval").style.display="none";
	
	var spltArr = strGlblUsrPrvlg.split(",");
	
	if((strMngrFlg.toUpperCase() == "Y")){
		//alert("strMngrFlg" +strMngrFlg);
		var ajaxResponse = callAjax("dbcall=loadFnaDetails",false);
		retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 		 

		for ( var val in retval) {
			var tabdets = retval[val];
			
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {

					var key = tab;
					var value = tabdets[tab];
					
					if (key == "MANAGER_FNA_DETAILS") {
						loadTblValuesFromResultSet(value,"M");
					}
					
					if (key == "MANAGER_NO_RECORDS_FNA_DETAILS") {
						removeTblRows("kycapproval");
//					 		alert(value[0].NO_RECORDS_FOUND);
					 		return false;
						 
					}
					
				}
			}
		}
	}else{
		
//	}
	
	 var adminFlg=false;
	 var compFlg=false;
	 for(var arrlen=0;arrlen<spltArr.length;arrlen++){
		 if(spltArr[arrlen] == ADMIN_KYC_APPROVAL){
			 adminFlg = true;
		 }
		 
		 if(spltArr[arrlen] == COMP_KYC_APPROVAL){
			 compFlg = true;
		 }
	 }
	 
	 if(adminFlg && !compFlg){
		 removeTblRows("kycapproval");
		 	var ajaxResponse = callAjax("dbcall=loadAdminFnaDetails",false);
			retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 

			for ( var val in retval) {

				var tabdets = retval[val];

				for ( var tab in tabdets) {

					if (tabdets.hasOwnProperty(tab)) {

						var key = tab;
						var value = tabdets[tab];
						
						if (key == "ADMIN_FNA_DETAILS") {
							loadTblValuesFromResultSet(value,"A");
						}
						
						if (key == "ADMIN_NO_RECORDS_FNA_DETAILS") {
							removeTblRows("kycapproval");
//						 		alert(value[0].NO_RECORDS_FOUND);
						 		return false;
							 
						}
						
					}
				}
			}
	 }
	 
	 if(compFlg && !adminFlg){
		 removeTblRows("kycapproval");
		 var ajaxResponse = callAjax("dbcall=loadComplianceFnaDetails",false);
			retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		 for ( var val in retval) {

				var tabdets = retval[val];

				for ( var tab in tabdets) {

					if (tabdets.hasOwnProperty(tab)) {

						var key = tab;
						var value = tabdets[tab];
						
						if (key == "COMPLIANCE_FNA_DETAILS") {
							loadTblValuesFromResultSet(value,"C");
						}
						
						if (key == "COMPLIANCE_NO_RECORDS_FNA_DETAILS") {
							removeTblRows("kycapproval");
//						 		alert(value[0].NO_RECORDS_FOUND);
						 		return false;
							 
						}
						
					}
				}
			}
	 }
	 
	 if(adminFlg && compFlg){
		 removeTblRows("kycapproval");
		 var ajaxResponse = callAjax("dbcall=loadCompAndAdminFnaDetails",false);
			retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		 for ( var val in retval) {

				var tabdets = retval[val];

				for ( var tab in tabdets) {

					if (tabdets.hasOwnProperty(tab)) {

						var key = tab;
						var value = tabdets[tab];
						
						if (key == "COMPLIANCE_FNA_DETAILS") {
							loadTblValuesFromResultSet(value,"C");
						}
						
						if (key == "COMPLIANCE_NO_RECORDS_FNA_DETAILS") {
							removeTblRows("kycapproval");
//						 		alert(value[0].NO_RECORDS_FOUND);
						 		return false;
							 
						}
						
					}
				}
			}
	 }
	
	}
	
	reorderTblById("kycapproval"); 
}	

function clearPrevHistRec(tblId){
	
	removeTblRows(tblId);	
}

function loadMgrUpApprovedRec(){
	
	if(strFnaMgrUnAppDets != null){
		
		var retval = strFnaMgrUnAppDets;

		for ( var val in retval) {
			var tabdets = retval[val];
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					
					var key = tab;
					var value = tabdets[tab];
					console.log(key +'='+value)
					
					if (key == "MANAGER_FNA_DETAILS") {
						
						// poovathi add this common func to  populate details for manager,admin andc omp. on 19-03-2020
						 populateApprovedRec(value,"MANAGER");
					}					
					if (key == "MANAGER_NO_RECORDS_FNA_DETAILS") {
						
						$('#cover-spin').hide(0);
                          return false;
					}
					
				}
			}
		}
		
	}

//reorderTblById("kycapproval");
}

function loadAdminUpApprovedRec(){
	
	if(strAdminFNADets != null){
		
		var retval = strAdminFNADets;

		for ( var val in retval) {
			var tabdets = retval[val];
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					
					var key = tab;
					var value = tabdets[tab];
					
					if (key == "ADMIN_FNA_DETAILS") {
						
						// poovathi add this common func to  populate details for manager,admin andc omp. on 19-03-2020 
						populateApprovedRec(value,"ADMIN");
						}
					if (key == "ADMIN_NO_RECORDS_FNA_DETAILS") {
						
						$('#cover-spin').hide(0);
                         return false;
						}
					
				}
			}
		}
		
	}
	
//	reorderTblById("kycapproval");
}

function loadCompUnApprovedRec(){
	
	
	if(strComplianceFNADets != null){
		var retval = strComplianceFNADets;

		for ( var val in retval) {
			var tabdets = retval[val];
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					
					var key = tab;
					var value = tabdets[tab];
					if (key == "COMPLIANCE_FNA_DETAILS") {
						// poovathi add this common func to  populate details for manager,admin andc omp. on 19-03-2020 
						populateApprovedRec(value,"COMPLIANCE");
						
                     }
					
					if (key == "COMPLIANCE_NO_RECORDS_FNA_DETAILS") {
						
						$('#cover-spin').hide(0);
						alert(value[0].NO_RECORDS_FOUND);
						return false;}
					
				}
			}
		}
	}
	
//	reorderTblById("kycapproval");
}

$("#imgPolicyDocs").on("click",function(){
	downloadAndOpenFile(_selectedFnaid,true);
});

function downLoadAllFile(fnaId,msgflag){

	downloadAndOpenFile(fnaId);
	}

function downloadAndOpenFile(hrefobj,msgflag){
	
//	var fnaid = hrefobj.parentNode.parentNode.cells[4].childNodes[0].value;
	var fnaid = hrefobj;
	/*alert(KYC_APPROVAL_ENSURE_MSG); 
	var isPdfOpenFld = hrefobj.parentNode.parentNode.childNodes[4].childNodes[3];
	var nutcpolicyid = hrefobj.parentNode.childNodes[1].value;
	var fnaid = hrefobj.parentNode.parentNode.cells[4].childNodes[0].value;*/
	//var url = 'DownloadFile.action?txtFldNtucPolId='+nutcpolicyid;
//	var url = 'DownloadFile.action?txtFldFnaId='+fnaid;
	
//	isPdfOpenFld.value="Y";
//	window.open(url, 'TheWindow_PDFBIRT',"channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
	
//	showPDFWindow(); 
	
	
//	alert(url);
//
//	var content = "<object id='pdfObject' type='application/pdf' data='http://192.168.0.12:8203/KYC/DownloadFile.action?txtFldNtucPolId=NTUC_POL1' width='100%' height='100%' />";
//	$("#pdfDisplay").empty();
//
//	$("#pdfDisplay").attr("title", "tsos" + " Preview");
//	$("#pdfDisplay").html(content);
//
//	$("#pdfDisplay").dialog({modal: true, width: 800, height: 400, resizable: true, buttons: { 'Ok': function() { $(this).dialog('close'); } } });
//	return false;
	
	$.ajax({
		type : "POST",
		url : "KycUtilsServlet",
		data :{"dbcall":"FETCHNTUCDOCUMENT","strFnaId":fnaid},
		dataType : "json",
		async : false,
		beforeSend : function() {
			$("#loadingLayer").show();
		},

		success : function(data) {
			var jsnResponse = data;
			
			 if(jsnResponse[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
				  window.location = baseUrl + SESSION_EXP_JSP;
				  return;
			 }
			 
			 if(!jsnResponse[0].NO_RECORDS){
				// poovathi add on 17-03-2020
				 var strNoRecords ="No Document found.";
				 $("#dynaAttachNTUCList").append(strNoRecords);
				 if(msgflag){
					 alert("No Document found.")	 
				 }
				 
				  return;
			 }
			 
			/*ModalPopups.polAttachmentUWPopup("POL_UW_ATTACH", {width : 900,height : 490,onOk : "closeAttachDialog()"});
			$("#POL_UW_ATTACH_okButton").val("CLOSE");
			 */
			$.each(jsnResponse[0].CLIENT_DOCUMENT,function(i,obj){
//				loadDocJsnData(obj,fnaid);
				loadDocJsnDataNew(obj,fnaid);
			});
			 
		},
		complete : function() {
			$("#loadingMessage").hide();
		},
		error : function() {
			$("#loadingMessage").hide();
		}
	});

}

function showPDFWindow(){
	
	//$("#pdfcontent").html(content);

    $("#pdfdialog").dialog({
        resizable: false,
        title: "KYC Approval",
        modal: true,
        minWidth: 450,
        minHeight:200,
        //height: 'auto',
        bgiframe: false,        
        buttons: [{text: " OK ",click:function(){
        	$("#singlerowviewdiv").dialog('close');
        	}
        }]
    });
}



function showMgrInfo(e,imgobj,tt_for){
	
	var cellobj = imgobj.parentNode;
	var rowobj = cellobj.parentNode;
//	var advmgrid = cellobj.childNodes[3].value;
//	var advmgrname = cellobj.childNodes[4].value;
	
	
	var mgrApprBy = rowobj.cells[4].childNodes[2].value;
	var adminApprBy = rowobj.cells[10].childNodes[2].value;
	var compApprBy = rowobj.cells[13].childNodes[2].value;
	
	var icdets = rowobj.cells[2].childNodes[2].value;
	
	//var tt_txt = " Manager  : "+advmgrname ; //+ " [" +advmgrid + "] ";
	var tt_txt="";
	
	
	if(tt_for == "mgr"){
		if(!isEmpty(mgrApprBy))tt_txt += "<br>"+ " KYC Approved By :"+mgrApprBy;
		else if(isEmpty(mgrApprBy)) tt_txt += "<br>"+ " KYC Yet to Approve by Manager!";
	}
	
	if(tt_for == "admin"){
		if(!isEmpty(adminApprBy))tt_txt += "<br>"+ " KYC Approved By :"+adminApprBy;
		else if(isEmpty(adminApprBy)) tt_txt += "<br>"+ " KYC Yet to Approve by Admin!";
	}
	
	if(tt_for == "comp"){
		if(!isEmpty(compApprBy))tt_txt += "<br>"+ " KYC Approved By :"+compApprBy;
		else if(isEmpty(compApprBy)) tt_txt += "<br>"+ " KYC Yet to Approve by Compliance!";
	}
	
	if(tt_for == "icinfo"){
		tt_txt += "<br>"+ " Customer NRIC/Passport/FIN : "+icdets;
	}
	
	
	showTooltip(e,tt_txt);
	
}

//added by kavi 21/09/2018
function ModalPopupsCancelKycDoc(selObj){
	
	var loadMsg = document.getElementById("loadingMessage");
	var loadingLayer = document.getElementById("loadingLayer");
	loadMsg.style.display="none";
	
	selObj.value="";
	ModalPopups.Cancel("KycApprovalDoc");
	 
	 /*loadingLayer.style.display="none";*/
	 
}

function kycCompApproveDocBinary(){
	
	var parameter = "dbcall=KYCDOCTOBINARYDATA"; 
	
	var form = $('#hdnFormKycApprDoc')[0];
    var data = new FormData(form);
	
    $.ajax({
        url : baseUrl+"/KycUtilsServlet?"+parameter,
        type: "POST",
        data :data,
        cache: false,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false
    }).done(function(response){ //
    	var saveResp=$.parseJSON(response);
		  
		  if(saveResp[0].SESSION_EXPIRY=='SESSION_EXPIRY'){
			  window.location = baseUrl + SESSION_EXP_JSP;
			  return;
		  }
		  
		  if(saveResp[0].STATUS=='SUCCESS'){
			  alert(saveResp[0].BINARYDATA);
			  ModalPopupsCancelKycDoc();
		  }
		  
		  if(saveResp[0].STATUS=='FAIL'){
			  alert("Failed to convert to base64.");
		  }
    });
    
    ModalPopupsCancelKycDoc();
}


function closeAttachDialog(){
	document.getElementById("uwattachjsppage").appendChild(document.getElementById("polAttachmentJspUW"));
	$("#KycAttachTbl tbody tr").remove();
	ModalPopups.Cancel("POL_UW_ATTACH");
	return true;
}


function loadDocJsnData(jsnData,fnaid){
	
	var tblobj = document.getElementById("KycAttachTbl");
	var tbodyobj = tblobj.tBodies[0];
	$("[name=hdnTxtFldNtucFnaId]").val(fnaid);
	
	var rowlenobj = tbodyobj.rows.length;
	var jqRwLen=$("#KycAttachTbl tbody tr:visible").length;
	
	var rowobj = tbodyobj.insertRow(rowlenobj);
	
	var cell0 = rowobj.insertCell(0);
	cell0.innerHTML =  '<input type="text" readonly="readonly" name="txtFldFileSino" value="'+(Number(jqRwLen)+1)+'" style="width:100%;text-align:center;background:none;border-style:none;"/>'+
					   '<input type="hidden" name="txtFldClntDocMode" value="'+ INS_MODE+ '"/>';
	cell0.style.textAlign = "center";
	cell0.style.display= "none";
	
	var cell1 = rowobj.insertCell(1);
	cell1.style.textAlign = "left";
	cell1.innerHTML = '<input type = "text" readOnly="true" class="fplblText"  name="txtFldFileTitle" value="'+jsnData.txtFldDocTitle+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="background:none;border-style:none;width:95%;"/>';
	
	var cell2 = rowobj.insertCell(2);
	cell2.style.textAlign = "left";
	cell2.innerHTML = '<input type = "text" readOnly="true" class="fplblText"  name="txtFldFileDoc" value="'+jsnData.txtFldDocFileName+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="background:none;border-style:none;width:86%;"/>'+
	'<img src="images/file_download.png" width="20" height="20" style="cursor:pointer;" title="Download File"/>'+
	'<input type="hidden" name="txtFldFileNDID" value="'+jsnData.txtFldDocNdId+'"/>';
	$(cell2).find("img:eq(0)").on("click",function(){
		var ntucid=$(this).closest("td").find("input:eq(1)").val();
		downLoadNtucAttach(ntucid);
	});
	
	var cell3 = rowobj.insertCell(3);
	cell3.style.textAlign = "left";
	cell3.innerHTML = '<input type = "text" readOnly="true" class="fplblText"  name="txtFldFileType" value="'+jsnData.txtFldDocFileType+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="background-color: rgb(233, 244, 235);width:95%;"/>';
	cell3.style.display="none";
	
	var cell4 = rowobj.insertCell(4);
	cell4.style.textAlign = "left";
	cell4.innerHTML = '<input type = "text" readOnly="true" class="fplblText"  name="txtFldFileDte" value="'+jsnData.txtFldDocAtchDte+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="background-color: rgb(233, 244, 235);width:95%;"/>';
	cell4.style.display="none";
	
	var cell5 = rowobj.insertCell(5);
	cell5.style.textAlign = "left";
	cell5.innerHTML = '<input type = "text" readOnly="true" class="fplblText"  name="txtFldFileBy" value="'+jsnData.txtFldDocAtchBy+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="background-color: rgb(233, 244, 235);width:95%;"/>';
	cell5.style.display="none";
	
	
//	poovathi commented on 25-02-2020
	/*var tblobj = document.getElementById("KycAttachTbl");
	var tbodyobj = tblobj.tBodies[0];
	$("[name=hdnTxtFldNtucFnaId]").val(fnaid);

	$("[name=hdnTxtFldNtucFnaId]").val(fnaid);
	var jqRwLen=$("#KycAttachTbl tbody tr:visible").length;
	
	var cell0 = '<input type="text" readonly="readonly" name="txtFldFileSino" value="'+(Number(jqRwLen)+1)+'" style="width:100%;text-align:center;background:none;border-style:none;"/>'+
	             '<input type="hidden" name="txtFldClntDocMode" value="'+ INS_MODE+ '"/>';
	var cell1 =  '<input type = "text" readOnly="true" class="fplblText"  name="txtFldFileTitle" value="'+jsnData.txtFldDocTitle+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="background:none;border-style:none;width:95%;"/>';
	
	var cell2 = '<input type = "text" readOnly="true" class="fplblText"  name="txtFldFileDoc" value="'+jsnData.txtFldDocFileName+'" onclick="hideTooltip()" maxlength="60" onmouseover="showTooltip(event,this.value);" onmouseout="hideTooltip();" style="background:none;border-style:none;width:86%;"/>'+
	            '<img src="images/file_download.png" width="20" height="20" style="cursor:pointer;" title="Download File"/>'+
	            '<input type="hidden" name="txtFldFileNDID" value="'+jsnData.txtFldDocNdId+'"/>';
	 $(cell2).find("img:eq(0)").on("click",function(){
		var ntucid=$(this).closest("td").find("input:eq(1)").val();
		downLoadNtucAttach(ntucid);
	});
	
	KycAttachTblDets.row.add( [cell0,cell1,cell2] ).draw( true ); */
}


function downLoadNtucAttach(ntid){
	
	var url = 'DownloadNtucFile.action?txtFldNtucPolId='+ntid;
	
	window.open(url,"_blank");

//	window.location.href =url;
	
//	var pop_up=window.open(url, 'TheWindow_PDFBIRT',"channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
//	pop_up.document.write('<script src="'+baseUrl+'/jQuery_Plugins/jquery.js"></script>');
//	pop_up.document.write('<script type="text/javascript">$(function(){window.close()});</script>');
//	showPDFWindow(); 
}


function DownLoadAllNtucFile(){
//	alert($("[name=hdnTxtFldNtucFnaId]").val());
	var url = 'DownloadNtucZipFile.action?txtFldFnaPolId='+$("[name=hdnTxtFldNtucFnaId]").val();
	window.open(url,"_blank");

//	window.location.href =url;
}

/*
 * Reorder with tbl id with 1st child element.
 */
function reorderTblById(id){
	var count =1;
	$("#"+id+" tbody tr").each(function(){
		$(this).find("td:first input:first").val(count);
		count++;
	});
}

function getFnaRowObj(fnaid){
	
	var rowobj = null;
	var tblId = document.getElementById("kycapproval");
	var tbody = tblId.tBodies[0];
	var rowlen = tbody.rows.length;
	for(var r=0;r<rowlen;r++){
		
		var rowfaid = tbody.rows[r].cells[4].childNodes[0].value;
		if(rowfaid==fnaid){
			rowobj = tbody.rows[r];
			break;
		}
	}

	return rowobj;
}



function callPolNoAPI(rowobj){
	
	
	var caseid = rowobj.parentNode.parentNode.cells[5].childNodes[2].value;
	var fnaid = rowobj.parentNode.parentNode.cells[4].childNodes[0].value;
	var  servadvid= rowobj.parentNode.parentNode.cells[1].childNodes[1].value;
	var  servadvname= rowobj.parentNode.parentNode.cells[1].childNodes[0].value;
	var custname= rowobj.parentNode.parentNode.cells[2].childNodes[0].value;
	var custid= rowobj.parentNode.parentNode.cells[2].childNodes[1].value;
	
	
	if(!isEmpty(caseid)){

		var loadMsg = document.getElementById("loadingMessage");
		var loadingLayer = document.getElementById("loadingLayer");
		
		loadMsg.style.display="block";
		//alert(loadMsg.style.display)
		loadingLayer.style.display="block"; 
		
		$.ajax({
			
		    url:"OAuthServlet", 
		    
		    type:"POST",
		    
		    data:$.param(
		    		
		    		{CALLFOR: "GETNTUCPOLICYDETS",
		    			
		    			 
		    			CASEID:caseid,
		    			FNAID:fnaid,
		    			SERVADVID:servadvid,
		    			SERVADVNAME:servadvname,
		    			CUSTOMERNAME:custname,
		    			CUSTID:custid
		    				
		    			
		    		}),
		    		
		      success:function(data) {

		    	  loadMsg.style.display="none";
		    	   
					var response = $.parseJSON(data); 
					$.each(response, function(count, arr) {
						$.each(arr,function(tab,data){
							$.each(data,function(key,val){
								
								if(key == "UPDATEKYC2_STATUS"){
									alert(val);
//									$(rowobj).closest('tr').find('td:eq(3) select:eq(0)').val("In-Progress");
								}
								if(key == "UPDATEKYC2_DSF_STATUS"){
									if(val.toLowerCase() == "success"){
										alert("Policy No. details are successfully downloaded")	;
									}
									
								} 
															
							});
							
						});					
					});
					
					
		      }
		});	
		
	}
	

	
}


function openEKYC(imgobj){
	
	var rowobj = imgobj.parentNode.parentNode;
	
	
	var custid = rowobj.cells[2].childNodes[1].value; 
	var custname = rowobj.cells[2].childNodes[0].value.toUpperCase();
	var advId = rowobj.cells[1].childNodes[1].value; 
	var fnaId = rowobj.cells[4].childNodes[0].value;
	var kycformname = "SIMPLIFIED";
	var lobArrtoKYC = "ALL";
	var strLoggedUserDesig ="";
	var strLoggedDistrbutor="";
	var emailId = rowobj.cells[1].childNodes[2].value;
	var formType = "SIMPLIFIED";
	
	
//	var url  = "KycPre.action?txtFldCustId="+custid+"&txtFldCustName="+custname+"&txtFldServAdvId="+advId
//	+"&strDistributor="+LOGGED_DISTID+"&txtFldCrtdUser="+strLoggedUserId+"&txtFldKycForm=ALL&txtFldLogUserAdvId="
//	+strLoggedAdvStfId+"&LoggedUser="+strLoggedUserId+"&strFNAId="+fnaId;
	
	
	var url = "KycPre.action?txtFldCustId="+custid+
	"&txtFldCrtdUser="+strLoggedUserId+
	"&txtFldKycForm="+kycformname+
//	"&accssLvlStfType="+GLBL_ASSCESS_LVL_STAFF_TYPE+
	"&txtFldCustName="+custname+
	"&mgrAcsFlg="+strMngrFlg+
	"&txtFldCustLob="+lobArrtoKYC+
	"&txtFldServAdvId="+advId+
	"&txtFldLogUserAdvId="+strLoggedAdvStfId+
	"&strLoggedUserDesig="+strLoggedUserDesig+
	"&strDistributor="+LOGGED_DISTID+
	"&strDistributorName="+strLoggedDistrbutor+
	"&LoggedUser="+strLoggedUserId+
//	"&txtFldCustInitials="+initials+
	"&selFnaFrom="+fnaId+
	"&txtFldCurrAdvtsfEmailId="+emailId+
	"&selFnaFormType="+formType+
	"&txtFldFnaId="+fnaId;
//	"&selCustCateg=PERSON&txtFldLoggedUserName="+strLoggedUserName+
//	"&txtFldServAdvName="+ClientForm.selAdviserStaffByAgentIdCurr.options[ClientForm.selAdviserStaffByAgentIdCurr.selectedIndex].text+
//	"&selClientStatus="+ClientForm.selPersClntStatus.options[ClientForm.selPersClntStatus.selectedIndex].text	
//	creatSubmtDynaNewWindow(url);
	
	
	

	
	var $dialog = $('<div></div>')
	               .html('<iframe style="border: 0px; " src="' + url + '" width="100%" height="100%" id="dynaFrame"></iframe>')
	               .dialog({
	                   autoOpen: false,
	                   closeOnEscape: false,
	                   modal: true,
	                   height: "750",
	                   width: "1090",
	                   title: "FNA - Manager Verification",
	                   buttons : {
	                	   
	                	   " OK " :{
	                		   text : " OK ",
	                		   id : "btnEKYCOK",
	                		   click : function (){
	   	               			
		               				var ret = chkMgrSignAndDets($dialog,rowobj);
		               				if(ret.length == 4){
		               					if(ret[3]){
			               					$(this).dialog("close");
			               				}
		               				}else if(ret.length == 0){
		               					$(this).dialog("close");
		               				}
	                		   }
	                		   
	                	   },
	                	   " CANCEL " :{
	                		   text : " CANCEL ",
	                		   click : function(){
	                			   $(this).dialog("close");
	                		   }
	                	   }
	               		
	               	},
	               	open: function(event, ui) {
	               		
	                    $(".ui-dialog-titlebar-close", ui.dialog || ui).hide();
	                }
	               });
	$dialog.dialog('open');
	$('.ui-button.ui-corner-all.ui-widget.ui-button-icon-only.ui-dialog-titlebar-close').css("display","none") ;
 
	
}

function chkMgrSignAndDets($dialog,rowobj){
	
	var MGR_SIGN_STATUS="N";
	
	var fnaId = rowobj.cells[4].childNodes[0].value;
	
	var ajaxParam = "dbcall=GETMANAGERSIGNDETS" +"&strFNAID="+fnaId	;

	var ajaxResponse = callAjax(ajaxParam,false);
	retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		 
		
		for ( var val in retval) {
			
			var tabdets = retval[val];
			
			if (tabdets["SESSION_EXPIRY"]) {
				window.location = baseUrl + SESSION_EXP_JSP;
				return;
			}
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					var value = tabdets[tab];
					
					
					if (tab == "MANAGER_SIGNED") {
						MGR_SIGN_STATUS = (value);
						
						 
					}
				}
			}
		}
	
		console.log("MGR_SIGN_STATUS------->"+MGR_SIGN_STATUS)
		
	var returnObj = [];
	var agree=false ,disagree=false,reason="";
	var trigger = false;
	
	var agreefld ,disagreefld;
	
	var mgrsign = $("#dynaFrame").contents().find("input[id='hTxtFldMgrSignStatus']").val();
	console.log(mgrsign);
	
	var spltArr = strGlblUsrPrvlg.split(",");
	
	if((strMngrFlg.toUpperCase() == "Y")){
		
//		var mgrsign = $("#dynaFrame").contents().find("input[id='hTxtFldMgrSignStatus']").val();
		
		if(MGR_SIGN_STATUS != "Y"){
			if(window.confirm("Do you want to close the FNA preview without signature?")){
				
				
				$dialog.dialog("close");
				
				var loadMsg = document.getElementById("loadingMessage");			
				loadMsg.style.display="none";
				
				return false;
				
			}else{
				var loadMsg = document.getElementById("loadingMessage");			
				loadMsg.style.display="none";
				
				alert("Please verify and approve/reject the FNA Form");
				return false;
			}
		}
		
		if(MGR_SIGN_STATUS == "Y"){
			
//			var iframe = document.getElementById('dynaFrame');
//			iframe.src = iframe.src;
//			
			
			agreefld  = $("#dynaFrame").contents().find("#fnaForm_suprevMgrFlgagree");
			disagreefld = $("#dynaFrame").contents().find("#fnaForm_suprevMgrFlgdisagree");
			
			agree = $("#dynaFrame").contents().find("#fnaForm_suprevMgrFlgagree").prop("checked");
			disagree = $("#dynaFrame").contents().find("#fnaForm_suprevMgrFlgdisagree").prop("checked");	
			reason = $("#dynaFrame").contents().find("#suprevFollowReason").val();
			
			if(!agree && !disagree){
				alert(FNA_APPR_MGR_AGREE );
				$("#dynaFrame").contents().find("#tr_mgr_agreesec").css({"border":"1px solid red"});
				$("#dynaFrame").contents().find("input[name='suprevMgrFlg']").closest("td").css({"background-color":"yellow"});
				return false;
			}

			if(!agree && disagree){
				
				if(isEmpty(reason)){
					alert(MANAGER_KEYIN_REMARKS);
					$("#dynaFrame").contents().find("#suprevFollowReason").focus()
					return false;
				}
				
			}
			
			
//			if(agree && !disagree){
//				var conf = window.confirm(CONFIRM_APPROVE_MANAGER);
//				if(!conf){
//					var loadMsg = document.getElementById("loadingMessage");
//					var loadingLayer = document.getElementById("loadingLayer");
//					loadMsg.style.display="none";
//					agreefld.prop("checked",false);
//					disagreefld.prop("checked",false);
//					return false;
//				}else{
//					agreefld.prop("checked",true).prop("disabled",true);
//					disagreefld.prop("checked",false).prop("disabled",true);
//					
//				}	
//			}
			
			
//			if(disagree && !agree){
//				var conf = window.confirm(CONFIRM_REJECT_MANAGER);
//				if(!conf){
//					var loadMsg = document.getElementById("loadingMessage");
//					var loadingLayer = document.getElementById("loadingLayer");
//					loadMsg.style.display="none";
//					agreefld.prop("checked",false);
//					disagreefld.prop("checked",false);
////					selObj.value = "";
//					return false;
//				}
//			}
			
			
			trigger=true;
		}
		
		
		returnObj.push(agree);
		returnObj.push(disagree);
		returnObj.push(reason);
		returnObj.push(trigger);
//		alert(returnObj)
			
			rowobj.cells[5].childNodes[9].value=MGR_SIGN_STATUS;
			rowobj.cells[6].childNodes[0].value = ( agree == true ? "APPROVE" : (disagree == true ? "REJECT" : ""));
			rowobj.cells[7].childNodes[0].value = reason;
			
			
			setTimeout(function() { loaderBlock() ;}, 0);	
			setTimeout(function() { managerApproveStatusAppr(rowobj.cells[6].childNodes[0],$dialog,false); ;}, 1000);	
		
			return returnObj;
			
	}else{
		
		 var adminFlg=false;
		 var compFlg=false;
		 for(var arrlen=0;arrlen<spltArr.length;arrlen++){
			 if(spltArr[arrlen] == ADMIN_KYC_APPROVAL){
				 adminFlg = true;
			 }
			 
			 if(spltArr[arrlen] == COMP_KYC_APPROVAL){
				 compFlg = true;
			 }
		 }
		 returnObj.length =0;
		
		 return returnObj;
		 
		
	}
	
	
	
		
//		window.location.reload(true);
	

	
	
}


function callGenerateFNAPDF(imgobj){
	
	var custIdObj = imgobj.parentNode.parentNode.cells[2].childNodes[1];
	var fnaIdObj = imgobj.parentNode.parentNode.cells[4].childNodes[0];
	

	var fnaid = fnaIdObj.value;
	var custid = custIdObj.value;
	var custlobdets  = "ALL";
	
	generateFNAPDF(fnaid,custid,custlobdets);

}




function ajaxCall(parameter) {

	var jsnResponse = "";

	$.ajax({
		type : "POST",
		url : "KYCServlet",
		data : parameter,
		dataType : "json",

		async : false,

		beforeSend : function() {
//			$("#loadingLayer").show();
//			$("#loadingMessage").show();
//			loaderBlock();
		},

		success : function(data) {
			jsnResponse = data;
		},
		complete : function() {
			// alert("Loaded Success...");
//			$("#loadingMessage").hide();
//			setTimeout(function(){ loaderHide(); },200);
		},
		error : function() {
			// alert("Loading Error...");
//			$("#loadingMessage").hide();
//			setTimeout(function(){ loaderHide(); },200);
		}
	});

	return jsnResponse;
}


function jsonDataPopulate(contData, tab) {

	var i=tab;

	for ( var cont in contData) {

		if (contData.hasOwnProperty(cont)) {

			var contvalue = contData[cont];
			console.log(cont + "=" + contvalue)

			var elemObj = $("#modalFNADetails").find("#"+cont);

			if (elemObj) {


				if (elemObj.type == "radio" || elemObj.type == "checkbox") {

					if (elemObj.val() == contvalue || contvalue == "on" ) {
						elemObj.prop("checked",true)
					}
				} else {

					elemObj.val(contvalue);
				}

			}
		}
	}
}



function openEKYCNew(dataset){

	
//	poovathi add on 24-02-2020
	var custid = dataset[0]["txtFldCustId"]; 
	var custname = dataset[0]["txtFldCustName"].toUpperCase();
	var advId = dataset[0]["txtFldAdvStfId"]; 
	var fnaId = dataset[0]["txtFldFnaId"]; 
	var kycformname = "SIMPLIFIED";
	var lobArrtoKYC = "ALL";
	var strLoggedUserDesig ="";
	var strLoggedDistrbutor=""; 
	var emailId = dataset[0]["txtFldAdvStfEmailId"]; 
	var formType = "SIMPLIFIED";
	
	var url = "KycPre.action?txtFldCustId="+custid+
	"&txtFldCrtdUser="+strLoggedUserId+
	"&txtFldKycForm="+kycformname+
//	"&accssLvlStfType="+GLBL_ASSCESS_LVL_STAFF_TYPE+
	"&txtFldCustName="+custname+
	"&mgrAcsFlg="+strMngrFlg+
	"&txtFldCustLob="+lobArrtoKYC+
	"&txtFldServAdvId="+advId+
	"&txtFldLogUserAdvId="+strLoggedAdvStfId+
	"&strLoggedUserDesig="+strLoggedUserDesig+
	"&strDistributor="+LOGGED_DISTID+
	"&strDistributorName="+strLoggedDistrbutor+
	"&LoggedUser="+strLoggedUserId+
//	"&txtFldCustInitials="+initials+
	"&selFnaFrom="+fnaId+
	"&txtFldCurrAdvtsfEmailId="+emailId+
	"&selFnaFormType="+formType+
	"&txtFldFnaId="+fnaId;
	
	$("#dynaFrame").attr("src",url);
	
	
	
//	 document.getElementById("dynaFrame").setStyles({ 'overflow': 'auto' });
	
	setTimeout(function(){
		$('#cover-spin').hide(0);
	}, 1500);
	
	
}

function getFNADetailsNew(strStatus){


	if(!isEmpty(trim(strStatus))){
		
		
		var strQueryString = "dbcall=getFNADetails&selKycApprovalStatus="+strStatus;
		
		
				 
		
		var ajaxResponse = callAjax(strQueryString,false);
		retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
		
		
		retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 		 

		for ( var val in retval) {
			var tabdets = retval[val];
			
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {

					var key = tab;
					var value = tabdets[tab];
					
					if (key == "FNA_DETAILS_NEW") {
//						loadTblValuesFromResultSet(value,"M");
//							resultSets[len].txtFldCustName
						
							
							$('#approvalDataModal').modal({
								  backdrop: 'static',
								  keyboard: false,
								  show:true,
								}); 
							
							$('#approvalDataModal').on('shown.bs.modal', function() {
								
								$("#dynaApprovalDataFNAID tbody").empty();
								
								$("#dynaApprovalLoader").removeClass("d-none");
								
								setTimeout(function(){
									
									var tbody = $("#dynaApprovalDataFNAID").find("tbody");
									
									for(var len=0;len<value.length;len++){
										
										console.log("-->"+ value[len].txtFldFnaId);
										
										tbody.append("<tr><td>"+value[len].txtFldFnaId+"</td><td>"+value[len].txtFldCustName+"</td><td>"+value[len].txtFldCustId+"</td><td>"+value[len].txtFldAdvStfId+"</td><td>"+value[len].txtFldAdvStfEmailId+"</td></tr>")
									
//										$("#dynaApprovalDataFNAID").append('<a class="nav-item nav-link" href="#">'+value[len].txtFldFnaId+' | '+value[len].txtFldCustName+'</a> ') ;
									}
									
									
									
									
								}, 1000);
								
								setTimeout(function(){
									$("#dynaApprovalLoader").addClass("d-none hidden");
									
									 $('#dynaApprovalDataFNAID').DataTable({"columnDefs": [ {  "targets": [ 2,3,4 ],  "visible": false,  "searchable": false }  ]});
									 $('#dynaApprovalDataFNAID tbody').find("tr").on("click",function(){
										 var row=$(this);
										 var fnaid =row.find("td:eq(0)").text();
//										 alert(fnaid);
										 
										 
										 var fnaId = row.find("td:eq(0)").text(); 
											var custname = row.find("td:eq(1)").text().toUpperCase();
											 var custid = row.find("td:eq(2)").text(); 
											var advId = row.find("td:eq(3)").text(); 
											
											var kycformname = "SIMPLIFIED";
											var lobArrtoKYC = "ALL";
											var strLoggedUserDesig ="";
											var strLoggedDistrbutor=""; 
											var emailId = row.find("td:eq(4)").text(); 
											var formType = "SIMPLIFIED";
											console.log($("#frmTempLogin").find("#txtFldCrtdUser").html())
											document.getElementById("txtFldCrtdUser").value= (strLoggedUserId)
											document.getElementById("hTxtFldFnaId").value = (fnaId);
											
											var url = "KycApprovalPre.action";
											document.forms["frmTempLogin"].action=url;
											document.forms["frmTempLogin"].submit();
											
//											var url = "KycPre.action?txtFldCustId="+custid+
//											"&txtFldCrtdUser="+strLoggedUserId+
//											"&txtFldKycForm="+kycformname+
////											"&accssLvlStfType="+GLBL_ASSCESS_LVL_STAFF_TYPE+
//											"&txtFldCustName="+custname+
//											"&mgrAcsFlg="+strMngrFlg+
//											"&txtFldCustLob="+lobArrtoKYC+
//											"&txtFldServAdvId="+advId+
//											"&txtFldLogUserAdvId="+strLoggedAdvStfId+
//											"&strLoggedUserDesig="+strLoggedUserDesig+
//											"&strDistributor="+LOGGED_DISTID+
//											"&strDistributorName="+strLoggedDistrbutor+
//											"&LoggedUser="+strLoggedUserId+
////											"&txtFldCustInitials="+initials+
//											"&selFnaFrom="+fnaId+
//											"&txtFldCurrAdvtsfEmailId="+emailId+
//											"&selFnaFormType="+formType+
//											"&txtFldFnaId="+fnaId;
//											
//											$("#dynaFrame").attr("src",url);
											
											$('#approvalDataModal').modal({
												  backdrop: 'static',
												  keyboard: false,
												  show:false,
												}); 
									 });
									 
									  
									 
								}, 1100);
								
								
								
							});

							
						
					}
					
					if (key == "MANAGER_NO_RECORDS_FNA_DETAILS") {
						removeTblRows("kycapproval");
					 		return false;
						 
					}
					
				}
			}
		}
		 
		
		
		
	}
	

}



function loadALLFNAId(){
	
	if(strALLFNAIds != null){
	
		
		var retval = strALLFNAIds;

		for ( var val in retval) {
			var tabdets = retval[val];
			for ( var tab in tabdets) {
				if (tabdets.hasOwnProperty(tab)) {
					
					var key = tab;
					var value = tabdets[tab];
				        if (key == "MANAGER_FNA_DETAILS" || key == "ADMIN_FNA_DETAILS" || key == "COMPLIANCE_FNA_DETAILS") {
						 var ResultLength = value.length;
						 
						  for(var len=0;len<ResultLength;len++){
							  
							  
			                var status = "";
			                
							if(key == "MANAGER_FNA_DETAILS"){
								status = value[len]["txtFldMgrApproveStatus"];
							}else if(key == "ADMIN_FNA_DETAILS"){
								status = value[len]["txtFldAdminApproveStatus"];
							}else if(key == "COMPLIANCE_FNA_DETAILS"){
								status = value[len]["txtFldCompApproveStatus"];
							}
							
//PENDING  Table List
							if(isEmpty(status)){
	                    	   
						        var fnaid = value[len]["txtFldFnaId"],
						        custname = value[len]["txtFldCustName"];
						        pendingdatatable.row.add( [
						                    pendingdatatable.rows().count()+1 ,
						                    value[len]["txtFldFnaId"],
						                    value[len]["txtFldCustName"],						                    
						                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
						                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
						                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
						                    value[len]["txtFldCustId"],
						                    value[len]["txtFldAdvStfId"],
						                    value[len]["txtFldAdvStfEmailId"],
						                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>')
						                ] ).draw( false );
						       
						        
						        	  $("#optgrpPending").append('<option value="'+fnaid+'">'+custname + '- '+fnaid+'</option>');
						        
							
							}
							
						
//APPROVE Table List							
							if(status.toUpperCase() == "APPROVE"){
								
								
								approveddatatable.row.add( [
										                    approveddatatable.rows().count()+1 ,
										                    value[len]["txtFldFnaId"],
										                    value[len]["txtFldCustName"],						                    
										                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
										                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
										                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
										                    value[len]["txtFldCustId"],
										                    value[len]["txtFldAdvStfId"],
										                    value[len]["txtFldAdvStfEmailId"],
										                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
										                    value[len]["txtFldMgrStsApprBy"],
										                    "",
										                    value[len]["txtFldAdminStsApprBy"],
										                    "",
										                    value[len]["txtFldCompStsApprBy"],
										                    ""
										                ] ).draw( false );
//								
								 var fnaid = value[len]["txtFldFnaId"],
							        custname = value[len]["txtFldCustName"] ;
								 
								
						        		  $("#optgrpApproved").append('<option value="'+fnaid+'">'+custname + '- '+fnaid+'</option>');
								 
								 
							}
							
							
//	REJECTED Table List						
							if(status.toUpperCase() == "REJECT"){
								
								
								 var fnaid = value[len]["txtFldFnaId"],
							        custname = value[len]["txtFldCustName"];
								 
								rejecteddatatable.row.add( [
										                    rejecteddatatable.rows().count()+1 ,
										                    value[len]["txtFldFnaId"],
										                    value[len]["txtFldCustName"],						                    
										                    (value[len]["txtFldMgrApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldMgrApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
										                    (value[len]["txtFldAdminApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldAdminApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
										                    (value[len]["txtFldCompApproveStatus"] == "APPROVE" ? "Approved" : value[len]["txtFldCompApproveStatus"] == "REJECT" ? "Rejected" : "Pending"),
										                    value[len]["txtFldCustId"],
										                    value[len]["txtFldAdvStfId"],
										                    value[len]["txtFldAdvStfEmailId"],
										                    (value[len]["txtFldFnaPrin"] == "NTUC" ? '<i class="fa fa-check-square"></i>' : '<i class="fa fa-window-close-o"></i>'),
										                    value[len]["txtFldMgrStsApprBy"],
										                    "",
										                    value[len]["txtFldAdminStsApprBy"],
										                    "",
										                    value[len]["txtFldCompStsApprBy"],
										                    ""
										                ] ).draw( false );
								
								 
						        		 $("#optgrpRejected").append('<option value="'+fnaid+'">'+custname + '- '+fnaid+'</option>');
								
								
							}
							
							
							
						   }
						
						
//poovathi check NTUCCaseId [is null or not ] on 17-03-2020
						  
						  $('#cover-spin').hide(0);
						
						}					
					if (key == "MANAGER_NO_RECORDS_FNA_DETAILS") {
						  $('#cover-spin').hide(0);
						return false;
					}
					
				}
			}
		}
		 
		var currFNAID = $("#hTxtFldFnaId").val();
		$("#dropDownQuickLinkFNAHist").val(currFNAID);
		
		 $('#tblPending tbody').on('click', 'tr', function () {
			 $(this).toggleClass('selected');
	            var data = pendingdatatable.row( this ).data();
	            getSelectedFNADetsNew(data);
	        } );
		 
		 $('#tblApproved tbody').on('click', 'tr', function () {
			 $(this).toggleClass('selected');
	            var data = approveddatatable.row( this ).data();
	            getSelectedFNADetsNew(data);
	        } );
		 
		 $('#tblRejected tbody').on('click', 'tr', function () {
			 $(this).toggleClass('selected');
	            var data = rejecteddatatable.row( this ).data();
	            getSelectedFNADetsNew(data);
	        } );
		 
		 $('#tblPending tbody tr').each(  function () {
	            var data = pendingdatatable.row( this ).data();
	            if(data[1] == currFNAID){
	           	 $(this).addClass('highlight');
	           }else{
	        	   $(this).removeClass('highlight');
	           }
	            
	        } );
		 
		 $('#tblApproved tbody tr').each(  function () {
	            var data = approveddatatable.row( this ).data();
	            if(data[1] == currFNAID){
	           	 $(this).addClass('highlight');
	           }else{
	        	   $(this).removeClass('highlight');
	           }
	        } );
		 
		 $('#tblRejected tbody tr').each(  function () {
	            var data = rejecteddatatable.row( this ).data();
	            if(data[1] == currFNAID){
	           	 $(this).addClass('highlight');
	           }else{
	        	   $(this).removeClass('highlight');
	           }
	        } );
		 
		 $('#tblApproved').on('mousemove', 'tr', function(e) {
			   var rowData = approveddatatable.row(this).data();
			   
			   var info = '<h6>'+rowData[1]+'<br/><small>(Click the row to view the details)</small></h6><small><strong>'+rowData[3]+'</strong> by(Manager) : <strong>'+rowData[10]+'</strong>'+
			   '<hr/>';
			   if(rowData[4] != "Pending"){
				   info += ' <strong>'+rowData[4]+'</strong> by(Admin) : '+rowData[12]+'<hr/>';
			   }
			   if(rowData[5] != "Pending"){
				   info += '<strong>'+rowData[5]+'</strong> by(Compliance) : '+rowData[14]+'<hr/>';
			   }
			   
			   
			   info += '</small>';
			   
			   $("#tooltip").html(info).animate({ left: e.pageX, top: e.pageY }, 1);
			   if (!$("#tooltip").is(':visible')) $("#tooltip").show();
			})
			
			$('#tblApproved').on('mouseleave', function(e) {
			  $("#tooltip").hide();
			}) ;
		 
		 $('#tblRejected').on('mousemove', 'tr', function(e) {
			   var rowData = rejecteddatatable.row(this).data();
			   
			   var info = '<h6>'+rowData[1]+'</h6><small>(Click the row to view the details)</small><br/><small><strong>'+rowData[3]+'</strong> by(Manager) : <strong>'+rowData[10]+'</strong>'+
			   '<hr/>';
			   if(rowData[4] != "Pending"){
				   info += ' <strong>'+rowData[4]+'</strong> by(Admin) : '+rowData[12]+'<hr/>';
			   }
			   if(rowData[5] != "Pending"){
				   info += '<strong>'+rowData[5]+'</strong> by(Compliance) : '+rowData[14]+'<hr/>';
			   }
			   
			   
			   info += '</small>';
			   
			   $("#tooltip").html(info).animate({ left: e.pageX, top: e.pageY }, 1);
			   if (!$("#tooltip").is(':visible')) $("#tooltip").show();
			})
			
			$('#tblRejected').on('mouseleave', function(e) {
			  $("#tooltip").hide();
			}) ;
		
		$('#dropDownQuickLinkFNAHist').select2({
			width :'400',				
			 templateResult: formatDataSelect2
		})
		

		
	}

}

function formatDataSelect2 (data) {
	  if (!data.id) {
	    return data.text;
	  }
	  var $format = $( '<span>' + data.text + '</span>'	  );
	  return $format;
}

$('#dropDownQuickLinkFNAHist').on("change",function(){
	var tthisfnaid = $(this).val();
	getSelectedFNADets(tthisfnaid)
})
	
 

function callajaxFunc(strClntName,strLoggedAdvStfId){
	
	var ajaxParam = "dbcall=GETCLNTSRCHDETAILS"+"&strClntName="+ strClntName +"&strLoggedAdvStfId="+ strLoggedAdvStfId;
      
	console.log(ajaxParam);
	
	   $.ajax({
	        url : "KYCServlet?"+ajaxParam,
	        type: "POST",
//	        data :data,
	        cache: false,
//	        enctype: 'multipart/form-data',
	        contentType: false,
	        processData: false
	    }).done(function(data){ 
	    	
	    	 //var retval = $.parseJSON(data);
	    	var retval = JSON.parse(data)
	    	 
	    	 if (retval.length == 0) {
	    		 alert("No Record Exists!");
					return false;
				}
	    	
	    	$.each(retval, function(key,value) {
	    		  
	   		 	$.each(value, function(k,v){
	   		 		
	   		 	 var listitem = "<option>";
	   		 	 
	   		 	var custname="",fnaid = "";
	   			  
	   			   $.each(v, function(k1,v1){
	   				   
	   				    if(k1=="CUST_NAME"){ custname =  v1;}
	   				    if(k1=="FNA_ID"){ fnaid = v1;}
	   				     
	   		        });
	   			   
	   			
	   			 var dynaLink = '<option data-subtext="'+fnaid+'">'+custname+'</option>';
	   				$("#dropDownClntNameDiv").append(dynaLink);
	   				
	   			  
	   		     });
	   		  
	   		 }); 
	    	
	    	
	    });
	}




$("#txtFldAdminApproveStatus").on("change",function(){
	
	var thisval = $(this).val().toUpperCase();
	
	if(!isEmpty(thisval)){
			 
		var fnaId = $("#txtFldFnaId").val();
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = $("#txtFldAdminApproveRemarks").val();
		var hNTUCPolicyId = $("#txtFldNTUCCaseId").val();
		var polNo = $("#txtFldNTUCPolNo").val();
		var advName = $("#txtFldAdvStfName").val();
		var custName = $("#txtFldCustName").val();
		var custId = $("#txtFldCustId").val();
		var advstfEmailId = $("#txtFldAdvEmailId").val();
		
		var approverAdvStfId = ""
		var approverUserId = ""
		
		var fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";


			if(thisval == "REJECT" && isEmpty(remarks)){
				
				alert(ADMIN_KEYIN_REMARKS);
				
		    	$("#txtFldAdminApproveRemarks").prop("readOnly",false);
				$("#txtFldAdminApproveRemarks").focus();
				
				return false;
			}
			
			if(thisval == "REJECT"){
				var conf = window.confirm(ADMIN_REJECTSTATUS_CONFIRM);
				if(!conf){
					$(this).val("");
					return false;
				}
			}else if(thisval == "APPROVE"){
				var conf = window.confirm(ADMIN_APPROVESTATUS_CONFIRM);
				if(!conf){
					$(this).val("");
					return false;
				}
			}
			
			
			var ajaxParam = 'dbcall=ADMIN_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldAdminAppStatus="+thisval+
			"&txtFldManagerEmailId="+mangrEmailId+"&txtFldAdminRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
			"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
			+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldFnaPrin="+fnaPrin+"&txtFldCustId="+custId;
			
			
			setTimeout(function() {
				document.getElementById('cover-spin').style.display="block";
			}, 0);
			
			setTimeout(function() {
				var ajaxResponse = callAjax(ajaxParam,false);
				retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
				
				for ( var val in retval) {
					
					var tabdets = retval[val];
					
					if (tabdets["SESSION_EXPIRY"]) {
						window.location = baseUrl + SESSION_EXP_JSP;
						return;
					}
					for ( var tab in tabdets) {
						if (tabdets.hasOwnProperty(tab)) {
							var value = tabdets[tab];						
							 
							if (tab == "TAB_ADMIN_APPROVE_DETAILS") {							 
									
								if(value[0].ADMIN_APPROVE_STATUS == "APPROVE"){
									 
									 if(strAdminFlg == "Y" && strCompFlg == "Y"){
										 
									 }
									
								}
								
								if(value[0].ADMIN_APPROVE_STATUS == "APPROVE"){
									document.getElementById("txtFldAdminApproveStatus").options[0].innerHTML = "Approved";
								}
								if(value[0].ADMIN_APPROVE_STATUS == "REJECT"){
									document.getElementById("txtFldAdminApproveStatus").options[1].innerHTML = "Rejected";
								}	
								
								alert(value[0].ADMIN_POLICY_STATUS);
								parent.location.reload(true);
							}
						}	
					}
				}
			}, 1000);
			
			
		
		

		
	}
	
});


$("#txtFldCompApproveStatus").on("change",function(){
	var thisval = $(this).val();
	if(!isEmpty(thisval)){
		
			 
		var fnaId = $("#txtFldFnaId").val() 
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = $("#txtFldAdminApproveRemarks").val();
		var hNTUCPolicyId = $("#txtFldNTUCCaseId").val(),
		ntucCaseId= $("#txtFldNTUCCaseId").val();
		var polNo = $("#txtFldNTUCPolNo").val();
		var advName = $("#txtFldAdvStfName").val();
		var advstfId = "";
		var custName = $("#txtFldCustName").val();
		var custId = $("#txtFldCustId").val();
		var custNRIC = "";
		var advstfEmailId = $("#txtFldAdvEmailId").val();
		
		var approverAdvStfId = "";
		var approverUserId = "";
		var advstfcode = "";
		
		var ntucCasests = "";
		var ntucPolCrtdFlg = "";
		
		var fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";

			if(thisval == "REJECT" && isEmpty(remarks)){
				alert(ADMIN_KEYIN_REMARKS);
				$("#txtFldCompApproveRemarks").prop("disabled",false);
				$("#txtFldCompApproveRemarks").focus();
				return false;
			}
			
			var jsontext = formJsonText(fnaId,advstfcode);
			
			if(thisval == "REJECT"){
				var conf = window.confirm(ADMIN_REJECTSTATUS_CONFIRM);
				if(!conf){
					$(this).val("");
					return false;
				}
			}else if(thisval == "APPROVE"){
				var conf = window.confirm(ADMIN_APPROVESTATUS_CONFIRM);
				if(!conf){
					$(this).val("");
					return false;
				}
			}
			
			
			if(thisval == "APPROVE" ){
				
				if(fnaPrin == "NTUC"){
					
					$('#modalNTUCApproval').modal({
						  backdrop: 'static',
						  keyboard: false,
						  show:true,
						}); 				
				}else{
					
					
					
					var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
					thisval+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
					"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
					+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+hNTUCPolicyId
					+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
					+"&CASEID="+ntucCaseId+"&CASESTATUS="+thisval+"&ADVSTFCODE="+advstfcode
					+"&FNAID="+fnaId+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
					+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin+"&KYCJSON="+jsontext
					
					console.log(ajaxParam);
					
					setTimeout(function() {
						document.getElementById('cover-spin').style.display="block";
					}, 0);
					
					
					setTimeout(function() {
						 $.ajax({
						        url : "KYCServlet?"+ajaxParam,
						        type: "POST",
//						        data :data,
						        cache: false,
//						        enctype: 'multipart/form-data',
						        contentType: false,
						        processData: false
						    }).done(function(response){ 
						    	document.getElementById('cover-spin').style.display="none";
						    	
						    	var retval =  $.parseJSON(response);
								
								for ( var val in retval) {
									
									var tabdets = retval[val];
									
									if (tabdets["SESSION_EXPIRY"]) {
										window.location = baseUrl + SESSION_EXP_JSP;
										return;
									}
									for ( var tab in tabdets) {
										if (tabdets.hasOwnProperty(tab)) {
											var value = tabdets[tab];
											
//											alert(tab +","+value[0]);
											 
											if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
												alert(value[0].COMP_POLICY_STATUS);
												
											}
											
											
											
											
											
											if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
												alert(value);
												
											}
										}
									}
								}
						    });
					}, 1000);
				}
				
			}else if(thisval == "REJECT"){
				
				var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
				thisval+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
				"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
				+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+ntucCaseId
				+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
				+"&CASEID="+ntucCaseId+"&CASESTATUS="+thisval+"&ADVSTFCODE="+advstfcode
				+"&FNAID="+fnaId+"&KYCJSON="+jsontext+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
				+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin;
				
				
				console.log(ajaxParam);
			    
				
				setTimeout(function() {
					document.getElementById('cover-spin').style.display="block";
				}, 0);
				
				
				setTimeout(function() {
					$.ajax({
				        url : "KYCServlet?"+ajaxParam,
				        type: "POST",
//				        data :data,
				        cache: false,
//				        enctype: 'multipart/form-data',
				        contentType: false,
				        processData: false,
				        // async:false
				    }).done(function(response){ 
				    	
				    	document.getElementById('cover-spin').style.display="none";
				    	
				    	var retval =  $.parseJSON(response);
						
						for ( var val in retval) {
							
							var tabdets = retval[val];
							
							if (tabdets["SESSION_EXPIRY"]) {
								window.location = baseUrl + SESSION_EXP_JSP;
								return;
							}
							for ( var tab in tabdets) {
								if (tabdets.hasOwnProperty(tab)) {
									var value = tabdets[tab];
									
									 
									if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
										
										
										alert(value[0].COMP_POLICY_STATUS);
										parent.location.reload(true);
										
									}
									
									if(tab == "UPDATEKYC2_STATUS"){
										alert(value);
										parent.location.reload(true);
									}
									
									if(tab == "UPDATEKYC2_DSF_STATUS" ){
										if(value.toLowerCase() == "success"){
										
										}else if(value.toLowerCase() == "failure"){
											

											
										}
										
									}
									
									if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
										alert(value);
										parent.location.reload(true);
										
									}
								}
							}
						}
				    });
				}, 1000);
			    
			    
				
			}
			
			/*
			setTimeout(function() {
				document.getElementById('cover-spin').style.display="block";
			}, 0);
			
			
			
			setTimeout(function() {
				var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldAdminAppStatus="+thisval+
				"&txtFldManagerEmailId="+mangrEmailId+"&txtFldAdminRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
				"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
				+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldFnaPrin="+fnaPrin+"&txtFldCustId="+custId;
				
				var ajaxResponse = callAjax(ajaxParam,false);
				retval = jsonResponse = eval('(' +ajaxResponse+ ')'); 
				
				for ( var val in retval) {
					
					var tabdets = retval[val];
					
					if (tabdets["SESSION_EXPIRY"]) {
						window.location = baseUrl + SESSION_EXP_JSP;
						return;
					}
					for ( var tab in tabdets) {
						if (tabdets.hasOwnProperty(tab)) {
							var value = tabdets[tab];
							
							 
							if (tab == "TAB_ADMIN_APPROVE_DETAILS") {
								 
									
								if(value[0].ADMIN_APPROVE_STATUS == "APPROVE"){
									
									 
									 if(strAdminFlg && strCompFlg){
										 
									 }
									
								}
								
								
								alert(value[0].ADMIN_POLICY_STATUS);
								parent.location.reload(true);
							}
						}	
					}
				}
			}, 1000);
			
			*/
			
			
			
	}
	
});

function logoutKyc() {

		window.close();
		return true;
	 
}


function validateNTUCApprDoc(){

	
	var file = document.getElementById("fileKycCompApprove").value;

	if(isEmpty(file)){
		alert("Select the document");
		return false;
	}else{
		
		
		var fnaId = $("#txtFldFnaId").val() 
		var mangrEmailId = strLoggedUserEmailId;
		var remarks = $("#txtFldAdminApproveRemarks").val();
		var hNTUCPolicyId = $("#txtFldNTUCCaseId").val(),
		ntucCaseId = $("#txtFldNTUCCaseId").val();
		var polNo = $("#txtFldNTUCPolNo").val();
		var advName = $("#txtFldAdvStfName").val();
		var advstfId = "";
		var custName = $("#txtFldCustName").val();
		var custId = $("#txtFldCustId").val();
		var custNRIC = "";
		var advstfEmailId = $("#txtFldAdvEmailId").val();
		
		var approverAdvStfId = "";
		var approverUserId = "";
		var advstfcode = "";
		
		var jsontext = formJsonText(fnaId,advstfcode);
		var thisval = $("#txtFldCompApproveStatus").val();
		
		var ntucCasests = $("#txtFldNTUCCaseStatus").val();
		var ntucPolCrtdFlg = "";
		
		var fnaPrin = !isEmpty(hNTUCPolicyId) ? "NTUC" : "NON-NTUC";

		
		
		var ajaxParam = 'dbcall=COMP_APPROVE_STATUS&txtFldFnaId=' + fnaId +"&txtFldCompAppStatus="+
		thisval+"&txtFldManagerEmailId="+mangrEmailId+"&txtFldCompRemarks="+remarks+"&hNTUCPolicyId="+hNTUCPolicyId+"&txtFldPolNum="+polNo+
		"&txtFldAdviserName="+advName+"&txtFldCustName="+custName+"&txtFldAdvEmailId="+advstfEmailId 
		+"&txtFldApprAdvStfId="+approverAdvStfId + "&txtFldApprUserId="+approverUserId+"&txtFldNtucCaseId="+hNTUCPolicyId
		+"&txtFldNtucCasests="+ntucCasests+"&txtFldNtucPolCrtdFlg="+ntucPolCrtdFlg
		+"&CASEID="+ntucCaseId+"&CASESTATUS="+thisval+"&ADVSTFCODE="+advstfcode
		+"&FNAID="+fnaId+"&CUSTID="+custId+"&CUSTNRIC="+custNRIC
		+"&SERVADVID="+advstfId+"&txtFldCustId="+custId+"&txtFldFnaPrin="+fnaPrin+"&KYCJSON="+jsontext
		console.log(ajaxParam)
		
		
		var form = $('#hdnFormKycApprDoc')[0];
		var data = new FormData(form);
		
		
		$.ajax({
	        url : "KYCServlet?"+ajaxParam,
	        type: "POST",
	        data :data,
	        cache: false,
	        enctype: 'multipart/form-data',
	        contentType: false,
	        processData: false
	    }).done(function(response){ 
	    	
	    	
//	    	loadMsg.style.display="none";	 
	    	
	    	var retval =  $.parseJSON(response);
			
			for ( var val in retval) {
				
				var tabdets = retval[val];
				
				if (tabdets["SESSION_EXPIRY"]) {
					window.location = baseUrl + SESSION_EXP_JSP;
					return;
				}
				for ( var tab in tabdets) {
					if (tabdets.hasOwnProperty(tab)) {
						var value = tabdets[tab];
						
						 
						if (tab == "TAB_COMPILANCE_REJECT_DETAILS") {
							
							
							alert(value[0].COMP_POLICY_STATUS);
							
						}
						
						if(tab == "UPDATEKYC2_STATUS"){
							alert(value);
						}
						
						
						if(tab == "UPDATEKYC2_DSF_STATUS" ){
//							alert(value.toLowerCase())
							if(value.toLowerCase() == "success"){
							
							}else if(value.toLowerCase() == "failure"){
								

								
							}
							
						}
						
						
						if (tab == "TAB_COMPILANCE_REJECT_DETAILS_ERROR") {
							alert(value);
							
						}
					}
				}
			}
	    });
	}
	return true;
	
}


function loadDocJsnDataNew(jsnData,fnaid){

	var docid = jsnData.txtFldDocNdId;
	
	
	var strList = '<a href="#" class="list-group-item list-group-item-action">'+
	'<div class="d-flex w-100 justify-content-between "><h6 class="mb-1">Doc.Title : '+jsnData.txtFldDocTitle+'</h6> <small><i class="fa fa-download cursor-pointer" style="color: #009A78;" onclick="downLoadNtucAttach('+docid+')" title="Click to Download Doc."></i></small></div>'+
    '<small>File Name : '+jsnData.txtFldDocFileName+'</small>  </a>';
	
	$("#dynaAttachNTUCList").append(strList);
	
	
  
  
}

// Poovathi add common function to get Approved Rec  details for manager,admin and comp.on 19-03-2020  
function populateApprovedRec(value,loggedUser){
	
    $("#txtFldFnaId").val(value[0]["txtFldFnaId"])
	$("#txtFldCustName").val(value[0]["txtFldCustName"])
	$("#txtFldAdvStfName").val(value[0]["txtFldAdvStfName"]);
	$("#spanLoggedAdviser").text($("#txtFldAdvStfName").val())
	
	$("#txtFldMgrName").val(value[0]["txtFldMgrName"]);
	
    var ntucCaseId = value[0]["txtFldFnaNtucCaseId"];
    
    var mgrstatus = value[0]["txtFldMgrApproveStatus"];
    var adminstatus = value[0]["txtFldAdminApproveStatus"];
    var compstatus = value[0]["txtFldCompApproveStatus"];
	
    //poovathi get hidden text field values below on 16-03-2020
	$("#txtFldNTUCCaseId").val(ntucCaseId);
	$("#txtFldCustId").val(value[0]["txtFldCustId"]);
	$("#txtFldAdvEmailId").val(value[0]["txtFldAdvStfEmailId"]);
	
	if(isEmpty(ntucCaseId)){
		//		var strNTUCNull = '<span class="badge badge-warning"><i class="fa fa-times"></i></span>';
		//		$("#NTUCFieldChkDiv").append(strNTUCNull);
		$("#ntucblock").hide();
	}else{
		//		var strNTUCNotNull  = '<span class="badge badge-success"><i class="fa fa-check-circle-o"></i></span>';
		//		$("#NTUCFieldChkDiv").append(strNTUCNotNull);		
		$("#ntucblock").show();
		
	}
	
	
	//MANAGER	    
	if((mgrstatus.toUpperCase()) == "APPROVE"){
		
		$("#mngrapprLbl").text('Approved Date');
		$("#mngrapprByLbl").text('Approved By');
		$("#iFontMgr").removeClass("fa-question-circle-o");
		$("#iFontMgr").removeClass("fa-times-circle-o");
		$("#iFontMgr").addClass("fa-check-circle-o");
		
	}else if((mgrstatus.toUpperCase()) == "REJECT") {
		
		$("#mngrapprLbl").text('Rejected Date');
		$("#mngrapprByLbl").text('Rejected By');
		$("#iFontMgr").addClass("fa-times-circle-o");
		$("#iFontMgr").removeClass("fa-check-circle-o");
		$("#iFontMgr").removeClass("fa-question-circle-o");
		
	}else {
		
		$("#mngrapprLbl").text('');
		//$("#mngrapprByLbl").text('Yet to Approve By');
		$("#iFontMgr").removeClass("fa-times-circle-o")
		$("#iFontMgr").removeClass("fa-check-circle-o")
		$("#iFontMgr").addClass("fa-question-circle-o");
	}
	
//ADMIN
	
	if(adminstatus.toUpperCase() == "APPROVE"){

		$("#admnapprLbl").text('Approved Date');
		$("#admnapprByLbl").text('Approved By');
		$("#iFontAdmin").removeClass("fa-times-circle-o")
		$("#iFontAdmin").addClass("fa-check-circle-o")
		$("#iFontAdmin").removeClass("fa-question-circle-o");
		
	}else	if((adminstatus.toUpperCase()) == "REJECT") {
		
		$("#admnapprLbl").text('Rejected Date');
		$("#admnapprByLbl").text('Rejected By');
		$("#iFontAdmin").addClass("fa-times-circle-o");
		$("#iFontAdmin").removeClass("fa-check-circle-o");
		$("#iFontAdmin").removeClass("fa-question-circle-o");
		
	 }else{
		$("#admnapprLbl").text('');
		//$("#admnapprByLbl").text('Yet to Approve By');
		$("#iFontAdmin").removeClass("fa-times-circle-o")
		$("#iFontAdmin").removeClass("fa-check-circle-o")
		$("#iFontAdmin").addClass("fa-question-circle-o");
	}
	
	//COMPLIANCE					
	if(compstatus.toUpperCase() == "APPROVE"){
		
		$("#compapprLbl").text('Approved Date');
		$("#compapprByLbl").text('Approved By');
		$("#iFontComp").removeClass("fa-times-circle-o")
		$("#iFontComp").addClass("fa-check-circle-o")
		$("#iFontComp").removeClass("fa-question-circle-o");
		
	}else 	if(compstatus.toUpperCase() == "REJECT") {
		
		$("#compapprLbl").text('Rejected Date');
		$("#compapprByLbl").text('Rejected By');
		$("#iFontComp").addClass("fa-times-circle-o")
		$("#iFontComp").removeClass("fa-check-circle-o")
		$("#iFontComp").removeClass("fa-question-circle-o");
		
	 }else {
		 
		$("#compapprLbl").text('');
		//$("#compapprByLbl").text('Yet to Approve By');
		$("#iFontComp").removeClass("fa-times-circle-o")
		$("#iFontComp").removeClass("fa-check-circle-o")
		$("#iFontComp").addClass("fa-question-circle-o");
	}
	
// FOR MANAGER
	$("#txtFldMgrApproveStatus").val(mgrstatus)
	$("#txtFldMgrApproveDate").val(value[0]["txtFldMgrApproveDate"]);
	$("#txtFldMgrApproveRemarks").val(value[0]["txtFldMgrApproveRemarks"]);	
	$("#txtFldMgrStsApprBy").val(value[0]["txtFldMgrStsApprBy"]);
//	setApprovalStatus($("#txtFldMgrApproveStatus"));
	
// FOR ADMIN				
	$("#txtFldAdminApproveStatus").val(adminstatus);
	$("#txtFldAdminApproveDate").val(value[0]["txtFldAdminApproveDate"]);
	$("#txtFldAdminApproveRemarks").val(value[0]["txtFldAdminApproveRemarks"]);
	$("#txtFldAdminStsApprBy").val(value[0]["txtFldAdminStsApprBy"]);
//	setApprovalStatus($("#txtFldAdminApproveStatus"));
	
// FOR COMPLIANCE	
	$("#txtFldCompApproveStatus").val(compstatus);
	$("#txtFldCompApproveDate").val(value[0]["txtFldCompApproveDate"]);
	$("#txtFldCompApproveRemarks").val(value[0]["txtFldCompApproveRemarks"]);
	$("#txtFldCompStsApprBy").val(value[0]["txtFldCompStsApprBy"]);
//	setApprovalStatus($("#txtFldCompApproveStatus"));
	
	var mgrApprStatus = $("#txtFldMgrApproveStatus").val();
	
	var adminApprStatus = $("#txtFldAdminApproveStatus").val();
	
	var compApprStatus = $("#txtFldCompApproveStatus").val();
	
    openEKYCNew(value);
	downLoadAllFile(value[0]["txtFldFnaId"],false);
	var custId = $("#txtFldCustId").val();
	getAllNRICDocsList(custId);
	
	if(loggedUser == "ADMIN"){
		
		$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
		$("#appr-mgr-tab-cont").removeClass("show").removeClass("active");;
		
    	$("#appr-admin-tab").removeClass("no-drop1");
    	$("#appr-admin-tab").addClass("active");
    	$("#appr-admin-tab-cont").addClass("show").addClass("active");
    	$("#txtFldAdminApproveRemarks").prop("readOnly",false);
    	 
    	$("#appr-comp-tab").addClass("no-drop1");	
    	
    	setApprovalStatus($("#txtFldAdminApproveStatus"));
	}
	
	if(loggedUser == "COMPLIANCE"){
		
		var tabfocus = true;
		
		$("#appr-mgr-tab").addClass("no-drop1").removeClass("active");
		$("#appr-mgr-tab-cont").addClass("show").removeClass("active");
		
		if(strAdminFlg == "Y" && strCompFlg == "Y"){
			
			if(adminApprStatus == "APPROVE"){
				
				$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
		    	$("#appr-admin-tab-cont").addClass("show").removeClass("active");				    	
		    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
		    	 
		    	$("#appr-comp-tab").removeClass("show").addClass("active");
		    	$("#appr-comp-tab").addClass("active");
		    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
				
			}else{
				
				$("#appr-admin-tab").addClass("active");
		    	$("#appr-admin-tab-cont").addClass("show").addClass("active");
		    	
		    	$("#txtFldAdminApproveRemarks").prop("readOnly",false);
				
			}
			
			setApprovalStatus($("#txtFldAdminApproveStatus"));
			
		}else if(strAdminFlg != "Y" && strCompFlg == "Y"){
			
			$("#appr-admin-tab").addClass("no-drop1").removeClass("active");				    					    	
	    	$("#appr-admin-tab-cont").addClass("show").removeClass("active");				    	
	    	$("#txtFldAdminApproveRemarks").prop("readOnly",true);
	    	 
	    	$("#appr-comp-tab").removeClass("show").addClass("active");
	    	$("#appr-comp-tab").addClass("active");
	    	$("#appr-comp-tab-cont").addClass("show").addClass("active");
			
		}
		
		
		setApprovalStatus($("#txtFldCompApproveStatus"));
		
	}
	
	
	
	
	/*if(mgrApprStatus.toUpperCase() == "APPROVE"){
		$("#appr-mgr-tab-cont").find(":input").prop("disabled",true);
	}else{
		$("#appr-mgr-tab-cont").find(":input").prop("disabled",false);
	}*/
	
	if(adminApprStatus.toUpperCase() == "APPROVE"){
		$("#appr-admin-tab-cont").find(":input").prop("disabled",true);
	}else{
		$("#appr-admin-tab-cont").find(":input").prop("disabled",false);
	}
	
	if(compApprStatus.toUpperCase() == "APPROVE"){
		$("#appr-comp-tab-cont").find(":input").prop("disabled",true);
	}else{
		$("#appr-comp-tab-cont").find(":input").prop("disabled",false);
	}
	
	
	
	if(ntuc_policy_access_flag == "true" && compApprStatus.toUpperCase() == "APPROVE"){			
		 $("#imgpolapi").show()		
	}else{
		 $("#imgpolapi").hide()
	}
	
}

function generateFNA(){
	
	var fnaid = $("#txtFldFnaId").val() 
	var custid = $("#txtFldCustId").val();
	var custLobDets="";
	var formtype = "SIMPLIFIED"
	
	var machine = "";
	machine = BIRT_URL + "/frameset?__report=" + FNA_RPT_FILELOC //
			+ "&P_CUSTID=" + custid + "&P_FNAID=" + fnaid+"&__format=pdf"
			+ "&P_PRDTTYPE=" + custLobDets
			+ "&P_CUSTOMERNAME=&P_CUSTNRIC=&P_FORMTYPE=" + formtype
			+ "&P_DISTID=" + LOGGED_DISTID;
	
	
	$("#PDFReportFrame").attr("src",machine);
	
}

function setApprovalStatus(object){
	var thisval = $(object).val();
//	console.log("inside setApprovalStatus thisval --->"+thisval +"," +  $(object).prop("id") )
//	if(thisval == "APPROVE"){
//		$(object).find('option:contains("APPROVE")').text('Approved');
//	}else if(thisval == "REJECT"){
//		$(object).find('option:contains("REJECT")').text('Rejected');
//	}
	
	if(thisval == "APPROVE"){
		document.getElementById($(object).prop("id")).options[0].innerHTML = "Approved";
	}
	if(thisval == "REJECT"){
		document.getElementById($(object).prop("id")).options[1].innerHTML = "Rejected";
	}	
}


//function getFnaIdValue(fnaid,ds){
//	getSelectedFNADetsNew(ds,fnaid);
//}


function callPolNoAPINew(rowobj){
	
	
	var caseid = $("#txtFldNTUCCaseId").val();
	var fnaid = $("#txtFldFnaId").val();
	var  servadvid= $("#txtFldAdvStfId").val();
	var  servadvname=$("#txtFldAdvStfName").val();
	var custname= $("#txtFldCustName").val();
	var custid= $("#txtFldCustId").val()
	
	
	if(!isEmpty(caseid)){

		
		$.ajax({
			
		    url:"OAuthServlet", 
		    
		    type:"POST",
		    
		    data:$.param(
		    		
		    		{CALLFOR: "GETNTUCPOLICYDETS",
		    			
		    			 
		    			CASEID:caseid,
		    			FNAID:fnaid,
		    			SERVADVID:servadvid,
		    			SERVADVNAME:servadvname,
		    			CUSTOMERNAME:custname,
		    			CUSTID:custid
		    				
		    			
		    		}),
		    		
		      success:function(data) {

		    	   
					var response = $.parseJSON(data); 
					$.each(response, function(count, arr) {
						$.each(arr,function(tab,data){
							$.each(data,function(key,val){
								
								if(key == "UPDATEKYC2_STATUS"){
									alert(val);
//									$(rowobj).closest('tr').find('td:eq(3) select:eq(0)').val("In-Progress");
								}
								if(key == "UPDATEKYC2_DSF_STATUS"){
									if(val.toLowerCase() == "success"){
										alert("Policy No. details are successfully downloaded")	;
									}
									
								} 
															
							});
							
						});					
					});
					
					
		      }
		});	
		
	}
	

	
}



function getSelectedFNADets(fnaId){

//	 var fnaId = $("#txtFldFnaId").val() 
		var custname = $("#txtFldCustName").val().toUpperCase();
		 var custid = $("#txtFldCustId").val(); 
		var advId = $("#txtFldAdvStfId").val(); 
		
		var kycformname = "SIMPLIFIED";
		var lobArrtoKYC = "ALL";
		var strLoggedUserDesig ="";
		var strLoggedDistrbutor=""; 
		var emailId = $("#txtFldAdvEmailId").val(); 
		var formType = "SIMPLIFIED";
		
		document.getElementById("txtFldCrtdUser").value= (strLoggedUserId)
		document.getElementById("hTxtFldFnaId").value = (fnaId);
		document.getElementById("hTxtFldReload").value="true";
		
		var url = "KycApprovalPre.action";
		document.forms["frmTempLogin"].action=url;
		document.forms["frmTempLogin"].submit();
}

function getSelectedFNADetsNew(dataset){
	

	var fnaId = dataset[1];
	var custname = dataset[2].toUpperCase();
	var custid = dataset[6]; 
	var advId = dataset[7]; 
	var emailId = dataset[8]; 
		
		
		var kycformname = "SIMPLIFIED";
		var lobArrtoKYC = "ALL";
		var strLoggedUserDesig ="";
		var strLoggedDistrbutor=""; 
		var formType = "SIMPLIFIED";
		
		document.getElementById("txtFldCrtdUser").value= (strLoggedUserId);
		document.getElementById("hTxtFldFnaId").value = (fnaId);
		document.getElementById("hTxtFldReload").value="true";
		
		var url = "KycApprovalPre.action";
		
		document.forms["frmTempLogin"].action= url;
		document.forms["frmTempLogin"].submit();
}


function openList(statusMsg){
	
  if(statusMsg == "PENDING"){
	  $("#yourAprovTabs").find("li:eq(0) a").trigger("click");	
    }
	if(statusMsg == "APPROVED"){
	$("#yourAprovTabs").find("li:eq(1) a").trigger("click");	
    }
    if(statusMsg == "REJECTED"){
	$("#yourAprovTabs").find("li:eq(2) a").trigger("click");
   }
}


function getAllNRICDocsList(custId){

	$.ajax({
		type : "POST",
		url : "KycUtilsServlet",
		data :{"dbcall":"FETCHNRICDOCUMENT","strcustId":custId},
		dataType : "json",
		async : true,
		beforeSend : function() {
//			$("#loadingLayer").show();
		},

		success : function(data) {
			var jsnResponse = data;
			
			 
			 if(!jsnResponse[0].NO_RECORDS){
				
				 var strNoRecords ="No Document found.";
				 $("#dynaAttachNRICList").append(strNoRecords);
				 
				  return;
			 }
			 
			$.each(jsnResponse[0].CLIENT_NRIC_DOCUMENT,function(i,obj){
			loadNRICDocJsnDataNew(obj,custId);
			});
			 
		},
		complete : function() {
			$("#loadingMessage").hide();
		},
		error : function() {
			$("#loadingMessage").hide();
		}
	});
	
}

function loadNRICDocJsnDataNew(obj,custId){
	
	var custAttchId = obj.txtFldCustAttachId;
    var strNricList = '<a class="list-group-item list-group-item-action">'+
	'<div class="d-flex w-100 justify-content-between "><h6 class="mb-1">Doc.Title : '+obj.txtFldTitle+'</h6> <small><i class="fa fa-download cursor-pointer"  style="color: #009A78;" id="btndownload" onclick="downLoadNtucAttach(\''+custAttchId+'\')" title="Click to Download Doc."></i></small></div>'+
    '<small>File Name : '+obj.txtFldFileName+'</small>  </a>';
	
	$("#dynaAttachNRICList").append(strNricList);	
}



function downLoadNRICAttachDoc(custAttchId){
	var url = 'DownloadCustNRICFile.action?txtFldNricCustId='+custAttchId;
	window.open(url,"_blank");
}


/*PIE CHART START*/
function sliceSize(dataNum, dataTotal) {
	  return (dataNum / dataTotal) * 360;
	}
	function addSlice(sliceSize, pieElement, offset, sliceID, color) {
	  $(pieElement).append("<div class='slice "+sliceID+"'><span></span></div>");
	  var offset = offset - 1;
	  var sizeRotation = -179 + sliceSize;
	  $("."+sliceID).css({
	    "transform": "rotate("+offset+"deg) translate3d(0,0,0)"
	  });
	  $("."+sliceID+" span").css({
	    "transform"       : "rotate("+sizeRotation+"deg) translate3d(0,0,0)",
	    "background-color": color
	  });
	}
	function iterateSlices(sliceSize, pieElement, offset, dataCount, sliceCount, color) {
	  var sliceID = "s"+dataCount+"-"+sliceCount;
	  var maxSize = 179;
	  if(sliceSize<=maxSize) {
	    addSlice(sliceSize, pieElement, offset, sliceID, color);
	  } else {
	    addSlice(maxSize, pieElement, offset, sliceID, color);
	    iterateSlices(sliceSize-maxSize, pieElement, offset+maxSize, dataCount, sliceCount+1, color);
	  }
	}
	function createPie(dataElement, pieElement) {
	  var listData = [];
	  $(dataElement+" span").each(function() {
	    listData.push(Number($(this).html()));
	  });
	  var listTotal = 0;
	  for(var i=0; i<listData.length; i++) {
	    listTotal += listData[i];
	  }
	  var offset = 0;
	  var color = [ "#333E48", 	    "#009A78",	    "#d9534f"  ];
	  for(var i=0; i<listData.length; i++) {
	    var size = sliceSize(listData[i], listTotal);
	    iterateSlices(size, pieElement, offset, i, 0, color[i]);
	    $(dataElement+" li:nth-child("+(i+1)+")").css("border-color", color[i]);
	    offset += size;
	  }
	}
/*PIE CHART END*/