/*
 * filterSearch Function is used search the the given value in the specified table
 * It has two arguments
 * formObj -- the object of the textfield
 * tableId -- in which table id has to be searched
 * 
 */
function filterSearch(formObj,tableId) {
	 
	var flag =0;	
	var searchs = trim(formObj);	
	var searchtbl = document.getElementById(tableId);
	var searchtBodyObj = searchtbl.tBodies[0];
    var rowLen = searchtBodyObj.rows.length;  
    var cellLen = searchtbl.tBodies[0].rows[0].cells.length;    
    if(searchs == "" || searchs == null){
    	alert("Enter Search Value ");
    	 for(var fsRow=0;fsRow<rowLen;fsRow++){
    		 
    		  for(var fsCol =0;fsCol<cellLen;fsCol++){
    	 		 var highlight = document.getElementById('highlightSearch');      
    	 		  
    	 	        if (highlight != null) {
    	 	            highlight.id = "";
    	 	        }		
    	 	  }
    		}      
    	return false;
    }
    
    if(searchs.length < 3){
    	alert("Enter More than 3 Characters ");    	
    	return false;
    }
        
    
    if(rowLen < 1){			
		alert("No rows Inserted ");
 		return false;
    }
    
    
    var value;
    
    for(var fsRow=0;fsRow<rowLen;fsRow++){
	  for(var fsCol =0;fsCol<cellLen;fsCol++){
 				
		 var highlight = document.getElementById('highlightSearch');      
 	        if (highlight != null) {
 	            highlight.id = "";
 	        }		
 	  }
	}       
  
 	for(var fsRow=0;fsRow<rowLen;fsRow++){
     	for(var fsCol=0;fsCol< cellLen ;fsCol++){    
     		
     			var childNodeLen = searchtBodyObj.rows[fsRow].cells[fsCol].childNodes.length;
     			
     			for(var child=0;child<childNodeLen;child++){
     				if(searchtBodyObj.rows[fsRow].cells[fsCol].childNodes[child].type=='select-one'){
    	 				var indexValue = searchtBodyObj.rows[fsRow].cells[fsCol].childNodes[child].selectedIndex;
    	 				value = searchtBodyObj.rows[fsRow].cells[fsCol].childNodes[child].options[indexValue].text.toUpperCase();	
    	 			}
    	 			else if(searchtBodyObj.rows[fsRow].cells[fsCol].childNodes[child].type=='text' || searchtBodyObj.rows[fsRow].cells[fsCol].childNodes[child].type=='hidden'){ 
    	 				value = searchtBodyObj.rows[fsRow].cells[fsCol].childNodes[child].value.toUpperCase();
    	 			}
         			 
         		//	value = searchtBodyObj.rows[fsRow].cells[0].childNodes[0].value.toUpperCase();
         			 
             		var matchpos = value.search(searchs.toUpperCase());
             		if(matchpos!= -1) {                 		
             			searchtBodyObj.rows[fsRow].id = "highlightSearch";             			
             			flag = 1;             			
             		}    
     			}
     		
	     		     		
     	}	
 	}
 	
 	if(flag == 0){
     	alert("No Matches Found ");
 	}
 		
 	
 }
