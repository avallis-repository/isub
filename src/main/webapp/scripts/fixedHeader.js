// Component for Fixed Header

 
var image_up = "images/arrow-up.png";
var image_down = "images/arrow-down.png";
var image_none = "images/arrow-none.gif";

	
	/*
	 * fixedHeader Function is component for creating the fixed header for the html table.
	 * It Contains 11 arguments 
	 * 		tbl -- id of the table as fixed header needs.
	 * 	    fixedcdn -- representing the big or smaller table.
	 * 		fixedWidth -- Div Width for given table.
	 * 		IE -- Width represented in pixels which subtracted from fixedWidth for IE Browser
	 * 		firefox -- Width represented in pixels which subtracted from fixedWidth for Netscape Browser
	 * 		fixedHeight -- height of the table
	 * 		cdnValue -- representing the big or smaller table for headContainwidth
	 * 		headContainwidth -- if cdnValue is 1 then headContainwidth is substituted representing the smaller table 
	 * 		screenValue -- if cdnValue is other than 1 then screenValue is assigned representing the bigger table for Netscape 
	 * 		headHeigth -- height of the cloned header representing the table header
	 * 		headContainwidthIE -- if cdnValue is other than 1 then headContainwidthIE is assigned representing the bigger table for IE
	 * 		sortFlag -- whether table requires sortable up and down image. if true the sortable image comes otherwise not.
	 * 		grpableFlag -- whether table is groupable or not.
	 * 		Cols -- Representing Number of Groupable Column no.
	 */

		function fixedHeader(tbl,fixedcdn,fixedWidth,IE,firefox,fixedHeight,cdnValue,headContainwidth,screenValue,headHeigth,headContainwidthIE,sortFlag,grpableFlag,grpableColn,Cols){
			 
				tblId = document.getElementById(tbl);
				
				var tableContainer = document.createElement("div");	
				tableContainer.id = tblId.id + "tableContainer";	
				if (getBrowserApp=="Netscape"){	  
					 
						if(tblId.id == 'verdettable' || tblId.id == 'kycapproval'){
							tableContainer.style.position = 'absolute';
						}else {
							tableContainer.style.position = 'relative';
						}
					  
				}
				if (getBrowserApp=="Microsoft Internet Explorer"){		
					tableContainer.style.position = 'relative';
				}		
								
				var fixedHead = document.createElement("div");
				fixedHead.id = tblId.id + "fixedHead";
				if (getBrowserApp=="Netscape"){	
					if(fixedcdn == '1'){
						fixedHead.style.width=firefox;			
					}
					else if(fixedcdn == '2'){
						fixedHead.style.width=fixedWidth-firefox + "px";
					}
					
				}
				else if (getBrowserApp=="Microsoft Internet Explorer"){
					if(fixedcdn == '1'){		
						fixedHead.style.width=fixedWidth;
					}
					else if(fixedcdn == '2'){
						fixedHead.style.width=fixedWidth-IE+ "px";			
					}
				}
				
				fixedHead.style.height=fixedHeight;
				fixedHead.style.left="0px";
				fixedHead.style.overflowY='scroll';	
				fixedHead.style.overflowX='auto';
				
				var headerContainer = document.createElement("div");
				headerContainer.id = tblId.id +"headerContainer";
				if (getBrowserApp=="Netscape"){ 	
					if(cdnValue == "1"){
						headerContainer.style.width = headContainwidth;			
					}
					else
						headerContainer.style.width = screenValue;
					
				}
				else if (getBrowserApp=="Microsoft Internet Explorer"){  
					
					if(cdnValue == "1")
						headerContainer.style.width = headContainwidth;
					else{
						headerContainer.style.width = headContainwidthIE;			
					}
				}
				
				headerContainer.style.overflow = 'hidden';	
				headerContainer.style.top = 0;	
				headerContainer.style.left = '0px';
				headerContainer.style.position = 'absolute';	
				headerContainer.style.height = headHeigth;	
				
				var headTbl = document.createElement("table");	
				var tblIdHead = tblId.getElementsByTagName("thead")[0];	
				var headerTable = tblIdHead.cloneNode(true);
				
				headTbl.id = tblId.id + "headerTable";
				headTbl.style.position = 'relative';
				headTbl.style.top = 0 + 'px';
				headTbl.style.left = '0';	
					
				headTbl.cellPadding="0";
				headTbl.cellSpacing="0";
				headTbl.border="1";	
				headTbl.style.borderLeft =  "1px Solid #CBCBCB"; 
				headTbl.style.borderStyle = "outset";
				headTbl.style.borderCollapse = "separate";
				
				if(tblId.id != "clntCompanyRelTable" /*&& tblId.id != "fnaartuplanTbl"*/){
					headTbl.style.tableLayout="fixed";
				}
				
				
				
				if(tblId.id == "fnaartuplanTblss"){
				fixedHead.style.overflowX='scroll';
				tblId.style.borderCollapse="collapse";
				document.getElementById("fnaartuplanTbl").style.borderCollapse="collapse";
				}
				
				if(tblId.id == "fnaswrepplanTblss"){
					fixedHead.style.overflowX='scroll';
					tblId.style.borderCollapse="collapse";
					document.getElementById("fnaswrepplanTbl").style.borderCollapse="collapse";
				}
				
				headTbl.appendChild(headerTable); 
				
				if(cdnValue == "2"){
					headTbl.width = tblId.width;		
				}
					
				tblId.parentNode.insertBefore(tableContainer, tblId);
				tblId.parentNode.insertBefore(fixedHead, tblId);
				fixedHead.parentNode.insertBefore(headerContainer, fixedHead);
				
				fixedHead.appendChild(tblId);
				tableContainer.appendChild(headerContainer);
				tableContainer.appendChild(fixedHead);	
				headerContainer.appendChild(headTbl);	
					
				fixedHead.onscroll = function(e){		
					headTbl.style.left = '-' + this.scrollLeft +'px';		
				};
				
				var thead = tblId.getElementsByTagName("thead")[0];	
				thead.style.visibility = "hidden";
				
//				if(tbl == "fnaDepnTbl"){
//					var tb = document.getElementById(tbl+"headerTable");
//					var headCell = tb.rows[0].cells.length;
//					var cellNum;
//					var span;
//					
//					 
//					for(var head=0;head<6;head++){
//						span = document.createElement("span"); 
//						cellNum = tb.getElementsByTagName("thead")[0].getElementsByTagName("th")[head];		
//						span.id = tbl +"imgSpan_"+head;					
//						span.innerHTML = "&nbsp;<img name='"+tbl+"imgs"+head+"' src='"+image_none+"'/>";
//						cellNum.appendChild(span);
//					}
//					 
//					span = document.createElement("span"); 
//					cellNum = tb.getElementsByTagName("thead")[0].getElementsByTagName("tr")[1].getElementsByTagName("th")[0];		
//					span.id = tbl +"imgSpan_"+head;					
//					span.innerHTML = "&nbsp;<img name='"+tbl+"imgs"+head+"' src='"+image_none+"'/>";
//					cellNum.appendChild(span);
//					
//					head++;
//					span = document.createElement("span"); 
//					cellNum = tb.getElementsByTagName("thead")[0].getElementsByTagName("tr")[1].getElementsByTagName("th")[1];			
//					span.id = tbl +"imgSpan_"+head;					
//					span.innerHTML = "&nbsp;<img name='"+tbl+"imgs"+head+"' src='"+image_none+"'/>";
//					cellNum.appendChild(span);
//				}	 
				
				if(tbl == "fnaFlowDetTbl"){
					var tb = document.getElementById(tbl+"headerTable");
					var headCell = tb.rows[1].cells.length;
					var cellNum;
					var span; 
					
					for(var head=0;head<5;head++){
						span = document.createElement("span"); 
						cellNum = tb.getElementsByTagName("thead")[0].getElementsByTagName("tr")[1].getElementsByTagName("th")[head];		
						span.id = tbl +"imgSpan_"+head;					
						span.innerHTML = "&nbsp;<img name='"+tbl+"imgs"+head+"' src='"+image_none+"'/>"; 
						cellNum.appendChild(span);
					}
				}
				
				if(tbl == "fnadepntfinanceTbl"){
					var tb = document.getElementById(tbl+"headerTable");
					var headCell = tb.rows[0].cells.length-1;
					var cellNum;
					var span;
					
					 
					for(var head=0;head<headCell;head++){
						span = document.createElement("span"); 
						cellNum = tb.getElementsByTagName("thead")[0].getElementsByTagName("th")[head];		
						span.id = tbl +"imgSpan_"+head;					
						span.innerHTML = "&nbsp;<img name='"+tbl+"imgs"+head+"' src='"+image_none+"'/>";
						cellNum.appendChild(span);
					}
					 
					span = document.createElement("span"); 
					cellNum = tb.getElementsByTagName("thead")[0].getElementsByTagName("tr")[1].getElementsByTagName("th")[0];		
					span.id = tbl +"imgSpan_"+head;					
					span.innerHTML = "&nbsp;<img name='"+tbl+"imgs"+head+"' src='"+image_none+"'/>";
					cellNum.appendChild(span);
					
					head++;
					span = document.createElement("span"); 
					cellNum = tb.getElementsByTagName("thead")[0].getElementsByTagName("tr")[1].getElementsByTagName("th")[1];			
					span.id = tbl +"imgSpan_"+head;					
					span.innerHTML = "&nbsp;<img name='"+tbl+"imgs"+head+"' src='"+image_none+"'/>";
					cellNum.appendChild(span);
				}
				
				if(tblId.id == "fnaartuplanTbl"){
					fixedHead.style.width='103%';
//					alert(getBrowserApp);
					var isIE = /*@cc_on!@*/false || !!document.documentMode;
					document.getElementById("fnaartuplanTblheaderTable").width="1316"
					if (isIE){ 
					document.getElementById("fnaartuplanTblheaderTable").width="1317"
					}	
					var tablerows=document.getElementById("fnaartuplanTblheaderTable").tHead.rows[0].children;
					tablerows[0].width="107";
					tablerows[1].width="500";
					tablerows[2].width="117";
					tablerows[3].width="116";
					tablerows[4].width="126";
					tablerows[5].width="126";
					tablerows[6].width="207";
				}
				
				if(tblId.id=="fnaClntAtchTbl"){
					document.getElementById("fnaClntAtchTblheaderTable").style.borderCollapse="collapse";
				}
				
				if(tblId.id=="KycAttachTbl"){
					document.getElementById("KycAttachTblheaderTable").style.borderCollapse="collapse";
				}
				
				
		}//End Function
		
		
		 