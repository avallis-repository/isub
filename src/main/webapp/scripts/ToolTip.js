/**
 * SCI
 * All Rights Reserved. Copyright (C) 2010, First Principal Financial Pte Ltd.
 * File   : ToolTip.js
 * Author : Sathish Kumar / J.Selva Kumar
 * Date   : 2010/07/15
 *
 */

	var dhtmlgoodies_tooltip = false;
	var dhtmlgoodies_tooltipShadow = false;
	var dhtmlgoodies_shadowSize = 4;
	var dhtmlgoodies_tooltipMaxWidth = 200;
	var dhtmlgoodies_tooltipMinWidth = 100;
	var dhtmlgoodies_iframe = false;
	var tooltip_is_msie = (navigator.userAgent.indexOf('MSIE')>=0 && navigator.userAgent.indexOf('opera')==-1 && document.all)?true:false;
	
	function IsEmpty(str){
		if(str == null || str.length == 0)return true;
		else return false;
	}
	
	//This method is used to show the tooltip for combo box, added on 16/10/2012
	function showTooltipCombo(e,selbox){
				
		if(!selbox.disabled){			
			selbox.title = "";
		}
		
		if(!IsEmpty(selbox.value)){
			var tt_txt = selbox.options[selbox.selectedIndex].text;
			showTooltip(e,tt_txt);
		}
	}//End of showTooltipCombo
	
	function showTooltip(e,tooltipTxt)
	{
		if(!IsEmpty(tooltipTxt)) {

		var bodyWidth = Math.max(document.body.clientWidth,document.documentElement.clientWidth) - 20;
	
		if(!dhtmlgoodies_tooltip){
			dhtmlgoodies_tooltip = document.createElement('DIV');
			dhtmlgoodies_tooltip.id = 'tooltip';
			dhtmlgoodies_tooltipShadow = document.createElement('DIV');
			dhtmlgoodies_tooltipShadow.id = 'tooltipShadow';
			
			document.body.appendChild(dhtmlgoodies_tooltip);
			document.body.appendChild(dhtmlgoodies_tooltipShadow);	
			
			if(tooltip_is_msie){
				dhtmlgoodies_iframe = document.createElement('IFRAME');
				dhtmlgoodies_iframe.frameborder='5';
				dhtmlgoodies_iframe.style.backgroundColor='#FFFFFF';
				dhtmlgoodies_iframe.src = '#'; 	
				dhtmlgoodies_iframe.style.zIndex = 100;
				dhtmlgoodies_iframe.style.position = 'absolute';
				document.body.appendChild(dhtmlgoodies_iframe);
			}
			
		}
		
		dhtmlgoodies_tooltip.style.display='block';
		dhtmlgoodies_tooltipShadow.style.display='block';
		if(tooltip_is_msie)dhtmlgoodies_iframe.style.display='block';
		
		var st = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
		if(navigator.userAgent.toLowerCase().indexOf('safari')>=0)st=0; 
		var leftPos = e.clientX + 10;
		
		var txtValue="";
		var len = tooltipTxt.length, lc = 0, chunks = [], c = 0, chunkSize = 80; 
		for (; lc < len; c++) { 
			txtValue += tooltipTxt.slice(lc, lc += chunkSize) + "\n";
		} 
		
		dhtmlgoodies_tooltip.style.width = null;	// Reset style width if it's set 
		dhtmlgoodies_tooltip.innerHTML = txtValue;
		
		dhtmlgoodies_tooltip.style.left = leftPos + 'px';
		dhtmlgoodies_tooltip.style.top = e.clientY + 10 + st + 'px';

		
		dhtmlgoodies_tooltipShadow.style.left =  leftPos + dhtmlgoodies_shadowSize + 'px';
		dhtmlgoodies_tooltipShadow.style.top = e.clientY + 10 + st + dhtmlgoodies_shadowSize + 'px';
		
		if(dhtmlgoodies_tooltip.offsetWidth>dhtmlgoodies_tooltipMaxWidth){	/* Exceeding max width of tooltip ? */
			dhtmlgoodies_tooltip.style.width = dhtmlgoodies_tooltipMaxWidth + 'px';
		}
		
		var tooltipWidth = dhtmlgoodies_tooltip.offsetWidth;		
		if(tooltipWidth<dhtmlgoodies_tooltipMinWidth)tooltipWidth = dhtmlgoodies_tooltipMinWidth;
		
		
		dhtmlgoodies_tooltip.style.width = tooltipWidth + 'px';
		dhtmlgoodies_tooltipShadow.style.width = dhtmlgoodies_tooltip.offsetWidth + 'px';
		dhtmlgoodies_tooltipShadow.style.height = dhtmlgoodies_tooltip.offsetHeight + 'px';		
		
		if((leftPos + tooltipWidth)>bodyWidth){
			dhtmlgoodies_tooltip.style.left = (dhtmlgoodies_tooltipShadow.style.left.replace('px','') - ((leftPos + tooltipWidth)-bodyWidth)) + 'px';
			dhtmlgoodies_tooltipShadow.style.left = (dhtmlgoodies_tooltipShadow.style.left.replace('px','') - ((leftPos + tooltipWidth)-bodyWidth) + dhtmlgoodies_shadowSize) + 'px';
		}
		
		if(tooltip_is_msie){
			dhtmlgoodies_iframe.style.left = dhtmlgoodies_tooltip.style.left;
			dhtmlgoodies_iframe.style.top = dhtmlgoodies_tooltip.style.top;
			dhtmlgoodies_iframe.style.width = dhtmlgoodies_tooltip.offsetWidth + 'px';
			if(IsEmpty(tooltipTxt))
			{
				dhtmlgoodies_iframe.style.width = '0px';
				dhtmlgoodies_tooltip.style.width = null;
				//dhtmlgoodies_iframe.style.width = null;
				dhtmlgoodies_tooltipShadow.style.width = null;
			}
			dhtmlgoodies_iframe.style.height = dhtmlgoodies_tooltip.offsetHeight + 'px';
		
		}
	}
	}
	
	function hideTooltip()
	{	
		if(dhtmlgoodies_tooltip!=false) {
			dhtmlgoodies_tooltip.style.display='none';
			dhtmlgoodies_tooltipShadow.style.display='none';		
			if(tooltip_is_msie)dhtmlgoodies_iframe.style.display='none';		
		}
	}
	
	function showToolTipComboTbl(selobj){
		
		if(!IsEmpty(selobj.value)){
			selobj.title = selobj.options[selobj.selectedIndex].text;
		}
	}
