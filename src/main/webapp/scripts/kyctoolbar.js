
var blnkBtnObj = new YAHOO.widget.Button({
    id: "blankFormBtn", 
    type: "button", 
    label: "", 
    container: "toolbar"
});

blnkBtnObj.on("click", printBlankForm);
var blnkTxt = new YAHOO.widget.Tooltip("blnkTxt", { context:"blankFormBtn", text:"PRINT BLANK FORM" });
blnkBtnObj.setStyle('display', 'none');


var saveBtnObj = new YAHOO.widget.Button({
    id: "saveBtn", 
    type: "button", 
    label: "", 
    container: "toolbar" 
    	
});
saveBtnObj.on("click", insertkycdets);
var saveBtnTxt = new YAHOO.widget.Tooltip("saveBtnTxt", { context:"saveBtn", text:"SAVE FNA FORM" });	
saveBtnObj.setStyle('display', 'none');


var reportBtnObj = new YAHOO.widget.Button({
    id: "reportBtn", 
    type: "button", 
    label: "", 
    container: "toolbar"
    
    	
});
reportBtnObj.on("click", genreportkyc);
var reportBtnTxt = new YAHOO.widget.Tooltip("reportBtnTxt", { context:"reportBtn", text:"GENERATE PDF" });	
reportBtnObj.setStyle('display', 'none');


var ntucBtnObj = new YAHOO.widget.Button({
    id: "ntucBtn", 
    type: "button", 
    label: "", 
    container: "toolbar" 
    	
});

ntucBtnObj.on("click",ntucArchiveClick);
var ntucBtnObjTxt = new YAHOO.widget.Tooltip("ntucBtnTxt", { context:"ntucBtn", text:"E-Submission to NTUC Income" });	
ntucBtnObj.setStyle('display', 'none');


var logoutBtnObj = new YAHOO.widget.Button({
    id: "logoutBtn", 
    type: "button", 
    label: "", 
    container: "toolbar"
});
logoutBtnObj.on("click", logoutKyc);
var logoutTxt = new YAHOO.widget.Tooltip("logoutTxt", { context:"logoutBtn", text:"LOGOUT" });	

var wsBtnObj = new YAHOO.widget.Button({
    id: "wsBtn", 
    type: "button", 
    label: "", 
    container: "toolbar" 
    	
});
wsBtnObj.on("click", callWebServices);
var wsBtnTxt = new YAHOO.widget.Tooltip("wsBtnTxt", { context:"wsBtn", text:"CALL WEB SERVICES" });	
wsBtnObj.setStyle('display', 'none');


var wsClientBtnObj = new YAHOO.widget.Button({
    id: "wsClientBtn", 
    type: "button", 
    label: "", 
    container: "toolbar" 
    	
});
//wsClientBtnObj.on("click", callExportViaAjax);
wsClientBtnObj.setStyle('display', 'none');
var wsClientBtnTxt = new YAHOO.widget.Tooltip("wsClientBtnTxt", { context:"wsClientBtn", text:"EXPORT DOCUMENT" });


//GOJS TEST
var clientSignBtnObj = new YAHOO.widget.Button({
    id: "clientSignBtn", 
    type: "button", 
    label: "", 
    container: "toolbar" 
    	
});

clientSignBtnObj.on("click",openSignaturePad);
var clientSignBtnObjTxt = new YAHOO.widget.Tooltip("clientSignBtnTxt", { context:"clientSignBtn", text:"eSignature" });	
clientSignBtnObj.setStyle('display', 'none');