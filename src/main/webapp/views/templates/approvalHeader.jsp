<nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top" id="navbar-bg">
          <h1 class="h3 mb-0 text-gray-800" id="goPreviousArrow">
				<a  href="index" class="btn btn-sm btn-default" style="color:white"><i class="fa fa-arrow-left"></i></a>
			</h1>
          <ul class="navbar-nav ml-auto">
           
            <li class="nav-item dropdown no-arrow" id="searchIcon">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in"
                aria-labelledby="searchDropdown">
                <form class="navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-1 small checkdb" placeholder="Search for client details?"
                      aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;" maxlength="60">
                    <div class="input-group-append">
                      <button class="btn btn-sm btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>
          
            <div class="topbar-divider d-none d-sm-block"></div>
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                aria-haspopup="true" aria-expanded="false">
                <input type="hidden" id="hTxtFldSessionTimeout" value=<% out.print(session.getAttribute("sessionTimeout")); %> />
                <img class="img-profile rounded-circle" src="vendor/psgisubmit/img/users/client.png" 
                        style="max-width: 60px" onerror="this.onerror=null;this.src='vendor/psgisubmit/img/users/client.png';">
                <span class="ml-2 d-none d-lg-inline text-white small"><% out.print(session.getAttribute("adviserName")); %></span>
                <!-- <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown" style="display:inline-block">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><strong>Salim <span id="spanLoggedAdviser"></span></strong></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item">Reports</a>
                        <a href="#" class="dropdown-item">Settings</a>
                        <div class="dropdown-divider"></div>
                        <a href="#"class="dropdown-item">Logout</a>
                    </div>
                </li>
            </ul> -->
              </a>
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                
                <a href="#" class="dropdown-item" title="Generate FNA Doc." onclick="generateFNA();" data-toggle="modal" data-target="#PDFReportModal" data-backdrop="static">
                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>Generate FNA</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal" data-backdrop="static">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>
          </ul>
        </nav>