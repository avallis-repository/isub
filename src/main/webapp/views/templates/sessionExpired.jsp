<!doctype html>
                        <html>
                            <head>
                                <meta charset='utf-8'>
                                <meta name='viewport' content='width=device-width, initial-scale=1'>
                                <title>Session Expired</title>
                                <link href="vendor/avallis/img/logo/logo.png" rel="icon">
                                <link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
                                <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
                                <link rel="stylesheet" href="vendor/avallis/css/ekyc-error.css">
                            </head>
                            <body>
								
								<div class="row">
        <div class="container d-flex justify-content-center">
            <div class="col-md-8" style="margin-top:10%;">
                <div class="card text-center">
                    <div class="card-body" id="sessCrdBdy"> 
                         <div class="row">
                             <div class="col-4">
                             <img src="vendor/avallis/img/logo/logo.png" class="imgsyle" alt="AvallisLogo">
                             </div>
                        </div>
                        
                        <div class="row">
                             <div class="col-4"></div>
                             <div class="col-4"><img src="vendor/avallis/img/sessionError.png" alt="Session Expired"></div>
                             <div class="col-4"></div>
                        </div>
                        
                        <div class="row">
                             <div class="col-2"></div>
                             <div class="col-8"> <h3 class="title">Session Expired</h3></div>
                             <div class="col-2"></div>
                       </div>
                       
                        <div class="row">
                             <div class="col-1"></div>
                             <div class="col-10"><h5 class="description">Your Session has been Expired, Please Try Again !</h5></div>
                             <div class="col-1"></div>
                       </div>
                        
                       <div class="row  mt-3">
							 <div class="col-4"></div>
							<div class="col-4 mb-2 pl-3 pr-3"><button class="btn btn-block btn-bs3-prime p-2"  onclick="reLogin();"> <i class="fa fa-user"></i> Login Again </button> </div>
                            <div class="col-4"></div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
					 </body>
                            
                  <script src="vendor/jquery/jquery.js"></script>
                  <script src="vendor/bootstrap453/js/bootstrap.js"></script>
                  <script>
                  function reLogin(){
                	 window.location.href="relogin"; 
                  }
                  </script>
                        </html>
