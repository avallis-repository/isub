<nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top" id="navbar-bg">
<h1 class="h3 mb-0 text-gray-800" id="goPreviousArrow">
				<a href="index" id="backToIndexLink" class="btn btn-sm btn-default" style="color:white"><i class="fa fa-arrow-left"></i> Search / Add New Client</a>
			</h1>
                  <button id="sidebarToggleTop hide" class="btn btn-link rounded-circle mr-3 hide">
                     <i class="fa fa-bars hide"></i> 
                  </button>
                 
           <ul class="navbar-nav ml-auto navbar-fixed-top ">
            
            <li class="nav-item dropdown no-arrow" id="tenantadviserLink">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                       <%--  <% out.print(session.getAttribute("adviserName")); %> --%>
                        <span class="ml-2 d-none d-lg-inline text-white small">Tenant / Advisers</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        	<a class="dropdown-item" href="masterTenantDist" >
                           		<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                           		Tenant
                           	</a>
                            <div class="dropdown-divider"></div>
                        	<a class="dropdown-item" style="cursor: pointer" href="masterAdviserManagement">
                           		<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                           		Advisers
                           	</a>
                           	</div>
                           	  
                        
                     </li>
                      
                   <!-- <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" onclick="openSrchScreen()" id="searchDropdown" role="button" title="Click to Search/Add Clien Details Sectiont">
                        <i class="fas fa-search fa-fw" title="Click to Search/Add Clien Details Sectiont"></i>&nbsp;
                        </a>
                      
                     </li> -->
                     <div class="topbar-divider d-none d-sm-block"></div>
                     <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               	 	<input type="hidden" id="hTxtFldSessionTimeout" value=<% out.print(session.getAttribute("sessionTimeout")); %> />
               	 	<input type="hidden" id="hTxtFldUserType" value=<% out.print(session.getAttribute("userType")); %> />
					<img class="img-profile rounded-circle" src="vendor/psgisubmit/img/users/client.png" 
		                        style="max-width: 60px" onerror="this.onerror=null;this.src='vendor/psgisubmit/img/users/client.png';">
                        
                        
                        <span class="ml-2 d-none d-lg-inline text-white small"><% out.print(session.getAttribute("AdviserName")); %></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        	<a class="dropdown-item" href="#" data-toggle="modal" data-target="#profileModal">
                           		<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                           		Profile
                           	</a>
                           	<!-- <a class="dropdown-item" href="#">
                           	<i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                           	Settings
                           	</a>
                           	<a class="dropdown-item" href="#">
                           	<i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                           	Activity Log
                           	</a> -->
                        	<a class="dropdown-item" id="adviserLink" style="cursor: pointer" href="adviserInfo">
                           		<i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                           		Advisers
                           	</a>
                           	<div class="dropdown-divider"></div>
                           	<a class="dropdown-item" href="javascript:void(0);" data-toggle="modal" data-target="#logoutModal">
                           		<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                           		Logout
                           	</a>
                        </div>
                     </li>
                  </ul>         
                  
               </nav>
               <script>
					let userType = $("#hTxtFldUserType").val();
		           	if (userType === 'Admin') {
		           		// $("#backToIndexLink").html("<i class='fa fa-arrow-left'></i> Tenant/Adviser Home");
		           		$("#tenantadviserLink").show();
		           		$("#adviserLink").show();
		           		/* $("#tenantadviserLink").hide();
		           		$("#adviserLink").hide(); */
		           	} else {
		           		$("#adviserLink").hide();
		           		$("#tenantadviserLink").hide();
		           		// $("#tenandadviserbar").hide();
		           	}
               </script>
               
        