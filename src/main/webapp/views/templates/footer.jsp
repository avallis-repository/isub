<footer class="sticky-footer bg-white p-3">
	<div class="row">
		<div class="col-3">
		    <input type="hidden" value="${PREV_SCREEN}" id="applPrevScreen"/>
			<a class="btn pervNxtBtnstyle btn-sm ltrspc02"  id="btnPrevFormUniq" onclick="goPrevPage(this,'${PREV_SCREEN}')"  title="Go Previous Page"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>&nbsp;Prev</a>
		</div>
		<div class="col-6 copyright text-center">
			<span> � <script> document.write(new Date().getFullYear()); </script> - <strong>Association of Financial Advisers Pte Ltd.,</strong>
			</span>
		</div>
		<div class="col-3">
		     
		     <!--ekyc key Validation Sec Start  -->
		     <div class="floating-chat">
	           <i class="fa fa-question-circle-o" aria-hidden="true" style="color: #46f5edf2;"></i>
	              <div class="chat">
				        <div class="header">
				            <span class="title">
				             <i class="fa fa-info-circle fa-2x" aria-hidden="true" style="color: #75cbfd;"></i> Key Information - Missing
				            </span>
				            <button class="float-right">
				                <i class="fa fa-times" aria-hidden="true"></i>
				            </button>
				                         
				        </div>
				        <ul class="messages" id="ekycValidMessageList"></ul>
				    </div>
	           </div>
	           <!--key validation end  -->
		
			<input type="hidden" value="${NEXT_SCREEN}" id="applNextScreen"/>
			 <a class="btn pervNxtBtnstyle btn-sm right ltrspc02"  id="btnNextFormUniq" onclick="saveData(this,'${NEXT_SCREEN}',true,true)" title="Go Next Page">&nbsp;Next&nbsp;<i class="fa fa-long-arrow-right" aria-hidden="true"></i></a>
		</div>
	</div>
</footer>

<script>
var nextscreen = $("#applNextScreen").val();
if(nextscreen == "FINISHED"){
	$("#btnNextFormUniq").html('&nbsp;Finish&nbsp;<i class="fa fa-check-circle-o" aria-hidden="true"></i>');
}
</script>