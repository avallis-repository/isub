<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Error Page</title>
<link rel="stylesheet" href="vendor/psgisubmit/img/logo/logo.png" rel="icon">
<link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
<link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
<link rel="stylesheet" href="vendor/psgisubmit/css/ekyc-error.css">
</head>
<body>
	<div class="row">
        <div class="container d-flex justify-content-center">
            <div class="col-md-8" style="margin-top:10%;">
                <div class="card text-center">
                    <div class="card-body" id="sessCrdBdy"> 
                         <div class="row">
                             <div class="col-4">
                             <img src="vendor/psgisubmit/img/logo/logo.png" class="imgsyle" alt="afasLogo">
                             </div>
                        </div>
                        
                        <div class="row">
                             <div class="col-4"></div>
                             <div class="col-4"><img src="vendor/psgisubmit/img/error.svg" alt="Page Not Found" style="width: 150px; height: 150px;"></div>
                             <div class="col-4"></div>
                        </div>
                        
                        <div class="row">
                             <div class="col-2"></div>
                             <div class="col-8"> <h3 class="title">${ errorReason } - ${ errorTitle }</h3></div>
                             <div class="col-2"></div>
                       </div>
                       
                        <div class="row">
                             <div class="col-1"></div>
                             <div class="col-10"><h5 class="description">${ errorDesc }</h5></div>
                             <div class="col-1"></div>
                       </div>
                        
                       <div class="row  mt-3">
							 <div class="col-4"></div>
							<div class="col-4 mb-2 pl-3 pr-3"><button class="btn btn-block btn-bs3-prime p-2" onclick="window.history.go(-1);"> <i class="fa fa-long-arrow-left"></i> Back </button> </div>
                            <div class="col-4"></div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
					 </body>
                            
                  <script src="vendor/jquery/jquery.js"></script>
                  <script src="vendor/bootstrap453/js/bootstrap.js"></script>
                  <script>
                  
                  function close_window() {
                 	  if (confirm("Close Window?")) {
                 		  window.close();
                 	  }
                 	}
                  </script>
</html>