<%@ page language="java" import="com.afas.isubmit.util.PsgConst" %>
<html>
<head>
<meta charset="ISO-8859-1">

<div id="wrapper">

<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar" style="background-color:#1d655cf7 !important">
		<li class="nav-item">
			<a class="sidebar-brand d-flex align-items-center justify-content-center">
               <span class="sidebar-brand-icon bg-white" style="border-radius:10px">
                  <img src="vendor/psgisubmit/img/logo/logo.png">
               </span>
              <!--  <span class="sidebar-brand-text mx-3">eKYC</span> -->
             </a>
             
           <div class="sidebar-heading d-flex justify-content-center mt-n2">
               <span class=" font-sz-level6">FNA - V4.1 </span>
            </div>
			
			<hr class="sidebar-divider">
            <div class="sidebar-heading">
              <small class="white">FNA Id : <%=session.getAttribute(PsgConst.SESS_FNA_ID)%></small><br>
            </div>
            
			<hr class="sidebar-divider">
            <div class="sidebar-heading">
	     <div class="row">
               <div class="col-12">
                 <span><small>Client(1) :</small> <br/><span class="o_ellipsis" id="MenuselfName"><%=session.getAttribute("CustomerName")%></span></span>
            		</div>
            		<div class="col-12 mt-2">
            		<%--  <%if(session.getAttribute(PsgConst.SESS_SPOUSE_NAME) !=null){%>
            			<span><small>Client(2):</small> <br><span id="MenuSpsName"><%=session.getAttribute(PsgConst.SESS_SPOUSE_NAME) %></span></span>
            		<%}%> --%>
            		<c:if test = "${SESS_DF_SPS_NAME ne null}">
            		<span><small id="clnt2Text">Client(2):</small> <br><span class="o_ellipsis" id="MenuSpsName">${SESS_DF_SPS_NAME}</span></span>
            		</c:if>
            		</div>
            		
            		
            	</div>
              
            </div>
            
            
            
			
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
            </div>
</li>

            
          
             <li class="nav-item">
               <a class="nav-link" href="kycHome" >
               <i class="fas fa-fw fa-user"></i>
               <span>Personal Details</span>
               </a>
            </li>
            
            
              <li class="nav-item">
               <a class="nav-link" href="taxResidency">
               <i class="fas fa-fw fa-chart-area"></i>
               <span>Tax Residency</span>
               </a>
            </li>
			
			 <!--<hr class="sidebar-divider">-->
			 
			 
            <li class="nav-item">
               <a class="nav-link" href="finanWellReview">
               <i class="fas fa-fw fa-plus"></i>
               <span>Finan. Wellness Review</span>
               </a>
            </li>
			
			  <!--<hr class="sidebar-divider">-->
            <li class="nav-item d-none">
               <a class="nav-link" href="#">
               <i class="fas fa-fw fa-chart-area"></i>
               <span>Investment Objectives</span>
               </a>
            </li>
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
               Recommendations
            </div>
            <li class="nav-item">
               <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePageLife" aria-expanded="true" aria-controls="collapsePageLife">
                  
               <i class="fas fa-fw fa-life-ring"></i>
               <span>Life&nbsp;&amp;&nbsp;ILP</span>
               </a>
               <div id="collapsePageLife" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                     <a class="collapse-item" href="productRecommend">New Purchase</a>
                     <a class="collapse-item" href="productSwitchRecomm">Switching</a>
                  </div>
               </div>
            </li>
            
            
             <!-- <li class="nav-item">
               <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePageUT" aria-expanded="true" aria-controls="collapsePageUT">
               <i class="fas fa-fw fa-line-chart"></i>
               <span>UT</span>
               </a>
               <div id="collapsePageUT" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                  <div class="bg-white py-2 collapse-inner rounded">
                     <a class="collapse-item"  id="linkekycinvaddnew">UT - EKYC IAS</a>
                     <a class="collapse-item" id="linkekycinvsummary">Summary</a>
                  </div>
               </div>
            </li> -->
            
		    <hr class="sidebar-divider">
			     <div class="sidebar-heading">
	                  Advice
	            </div>
	            <li class="nav-item">
	               <a class="nav-link"  href="adviceBasisRecomm">
	               <i class="fas fa-fw fa-comments"></i>
	               <span>Advice &amp; Basis of Recom.</span>
	               </a>
	            </li>
		 
           
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                Declaration
            </div>
           
            <li class="nav-item">
               <a class="nav-link" href="clientSignature">
               <i class="fas fa-fw fa-user"></i>
               <span>Client Signature</span>
               </a>
            </li>
			
			<li class="nav-item">
               <a class="nav-link" href="clientDeclaration">
               <i class="fas fa-fw fa-users"></i>
               <span>Client Decl.</span>
               </a>
            </li>
			
			  <!--<hr class="sidebar-divider">-->
            <li class="nav-item">
               <a class="nav-link" href="managerDeclaration">
              <i class="fas fa-fw fa-user-secret"></i>
               <span>Adv&Mgr Decl.</span>
               </a>
            </li>
            <hr class="sidebar-divider">
            <div class="version" id="version-div"></div>
         </ul>
         </div>
</body>
</html>