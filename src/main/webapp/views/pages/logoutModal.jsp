<body>
	<!-- Modal Logout -->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog"
		aria-labelledby="exampleModalLabelLogout" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" id="cardHeaderStyle-22">
					<h5 class="modal-title" id="exampleModalLabelLogout">iSubmit</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close" style="color: #fff;">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p>Are you sure you want to logout?</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-bs3-prime"
						data-dismiss="modal">Cancel</button>
					<a href="logout" class="btn btn-sm btn-bs3-prime">Logout</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Logout modal end -->
	