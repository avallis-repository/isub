<%@page import="com.afas.isubmit.util.PsgConst"%>
<div class="row">
    		  <div class="col-md-12">
							
							<!-- <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="horizontal" style="margin-top: 2%;">
							       <a id="mgrTab" class="nav-link font-sz-level5 appr-sec-tab bold" data-toggle="pill" href="#pop1" role="tab" >Manager&nbsp;</a> 
							       <a id="adminTab" class="nav-link font-sz-level5 appr-sec-tab bold " data-toggle="pill" href="#pop2" role="tab" >Admin&nbsp;</a>
							       <a id="compTab" class="nav-link font-sz-level5 appr-sec-tab bold " data-toggle="pill" href="#pop3" role="tab" >Compliance&nbsp;</a>
							</div>		 -->	
							<!-- <ul class="nav nav-pills nav-stacked">
   								<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu 1 <span class="caret"></span></a>
	                               <ul class="dropdown-menu">  <li> <a id="mgrTab" class="nav-link font-sz-level5 appr-sec-tab bold" data-toggle="pill" href="#pop1" role="tab" >Manager&nbsp;</a></li>
	                                 <li><a id="adminTab" class="nav-link font-sz-level5 appr-sec-tab bold " data-toggle="pill" href="#pop2" role="tab" >Admin&nbsp;</a></li>
	                                 <li><a id="compTab" class="nav-link font-sz-level5 appr-sec-tab bold " data-toggle="pill" href="#pop3" role="tab" >Comp.&nbsp;</a></li>
	                                </ul> 
	                              </li>
	                              </ul>       -->                      
								<ul class="nav nav-tabs " id="viewTab" role="tablist">
	                                 <li class="nav-item" title="Manager"> <a id="mgrTab" class="nav-link font-sz-level5 appr-sec-tab bold" data-toggle="pill" href="#pop1" role="tab" >Mgr&nbsp;</a></li>
	                                 <li class="nav-item" title="Admin"><a id="adminTab" class="nav-link font-sz-level5 appr-sec-tab bold " data-toggle="pill" href="#pop2" role="tab" >Adm.&nbsp;</a></li>
	                                 <li class="nav-item" title="Compliance"><a id="compTab" class="nav-link font-sz-level5 appr-sec-tab bold " data-toggle="pill" href="#pop3" role="tab" >Comp.&nbsp;</a></li>
	                              </ul>
                			<!-- </div> 
                		<div class="col-md-12">	 -->
                		<div class="tab-content" id="nav-tabContent">
                  			<div class="tab-pane fade" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
                  			
                       <div class="card" style="min-height:50vh;">
						   <div class="card-header" id="cardHeaderStyle">
						  	  <span class="font-sz-level7">Manager : Salim</span>
						  	  <i class="fas fa-file-signature" title="Edit Signature" aria-hidden="true" style="float: right;color:#0044ccfa;" onclick="showSignModal();"></i>
          				   </div>
					     <div class="card-body">
						    <div class="row">
						      <div class="col-md-12">
								   <label for="" class="lbl-font">Application Status :</label>
								    <select class="form-control checkdb" id="mgrApproveStatus" name="mgrApproveStatus" style="margin-bottom: 10%;" onchange="enableTxtArea();" disabled>
								         <option value="">--Select--</option> 
								        <option value="APPROVE">Approved</option>
								        <option value="REJECT">Rejected</option>
								        <option value="PENDING">Pending</option>
								    </select>
								    
								    <label for="" class="lbl-font">Remarks:</label>
							  <textarea class="form-control checkdb" id="mgrStatusRemarks" name="mgrStatusRemarks" placeholder="Enter remarks here..." style="height:50%" disabled maxlength="300"></textarea>
							  <input type="button" id="mgrBtn" class="mail-btn" value="Send mail to Admin" onclick="showModal();" disabled>
						   	 </div>
						   
						  
						   </div>                                        
						 </div>
				   </div>  
               </div>
                  <div class="tab-pane fade" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
                     
                   <div class="card" style="min-height:50vh;">
						<div class="card-header" id="cardHeaderStyle">
						  	<span class="font-sz-level7">Admin : Salim</span></div>
					     <div class="card-body">
						    <div class="row">
						      <div class="col-md-12">
								   <label for="" class="lbl-font">Application Status :</label>
								    <select class="form-control checkdb" id="adminApproveStatus" name="adminApproveStatus" style="margin-bottom: 10%;" onchange="enableAdminTxtArea();" disabled> 
								        <option value="APPROVE">Approved</option>
								        <option value="REJECT">Rejected</option>
								        <option value="PENDING">Pending</option>
								    </select>
								    
								    <label for="" class="lbl-font">Remarks:</label>
							  <textarea class="form-control checkdb" id="adminRemarks" name="adminRemarks" placeholder="Enter remarks here..." style="height:50%" disabled maxlength="300"></textarea>
							  <input type="button" id="adminBtn" value="Send mail to Compliance" onclick="showModal();" class="mail-btn" disabled>
						   	 </div>
						   
						   </div>                                        
						 </div>
				   </div>  
		
                  </div>
                  <div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
                       <div class="card" style="min-height:50vh;">
						<div class="card-header" id="cardHeaderStyle">
						  	<span class="font-sz-level7">Comp. : Salim</span></div>
					     <div class="card-body">
						    <div class="row">
						      <div class="col-md-12">
								   <label for="" class="lbl-font">Application Status :</label>
								    <select class="form-control checkdb" id="compApproveStatus" name="compApproveStatus" style="margin-bottom: 10%;" onchange="enableCompTxtArea();" disabled> 
								        <option value="APPROVE">Approved</option>
								        <option value="REJECT">Rejected</option>
								        <option value="PENDING">Pending</option>
								    </select>
								    
								    <label for="" class="lbl-font">Remarks:</label>
							  <textarea class="form-control checkdb" id="compRemarks" name="compRemarks" placeholder="Enter remarks here..." style="height:50%" disabled maxlength="300"></textarea>
							  <input type="button" id="compBtn" value="Send mail to Advisor" onclick="showModal();" class="mail-btn" disabled>
						   	 </div>
						   
						   </div>                                        
						 </div>
				   </div>
		</div>
		
		
                  </div>
                  
                </div>
            </div>

		



	

