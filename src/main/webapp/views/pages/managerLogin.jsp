<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Manager Login</title>
  <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="css/app/login.css">
  <link rel="stylesheet" href="vendor/psgisubmit/css/login.css">
  <link rel="stylesheet" href="vendor/SweetAlert2/css/sweetalert2.min.css">
  <script src="vendor/SweetAlert2/js/sweetalert2.min.js"></script>
  <script src="vendor/jquery/jquery.js"></script>
  <script src="vendor/jquery/popper.min.js"></script>
  <script src="vendor/bootstrap453/js/bootstrap.js"></script>
</head>
<body >
  <main class="d-flex align-items-center min-vh-100 py-3 py-md-0">
    <div class="container">
      <div class=" " style="background-color:transparent">
      
        <div class="row no-gutters">
          <div class="col-6" style="background-color:transparent">
             &nbsp;
          </div>
                    
          <div class="col-6  ">
                  
            <div class="loginBox">
            
          
  
  <form name="mngrLoginForm" method="POST" autocomplete="off">
	 
	<div class="row" style=" background: white;border-radius: 10px;color: #1d655cf7;">
               <div class="col-5">
                
					<img src="img/logo.png" alt="AFAS Logo" style="height :60px;">
               
               </div>
               
               <div class="col-7 center">
                   Sign in to your account  
               </div>
               
            </div>
	 
	
			<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
    <p>UserId</p>
    <input type="text" class="checkdb" maxlength="60" name="txtFldUserName" id="txtFldUserName" placeholder="Type your UserId" />
    <div class="logn-ErrorMsg hide mb-2" id="txtFldUserNameError">UserId is Required !</div>
    <p>Password</p>
    <input type="password" name="txtFldpassword" class="checkdb" maxlength="60" id="txtFldpassword" placeholder="Type your Password" />
    <div class="logn-ErrorMsg hide" id="txtFldpasswordError">Password is Required !</div>
    <input type="button" name="sign-in" value="Login" onclick="login()">
	<a href="#">&copy;&nbsp;<script>document.write(new Date().getFullYear());</script> - <strong>Association of Financial Advisers Pte Ltd.,</strong> </a>
  </form>
</div>
          
          </div>
        </div>
      </div>
      
    </div>
  </main>
  <script>
	function isEmptyFld(value) {
		if (value == " " || value == null || value.length == 0 || value == undefined)
			return true;
		else
			return false;
	}

	$("#txtFldUserName").focus();
	
	function login() {
		document.cookie = "session=valid; path=/";
		var loginFrm = document.forms["mngrLoginForm"];
		var userIdFld = loginFrm.txtFldUserName;
		var passwordFld = loginFrm.txtFldpassword;
		
		if (isEmptyFld(userIdFld.value)) {
			$("#txtFldUserNameError").removeClass('hide').addClass('show');
			return false;
		} else {
			$("#txtFldUserNameError").removeClass('show').addClass('hide');
		}
		
		if (isEmptyFld(passwordFld.value)) {
			$("#txtFldpasswordError").removeClass('hide').addClass('show');
			return false;
		} else {
			$("#txtFldpasswordError").removeClass('show').addClass('hide');
		}
		
		$.ajax({
			type: "POST",
			url: "approval/loginValidate?email=" + userIdFld.value + "&password=" + passwordFld.value,
			success: function(response) {
				let loginResponse = JSON.parse(response);
				if (loginResponse && loginResponse.status) {
					if (loginResponse.status == "failed") {
						Swal.fire({
							icon: "error",
							text: loginResponse.reason,
							allowEscapeKey: false,
			       			allowOutsideClick: false
						});
					} else if (loginResponse.status == "success") {
					    window.location.href="kycApproval";
					}
				}
			},
			error: function(error) {
				Swal.fire({
					icon: 'error',
					text: 'Please Try again Later or else Contact your System Administrator',
				});
			}
		});
	}
  </script>
</body>
</html>
