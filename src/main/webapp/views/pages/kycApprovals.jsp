<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@page import="com.afas.isubmit.util.PsgConst"%>

<html lang="en">

<head>
  <title>eKYC Approvals</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport"  content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
 <%-- <link rel="stylesheet" href="${contextPath}/newapproval/style.css">  --%>
<script src="vendor/googlechart/chart.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
 
 <link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
 <link rel="stylesheet" href="newapproval/css/normalize.min.css">
  <link rel="stylesheet" href="newapproval/css/select2.min.css">
 <link rel="stylesheet" href="newapproval/css/dataTables.bootstrap4.min.css">
 <link rel="stylesheet" href="newapproval/css/ekyc-layout.css">
 <link rel="stylesheet" href="newapproval/css/style_checkboxs.css">
 <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />

<%--  <link href="${contextPath}/vendor/SweetAlert/css/sweetalert.css" rel="stylesheet" > --%>

<link href="vendor/SweetAlert2/css/sweetalert2.all.min.css">
<script src="vendor/SweetAlert2/js/sweetalert2.all.min.js"></script>
<script src="vendor/SweetAlert2/js/sweetalert2.min.js"></script>

</head>
<body >

<input type="hidden" id="hdnSessfnaId" name="hdnSessfnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
<input type="hidden" id="hdnSessDataFormId" value="<%=session.getAttribute(PsgConst.SESS_DATAFORM_ID)%>"/>
<input type="hidden" id="hdnSessAccRole" value="<%=session.getAttribute(PsgConst.ACCESS_ROLE)%>"/>
<input type="hidden" name="hdnFnaDetails" id="hdnFnaDetails" value='${FNA_DETAILS}' />

<div id="wrapper" style="background-color: #ecf0f1;">

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top" id="navbar-bg" style="width: 99.8%;">
	       <div>
	        <h5 class="sidebar-brand d-flex align-items-center justify-content-center">
               <span class="sidebar-brand-icon bg-white" style="border-radius:10px">
                  <img src="vendor/psgisubmit/img/logo/logo.png" style="width: 85px;">
               </span>
               <span class="sidebar-brand-text mx-1 white">eKYC Approval</span>
             </h5>
             </div>
             
	<ul class="navbar-nav ml-auto navbar-fixed-top" >
            <li class="nav-item dropdown no-arrow d-none">
                        <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                        </a>
                       <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown"><!--   -->
                           <form class="navbar-search">
                              <div class="input-group">
                                 <input type="text" class="form-control bg-light border-1 small" placeholder="Search for client details?" aria-label="Search" aria-describedby="basic-addon2" style="border-color: #3f51b5;">
                                 <div class="input-group-append">
                                    <button class="btn btn-sm btn-bs3-prime" type="button">
                                    <i class="fas fa-search fa-sm"></i>
                                    </button>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </li>
                     <div class="topbar-divider d-none d-sm-block"></div>
                     <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <img class="img-profile rounded-circle" src="vendor/psgisubmit/img/users/Salim.png" style="max-width: 60px" onerror="this.onerror=null;this.src='vendor/psgisubmit/img/users/client.png';">
                        
                        
                        <span class="ml-2 d-none d-lg-inline text-white small">Logged User : <span id=""><% out.print(session.getAttribute("adviserName")); %></span></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                           <a class="dropdown-item" href="#" title="Generate FNA Doc." onclick="generateFNA();">
                          <i class="fa fa-external-link  fa-sm fa-fw mr-2" aria-hidden="true"></i>
                           Generate FNA</a>
                           
                     
                           <div class="dropdown-divider"></div>
                           <a class="dropdown-item" href="javascript:void(0);" title="Close" data-toggle="modal" data-target=".bs-example-modal-sm" data-backdrop="static"
                           ><i class="fa fa-sign-out fa-sm fa-fw mr-2 "></i>
                           Logout
                           </a>
                        </div>
                     </li>
                  </ul>
</nav>




        <div class="container-fluid" id="container-wrapper">
              <div class="card mb-4 " style="border:1px solid #044CB3;min-height:90vh" id="">
			      <div class="card-body" style="font-size: 13px;">
						 
						 <div class="row">
		
		 <div class="tab-regular" id="clntProdTabs" style="width:100%;">
		 
		  <ul class="nav nav-tabs small" id="landingpage" role="tablist" style="list-style-type:bullet;">
                <li class="nav-item" style="list-style-type:none;">
                	<a class="nav-link active" id="homepage-tab" data-toggle="tab" href="#homepage" role="tab" aria-controls="homepage" aria-selected="true" title="">
                		<img src="vendor/psgisubmit/img/analyze.png"><span> Dashboard</span>
                	</a>
                </li>
                
                
                <li class="nav-item" style="list-style-type:none;">
                	<a class="nav-link" id="detailspage-tab" data-toggle="tab" href="#detailspage" role="tab" aria-controls="detailspage" aria-selected="true" title="">
                		<img src="vendor/psgisubmit/img/assessment.png"><span> View FNA</span>
                	</a>
                </li>
							
            </ul>
            
            
           
                              <div class="tab-content" id="myTabContent" style="margin: 15px;">
                                 <div class="tab-pane fade show active" id="homepage" role="tabpanel" aria-labelledby="client1-tab">
									  
									  <!-- HOME PAGE(DASHBOARD CONTENT START HERE -->
									       <div class="row" style="margin-top: 20px;">

    <div class="col-4">
    	<div class="right-tabs clearfix">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs block">
          <li class="nav-item ">
            <a class="nav-link" data-toggle="tab" href="#shome"><span class="txt_colr"><img src="vendor/psgisubmit/img/survey.png"> Your Approvals</span></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content block">

          <div class="tab-pane container active" id="shome">
          
            <div class="row stats cht cursor-pointer mt-2" onclick="openList('PENDING')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle; font-size:130%;" ><i class="fa fa-question-circle"></i>&nbsp;Pending</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc-secondary progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.PENDING}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                      	 ${FNA_MGR_STATUS_LIST.PENDING}
		                      	 </c:if>	</span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>
          
          

            <div class="row stats cht cursor-pointer mt-2" onclick="openList('APPROVED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle; font-size:130%;"><i class="fa fa-check-circle"></i>&nbsp;Approved</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-ekyc progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.APPROVE}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                    	${FNA_MGR_STATUS_LIST.APPROVE}
		                     </c:if> </span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          

            <div class="row stats cht cursor-pointer mt-2" onclick="openList('REJECTED')">

              <div class="col-sm-12">

                <div class="row">

                  <div class="col-sm-12">

                    <div class="row">

                      <div class="col-sm-6">

                        <span style="vertical-align: middle;font-size:130%;"><i class="fa fa-times-circle"></i>&nbsp;Rejected</span>

                      </div>

                      <div class="col-sm-4">

                        <div class="progress" style="height:25px">
                          <div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" style="width:${FNA_MGR_STATUS_LIST.REJECT}%;height:25px;font-size:180%;"></div>
                        </div>

                      </div>

                      <div class="col-sm-2">

                        <span><c:if test="${not empty FNA_MGR_STATUS_LIST}">
		                      	 ${FNA_MGR_STATUS_LIST.REJECT}
		                      	 </c:if></span>

                      </div>

                    </div>

                  </div>

                </div>

              </div>

            </div>

          </div>
          
          <hr/>
          <div class="clearfix"></div>
          
          <section>
                  <div class="pieID pie"></div>
                    <ul class="pieID legend">
                         <li>
                            <em>Pending</em>
                            <span>${FNA_MGR_STATUS_LIST.PENDING}</span>
                         </li>
                         
                          <li>
                             <em>Approved</em>
                             <span>${FNA_MGR_STATUS_LIST.APPROVE}</span>
                          </li>
                          
                          <li>
                             <em>Rejected</em>
                             <span>${FNA_MGR_STATUS_LIST.REJECT}</span>
                         </li>
      
                    </ul>
            </section>

        </div>
        </div>
        
       
     
    </div>
    
    <div class="col-8">
   
    	<div class="right-tabs clearfix">
    	
    	 <ul class="nav nav-tabs block"  id="yourAprovTabs">
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#home"><i class="fa fa-question-circle-o"></i><span>&nbsp;Pending</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu1"><i class="fa fa-check-circle-o"></i><span>&nbsp;Approved</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link"  data-toggle="tab" href="#menu2"><i class="fa fa-times-circle-o"></i><span>&nbsp;Rejected</span></a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

          <div class="tab-pane active  mt-2" id="home">
          
          <table id="tblPending" class="table table-striped table-bordered hover"  style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:200px">Client</div></th>				                
				                <th><div style="width:80px">Manager</div></th>
				                <th><div style="width:80px">Admin</div></th>
				                <th><div style="width:80px">Compliance</div></th>
				                <th><div style="width:100px">CustId</div></th>
				                <th><div style="width:100px">AdvId</div></th>
				                <th><div style="width:100px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>


          </div>

          <div class="tab-pane fade mt-2" id="menu1">
          
          	<table id="tblApproved" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				           <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:200px">Client</div></th>				                
				                <th><div style="width:80px">Manager</div></th>
				                <th><div style="width:80px">Admin</div></th>
				                <th><div style="width:80px">Compliance</div></th>
				                <th><div style="width:100px">CustId</div></th>
				                <th><div style="width:100px">AdvId</div></th>
				                <th><div style="width:100px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>

          <div class="tab-pane fade mt-2" id="menu2">
         	 <table id="tblRejected" class="table table-striped table-bordered hover" style="width:100%">
				        <thead>
				            <tr>
				                <th>#</th>
				                <th><div style="width:120px">FNA ID</div></th>
				                <th><div style="width:200px">Client</div></th>				                
				                <th><div style="width:80px">Manager</div></th>
				                <th><div style="width:80px">Admin</div></th>
				                <th><div style="width:80px">Compliance</div></th>
				                <th><div style="width:100px">CustId</div></th>
				                <th><div style="width:100px">AdvId</div></th>
				                <th><div style="width:100px">Email</div></th>
				                <th><div style="width:60px">NTUC?</div></th>
				                 <th><div style="width:50px">Mgr Appr By</div></th>
				                <th><div style="width:50px">Mgr Appr Date</div></th>
				                <th><div style="width:50px">Mgr Appr Rem</div></th>
				                
				                <th><div style="width:50px">Admin Appr By</div></th>
				                <th><div style="width:50px">Admin Appr Date</div></th>
				                 <th><div style="width:50px">Admin Appr Rem</div></th>
				                 
				                <th><div style="width:50px">Comp Appr By</div></th>
				                <th><div style="width:50px">Comp Appr Date</div></th>
				                 <th><div style="width:50px">Comp Appr Rem</div></th>
				                 <th><div style="width:50px">s1</div></th>
				                <th><div style="width:50px">s2</div></th>
				                 <th><div style="width:50px">s3</div></th>
				            </tr>
				        </thead>
				        <tbody>
				        </tbody>
			        </table>
          </div>

        </div>
    	
       
        </div>

    </div>

  </div> 
									   <!--DASHBOARD TAB SEC CONTENT END HERE  -->     
                                 </div>
                                  
                              <div class="tab-pane fade show" id="detailspage" role="tabpanel" aria-label="client2-tab">
                                           <!--VIEW FNA TAB CONTENT START HERE  -->
                                           <form name="frmKycApprovalNew"  id="frmKycApprovalNew" >
  




<div class="row">
     <div class="col-4">
     <!-- manager,admin,comp and Ntuc status List -->
 <ul class="list-group list-group-horizontal">
  <li class="list-group-item">Manager Status : <i id="iFontMgr" class="fa fa-question-circle"></i> </li>
  <li class="list-group-item">Admin Status : <i id="iFontAdmin" class="fa fa-question-circle" ></i> </li>
  <li class="list-group-item">Comp Status : <i id="iFontComp" class="fa fa-question-circle"></i> </li>
</ul> 
      
    
</div>

  <div class="col-2">
  <ul class="list-group list-group-horizontal">
  <li class="list-group-item d-none">Ntuc Status : <span class="text-center"><i id="ntucblock" class="" aria-hidden="true"></i></span> <span class="text-center"><img src="vendor/psgisubmit/img/policy-pdf.png" style="width: 10px;" class="rounded mx-auto" alt="Get NTUC Policy Details"  title="Get NTUC Policy Details" onclick="" id="imgpolapi" style="display:none" /></span></li>
  
</ul> 
  </div>   <!--end  -->
    
     <div class="col-4">
     <label class="frm-lable-fntsz" >Client Name&nbsp;:&nbsp;</label>
        <select id="dropDownQuickLinkFNAHist" class="form-control" onchange="" style="vertical-align: middle" >
		        		  <option selected value="">--Select FNA List--</option>
		        		  <optgroup label="Pending" id="optgrpPending"></optgroup>
		        		  <optgroup label="Approved" id="optgrpApproved"></optgroup>
		        		  <optgroup label="Rejected" id="optgrpRejected"></optgroup>
		           </select>
        	
     
     </div>
      <div class="col-2">
      <small>FNA Id : <span id="txtFnaId"></span></small><br>
      <small>Adviser : <span id="txtFldAdvStfName"  title=""><span class="adviserNameTab"></span></span></small>
       </div>
   </div> 

    <div class="row mt-2" >
    
    <!--Approval Sec Start  -->
          <div class="col-4 col-md-4">
		      <div class="card">			
				<div class="card-header txt_colr" id="cardHeaderStyle-22">
				    <div class="row">
				          <div class="col-5"><span id="cardSecTitle">Approval Sections</span></div>
				          <div class="col-5">
				               <!-- <input type="button" class=" btn btn-bs3-warning" id="cardBtnTitle" onclick="toggleApprovPolcySec(this)" value="View Policy Docs."/> -->
				         
				         <span class="badge badge-pill badge-warning amber py-2 btnshake show" title="View Policy Submission Docs."  onclick="toggleApprovPolcySec(this)">
				         <i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;<span class="font-sz-level9" id="cardBtnTitle">Click here to View Policy Docs.</span></span>
				         
				         
				          </div>
				          <div class="col-2"><span id="polAllDocDownld" class="hide"><i class="fa fa-file-archive-o" aria-hidden="true"></i><sub><i class="fa fa-cloud-download cursor-pointer" aria-hidden="true"
				            title="Zip all Documents" onclick="downLoadAllFile();"></i></sub></span></div>
				    </div>
				</div>		
				 	
				<div class="card-body" id="ApprovalSec" >			
				<nav class="nav-justified ">
				                  <div class="nav nav-tabs " id="nav-tab" role="tablist">
				                    <a class="nav-item nav-link active" id="appr-mgr-tab" data-toggle="tab" href="#appr-mgr-tab-cont" role="tab" aria-controls="appr-mgr-tab-cont" aria-selected="true"><strong>Manager</strong> </a>
				                    <a class="nav-item nav-link" id="appr-admin-tab" data-toggle="tab" href="#appr-admin-tab-cont" role="tab" aria-controls="appr-admin-tab-cont" aria-selected="false"><strong>Admin</strong> </a>
				                    <a class="nav-item nav-link" id="appr-comp-tab" data-toggle="tab" href="#appr-comp-tab-cont" role="tab" aria-controls="appr-comp-tab-cont" aria-selected="false"><strong>Compliance</strong> </a>
				                  </div>
				</nav>
				                <div class="tab-content" id="nav-tabContent" style="padding:9px;">
				                
				                
				                  <div class="tab-pane fade show active" id="appr-mgr-tab-cont" role="tabpanel" aria-labelledby="appr-mgr-tab">
				                  
				                        <div class="pt-3"></div>
				                        
				                        <div class="row">
										   <div class="col-md-12">
										   <label for="" class="frm-lable-fntsz">Status</label>
										    <select class="form-control no-border no-bgcolor bottom-border" 
										    id="txtFldMgrApproveStatus" onchange="setManagerStatus(this)" disabled> 
										        <option value="APPROVE">Approve</option>
										        <option value="REJECT">Reject</option>
										        <option value="">Pending</option>
										    </select>
										   </div>
										</div>
										
										<div class="row">
											<div class="col-md-12">
											 <label for="" class="frm-lable-fntsz">Remarks:</label>
											  <textarea class="form-control no-border no-bgcolor bottom-border" id="txtFldMgrApproveRemarks" readonly ></textarea>
											</div>
										</div>
										
										<div class="row">
											 <div class="col-md-6">
										        <label class="frm-lable-fntsz" id="mngrapprLbl" ></label> 
										             <div class="input-group form">
														<input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldMgrApproveDate" readonly>											                 
											         </div>
										    </div>
										    
										    <div class="col-md-6">
										        <label class="frm-lable-fntsz" id="mngrapprByLbl"></label> 
										             <div class="input-group form">
														<input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldMgrStsApprBy" readonly>											                 
											         </div>
										    </div>
										    
										    
										</div>
										
						
				                        
				                  </div>
				                  <div class="tab-pane fade" id="appr-admin-tab-cont" role="tabpanel" aria-labelledby="appr-admin-tab">
				                       <div class="pt-3"></div>
				                       
				                       <div class="row">
										   <div class="col-md-12">
										   <label for="" class="frm-lable-fntsz">Status</label>
										    <select class="form-control no-border no-bgcolor bottom-border" 
										    id="txtFldAdminApproveStatus"  onchange="setAdminStatus(this)" disabled> 
										        <option value="APPROVE">Approve</option>
										        <option value="REJECT">Reject</option>
										        <option value="">Pending</option>
										    </select>
										   </div>
										</div>
										
										<div class="row">
											<div class="col-md-12"> 
											 <label for="" class="frm-lable-fntsz">Remarks:</label>
											  <textarea class="form-control no-border no-bgcolor bottom-border" id="txtFldAdminApproveRemarks" readOnly></textarea>
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6">
										        <label class="frm-lable-fntsz" id="admnapprLbl" ></label> 
										             <div class="input-group form">
											               <input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldAdminApproveDate" readonly>
											                   
											         </div>
										    </div>
										    
										    <div class="col-md-6">
										        <label class="frm-lable-fntsz" id="admnapprByLbl" ></label> 
										             <div class="input-group form">
											               <input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldAdminStsApprBy" readonly>
											                   
											         </div>
										    </div>
										    
										</div>
						
				                  </div>
				                  <div class="tab-pane fade" id="appr-comp-tab-cont" role="tabpanel" aria-labelledby="appr-comp-tab">
				                       <div class="pt-3"></div>
				                        <div class="row">
										   <div class="col-md-12">
										   <label for="" class="frm-lable-fntsz">Status</label>
										    <select class="form-control no-border no-bgcolor bottom-border" 
										    id="txtFldCompApproveStatus" onchange="setComplianceStatus(this)" disabled> 
										        <option value="APPROVE">Approved</option>
										        <option value="REJECT">Rejected</option>
										        <option value="">Pending</option>
										    </select>
										   </div>
						   
										   
										</div>
						
										<div class="row">
											<div class="col-md-12">
											 <label for="" class="frm-lable-fntsz">Remarks:</label>
											  <textarea class="form-control no-border no-bgcolor bottom-border" id="txtFldCompApproveRemarks" readonly></textarea>
											</div>
										</div>
						
										<div  class="row">
											<div class="col-md-6">
														     <label class="frm-lable-fntsz" id="compapprLbl" ></label> 
														             <div class="input-group form">
															               <input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldCompApproveDate" readonly>
															                  
															         </div>
											</div>
											
											<div class="col-md-6">
														     <label class="frm-lable-fntsz" id="compapprByLbl" ></label> 
														             <div class="input-group form">
															               <input type="text" class="form-control no-border no-bgcolor bottom-border"  id="txtFldCompStsApprBy" readonly>
															                  
															         </div>
														    </div>
										
										</div>
				                  </div>
				                </div>
				
				
				</div>
				
				
				 <div class="card-body hide" id="policySubDocSec" >
				 
				 <div class="card">
				 <div class="card-header" id="cardHeaderStyle"><strong>Reference Docs.</strong></div>
				 <div class="card-body">
				  <div class="list-group-item" id="dynaAttachNRICList"  style="max-height: 50vh;overflow-y: scroll; overflow-x: hidden;"></div>
				 </div>
				  </div>
				  
				  <div class="card mt-2">
				 <div class="card-header" id="cardHeaderStyle"><strong>Other Documents</strong></div>
				 <div class="card-body">
				  <div class="list-group-item" id="dynaAttachNTUCList"  style="max-height: 50vh;overflow-y: scroll; overflow-x: hidden;"></div>
				 </div>
				  </div>
				 
				</div>
				
				
				</div>
				
				
				
			
			 
			 </div>
			
    
    <!-- Approval Sec End -->
    
    
      <div class="col-8 col-md-8">
				<div class="card">
				<div class="card-header" id="cardHeaderStyle-22">
				<div class="row">
				     <div class="col-5">
				        Verify FNA Form:-
				     </div>
				     <div class="col-4">
				         <span class="badge badge-pill badge-success py-2 " title="Click Here to Sign & Approve FNA"   onclick="getQrorSign();" id="btnQRCodesignMgr">
				         <i class="fa fa-check-square-o fa-1x" aria-hidden="true">
				         </i>&nbsp;<span class="font-sz-level9 ls-14">Click here to Sign & Approve FNA</span></span>
				     </div>
				     <div class="col-3">
				          <span class="badge badge-pill badge-warning py-2 " title="Full View FNA" onclick="fullViewFNA();" id="">
				         <i class="fa fa-print icncolr" aria-hidden="true">
				         </i>&nbsp;<span class="font-sz-level9 ls-14">Print FNA</span></span>
				     </div>
				     
				</div>
				</div>
				 
				<div class="card-body" style="overflow-x:hidden">
				
					<iframe class="iframe-placeholder"  style="height:calc(100vh - 40vh);width:100%;border:0px;display:block" id="dynaFrame" >				 
					
					
					</iframe>
					
						
				</div>
				</div>
			</div>
			
			
			
			
			
			
		 
    </div>
    
    
    </form>
                                           <!--VIEW FNS TAB CONTENT SEC END HERE  -->
                              </div>
                              
                              
                              </div>
                           </div>
		    
		</div>     
						
							   
							   
				  </div>
	  
	          </div>
		
        </div>
     
        
        


</div>

</div>

</div>

<!--lOGOUT MODAL   -->
<div class="modal bs-example-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><h4>Logout <i class="fa fa-lock"></i></h4><button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body"><i class="fa fa-question-circle"></i> Are you sure  want to Logout?</div>
      <div class="modal-footer"><a href="javascript:;" class="btn btn-primary " onclick="logoutKyc();">Ok</a>
       <a href="javascript:;" class="btn btn-primary" data-dismiss="modal">Cancel</a> 
      </div>
    </div>
  </div>
</div>
<!--LOGOUT MAODAL END  -->

<div class="modal" tabindex="-1" role="dialog" aria-hidden="true" id="modalNTUCApproval">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header"><h6>KYC Approval Document<i class="fa fa-upload"></i></h6></div>
      <div class="modal-body">
      		<form id='hdnFormKycApprDoc' method='POST'>
      			<div class="form-control">
      				<input type='file' name='fileKycCompApprove' id='fileKycCompApprove' class='fpFileBrowseBtn' style='width:100%;'>
      			</div>
      			
      		</form>
      </div>
      <div class="modal-footer"><a href="javascript:;" class="btn btn-primary " onclick="validateNTUCApprDoc()">Upload</a>
       
      </div>
    </div>
  </div>
</div>

<!--REPORT MODAL START  -->
  <div class="modal fade" id="PDFReportModal" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg">
       <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">PDF REPORT</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <div class="">
          <iframe  style="height:100vh;width:53vw;border:0px;" id="PDFReportFrame"></iframe>
          </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
<!--REPORT MODAL END  -->

    <!--mngr sec modal start  -->
      <!-- manager Sec Open Modal Start -->
      
      
      <div class="modal fade" id="openMngesecModal"  aria-modal="true" role="dialog">
    <div class="modal-dialog modal-xl  modal-dialog-scrollable">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title bold"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;Supervisor's Review</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">�</button>
      </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            
          <!-- manager Sec Start  -->
          
                <div class="card card-content-fnsize" style="margin-top:10px;" id="diManagerSection">
                             <div class="card-body">
                                   <div class="row">
										           <div class="col-md-12">
										              <span class="font-sz-level6">I have reviewed the information disclosed in the "Financial Needs Analysis" Form which relates to the
                                                       client's priorities and objectives, investment profile, insurance portfolio, CKA outcome and the client's. 
                                                        declaration &amp; acknowledgement.</span>
										           </div>
								  </div>
								   <div class="row mt-2" id="mngrSecChk">
                                         <div class="col-md-12">
                                          <ul class="list-group mt-2">
		                                          <li class="list-group-item bg-hlght" id="errorFld">
		                                              <span class="italic bold text-primaryy font-sz-level6 "> Select any <span class="txt-urline  bold">ONE</span> Option Below :-</span>
		                                                      <div class="row">
								        <div class="col-md-6 ">
								          <div class="col-md-12">
								              
								              <div id="Sec2ContentFormId">
			                                    <div class="inputGroup">
													         <input id="suprevMgrFlgA" name="suprevMgrFlg" type="radio" value="" onclick="enableMgr();chkAgreeDisagreeFld(this,'suprevFollowReason');">
															<label for="suprevMgrFlgA" id="lblTxtSz-03" style="border: 1px solid grey;"><span><span class="font-sz-level6">  <span class="text-success"><strong>I agree</strong></span> with the Representative's needs analysis and<br> recommendation(s). </span> </span></label>
												 </div>
                                              </div>
										            
										  </div>
								        </div>
								        <div class="col-md-6">
								             <div class="col-md-12">
								             <div id="Sec2ContentFormId">
												<div class="inputGroup">
													         <input id="suprevMgrFlgD" name="suprevMgrFlg" type="radio" value="" onclick="enableMgr();chkAgreeDisagreeFld(this,'suprevFollowReason');">
															<label for="suprevMgrFlgD" id="lblTxtSz-03" style="border: 1px solid grey;"><span><span class="font-sz-level6"> <span class="text-danger"><strong>I disagree</strong></span> with the Representative's needs analysis and<br> recommendation(s).</span></span></label>
												 </div>
                                              </div>
										            
										       </div>
								        </div>
								  </div>
		                                          
		                                          </li>
		                                          
		                                          
		                                          </ul>
                                           <div class="invalid-feedbacks" id="mngrBtnErrorMsg"></div>
                                         
                                             
								          </div>
								        
								    </div>
								 
								 
								  
								  <div class="row mt-2">
								       <div class="col-md-12">
								         <small><i> <strong>Note: </strong> (If <span class=" text-danger bold font-sz-level8">disagree</span> , please state the reasons and/or advise on the follow-up action required, where applicable.)</i></small>
								       </div>
								       
								   </div>
								   
								   
								    <div class="row">
								       <div class="col-md-8">
								             <div class="form-group">
								                  <label class="frm-lable-fntsz "> Reason(s) and Follow-up Action:</label>
									             <textarea class="form-control txtarea-hrlines text-wrap" rows="7" id="suprevFollowReason" name="suprevFollowReason" onblur="hideErrmsg(this)"
									            	 maxlength="300" onkeydown="textCounter(this,300);" onkeyup="textCounter(this,300);"  ></textarea>
								                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							                     <div class="invalid-feedbacks" id="mngrDisagrReason"></div>
							                </div> 
								       </div>
								       
								        <div class="col-md-4">
								             <div class="card" style="margin-top:15px;" id="SignCard">
								             <div class="card-header" id="cardHeaderStyle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Manager Signature</div>
								                								                  <div class="card-body">
								                     <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Sign. of Supervisor :  </label>
								                            </div>   
								                            
								                            
								                            <!-- signature Click open San to Sign Modal Page start  -->
								                             <div class="col-md-6">
								                                  <div id="mgrQrSecNoData"  class="mgrQrSecNoData show">
								                                   <p class="center noData" id="" style="font-size: 11px;"><img src="vendor/psgisubmit/img/warning.png" class="">
								                                     &nbsp;Manager Signature Not Found&nbsp;</p>
								                                  </div>
								                                  
								                                  <div id="sign-QR_style" class="mgrQrSec hide">
								                                    <small><img src="vendor/psgisubmit/img/contract.png"class="mgrQr"></small>
								                                  </div>
								                            </div>  
								                            
								                            <!-- signature Click open San to Sign Modal Page End -->
								                             
								                      </div>
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                <label class="frm-lable-fntsz p-0">Name of Supervisor: </label>
								                            </div>
								                            
								                             <div class="col-md-6">
								                             <input type="text" name="mgrName" id="mgrName"  class="form-control" readonly="readonly" value="${LOGGED_USER_INFO.FNA_ADVSTF_MGRNAME}">
								                            
								                            </div>  
								                                
								                      </div>
								                      
								                       
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Date : </label>
								                            </div> 
								                            
								                             <div class="col-md-6">
								                              <span id="mgrSignDate" style="border: none;"></span>
								                              <!--    <input type="text" name="txtFldMgrSignDate" id="txtFldMgrSignDateDeclarepage" readonly="true" style="width:80px;    border: none;"> -->
								                            </div>    
								                      </div>
								                      
								                      <div class="row mt-2 d-none">
								                            <div class="col-md-6">
								                                <label class="frm-lable-fntsz p-0"> Mail Sent Status : </label>
								                            </div> 
								                             <div class="col-md-6">
								                                <span class="badge badge-success " id="mailSentMgr">Email Sent <i class="fa fa-check" aria-hidden="true"></i></span>
								                                <span class="badge badge-warning font-normal d-none" id="mailNotSentMgr">Email Not Sent <i class="fa fa-times" aria-hidden="true"></i></span>
								                            </div>    
								                      </div>
								                  </div>
								             </div> 
								       </div>
								       
								   </div>
								   
								     
                               </div>
                       </div>
          
          <!--manager Sec  End -->
             </div>
      
      <div class="modal-footer">
				<button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Cancel</button>
				<button type="button" class="btn btn-sm btn-bs3-prime" id="btnApprvMngr" onclick="mngrSendToApprove()">Update Status</button>
				<button type="button" class="btn btn-sm btn-bs3-prime d-none" id="btnApprMgrFlag" data-dismiss="modal">Close</button>
	   </div>
    </div>
  </div>
  
</div>
   
   <!-- mngr sec modal end -->

</body>

<!-- <script type="text/javascript" src="vendor/psgisubmit/js/kyc_intro.js"></script> -->
<!-- <script src="js/jquery.steps.js"></script> -->
<script src="vendor/psgisubmit/js/kyc_approval_script.js"></script>
<script type="text/javascript" src="vendor/psgisubmit/js/fna_common.js"></script>
<script src="vendor/psgisubmit/js/scan_sign.js"></script>
<!-- App js -->
  <!-- <script src="vendor/psgisubmit/js/kyc_home.js"></script> -->
 <!-- <script src="vendor/psgisubmit/js/finan_well_review.js"></script> -->
 
 <!-- <script src="vendor/psgisubmit/js/tax_residency.js"></script> -->
 <!-- <script src="vendor/psgisubmit/js/client_signature.js"></script> -->
 
 <!-- <script src="vendor/GitUpToast/js/jquery.toast.js"></script> -->
 <!-- <script src="vendor/psgisubmit/js/product_recomm.js"></script> -->
 <script>
 	var sessspsname = '${SESS_DF_SPS_NAME}';
 	var approvalScreen = "true";
 	jsnDataProdRecomDetls = ${PRODUCT_RECOM_PLAN_DETS};
 	callBodyInit();
 </script>
 
 

</html>