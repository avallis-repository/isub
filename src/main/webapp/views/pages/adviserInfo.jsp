<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<script type="text/javascript" src="vendor/psgisubmit/js/adviser_info.js"></script>
	</head>
	<body id="page-top">
		<div class="container" id="container-wrapper">
			<div class="card" style="border:1px solid #044CB3;">
				<div class="card-header">
                  <h5>Adviser Info</h5>
                </div>
				<div class="card-body">
					<div class="adviserForm">
						<div class="form-row">
							<div class="col-12 col-sm-6">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Adviser ID</span>
									</div>
									<input type="text" class="form-control" id="adviserId" name="adviserId" readonly>
								</div>
							</div>
							<div class="col-12 col-sm-6">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Adviser Name</span>
									</div>
									<input type="text" class="form-control" id="adviserName" name="adviserName" readonly>
								</div>
							</div>
						</div>
						<div class="form-row mt-3">
							<div class="col-12 col-sm-6">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Email ID</span>
									</div>
									<input type="text" class="form-control" id="adviserEmail" name="emailId" readonly>
								</div>
							</div>
							<div class="col-12 col-sm-6">
								<div class="input-group">
									<div class="input-group-prepend">
										<span class="input-group-text">Password<sup style="color: red;"><strong>*</strong></sup></span>
									</div>
									<input type="text" class="form-control" id="adviserPassword" name="password">
									<div class="invalid-feedbacks hide" id="adviserPasswordError">Key in Password Field</div>
								</div>
							</div>
							<div class="mt-3">
								<button id="btnUpdateAdviser" type="button" class="btn btn-bs3-prime" onclick="updateAdviserInfo();" disabled>Update</button>
								<button id="btnDeleteAdviser" type="button" class="btn btn-bs3-prime" onclick="deleteAdviserInfo();" disabled>Delete</button>
							</div>
							<input type="hidden" id="hTxtFldUserLoginData">
							<input type="hidden" id="hTxtFldTenantId">
						</div>
					</div>
					<div class="table-responsive mt-3">
						<table class="table table-hover" id="adviserTable">
							<thead class="thead-light">
								<tr>
									<!-- <th>Adviser ID</th> -->
									<th>Adviser name</th>
									<th>Email ID</th>
								</tr>
							</thead>
							<tbody id="tBodyAdviser">
							
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>