<%@ page language="java" import="java.util.*" %> 
<%@page import="com.afas.isubmit.util.PsgConst"%>
<%ResourceBundle psgprop =ResourceBundle.getBundle("properties/psgisubmit");%>

  <div class="tab-pane fade show active" id="client1" role="tabpanel" aria-labelledby="client1-tab" >
                                    <!-- Simple Tables  align-items-center -->
									    <!--start  -->
															
									            <div class="row" id="accordian-row">
        <div class="col-6">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                <i class="fa fa-user" style="color:#4caf50;"></i>&nbsp;Personal Details
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
						<div class="card">
												
												<div class="card-body">
														<div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-12">
		                                                    
		                                                    
		                                                    
		                                                  
		                                                    <input type="hidden" id="DataFormId" name="dataFormId" value="<%= session.getAttribute("DataFormId") %>"/>
		                                                        <label class="frm-lable-fntsz" for="name">Name(as per NRIC)</label>
		                                                        <input id="name" name="dfSelfName" type="text" placeholder="" class="form-control input-md checkdb" required="" maxlength="60">
		                                                    </div>
		                                               </div>
                                                   </div>
												   
												   
												   <div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-6">
		                                                        <label class="frm-lable-fntsz" for="name">NRIC/Passport No.</label>
		                                                        <input id="name" name="dfSelfNric" type="text" placeholder=""  class="form-control input-md checkdb" required="" maxlength="15">
		                                                   </div>
															
															<!-- <div class="col-md-6">
		                                                        <label class="frm-lable-fntsz" for="name">Gender</label>
		                                                          <select class="form-control">
		                                                             <option selected>--SELECT--</option>
		                                                             <option>Male</option>
		                                                              <option>Female</option>
		                                                           </select>
		                                                    </div> -->
		                                                    
		                                                     <div class="col-md-6">
															        <div class="form-group">
																	       <!--  <label for="pwd" class="frm-lable-fntsz">Gender</label><br> -->
																	        <button type="button" class="btn  btn-toggle" data-toggle="button" aria-pressed="false" autocomplete="off" style="margin: 1rem 3rem;">
																			<div class="handle"></div>
																			</button>
																	</div>
							   
  															 </div>
															
		                                               </div>
                                                   </div>
												   
												   <div class="form-group">
                                                         <div class="row">
		                                                    
		                                                    
		                                                    <div class="col-md-6">
		                                                         <div class="form-group" id="simple-date1">
                                                                   <label class="frm-lable-fntsz" for="name">Date of Birth</label>
											                      <div class="input-group date">
											                        <div class="input-group-prepend">
											                          <span class="input-group-text"><i class="fas fa-calendar"></i></span>
											                        </div>
											                        <input type="text" class="form-control checkdb" value="01/06/2020" id="simpleDataInput" style="" name="dfSelfDob" maxlength="10">
											                      </div>
                  </div>
		                                                       <!--  <label class="frm-lable-fntsz" for="name">Date of Birth</label>
		                                                        <input id="name" name="name" type="date" placeholder="" class="form-control input-md" required=""> -->
		                                                    </div>
															
															<div class="col-md-6">
		                                                        <label class="frm-lable-fntsz" for="name">Marital Status</label>
		                                                          <select class="form-control checkdb" name="dfSelfMartsts">
		                                                             <option value="" selected>--Select--</option>
																		    <%for(String data:psgprop.getString("app.maritalstatus").split("\\^")){ %>
											           							<option value="<%=data%>"><%=data%></option>
											    							<%}%>
		                                                           </select>
		                                                    </div>
															
		                                                </div>    
                                                  </div>
												  
												  
												  <div class="form-group">
                                                         <div class="row">
														 <div class="col-md-6">
		                                                        <label class="frm-lable-fntsz" for="name">Country of Birth</label>
		                                                             <select class="form-control checkdb" name="dfSelfBirthCntry">
		                                                             <option value="" selected>--Select--</option>
		                                                              <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
    																			<option value="<%=country%>"><%=country%></option>
     																   <%}%>
		                                                           </select>
		                                                 </div>
															
															
															<div class="col-6">
															 <label class="frm-lable-fntsz" for="name">Nationality</label>
															<div class="form-check ">
  <input class="form-check-input checkdb" type="radio" name="dfSelfNation" id="Singaporean" value="SG" checked onchange="getNationalityHide()">
  <label class="form-check-label frm-lable-fntsz font-normal" for="exampleRadios11">
   Singaporean
  </label>
</div>
<div class="form-check ">
  <input class="form-check-input checkdb" type="radio" name="dfSelfNation" id="Singaporean-PR" value="SG-PR" onchange="getNationalityHide()">
  <label class="form-check-label frm-lable-fntsz font-normal" for="exampleRadios12">
    Singaporean - PR
  </label>
</div>

<div class="form-check ">
  <input class="form-check-input checkdb" type="radio" name="dfSelfNation" id="Others" value="Others" onchange="getNationalityShow()">
  <label class="form-check-label frm-lable-fntsz font-normal" for="exampleRadios13">
   Others
  </label>
</div>
<div class="form-check ">
  <select class="form-control checkdb" id="selectNationOther" name="dfSelfNationalityOther" style="display:none;">
			<option value="" selected>...Select...</option>
			<%for(String data:psgprop.getString("app.nationalitywithcode").split("\\^")){ %>
		 		<option value="<%=data.split(":")[0]%>"><%=data.split(":")[1]%></option>
		 	<%} %>
									  
								</select>		
</div>

															</div>
															
														 </div>
												  </div>
												  
												  
												  
												  
												</div>
												
						</div>
						
						</div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                               <i class="fa fa-map-marker" style="color: #f33;"></i>&nbsp;Address Details
                            </a>
                        </h4>
                   </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
					
					<div class="panel-body">
					
					 <div class="card">
												
												<div class="card-body">
												
												
												<div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-12">
		                                                        <label class="frm-lable-fntsz" for="name">Registered Residential Address</label>
		                                                        <input id="rra" name="dfSelfHomeAddr" type="text" placeholder="" class="form-control input-md checkdb" maxlength="60" required="">
		                                                    </div>
		                                              </div>
                                                 </div>
                                                 
												   <div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-12">
		                                                        <div class="checkbox">
			                                                        <label for="time-01">
			                                                        <input type="checkbox" class="checkdb" name="chktest" id="time-01" value="checked" checked="checked" onchange="getMailAddressCheckBox()">&nbsp;<span class="frm-lable-fntsz">Mailing Address is same as Residential Address</span></label>
			                                                    </div>
		                                                    </div>
		                                                </div>
		                                          </div>
												  
												  
												  <div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-12">
		                                                        <div class="checkbox">
			                                                        <label for="time-02">
			                                                        <input type="checkbox" class="checkdb" name="chktest1" id="time-02" value="" onchange="getDiffAddrCheckBox()">&nbsp;<span class="frm-lable-fntsz text-wrap">If different from Registered Residential Address.
                                                                   </span></label>
			                                                    </div>
		                                                    </div>
		                                                </div>
		                                                <div class="row pl-3 ">
		                                                      <div class="col- "><img src="img/infor.png" class="mt-2"></div>
		                                                    <div class="col-11">
			                                                     <div class="col-">
			                                                       <small class="font-italic bold text-primaryy font-sz-level8" >please submit a proof of address issued within the last 6 months(e.g.) Bank/Utility/Phone bill statement.</small>
			                                                     </div>
		                                                     </div>
		                                                </div>
		                                                 
		                                          </div>
		                                          
		                                         
		                                          
		                                          
												  
												     <div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-12">
		                                                        
		                                                        <input id="name" name="dfSelfMailAddr" type="text" placeholder="" class="form-control input-md checkdb" required="" maxlength="60">
		                                                    </div>
		                                                </div>
		                                           </div>
												   
												   
												</div>
												
												</div>
					</div>
                        
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                               <i class="fa fa-phone-square" style="color: #028686;"></i>&nbsp;Contact Details
                            </a>
                        </h4>
                    </div>
                    
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                        <div class="panel-body">
                             <div class="card">
												
												<div class="card-body">
												
												<div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-6">
		                                                        <label class="frm-lable-fntsz" for="name">Mobile</label>
		                                                        <input id="name" name="dfSelfMobile" type="text" placeholder="" class="form-control input-md checkdb" required="" maxlength="60">
		                                                 </div>
		                                                    
		                                                    <div class="col-md-6">
		                                                        <label class="frm-lable-fntsz" for="name">Home</label>
		                                                        <input id="name" name="dfSelfHome" type="text" placeholder="" class="form-control input-md checkdb" required="" maxlength="60">
		                                                    </div>
		                                               </div>
                                                   </div>
												   
												       <div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-6">
		                                                        <label class="frm-lable-fntsz" for="name">Office</label>
		                                                        <input id="name" name="dfSelfOffice" type="text" placeholder="" class="form-control input-md checkdb" required="" maxlength="60">
		                                                    </div>
		                                                    
		                                                    <div class="col-md-6">
		                                                        <label class="frm-lable-fntsz" for="name">Email Address</label>
		                                                        <input id="name" name="dfSelfPersEmail" type="email" placeholder="" class="form-control input-md checkdb" required="" maxlength="60">
		                                                    </div>
		                                               </div>
                                                   </div>
												</div>
												</div>
                                 </div>
                    </div>
                </div>
            </div>
        </div>
		<div class="col-6">
            <div class="panel-group" id="accordion_right" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOneR">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_right" href="#collapseOneR" aria-expanded="true" aria-controls="collapseOneR">
                                <i class="fa fa-money" style="color:#ff9800 ;"></i>&nbsp;Employment & Financial Details
                            </a>
                        </h4>
                    </div>
                    
                    <div id="collapseOneR" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOneR" style="margin-bottom:15px;">
                        <div class="panel-body">
                           <div class="row">
									  <div class="col-6">
									  <div class="card h-100">
												<div class="card-header" id="cardHeaderStyle" ><span>Employment Info</span></div>
												<div class="card-body">
												
												 
  <div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-12">
		                                                        <label class="frm-lable-fntsz" for="name">Name of Employer</label>
		                                                        <input id="name" name="dfSelfCompname" type="text" placeholder="" class="form-control input-md checkdb" maxlength="60" required="">
		                                                    </div>
		                                               </div>
                                                   </div>
  
  <div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-12">
		                                                        <label class="frm-lable-fntsz" for="name">Occupation</label>
		                                                        <input id="name" name="dfSelfOccpn" type="text" placeholder="" class="form-control input-md checkdb" maxlength="60" required="">
		                                                    </div>
		                                                     
		                                              </div>
                                                   </div>
												   
												    <div class="form-group">
                                                        <div class="row">
		                                                    
		                                                     <div class="col-md-12">
		                                                        <label class="frm-lable-fntsz" for="name">Estd.Annual Income</label>
		                                                        <!-- <input id="name" name="name" type="text" placeholder="" class="form-control input-md" required=""> -->
		                                                          <div class="input-group ">
  <div class="input-group-prepend">
    <span class="input-group-text">$</span>
  </div>
  <input type="text" class="form-control checkdb" maxlength="15" aria-label="Amount (to the nearest dollar)" name="dfSelfAnnlIncome">
  <div class="input-group-prepend">
    <span class="input-group-text">.00</span>
  </div>
</div>
		                                                   
		                                                    </div>
		                                              </div>
                                                   </div>
												   
												   <div class="form-group">
                                                        <div class="row">
		                                                    <div class="col-md-12">
		                                                          <label class="frm-lable-fntsz" for="name">Nature of Business</label>  
		                                                           <select class="form-control checkdb" name="dfSelfBusinatr">
		                                                             <option value="" selected>--SELECT--</option>
		                                                             <% for(String data:psgprop.getString("app.natureofbusiness").split("\\^")){ %>
		                                                             <option value="<%=data%>"><%=data%></option>
		                                                             <%} %>
		                                                              <option>.....</option>
		                                                           </select>
		                                                    </div>
		                                                     
		                                              </div>
                                                   </div>
												
												</div>
												</div>
										</div>
										
										<div class="col-6">
									<div class="card ">
							<div class="card-header" id="cardHeaderStyle"><span>Source of Fund</span></div>
								<div class="card-body">
																						
										<ul class="list-group" id="ulLiStyle-01">
																							  <li class="list-group-item"><input type="checkbox" name="chkCDClntErndIncm" id="chkCDClntErndIncm"  data="CERN" value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc')"><label class="frm-lable-fntsz" for="chkCDClntErndIncm">&nbsp;&nbsp;Earned Income</label>
																							  <input type="hidden" name="htfcderndicn"/>
																							  </li>
																							  
																							  <li class="list-group-item"><input type="checkbox"   name="chkCDClntInvIncm" id="chkCDClntInvIncm"  data="CINV"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');"><label class="frm-lable-fntsz" for="chkCDClntInvIncm">&nbsp;&nbsp;Investment</label>
																							   <input type="hidden" name="htfcdinvsticn"/>
																							  </li>
																							  
																							  <li class="list-group-item"><input type="checkbox"  name="chkCDClntPrsnlIncm" id="chkCDClntPrsnlIncm"   data="CPRS"  value=""  onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');"><label class="frm-lable-fntsz" for="chkCDClntPrsnlIncm">&nbsp;&nbsp;Savings</label>
																							  <input type="hidden" name="htfcdpersicn"/>
																							  </li>
																							  
																							  <li class="list-group-item"><input type="checkbox"  name="chkCDClntCPFSvngs" id="chkCDClntCPFSvngs"  data="CCPF"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');"><label class="frm-lable-fntsz" for="chkCDClntCPFSvngs">&nbsp;&nbsp;CPF Savings</label>
																							  <input type="hidden" name="htfcdcpficn"/>
																							  </li>
																							  
																							  <li class="list-group-item"><input type="checkbox"  name="chkCDClntSrcIncmOthrs" id="chkCDClntSrcIncmOthrs" data="COTH"  value="" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');chkSelfSpsOthrValid(this,'dfSelfFundsrcDets');"><label class="frm-lable-fntsz" for="chkCDClntSrcIncmOthrs">&nbsp;&nbsp;Others</label>
													                                              <input type="hidden" name="htfcothicn"/>
													                                              
													                                              <textarea name="dfSelfFundsrcDets" id="dfSelfFundsrcDets" class="form-control" rows="2" disabled></textarea>
													                                             
										  													 
										  													      <input type="hidden" name="txtFldCDClntSrcIncmOthDet" id="txtFldCDClntSrcIncmOthDet"/>
										  													      <input type="hidden" name="dfSelfFundsrc" id="dfSelfFundsrc"/>
										  													 </li>
										  												</ul>
																						
																						</div>
																					</div>
										
										</div>
                        </div>
                    </div>
                </div>
                
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwoR">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_right" href="#collapseTwoR" aria-expanded="false" aria-controls="collapseTwoR">
                                <i class="fa fa-graduation-cap" style="color:#01579b;"></i>&nbsp;Education Details
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwoR" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwoR">
                        <div class="panel-body">
                           
                                      <div class="row">
									  
									  <div class="col-6">
									  <div class="card">
												<div class="card-header" id="cardHeaderStyle" ><span>Highest Education Qualification</span></div>
												<div class="card-body">
												<ul class="list-group" id="ulLiStyle-01">
  <li class="list-group-item">  
												
												<div class="form-check">
  <input class="form-check-input checkdb" type="radio" name="dfSelfEduLevel" id="Primary" value="Primary" checked>
  <label class="form-check-label frm-lable-fntsz" for="exampleRadios1">
    Primary
  </label>
</div>
  <li class="list-group-item"> 
<div class="form-check">
  <input class="form-check-input checkdb" type="radio" name="dfSelfEduLevel" id="Secondary" value="Secondary">
  <label class="form-check-label frm-lable-fntsz" for="exampleRadios2">
    Secondary
  </label>
</div>
  <li class="list-group-item"> 
<div class="form-check">
  <input class="form-check-input checkdb" type="radio" name="dfSelfEduLevel" id="Pre-Tertiary" value="Pre-Tertiary">
  <label class="form-check-label frm-lable-fntsz" for="exampleRadios3">
    Pre-Tertiary
  </label>
</div>
  <li class="list-group-item"> 
<div class="form-check">
  <input class="form-check-input checkdb" type="radio" name="dfSelfEduLevel" id="Tertiary_and_above" value="Tertiary_and_above">
  <label class="form-check-label frm-lable-fntsz" for="exampleRadios4">
    Tertiary and above
  </label>
</div>
  <li class="list-group-item"> 
<div class="form-check">
  <input class="form-check-input checkdb" type="radio" name="dfSelfEduLevel" id="GCE_N_or_O_Level" value="GCE_N_or_O_Level">
  <label class="form-check-label frm-lable-fntsz text-wrap" for="exampleRadios5" style="text-align:left;">
  GCE 'N' or 'O Level or Equivalent Academic Qualification
  </label>
</div>
</ul>
											 
												
												</div>
											
											</div>
									  </div>
									  <div class="col-6">
									  <div class="card">
												<div class="card-header" id="cardHeaderStyle" ><span>Language Proficiency</span></div>
												<div class="card-body">
												
												
												<ul class="list-group" id="ulLiStyle-01">
  <li class="list-group-item" >  
  <div class="form-check ">
  <input class="form-check-input checkdb" type="checkbox" id="inlineCheckbox1"  name="dfSelfEngSpoken" value="Y">
  <label class="form-check-label frm-lable-fntsz text-wrap" for="inlineCheckbox1"  style="text-align:left;">Proficient in <span class="text-primaryy">Spoken English</span></label>
</div></li>
  
  <li class="list-group-item">
    <div class="form-check">
  <input class="form-check-input checkdb" type="checkbox" id="inlineCheckbox2"  name="dfSelfEngWritten" value="Y">
  <label class="form-check-label frm-lable-fntsz text-wrap" for="inlineCheckbox2"  style="text-align:left;">Proficient in <span class="text-primaryy">Written English</span> </label>
</div>
  </li>
  </ul>
											 
												
												</div>
											
											</div>
											
											
                                      </div>
									  </div>
					 </div>
                    </div>
                </div>
                
               
                    <div class="panel panel-default" id="intrPrePanelId" style="display:block;">
										                    <div class="panel-heading" role="tab" id="headingIntrPreTrust">
										                        <h4 class="panel-title">
										                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_right" href="#headingInterpreterDetls" aria-expanded="false" aria-controls="headingInterpreterDetls">
										                                <i class="fa fa-user" style="color:#388e3c;"></i>&nbsp;Interpreter / Trusted Individual Details
										                            </a>
										                        </h4>
										                    </div>
										                    <div id="headingInterpreterDetls" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingIntrPreTrust">
										                        <div class="panel-body">
										                           
										                                      <div class="row">
																					
																				  <div class="col-6">
																			   
																				
																				<div class="card ">
																				     <div class="card-header" id="cardHeaderStyle">
																				        <div class="row">
																				          <div class="col-md-9">
																					          <div class="custom-control custom-switch ">
																								  <input type="checkbox" class="custom-control-input checkdb" id="btnInptrSwtch" onclick="enableIntrprt(this)" name="cdIntrprtFlg">
																								  <label class="custom-control-label pt-1" for="btnInptrSwtch">&nbsp;Any Interpreter</label>
																								</div> 
																							</div>
																				          <div class="col-md-3"><span class="badge badge-pill badge-warning mt-1" style="margin: 0px 0px 0px -3em;">Note :&nbsp;<i class="fa fa-commenting" aria-hidden="true" data-toggle="popoverPersDetls" data-trigger="hover" data-original-title="" title=""></i></span></div>
																				        </div>
																				     </div>
																				     
																				     <div class="card-body" id="tempIntprtDiv">
																				    
																				     
																				     <div class="row pl-3 ">
												                                                      <div class="col- "><img src="vendor/psgisubmit/img/infor.png" class="mt-2"></div>
												                                                    <div class="col-11">
													                                                     <div class="col-">
													                                                       <small class="font-italic bold text-primaryy font-sz-level8"> (If Any Interpreter / Trusted Individual presented then key-in the following details)</small>
													                                                     </div>
												                                                     </div>
												                                                </div>
																					    
																						<div class="">
																						    <form action="">
																						  <div class="form-group">
																						    <label class="frm-lable-fntsz" for="intprtName" style="margin-bottom: 0px;">Name</label>
																						    <input type="text" class="form-control checkdb" maxlength="60" name="intprtName" id="intprtName" disabled="true">
																						  </div>
																						  <div class="form-group">
																						    <label class="frm-lable-fntsz" for="intprtContact" style="margin-bottom: 0px;">Contact Number</label>
																						    <input type="text" class="form-control checkdb" maxlength="60" name="intprtContact" id="intprtContact" disabled="true">
																						  </div>
																						  
																						  <div class="form-group">
																						    <label class="frm-lable-fntsz" for="" style="margin-bottom: 0px;">Relationship</label>
																						    <select class="form-control checkdb" id="intprtRelat" name="intprtRelat" disabled="true">
																						    <option value="" selected="selected">--Select--</option>
																						    <% for(String relationship:psgprop.getString("app.relationship").split("\\^")){ %>
																								 <option value="<%=relationship%>"><%=relationship%></option>
																							<%}%>
																						    </select>
																						  </div>
																						  </form>
																						</div>				
																										
																										
																				     </div>
																				     
																				
																				</div>
																			</div> 	
																					   
																				<div class="col-6">
																			  <div class="card ">
																						<div class="card-header" id="cardHeaderStyle"><span>Language Use</span></div>
																						<div class="card-body">
																							<ul class="list-group" id="ulLiStyle-01">
																							  <li class="list-group-item">  
																									  <div class="form-check ">
																									  <input class="form-check-input checkdb" type="checkbox" name="htflaneng" id="htflaneng" value="Y" data="ENG" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																									  <label class="form-check-label frm-lable-fntsz" for="htflaneng"> English </label>
																									</div>
																							  </li>
																							  
																							  <li class="list-group-item">
																									    <div class="form-check">
																									  <input class="form-check-input checkdb" type="checkbox" name="htflanman" id="htflanman" value="Y" data="MAN" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																									  <label class="form-check-label frm-lable-fntsz" for="htflanman">Mandarin </label>
																									</div>
																							  </li>
																							  
																							  <li class="list-group-item">
																									  <div class="form-check">
																									  <input class="form-check-input checkdb" type="checkbox" name="htflanmalay" id="htflanmalay" value="Y" data="MAL" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																									  <label class="form-check-label frm-lable-fntsz" for="htflanmalay">Malay </label>
																									</div>
																							  </li>
																							  
																							  <li class="list-group-item">
																									    <div class="form-check">
																									  <input class="form-check-input checkdb" type="checkbox" name="htflantamil" id="htflantamil" value="Y" data="TAM" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																									  <label class="form-check-label frm-lable-fntsz" for="htflantamil">Tamil </label>
																									</div>
																							  </li>
																							  
																							  <li class="list-group-item">
																									    <div class="form-check">
																									  <input class="form-check-input checkdb" type="checkbox" name="htflanoth" id="htflanoth"  value="Y" data="OTH" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');chkSelfSpsOthrValid(this,'cdLanguageOth');" >
																									  <label class="form-check-label frm-lable-fntsz" for="htflanoth">Others </label>
																									  <textarea name="text" maxlength="60" name="cdLanguageOth" id="cdLanguageOth" disabled class="form-control checkdb" rows="2"></textarea>
																									   <input type="hidden" name="cdLanguages" id="cdLanguages"  />
																									</div>
																							  </li>
																							  
																							  </ul>
																						</div>
																					
																					</div>
																					
																					
										                                      </div>	
										                                      
										                                         
										                
										                
													
										        </div>
												  <br>
												<div class="row">
													<div class="col-12">
													
													<div class="card">
																			 <div class="card-body">
																					 <div class="list-group" id="listgroupbg-01">
																						  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
																						    <div class="d-flex w-100 justify-content-between">
																						      <h6 class="mb-1 bold font-sz-level6">Benefical Owner</h6>
																						     
																						       <div class="custom-control custom-switch">
																								  <input type="checkbox" class="custom-control-input checkdb" id="BtnInptrBenOwnSwtch" onclick="openOthInptrSec(this,'BENOWNER')" >
																								  <label class="custom-control-label" for="BtnInptrBenOwnSwtch">&nbsp;</label>
																						      </div>
																						    
																						    </div>
																						    <p class="mb-1 italic font-sz-level7">Is someone else expected to participate in, make decisions about, or benefit from this policy in any way?
																						(This does not include Account holder/Policyholder, Proposed Life Insured, Payer, Beneficiary or signing officer.)</p>
																						    
																						  </a>
																						  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
																						    <div class="d-flex w-100 justify-content-between">
																						      <h6 class="mb-1 bold font-sz-level6">Third Party Payer</h6>
																						          <div class="custom-control custom-switch">
																								  <input type="checkbox" class="custom-control-input checkdb" id="BtnInptrTppSwtch"  onclick="openOthInptrSec(this,'TPP')">
																								  <label class="custom-control-label" for="BtnInptrTppSwtch">&nbsp;</label>
																						         </div>
																						   </div>
																						    <p class="mb-1 italic font-sz-level7">Is anyone other than the Account holder/Policyholder be paying for this policy / Investment account?</p>
																						    
																						  </a>
																						  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
																						    <div class="d-flex w-100 justify-content-between">
																						      <h6 class="mb-1 bold font-sz-level6">PEP and RCA</h6>
																						      
																						       <div class="custom-control custom-switch">
																								  <input type="checkbox" class="custom-control-input checkdb" id="BtnInptrPepSwtch"  onclick="openOthInptrSec(this,'PEP')">
																								  <label class="custom-control-label" for="BtnInptrPepSwtch">&nbsp;</label>
																						      </div>
																						   
																						    </div>
																						    <p class="mb-1 italic font-sz-level7">Are you or any close relative(s) currently or previously held a senior position in a government, political party, military, tribunal or government-owned corporation?.</p>
																						    
																						  </a>
		<!-- Txtarea Modal1 -->																				</div>
		<div class="modal fade" id="txtAreaModal1">
		   <div class="modal-dialog modal-dialog-scrollable"">
				    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View Beneficiary.Details</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">�</button>
      </div>
       <input type="hidden" id="othperIdBen" name="othperIdBen">
       
      <!-- Modal body -->
      <div class="modal-body">
         <div class="row">
             <div class="col-md-12">
                  <div class="form-group">
					  <label class="frm-lable-fntsz">Name</label>
					     <input type="text" class="form-control checkdb" name="othperNameBen" id="txtFldCDBenfName" maxlength="60"/>
					     <input type="hidden" name="othperName" id="txtFldCDBenfId">
					  </div>
             
                  </div>
           </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.</label>
					    <input type="text" class="form-control checkdb" maxlength="15" name="othperNricBen" id="txtFldCDBenfNric">
					     
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.</label>
					   <input type="text" maxlength="60" class="form-control checkdb" name="othperIncorpnoBen" id="txtFldCDBenfIncNo">
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address</label>
                     <textarea class="form-control txtarea-hrlines checkdb" maxlength="60" rows="4" name="othperRegaddr" id="txtFldCDBenfAddr"></textarea>
                 </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held</label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkBen" class="checkdb" id="IntrBenJob" type="radio" value="Job" >
													    <label for="IntrBenJob" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
						 
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkBen" class="checkdb" id="IntrBenPol" type="radio" value="Political">
													    <label for="IntrBenPol" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/political.png">&nbsp;Political</span></label>
							                         </div> 	
					      </div>
					      
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkBen" class="checkdb" id="IntrBenMil" type="radio" value="Military">
													    <label for="IntrBenMil" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/soldier.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>							  
			 </div>
        </div>
             
             </div>
        </div>
        
        <div class="row">
             <div class="col-md-12">
             <div class="form-group">
   <label class="frm-lable-fntsz">In what country is / was the position held</label>
     <select class="custom-select checkdb" id="txtFldCDBenfJobCont" name="othperWorkCountryBen">
    <option selected="">--Select--</option>
       <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
	<option value="<%=country%>"><%=country%></option>
	 <%}%>
  </select>
  </div>
             </div>
        
        </div>
        
        
   <div class="row">
       <div class="col-md-12">
             <div class="form-group">
           <label class="frm-lable-fntsz">Relationship to Client (1)/(2)
            </label>
      <select class="custom-select checkdb" id="txtFldCDBenfRel" name="othperRelationBen">
    <option selected="">--Select--</option>
    <% for(String relationship:psgprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
  </div>
       </div>
  </div>
  
  
  <!--input hdnFld Configuration  -->
  
  <input type="hidden" name="othperId" id="othperId">
  <input type="hidden" name="fnaId" id="fnaId">
  
  <!--input hdnFld Configuration  -->
  
  
  
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal2">TPP Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal3">PEP Dets </button>
	   <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">CLOSE</button>
        
       
      </div>

    </div>
  </div>
</div>
<!--End  -->
<!--txtArea TargetModal2  -->
		  <div class="modal fade" id="txtAreaModal2">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View TPP.Details</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">�</button>
      </div>
	<input type="hidden" id="othperIdTpp" name="othperIdTpp">
      <!-- Modal body -->
       <div class="modal-body">
        
        
        <div class="row">
             <div class="col-md-12">
	              <div class="form-group">
	             <label class="frm-lable-fntsz">Name</label>
	             <input type="text" maxlength="60" class="form-control checkdb" name="othperNameTpp" id="txtFldCDTppName">
                  <input type="hidden" name="txtFldCDTppId" id="txtFldCDTppId">
              </div>
             
             </div>
          
        </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.</label>
					     <input type="text" maxlength="15" class="form-control checkdb" name="othperNricTpp" id="txtFldCDTppNric">
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.</label>
					     <input type="text" maxlength="60" class="form-control checkdb" name="othperIncorpnoTpp" id="txtFldCDTppIncNo">
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address</label>
                     <textarea class="form-control txtarea-hrlines checkdb" maxlength="60" rows="4" name="othperRegaddrTpp" id="txtFldCDTppAddr"></textarea>
                 </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held</label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkTpp" class="checkdb" id="IntrTppJob" type="radio" value="Job">
													    <label for="IntrTppJob" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkTpp" class="checkdb" id="IntrTppPol" type="radio" value="Political">
													    <label for="IntrTppPol" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/political.png">&nbsp;Political</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkTpp" class="checkdb" id="IntrTppMil" type="radio" value="Military">
													    <label for="IntrTppMil" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/soldier.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>							  
				 </div>
                
              </div>
             
             </div>
             
            
        
        </div>
        
        <div class="row">
             <div class="col-md-7">
             <div class="form-group">
   <label class="frm-lable-fntsz text-wrap" style="text-align:left;">In what country is / was the position held</label>
     <select class="custom-select checkdb" id="txtFldCDTppJobCont" name="othperWorkCountryTpp">
    <option selected="">--Select--</option>
   <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
	<option value="<%=country%>"><%=country%></option>
	 <%}%>
  </select>
  </div>
             </div>
             
             <div class="col-md-5">
             <div class="form-group">
  <label class="frm-lable-fntsz text-wrap" style="text-align:left;">Relationship to Client (1)/(2)
</label>
      <select class="custom-select checkdb" id="txtFldCDTppRel" name="othperRelationTpp">
    <option selected="">--Select--</option>
    <% for(String relationship:psgprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
  </div>
       </div>
        
        </div>
   <div class="row">
        <div class="col-md-12">
             <div class="form-group">
  <label class="frm-lable-fntsz mandLblTxtBenTPP">Payment Mode</label>
      <div class="row">
                     <div class="col-md-7">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="radPayChq" class="checkdb" name="othperPaymentmodeTpp" type="radio" value="CHQ">
													    <label for="radPayChq" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/cheque.png">&nbsp;Cheque/Cashier Order</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-5">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="radPayCrdCard" class="checkdb" name="othperPaymentmodeTpp" type="radio" value="CRDTCARD">
													    <label for="radPayCrdCard" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/card.png">&nbsp;Credit Card</span></label>
							                         </div> 	
							 </div>
													</div>
									</div>	
							  </div>
							       </div>
							       </div>
       
                                                  <div class="row">
                                                  
                                                           <div class="col-7">
			                                                     <div class="col-">
			                                                       <img src="vendor/psgisubmit/img/infor.png" class="mt-2">&nbsp;<small class="font-italic bold text-primaryy font-sz-level8">Please attach a copy of the Third Party Payer's NRIC/Passport/ACRA (For Company).</small>
			                                                     </div>
			                                                     <div class="col-">
                                                                 <div class="custom-file">
																	    <input type="file" class="custom-file-input checkdb" id="filecdTppNricCopy" name="filecdTppNricCopy">
																	    <label class="custom-file-label frm-lable-fntsz pt-1"  style="font-weight: normal;" for="filecdTppNricCopy">Choose file</label>
  																</div>
                                                                 </div>
		                                                     </div>
		                                                     
		                                                      <div class="col-5">
				                                                   <div class="form-group">
																		<label class="frm-lable-fntsz mandLblTxtBenTPP mt-3">Contact</label>
																		<input type="text" class="form-control checkdb" name="othperContactnoTpp" id="txtFldCDTppCcontact" maxlength="60">
																		<input type="text" class="form-control hide checkdb" name="othperBanknameTpp" id="txtFldCDTppBankName" maxlength="60">
																		 <input type="text" class="form-control hide checkdb" name="othperChqOrdernoTpp" id="txtFldCDTppChqNo" maxlength="60">
																	</div>
		                                                      </div>
		                                                   
		                                                </div>
			                                                
      </div>
	  
	  

      <!-- Modal footer -->
        <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal1">BENF Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal3">PEP Dets </button>
	   
	  
         <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">CLOSE</button>
        
       
      </div>

    </div>
  </div>
</div>
<!--End  -->
<!--TextArea Modal3  -->
<div class="modal fade" id="txtAreaModal3">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View PEP/RCA.Details</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">�</button>
      </div>
      <input type="hidden" id="othperIdPep" name="othperIdPep">
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
             <div class="col-md-12">
                  <div class="form-group">
				  <label class="badge">Name</label>
				     <input type="text" class="form-control checkdb" maxlength="60" name="othperNamePep" id="txtFldCDPepName">
				     <input type="hidden" name="txtFldCDPepId" id="txtFldCDPepId">
				  </div>
				             
             </div>
          
        </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.</label>
					  <input type="text" class="form-control checkdb" maxlength="15" name="othperNricPep" id="txtFldCDPepNric">
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.</label>
					    <input type="text" class="form-control checkdb" maxlength="60" name="othperIncorpnoPep" id="txtFldCDPepIncNo">
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address</label>
                     <textarea class="form-control txtarea-hrlines checkdb" maxlength="60" rows="4" name="txtFldCDPepAddr" id="txtFldCDPepAddr"></textarea>                 </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held</label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepJob" class="checkdb" name="othperWorkPep" type="radio" value="Job">
													    <label for="IntrPepJob" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepPol" class="checkdb" name="othperWorkPep" type="radio" value="Political">
													    <label for="IntrPepPol" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/political.png">&nbsp;Political</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepMil" class="checkdb" name="othperWorkPep" type="Military">
													    <label for="IntrPepMil" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/soldier.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>							  
				   </div>
            </div>
             
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
             <div class="form-group">
   <label class="frm-lable-fntsz">In what country is / was the position held</label>
     <select class="custom-select checkdb" id="txtFldCDPepJobCont" name="othperWorkCountryPep">
    <option selected="">--Select--</option>
   <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
	   <option value="<%=country%>"><%=country%></option>
    <%}%>
  </select>
  </div>
             </div>
        
        </div>
   <div class="row">
       <div class="col-md-12">
             <div class="form-group">
  <label class="frm-lable-fntsz">Relationship to Client (1)/(2)
</label>
      <select class="custom-select checkdb" id="txtFldCDPepRel" name="othperRelationPep">
    <option selected="">--Select--</option>
     <% for(String relationship:psgprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
  </div>
       </div>
  </div>
  
  
  
      </div>

      <!-- Modal footer -->
        <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal1">BENF Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal2">TPP Dets </button>
	   
	  
         <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">CLOSE</button>
        
       
      </div>

    </div>
  </div>
</div>
<!--End  -->
																					 </div>
																			</div>
																		</div>
																	
																	</div>
																	
																	 
															    </div>
																
																
															</div>
										                   
										                </div>
               
                
                
                
			 
            </div>
        </div>
		
		 
    </div>
	
	
</div>
									    <!-- End -->
									
									
									
                                                     
											
									
                                   
                                 </div>