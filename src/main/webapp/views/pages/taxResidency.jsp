<%@page import="com.afas.isubmit.util.PsgConst"%>
<%@ page language="java" import="java.util.*" %>
<%ResourceBundle psgprop = ResourceBundle.getBundle("properties/psgisubmit");%>
<html lang="en">

<head>
  
    <link rel="stylesheet" href="vendor/psgisubmit/css/tax-residency.css">
</head>

<body id="page-top">
  	 <!-- Container Fluid-->
       
	     <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		    <div class="col-5">
			 <h5>Tax Residency Declaration</h5>
			</div>
			
	        <div class="col-md-7">
	           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
			 </div>
       </div>
       
      <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
       </div>
         
<!--Page 11 content area Start  -->
			      <div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh">
						<div class="card-body" style="font-size: 13px;">
						     <jsp:include page="/views/pages/taxResiContent.jsp"></jsp:include>
				  </div>
	  
	          </div><!--  Container fluid end-->
	          
	  
      
      
  
   </div>
	  
	   
      <!--  End Container Fluid-->    
          
          
        <!--Add  Form Details Popup Page    -->   
          
<div class="modal fade" id="addModalFormDetls" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header" id="cardHeaderStyle-22">
        <h5 class="modal-title" id="exampleModalLabel" >Tax Residency Form Details</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <input type="hidden" name="taxId" id="sessTaxId">
      </div>
      <div class="modal-body">
        <div class="card">
             <div class="card-body">
                  <div class="row">
                       <div class="col-md-12">
                          <div class="form-group">
                              <label for="exampleInputPassword1" class="badge">Country of Tax Residence</label>
						               <select id="country" name="taxCountry" class="form-control checkdb" maxlength="60">
                <option selected="true" value="">--Select--</option>
                <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
				<option value="<%=country%>"><%=country%></option>
				 <%}%>
            </select>
                        <div class="invalid-feedbacks hide" id="cntryError">Select any one country.</div>
						  </div>
                       </div>
                       <div class="col-md-12">
                             <div class="form-group">
						    <label for="exampleInputPassword1" class="badge">Tax Identification Number (TIN)</label>
						    <input type="text" class="form-control checkdb" maxlength="20" id="taxRefNo" name="taxRefNo">
						  </div>
                       </div>
                       <div class="invalid-feedbacks hide" id="taxRefError">TIN Number cannot be empty.</div>
                  </div>
                  
                  <div class="row">
                       <div class="col-md-12">
                              <div class="form-group">
						    <label for="reasonToNoTax" class="badge">If no TIN available,enter <span class="text-secondary" >Reason A <i class="fa fa-info-circle" id="noteReasonA" aria-hidden="true" data-toggle="ReasonAPopup" data-trigger="hover"></i></span> 
						    
						    |<span  class="text-primaryy"> Reason B <i class="fa fa-info-circle" id="noteReasonB" aria-hidden="true" data-toggle="ReasonBPopup" data-trigger="hover"></i></span>
						     |<span class="text-info"> Reason C <i class="fa fa-info-circle" id="noteReasonC" aria-hidden="true" data-toggle="ReasonCPopup" data-trigger="hover"></i></span></label>
						    <select class="form-control checkdb" maxlength="20" id="reasonToNoTax" name="reasonToNoTax" onchange="enableReasonDets(this);">
						        <option selected="true" value="">--Select--</option>
						        <option>Reason A</option>
						        <option>Reason B</option>
						        <option>Reason C</option>
						    </select>
						    <div class="invalid-feedbacks hide" id="reasonToNoTaxError">Select any one reason.</div>
						  </div>
                       </div>
                       <div class="col-md-12">
                             <div class="form-group">
						    <label for="exampleInputPassword1" class="badge">Please state reason(s) 
                             if Reason B is selected</label>
						    <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="5" id="reasonbDetails" name="reasonbDetails"
						    maxlength="800" onkeydown="textCounter(this,800);" onkeyup="textCounter(this,800);"></textarea>
						    <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
						    <div class="invalid-feedbacks hide" id="reasonBError">Reason details cannot empty.</div>
						  </div>
                       </div>
                  </div>
             </div>
        
        </div>
        <!--set self and spouse hidden field values  -->
        <input type="hidden" id="hdnSelfNric">
        <input type="hidden" id="hdnSpsNric">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-sm btn-bs3-prime " style="background: green;border:1px solid green;"   onclick="saveTaxDets();">Add detls.</button>
        <button type="button" class="btn btn-sm btn-bs3-prime " data-dismiss="modal" style="background: grey;border:1px solid grey;">Close</button>
        
      </div>
    </div>
  </div>
</div>
        <!-- End Tax Resi.Form Details Page --> 
        
        <!-- Popover contents Tax Residency  each card notes content here -->


<div id="popover-ReasonAPopup" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;<span class="text-secondary"><strong>Reason A</strong></span> - The country where you are liable to pay tax does not issue TINs to its residents.</li>
    
  </ul>
</div>

<div id="popover-ReasonBPopup" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;<span class="text-primaryy"><strong>Reason B</strong></span> -  You are unable to obtain a TIN or equivalent number (Please explain in the above table if you have selected this reason).</li>
    
    
  </ul>
</div>

<div id="popover-ReasonCPopup" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;<span class="text-info"><strong>Reason C</strong></span> - No TIN is required. (Note: Only select this reason if the authorities of the country of tax residence entered above do not require a TIN to be disclosed). </li>
    
  </ul>
</div><!-- page 11 tax Resi Reason Popup Notes End -->
	  
   
 </body>
  
         
           <script src="vendor/psgisubmit/js/Common Script.js"></script>
           <script src="vendor/psgisubmit/js/session_timeout.js"></script>
          <script type="text/javascript" src="vendor/psgisubmit/js/tax_residency.js"></script>
          
 </html>
                