<%@page import="com.afas.isubmit.util.PsgConst"%>
<%@ page language="java" import="java.util.*" %>
<%ResourceBundle psgprop = ResourceBundle.getBundle("properties/psgisubmit");%>
<html lang="en">
<head>
	<link href="vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" >
</head>

<body id="page-top">
        
<div class="container-fluid" id="container-wrapper" style="min-height:75vh">
         <input type="hidden" id="hdnSessDataFormId" name="hdnSessDataFormId">
		 <div class="d-sm-flex align-items-center justify-content-between mb-1">
		        <div class="col-md-5">
		            <h6>Personal Details</h6>
		        </div>
		        
		        <div class="col-md-7">
		            <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
				</div>
          </div>
          
           <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>
          
		<div class="card mb-1 maincontentdiv " style="border:1px solid #044CB3;min-height:65vh" id="fnaPersonalForm">

		      <div class="card-body">
		  
		 			<jsp:include page="/views/pages/personalDetails.jsp"></jsp:include>
		     
		
		<!--Tabs  -->
		
	    </div>
        
		  
           <div class="modal fade" id="new2ModalScrollable" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="exampleModalLabelClient2" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="exampleModalLabelLogout"><i class="fa  fa-user-plus"></i>&nbsp;Add New Insured Details</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="card">
                    
                      <div class="card-body" style="height:350px;">
                      <div class="row">
                      
                           <div class="col-12">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb" id="SrchInsured" name="AddSrchInsured" value="SrchInsured" checked="checked">
								    <label class="custom-control-label" for="SrchInsured"><i class="fa fa-search" style="color: #1799b7;"></i>&nbsp;Search Existing Insured Details</label>
								 </div>
                           
                           </div>
                          
                           <div class="col-12 mt-3">
                                 <form action="">
									  <div class="form-group ml-5">
									    <label class="frm-lable-fntsz" for="" style="margin-bottom: 0px;">Search  Insured Name:</label>
									     <select class="form-control checkdb" id="selFldNRICName" style="width:350px" aria-hidden="true">
									     	<option value="" selected>Select Client</option>
											<!-- <optgroup id="addMewGrp" label="Add New Insured Details">
												<option>Add New</option>
											  </optgroup> -->
											  
											  <optgroup id="searchExistGrp" label="Search Insured Details">
												</optgroup>
			                             </select>
			                             
			                             <div class="invalid-feedbacks hide" id="selFldNRICNameError">
			                                  Select Insured Name / Insured Name cannot be empty 
			                             </div>
			                             
			                            
									  </div>
						        </form>
                           </div>
                           
                           <div class="col-12 mt-4">
                                <div class="custom-control custom-radio">
								    <input type="radio" class="custom-control-input checkdb" id="AddInsured" name="AddSrchInsured" value="AddInsured" onclick="addNewInsuredClnt(this)">
								    <label class="custom-control-label" for="AddInsured"><i class="fa  fa-user-plus" style="color: #0aa00ad4;"></i>&nbsp;Add New Insured Details</label>
								 </div>
                           
                           </div>
                           
                           
                      </div>
                        
                      </div>
                      
                    
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Cancel</button>
                  <button type="button" class="btn btn-sm btn-bs3-prime" id="addNewClient2Btn">OK</button>
                </div>
              </div>
            </div>
          </div>

          <div class="modal fade" id="genKYCModalScrollable" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="genKYCModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="genKYCModalScrollableTitle">Loading...</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  
                  <div class="col-lg-12">
              
               <div class="loader">
					  <p></p>
					</div>
              
            </div>
                  
                  
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">OK</button>
                  
                </div>
              </div>
            </div>
          </div>
		  
<div class="modal fade" id="txtAreaModal1" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View Beneficiary.Details</h6>
        <button type="button" class="close inptrModalClass" data-dismiss="modal" style="color:#fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
         <div class="row">
             <div class="col-md-12">
                  <div class="form-group">
					  <label class="frm-lable-fntsz">Name<sup style="color: red;"><strong>*</strong></sup></label>
					  <input type="hidden" id="othperIdBen" name="othperIdBen">
					     <input type="text" class="form-control checkdb mandatory checkMand" name="othperNameBen" 
					     id="othperNameBen" maxlength="60" onchange="validateFld(this)">
					     <input type="hidden" name="txtFldCDBenfId" id="txtFldCDBenfId">
					     <div class="invalid-feedbacks hide" id="othperNameBenError">Keyin Name</div>
					  </div>
					</div>
           </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.<sup style="color: red;"><strong>*</strong></sup></label>
					    <input type="text" class="form-control checkdb mandatory checkMand"  name="othperNricBen"
					     id="othperNricBen" maxlength="20">
					     <div class="invalid-feedbacks hide" id="othperNricBenError">Keyin NRIC / Passport No. </div>
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.<sup style="color: red;"><strong>*</strong></sup></label>
					   <input type="text" class="form-control checkdb mandatory checkMand"  name="othperIncorpnoBen"
					    id="othperIncorpnoBen" maxlength="20">
					    <div class="invalid-feedbacks hide" id="othperIncorpnoBenError">Keyin Entity Incorporation No.</div>
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address<sup style="color: red;"><strong>*</strong></sup></label>
                     <textarea class="form-control checkdb txtarea-hrlines text-wrap checkMand mandatory" rows="4" name="txtFldCDBenfAddr" id="txtFldCDBenfAddr" 
                     maxlength="450" onkeydown="textCounter(this,450);" onkeyup="textCounter(this,450);"></textarea>
                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
                     <div class="invalid-feedbacks hide" id="txtFldCDBenfAddrError">Keyin Registered Residential / Cooperation Address</div>
                 </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held<sup style="color: red;"><strong>*</strong></sup></label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkBen" id="IntrBenJob" type="radio" value="JOB" maxlength="30" class="checkdb mandatory">
													    <label for="IntrBenJob" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
						 
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkBen" id="IntrBenPol" type="radio" value="POLITICAL" maxlength="30" class="checkdb mandatory">
													    <label for="IntrBenPol" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/job.png">&nbsp;Political</span></label>
							                         </div> 	
					      </div>
					      
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkBen" id="IntrBenMil" type="radio" value="MILITARY" maxlength="30" class="checkdb mandatory">
													    <label for="IntrBenMil" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/job.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						<div class="invalid-feedbacks hide ml-3" id="othperWorkBenError">Select the anyone option in  Job / Political / Military Title of Position Held</div>						  
			 </div>
        </div>
             
             </div>
        </div>
        
        <div class="row">
             <div class="col-md-12">
             <div class="form-group">
   <label class="frm-lable-fntsz">In what country is / was the position held<sup style="color: red;"><strong>*</strong></sup></label>
     <select class="custom-select checkdb checkMand mandatory" id="othperWorkCountryBen" name="othperWorkCountryBen" maxlength="30">
    <option selected value="">--Select--</option>
       <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
	<option value="<%=country%>"><%=country%></option>
	 <%}%>
  </select>
  <div class="invalid-feedbacks hide" id="othperWorkCountryBenError">Select the Country</div>
  </div>
             </div>
        
        </div>
        
        
   <div class="row">
       <div class="col-md-12">
             <div class="form-group">
           <label class="frm-lable-fntsz">Relationship to Client (1)/(2)<sup style="color: red;"><strong>*</strong></sup>
            </label>
      <select class="custom-select checkdb checkMand mandatory" id="othperRelationBen" name="othperRelationBen" maxlength="60" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
    <% for(String relationship:psgprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
  <div class="invalid-feedbacks hide" id="othperRelationBenError">Select the Relationship to Client (1)/(2)</div>
  </div>
       </div>
  </div>
  
  
  <!--input hdnFld Configuration  -->
  
  <input type="hidden" name="othperId" id="othperId">
  
  <!--input hdnFld Configuration  -->
  
  
  
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnBenTpp" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal2">TPP Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnBenPep" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal3">PEP Dets </button>
	   <button type="button" class="btn btn-sm btn-bs3-prime inptrModalClass" data-dismiss="modal">Ok</button>
        
       
      </div>

    </div>
  </div>
</div>













<div class="modal fade" id="txtAreaModal2" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View TPP.Details</h6>
        <button type="button" class="close inptrModalClass" data-dismiss="modal" style="color:#fff;">&times;</button>
      </div>

      <!-- Modal body -->
       <div class="modal-body">
        
        
        <div class="row">
             <div class="col-md-12">
	              <div class="form-group">
	             <label class="frm-lable-fntsz">Name<sup style="color: red;"><strong>*</strong></sup></label>
	              <input type="text" class="form-control checkdb mandatory checkMand" name="othperNameTpp"
	              id="othperNameTpp" maxlength="60" onchange="validateFld(this)">
                  <input type="hidden" id="othperIdTpp" name="othperIdTpp">
                  <div class="invalid-feedbacks hide" id="othperNameTppError">Keyin Name</div>
              </div>
             
             </div>
          
        </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.<sup style="color: red;"><strong>*</strong></sup></label>
					     <input type="text" class="form-control checkdb mandatory checkMand" name="othperNricTpp" 
					     id="othperNricTpp" maxlength="20"  onchange="validateFld(this)">
					     <div class="invalid-feedbacks hide" id="othperNricTppError">Keyin NRIC / Passport No.</div>
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.<sup style="color: red;"><strong>*</strong></sup></label>
					     <input type="text" class="form-control checkdb checkMand mandatory" name="othperIncorpnoTpp"
					      id="othperIncorpnoTpp" maxlength="20" onchange="validateFld(this)">
					      <div class="invalid-feedbacks hide" id="othperIncorpnoTppError">Keyin Entity Incorporation No.</div>
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address<sup style="color: red;"><strong>*</strong></sup></label>
                     <textarea class="form-control checkdb txtarea-hrlines text-wrap checkMand mandatory" rows="4" name="txtFldCDTppAddr" id="txtFldCDTppAddr"
                     maxlength="450" onkeydown="textCounter(this,450);" onkeyup="textCounter(this,450);" onchange="validateFld(this)"></textarea>
                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
                     <div class="invalid-feedbacks hide" id="txtFldCDTppAddrError">Keyin  Registered Residential / Cooperation Address</div>
                 </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held<sup style="color: red;"><strong>*</strong></sup></label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkTpp" id="IntrTppJob" type="radio" value="JOB" maxlength="30" class="checkdb mandatory"  onchange="radioAndChkboxFld(this)">
													    <label for="IntrTppJob" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkTpp" id="IntrTppPol" type="radio" value="POLITICAL" maxlength="30" class="checkdb mandatory" onchange="radioAndChkboxFld(this)" > 
													    <label for="IntrTppPol" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/political.png">&nbsp;Political</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="othperWorkTpp" id="IntrTppMil" type="radio" value="MILITARY" maxlength="30" class="checkdb mandatory" onchange="radioAndChkboxFld(this)">
													    <label for="IntrTppMil" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/soldier.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						<div class="invalid-feedbacks hide ml-3" id="othperWorkTppError">Select the anyone of the option in Job / Political / Military Title of Position Held </div>						  
				 </div>
                
              </div>
             
             </div>
             
            
        
        </div>
        
        <div class="row">
             <div class="col-md-7">
             <div class="form-group">
   <label class="frm-lable-fntsz  checkMand mandatory" style="text-align:left;">In what country is / was the position held<sup style="color: red;"><strong>*</strong></sup></label>
     <select class="custom-select checkdb" id="othperWorkCountryTpp" name="othperWorkCountryTpp" maxlength="30" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
   <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
	<option value="<%=country%>"><%=country%></option>
	 <%}%>
  </select>
  <div class="invalid-feedbacks hide" id="othperWorkCountryTppError">Select the Country</div>
  </div>
             </div>
             
             <div class="col-md-5">
             <div class="form-group">
  <label class="frm-lable-fntsz text-wrap" style="text-align:left;">Relationship to Client (1)/(2)<sup style="color: red;"><strong>*</strong></sup>
</label>
      <select class="custom-select checkdb checkMand mandatory" id="othperRelationTpp" name="othperRelationTpp" maxlength="60" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
    <% for(String relationship:psgprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
  <div class="invalid-feedbacks hide" id="othperRelationTppError">Select the Relationship to Client (1)/(2)</div>
  </div>
       </div>
        
        </div>
   <div class="row">
        <div class="col-md-12">
             <div class="form-group">
  <label class="frm-lable-fntsz mandLblTxtBenTPP">Payment Mode<sup style="color: red;"><strong>*</strong></sup></label>
      <div class="row">
                     <div class="col-md-7">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="radPayChq" name="othperPaymentmodeTpp" type="radio" value="CHQ" maxlength="20" class="checkdb mandatory" onchange="radioAndChkboxFld(this)" >
													    <label for="radPayChq" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/cheque.png">&nbsp;Cheque/Cashier Order</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-5">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="radPayCrdCard" name="othperPaymentmodeTpp" type="radio" value="CRDTCARD" maxlength="20" class="checkdb mandatory" onchange="radioAndChkboxFld(this)" >
													    <label for="radPayCrdCard" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/card.png">&nbsp;Credit Card</span></label>
							                         </div> 	
							 </div>
													</div>
													
							 <div class="invalid-feedbacks hide ml-3" id="othperPaymentmodeTppError">Select anyone of the Payment Mode</div>
													
									</div>	
							  </div>
							       </div>
							       </div>
       
                                                  <div class="row">
                                                  
                                                           <div class="col-7">
			                                                     <div class="col-">
			                                                       <img src="vendor/psgisubmit/img/infor.png" class="mt-2">&nbsp;<small class="font-italic bold text-primaryy font-sz-level8">Please attach a copy of the Third Party Payer's NRIC/Passport/ACRA (For Company).</small>
			                                                     </div>
			                                                     <div class="col-">
                                                                 <div class="custom-file">
																	    <input type="file" class="custom-file-input checkdb mandatory" id="filecdTppNricCopy" name="filecdTppNricCopy" disabled>
																	    <label class="custom-file-label frm-lable-fntsz pt-1"  style="font-weight: normal;" for="filecdTppNricCopy">Choose file</label>
  																</div>
                                                                 </div>
		                                                     </div>
		                                                     
		                                                      <div class="col-5">
				                                                   <div class="form-group">
																		<label class="frm-lable-fntsz mandLblTxtBenTPP mt-3">Contact<sup style="color: red;"><strong>*</strong></sup></label>
																		<input type="text" class="form-control checkdb checkMand mandatory" name="othperContactnoTpp" id="othperContactnoTpp" maxlength="20" onchange="validateFld(this)">
																		<input type="text" class="form-control checkdb hide" name="othperBanknameTpp" id="othperBanknameTpp" maxlength="60" >
																		 <input type="text" class="form-control checkdb hide" name="othperChqOrdernoTpp" id="othperChqOrdernoTpp" maxlength="20">
																		  <div class="invalid-feedbacks hide" id="othperContactnoTppError">Keyin Contact</div>
																		  
																	</div>
		                                                      </div>
		                                                   
		                                                </div>
			                                                
      </div>
	  
	  

      <!-- Modal footer -->
        <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnTppBen" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal1">BENF Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnTppPep" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal3">PEP Dets </button>
	   
	  
         <button type="button" class="btn btn-sm btn-bs3-prime inptrModalClass" data-dismiss="modal">Ok</button>
        
       
      </div>

    </div>
  </div>
</div>



<div class="modal fade" id="txtAreaModal3" data-backdrop="static" data-keyboard="false" >
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;View PEP/RCA.Details</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">&times;</button>
      </div>
      
      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
             <div class="col-md-12">
                  <div class="form-group">
				  <label class="badge">Name<sup style="color: red;"><strong>*</strong></sup></label>
				     <input type="text" class="form-control checkdb mandatory checkMand" name="othperNamePep"
				      id="othperNamePep" maxlength="60" onchange="validateFld(this)">
				     <input type="hidden" name="othperIdPep" id="othperIdPep">
				      <div class="invalid-feedbacks hide" id="othperNamePepError">Keyin Name</div>
				  </div>
				             
             </div>
          
        </div>
        
        <div class="row">
             <div class="col-md-6">
                 <div class="form-group">
					  <label class="frm-lable-fntsz">NRIC / Passport No.<sup style="color: red;"><strong>*</strong></sup></label>
					  <input type="text" class="form-control checkdb mandatory" name="othperNricPep"
					   id="othperNricPep" maxlength="20" onchange="validateFld(this)">
					    <div class="invalid-feedbacks hide" id="othperNricPepError">Keyin NRIC / Passport No.</div>
                  </div>
             </div>
             
              <div class="col-md-6">
                    <div class="form-group">
					  <label class="frm-lable-fntsz">Entity Incorporation No.<sup style="color: red;"><strong>*</strong></sup></label>
					    <input type="text" class="form-control checkdb checkMand mandatory" name="othperIncorpnoPep"
					     id="othperIncorpnoPep" maxlength="20"onchange="validateFld(this)">
					      <div class="invalid-feedbacks hide" id="othperIncorpnoPepError">Keyin Entity Incorporation No.</div>
                  </div>
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
                 <div class="form-group">
                     <label class="frm-lable-fntsz">Registered Residential / Cooperation Address<sup style="color: red;"><strong>*</strong></sup></label>
                     <textarea class="form-control checkdb txtarea-hrlines text-wrap checkMand mandatory" rows="4" name="txtFldCDPepAddr" id="txtFldCDPepAddr"
                     maxlength="450" onkeydown="textCounter(this,450);" onkeyup="textCounter(this,450);"  onchange="validateFld(this)"></textarea>
                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
                      <div class="invalid-feedbacks hide" id="txtFldCDPepAddrError">Select the Registered Residential / Cooperation Address</div>
                                    </div>
             </div>
        
        </div>
        
        <div class="row">
            <div class="col-md-12">
             
             <div class="form-group">
                  <label class="frm-lable-fntsz">Job / Political / Military Title of Position Held<sup style="color: red;"><strong>*</strong></sup></label><br>
                <div class="row">
                     <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepJob" name="othperWorkPep" type="radio" value="JOB" maxlength="30" class="checkdb mandatory" onchange="radioAndChkboxFld(this)">
													    <label for="IntrPepJob" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/job.png">&nbsp;Job</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepPol" name="othperWorkPep" type="radio" value="POLITICAL" maxlength="30" class="checkdb mandatory" onchange="radioAndChkboxFld(this)">
													    <label for="IntrPepPol" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/political.png">&nbsp;Political</span></label>
							                         </div> 	
							 </div>
						</div>	
						
						 <div class="col-md-4">
                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input id="IntrPepMil" name="othperWorkPep" type="radio" value="MILITARY" maxlength="30" class="checkdb mandatory" onchange="radioAndChkboxFld(this)">
													    <label for="IntrPepMil" id="lblTxtSz-01"><span><img src="vendor/psgisubmit/img/soldier.png">&nbsp;Military</span></label>
							                         </div> 	
							 </div>
						</div>
						 <div class="invalid-feedbacks hide ml-3" id="othperWorkPepError">Select anyone of the option in  Job / Political / Military Title of Position Held</div>							  
				   </div>
            </div>
             
             </div>
        
        </div>
        
        <div class="row">
             <div class="col-md-12">
             <div class="form-group">
   <label class="frm-lable-fntsz">In what country is / was the position held<sup style="color: red;"><strong>*</strong></sup></label>
     <select class="custom-select checkdb checkMand mandatory" id="othperWorkCountryPep" 
     name="othperWorkCountryPep" maxlength="30" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
   <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
	   <option value="<%=country%>"><%=country%></option>
    <%}%>
  </select>
   <div class="invalid-feedbacks hide" id="othperWorkCountryPepError">Select the Country</div>
  </div>
             </div>
        
        </div>
   <div class="row">
       <div class="col-md-12">
             <div class="form-group">
  <label class="frm-lable-fntsz">Relationship to Client (1)/(2)<sup style="color: red;"><strong>*</strong></sup>
</label>
      <select class="custom-select checkdb checkMand mandatory" id="othperRelationPep"
       name="othperRelationPep" maxlength="60" onchange="validateFld(this)">
    <option selected value="">--Select--</option>
     <% for(String relationship:psgprop.getString("app.relationship").split("\\^")){ %>
		<option value="<%=relationship%>"><%=relationship%></option>
		<%}%>
  </select>
   <div class="invalid-feedbacks hide" id="othperRelationPeptxtFldCDPepRelError">Select the Relatinship</div>
  </div>
       </div>
  </div>
  
  <input type="hidden" id="hTxtDataFrmId" name="hTxtDataFrmId" value="${SELF_SPS_ID}">
  
      </div>

      <!-- Modal footer -->
        <div class="modal-footer">
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnPepBen" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal1">BENF Dets</button>
	  
	  <button type="button" class="btn btn-sm btn-bs3-prime mr-auto inptrModalClass" id="btnPepTpp" data-dismiss="modal" data-toggle="modal" data-target="#txtAreaModal2">TPP Dets </button>
	   
	  
         <button type="button" class="btn btn-sm btn-bs3-prime inptrModalClass" data-dismiss="modal">Ok</button>
        
       
      </div>

    </div>
  </div>
</div>







  
</div>
  
   </div>
   
  
   
   
   
    <!-- Popover contents kycpage2.html  personal details card notes content here -->
		 <div id="popover-contentPg2PersonalDetlsCardNotes" style="display: none">
		  <ul class="list-group custom-popover">
		    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Our company may contact you to confirm your understanding of the product(s) recommended by the Representative.</li>
		  </ul>
		 </div>
		<!--End--> 

         <script src="vendor/jquery-easing/jquery.easing.min.js"></script> 
          <!-- <script src="vendor/psgisubmit/js/ekyc-layout.js"></script> --> 
          <script src="js/jquery.steps.js"></script>
          <script src="js/select2.js"></script>
		  <script src="vendor/gojs/go.js"></script>  
		  <script src="vendor/gojs/FreehandDrawingTool.js"></script>
		  <script src="vendor/gojs/GeometryReshapingTool.js"></script>
		  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
		  <script src="vendor/psgisubmit/js/Common Script.js"></script>
		  <script src="vendor/psgisubmit/js/kyc_home.js"></script>
		  <script src="vendor/psgisubmit/js/session_timeout.js"></script>
		  
		  
		  
		  <!-- <script>jsnDataCustomerDetails =${CUSTOMER_DETAILS}</script>
		  
		  <script>jsnDataSelfSpsDetails = ${SELFSPOUSE_DETAILS}</script> -->
<script>

$(function ()
       {
                    $("#wizard").steps({
                        headerTag: "h2",
                        bodyTag: "section",
                        transitionEffect: "slideLeft",
                        stepsOrientation: "vertical",
						titleTemplate: '<span class="badge badge--info">#index#.</span> #title#',
						onFinishing: function (event, currentIndex) {
							//$("#genKYCModalScrollable").modal("show");
							window.location.href="kycHome"
							return true; 
						
						}
						
						
            });
                window.addEventListener("beforeunload", function (e) {
        				// saveData();
        	    });   
                    
                });
	//Bootstrap Date Picker
		  $('#simple-date1 .input-group.date').datepicker({
		  	//dateFormat: 'dd/mm/yyyy',
		  	 format: 'dd/mm/yyyy',
		  	todayBtn: 'linked',
		  	language: "it",
		  	todayHighlight: true,
		  	autoclose: true,         
		  });

		  $('#simple-date2 .input-group.date').datepicker({
		  	//dateFormat: 'dd/mm/yyyy',
		  	  format: 'dd/mm/yyyy',
		  	todayBtn: 'linked',
		  	language: "it",
		  	todayHighlight: true,
		  	autoclose: true,         
		  });
		  

</script>
		  
</body>
</html>