<%@ page language="java" import="com.afas.isubmit.util.PsgConst" %>
<script src="vendor/psgisubmit/js/gojs/go.js"></script>
  <script src="vendor/psgisubmit/js/gojs/FreehandDrawingTool.js"></script>
  <script src="vendor/psgisubmit/js/gojs/GeometryReshapingTool.js"></script>
 <link rel="stylesheet" href="vendor/psgisubmit/css/stylesheet.css"/>
  <script src="vendor/psgisubmit/js/scan_sign.js"></script>
<div class="modal right fade" id="ScanToSignModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
      <!-- Modal body -->
        <div class="modal-body">
        <small class="noteInfoMsg" title="Note Information"><img src="vendor/psgisubmit/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;<span id="noteInfo">Click on your name to generate QR Code</span></small>
     <input type="hidden" id="hdnSessFnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
     <input type="hidden" id="hdnSessSpsName" value="${SESS_DF_SPS_NAME}"/>
     <input type="hidden" id="curPage" value="<%=session.getAttribute("CURRENT_SCREEN")%>"/>
<div class="tab-regular mt-3" id="clntProdTabs" style="width:100%;">
                              <ul class="nav nav-tabs " id="myTab" role="tablist">
                                 <li class="nav-item"> <a class="nav-link active" id="test1-tab" data-toggle="tab" href="#test1" style="padding: 8px;" role="tab" aria-controls="test1" aria-selected="true"><i class="fa fa-qrcode" aria-hidden="true"></i>&nbsp;Scan to Sign</a> </li>
                                 <!-- <li class="nav-item"> <a class="nav-link" id="test2-tab" data-toggle="tab" href="#test2" style="padding: 8px;" role="tab" aria-controls="test2" aria-selected="false"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Signpad</a> </li> -->
                                
                              </ul>
                              <!--Client 1 Table Tab Content Start  -->
                              <div class="tab-content" id="myTabContent" style="margin: 15px;">
                                 <div class="tab-pane fade show active" id="test1" role="tabpanel" aria-labelledby="test1-tab">
                                   
									<!-- Signature Card Start  -->
									<div class="row">
									    <div class="col-12">
									         <div class="card p-3">
								                    <div class="row">
								                        <div class="col-5 border-right">
								                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
										                            <a id="scanToSignClient1Btn" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-Client1" role="tab" onclick="genQrOrSign('CLIENT','Client1');">${SESS_DF_SELF_NAME}
										                            <br/><small>Client(1)</small><small class="float-right"><i id="signmodalclient1qrsign" class="fa fa-exclamation-triangle" aria-hidden="true"></i></small>
										                            </a> 
										                            <%-- <c:if test = "${SESS_DF_SPS_NAME ne null}">
            															 <a  class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-Client2" role="tab" onclick="genQrOrSign('SPS','Client2');">${SESS_DF_SPS_NAME}
																	    <br/><small>Client(2)</small><small class="float-right"><i id="signmodalclient2qrsign" class="fa fa-exclamation-triangle" aria-hidden="true"></i></small>
																	    </a>
            														</c:if> --%>
										                            <%
 										                            	if (session.getAttribute("SESS_DF_SPS_NAME") != null && !"".equals(session.getAttribute("SESS_DF_SPS_NAME"))) {
 										                            %>
																	    <a  class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-Client2" role="tab" onclick="genQrOrSign('SPS','Client2');"><%=session.getAttribute("SESS_DF_SPS_NAME")%>
																	    <br/><small>Client(2)</small><small class="float-right"><i id="signmodalclient2qrsign" class="fa fa-exclamation-triangle" aria-hidden="true"></i></small>
																	    </a>
																	<%
																		}
																	%>
																	   
										                             
										                            <a id="signAdvisor" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-Advisor" role="tab" onclick="genQrOrSign('ADVISER','Adviser');">${SESS_ADVISER_NAME}
										                            <br/><small>Adviser</small><small class="float-right"><i id="signmodaladviserqrsign" class="fa fa-exclamation-triangle" aria-hidden="true"></i></small>
										                            </a>
										                            <%-- <a id="signManagerQr" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-Manager" role="tab" onclick="getQrCode('MANAGER','qrCodeManager','signLoaderManager');" style="display:none;"><%= session.getAttribute(Constants.SESS_MANAGER_NAME) %></a> --%>
								                             </div>
								                        </div>
								                        <div class="col-7"><!-- style="width:100%;height:22vh;"  -->
								                            <div class="tab-content" id="v-pills-tabContent">
								                                <div class="tab-pane fade active show " id="v-pills-Client1" role="tabpanel" aria-labelledby="v-pills-Client1-tab">
								                                      <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Client(1) : ${SESS_DF_SELF_NAME}</span>
								                                           
								                                           </div>
								                                           <div class="card-body">
								                                                 <!-- Signature Loader -->
								                                                      <img src="vendor/psgisubmit/img/signload.gif" id="signLoaderClient1" style="margin: 10% 32%; width: 40%; display: none;">
								                                                  <!-- end -->
								                                               	 <img src="" id="qrCodeClient1">
								                                           </div>
								                                           
								                                           <div class="card-body text-center" id="signSecClient1">
									                                           <div id="self">
	  																			 <div id="selfDiagram" style="border: solid 1px black; width: 100%; height: 250px" class="disabledsec"></div>
	  																		   	<div id="buttons">
																				    <!-- <button onclick="selfSave();">Save</button> -->
																				    <!-- <button onclick="selfClearDiagram('selfSavedDiagram');">Clear</button> -->
																				     <button class="genqrbtn" onclick="generateQrCode('CLIENT','Client1');">Clear Signature</button>
																			  </div>
																			  <form name="selfform">
																			  <input type="hidden" id="selfSignId" name="selfSignId">
																			  <input type="hidden" id="selfLoad" name="selfLoad" value="Y">
																			  <textarea id="selfSavedDiagram" name="selfSavedDiagram" style="width:100%;height:300px;display:none">
																			{ "position": "0 0",
																			  "model": { "class": "GraphLinksModel",
																			  "nodeDataArray": [],
																			  "linkDataArray": []} }
																			  </textarea>
																			  </form>
									                                      </div>  
									                                    </div>
									                                    <div class="card-footer p-1 bg-info"><div class="font-sz-level7 white" id="spanClientSignWaitMsg"></div></div> 
								                                      </div>  
								                                </div>
								                                <div class="tab-pane fade" id="v-pills-Client2" role="tabpanel" aria-labelledby="v-pills-Client2-tab">
								                                        <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Client(2) : ${SESS_DF_SPS_NAME}</span>
								                                           
								                                           </div>
								                                           <div class="card-body">
								                                                <!-- Signature Loader -->
								                                                      <img src="vendor/psgisubmit/img/signload.gif" id="signLoaderClient2" style="margin: 10% 32%; width: 40%; display: none;">
								                                                  <!-- end -->
								                                               	 <img src="" id="qrCodeClient2">
								                                           </div>
								                                           
								                                                <div class="card-body text-center" id="signSecClient2">
								                                           <div id="sps">
  																			 <div id="spsDiagram" style="border: solid 1px black; width: 100%; height: 250px" class="disabledsec"></div>
  																		   	<div id="buttons">
  																		   	   <!-- <button onclick="spsClearDiagram('spsSavedDiagram');">Clear</button>
																			    <button onclick="spsSave();">Save</button> -->
																			    <button class="genqrbtn" onclick="generateQrCode('SPS','Client2');">Clear Signature</button>
																			    
																			    
																		  
																		  </div>
																		  <form name="spsform">
																		 <input type="hidden" id="spsSignId" name="spsSignId">
																		 <input type="hidden" id="spsLoad" name="spsLoad" value="Y">
																		  <textarea id="spsSavedDiagram" name="spsSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div>  
								                                      </div>
								                                           
								                                            <div class="card-footer p-1 bg-info"><div class="font-sz-level7 white" id="spanSpsSignWaitMsg"></div></div> 
								                                      </div> 
								                                </div>
								                                
								                                <div class="tab-pane fade" id="v-pills-Advisor" role="tabpanel" aria-labelledby="v-pills-Advisor-tab">
								                                          <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Adviser : ${SESS_ADVISER_NAME}</span>
								                                           
								                                           </div>
								                                           <div class="card-body">
								                                                  <!-- Signature Loader -->
								                                                      <img src="vendor/psgisubmit/img/signload.gif" id="signLoaderAdviser" style="margin: 10% 32%; width: 40%; display: none;">
								                                                  <!-- end -->
								                                               	 <img src="" id="qrCodeAdviser">
								                                           </div>
								                                           
								                                         <div class="card-body text-center" id="signSecAdviser">
								                                           <div id="adv">
  																			 <div id="advDiagram" style="border: solid 1px black; width: 100%; height: 250px" class="disabledsec"></div>
  																		   	<div id="buttons">
  																		   	    <!-- <button onclick="advClearDiagram('advSavedDiagram');">Clear</button>
																			    <button onclick="advSave();">Save</button>
																			     -->
																		       <button class="genqrbtn" onclick="generateQrCode('ADVISER','Adviser');">Clear Signature</button>
																		  </div>
																		  <form name="advform">
																		  <input type="hidden" id="advSignId" name="advSignId">
																		  <input type="hidden" id="advLoad" name="advLoad" value="Y">
																		  <textarea id="advSavedDiagram" name="advSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div> 
								                                      </div> 
								                                      <div class="card-footer p-1 bg-info"><div class="font-sz-level7 white" id="spanAdvSignWaitMsg"></div></div>
								                                      </div> 
								                                </div>
								                              
								                                <div class="tab-pane fade" id="v-pills-Manager" role="tabpanel" aria-labelledby="v-pills-Manager-tab">
								                                          <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Manager : <%=session.getAttribute(PsgConst.SESS_MANAGER_NAME)%></span>
								                                            
								                                           </div>
								                                           <div class="card-body">
								                                                   <!-- Signature Loader -->
								                                                      <img src="vendor/psgisubmit/img/signload.gif" id="signLoaderManager" style="margin: 10% 32%; width: 40%; display: none;">
								                                                  <!-- end -->
								                                               	 <img src="" style="" id="qrCodeManager">
								                                           </div>
								                                           
								                                           <div class="card-body text-center" id="signSecManager">
								                                                 <div id="mgr">
  																			 <div id="mgrDiagram" style="border: solid 1px black; width: 100%; height: 250px" class="disabledsec"></div>
  																			 <div id="buttons">
  																		   	    <!-- <button onclick="advClearDiagram('advSavedDiagram');">Clear</button>
																			    <button onclick="advSave();">Save</button>
																			     -->
																		       <button class="genqrbtn" onclick="generateQrCode('MANAGER','Manager');">Clear Signature</button>
																		  </div>
  																		   	<!-- <div id="buttons"><button onclick="mgrClearDiagram('mgrSavedDiagram');">Clear</button>
																			    <button onclick="mgrSave();">Save</button>
																			    
																		  
																		  </div> -->
																		  <form name="mgrform">
																		   <input type="hidden" id="mgrSignId" name="mgrSignId">
																		   <input type="hidden" id="mgrLoad" name="mgrLoad" value="Y">
																		  <textarea id="mgrSavedDiagram" name="mgrSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                           </div>
								                                           
								                                           
								                                      </div> 
								                                      
								                                       <div class="card-footer">   <span class="font-sz-level7" id="spanMgrSignWaitMsg"></span> </div>
								                                </div>
								                            </div>
								                        </div>
								                    </div>
                                             </div>
									    </div>
									</div>
									<!-- Signature Card End -->
									
									
                                   </div>
                                   
  <%--                                  <div class="tab-pane fade" id="test2" role="tabpanel" aria-labelledby="test2-tab">
                                   <!-- Start signpad -->
                                    <div class="row">
									    <div class="col-12">
									         <div class="card p-3">
								                    <div class="row">
								                        <div class="col-4 border-right">
								                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
										                            <a  class="nav-link font-sz-level5 text-primaryy bold " data-toggle="pill" href="#v-pills-spClient1" role="tab" onclick="selfInit('selfDiagram','selfSavedDiagram');setClientType('CLIENT');selfSignData();"><%=session.getAttribute(PsgConst.SESS_CLIENT_NAME)%></a> 
										                            <%
 										                            	if (session.getAttribute(PsgConst.SESS_SPOUSE_NAME) != null) {
 										                            %>
																	    <a  class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-spClient2" role="tab" onclick="spsInit('spsDiagram','spsSavedDiagram');setClientType('SPS');spsSignData();"><%=session.getAttribute(PsgConst.SESS_SPOUSE_NAME)%></a>
																	<%
																		}
																	%>
																	   
										                             
										                            <a id="signAdvisor" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-spAdvisor" role="tab" onclick="advInit('advDiagram','advSavedDiagram');setClientType('ADVISOR');advSignData();"><%=session.getAttribute(PsgConst.SESS_ADVISOR_NAME)%></a>
										                            <a id="signManager" class="nav-link font-sz-level5 text-primaryy bold mt-4" data-toggle="pill" href="#v-pills-spManager" role="tab" onclick="mgrInit('mgrDiagram','mgrSavedDiagram');setClientType('MANAGER');mgrSignData();" style="display:none;"><%=session.getAttribute(PsgConst.SESS_MANAGER_NAME)%></a>
								                             </div>
								                        </div>
								                        <div class="col-8" >
								                            <div class="tab-content" id="v-pills-tabContent" >
								                            <input type="hidden" id="perType" name="perType">
								                                <div class="tab-pane fade active show " id="v-pills-spClient1" role="tabpanel" aria-labelledby="v-pills-spClient1-tab">
								                                      <div class="card" style="min-height:33vh;" >
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Client(1) : <%=session.getAttribute(PsgConst.SESS_CLIENT_NAME)%></span>
								                                           
								                                           
								                                           </div>
								                                           <div class="card-body" onload="selfInit('selfDiagram','selfSavedDiagram');">
									                                           <div id="self">
	  																			 <div id="selfDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
	  																		   	<div id="buttons"><button onclick="selfClearDiagram('selfSavedDiagram');">Clear</button>
																				    <button onclick="selfSave();">Save</button>
																				   
																			  </div>
																			  <form name="selfform">
																			  <input type="hidden" id="selfSignId" name="selfSignId">
																			  <input type="hidden" id="selfLoad" name="selfLoad" value="Y">
																			  <textarea id="selfSavedDiagram" name="selfSavedDiagram" style="width:100%;height:300px;display:none">
																			{ "position": "0 0",
																			  "model": { "class": "GraphLinksModel",
																			  "nodeDataArray": [],
																			  "linkDataArray": []} }
																			  </textarea>
																			  </form>
									                                      </div>  
									                                    </div>
								                                </div>
								                                </div>
								                                <div class="tab-pane fade" id="v-pills-spClient2" role="tabpanel" aria-labelledby="v-pills-spClient2-tab">
								                                        <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Client(2) : <%=session.getAttribute(PsgConst.SESS_SPOUSE_NAME)%></span>
								                                           
								                                           </div>
								                                           <div class="card-body" >
								                                           <div id="sps">
  																			 <div id="spsDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																		   	<div id="buttons"><button onclick="spsClearDiagram('spsSavedDiagram');">Clear</button>
																			    <button onclick="spsSave();">Save</button>
																			    
																		  
																		  </div>
																		  <form name="spsform">
																		 <input type="hidden" id="spsSignId" name="spsSignId">
																		 <input type="hidden" id="spsLoad" name="spsLoad" value="Y">
																		  <textarea id="spsSavedDiagram" name="spsSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div>  
								                                      </div>
								                                      </div> 
								                                </div>
								                                
								                                <div class="tab-pane fade" id="v-pills-spAdvisor" role="tabpanel" aria-labelledby="v-pills-spAdvisor-tab">
								                                          <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Advisor : <%=session.getAttribute(PsgConst.SESS_ADVISOR_NAME)%></span>
								                                           
								                                           </div>
								                                           <div class="card-body" >
								                                           <div id="adv">
  																			 <div id="advDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																		   	<div id="buttons"><button onclick="advClearDiagram('advSavedDiagram');">Clear</button>
																			    <button onclick="advSave();">Save</button>
																			    
																		  
																		  </div>
																		  <form name="advform">
																		  <input type="hidden" id="advSignId" name="advSignId">
																		  <input type="hidden" id="advLoad" name="advLoad" value="Y">
																		  <textarea id="advSavedDiagram" name="advSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div> 
								                                      </div> 
								                                      </div> 
								                                </div>
								                              
								                                <div class="tab-pane fade" id="v-pills-spManager" role="tabpanel" aria-labelledby="v-pills-spManager-tab">
								                                          <div class="card" style="min-height:33vh;">
								                                           <div class="card-header" id="cardHeaderStyle">
								                                           <span class="font-sz-level7">Manager : <%=session.getAttribute(PsgConst.SESS_MANAGER_NAME)%></span>
								                                           
								                                           </div>
								                                           <div class="card-body">
								                                          <div id="mgr">
  																			 <div id="mgrDiagram" style="border: solid 1px black; width: 100%; height: 250px"></div>
  																		   	<div id="buttons"><button onclick="mgrClearDiagram('mgrSavedDiagram');">Clear</button>
																			    <button onclick="mgrSave();">Save</button>
																			    
																		  
																		  </div>
																		  <form name="mgrform">
																		   <input type="hidden" id="mgrSignId" name="mgrSignId">
																		   <input type="hidden" id="mgrLoad" name="mgrLoad" value="Y">
																		  <textarea id="mgrSavedDiagram" name="mgrSavedDiagram" style="width:100%;height:300px;display:none">
																		{ "position": "0 0",
																		  "model": { "class": "GraphLinksModel",
																		  "nodeDataArray": [],
																		  "linkDataArray": []} }
																		  </textarea>
																		  </form>
								                                      </div>  
								                                      </div> 
								                                      </div>
								                                </div>
								                            </div>
								                        </div>
								                    </div>
                                             </div>
									    </div>
									</div>
									<!-- End Signpad -->  
   

</div> --%>
                                   </div>
                               </div>

          
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
        	<button type="button" class="btn btn-bs3-success" onclick="closeScanToSign()">OK</button>
			<button type="button" class="btn btn-bs3-warning" onclick="cancelScanToSign()">Cancel</button>
        </div>
        
       </div>
    </div>
</div></div>
 <!--Mail popup  -->
  
  <!-- Mail Modal -->
  <div class="modal fade" id="mailModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
          <h6 class="modal-title bold"><i class="fa fa-envelope" aria-hidden="true" style=" color: #f7bd0e; font-size: 1.2rem;"></i>&nbsp;&nbsp;Mail Notification</h6>
          <button type="button" class="close" data-dismiss="modal" style="color:#fff;">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body text-center">
          <h5>Your details will send to manager for approval.</h5>
           <input type="text" class="form-control" maxlength="60" id="mgrMailId" name="mgrMailId" placeholder="Enter manager E-mail" value=<% out.print(session.getAttribute("managerEmailId")); %> />
          <div id="mailLoaderDiv" style="display:none;">
	          <div class="circle-loader">
				  <div class="checkmark draw"></div>
			  </div>
				<div id="modalMsgLbl">Sending mail...</div>
		  </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success"  onclick="sendEmail();">Send</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  <!--End Popup  -->
  
  <!-- Manager Mail send to advisor -->
  <div class="modal fade" id="mgrMailModal">
    <div class="modal-dialog">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Mail Notification</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          Your feedback is send to verified authority .
          <input type="text" id="advMailId" class="checkdb" maxlength="60" name="advMailId" placeholder="Enter E-mail">
          <div id="mailLoaderAdv" style="display:none;">
	          <div class="circle-loader">
				  <div class="checkmark draw"></div>
			  </div>
				<div id="modalMsgLblAdv">Sending mail...</div>
		  </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-success"  onclick="sendMgrEmail();">Send</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  <!--End Popup  -->
  <!-- Kyc Document Upload Modal -->
  
 <!--  <div class="modal fade show" id="uplodModal" aria-modal="true" role="dialog" style="padding-right: 15px; display: block;">
    <div class="modal-dialog">
      <div class="modal-content">
      
        Modal Header
        <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title bold"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;&nbsp;Document Uploads</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">�</button>
      </div>
        
        Modal body
        <div class="modal-body">
					         <div class="upload">
					 <div class="upload-files">
					  <header>
					   <p>
					    <i class="fa fa-cloud-upload" aria-hidden="true"></i>
					    <span class="up">up</span>
					    <span class="load">load</span>
					   </p>
					  </header>
					  <div class="body" id="drop">
					   <i class="fa fa-file-text-o pointer-none" aria-hidden="true"></i>
					   <p class="pointer-none"><b>Drag and drop</b> files here <br> or <a href="" id="triggerFile">browse</a> to begin the upload</p>
								<input type="file" multiple="multiple">
					  </div>
					  <footer>
					   <div class="divider">
					    <span><ar>FILES</ar></span>
					   </div>
					   <div class="list-files">
					      template  
					   </div>
								<button class="importar" type="button">UPDATE FILES</button>
					  </footer>
					 </div>
					</div>
        </div>
        
        Modal footer
        <div class="modal-footer p-1">
           <button type="button" class="btn btn-bs3-prime btn-sm">Upload</button>
          <button type="button" class="btn btn-bs3-prime btn-sm" data-dismiss="modal">Cancel</button>
        </div>
        
      </div>
    </div>
  </div> -->
  
  <script id="code">
  $( document ).ready(function() {
	 
	  var fnaId=$("#hdnSessFnaId").val();
	 	getAllSign(fnaId);
	  /* $('#myTab').click('shown.bs.tab', function(event){
		   event.preventDefault();
		   // var x = $(event.target).text();         // active tab
		   
		   var x=$(event.target).attr("id");
		   var tabName=$(event.target).text();
		   
		   if(tabName.includes("Signpad")){
			   
			   //selfInit('selfDiagram','selfSavedDiagram');
			   //spsInit('spsDiagram','spsSavedDiagram');
			   //advInit('advDiagram','advSavedDiagram');
			  //mgrInit('mgrDiagram','mgrSavedDiagram');
			 	
			 	var fnaId=$("#hdnSessFnaId").val();
			 	getAllSign(fnaId);
		   }
		   
	  }); */
	  
  });
  </script>
