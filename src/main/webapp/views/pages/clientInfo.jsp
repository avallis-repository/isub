<%@ page language="java" import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%ResourceBundle psgprop =ResourceBundle.getBundle("properties/psgisubmit");%>
<!DOCTYPE html>
<html lang="en">

<head>
  <link href="vendor/jquery/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="vendor/psgisubmit/css/style_toggle.css">
  <link rel="stylesheet" href="vendor/psgisubmit/css/bs_style.css">
  <link href="vendor/psgisubmit/css/stepper.css" rel="stylesheet">
  <!-- <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css"/> -->
  <link href="vendor/psgisubmit/css/theme2.css" media="all" rel="stylesheet" type="text/css"/>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  <link href="vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" >
 </head>

<body id="page-top">
 
        <!-- Container Fluid-->
       <div class="container customerDetailsFrm" id="container-wrapper" style="min-height:100vh">   
          
       <input type="hidden" value="${custId}" id="custId"/>
	   <input type="hidden" value="${dataFormId}" id="dataFormId"/>
	   <input type="hidden" value="${createdBy}" id="createdBy"/>
	   <input type="hidden" value="${createdDate}" id="createdDate"/>
		  
      <!--multisteps-form-->
      <div class="multisteps-form" style="margin-top:10px;">
        <!--progress bar-->
        <div class="row">
          <div class="col-12 col-lg-12 ml-auto mr-auto ">
            <div class="multisteps-form__progress">
              <button class="multisteps-form__progress-btn js-active" type="button" title="User Personal Info" id="btnPersonalInfo" onclick="openTabSec('Personal')"><i class="fa fa-user" style="font-size:1.4rem;color:#4caf50;" ></i><span>&nbsp;Personal&nbsp;Details</span></button>
              <button class="multisteps-form__progress-btn" type="button" title="Residential Address" id="btnResiAddress" onclick="openTabSec('Address')"><i class="fa fa-map-marker" style="font-size:1.4rem;color:#f33;" ></i><span>&nbsp;Residential&nbsp;Address</span></button>
              <!-- <button class="multisteps-form__progress-btn" type="button" title="Line Of Business" id="btnLOB" onclick="openTabSec('LOB')"><i class="fa fa-industry" style="font-size:1.4rem;color:#ff9800;"></i><span>&nbsp;LOB </span></button> -->
              <!-- <button class="multisteps-form__progress-btn hide" type="button" title="Document Upload" id="btnUploads" onclick="openTabSec('Uploads')"><i class="fa fa-upload" style="font-size:1.4rem;color:#3f9a9a;"></i><span>&nbsp;Uploads</span></button> -->
              <%-- <%if(session.getAttribute("BackToUrl")!=null) {%>
                    <a href="<%=session.getAttribute("BackToUrl")%>"><%=session.getAttribute("BackToUrl")%></a>
               <%} %> --%>
            </div>
          </div>
        </div>
        <!--form panels-->
        <div class="row">
          <div class="col-12 col-lg-12 m-auto">
            <div class="multisteps-form__form" style="height:550px;">
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn" id="PerInfoSec">
                
                <div class="multisteps-form__content">
                
                <div class="form-row mt-2">
				      <div class="col-9"><span class="bagge"><span style="color: red;"><sup>*</sup></span> <small style="color:#000000;">Indicated Fields are Mandatory.</small></span></div>
				      <div class="col-5 hide"><h4><img src="vendor/psgisubmit/img/srchClnt.png" alt="Adviser Staff Img" class="" style="width: 7%;"> <span class="badge badge-primary srvAdvstyle">Servicing Adviser : ${LOGGED_USER_INFO.LOGGED_USER_ADVSTFNAME}</span></h4></div>
				      <!-- <div class="col-3"><a target="_new" href="https://www.singpass.gov.sg/myinfo/intro" class="btn btn-sm btn-outline-info float-right" id="myInfo"> Get &nbsp;&nbsp;<img src="vendor/psgisubmit/img/myinfo.png" style="width:75px;"/>&nbsp;&nbsp;<i class="fa fa-external-link "></i></a></div> -->
				      <div class="col-3"><button id="btnMyInfo" class="btn btn-sm btn-outline-info float-right"> Get &nbsp;&nbsp;<img src="vendor/psgisubmit/img/myinfo.png" style="width:75px;"/>&nbsp;&nbsp;<i class="fa fa-external-link "></i></button></div>
                 </div>
           
				<div class="form-row mt-2" id="custTypeRadioBtn">
				<div class="col-">
				 <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text p-2"  id="">Select a Customer Type</span>
					  </div>
		         </div>
				
				
			   
			</div>
			
			<div class="col-2 ">
			     <div>
					  <input type="radio" name="custCateg" id="custCatPer" value="PERSON" onclick="custCategType('IND');" checked>
					  <label class="lblStyle" for="custCatPer">
					    <img src="vendor/psgisubmit/img/individual.png">&nbsp;Individual
					    </label>
                 </div>
			</div>
			
			<div class="col-2">
			      <div>
				  <input type="radio" class="checkdb"  name="custCateg" id="custCatComp" onclick="custCategType('COR');" value="COMPANY">
				  <label class="lblStyle" for="custCatComp" >
				  <img src="vendor/psgisubmit/img/corporate.png">&nbsp;Corporate
				  </label>
               </div>
			</div>
				<div class="col-5 ">
				     <div class="input-group" style="margin-left: 2rem;">
									 <div class="input-group-prepend">
				                        <span class="input-group-text" for="">Client Status<sup style="color: red;"><strong>*</strong></sup></span>
				                        
				                    </div>
											   <select class="form-control checkMand checkdb" name="customerStatusId" id="customerStatusId">
											   <option value="" selected>--Select--</option>
											   <c:if test="${not empty MASTER_CUSTSTS}">
												   <c:forEach items="${MASTER_CUSTSTS}" var="sts">
												   		<option value="${sts.customerStatusId}">${sts.customerStatusName}</option>
												   	</c:forEach>
											   </c:if> 
										    </select>
										    
										    <div class="invalid-feedbacks hide" id="customerStatusIdError">Select the Client Status.</div>
                                </div>
				</div>
				
				</div>
				
				
					
				<div id="individualSec" class="show">		
                  <div class="form-row  mt-3">
                    <div class="col-12 col-sm-6 ">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon1">Name<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  
					  <input type="text" class="form-control checkMand checkdb" name="custName" id="custName" maxlength="75" />				  
					  <div class="invalid-feedbacks hide" id="custNameError">Keyin Client Name.</div>
                 </div>
                </div>
                  
                   <div class="col-12 col-sm-2 mt-sm-0">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  for="custInitials">Initials<sup style="color: red;"><strong>*</strong></sup></span>
						  </div>
						<input type="text" class="form-control checkMand checkdb" id="custInitials" name="custInitials" maxlength="40" />
						  <div class="invalid-feedbacks hide" id="custInitialsError">Keyin Client Initials.</div>
						 
						</div>
                     </div> 
                  
                   <div class="col-12 col-sm-2 mt-sm-0" id="simple-date1">
                   
                   <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="" for="dob">DOB</span>
						  </div>
						 <input type="text" class="form-control checkdb" id="dob" name="dob" onchange="setClientDob(this)" maxlength="10">
						 <div class="invalid-feedbacks hide" id="dobError"></div>
						 
						</div>
                     </div> 
                     
                         
                 
						<div class="col-12 col-sm-1">
                       <div class="input-group ">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="" for="">Age</span>
						  </div>
						 <input type="text" class="form-control checkdb" id="txtFldClntAge" name="txtFldClntAge" readonly="true">
						</div>
					 </div>
				</div> 
               
                   <div class="form-row mt-3">
                   
                     <div class="col-12 col-sm-3">
						
						      <div class="input-group">
									 <div class="input-group-prepend">
				                        <span class="input-group-text" for="maritalStatus">Marital Status</span>
				                    </div>
											  <select class="form-control checkdb" id="maritalStatus" name="maritalStatus">
											    <option value="" selected>...Select...</option>
											    <%for(String data:psgprop.getString("app.maritalstatus").split("\\^")){ %>
											           <option value="<%=data%>"><%=data%></option>
											    <%}%>
											    
											 </select>
  
					 
				                    </div>
						</div>
						
			<div class="col-12 col-sm-2">
			      <div id="custTypeRadioBtn">
					  <input type="radio" class="checkdb" name="sex" id="radBtnmale" value="M">
					  <label for="radBtnmale" class="lblStyle">
					  <img src="vendor/psgisubmit/img/man.png">&nbsp;Male
					  </label>
                 </div>
			</div>
			
			<div class="col-12 col-sm-2">
			      <div id="custTypeRadioBtn">
					  <input type="radio" class="checkdb" name="sex" id="radBtnFemale" value="F">
					  <label for="radBtnFemale" class="lblStyle">
					  <img src="vendor/psgisubmit/img/woman.png">&nbsp;Female
					  </label>
			     </div>
			</div>
			             
			 <div class="col-12 col-sm-2">
                       <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input checkdb" id="notsmokerFlg" name="smokerFlg" value="N">
							  <label class="custom-control-label font-sz-level8" id="notsmokerFlg" for="notsmokerFlg" style="font-size: 0.8rem;color: #000;">Non-Smoker</label>
							  <img class="pl-2" alt="smokeImg" src="vendor/psgisubmit/img/no-smoking.png" id="smokeImg">
                       </div>
                       
                       <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input checkdb" id="smokerFlg" name="smokerFlg" value="Y">
							  <label class="custom-control-label font-sz-level8" id="smokerFlg" for="smokerFlg" style="font-size: 0.8rem;color: #000;">Smoker</label>
							  <img class="pl-2" alt="smokeImg" src="vendor/psgisubmit/img/smoking.png" id="smokeImg">
                       </div>
			</div>
								  
								   <div class="col-12 col-sm-3">
								       
								       <div class="input-group">
									 <div class="input-group-prepend">
				                        <span class="input-group-text" for="race">Race</span>
				                    </div>
											  <select class="form-control checkdb" id="race" name="race">
											    <option value="" selected>---Select---</option>
											    <option value="American">American</option>
											    <option value="Australian">Australian</option>
											    <option value="Chinese">Chinese</option>
											    <option value="Indian">Indian</option>
											    <option value="Singaporean">Singaporean</option>
											    <option value="Malay">Malay</option>
											    <option value="European">European</option>
											    <option value="Others">Others</option>
											</select>
  
					 
				                    </div>
								  
								  </div>
								  
						
                   </div>
                    
                    
                    
                    
                    
                     <div class="form-row mt-3"> 
			                    <div class="col-12 col-sm-4">
										  	  <div class="input-group">
								 
								  				  
								  <select class="form-control checkdb" id="nricType" name="nricType">
											    <option value="" >---Select---</option>
											     <option value="NRIC" selected>NRIC</option>
											    <option value="Passport">Passport</option>
											    <option value="Other">Other</option>
											    
								</select>
								<input type="text" class="form-control checkdb" id="nric" name="nric" maxlength="15" />
								<div class="invalid-feedbacks hide" id="nricError">Keyin NRIC.</div>
								 </div>
								  </div>
								  
								  <div class="col-12 col-sm-4">
										<div class="input-group">
										 <div class="input-group-prepend">
					                         <span class="input-group-text">Email<sup style="color: red;"><strong>*</strong></sup></span>
					                       </div>
					             
					                        <input type="text" class="form-control checkdb"  id="emailId" name="emailId" onblur="validateEmail(this,'emailId')"  maxlength="60">
									        <div class="invalid-feedbacks hide" id="emailIdError">keyin Email Id</div>
									        <!-- <div class="invalid-feedbacks hide" id="emailIdFldError">keyin Email Id</div> -->
									    
									    </div>
                      
                                 </div>
                                  
                                   <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                                         <div class="input-group">
                                         
                                          <div class="input-group-prepend">
					                         <span class="input-group-text">Mobile</span>
					                       </div>
										 <input class="multisteps-form__input form-control checkdb" type="text" name="resHandPhone" id="resHandPhone"  maxlength="60"/>
						                </div>
					              </div>
                    
								  
								  
					    </div>
                    
				  
				  <div class="form-row mt-3">
                       <div class="col-12 col-sm-4">
					
					 <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text" id="inpGrpTxtPading-01" id="basic-addon2">Nationality</span>
					  </div>
					  	 <select class="form-control checkdb" id="nationality" name="nationality">
									 <option value="" selected >---Select---</option>
									 <option value="SG">Singaporean</option>
									 <option value="SG-PR">Singaporean-PR</option>
									 <%for(String data:psgprop.getString("app.nationalitywithcode").split("\\^")){ %>
									 		<option value="<%=data.split(":")[0]%>"><%=data.split(":")[1]%></option>
									 <%} %>
									  
								</select>		  
				            </div>
                       </div>
                       <div class="col-12 col-sm-4">
                       <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Company</span>
						  </div>
						 <input type="text" class="form-control checkdb"  id="companyName" name="companyName" maxlength="200">
						</div>
					 </div>
                     	<div class="col-12 col-sm-4">
							  	  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon1">Height &amp; Weight</span>
					  </div>
					  <input type="text" class="form-control checkdb"  id="height" name="height" maxlength="6"/>
					  		<div class="input-group-prepend">
						    <span class="input-group-text">m</span>
						  </div>		  
					  <input type="text" class="form-control checkdb"  id="weight" name="weight" maxlength="6">
					  <div class="input-group-prepend">
						    <span class="input-group-text"  style="border-radius: 0px 5px 5px 0px;">Kg</span>
						  </div>
			           </div>
					  </div>
                       
            </div>
				  
                <div class="form-row">
                      <div class="col-12 col-sm-4  mt-sm-0">
                      <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text" >Estd. Annual Income ($)</span>
						  </div>
						 <input type="text" class="form-control numberFldClass checkdb" id="income" name="income" autocomplete="off" maxlength="24">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  style="border-radius: 0px 5px 5px 0px;">.00</span>
						  </div>
                        </div>
                    </div>
                     <div class="col-12 col-sm-4 mt-sm-0">
                                  <div class="input-group mb-3">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Occupation</span>
						  </div>
						 <input type="text" class="form-control checkdb" id="occpnDesc" name="occpnDesc" autocomplete="off" maxlength="200">
                        </div>
					   
                    </div>
                     <div class="col-12 col-sm-4">
                  
				     <div class="input-group">
									 <div class="input-group-prepend">
				                        <span class="input-group-text" >Type of Prospecting</span>
				                    </div>
											  <select class="form-control checkdb" name="typesOfProspect" id="typesOfProspect">
											    <option value="" selected >---Select---</option>
											    <option value="Branch Walk-in">Branch Walk-in</option>
											    <option value="CompareFirst">CompareFirst</option>
											    <option value="Cold-calling">Cold-calling</option>
											    <option value="Door-to-door knocking">Door-to-door knocking</option>
											    <option value="Introducer">Introducer</option>
											    <option value="Other forms of public prospecting">Other forms of public prospecting</option>
											    <option value="Prospecting">Prospecting</option>
											    <option value="Roadshows">Roadshows</option>
											    <option value="Referral">Referral</option>
											    <option value="Web-sites">Web-sites</option>
										    </select>
                                </div>
				</div>
                    
                  </div>
                  
                   <div class="form-row ">
                  
				<div class="col-12 col-sm-4 mt-4 mt-sm-0">
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text" >Nature Of Business</span>
					  </div>
					    
					  	<select class="form-control checkdb" id="businessNatr" name="businessNatr">
											    <option value="" selected>...Select...</option>
											    <%for (String value:psgprop.getString("app.natureofbusiness").split("\\^")){%>
											      <option value="<%=value%>"><%=value%></option>
											   <%}%>
						</select>		  
				        </div>
                      
                    </div>
                    
                    <div class="col-12 col-sm-4 mt-sm-0 hide" id="busiNatOthSec">
                           <div class="input-group mb-3">
				        <div class="input-group-prepend">
				            <span class="input-group-text">Other Detls.<sup style="color: red;"><strong>*</strong></sup></span>
				       </div>
				          <input type="text" class="form-control checkMand checkdb" id="businessNatrDets" name="businessNatrDets" maxlength="75">
				         <div class="invalid-feedbacks hide" id="businessNatrDetsError">keyin Other Details </div>
                         </div>
                    </div>
				</div>
               </div>
               <!--Individual section end  -->
               <!--Start corporate section  -->
               	<div id="corporateSec" class="hide">		
                  <div class="form-row  mt-3">
                    <div class="col-12 col-sm-6 ">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon1">Name<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  
					  <input type="text" class="form-control checkMand checkdb"  name="custNameCom" id="custNameCom"  maxlength="75" />				  
					  <div class="invalid-feedbacks hide" id="custNameComError">Keyin Client Name.</div>
                 </div>
                </div>
                  
                   <div class="col-12 col-sm-2 mt-sm-0">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text" for="custInitials">Initials<sup style="color: red;"><strong>*</strong></sup></span>
						  </div>
						<input type="text" class="form-control checkMand checkdb" id="custInitialsCom" name="custInitialsCom" maxlength="40" />
						  <div class="invalid-feedbacks hide" id="custInitialsComError">Keyin Client Initials.</div>
						 
						</div>
                     </div> 
                  
                   <div class="col-12 col-sm-4 mt-sm-0" id="roc-date">
                   
                   <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="" for="rocDate">ROC Date</span>
						  </div>
						 <input type="text" class="form-control checkdb" id="rocDate" name="rocDate" maxlength="10">
						 <div class="invalid-feedbacks hide" id="dobError"></div>
						 
						</div>
                     </div> 
                     
                         
                 
						
				</div> 
				     <div class="form-row mt-3"> 
			                 <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                                         <div class="input-group">
                                         
                                          <div class="input-group-prepend">
					                         <span class="input-group-text">ROC Code</span>
					                       </div>
										 <input class="multisteps-form__input form-control checkdb" type="text" 
										 name="rocNo" id="rocNo"  maxlength="60"
										 />
						                </div>
						                
						                
					 </div>
								  
								  <div class="col-12 col-sm-4">
										<div class="input-group">
										 <div class="input-group-prepend">
					                         <span class="input-group-text">Email</span>
					                       </div>
					             
					                        <input type="text" class="form-control checkdb"  id="emailIdCom" name="emailIdCom" onblur="validateEmail(this,'emailIdCom')"  maxlength="60">
									        <div class="invalid-feedbacks hide" id="emailIdComError"></div>
									    
									    </div>
                      
                                 </div>
                                  
                                   <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                                         <div class="input-group">
                                         
                                          <div class="input-group-prepend">
					                         <span class="input-group-text">Mobile</span>
					                       </div>
										 <input class="multisteps-form__input form-control checkdb" type="text" 
										 name="offPh" id="offPh"  maxlength="60"
										 />
						                </div>
						                
						                
					              </div>
                    
								  
								  
					    </div>
			<div class="form-row mt-3">	    
				<div class="col-12 col-sm-4 mt-4 mt-sm-0">
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text" >Nature Of Business</span>
					  </div>
					    
					  	<select class="form-control checkdb" id="businessNatrCom" name="businessNatrCom">
											    <option value="" selected>...Select...</option>
											    <%for (String value:psgprop.getString("app.natureofbusiness").split("\\^")){%>
											      <option value="<%=value%>"><%=value%></option>
											   <%}%>
						</select>		  
				        </div>
                      
                    </div>
                    
                    <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                                         <div class="input-group">
                                         
                                          <div class="input-group-prepend">
					                         <span class="input-group-text">Website</span>
					                       </div>
										 <input class="multisteps-form__input form-control checkdb" type="text" 
										 name="website" id="website"  maxlength="60"
										 />
						                </div>
						                
						                
					 </div>
             </div>
				</div>
				<!--End corporate sec  -->
                  <div class="button-row d-flex mt-3">
                    <button class="btn btn-sm btn-bs3-prime  ml-auto js-btn-next  SteperBtnStyle" type="button" title="Next" onclick="openTabSec('Address')">Next&nbsp;<i class="fa fa-arrow-circle-right " ></i></button>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn" id="AddrSec">
                
                <div class="multisteps-form__content">
				<div class="form-row mt-1">
				  <div class="col-2 col-sm-8"></div>
				 <div class="col-2 col-sm-4">
				 </div> 
                    
                    
				
				</div>
                  <div class="form-row mt-2">
				  
				  
                    <div class="col">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text" id="inpGrpTxtPading-01" id="basic-addon3">Address1<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  <input class="multisteps-form__input form-control checkMand checkdb" type="text" name="resAddr1" id="resAddr1" maxlength="60"/>
				       <div class="invalid-feedbacks hide" id="resAddr1Error">Keyin Address1 Field.</div>
				      
				      </div>
				  
                    </div>
                  </div>
                  <div class="form-row mt-4">
                    <div class="col">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon3">Address2</span>
					  </div>
					  <input class="multisteps-form__input form-control checkdb" type="text" name="resAddr2" id="resAddr2"  maxlength="60"/>
					  
				</div>
				
                      
                    </div>
                  </div>
                  
                  <div class="form-row mt-4">
                    <div class="col">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon3">Address3</span>
					  </div>
					  <input class="multisteps-form__input form-control checkdb" type="text" name="resAddr3" id="resAddr3"  maxlength="60"/>
				
				
				</div>
				
                      
                    </div>
                  </div>
                  <div class="form-row mt-4">
				 <div class="col-10 col-sm-3">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon4">PostalCode<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					 <input class="multisteps-form__input form-control checkMand checkdb" type="text"  name="resPostalCode" 
					 id="resPostalCode" maxlength="60"/>
			      <div class="invalid-feedbacks hide" id="resPostalCodeError">Keyin  PostalCode Field.</div>
			      
			       </div>
                 </div>
                    
                    <div class="col-2 col-sm-2">
					 <!-- <a target="_new" href="https://www.sglocate.com/documentations.aspx" class="btn btn-sm btn-outline-info" style="height: 5.5vh;"> Get &nbsp;&nbsp;<img src="vendor/psgisubmit/img/sglocate.png" style="width:55px;"/>&nbsp;&nbsp;<i class="fa fa-external-link "></i></a> -->
					 <button id="btnSgLocate" class="btn btn-sm btn-outline-info" style="height: 5.5vh;"> Get &nbsp;&nbsp;<img src="vendor/psgisubmit/img/sglocate.png" style="width:55px;"/>&nbsp;&nbsp;<i class="fa fa-external-link "></i></button>
				   </div>
				   
				    <div class="col-12 col-sm-3 mt-4 mt-sm-0">
                        <div class="input-group mb-3">
									  <div class="input-group-prepend">
									    <span class="input-group-text" >Country<sup style="color: red;"><strong>*</strong></sup></span>
									  </div>
									  <select class="form-control checkMand checkdb" id="resCountry" name="resCountry" onchange="filtrCuntry(this)">
									   <option value="" selected>---Select---</option>
									   
									    <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
									    	<option value="<%=country.toUpperCase()%>"><%=country%></option>
									     <%}%>
									    
									  </select>
									  
									  <div class="invalid-feedbacks hide" id="resCountryError">Keyin  Country Field.</div>
							</div>      
                      </div>
                      
                       <div class="col-12 col-sm-3 mt-4 mt-sm-0">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon3">City</span>
					  </div>
					  <input class="multisteps-form__input form-control checkdb" type="text"  value="" name="resCity" id="resCity" maxlength="60"/>
					</div>
				 </div>
				 
				 <div class="col-12 col-sm-3 mt-4 mt-sm-0">
					            <div class="input-group">
								  <div class="input-group-prepend">
									<span class="input-group-text"  id="basic-addon3">State</span>
								  </div>
								  <input class="multisteps-form__input form-control checkdb" type="text"  value="" name="resState" id="resState" maxlength="60"/>
							   </div>
				         </div>
               
					
					 
					 </div>
                  
                 
                  
				
					 
                  <div class="button-row d-flex mt-4">
                    <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('Personal')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
                    <!-- <button class="btn btn-sm btn-bs3-prime ml-auto js-btn-next SteperBtnStyle" type="button" title="Next" onclick="openTabSec('LOB')">Next&nbsp;<i class="fa fa-arrow-circle-right "></i></button> -->
                    <button class="btn btn-sm btn-bs3-prime ml-auto SteperBtnStyle" onclick="validateClientMand()"   type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Save Client Dets & Proceed</button>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-3 rounded bg-white hide" data-animation="scaleIn" id="LOBSec">
                <h3 class="multisteps-form__title">&nbsp;</h3>
                <div class="multisteps-form__content">
				
				<div class="col-12">
						<p class="mb-4 pb-2" style="color: #1d655cf7;">Select Any Line of Business:</p>
					</div>
					
					 <div class="col-12">
						<div class="invalid-feedbacks hide" id="LOBError">Select any one of the Line of Business below </div>
					</div>
					
			<div class="col-12 pb-3 LobDisableSec">
						<input class="checkbox-budget checkdb" type="radio" name="typeOfPdt" id="typeofpdt_life" value="LIFE" onclick="chkLob(this)">
						<label class="for-checkbox-budget" for="typeofpdt_life">
							<img src="vendor/psgisubmit/img/life.jpg" class="rounded-circle" style="width:150px"><br>Insurance
						</label><!--
						--><input class="checkbox-budget checkdb" type="radio" name="typeOfPdt" id="typeofpdt_inv" value="INVESTMENT" onclick="chkLob(this)">
						<label class="or-checkbox-budget" for="typeofpdt_inv">
							<img src="vendor/psgisubmit/img/investment7.jpg" class="rounded-circle" style="width:150px"><br>Investments
						</label>
						
						<input class="checkbox-budget checkdb" type="radio" name="typeOfPdt" id="typeofpdt_all" value="ALL" onclick="chkLob(this)">
						<label class="or-checkbox-budget" for="typeofpdt_all">
							<img src="vendor/psgisubmit/img/design.jpg" class="rounded-circle" style="width:150px"><br>Both
						</label>
			  </div>
				
                  
                  <div class="row">
                    <div class="button-row d-flex mt-4 col-12">
                      <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('Address')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
                      <button class="btn btn-sm btn-bs3-prime ml-auto SteperBtnStyle" onclick="validateClientMand()"   type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Save Client Dets & Proceed</button>
                     
                    </div>
                  </div>
                </div>
              </div>
              
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white hide" data-animation="scaleIn" id="DocUplodSec">
                <h3 class="multisteps-form__title">&nbsp;</h3>
                <div class="multisteps-form__content">
				
				<div class="upload">
					 <div class="upload-files">
					  <header>
					   <p>
					    <i class="fa fa-cloud-upload" aria-hidden="true"></i>
					    <span class="up">up</span>
					    <span class="load">load</span>
					   </p>
					  </header>
					  <div class="body" id="drop">
					   <i class="fa fa-file-text-o pointer-none" aria-hidden="true"></i>
					   <p class="pointer-none"><b>Drag and drop</b> files here <br /> or <a href="" id="triggerFile">browse</a> to begin the upload</p>
								<input type="file" class="checkdb" multiple="multiple" />
					  </div>
					  <footer>
					   <div class="divider">
					    <span><AR>FILES</AR></span>
					   </div>
					   <div class="list-files">
					    <!--   template   -->
					   </div>
								<button class="importar" type="button">UPDATE FILES</button>
					  </footer>
					 </div>
					</div>
				  <div class="button-row d-flex mt-4">
                    <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('LOB')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
					 <button class="btn btn-sm btn-bs3-prime ml-auto SteperBtnStyle"  data-toggle="modal" data-target="#finishedModal"  type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Save Client Dets & Proceed</button>
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

        <!--   </div> -->
         
          <!--Row-->
     <div class="modal fade" id="finishedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout" data-backdrop="static" data-keyboard="false" tabindex="-1" 
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22" >
                  <h5 class="modal-title" id="finishedModalLabelClnt"> Client Details</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body text-center">
				
				 
                  <div class="circle-loader">
					  <div class="checkmark draw"></div>
					   
					</div>
					
					<h4><div id="modalMsgLbl">Save in progress...</div></h4>


                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Close</button>
                  <a  class="btn btn-sm  btn-bs3-prime disabled" id="generateKYCBtn">Generate eKYC</a>
                </div>
              </div>
            </div>
          </div>
          
           <!-- Client Search Modal -->
            <!-- Modal Scrollable -->
          <div class="modal fade" id="srchClntForNricModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
            aria-labelledby="srchClntForNricModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document" style=" height: 90%;" >
              <div class="modal-content" style=" height: 90%;">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="srchClntForNricModalTitle"><img src="vendor/psgisubmit/img/srch.png">&nbsp;&nbsp;Search Client</h6>
                  <button type="button" class="btnNricMdlClse close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                
                <div class="col-lg-12">
              <div class="card mb-4" >
              
              <div class="card-body" id="secOverlay">
              
               <div class="row">
                   <div class="co-12">
                   <small class="pl-3 noteInfoMsg" title="Note Information"><img src="vendor/psgisubmit/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;<span id="noteInfo">
                   The Same NRIC is already registered with the following client, Click on the name/nric to continue</span></small>
                   </div>
              </div>
               
               <div class="row hide" id="srchClntTblForNricSec">
                   <div class="col-12">
	                
	                    
	                     <div class="table-responsive p-2" >
			                  <table class="table align-items-center table-flush table-striped table-hover" id="tblNricListRec" style="width: 100%;">
			                    <thead class="thead-light">
			                      <tr>					    
			                        <th><div style="width:200px">Name</div></th>
			                        <th><div style="width:90px">Initials</div></th>
			                        <th><div style="width:90px">NRIC</div></th>
			                       	<th><div style="width:90px">DOB</div></th>
			                        <th><div style="width:90px">Contact</div></th>
			                        <th><div style="width:90px">Cust. Type</div></th>
			                        <!-- <th><div style="width:90px">CustID</div></th> -->
			                      </tr>
			                    </thead>
			                   
			                    <tbody>
			                      
			                    </tbody>
			                  </table>
                        </div>
                   
                   
                   </div>
              </div>
              
              
                
               
                </div>
              </div>
            </div>
                  
                  
                </div>
                <div class="modal-footer float-left p-1">
				
                  <button type="button" class="btn btn-sm pl-3 pr-3 btn-bs3-prime btnNricMdlClse" data-dismiss="modal" id="">OK</button>
                  
                </div>
              </div>
            </div>
          </div>
     <!-- client Search Modal End -->
          
          <div class="modal fade" id="addressModal" tabindex="-1" aria-labelledby="chooseAddressModalTitle" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="chooseAddressModalTitle">Choose Address</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
					<div class="modal-body">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>Block</th>
									<th>Building Name</th>
									<th>Street Name</th>
									<th>Postal Code</th>
								</tr>
							</thead>
							<tbody id="tBodyAddress">
							
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<!-- <button id="okayBtn" type="button" class="btn btn-primary" data-dismiss="modal">Okay</button>
						<button id="yesBtn" type="button" class="btn btn-danger" data-dismiss="modal">Yes</button>
						<button id="noBtn" type="button" class="btn btn-secondary" data-dismiss="modal">No</button> -->
					</div>
				</div>
			</div>
		</div>
          
		  <!-- </div> -->
        
   
<!--     <script src="js/piexif.js" type="text/javascript"></script>
    <script src="js/sortable.js" type="text/javascript"></script>
    <script src="js/fileinput.js" type="text/javascript"></script>
     <script src="js/theme.js" type="text/javascript"></script>
    <script src="js/theme-explorer.js" type="text/javascript"></script>
  -->  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="vendor/psgisubmit/js/client_info.js"></script>
    <script src="vendor/psgisubmit/js/Common Script.js"></script>
    
    <script>
     
 // Bootstrap Date Picker
	 $('#simple-date1 .input-group.date').datepicker({
	 	//dateFormat: 'dd/mm/yyyy',
	 	format: 'dd/mm/yyyy',
	 	todayBtn: 'linked',
	 	language: "it",
	 	todayHighlight: true,
	 	autoclose: true,         
	 });
	 $('#roc-date .input-group.date').datepicker({
		 	//dateFormat: 'dd/mm/yyyy',
		 	format: 'dd/mm/yyyy',
		 	todayBtn: 'linked',
		 	language: "it",
		 	todayHighlight: true,
		 	autoclose: true,         
		 });

       // var customer_details = ${CUSTOMER_DETAILS};

       

     </script>
  
  
</body>

</html>