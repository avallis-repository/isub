<%@ page language="java" import="com.afas.isubmit.util.PsgConst" %>
<html>
<head>

</head>
<body >

<div id="viewFnaDets">
        
 		<div id="wizard">
		     <h2><i class="fa fa-user icon-color">&nbsp;Personal Details</i></h2>
                <section class="readonly">
                  <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
			         <div class="card-header" id="cardHeaderStyle">
			         <i class="fa fa-user"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Personal Details</span></div>
			         <hr>
                    	<jsp:include page="/views/pages/personalDetails.jsp"></jsp:include>
                    </div>
                </section>
                
                <h2><i class="fa fa-chart-area icon-color">&nbsp;Tax Residency</i></h2>
                <section class="readonly">
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-chart-area"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Tax Residency</span></div>
				         <hr>
                    <jsp:include page="/views/pages/taxResiContent.jsp"></jsp:include>
                    </div>
                </section>
                
                <h2><i class="fa fa-plus icon-color">&nbsp;Fin.Wellness Review</i></h2>
                <section class="readonly">
                    <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-plus"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Fin.Wellness Review</span></div>
				         <hr>
	                    <jsp:include page="/views/pages/finanWellContent.jsp"></jsp:include>
                    </div>
				</section>
                
               
                <h2><i class="fa fa-columns icon-color">&nbsp;Product Recomm </i></h2>
                <section class="readonly">
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-columns"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Product Recommendations</span></div>
				         <hr>
                    <jsp:include page="/views/pages/prodRecomContent.jsp"></jsp:include>
                    </div>
                </section>
                
                <h2><i class="fa fa-columns icon-color">&nbsp;Product Switching </i></h2>
                <section class="readonly">
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-columns"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Product Switching</span></div>
				         <hr>
                    	<jsp:include page="/views/pages/prodSwitchContent.jsp"></jsp:include>
					</div>
                </section>
                
                <h2><i class="fa fa-chart-area icon-color">&nbsp;Adv & Basis Recomm </i></h2>
                <section class="readonly">
                	<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-chart-area"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Adv & Basis Recomm</span></div>
				         <hr>
                    <jsp:include page="/views/pages/advBasisContent.jsp"></jsp:include>
                    </div>
				</section>

                 
                
                <h2><i class="fa fa-user icon-color">&nbsp;Client Signature</i></h2>
                <section>
                   <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-user"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Client Signature</span></div>
				         <hr>
                    <jsp:include page="/views/pages/clientSignContent.jsp"></jsp:include>
                    </div>
                </section>
                
                <h2><i class="fa fa-users icon-color">&nbsp;Advisor Declr.</i></h2>
                <section class="readonly">
                    <div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
				         <div class="card-header" id="cardHeaderStyle">
				         <i class="fa fa-users"  aria-hidden="true"></i>&nbsp;<span class="font-sz-level3">Advisor Declaration</span></div>
				         <hr>
                    <jsp:include page="/views/pages/advisorDeclrContent.jsp"></jsp:include>
					</div>
                </section>
        </div>
</div>

</body>
<script src="vendor/psgisubmit/js/view_fna_dets.js"></script>
</html>