<!-- Archive List left side Modal -->
	<div class="modal left fade" id="archveListModal" tabindex=""
		role="dialog" aria-labelledby="archveListModal" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
                 <div class="modal-body" style="padding: 10px 0px 0px 35px;">
					<div class="row">
						<p>
							<small><strong>You have 5 Archive List</strong></small>
						</p>
					</div>

					<div class="row">
						<div class="list-group" style="width: 93%;">

							<a href="#"
								class="list-group-item list-group-item-action flex-column align-items-start ">
								<div class="d-flex w-100 justify-content-between">
									<h6 class="mb-1">Archive 5</h6>
									<small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i
										class="fa fa-file-text" aria-hidden="true"></i></small>

								</div>
								<p class="mb-1">
									<small><strong>Created Date: </strong>11-07-2020</small>
								</p>
								<p class="mb-1">
									<small><strong>Created By: </strong>Salim M Amin</small>
								</p> <small><strong>Archive Status:</strong> Open</small>

								<p class="mb-1">
									<small><strong>DSF Status: </strong>Finalised</small>
								</p>
								<p class="mb-1">
									<small><strong>Manager Status: </strong>Approved </small>
								</p>
								<p class="mb-1">
									<small><strong>Compliance Status: </strong>Pending </small>
								</p>





							</a> <a href="#"
								class="list-group-item list-group-item-action flex-column align-items-start">
								<div class="d-flex w-100 justify-content-between">
									<h6 class="mb-1">Archive 4</h6>
									<small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i
										class="fa fa-file-text" aria-hidden="true"></i></small>
								</div>
								<p class="mb-1">
									<small><strong>Created Date: </strong>11-07-2020</small>
								</p>
								<p class="mb-1">
									<small><strong>Created By: </strong>Salim M Amin</small>
								</p> <small><strong>Archive Status:</strong> Open</small>

								<p class="mb-1">
									<small><strong>DSF Status: </strong>Finalised</small>
								</p>
								<p class="mb-1">
									<small><strong>Manager Status: </strong>Approved </small>
								</p>
								<p class="mb-1">
									<small><strong>Compliance Status: </strong>Pending </small>
								</p>
							</a> <a href="#"
								class="list-group-item list-group-item-action flex-column align-items-start">
								<div class="d-flex w-100 justify-content-between">
									<h6 class="mb-1">Archive 3</h6>
									<small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i
										class="fa fa-file-text" aria-hidden="true"></i></small>


								</div>
								<p class="mb-1">
									<small><strong>Created Date: </strong>11-07-2020</small>
								</p>
								<p class="mb-1">
									<small><strong>Created By: </strong>Salim M Amin</small>
								</p> <small><strong>Archive Status:</strong> Open</small>

								<p class="mb-1">
									<small><strong>DSF Status: </strong>Finalised</small>
								</p>
								<p class="mb-1">
									<small><strong>Manager Status: </strong>Approved </small>
								</p>
								<p class="mb-1">
									<small><strong>Compliance Status: </strong>Pending </small>
								</p>
							</a> <a href="#"
								class="list-group-item list-group-item-action flex-column align-items-start">
								<div class="d-flex w-100 justify-content-between">
									<h6 class="mb-1">Archive 2</h6>
									<small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i
										class="fa fa-file-text" aria-hidden="true"></i></small>
								</div>
								<p class="mb-1">
									<small><strong>Created Date: </strong>11-07-2020</small>
								</p>
								<p class="mb-1">
									<small><strong>Created By: </strong>Salim M Amin</small>
								</p> <small><strong>Archive Status:</strong> Open</small>

								<p class="mb-1">
									<small><strong>DSF Status: </strong>Finalised</small>
								</p>
								<p class="mb-1">
									<small><strong>Manager Status: </strong>Approved </small>
								</p>
								<p class="mb-1">
									<small><strong>Compliance Status: </strong>Pending </small>
								</p>
							</a> <a href="#"
								class="list-group-item list-group-item-action flex-column align-items-start">
								<div class="d-flex w-100 justify-content-between">
									<h6 class="mb-1">Archive 1</h6>
									<small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i
										class="fa fa-file-text" aria-hidden="true"></i></small>
								</div>
								<p class="mb-1">
									<small><strong>Created Date: </strong>11-07-2020</small>
								</p>
								<p class="mb-1">
									<small><strong>Created By: </strong>Salim M Amin</small>
								</p> <small><strong>Archive Status:</strong> Open</small>

								<p class="mb-1">
									<small><strong>DSF Status: </strong>Finalised</small>
								</p>
								<p class="mb-1">
									<small><strong>Manager Status: </strong>Approved </small>
								</p>
								<p class="mb-1">
									<small><strong>Compliance Status: </strong>Pending </small>
								</p>
							</a>

						</div>
</div>
              </div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<!--Archive List Modal End...  -->
