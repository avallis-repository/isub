<%@ page language="java" import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%ResourceBundle psgprop =ResourceBundle.getBundle("properties/psgisubmit");%>
<!DOCTYPE html>
<html lang="en">

<head>
  <link href="vendor/jquery/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="vendor/psgisubmit/css/style_toggle.css">
  <link rel="stylesheet" href="vendor/psgisubmit/css/bs_style.css">
  <link href="vendor/psgisubmit/css/stepper.css" rel="stylesheet">
  <!-- <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css"/> -->
  <link href="vendor/psgisubmit/css/theme2.css" media="all" rel="stylesheet" type="text/css"/>
   
  <link href="vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" >
   
  <style>
   /* #tblMasterTenantDis tr:hover{
     cursor: pointer;
     color: #fff;
	 background-color: #337ab7;
	 border-color: #2e6da4;
  } */
  </style>
 </head>

<body id="page-top">
 
        <!-- Container Fluid-->
       <div class="container " id="container-wrapper" style="min-height:100vh">   
		  
      <!--multisteps-form-->
      <div class="multisteps-form" style="margin-top:10px;">
        <!--progress bar-->
        <div class="row">
          <div class="col-12 col-lg-12 ml-auto mr-auto ">
            <div class="multisteps-form__progress">
              <span style="margin: auto;">&nbsp;Master&nbsp;Tenant&nbsp;(Distributor)</span>
               
            </div>
          </div>
        </div>
        <!--form panels-->
        <div class="row">
          <div class="col-12 col-lg-12 m-auto">
            <div class="multisteps-form__form" style="height:550px;">
              <!--single form panel-->
              <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn" id="MasterTenantListSec">
                <div class="card-header" id="cardHeaderStyle-22">
                 <div style="margin-left: 85%;"><!--  <button class="btn btn-sm btn-bs3-prime  ml-auto js-btn-next  SteperBtnStyle" type="button" title="Create Master Tenant" onclick="openTabSec('Next')">Create&nbsp;Master&nbsp;Tenant&nbsp;</button> -->
				    <span class="badge badge-pill badge-success py-2 btnshake  show" title="Create Master Tenant"  onclick="openTabSec('Next')"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;<span class="font-sz-level9">Create&nbsp;Master&nbsp;Tenant</span></span>
				 </div>
               </div>
                <div class="multisteps-form__content">
               
				<!--  <input class="form-control rounded bright" placeholder="Search Tenant..." type="search" id="srchTenant"> -->
                  <table class="table align-items-center table-flush table-striped table-hover" id="tblMasterTenantDis" style="width: 100%;">
			                    <thead class="thead-light">
			                      <tr>					    
			                        <th><div style="width:200px">Tenant Name</div></th>
			                        <th><div style="width:90px">Mnemonic</div></th>
			                        <th><div style="width:90px">Contact Ref</div></th>
			                       	<th><div style="width:90px">Registration No</div></th>
			                        <th><div style="width:90px">Email ID</div></th>
			                       <!--  <th><div style="width:90px">Parent Name</div></th> -->
			                        <th><div style="width:90px">Action</div></th>
			                        
			                      </tr>
			                    </thead>
			                   
			                    <tbody>
			                      
			                    </tbody>
			                  </table>
                  <div class="form-row mt-4">
					 
					 </div>
					 
                  <!-- <div class="button-row d-flex mt-4">
                    <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('Prev')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
                    
                  </div> -->
                </div>
              </div>
              
              <div class="masterTenantDistFrm" >   
              <div class="multisteps-form__panel shadow p-4 rounded bg-white " data-animation="scaleIn" id="MasterTenantSec">
                
                <div class="multisteps-form__content">
                 
				<div id="individualSec" class="show">		
                  <div class="form-row  mt-3">
                    <div class="col-12 col-sm-4">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Tenant Name <sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  
					  <input type="text" class="form-control checkMand checkdb" name="tenantName" id="tenantName" maxlength="75" />				  
					  <div class="invalid-feedbacks hide" id="tenantNameError">Keyin Tenant Name.</div>
					  
					  	<input type="hidden" value='${tenantJson }'  id="tenantJsonData" />	
					  	<!-- <input type="hidden" name="createdDate" id="createdDate" value="" />	
					  	<input type="hidden" name="createdBy" id="createdBy" value="" /> -->	
					  
                 </div>
                </div>
                  
                   <div class="col-12 col-sm-4 ">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  for="">Tenant mnemonic<sup style="color: red;"><strong>*</strong></sup></span>
						  </div>
						<input type="text" class="form-control checkMand checkdb" id="mnemonic" name="mnemonic" maxlength="70" />
						  <div class="invalid-feedbacks hide" id="mnemonicError">Keyin Tenant mnemonic.</div>
						 
						</div>
                     </div> 
                  
                   <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                   
                   <div class="input-group" id="testrad">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="" for="">Login appear<sup style="color: red;"><strong>*</strong></sup></span>
						    &nbsp;&nbsp;&nbsp;&nbsp;
						  </div>
						  <div class="custom-control custom-radio" >
							  <input type="radio" class="custom-control-input checkMand madcls" id="loginAppearY" name="loginAppear" value="Y" style="visibility: visible;">
							  <label class="custom-control-label font-sz-level8"  for="loginAppearY" style="font-size: 0.8rem;color: #000;">Yes</label>
							   
                       </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                       <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input checkMand " id="loginAppearN" name="loginAppear" value="N" style="visibility: visible;">
							  <label class="custom-control-label font-sz-level8"  for="loginAppearN" style="font-size: 0.8rem;color: #000;">No</label>
							   
                       </div>
						  <div class="invalid-feedbacks hide" id="loginAppearError">Keyin Login Appear.</div>
						</div>
                     </div> 
                     
				</div> 
				
				<div class="form-row  mt-3">
                    <div class="col-12 col-sm-4">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Contact Ref <sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  <input type="text" class="form-control checkMand" name="contactRef" id="contactRef" maxlength="75" />				  
					  <div class="invalid-feedbacks hide" id="contactRefError">Keyin Contact Ref .</div>
                 </div>
                </div>
                  
                   <div class="col-12 col-sm-4 ">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  for="">Contact Email Id <sup style="color: red;"><strong>*</strong></sup></span>
						  </div>
						  
						    <input type="text" class="form-control checkMand checkdb"  id="emailId" name="emailId" onblur="validateEmail(this,'emailId')"  maxlength="60">
						   <div class="invalid-feedbacks hide" id="emailIdError">keyin Email Id</div>
						 
						</div>
                     </div> 
                  
                   <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                   
                    <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Registration no  <sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="registrationNo" id="registrationNo" maxlength="75" />				  
					  <div class="invalid-feedbacks hide" id="registrationNoError">Keyin Registration no.</div>
                 </div>
                     </div> 
                     
				</div>
               <!-- <div class="card-body">
               <fieldset>
                <legend>Address:</legend> -->
                
               <div class="form-row  mt-3">
                    <div class="col-12 ">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Address1<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="address1" id="address1" maxlength="75" />				  
					  <div class="invalid-feedbacks hide" id="address1Error">Keyin Address 1.</div>
                 </div>
                </div>
                     
				</div>
				
				<div class="form-row  mt-3">
                  
                   <div class="col-12">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  for="">Address2</span>
						  </div>
						  
						    <input type="text" class="form-control checkdb"  id="address2" name="address2"   maxlength="75">
						 
						</div>
                     </div> 
                     
				</div>
				
				<div class="form-row  mt-3">
                  
                   <div class="col-12">
                   
                    <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Address3</span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="address3" id="address3" maxlength="75" />				  
					   
                 </div>
                     </div> 
                     
				</div>
				
				<!-- </fieldset>
				</div> -->
				<div class="form-row mt-4">
				 <div class="col-10 col-sm-3">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon4">PostalCode<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					 <input class="multisteps-form__input form-control checkMand checkdb" type="text"  name="pinCode"  id="pinCode"   maxlength="60"/>
			      <div class="invalid-feedbacks hide" id="pinCodeError">Keyin  PostalCode Field.</div>
			      
			       </div>
                 </div>
                    
                   <!--  <div class="col-2 col-sm-2">
					 <button id="btnSgLocate" class="btn btn-sm btn-outline-info" style="height: 5.5vh;"> Get &nbsp;&nbsp;<img src="vendor/psgisubmit/img/sglocate.png" style="width:55px;"/>&nbsp;&nbsp;<i class="fa fa-external-link "></i></button>
				   </div> -->
				   
				    <div class="col-12 col-sm-3 mt-4 mt-sm-0">
                        <div class="input-group mb-3">
									  <div class="input-group-prepend">
									    <span class="input-group-text" >Country<sup style="color: red;"><strong>*</strong></sup></span>
									  </div>
									  <select class="form-control checkMand checkdb" id="country" name="country" onchange="filtrCuntry(this)">
									   <option value="" selected>---Select---</option>
									   
									    <% for(String country:psgprop.getString("app.country").split("\\^")){ %>
									    	<option value="<%=country.toUpperCase()%>"><%=country%></option>
									     <%}%>
									    
									  </select>
									  
									  <div class="invalid-feedbacks hide" id="countryError">Keyin  Country Field.</div>
							</div>      
                      </div>
                      
                       <div class="col-12 col-sm-3 mt-4 mt-sm-0">
					
					<div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="basic-addon3">City</span>
					  </div>
					  <input class="multisteps-form__input form-control checkdb" type="text"  value="" name="city" id="city" maxlength="60"/>
					</div>
				 </div>
				 
				 <div class="col-12 col-sm-3 mt-4 mt-sm-0">
					            <div class="input-group">
								  <div class="input-group-prepend">
									<span class="input-group-text"  id="basic-addon3">State</span>
								  </div>
								  <input class="multisteps-form__input form-control checkdb" type="text"  value="" name="state" id="state" maxlength="60"/>
							   </div>
				         </div>
					 
					 </div>
					 
					 <div class="form-row  mt-3">
                    <div class="col-12 col-sm-4">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Telephone Office</span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="telephoneOff" id="telephoneOff"  onkeypress="return isNumber(event)" maxlength="75" />				  
					   
                 </div>
                </div>
                  
                   <div class="col-12 col-sm-4 ">
						
						<div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="" for="">Parent Exist</span>
						    &nbsp;&nbsp;&nbsp;&nbsp;
						  </div>
						  <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input checkdb" id="parentY" name="parent" value="Y">
							  <label class="custom-control-label font-sz-level8" id="parentYes" for="parentY" style="font-size: 0.8rem;color: #000;">Yes</label>
							   
                       </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                       <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input checkdb" id="parentN" name="parent" value="N">
							  <label class="custom-control-label font-sz-level8" id="parentNo" for="parentN" style="font-size: 0.8rem;color: #000;">No</label>
							   
                       </div>
						 
						</div>
                     </div> 
                  
                   <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                   
                    <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Parent Name</span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="parentName" id="parentName" maxlength="75" />				  
					 
                 </div>
                     </div> 
                     
				</div>
				
				 <div class="form-row  mt-3">
                    <div class="col-12 col-sm-4">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Previous name</span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="previousName" id="previousName" maxlength="75" />				  
					   
                 </div>
                </div>
                  
                   <div class="col-12 col-sm-4 ">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text">Website</span>
						  </div>
						  
						    <input type="text" class="form-control checkdb"  id="website" name="website" maxlength="100"> 
						    <button class="btn btn-sm btn-bs3-prime" type="button" id="checkWebLink" title="Click to check link"  onClick="javascript: window.open('http://' + document.getElementById('website').value);" >Go!</button>
						 
						</div>
                     </div> 
                  
                   <div class="col-12 col-sm-4 mt-4 mt-sm-0">
                   
                    <div class="input-group">
					 
                 </div>
                     </div> 
                     
				</div>	 
              
               </div>

                  <div class="button-row d-flex mt-3">
                  
                 
                    <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('Prev')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
                    
                  
                  
                  <button class="btn btn-sm btn-bs3-prime ml-auto js-btn-next  SteperBtnStyle" onclick="ValidateMasterTenantform()" id="MasterTenantformsave"  type="button" title="Save"><i class="fa fa-floppy-o"></i>&nbsp;Save&nbsp;Master&nbsp;Tenant&nbsp;Dets</button>
               	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               	   <button class="btn btn-sm btn-bs3-prime ml-auto js-btn-next SteperBtnStyle  d-none" onclick="UpdateMasterTenantform()" id="UpdateTenantformsave"  type="button" title="Update"><i class="fa fa-floppy-o"></i>&nbsp;Update&nbsp;Master&nbsp;Tenant&nbsp;Dets</button>
               	 <!--  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               	   <button class="btn btn-sm btn-bs3-prime SteperBtnStyle d-none" onclick="DeleteMasterTenantform()"  id="MasterTenantformDelete"   type="button" title="Delete"><i class="fa fa-floppy-o"></i>&nbsp;Delete</button>
               	    -->
                   <!--  <button class="btn btn-sm btn-bs3-prime  ml-auto js-btn-next  SteperBtnStyle" type="button" title="Next" onclick="openTabSec('Next')">Next&nbsp;<i class="fa fa-arrow-circle-right " ></i></button> -->
                  </div>
                </div>
              </div>
              </div>
              
             
               
            </div>
          </div>
        </div>
      </div>
    </div>
   
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="vendor/psgisubmit/js/master_tenant_dist.js"></script>
    <script src="vendor/psgisubmit/js/Common Script.js"></script>
    
    <script>
     
 // Bootstrap Date Picker
	 $('#simple-date1 .input-group.date').datepicker({
	 	//dateFormat: 'dd/mm/yyyy',
	 	format: 'dd/mm/yyyy',
	 	todayBtn: 'linked',
	 	language: "it",
	 	todayHighlight: true,
	 	autoclose: true,         
	 });
	 $('#roc-date .input-group.date').datepicker({
		 	//dateFormat: 'dd/mm/yyyy',
		 	format: 'dd/mm/yyyy',
		 	todayBtn: 'linked',
		 	language: "it",
		 	todayHighlight: true,
		 	autoclose: true,         
		 });
       

     </script>
  
  
</body>

</html>