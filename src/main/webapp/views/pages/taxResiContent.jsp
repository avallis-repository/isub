<%@page import="com.afas.isubmit.util.PsgConst"%>

						      <div class="row">
                                   <div class="col-md-12">
					                           <div class="card">
					                           
					                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
															        <div class="row">
															          <div class="col-md-12">
																	    <span> U.S Tax Declaration Under Foreign Account Tax Compliance Act (FATCA)</span>
																
																	  </div>
															          
															        </div>
												</div>
					                              <div class="card-body">
					                                  <div class="row">
					                                  
					                                   <div class="col-md-6">
						          <div class="card ">
						                <div class="card-header" id="cardHeaderStyle"><img src="vendor/psgisubmit/img/client1.png">&nbsp;Client(1)</div>
						                 <div class="card-body">
						                 
						                      <div class="row">
						                              <div class="col-md-12">
						                                  <!--  -->
						                                     <form class="bs-example" action="">
						                                     <input type="hidden" id="hdnSessDataFormId" value="${DATA_FORM_ID}"/>
						                                    
  <div class="panel-group" id="accordionTax">
    <div class="panel panel-default">
      <div class="panel-heading">
      
       <h6 class="panel-title">
           <span class="custom-control custom-checkbox">
			  <input type="checkbox" onclick="chngChktoRadFATCA(this,'client');" id="dfSelfUsnotifyflg1" name="dfSelfUsnotifyflg" 
			  class="custom-control-input checkdb dfSelfUsnotifyflggrp" maxlength="1">
			  <label class="custom-control-label font-sz-level6 pt-1 text-custom-color-gp" for="dfSelfUsnotifyflg1">&nbsp;&nbsp;I am  <span class="bold text-primaryy txt-urline">Not</span> a U.S Person
			  <a data-toggle="collapse" data-parent="#accordionTax" href="#collapseSec1"></a>
			  </label>
          </span>
       </h6>
       
      </div>
      <div id="collapseSec1" class="panel-collapse collapse in hide">
        <div class="panel-body">
             <div class="row">
				<div class="col-md-12">
				  <div class="pl-4 ml-4 divStyle-01">
					<span class="font-sz-level7"> <strong class="text-primaryy font-sz-level7  italic">I hereby confirm that:</strong><br>
	                       I am not a U.S. Person and I am not acting for / on behalf of a U.S. Person / U.S. Indicia .
	                       If my tax status change and i become a U.S. Person, I shall notify Association of Financial Advisers within 30 days from the date of change and provide the necessary documents? 
	                </span>
	              </div>
				</div>
			</div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
          <h6 class="panel-title">
           <span class="custom-control custom-checkbox">
			  <input type="checkbox" onclick="chngChktoRadFATCA(this,'client')"
				   id="dfSelfUspersonflg1" name="dfSelfUspersonflg" class="custom-control-input checkdb dfSelfUsnotifyflggrp" maxlength="1">
			  <label class="custom-control-label font-sz-level6 pt-1 text-custom-color-gp" for="dfSelfUspersonflg1">&nbsp;&nbsp;I am a <span class="bold text-primaryy txt-urline">U.S</span>&nbsp;(&nbsp;<img src="vendor/psgisubmit/img/americaflag.png">&nbsp;)&nbsp;Person
			  <a data-toggle="collapse" data-parent="#accordionTax" href="#collapseSec2"></a>
			  </label>
          </span>
       </h6>
      
       
      </div>
      <div id="collapseSec2" class="panel-collapse collapse hide">
        <div class="panel-body">
           <div class="row">
				 <div class="col-md-12">
				     <div class="pl-4 ml-4 divStyle-01">
					 <span class="font-sz-level7"><strong class="text-primaryy font-sz-level7 italic">I hereby confirm that:</strong><br> I am a U.S Person and i have Submitted the Completed Form <span class="text-info">W -9<sup>1</sup></span> </span>
							<div class="row">
								<div class="col-md-12">
									
										 <div class="form-group">
											 <label for="tdUSClntLblTxt" class="frm-lable-fntsz" id="tdUSClntLblTxt">TIN of Client(1)</label>
											 <input type="text" class="form-control checkdb" name="dfSelfUstaxno" id="dfSelfUstaxno" maxlength="60">
										</div>
									
								</div>
							</div>
					  </div>
				 </div>
			</div>  
        </div>
      </div>
    </div>
  </div></form>
                  </div>
						                         
						                         </div>
						                      
						                 </div>  
						           </div>
						     </div>
						     
						      <div class="col-md-6">
						          <div class="card ">
						                <div class="card-header" id="cardHeaderStyle"><img src="vendor/psgisubmit/img/client2.png">&nbsp;Client(2)</div>
						                 <div class="card-body">
						                 
						                 <div class="row">
						                              <div class="col-md-12">
						                                  <!--  -->
						                                     <form class="bs-example" action="">
  <div class="panel-group" id="accordionTaxi">
    <div class="panel panel-default">
      <div class="panel-heading">
      
       <h6 class="panel-title">
           <div class="custom-control custom-checkbox">
			  <input type="checkbox" onclick="chngChktoRadFATCA(this,'spouse');" id="dfSpsUsnotifyflg1" name="dfSpsUsnotifyflg"
			   class="custom-control-input checkdb dfSpsUsnotifyflggrp" maxlength="1">
			  <label class="custom-control-label font-sz-level6 pt-1  text-custom-color-gp" for="dfSpsUsnotifyflg1">&nbsp;&nbsp;I am  <span class="bold text-primaryy txt-urline">Not</span>  a U.S Person
			         <a data-toggle="collapse" data-parent="#accordionTaxi" href="#collapseSec3"></a>
			  </label>
          </div>
       </h6>
        
      </div>
      <div id="collapseSec3" class="panel-collapse collapse in hide">
        <div class="panel-body">
             <div class="row">
				<div class="col-md-12">
				     <div class="pl-4 ml-4 divStyle-01">
					<span class="font-sz-level7"> <strong class="text-primaryy font-sz-level7 italic">I hereby confirm that:</strong><br>
	                       I am not a U.S. Person and I am not acting for / on behalf of a U.S. Person / U.S. Indicia .
	                       If my tax status change and i become a U.S. Person, I shall notify Association of Financial Advisers within 30 days from the date of change and provide the necessary documents? 
	                </span>
	                </div>
				</div>
			</div>
        </div>
      </div>
    </div>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h6 class="panel-title">
           <div class="custom-control custom-checkbox">
			  <input type="checkbox" onclick="chngChktoRadFATCA(this,'spouse')"
			  id="dfSpsUspersonflg1" name="dfSpsUspersonflg" class="custom-control-input checkdb dfSpsUsnotifyflggrp" maxlength="1">
			  <label class="custom-control-label font-sz-level6 pt-1  text-custom-color-gp" for="dfSpsUspersonflg1">&nbsp;&nbsp;I am a <span class="bold text-primaryy txt-urline">U.S</span>&nbsp;(&nbsp;<img src="vendor/psgisubmit/img/americaflag.png">&nbsp;)&nbsp;Person
			  <a data-toggle="collapse" data-parent="#accordionTaxi" href="#collapseSec4"></a>
			  </label>
          </div>
       </h6>
       
      </div>
      <div id="collapseSec4" class="panel-collapse collapse hide">
        <div class="panel-body">
             <div class="row">
								<div class="col-md-12">
								    <div class="pl-4 ml-4 divStyle-01">
									 <span class="font-sz-level7"><strong class="text-primaryy font-sz-level7 italic">I hereby confirm that:</strong><br> I am a U.S Person and i have Submitted the Completed Form <span class="text-info">W -9<sup>1</sup></span> </span>
											<div class="row">
												  <div class="col-md-12">
														<div class="form-group">
																<label for="tdUSSpsLblTxt" class="frm-lable-fntsz" id="tdUSSpsLblTxt">TIN of Client(2)</label>
																<input type="text" class="form-control checkdb" name="dfSpsUstaxno" id="dfSpsUstaxno" maxlength="60">
														</div>
														
												  </div>
										
											</div>
									</div>
							 </div>
			</div>
        </div>
      </div>
    </div>
  </div></form>

						                                  
						                                  
						                              </div>
						                         
						                         </div>
						         
						                 
						                 </div>  
						           </div>
						     </div>
												       
												  </div>
												  
												  <div class="row mt-2">
																	            <div class="col-md-12">
																	             <span class="text-primaryy bold font-sz-level8"><span><i>1 Form W-9 / Form W-8BEN / Form W-8BENE can be obtained from http://www.irs.gov</i></span></span>
																	            </div>
															              </div>
												</div>
										</div>
								</div>
								
							</div><!--Sec 1 End  -->
							<!--Sec 2 start  -->
							      <div class="row">
                                   <div class="col-md-12">
                                   	           <div class="card" style="margin-top: 15px;">
					                           
					                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
															        <div class="row">
															          <div class="col-md-12">
																	    <span class="">Declaration for Common Reporting Standard (CRS)</span><span class="font-sz-level6"> [Please provide information on your Tax Residency.]</span>
																
																	  </div>
															          
															        </div>
															        
															       
												</div>
					                              <div class="card-body">
					                                  <div class="row">
					                                      <div class="col-md-12">
					                                      <span id="span_dfSelfUstaxno"></span>	
                                   	<span id="span_dfSpsUstaxno"></span>
					                
					                                           <span class="font-sz-level6">For more information on Tax Residency &amp; Tax Identification Number, please refer to <span class="text-info bold">OECD</span> website:&nbsp;</span>
					                                            
					                                            <span><i class="fa fa-external-link" aria-hidden="true"></i> <a class="bold btn-link italic font-sz-level7" target="_blank" href="http://www.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-residency/">Tax Residency</a>&nbsp;&nbsp;|&nbsp;&nbsp;</span>
					                                            <span><i class="fa fa-external-link" aria-hidden="true"></i> <a class="bold btn-link italic font-sz-level7" target="_blank" href="https://www.oecd.org/tax/automatic-exchange/crs-implementation-and-assistance/tax-identification-numbers/">Tax Identification Number</a></span>
					                                     
					                                      </div>
					                                  </div>
					                                  
					                                 
														<div class="tab-regular mt-2" >
															 <ul class="nav nav-tabs" id="selfSpouseTabs" role="tablist"> </ul>
															 	<div class="tab-content"  id="Taxcontent">
																</div>
														</div>
                                          			 </div><!--card-body -end  -->
							
							
							                 </div>
							       </div>
							       
							    </div><!-- Sec 2 end -->
							    
							    <!-- Sec 3 Start -->
							    
							     <div class="row">
                                   <div class="col-md-12">
					                           <div class="card card-content-fnsize" style="margin-top: 15px;">
					                           
					                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
															        <div class="row">
															          <div class="col-md-12">
																	    <span>Declaration</span>
																
																	  </div>
															        
															        </div>
															        
															       
												</div>
					                              <div class="card-body">
					                                 <div class="row">
					                                      <div class="col-md-12">
					                                      
					                                      <ul>
															  <li class="mt-1"><span class="font-sz-level6 ">I confirm that I am not a tax resident of any country(ies) other than the one(s) that I have declared above.</span></li>
															   <li class="mt-1"><span class="font-sz-level6 ">I declare that the information provided on this form is to the best of my knowledge and belief, accurate and complete.</span></li>
															   <li class="mt-1"><span class="font-sz-level6 ">I agree to notify and provide assistance Association of Financial Advisers Pte Ltd., immediately in the event of changes to the information I have declared for it to comply with the relevant tax regulations.</span></li>
															   <li class="mt-1"><span class="font-sz-level6 ">I agree that Association of Financial Advisers Pte Ltd., can supply any of such information or documentation to any relevant tax authority to the extent required under FATCA and CRS and its implementing regulations and applicable intergovernmental agreements.</span></li>
                                                          </ul>
					                                        
					                                       
					                                      
					                                      </div>
					                                 
					                                 </div>
					                              
					                              
					                              </div>
							
							
							                 </div>
							       </div>
							       
							    </div><!-- Sec 3 end -->
				 