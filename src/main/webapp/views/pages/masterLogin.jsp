<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<body>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	
	<title>Master Login</title>
	
	<link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="${contextPath}/vendor/bootstrap453/css/bootstrap.css">
	<link rel="stylesheet" href="${contextPath}/vendor/psgisubmit/css/login.css">
	<link rel="stylesheet" href="${contextPath}/vendor/SweetAlert2/css/sweetalert2.min.css">
	<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />
	
	<script src="${contextPath}/vendor/jquery/jquery.js"></script>
	<script src="${contextPath}/vendor/jquery/popper.min.js"></script>
	<script src="${contextPath}/vendor/bootstrap453/js/bootstrap.js"></script>
	<script src="${contextPath}/vendor/SweetAlert2/js/sweetalert2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
	<%-- <script src="${contextPath}/vendor/psgisubmit/js/Common Script.js"></script> --%>
</head>
<body>
	<div class="d-flex align-items-center min-vh-100 py-3 py-md-0">
		<div class="container">
			<div style="background-color: transparent">
				<div class="row no-gutters">
					<div class="col-6" style="background-color: transparent">&nbsp;</div>
						<div class="col-6">
							<div class="loginBox">
                         		<form name="masterLoginForm" method="POST" autocomplete="off">
									<div class="row" style="background: white; border-radius: 10px; color: #1d655cf7;">
										<div class="col-5">
											<img src="img/logo.png" alt="psgisubmit Logo" style="height :60px;">
               							</div>
										<div class="col-7 center" style="font-size: 125%">Sign in to admin account</div>
            						</div>
									<p>&nbsp;</p><p>&nbsp;</p><p>&nbsp;</p>
    								<p>Admin ID</p>
									<input type="text" class="checkdb" maxlength="60" name="txtFldAdminId" id="txtFldAdminId" placeholder="Type your Admin ID" autocomplete="off"/>
							 		<div class="logn-ErrorMsg hide mb-2" id="txtFldAdminIdError">Admin ID is Required !</div>	
							
									<p>Password</p>
									<input type="password" class="checkdb" maxlength="60" name="txtFldPassword" id="txtFldPassword" placeholder="Type your Password" autocomplete="off"  /> 
							 		<div class="logn-ErrorMsg hide" id="txtFldPasswordError">Password is Required !</div>
							
									<input type="button" name="sign-in" value="Login" onclick="validateLogin();">
							
									<span class="logn-ErrorMsg hide" id="lognError">${error_message}</span><p>&nbsp;</p>
							
									<span class="logn-ErrorMsg hide">${logout_message}</span><p>&nbsp;</p>
							
									<a href="#">&copy;&nbsp;<script>document.write(new Date().getFullYear());</script> - <strong>Association of Financial Advisers Pte Ltd.,</strong> </a>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
	<script>
		function isEmptyFld(value) {
			if (value == " " || value == null || value.length == 0 || value == undefined)
				return true;
			else
				return false;
		}
	
		$("#txtFldUserName").focus();
		
		function validateLogin() {
			document.cookie = "session=valid; path=/";
			let loginFrm = document.forms["masterLoginForm"];
			let adminIdFld = loginFrm.txtFldAdminId;
			let passwordFld = loginFrm.txtFldPassword;
			
			if (isEmptyFld(adminIdFld.value)) {
				$("#txtFldAdminIdError").removeClass('hide').addClass('show');
				return false;
			} else {
				$("#txtFldAdminIdError").removeClass('show').addClass('hide');
			}
			
			if (isEmptyFld(passwordFld.value)) {
				$("#txtFldPasswordError").removeClass('hide').addClass('show');
				return false;
			} else {
				$("#txtFldPasswordError").removeClass('show').addClass('hide');
			}
			
			$.ajax({
				type: "POST",
				url: "masterLogin/loginValidate?adminId=" + adminIdFld.value + "&password=" + passwordFld.value,
				success: function(response) {
					let loginResponse = JSON.parse(response);
					if (loginResponse && loginResponse.status) {
						if (loginResponse.status == "failed") {
							Swal.fire({
								icon: "error",
								text: loginResponse.reason,
								allowEscapeKey: false,
				       			allowOutsideClick: false
							});
						} else if (loginResponse.status == "success") {
						    window.location.href="index";
						}
					}
				},
				error: function(error) {
					Swal.fire({
						icon: 'error',
						text: 'Please Try again Later or else Contact your System Administrator',
					});
				}
			});
		}
  	</script>
</html>