	     <!--page 6 Objective Section Start -->
			      
			       <div class="row mb-4">
			           <div class="col-md-12">
			           <input type="hidden" name="prodrecObjectives" id="prodrecObjectives" />
			          <span id="span_prodrecObjectives"></span>
			               <div class="card">
                               <div class="card-header  d-flex flex-row align-items-center justify-content-between" id="cardHeaderStyle-22">
                                    <span> <img src="vendor/psgisubmit/img/objective.png">&nbsp;Objectives<span class="font-sz-level5 font-normal pl-1">[Please select the type of objective(s) which you are planning for.]</span> </span>
                                     
                               </div>
                               <div class="card-body">
                                  <div class="row" >
                                    
               <div class="row" id="objSec">                    
              <div class="col-sm-12 col-md-4  mb-2">
            
              <div class="card" style="background: #5674c229;">
                <div class="card-body p-2">
                  <div class="row align-items-center">
                    <div class="col mr-2">
                        
                      <div class="custom-control custom-checkbox">
	                      <input type="checkbox" class="custom-control-input checkdb" id="htfarallobj" name="chkProdObject" data="ALLOBJ" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'prodrecObjectives');slctAllObjTypes(this)">
	                      <label class="custom-control-label font-sz-level7 mb-0 bold pt-1" for="htfarallobj">&nbsp;Select All Objectives</label>
                      </div>
                      
                    
                     </div>
                    <div class="col-auto">
                      <img src="vendor/psgisubmit/img/allObj.png" >
                    </div>
                  </div>
                </div>
              </div>
            </div>  
                   
               <div class=" col-sm-12 col-md-4  mb-2">
              <div class="card ">
                 <div class="card-body p-2">
                  <div class="row align-items-center">
                    <div class="col mr-2">
                    
                      <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input checkdb" id="htfarpro" name="chkProdObject" data="PRO" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'prodrecObjectives');">
                     <label class="custom-control-label font-sz-level7 mb-0 bold pt-1" for="htfarpro">&nbsp;Protection Needs</label>
                    </div>
                     
                      
                    </div>
                    <div class="col-auto">
                      <img src="vendor/psgisubmit/img/shield.png">
                    </div>
                  </div>
                </div>
              </div>
            </div> 
            
            
            <div class=" col-sm-12 col-md-4  mb-2">
              <div class="card ">
                 <div class="card-body p-2">
                  <div class="row align-items-center">
                    <div class="col mr-2">
                    
                     <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input checkdb" id="htfarret" name="chkProdObject" data="RET" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'prodrecObjectives');">
                     <label class="custom-control-label font-sz-level7 mb-0 bold pt-1" for="htfarret">&nbsp;Retirement Needs</label>
                    </div>
                     </div>
                     
                    <div class="col-auto">
                      <img src="vendor/psgisubmit/img/old-man.png" >
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
                   <div class=" col-sm-12  col-xl-4 col-md-5 mb-2">
              <div class="card">
                 <div class="card-body p-2">
                  <div class="row align-items-center">
                    <div class="col mr-2">
                      <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input checkdb" id="htfarinv" name="chkProdObject" data="INV" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'prodrecObjectives');"/>
                     <label class="custom-control-label font-sz-level7 mb-0 bold pt-1" for="htfarinv">&nbsp;Investment Needs</label>
                    </div>
                     
                      
                    </div>
                    <div class="col-auto">
                      <img src="vendor/psgisubmit/img/deposit.png" >
                    </div>
                  </div>
                </div>
              </div>
            </div>
                   
            
                   <div class="  col-sm-12 col-xl-4 col-md-6 mb-2">
              <div class="card">
                 <div class="card-body p-2">
                  <div class="row align-items-center">
                    <div class="col mr-2">
                      
                      <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input checkdb" id="htfarhin" name="chkProdObject" data="HIN" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'prodrecObjectives');" >
                     <label class="custom-control-label font-sz-level7 mb-0 bold pt-1" for="htfarhin">&nbsp;Health Insurance Needs</label>
                    </div>
                      
                    
                      
                    </div>
                    <div class="col-auto">
                      <img src="vendor/psgisubmit/img/doctor.png" >
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
                <div class="col-sm-12 col-xl-4 col-md-6 mb-2">
              <div class="card">
                 <div class="card-body p-2">
                  <div class="row align-items-center">
                    <div class="col mr-2">
                       
                     <div class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input checkdb" id="htfarsav" name="chkProdObject" data="SAV" onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'prodrecObjectives');">
                     <label class="custom-control-label font-sz-level7 mb-0 bold pt-1" for="htfarsav">&nbsp;Savings / Wealth Accumulation</label>
                    </div>
                     
                      
                    </div>
                    <div class="col-auto">
                      <img src="vendor/psgisubmit/img/money.png" >
                    </div>
                  </div>
                </div>
              </div>
            </div>
          
            <div class="invalid-feedbacks pl-4" id="objSecError">
               
            </div>
            
             </div>                      
               
               
                                  </div>
                              
                                  
                               </div>
                           </div>
			           </div>
			       
			       </div>
			     <!-- page 6 Objective Section End -->
			     
			     <div class="row">
                  
                  <!--  -->
                  <div class="col-12 ">
                  <span id="span_prodrecomm_msg"></span>
                  
                      <div class="card mb-4 h-100">
                           <div class="card-header" id="cardHeaderStyle-22">
                             <div class="row">
                                  <div class="col-3">
                                       <span>New Purchase &amp; Top Up</span>
                                  </div>
                                  <div class="col-3"></div>
                                  <div class="col-3"><span class="badge badge-pill badge-success py-2 btnshake  show" title="Add Product Recommendations" id="badgeAddProdRecomm"><i class="fa fa-shield" aria-hidden="true"></i><sub><i class="fa fa-plus" aria-hidden="true"></i></sub>&nbsp;<span class="font-sz-level9">Click here to Add Product Recommendations</span></span></div>
                                  <div class="col-3">
                                       <span class="badge badge-pill badge-warning py-2 " title="View Recommendations Summary" data-toggle="" data-target="" onclick="loadRecommSumDet()" id="btnViewSumDetls"><i class="fa fa-external-link" aria-hidden="true" ></i>&nbsp;<span class="font-sz-level9">Click here to view Recomm. Summary</span></span>
                                  </div>
                             </div>
                           </div>
                           
                           <div class="card-body ProdDetlsSec" id="ProdDetlsSec" style="min-height:65vh;overflow-y:scroll;">
                           
                           <div class="row NoProdDetlsRow show" id="NoProdDetlsRow">
                                <div class="col-3">
                                
                                </div>
                                
                                <div class="col-6">
                                <p class="center noData mt-2 p-2" id="NoProdsInfo"><img src="vendor/psgisubmit/img/warning.png" class="mr-4">&nbsp;No Product Details Available in New Purchase &
                                 Top Up Section ,
                                 
                                 <span class="badge badge-pill badge-success py-2 btnshake mt-1 ls-14 show" title="Add Product Recommendations" id="btnAddRecommDetls">
                                 <i class="fa fa-shield" aria-hidden="true"></i><sub><i class="fa fa-plus" aria-hidden="true"></i></sub>&nbsp;<span class="font-sz-level9">Click here to Add Product Recommendations</span></span>
                                 
                                  to Recommend Product Details!&nbsp;
                                </p>
                                
                                 <!-- <img src="vendor/psgisubmit/img/insurance.gif" class="rounded mx-auto d-block img-fluid  prod-img" id="prodImg"  style="width: 100%;border: 1px solid grey;background: #ebf7f7;">
                                 <p class="center noData mt-2" id="NoProdsInfo"><img src="vendor/psgisubmit/img/warning.png" class="mr-5">&nbsp;No Product Details Available&nbsp;</p> -->
                                </div>
                                
                                 <div class="col-3">
                                
                                </div>
                           
                           </div>
                           
                           
                           
                           
                            <div class="row prodContentSec" id="prodContentSec">
                            
                            <div class="col-12">
		                               <ul class="nav nav-tabs hide" id="prodTabs" role="tablist">
		                             
		                             
		                               </ul>
		                             
		                               <div class="tab-content" id="prodTabContent">
		                              
		                               </div>
		                               
		                       </div>
                             </div>
                           
                          </div>
                          
                         
                     </div> 
                   </div>       
                
                  </div>
                   </div>       
                
                  </div>
			     