<%@page import="com.afas.isubmit.util.PsgConst"%>
<html lang="en">

<body id="page-top">
  		<!-- Container Fluid-->
       <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		<!-- <div id="frmBasisOfRecomForm"> -->
		<input type="hidden" id="hdnSessFnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		 
	       <div class="col-5">
		       <h5>Advice &amp; Basis of Recommendations.</h5>
		  </div>
		  <div class="col-md-7">
		           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
		  </div>
       
         </div>
		  
		  <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>
		 
		<div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh">
		
		    <div class="card-body">
		      <jsp:include page="/views/pages/advBasisContent.jsp"></jsp:include>
		 
		    </div>
		    <div class="card-footer"></div>
		
       
      </div>
      
      </div>
  

<!-- Popover contents Advice Basis Recomm.  each card notes content here -->

<div id="popover-contentPg10CardNotes1" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Client's concern and objectives, time horizon, where applicable.</li>
    
  </ul>
</div>

<div id="popover-contentPg10CardNotes2" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;How the plan meets client's need(s).</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Features and benefits of selected product.</li>
    
  </ul>
</div>

<div id="popover-contentPg10CardNotes3" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Possible risk(s) relating to
product(s) sold.</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Possible disadvantage(s)/
limitation / Exclusion /</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Warning based on
circumstances disclosed by client.</li>
  </ul>
</div>

<div id="popover-contentPg10CardNotes4" style="display: none">
  <ul class="list-group custom-popover">
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style" ></i>&nbsp;Premiums / Investment
amount are more than client's budget.</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Product(s) recommended
are of higher risk than client's risk preference.</li>
    <li class="list-group-item"><i class="fa fa-sticky-note-o" aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Client choice of product(s)
differs from product(s) recommended by Representative.</li>
    
  </ul>
</div>


<!--page 10 each card sec text area model start  -->

<!-- The Modal -->
<div class="modal" id="txtAreaModal1">
  <div class="modal-dialog" style="max-width: 550px;">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-user" aria-hidden="true" style="color:;"></i>&nbsp;Client's Objective(s)</h6>
        <button type="button" class="close viewAdvRec" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" name="advrecReason_pop" id="advrecReason_pop" onkeydown="textCounter(this,3950);" maxlength="3950" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          
        
       <div class="row" style="width: 550px;">
              <div class="col-md-4 text-center" style="border-right: 1px solid grey;">
              
              <button type="button" class="btn btn-link btn-sm text-wrap checkdb viewAdvRec" data-toggle="modal" data-target="#txtAreaModal3" data-dismiss="modal"  style="font-size: 11px;"  >Reason For Recomm.</button>
              </div>
              
              <div class="col-md-3 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal2" data-dismiss="modal"  style="font-size: 11px;"  >Risk/Limit</button>
              </div>
              
              <div class="col-md-4 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal4" data-dismiss="modal"  style="font-size: 11px;"  >Reason For Deviate.</button>
              </div>
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime viewAdvRec" data-dismiss="modal">OK</button>
              </div>
          </div>
      </div>

    </div>
  </div>
</div>


<div class="modal" id="txtAreaModal2">
  <div class="modal-dialog" style="max-width: 550px;">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-thumb-tack" aria-hidden="true" style="color: ;"></i>&nbsp;Risk / Limitation(s) of Products.</h6>
        <button type="button" class="close viewAdvRec" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReason2_pop" id="advrecReason2_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          
           <div class="row" style="width: 550px;">
              
              
              <div class="col-md-3 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal1" data-dismiss="modal"  style="font-size: 11px;"  >Client Obj</button>
              </div>
              
              <div class="col-md-4 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal3" data-dismiss="modal"  style="font-size: 11px;" >Reason For Recomm.</button>
              </div>
              
              <div class="col-md-4 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal4" data-dismiss="modal"  style="font-size: 11px;" >Reason For Deviate.</button>
              </div>
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime viewAdvRec" data-dismiss="modal">OK</button>
              </div>
          </div>
        
       
      </div>

    </div>
  </div>
</div>

<div class="modal" id="txtAreaModal3">
  <div class="modal-dialog" style="max-width: 550px;">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"><i class="fa fa-tags" aria-hidden="true" style="color:;"></i>&nbsp;Reason(s)for Recommendations.</h6>
        <button type="button" class="close viewAdvRec" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReason1_pop" id="advrecReason1_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
      
            <div class="row" style="width: 550px;">
              <div class="col-md-3 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal1" data-dismiss="modal"  style="font-size: 11px;"  >Client Obj</button>
              </div>
              
               <div class="col-md-4 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal2" data-dismiss="modal"  style="font-size: 11px;"  >Risk/Limit</button>
              </div>
              
              <div class="col-md-4 text-center" style="border-right: 1px solid grey;">
               <button type="button" class="btn btn-link btn-sm text-wrap checkdb viewAdvRec" data-toggle="modal" data-target="#txtAreaModal4" data-dismiss="modal"  style="font-size: 11px;" >Reason For Deviate.</button>
              </div>
              
             
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime viewAdvRec" data-dismiss="modal">OK</button>
              </div>
          </div>
         
        
       
      </div>

    </div>
  </div>
</div>

<div class="modal" id="txtAreaModal4">
  <div class="modal-dialog" style="max-width: 550px;">
    <div class="modal-content w-500">

      <!-- Modal Header -->
      <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title"> <i class="fa fa-question-circle" aria-hidden="true" style="color:;" ></i>&nbsp;Reason(s)for Deviation(s)</h6>
        <button type="button" class="close viewAdvRec" data-dismiss="modal" style="color: #fff;">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <div class="row">
              <div class="col-md-12">
		           <div class="form-group">
						 <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="15" cols="450" maxlength="3950" name="advrecReason3_pop" id="advrecReason3_pop" onkeydown="textCounter(this,3950);" onkeyup="textCounter(this,3950);"></textarea>
				         <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
				   </div>
		     </div>
        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
          <div class="row" style="width: 550px;">
              <div class="col-md-3 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal1" data-dismiss="modal"  style="font-size: 11px;" >Client Obj</button>
              </div>
              
              <div class="col-md-4 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm text-wrap checkdb viewAdvRec" data-toggle="modal" data-target="#txtAreaModal3" data-dismiss="modal"  style="font-size: 11px;"  >Reason For Recomm.</button>
              </div>
              
              <div class="col-md-4 text-center" style="border-right: 1px solid grey;">
              <button type="button" class="btn btn-link btn-sm viewAdvRec" data-toggle="modal" data-target="#txtAreaModal3" data-dismiss="modal" style="font-size: 11px;" >Risk/Limit</button>
              </div>
              
              <div class="col-md-1">
                <button type="button" class="btn btn-sm btn-bs3-prime viewAdvRec" data-dismiss="modal">OK</button>
              </div>
          </div>
       
      </div>

    </div>
  </div>
</div>



<script src="vendor/psgisubmit/js/session_timeout.js"></script>
<script type="text/javascript" src="vendor/psgisubmit/js/Common Script.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script> 
  <!-- <script src="vendor/psgisubmit/js/ekyc-layout.js"></script> --> 
  <script src="js/jquery.steps.js"></script>
  <script src="js/select2.js"></script>
  <script type="text/javascript" src="vendor/psgisubmit/js/fna_common.js"></script>
  <script type="text/javascript" src="vendor/psgisubmit/js/advice_basis_recomm.js"></script>
  
 
 
</body>

</html>