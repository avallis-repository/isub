   
		      <div class="row">
		          <div class="col-md-6">
		            <div class="card" id="card-btomspceStyle">
		            <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
		               <div class="row">
		                  <div class="col-md-9"><i class="fa fa-user" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Client's Objective(s)</span></div>
		                  <div class="col-md-2">
						      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" id="one" aria-hidden="true" data-toggle="popover1" data-trigger="hover" data-original-title="" title=""></i></span>
						      </div>
		                  <div class="col-md-1"><span><i class="fa fa-eye viewRemarkRec" id="iconViewStyle" aria-hidden="true" title="click to view Recomm.Details" data-toggle="modal" data-target="#txtAreaModal1"></i></span></div>
		                </div>
		            </div>
		            <div class="card-body">
		             
		             
		             
		               <div class="row">
		                   <div class="col-md-12">
		                         <div class="form-group">
									  
									  <textarea class="form-control txtarea-hrlines checkdb" maxlength="4000" name="advrecReason" id="advrecReason" onkeydown="textCounter(this,4000);" onkeyup="textCounter(this,4000);" rows="5"></textarea>
									  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							     </div>
		                      </div>
		              </div>
		            </div>
		         
		            
		            </div>

		             <div class="card">
		            <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">
      
                        <div class="row">
		                      <div class="col-md-9"><i class="fa fa-thumb-tack" aria-hidden="true" style="color: ;"></i>&nbsp;<span class="font-sz-level5">Risk / Limitation(s) of Product(s)</span></div>
		                     <div class="col-md-2">
						      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" aria-hidden="true" data-toggle="popover2" data-trigger="hover" data-original-title="" title=""></i></span>
						      </div>
		                      <div class="col-md-1"><span><i class="fa fa-eye viewRemarkRec" id="iconViewStyle" aria-hidden="true" title="click to view Recomm.Details" data-toggle="modal" data-target="#txtAreaModal2"></i></span></div>
		                </div>
		                
                    </div>
                    <div class="card-body">
		                 <div class="row">
		                   <div class="col-md-12">
		                         <div class="form-group">
									  
									  <textarea class="form-control txtarea-hrlines checkdb" maxlength="4000" name="advrecReason2" id="advrecReason2" onkeydown="textCounter(this,4000);" onkeyup="textCounter(this,4000);" rows="5"></textarea>
									  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							     </div>
		                      </div>
		              </div>
		            </div>
		            
		            </div>
		            
		            
		          </div>
		          
		          
		          
		          <div class="col-md-6">
		          <div class="card" id="card-btomspceStyle">
		              <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">

						<div class="row">
						    <div class="col-md-9"><i class="fa fa-tags" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Reason(s) for Recommendations</span> </div>
						      <div class="col-md-2">
						      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" aria-hidden="true" data-toggle="popover3" data-trigger="hover" data-original-title="" title=""></i></span>
						      </div>
						    <div class="col-md-1"><span><i class="fa fa-eye viewRemarkRec" id="iconViewStyle" aria-hidden="true" title="click to view Recomm.Details" data-toggle="modal" data-target="#txtAreaModal3"></i></span></div>
						  </div>
						
                       </div>
		                <div class="card-body">
		             
		              
		             
		               <div class="row">
		                   <div class="col-md-12">
		                         <div class="form-group">
									  <textarea class="form-control txtarea-hrlines checkdb" maxlength="4000" name="advrecReason1" id="advrecReason1" onkeydown="textCounter(this,4000);" onkeyup="textCounter(this,4000);" rows="5"></textarea>
									  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							     </div>
		                      </div>
		              </div>
		            </div>
		            
		            </div>
		            <div class="card">
		              <div class="card-header" id="cardHeaderStyle-22" style="font-weight:normal;">

						<div class="row">
						     <div class="col-md-9">
						      <i class="fa fa-question-circle" aria-hidden="true" style="color:;"></i>&nbsp;<span class="font-sz-level5">Reason(s) for Deviation(s) on Fin. Adviser's recomm.</span>
						     </div>
						       <div class="col-md-2">
						      <span class="badge badge-pill badge-warning">Note:&nbsp;<i class="fa fa-commenting Note-iconStyle" aria-hidden="true" data-toggle="popover4" data-trigger="hover" data-original-title="" title=""></i></span>
						      </div>
						      <div class="col-md-1"><span><i class="fa fa-eye viewRemarkRec" aria-hidden="true" id="iconViewStyle" title="click to view Recomm.Details" data-toggle="modal" data-target="#txtAreaModal4"></i></span></div>
						</div>
						
						

                            </div>


		            <div class="card-body">
		             
		               
		             
		               <div class="row">
		                   <div class="col-md-12">
		                         <div class="form-group">
									  <textarea class="form-control txtarea-hrlines checkdb" maxlength="4000" name="advrecReason3" id="advrecReason3" onkeydown="textCounter(this,4000);" onkeyup="textCounter(this,4000);" rows="5"></textarea>
									  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							     </div>
		                      </div>
		              </div>
		            </div>
		            
		            </div>
		          </div>
		      </div>