<%@ page language="java" import="java.util.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%ResourceBundle psgprop =ResourceBundle.getBundle("properties/psgisubmit");%>
<!DOCTYPE html>
<html lang="en">

<head>
  <link href="vendor/jquery/jquerysctipttop.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="vendor/psgisubmit/css/style_toggle.css">
  <link rel="stylesheet" href="vendor/psgisubmit/css/bs_style.css">
  <link href="vendor/psgisubmit/css/stepper.css" rel="stylesheet">
  <!-- <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css"/> -->
  <link href="vendor/psgisubmit/css/theme2.css" media="all" rel="stylesheet" type="text/css"/>
   
  <link href="vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" >
  
  <style>
  
  /* #tblAdviserManagementDis tr:hover{
     cursor: pointer;
     color: #fff;
	 background-color: #337ab7;
	 border-color: #2e6da4;
  } */
  </style>
 </head>

<body id="page-top">
 
        <!-- Container Fluid-->
       <div class="container " id="container-wrapper" style="min-height:100vh">   
		  
      <!--multisteps-form-->
      <div class="multisteps-form" style="margin-top:10px;">
       <div class="row">
          <div class="col-12 col-lg-12 ml-auto mr-auto ">
            <div class="multisteps-form__progress">
              <button class="multisteps-form__progress-btn  js-active" type="button" title="List Of Values" id="btnAdviserManagerList" onclick="openTabSec('AdviserManagerList')"><i class="fa fa-list" style="font-size:1.4rem;color:#ff9800;"></i><span>&nbsp;List</span></button>
              <button class="multisteps-form__progress-btn" type="button" title="Adviser Details" id="btnAdviserMag" onclick="openTabSec('AdviserManager')"><i class="fa fa-user" style="font-size:1.4rem;color:#4caf50;" ></i><span>&nbsp;Adviser&nbsp;Details</span></button>
              <button class="multisteps-form__progress-btn  d-none" type="button" title="Adviser Code" id="btnPrincipanInfo" onclick="openTabSec('PrincipalInfo')"><i class="fa fa-info" style="font-size:1.4rem;color:#f33;" ></i><span>&nbsp;Adviser&nbsp;Codes</span></button>
             
               
            </div>
          </div>
        </div> 
         
        <!--progress bar-->
        <!--  <div class="row">
          <div class="col-12 col-lg-12 ml-auto mr-auto ">
            <div class="multisteps-form__progress">
              <span style="margin: auto;">&nbsp;Adviser&nbsp;Management</span>
               
            </div>
          </div>
        </div> --> 
        <!--form panels-->
        <div class="row">
          <div class="col-12 col-lg-12 m-auto">
            <div class="multisteps-form__form" style="height:550px;">
              <!--single form panel-->
              
               <div class="multisteps-form__panel shadow p-4 rounded bg-white js-active" data-animation="scaleIn" id="AdviserManagementListSec">
                <div class="card-header" id="cardHeaderStyle-22">
                <div style="margin-left: 85%;"> <!-- <button class="btn btn-sm btn-bs3-prime  ml-auto js-btn-next  SteperBtnStyle" type="button" title="Create Master Adviser" onclick="openTabSec('AdviserManager')">Create&nbsp;Master&nbsp;Adviser&nbsp;</button> -->
				 <span class="badge badge-pill badge-success py-2 btnshake  show" title="Create Master Adviser" onclick="openTabSec('AdviserManager')"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;<span class="font-sz-level9">Create&nbsp;Master&nbsp;Adviser</span></span>
				 </div>
                </div>
                <div class="multisteps-form__content">
                
				 
				
                  <table class="table align-items-center table-flush table-striped table-hover" id="tblAdviserManagementDis" style="width: 100%;">
			                    <thead class="thead-light">
			                      <tr>					    
			                        <th><div style="width:200px">Adv Name</div></th>
			                        <th><div style="width:90px">Email ID</div></th>
			                        <th><div style="width:90px">NRIC</div></th>
			                       	<th><div style="width:90px">Staff Type</div></th>
			                        <th><div style="width:90px">Mag Access</div></th>
			                      <!--   <th><div style="width:90px">Gender</div></th> -->
			                        <th><div style="width:90px">Action</div></th> 
			                      </tr>
			                    </thead>
			                   
			                    <tbody>
			                      
			                    </tbody>
			                  </table>
                  <div class="form-row mt-4">
					 
					 </div>
					 
                  <div class="button-row d-flex mt-4">
                    <!-- <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Next" onclick="openTabSec('PrincipalInfo')"><i class="fa fa-arrow-circle-left " ></i>Next&nbsp;</button> -->
                    <!-- <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('PrincipalInfo')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button> -->
                   
                  </div>
                </div>
              </div>
              
              <form class="masterAdviserStaffFrm" >   
              <div class="multisteps-form__panel shadow p-4 rounded bg-white " data-animation="scaleIn" id="AdviserManagementSec">
                
                <div class="multisteps-form__content">
                 
				<div id="individualSec" class="show">		
                  <div class="form-row  mt-3">
                    <div class="col-12 col-sm-6 ">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Adviser Name<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="advStfName" id="advStfName" maxlength="75" />				  
					  <div class="invalid-feedbacks hide" id="advStfNameError">Keyin Adviser Name.</div>
					  <input type="hidden" value='${advStfIdJson}'  id="advStffJson" />	
					  <!-- <input type="hidden"  name="createdDate" id="createdDate" value=""/>	
					  <input type="hidden"  name="createdBy" id="createdBy" value=""/>	 -->
					  
                 </div>
                </div>
                   
                  <div class="col-12 col-sm-2 mt-sm-0" id="simple-date1">
                   
                   <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="" for="dob">DOB<sup style="color: red;"><strong>*</strong></sup></span>
						  </div>
						 <input type="text" class="form-control checkMand checkdb" id="dob" name="dob" onchange="setClientDob(this)" maxlength="10">
						 <div class="invalid-feedbacks hide" id="dobError">Keyin DOB</div>
						 
						 
						</div>
                     </div> 
                 
                     
				</div> 
				
				<div class="form-row  mt-3">
                    <div class="col-12 col-sm-6">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">NRIC <sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="nric" id="nric" maxlength="75" />				  
					  <div class="invalid-feedbacks hide" id="nricError">Keyin NRIC</div>
                 </div>
                </div>
                  
                   <div class="col-12 col-sm-6 ">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  for="">Email Id <sup style="color: red;"><strong>*</strong></sup></span>
						  </div>
						  
						    <input type="text" class="form-control checkdb"  id="emailId" name="emailId" onblur="validateEmail(this,'emailId')"  maxlength="60">
						   <div class="invalid-feedbacks hide" id="emailIdError">keyin Email Id</div>
						 
						</div>
                     </div> 
                     
				</div>
				
				<div class="form-row   mt-3">
				
				<div class="col-12 col-sm-6 ">
                   
                    <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Mobile No </span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="handPhone" id="handPhone"  maxlength="75" />				  
					   
                 </div>
                     </div>
                     
                  <div class="col-12 col-sm-2">
			      <div id="custTypeRadioBtn">
					  <input type="radio" class="checkdb" name="sex" id="radBtnmale" value="M">
					  <label for="radBtnmale" class="lblStyle">
					  <img src="vendor/psgisubmit/img/man.png">&nbsp;Male
					  </label>
                 </div>
			</div>
			
			<div class="col-12 col-sm-2">
			      <div id="custTypeRadioBtn">
					  <input type="radio" class="checkdb" name="sex" id="radBtnFemale" value="F">
					  <label for="radBtnFemale" class="lblStyle">
					  <img src="vendor/psgisubmit/img/woman.png">&nbsp;Female
					  </label>
			     </div>
			</div>   
                     
				</div>
               
            <div class="form-row  mt-3">
			
			   <div class="col-12 col-sm-6">
                    
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text" id="" for="">Manager</span>
						    &nbsp;&nbsp;&nbsp;&nbsp;
						  </div>
						  <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input checkdb" id="managerY" name="manager" value="Y">
							  <label class="custom-control-label font-sz-level8" id="managerYes" for="managerY" style="font-size: 0.8rem;color: #000;">Yes</label>
							   
                       </div>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                       <div class="custom-control custom-radio">
							  <input type="radio" class="custom-control-input checkdb" id="managerN" name="manager" value="N">
							  <label class="custom-control-label font-sz-level8" id="managerNo" for="managerN" style="font-size: 0.8rem;color: #000;">No</label>
							   
                       </div>
						 
						</div>
                     
                     </div>  	 
				
				<div class="col-12 col-sm-6">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  for="">Staff Type</span>
						  </div>
						  
						<select class="form-control checkdb" id="staffType" name="staffType">
											    <option value="" >---Select---</option>
											     <% for(String stafftype:psgprop.getString("app.stafftype").split("\\^")){ %>
									    	<option value="<%=stafftype%>"><%=stafftype%></option>
									     <%}%>
											</select>
						 
						</div>
                     </div>
			</div>	 
				
				  <div class="form-row  mt-3">
					 
				 <div class="col-12 col-sm-6">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  for="">Manager Name  </span>
						  </div>
						   <input type="hidden" value="${advStffFlgList}" id="advStffFlgjsplist"/>
						  <select class="form-control checkdb" id="managerId" name="managerId" >
											    <option value="">---Select---</option>
											    
											 </select>
						 
						</div>
				</div>
				   
				    <div class="col-12 col-sm-6">
                    
                     </div>	 
                     
               </div>
               
               <div class="form-row  mt-3">
					 
				 <div class="col-12 col-sm-6">
				 <div class="form-group">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <!-- <span class="input-group-text"  for="">Tenant Name   <sup style="color: red;"><strong>*</strong></sup></span> -->
						    <label for=""><strong>Upload an excel document: </strong></label>
						  </div>
						  
						   <input type="file" id="isubmitFile" class="form-control-file"><br>
						<input id="isubmitfileUpload" type="button" value="Upload" class="btn btn-primary"> 
						  
						</div>
						</div>
                     </div>	  
				
				<div class="col-12 col-sm-6">
				<div class="invalid-feedbacks hide" id="successAlertText" style="color: green;" >Data uploaded successfully!</div>
				<div class="invalid-feedbacks hide" id="dangerAlertText">Please select the file!</div>
		        
				</div>
				   
               </div>
              
               </div>
               
                  <div class="button-row d-flex mt-3">
                 
                
                 <!--  <button class="btn btn-sm btn-bs3-prime  SteperBtnStyle" onclick="ValidateMasterAdviserStff()" id="MasterAdviserStffsave"  type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Save&nbsp;Master&nbsp;Tenant&nbsp;Dets</button>
               	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
               	 <!--   <button class="btn btn-sm btn-bs3-prime  SteperBtnStyle  d-none" onclick="UpdateMasterAdviserStffform()" id="MasterAdviserStffUpdate"  type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Update&nbsp;Master&nbsp;Tenant&nbsp;Dets</button>
               	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               	   <button class="btn btn-sm btn-bs3-prime SteperBtnStyle d-none" onclick="DeleteMasterAdviserStffform()"  id="MasterAdviserStffDelete"   type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Delete</button>
               	    -->
                    <!--  <button class="btn btn-sm btn-bs3-prime  ml-auto js-btn-next  SteperBtnStyle" type="button" title="Next" onclick="openTabSec('AdviserManagerList')">Next&nbsp;<i class="fa fa-arrow-circle-right " ></i></button>  -->
                      <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('AdviserManagerList')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
                   
                     &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <button class="btn btn-sm btn-bs3-prime  SteperBtnStyle" onclick="ValidateMasterAdviserStff()" id="MasterAdviserStffsave"  type="button" title="Save"><i class="fa fa-floppy-o"></i>&nbsp;Save&nbsp;Master&nbsp;Adviser&nbsp;Dets</button>
                   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <button class="btn btn-sm btn-bs3-prime  SteperBtnStyle  d-none" onclick="UpdateMasterAdviserStffform()" id="MasterAdviserStffUpdate"  type="button" title="Send"><i class="fa fa-floppy-o"></i>&nbsp;Update&nbsp;Master&nbsp;Tenant&nbsp;Dets</button>
               	  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   
                   
                   
                    <button class="btn btn-sm btn-bs3-prime  ml-auto js-btn-next  SteperBtnStyle  d-none"   id="btnadvisercodes"  type="button" title="Next" onclick="openTabSec('PrincipalInfo')">Next&nbsp;<i class="fa fa-arrow-circle-right " ></i></button>
                 
                  </div>
                </div>
              </div>
              </form>
              
              <!-- <div class="col-3"><span class="badge badge-pill badge-success py-2 btnshake  show" title="Add Adviser Code" id="addAdviserCode"><i class="fa fa-shield" aria-hidden="true"></i><sub><i class="fa fa-plus" aria-hidden="true"></i></sub>&nbsp;<span class="font-sz-level9">Click here to Add Adviser Code</span></span></div> -->
              
              
              <div class="multisteps-form__panel shadow p-4 rounded bg-white" data-animation="scaleIn" id="PrincipalInfoSec">
              
              <!-- Modal -->
           <div class="card-header" id="cardHeaderStyle-22">
			 <!-- <input type="button" class="btn btn-sm btn-bs3-prime btnClntAddSrch" value="Add Adviser Code " data-toggle="modal" data-target="#popupAdviserCodeModal" id="" /> -->
			 <span class="badge badge-pill badge-success py-2 btnshake  show" title="Add Adviser Codes"  data-toggle="modal" data-target="#popupAdviserCodeModal"  id=""><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;<span class="font-sz-level9">Add&nbsp;Adviser&nbsp;Codes</span></span>
			               <!-- <input class="form-control rounded bright" placeholder="Search Adviser Codes..." type="search" id="srchAdviserCodes"> -->
			               
			   </div>
                                     <div class="tab-content" id="adviserCodesTabContent"  style="overflow-y: scroll; height: 450px;">
		                         
		                         
		                              
		                               </div>
		                               
		                               
		       <div class="button-row d-flex mt-3">
                 
                
                      <button class="btn btn-sm btn-bs3-prime js-btn-prev SteperBtnStyle" type="button" title="Prev" onclick="openTabSec('AdviserManager')"><i class="fa fa-arrow-circle-left "></i>&nbsp;Prev</button>
                   
                  </div>                                     
              </div>
              
               
               
               
            </div>
          </div>
        </div>
      </div>
      
      <div class="modal fade" id="popupAdviserCodeModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
            aria-labelledby="srchClntModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="srchClntModalTitle">&nbsp;&nbsp;Add&nbsp;Adviser&nbsp;Codes</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                
                <div class="col-lg-12">
              <div class="card mb-4" >
              
              <div class="card-body" id="">
              
             <form class="principalinfoFrm" >  
              <div class="multisteps-form__content">
                 
				<div id="individualSec" class="show">		
                  <div class="form-row  mt-3">
                    <div class="col-12 col-sm-6 ">
				  <div class="input-group">
					  <div class="input-group-prepend">
					   <input type="hidden" value="${princinfo }" id="princinfojsplist"/>
						<span class="input-group-text"  id="">Principal Name<sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  <select class="form-control checkMand checkdb" id="princName" name="princName" >
											    <option value="">---Select---</option>
											    
											 </select>
					  
					  <div class="invalid-feedbacks hide" id="princNameError">Keyin Principal Name.</div>
					  <input type="hidden" name="princeinfoid" id="princeinfoid" value=""/>	
					<!--   <input type="hidden" name="advstfId" id="princinfoadvStfId" value=""/>	 -->
					  <input type="hidden" name="createdBy"   id="princcreatedBy" value=""/>	
					  <input type="hidden" name="createdDate"   id="princcreatedDate" value=""/>	
                 </div>
                </div>
                   
                   <div class="col-12 col-sm-6 ">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Principal Code</span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="princCode" id="princCode" maxlength="75" readonly="readonly"/>				  
					  
					  
                 </div>
                </div> 
                     
				</div> 
				
				<div class="form-row  mt-3">
                    <div class="col-12 col-sm-6">
				  <div class="input-group">
					  <div class="input-group-prepend">
						<span class="input-group-text"  id="">Adviser Code <sup style="color: red;"><strong>*</strong></sup></span>
					  </div>
					  
					  <input type="text" class="form-control checkMand checkdb" name="adviserCode" id="adviserCode" maxlength="75" />				  
					 <div class="invalid-feedbacks hide" id="adviserCodeError">Keyin Principal Name.</div>
                 </div>
                </div>
                  
                   <div class="col-12 col-sm-6 ">
                    <div class="input-group input-group date">
						  <div class="input-group-prepend">
						    <span class="input-group-text"  for="">Remarks </span>
						  </div>
						  
						    <input type="text" class="form-control checkdb"  id="remarks" name="remarks"   maxlength="60">
						   
						 
						</div>
                     </div> 
                     
				</div>
				
				<div class="form-row   mt-3">
				
				<div class="col-12 col-sm-6 ">
                   
                     
                     </div>
                     
                   <div class="col-12 col-sm-6 ">
                   
                     
                     </div>
                     
				</div>
                
              
               </div>
               
                  <div class="button-row d-flex mt-4">
               	    <button class="btn btn-sm btn-bs3-prime  SteperBtnStyle" onclick="ValidateAdviserCode()" id="saveAdviserCode"  type="button" title="Save"><i class="fa fa-floppy-o"></i>&nbsp;Save&nbsp;Adviser&nbsp;Codes</button>
                 &nbsp;&nbsp;
                   <button class="btn btn-sm btn-bs3-prime  SteperBtnStyle  d-none" onclick="updateAdviserCode()" id="updateAdviser"  type="button" title="Update"><i class="fa fa-floppy-o"></i>&nbsp;Update&nbsp;Adviser&nbsp;Dets</button>
                  </div>
                </div>
                
               </form>
                </div>
              </div>
            </div>
                  
                </div>
                <div class="modal-footer float-left p-1">
				
                  <button type="button" class="btn btn-sm pl-3 pr-3 btn-bs3-prime" data-dismiss="modal">Close</button>
                  
                </div>
              </div>
            </div>
          </div>
          
          
    </div>

       
        
   
  <script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="vendor/psgisubmit/js/master_adviser_management.js"></script>
    <script src="vendor/psgisubmit/js/Common Script.js"></script>
    
    <script>
     
 // Bootstrap Date Picker
	 $('#simple-date1 .input-group.date').datepicker({
	 	//dateFormat: 'dd/mm/yyyy',
	 	format: 'dd/mm/yyyy',
	 	todayBtn: 'linked',
	 	language: "it",
	 	todayHighlight: true,
	 	autoclose: true,         
	 });
	 $('#roc-date .input-group.date').datepicker({
		 	//dateFormat: 'dd/mm/yyyy',
		 	format: 'dd/mm/yyyy',
		 	todayBtn: 'linked',
		 	language: "it",
		 	todayHighlight: true,
		 	autoclose: true,         
		 });


     </script>
  
  
</body>

</html>