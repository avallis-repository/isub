<!DOCTYPE html>

<html lang="en">
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" import="java.util.*" %>
<head>
  
  <link href="vendor/psgisubmit/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
   
  </head>
<body>
 

   <div class="modal fade" id="uplodModal" aria-modal="true" role="dialog" style="padding-right: 15px;">
    <div class="modal-dialog modal-xl  modal-dialog-scrollable">
      <div class="modal-content">
      <input type="hidden" id="categList" name="categList" value="<%= session.getAttribute("CATEGORY_LIST") %>">
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
        <h6 class="modal-title bold"><i class="fa fa-upload" aria-hidden="true"></i>&nbsp;&nbsp;Document Uploads</h6>
        <button type="button" class="close" data-dismiss="modal" style="color:#fff;">&times;</button>
      </div>
        <!-- Modal body -->
        
        
    
        <div class="modal-body">
          <div class="row">
                <div class="col-12">
                      <div class="upload">
					 <div class="upload-files">
					  <div class="row">
					            <div class="col-12">
					               <header>
					                  <div class="row">
							             <div class="col-4">
							                   <div class="">
									             <div class="pl-2 pt-3"><img src = "vendor/psgisubmit/img/custName.png" class="rounded-circle" alt="custName"> 
									             <small><span class="badge badge-light ltrspace-01 p-2"  style="cursor: default;" title="Client Name : <%=session.getAttribute("CustomerName") %>"><%=session.getAttribute("CustomerName") %></span>
									             </small></div>
							                   </div>
							             </div>
							              <div class="col-4">
							                        <p>
												    <i class="fa fa-cloud-upload" aria-hidden="true"></i>
												    <span class="up">up</span>
												    <span class="load">load</span>
												   </p>
										   </div>
										   
										   <div class="col-4">
										   	<div class="btn-group float-right">
										   	
										   	 <button type="button" class="btn btn-bs3-prime btn-sm m-2 p-2 ltrspc02 d-none" id="btnUplodAndEmail">Save & Email</button>
										   	 <!-- <button type="button" class="btn btn-bs3-prime btn-sm m-2 p-2 d-none" id="btnWithoutUplodAndEmail">Email Only</button> -->
										     <button type="button" class="btn btn-bs3-success btn-sm m-2 p-2 ltrspc02" id="btnUplodCustAttchFile">Save</button>
										     <button type="button" class="btn btn-bs3-warning amber btn-sm m-2 p-2 ltrspc02" data-dismiss="modal">Exit</button>
										  	</div>
										  </div>
										  
										
									 </div>
										  </header>
								 </div>
								
				         
				            
						 <div class="container my-4" style=" max-height: calc(100vh - 210px);  overflow-y: auto;">
							<div class="row">
					            <div class="col-9">
					                <div class="card">
					                    <div class="card-header" id="cardHeaderStyle-22"><i class="fa fa-cloud-upload" aria-hidden="true"></i> New Uploads&nbsp;
					                    <span class="font-sz-level5 font-normal pl-1">[Click Browse Button to Upload Files.]</span>
					                     </div>
					            	     <div class="card-body">
					            	          <form enctype="multipart/form-data" class="uploadMultipartFile">
									              <div class="file-loading">
										  		       <input id="file-0" name="file-0[]" type="file" multiple="multiple" multiple data-theme="fas" onchange="handleFiles(this.files)" class="checkdb">
										  	      </div>
								 	         </form>
					            	     </div>
					            	</div>
					             </div>
					            
					            <div class="col-3">
					                 <div class="card h-100">
					                     <div class="card-header" id="cardHeaderStyle-22"><i class="fa fa-cloud-download" aria-hidden="true"></i> Downloads</div>
						                 <div class="card-body" style="height: 57.5vh;
						                       overflow-y: scroll;overflow-x:hidden;padding: 0.5rem !important;border: 1px solid #5675C2;">
						                       
						                        <div class="row noDocFoundSec mt-5 show" id="noDocFoundSec">
						                             <div class="col-1"></div>
									                 <div class="col-10">
									                       <img src="vendor/psgisubmit/img/noFiles.png" class="rounded mx-auto d-block img-fluid  prod-img" id="noDocImg"
									                        style="width: 75%;border:1px solid #80808042;padding: 0px 12px;background: aliceblue;">
									                        <p class="center noData mt-2" id="noDocFoundInfo">
									                        <img src="vendor/psgisubmit/img/warning.png" class="">&nbsp;No File(s) Available&nbsp;</p>
						                            </div>
						                            <div class="col-1"></div>
						                        </div>
						                       
						                       
						                       <div class="row hide" id="DocListSec">
						                            <div class="col-12">
						                                  <div class="list-group" id="dynaAttachNTUCList">
						                            </div>
						                           </div>
						                       </div>
						                  </div>
					               </div>
					          </div>
					 </div>
				</div> 
		                  
                	 </div>
					</div>
                 </div>
           </div>
        </div>
       
      </div>
    </div>
  </div>
</div> 




  
  </body>
      <!--  <script  src="vendor/TextboxAutoComplete/jquery.autosuggest.js" type="text/javascript"></script>
     <script  src="vendor/TextboxAutoComplete/jquery.autosuggest.min.js" type="text/javascript"></script> -->
     <!-- <script type="text/javascript" src="vendor/ajaxautocomplete/jquery-1.8.2.min.js"></script> -->
    <script type="text/javascript" src="vendor/ajaxautocomplete/jquery.mockjax.js"></script>
    <script type="text/javascript" src="vendor/ajaxautocomplete/jquery.autocomplete.js"></script>
     <script type="text/javascript" src="vendor/ajaxautocomplete/countries.js"></script>
    <!--<script type="text/javascript" src="vendor/ajaxautocomplete/demo.js"></script> -->
    
     <script src="vendor/psgisubmit/js/piexif.js" type="text/javascript"></script>
    <script  src="vendor/psgisubmit/js/sortable.js" type="text/javascript"></script>
    <script  src="vendor/psgisubmit/js/fileinput.js" type="text/javascript"></script>
 
   <script  src="vendor/psgisubmit/js/document_upload.js" type="text/javascript"></script>
    
  
  </html>