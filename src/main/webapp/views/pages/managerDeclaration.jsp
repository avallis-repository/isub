<%@page import="com.afas.isubmit.util.PsgConst"%>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="vendor/psgisubmit/img/logo/logo.png" rel="icon">
  
  
    
   

<body id="page-top">
  
        <!-- Container Fluid-->
           <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		<input type="hidden" id="hdnSessFnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		 
		 <div class="col-6">
		   <h5>Representative's Declaration</h5>
		 </div>
      
          <div class="col-md-6">
		           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
		  </div>
</div>

           <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>
         
<!--Page 14 content area Start  -->
      <div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh">
			<div class="card-body" style="font-size: 13px;">
                  <div class="row">
                      <div class="col-md-12">
                           <div class="card card-content-fnsize">
                           
                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
										        <div class="row">
										          <div class="col-md-11">
												 <span> Representative's Declaration  </span>
											
												  </div>
										          
										        </div>
										        
										       
							</div>
                              <div class="card-body">
                              <div class="row">
                               <div class="col-md-8">
                                     <div class="row">
										           <div class="col-md-12 ">
										           
										           <ul class="mb-2">
										    <li class="ln-hei mb-3 "><span class="font-sz-level6 mt-1">The advice and product recommendation(s) made by me on the client(s) needs analysis has taken into
													      account of all the information disclosed by the client(s) in this "Financial Needs Analysis" Form</span></li>
										    <li class="ln-hei mb-3"><span class="font-sz-level6 mt-1"></span>The information is strictly confidential and is only to be used for the purpose of fact-finding as part of the
                                                         process in recommending suitable insurance / investment product(s) and shall not be used for any
                                                    other purposes.</li>
										    
										    </ul>
										    </div>
							       </div>
                              </div>
                              
                              <div class="col-md-4">
								             <div class="card " id="SignCard">
								                  <div class="card-header" id="cardHeaderStyle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Advisor Signature</div>
								              <div class="card-body">
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Sign. of Adviser : </label>
								                            </div>
								                            
								                             <div class="col-md-6">
                                                                 <div id="advQrSecNoData"  class="advQrSecNoData show">
								                                   <p class="center noData" id="" style="font-size: 11px;"><img src="vendor/psgisubmit/img/remove.png" class="">
								                                     &nbsp;Adviser Signature Not Found&nbsp;</p>
								                                  </div>
								                                  
								                                  <div id="sign-QR_style" class="advQrSec hide">
								                                 <small><img src="vendor/psgisubmit/img/contract.png" class="advQr"></small>
								                               </div>
								                            </div> 
								                             
								                      </div>
								                      
								                     
								                      
								                       <div class="row mt-2">
								                            <div class="col-md-6 ">
								                                 <label class="frm-lable-fntsz p-0 "> Name of Adviser:</label>
								                            </div> 
								                            <div class="col-md-6 pr-0">
								                                 <span class="frm-lable-fntsz p-0 font-normal "> <input type="text" readOnly="true" class="form-control advisername" value="<%=session.getAttribute(PsgConst.SESS_ADVISER_NAME)%>"/> </span>
								                            </div>  
								                           
								                      </div>
								                      
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                  <label class="frm-lable-fntsz p-0">Date: </label>
								                            </div>   
								                            <div class="col-md-6">
								                                  <span class="frm-lable-fntsz p-0 font-normal " id="advSignDate"> </span>
								                            </div> 
								                      </div>
								                      
								                        <div class="row mt-2 hide">
								                            <div class="col-md-6">
								                                  <label class="frm-lable-fntsz p-0 "> Mail Sent Status : </label>
								                            </div>
								                              <div class="col-md-6">
								                                 <span class="badge badge-success " id="mailSent" style="display:none;">Email Sent <i class="fa fa-check" aria-hidden="true"></i></span>
								                                 <span class="badge badge-warning font-normal" id="mailNotSent">Email Not Sent <i class="fa fa-times" aria-hidden="true"></i></span>
								                                
								                            </div> 
								                             
								                      </div>
								                  </div>
								             </div> 
								       </div>
                              </div>
                             
                                  
                               </div>
                       </div>
                  </div>
              </div>
              
              <div class="row">
                      <div class="col-md-12">
                           <div class="card card-content-fnsize disabledMngrSec" style="margin-top:10px;" id="diManagerSection">
                            
                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
										        <div class="row">
										          <div class="col-md-11">
												 <span> Supervisor's Review</span>
											
												  </div>
										          
										        </div>
										        
										       
							</div>
                              <div class="card-body">
                                   <div class="row">
										           <div class="col-md-12">
										              <span class="font-sz-level6">I have reviewed the information disclosed in the "Financial Needs Analysis" Form which relates to the
                                                       client's priorities and objectives, investment profile, insurance portfolio, CKA outcome and the client's. 
                                                        declaration &amp; acknowledgement.</span>
										           </div>
								  </div>
								   <div class="row mt-2">
                                         <div class="col-md-12">
                                         <span id="span_suprevMgrFlg"></span>
                                          <ul class="list-group mt-2">
		                                          <li class="list-group-item bg-hlght">
		                                              <span class="italic bold text-primaryy font-sz-level6 "> Select any <span class="txt-urline  bold">ONE</span> Option Below :-</span>
		                                                      <div class="row">
		                                                      
								        <div class="col-md-6 ">
								          <div class="col-md-12">
								              
								              <div id="Sec2ContentFormId">
			                                    <div class="inputGroup">
													         <input id="suprevMgrFlgY" name="suprevMgrFlg" type="radio" value="Y" class="checkdb">
															<label for="suprevMgrFlgY" id="lblTxtSz-03" style="border: 1px solid grey;"><span><span class="font-sz-level6">  <span class="text-success"><strong>I agree</strong></span> with the Representative's needs analysis and<br> recommendation(s). </span> </span></label>
												 </div>
                                              </div>
										            
										  </div>
								        </div>
								        <div class="col-md-6">
								             <div class="col-md-12">
								             <div id="Sec2ContentFormId">
												<div class="inputGroup">
													         <input id="suprevMgrFlgN" name="suprevMgrFlg" type="radio" value="N" class="checkdb">
															<label for="suprevMgrFlgN" id="lblTxtSz-03" style="border: 1px solid grey;"><span><span class="font-sz-level6"> <span class="text-danger"><strong>I disagree</strong></span> with the Representative's needs analysis and<br> recommendation(s).</span></span></label>
												 </div>
                                              </div>
										            
										       </div>
								        </div>
								        
								  </div>
		                                          
		                                          </li>
		                                          
		                                          
		                                          </ul>
                                         
                                         
                                             
								          </div>
								    </div>
								 
								 
								  
								  <div class="row mt-2">
								       <div class="col-md-12">
								         <span><i> <strong>Note: </strong> (If <span class=" text-danger bold font-sz-level8">disagree</span> , please state the reasons and/or advise on the follow-up action required, where applicable.)</i></span>
								       </div>
								       
								   </div>
								   
								   
								    <div class="row mt-2">
								       <div class="col-md-8">
								             <div class="form-group">
								                  <label class="frm-lable-fntsz "> Reason(s) and Follow-up Action:</label>
									             <textarea class="form-control txtarea-hrlines checkdb" rows="7"  id="suprevFollowReason" name="suprevFollowReason"
									             maxlength="300" onkeydown="textCounter(this,300);" onkeyup="textCounter(this,300);"  ></textarea>
							                     <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							                </div> 
								       </div>
								       
								        <div class="col-md-4">
								             <div class="card" style="margin-top:15px;" id="SignCard">
								             <div class="card-header" id="cardHeaderStyle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Manager Signature</div>
								                								                  <div class="card-body">
								                     <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Sign. of Supervisor :  </label>
								                            </div>   
								                            
								                            <div class="col-md-6">
								                                 <div id="mgrQrSecNoData"  class="mgrQrSecNoData show">
								                                   <p class="center noData" id="" style="font-size: 11px;"><img src="vendor/psgisubmit/img/remove.png" class="">
								                                     &nbsp;Manager Signature Not Found&nbsp;</p>
								                                  </div>
								                                  
								                                  <div id="sign-QR_style" class="mgrQrSec hide">
								                                    <small><img src="vendor/psgisubmit/img/contract.png"class="mgrQr"></small>
								                                  </div>
								                            </div>   
								                             
								                      </div>
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                <label class="frm-lable-fntsz p-0">Name of Supervisor:</label>
								                            </div>
								                            
								                             <div class="col-md-6">
								                             <input type="text" name="mgrName" readOnly="true" class="form-control checkdb" value="<%=session.getAttribute(PsgConst.SESS_MANAGER_NAME)%>" />
								                                 
								                            </div>  
								                                
								                      </div>
								                      
								                      
								                      
								                      <div class="row mt-2">
								                            <div class="col-md-6">
								                                 <label class="frm-lable-fntsz p-0">Date : </label>
								                            </div> 
								                            
								                             <div class="col-md-6">
								                                 <span class="frm-lable-fntsz p-0 font-normal" id="mgrSignDate"></span>
								                            </div>    
								                      </div>
								                      
								                       <div class="row mt-2 hide">
								                            <div class="col-md-6">
								                                <label class="frm-lable-fntsz p-0"> Mail Sent Status : </label>
								                            </div> 
								                             <div class="col-md-6">
								                                <span class="badge badge-success" id="mailSentMgr" style="display:none;">Email Sent <i class="fa fa-check" aria-hidden="true"></i></span>
								                                <span class="badge badge-warning font-normal" id="mailNotSentMgr">Email Not Sent <i class="fa fa-times" aria-hidden="true"></i></span>
								                            </div>    
								                      </div>
								                      
								                  </div>
								             </div> 
								       </div>
								       
								   </div>
								   
								     
                               </div>
                       </div>
                  </div>
              </div>
                  
             </div>
   </div><!-- page 14 content Area End -->
      
      
  </div>
        <!-- container-fluid-end -->
  
  </body>
          
          <script src="vendor/jquery-easing/jquery.easing.min.js"></script> 
          <!-- <script src="vendor/psgisubmit/js/ekyc-layout.js"></script> --> 
          <script src="js/jquery.steps.js"></script>
          <script src="js/select2.js"></script>
          <script src="vendor/psgisubmit/js/session_timeout.js"></script>
   		  <script type="text/javascript" src="vendor/psgisubmit/js/Common Script.js"></script>
          <script type="text/javascript" src="vendor/psgisubmit/js/fna_common.js"></script>
          <script src="vendor/psgisubmit/js/mngr_Declaration.js"></script>
           
          
  </html>
