<%@page import="com.afas.isubmit.util.PsgConst"%>
<html lang="en">

<head>
  
     <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"  >
     <link href="vendor/psgisubmit/css/ekyc-layout.css" rel="stylesheet">
     <link href="css/jquery.steps.css" rel="stylesheet">
	<link href="css/style_checkboxs.css" rel="stylesheet">
	<link href="css/style_accord.css" rel="stylesheet">
	<link href="css/select2.css" rel="stylesheet">
	<link rel="stylesheet" href="css/style_toggle.css">
	<link href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap" rel="stylesheet">  
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:400,700'>
   <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css'>
</head>
 

<body id="page-top">
  	<!-- Container Fluid-->
  	<div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		<input type="hidden" id="hdnSessFnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		<div class="col-5">
		          <h5>Product Recommendations</h5>
		     </div>
		<div class="col-md-7">
		           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
			 </div>
        
</div>

	  <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>
	
	<div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh" id="frmSwitchProdForm">
			<div class="card-body" style="font-size: 13px;">
                  <jsp:include page="/views/pages/prodSwitchContent.jsp"></jsp:include>
              
              
                  
             </div>
   </div><!-- page 14 content Area End -->
      
      
  </div>
    <!-- container-fluid-end -->
  
    </body>
        
        <script src="vendor/psgisubmit/js/session_timeout.js"></script>
        <script type="text/javascript" src="vendor/psgisubmit/js/Common Script.js"></script>
          <script src="vendor/jquery-easing/jquery.easing.min.js"></script> 
          <!-- <script src="vendor/psgisubmit/js/ekyc-layout.js"></script> --> 
          <script src="js/jquery.steps.js"></script>
           <script src="js/select2.js"></script>
           <script src="vendor/psgisubmit/js/fna_common.js"></script>
           <script src="vendor/psgisubmit/js/product_recomm_swtchplan.js"></script>
  </html>