<!-- Policy ESubmission Principal List Jsp File content Start-->
<div class="btn-group float-right btn-group mr-2" role="group" aria-label="First group"> 
				     <button type="button" class="btn btn-bs3-prime btn-sm bold"  onclick="validatePolicyEsubDetls();">
				      <i class="fa fa-shield icncolr" aria-hidden="true">
				      </i><sub><i class="fa fa-check icncolr" aria-hidden="true"></i></sub>&nbsp;Policy E-Submission
		    </button>
				<!-- <div class="dropdown">
				    <button type="button" class="btn btn-bs3-prime btn-sm dropdown-toggle" data-toggle="dropdown" style="background:;">
				      Policy E-Submission
				    </button>
				    <div class="dropdown-menu">
				      <h5 class="dropdown-header text-primaryy text-wrap">Select Your Principal below and Click to E-Submit.</h5>
				   <div class="" id="PrinList">
                    <div class=" rounded checkbox-form">
                        <div class="form-check"> <input class="form-check-input  " type="checkbox" value="aviva" id="flexCheckDefault-1"> <label class=" newsletter form-check-label" for="flexCheckDefault-1">  <img src="vendor/psgisubmit/img/aviva.png" alt="Aviva Ltd" width="70%" height="13vh"> </label> </div>
                         
                    </div>
                    <hr>
                    <div class=" rounded checkbox-form">
                        <div class="form-check"> <input class="form-check-input  " type="checkbox" value="manulife" id="flexCheckDefault-2"> <label class=" prospect form-check-label" for="flexCheckDefault-2"> <img src="vendor/psgisubmit/img/Manulife_logo.png" alt="ManuLife" width="70%" height="13vh"> </label> </div>
                          
                    </div>
                    <hr>
                    <div class=" rounded checkbox-form">
                        <div class="form-check"> <input class="form-check-input  " type="checkbox" value="ntuc_income" id="flexCheckDefault-3"> <label class=" event form-check-label" for="flexCheckDefault-3"> <img src="vendor/psgisubmit/img/ntucincome.png" alt="NTUC Income" width="70%" height="13vh"> </label> </div>
                           
                    </div>
                    <hr>
                    <div class=" rounded checkbox-form">
                        <div class="form-check"> <input class="form-check-input  " type="checkbox" value="aia" id="flexCheckDefault-4"> <label class=" customers form-check-label" for="flexCheckDefault-4"> <img src="vendor/psgisubmit/img/aia.jpg" alt="AIA" width="70%" height="13vh"> </label> </div>
                           
                    </div>
                    <hr>
						<div class="row">
	                       	<div class="col-md-3"></div>
	                       
	                       		<div class="col-md-7">
	                       			<button id="btnPolicyESubmit" type="button" class="btn btn-sm btn-bs3-prime">Submit</button>
	                       		</div> 
	                       	</div>
						</div>
				    </div>
              </div> -->
		 </div>
		 
		 <!-- <div class="btn-group float-right btn-group mr-2" role="group" aria-label="Third group">
		 	<button type="button" class="btn btn-bs3-prime btn-sm bold mr-2" onclick="onCaseInfo();"
		    	id="btnCaseInfo">Case Info</button>
		 	<button type="button" class="btn btn-bs3-prime btn-sm bold mr-2" onclick="onNtucService('retrievePolicy');"
		    	id="btnRetrievePolicy">Policy Retrieve</button>
		    <button type="button" class="btn btn-bs3-prime btn-sm bold mr-2" onclick="onNtucService('retrieveFileInfo');"
		    	id="btnRetrieveFileInfo">Policy FileInfo Retrieve</button>
		    	
	    	<div class="dropdown">
			    <button type="button" class="btn btn-bs3-prime btn-sm dropdown-toggle" data-toggle="dropdown">
			    	DSF Update Status
			    </button>
			    <div class="dropdown-menu">
			    	<h5 class="dropdown-header text-primaryy text-wrap">Select the status you want to update in DSF and Click to update!</h5>
					<div id="statusList">
	                    <div class="rounded checkbox-form">
	                        <div class="form-check">
	                        	<input class="form-check-input" type="radio" value="approved" id="flexCheckDefault-1"
	                        		name="dsfStsName">
	                        	<label class="form-check-label" for="flexCheckDefault-1">Approve</label>
	                        </div>
	                    </div>
	                    <hr>
	                    <div class="rounded checkbox-form">
	                        <div class="form-check">
	                        	<input class="form-check-input" type="radio" value="rejected" id="flexCheckDefault-2"
	                        		name="dsfStsName">
	                        	<label class="form-check-label" for="flexCheckDefault-2">Reject</label>
	                        </div>
	                    </div>
	                    <hr>
	                    <div class="rounded checkbox-form">
	                        <div class="form-check">
	                        	<input class="form-check-input" type="radio" value="cancel" id="flexCheckDefault-3"
	                        		name="dsfStsName">
	                        	<label class="form-check-label" for="flexCheckDefault-3">Cancel</label>
	                        </div>
	                    </div>
	                    <hr>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-7">
								<button id="dsfUpdateSts" type="button" class="btn btn-sm btn-bs3-prime"
									onclick="onNtucService('dsfUpdateSts');">Update
								</button>
							</div> 
						</div>
	                </div>
			    </div>
			</div>
		    	
		    <button type="button" class="btn btn-bs3-prime btn-sm bold" data-toggle="dropdown"
		    	onclick="onNtucService('dsfUpdateSts');" id="dsfUpdateSts">DSF Update Status</button>
		 </div> -->
		 
		<!-- Scan to sign and Document Upload Btn Content start --> 
		  <div class="btn-toolbar float-right" role="toolbar" aria-label="Toolbar with button groups">
			 <!--  <div class="btn-group btn-group mr-2" role="group" aria-label="First group">data-toggle="modal" data-target="#ScanToSignModal" 
				      <a class="btn btn-bs3-prime btn-sm " href="#" onclick="allPageValidation();">
								<i class="fa fa-qrcode" aria-hidden="true"></i>&nbsp;Scan to Sign</a>
			  </div> -->
			  
			  <div class="btn-group float-right btn-group mr-2" role="group" aria-label="Error Group">
				<div class="dropdown">
					<!-- <button class="btn btn-sm btn-bs3-prime bold" title="DeleteFnaDets" id="btnDelFnaDets"><i class="fa fa-trash-o"></i>&nbsp;Delete FNA Dets.</button> -->
				    
				    <button type="button" class="btn btn-bs3-prime btn-sm bold" data-toggle="dropdown" onclick="allPageValidation();" id="btntoolbarsign">
				     <i class="fa fa-pencil icncolr" aria-hidden="true"></i>&nbsp;Scan to Sign
				    </button>
				    <div class="dropdown-menu d-none" style="left: -60px; width: 820%;" ><!-- style="left: -60px; width: 820%;" -->
				      <h5 class="dropdown-header text-primaryy">Key in All Required Fields Below :&nbsp;-</h5>
	                         <div class="card m-1">
			                   <div class="card-body p-2">
			                        <div class="row">
			                             <div class="col-4">
				                                <div id="Sec2ContentFormId" style="padding:0px;"  onclick="saveCurrentPageData(this,'kycHome',true,true)">
								                          <div class="inputGroup mb-2 err-fld" id="validation1">
														    <input id="chkSignPersDet" class="checkdb" type="checkbox">
														    <label for="chkSignPersDet" id="lblTxtSz-01"><span><i class="fas fa-fw fa-user"></i>&nbsp;Personal Details</span></label>
								                         </div> 	
								                </div>
								                
								                <div id="Sec2ContentFormId" style="padding:0px;" onclick="saveCurrentPageData(this,'taxResidency',true,true)">
								                          <div class="inputGroup mb-2 err-fld"  id="validation2">
														    <input name="TaxResi" class="checkdb" id="TaxResi" type="checkbox">
														    <label for="TaxResi" id="lblTxtSz-01"><span><i class="fas fa-fw fa-chart-area"></i>&nbsp;Tax Residency</span></label>
								                         </div> 	
								                </div>
								                <div id="Sec2ContentFormId" style="padding:0px;" onclick="saveCurrentPageData(this,'finanWellReview',true,true)">
								                          <div class="inputGroup mb-2 err-fld"  id="validation3">
														    <input name="FinWell" class="checkdb" id="FinWell" type="checkbox">
														    <label for="FinWell" id="lblTxtSz-01"><span><i class="fas fa-fw fa-plus"></i>&nbsp;Financial Wellness Review</span></label>
								                         </div> 	
								                </div>
								                
								             </div>
								             
								             
								            <div class="col-4">
								                
								                <div id="Sec2ContentFormId" style="padding:0px;" onclick="saveCurrentPageData(this,'productRecommend',true,true)">
								                          <div class="inputGroup mb-2 err-fld"  id="validation4">
														    <input name="LifeILPNewPurchase" id="LifeILPNewPurchase" type="checkbox" class="checkdb">
														    <label for="LifeILPNewPurchase" id="lblTxtSz-01"><span><i class="fas fa-fw fa-life-ring"></i>&nbsp;Life &amp; ILP - New Purchase</span></label>
								                         </div> 	
								                </div>
								                
								                 <div id="Sec2ContentFormId" style="padding:0px;" onclick="saveCurrentPageData(this,'productSwitchRecomm',true,true)">
								                          <div class="inputGroup mb-2 err-fld"  id="validation5">
														    <input name="LifeILPSwtch" id="LifeILPSwtch" type="checkbox" class="checkdb">
														    <label for="LifeILPSwtch p-1" id="lblTxtSz-01"><span><i class="fas fa-fw fa-life-ring"></i>&nbsp;Life &amp; ILP - Switching</span></label>
								                         </div> 	
								                </div>
								                
								                
								               <!--   <div id="Sec2ContentFormId" style="padding:0px;">
								                          <div class="inputGroup">
														    <input name="UTAddNew" id="UTAddNew" type="checkbox">
														    <label for="UTAddNew" id="lblTxtSz-01"><span><i class="fas fa-fw fa-line-chart"></i>&nbsp;UT - Add New</span></label>
								                         </div> 	
								                </div>
								                
								                <div id="Sec2ContentFormId" style="padding:0px;">
								                          <div class="inputGroup">
														    <input name="UTSumary" id="UTSumary" type="checkbox">
														    <label for="UTSumary" id="lblTxtSz-01"><span><img src="vendor/avallis/img/job.png">&nbsp;UT - Summary</span></label>
								                         </div> 	
								                  </div> -->
								                  
								            </div>
								                  
								           <div class="col-4">
								                  
								                   <div id="Sec2ContentFormId" style="padding:0px;"  onclick="saveCurrentPageData(this,'adviceBasisRecomm',true,true)">
								                          <div class="inputGroup mb-2 err-fld"  id="validation6">
														    <input name="AdvBscRecomm" id="AdvBscRecomm" type="checkbox" class="checkdb">
														    <label for="AdvBscRecomm" id="lblTxtSz-01"><span><i class="fas fa-fw fa-comments"></i>&nbsp;Advice &amp; Basis of Recomm.</span></label>
								                         </div> 	
								                  </div>
								                  
								                   <div id="Sec2ContentFormId" style="padding:0px;" onclick="saveCurrentPageData(this,'clientDeclaration',true,true)">
								                          <div class="inputGroup mb-2 err-fld" id="validation7">
														    <input name="ClntDecl" id="ClntDecl" type="checkbox" class="checkdb">
														    <label for="ClntDecl" id="lblTxtSz-01"><span><i class="fas fa-fw fa-users"></i>&nbsp;Client Declaration</span></label>
								                         </div> 	
								                  </div>
								                  
								                   <div id="Sec2ContentFormId" style="padding:0px;" onclick="saveCurrentPageData(this,'managerDeclaration',true,true)">
								                          <div class="inputGroup mb-2 err-fld"  id="validation8">
														    <input name="AdvMngrDecl" id="AdvMngrDecl" type="checkbox" class="checkdb">
														    <label for="AdvMngrDecl" id="lblTxtSz-01"><span><i class="fas fa-fw fa-user-secret"></i>&nbsp;Adviser &amp; Manager Decl.</span></label>
								                         </div> 	
								                  </div>
			                             </div>
			                        </div>
			                        
			                   </div>
			             </div> 
				    
				    
				    </div>
              </div>
		 </div>
		 
			  
	          <div class="btn-group btn-group mr-2" role="group" aria-label="Second group">
				   <a id="btnAppUpload" class="btn btn-bs3-prime btn-sm " href="#" data-toggle="modal" data-target="#uplodModal" onclick="getAllUploadedFile();getAllCategoryList();">
						<i class="fa fa-upload" aria-hidden="true"></i>&nbsp;Doc.&nbsp;Uploads</a>
			  </div>
			   
			</div>
			
		<!--Scan to sign and Document Upload Btn Content End   -->
		
		<!--Generate PDF Button start  -->
		 <!-- <div class="btn-group btn-group mr-2 right" role="group" aria-label="Zero Group">
				   <a class="btn btn-bs3-prime btn-sm bold" href="#" onclick="generateFNAPDF();" >
						<i class="fa fa-file-pdf bold" aria-hidden="true"></i>&nbsp;&nbsp;View&nbsp;FNA&nbsp;PDF</a>
	    </div> -->
		<!--Generate PDF Button End  -->	  
			   
		
		
		<!-- view Archive Button start -->
		 <div class="btn-group float-right btn-group mr-2 hide" role="group" aria-label="First group">
			<span class="badge badge-pill badge-warning viwArchvBtnStyle pl-2 pr-2" data-toggle="modal" data-target="#archveListModal"><i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;<span class="pl-1">View Archive :&nbsp; </span></span>	
		 </div>
		 <!--view Archive button end  -->
		 
		 <div  id="curr_msg" style="position: absolute;min-height: 200px;z-index: 1000;background-color: #abcd;right: 0px; border-radius: 20px;display:none">
   
</div>
 <input type="hidden" name="kycSentStatus" id="kycSentStatus"/>
 <input type="hidden" name="adminKycSentStatus" id="adminKycSentStatus"/>
 <input type="hidden" name="compKycSentStatus" id="compKycSentStatus"/>
 <input type="hidden" id="hdnCaseId" value=<% out.print(session.getAttribute("caseId")); %> />
		  
<!-- End Jsp Content -->		 