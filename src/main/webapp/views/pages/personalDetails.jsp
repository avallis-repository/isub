<%@ page language="java" import="java.util.*"%>
<%@page import="com.afas.isubmit.util.PsgConst"%>
<%
ResourceBundle psgprop = ResourceBundle.getBundle("properties/psgisubmit");
%>
<input type="hidden" id="hdnSessDataFormId"
	value="<%=session.getAttribute(PsgConst.SESS_DATAFORM_ID)%>" />
<input type="hidden" id="hdnSessFnaId"
	value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>" />
<!--Personal Details  -->
<!--Tab1  -->
<div class="row">
	<div class="tab-regular" id="clntProdTabs" style="width: 100%;">
		<ul class="nav nav-tabs small" id="myTab" role="tablist"
			style="list-style-type: none;">
			<li class="nav-item" style="list-style-type: none;"><a
				class="nav-link active" id="client1-tab" data-toggle="tab"
				href="#client1" role="tab" aria-controls="client1"
				aria-selected="true"> <%=session.getAttribute("CustomerName")%>
			</a></li>
			<li class="nav-item ml-auto"><a class="btn btn-link"
				href="javascript:void(0)" onclick="javascript:expandAll($(this))"
				id="expandcollape"><i class="fa fa-expand" id="iconStyle-02"
					aria-hidden="true"></i>&nbsp;Expand All</a></li>
			<li class="nav-item" id="btnAddNewInsur"><a class="btn btn-link"
				href="javascript:void(0)" data-toggle="modal"
				data-target="#new2ModalScrollable" id="listStyle-02"
				onclick="AddClient2TabData();"><i class="fa fa-user-plus"
					id="iconStyle-01" aria-hidden="true"></i>&nbsp;Add New Insured</a></li>
		</ul>
		<div class="tab-content" id="myTabContent" style="margin: 15px;">
			<div class="tab-pane fade show active" id="client1" role="tabpanel"
				aria-labelledby="client1-tab">
				<div class="row" id="accordian-row">
					<div class="col-6">
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOne">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapseOne"
											aria-expanded="true" aria-controls="collapseOne"> <i
											class="fa fa-user" style="color: #4caf50;"></i>&nbsp;Personal
											Details
										</a>
									</h4>
								</div>
								<div id="collapseOne" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingOne">
									<div class="panel-body" style="">
										<div class="card">
											<div class="card-body">
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<label class="frm-lable-fntsz" for="dfSelfName">Name(as
																per NRIC)</label> <input id="dfSelfName" name="dfSelfName"
																type="text" class="form-control input-md checkdb"
																maxlength="75">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label class="frm-lable-fntsz" for="dfSelfNric">NRIC/Passport
																No.</label> <input type="text" id="dfSelfNric" name="dfSelfNric"
																class="form-control input-md checkdb" maxlength="15">
														</div>
														<div class="col-md-6">
															<!--  <div class="form-group">
																			       <button type="button" class="btn btn-toggle mt-4" id="btndfSelfGender" data-toggle="button" aria-pressed="false" autocomplete="off">
																						<div class="handle"></div>
																					</button>
																					<input type = "hidden" name="dfSelfGender" id="dfSelfGender">
																			</div> -->
															<div class="row">
																<span id="span_dfSelfGender"></span>
																<div class="col-6">
																	<div id="custTypeRadioBtn" class="mt-3">
																		<input type="radio" name="dfSelfGender"
																			id="radBtnSelfmale" value="M" maxlength="10"
																			class="checkdb dfSelfGendergrp"> <label
																			for="radBtnSelfmale" class="pt-1"
																			style="height: 5vh;"> <img
																			src="vendor/psgisubmit/img/man.png">&nbsp;Male
																		</label>
																	</div>
																</div>
																<div class=" col-6">
																	<div id="custTypeRadioBtn" class="mt-3">
																		<input type="radio" name="dfSelfGender"
																			id="radBtnSelffemale" value="F" maxlength="10"
																			class="checkdb dfSelfGendergrp"> <label
																			for="radBtnSelffemale" class="pt-1"
																			style="height: 5vh;"> <img
																			src="vendor/psgisubmit/img/woman.png">&nbsp;Female
																		</label>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<div class="form-group" id="simple-date1">
																<label class="frm-lable-fntsz" for="dfSelfDob">Date
																	of Birth</label>
																<div class="input-group input-group date">
																	<div class="input-group-prepend">
																		<span class="input-group-text"><i
																			class="fas fa-calendar"></i></span>
																	</div>
																	<input type="text" name="dfSelfDob" id="dfSelfDob"
																		class="form-control checkdb" style="" maxlength="10">
																	<div class="invalid-feedbacks hide" id="dobError"></div>
																</div>
															</div>
															<input type="hidden" name="dfSelfAge" id="dfSelfAge" />
														</div>
														<div class="col-md-6">
															<label class="frm-lable-fntsz" for="name">Marital
																Status</label> <select class="form-control checkdb"
																name="dfSelfMartsts" id="dfSelfMartsts">
																<option value="" selected>--Select--</option>
																<%
																for (String data : psgprop.getString("app.maritalstatus").split("\\^")) {
																%>
																<option value="<%=data%>">
																	<%=data%>
																</option>
																<%
																}
																%>
															</select>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label class="frm-lable-fntsz" for="name">Country
																of Birth</label> <select class="form-control checkdb"
																name="dfSelfBirthCntry" id="dfSelfBirthCntry">
																<option value="" selected>--SELECT--</option>
																<%
																for (String country : psgprop.getString("app.country").split("\\^")) {
																%>
																<option value="<%=country%>">
																	<%=country%>
																</option>
																<%
																}
																%>
															</select>
														</div>
														<div class="col-6">
															<span id="spanSelfNatlyDets"></span> <label
																class="frm-lable-fntsz" for="name">Nationality</label>
															<div class="custom-control custom-radio">
																<input type="radio"
																	class="custom-control-input checkdb dfSelfNationalitygrp"
																	type="radio" name="dfSelfNationality"
																	id="dfSelfNationalitySign" value="SG"
																	onclick="fnaNaltyFlg(this,'dfSelfNatyDets');">
																<label
																	class="custom-control-label  frm-lable-fntsz font-normal"
																	for="dfSelfNationalitySign">Singaporean</label>
															</div>
															<div class="custom-control custom-radio">
																<input type="radio"
																	class="custom-control-input checkdb dfSelfNationalitygrp"
																	type="radio" name="dfSelfNationality"
																	id="dfSelfNationalitySign-PR" value="SG-PR"
																	onclick="fnaNaltyFlg(this,'dfSelfNatyDets')"> <label
																	class="custom-control-label  frm-lable-fntsz font-normal"
																	for="dfSelfNationalitySign-PR"> Singapore - PR</label>
															</div>
															<div class="custom-control custom-radio">
																<input type="radio"
																	class="custom-control-input checkdb dfSelfNationalitygrp"
																	type="radio" name="dfSelfNationality"
																	id="dfSelfNationalityOth" value="Others"
																	onclick="fnaNaltyFlg(this,'dfSelfNatyDets')"> <label
																	class="custom-control-label  frm-lable-fntsz font-normal"
																	for="dfSelfNationalityOth">Others</label>
															</div>
															<div class="form-check ">
																<select class="form-control invisible checkdb"
																	id="dfSelfNatyDets" name="dfSelfNatyDets">
																	<option value="" selected>--SELECT--</option>
																	<%
																	for (String data : psgprop.getString("app.nationalitywithcode").split("\\^")) {
																	%>
																	<option value="<%=data.split(":")[0]%>">
																		<%=data.split(":")[1]%>
																	</option>
																	<%
																	}
																	%>
																</select>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="panel-group" id="accordion_right" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingOneR">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion_right" href="#collapseOneR"
											aria-expanded="true" aria-controls="collapseOneR"> <i
											class="fa fa-money" style="color: #ff9800;"></i>&nbsp;Employment
											&amp; Financial Details
										</a>
									</h4>
								</div>
								<div id="collapseOneR" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingOneR"
									style="margin-bottom: 15px;">
									<div class="panel-body" style="">
										<div class="row">
											<div class="col-6">
												<div class="card ">
													<div class="card-header" id="cardHeaderStyle">
														<span>Employment Info</span>
													</div>
													<div class="card-body">
														<div class="form-group">
															<div class="row">
																<div class="col-md-12">
																	<label class="frm-lable-fntsz" for="dfSelfCompname">Name
																		of Employer</label> <input id="dfSelfCompname"
																		name="dfSelfCompname" type="text"
																		class="form-control input-md checkdb" maxlength="60">
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-md-12">
																	<label class="frm-lable-fntsz" for="dfSelfOccpn">Occupation</label>
																	<input id="dfSelfOccpn" name="dfSelfOccpn" type="text"
																		class="form-control input-md checkdb" maxlength="60">
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-md-12">
																	<label class="frm-lable-fntsz" for="name">Estd.Annual
																		Income</label>
																	<div class="input-group ">
																		<div class="input-group-prepend">
																			<span class="input-group-text">$</span>
																		</div>
																		<input type="text" name="dfSelfAnnlIncome"
																			id="dfSelfAnnlIncome" class="form-control checkdb"
																			aria-label="Amount (to the nearest dollar)"
																			maxlength="15">
																		<div class="input-group-prepend">
																			<span class="input-group-text">.00</span>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-md-12">
																	<label class="frm-lable-fntsz" for="dfSelfBusinatr">Nature
																		of Business</label> <select class="form-control checkdb"
																		name="dfSelfBusinatr" id="dfSelfBusinatr"
																		onchange="valiBusNatOthers(this)">
																		<option value="" selected>--SELECT--</option>
																		<%
																		for (String data : psgprop.getString("app.natureofbusiness").split("\\^")) {
																		%>
																		<option value="<%=data%>">
																			<%=data%>
																		</option>
																		<%
																		}
																		%>
																	</select>
																</div>
															</div>
														</div>
														<div class="form-group invisible" id="dfSelfBusNatOthSec">
															<div class="row">
																<div class="col-md-12">
																	<label class="frm-lable-fntsz" for="dfSelfBusinatrDets">Other
																		Detls.</label> <input id="dfSelfBusinatrDets"
																		name="dfSelfBusinatrDets" type="text"
																		class="form-control checkMand input-md checkdb"
																		maxlength="150">
																	<div class="invalid-feedbacks invisible"
																		id="dfSelfBusinatrDetsError">keyin Other Details</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="card">
													<div class="card-header" id="cardHeaderStyle">
														<span>Source of Fund</span>
													</div>
													<div class="card-body">
														<ul class="list-group" id="ulLiStyle-01">
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input type="checkbox"
																		class="custom-control-input checkdb chkCDClntFundSrcgrp"
																		name="chkCDSelfErndIncm" id="chkCDSelfErndIncm"
																		data="CERN" value=""
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="chkCDSelfErndIncm">&nbsp;&nbsp;Earned
																		Income</label> <input type="hidden" name="htfcderndicn" />
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input type="checkbox"
																		class="custom-control-input checkdb chkCDClntFundSrcgrp"
																		name="chkCDSelfInvIncm" id="chkCDSelfInvIncm"
																		data="CINV" value=""
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="chkCDSelfInvIncm">&nbsp;&nbsp;Investment</label>
																	<input type="hidden" name="htfcdinvsticn" />
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input type="checkbox"
																		class="custom-control-input checkdb chkCDClntFundSrcgrp"
																		name="chkCDSelfPrsnlIncm" id="chkCDSelfPrsnlIncm"
																		data="CPRS" value=""
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="chkCDSelfPrsnlIncm">&nbsp;&nbsp;Savings</label> <input
																		type="hidden" name="htfcdpersicn" />
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input type="checkbox"
																		class="custom-control-input checkdb chkCDClntFundSrcgrp"
																		name="chkCDSelfCPFSvngs" id="chkCDSelfCPFSvngs"
																		data="CCPF" value=""
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="chkCDSelfCPFSvngs">&nbsp;&nbsp;CPF
																		Savings</label> <input type="hidden" name="htfcdcpficn" />
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input type="checkbox"
																		class="custom-control-input checkdb chkCDClntFundSrcgrp"
																		name="chkCDSelfSrcIncmOthrs"
																		id="chkCDSelfSrcIncmOthrs" data="COTH" value=""
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSelfFundsrc');chkSelfSpsOthrValid(this,'dfSelfFundsrcDets');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="chkCDSelfSrcIncmOthrs">&nbsp;&nbsp;Others</label>
																	<input type="hidden" name="htfcothicn" />
																	<textarea name="dfSelfFundsrcDets"
																		id="dfSelfFundsrcDets"
																		class="form-control txtarea-hrlines checkdb" rows="2"
																		maxlength="50" onkeydown="textCounter(this,50);"
																		onkeyup="textCounter(this,50);" disabled></textarea>
																	<div
																		class="font-sz-level7 mt-2 text-primaryy char-count"></div>
																	<input type="hidden" name="txtFldCDSelfSrcIncmOthDet"
																		id="txtFldCDSelfSrcIncmOthDet" /> <input
																		type="hidden" name="dfSelfFundsrc" id="dfSelfFundsrc" />
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row" id="accordian-row">
					<div class="col-6">
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwo">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapseTwo"
											aria-expanded="false" aria-controls="collapseTwo"> <i
											class="fa fa-map-marker" style="color: #f33;"></i>&nbsp;Address
											Details
										</a>
									</h4>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingTwo">
									<div class="panel-body" style="">
										<div class="card">
											<div class="card-body">
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<label class="frm-lable-fntsz" for="dfSelfHomeAddr">Registered
																Residential Address</label> <input id="dfSelfHomeAddr"
																name="dfSelfHomeAddr" type="text"
																class="form-control input-md checkdb"
																onblur="enableSelfSpsSameAddrFlg('dfSelfHomeAddr','self')"
																maxlength="300">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<div class="custom-control custom-checkbox">
																<input type="checkbox"
																	class="custom-control-input checkdb"
																	name="dfSelfSameAsregadd" id="chkSelfSameAddrFlg"
																	onclick="sameAsResiAddr(this,'chkSelfNoSameAddrFlg')"
																	value="N" maxlength="20"> <label
																	class="custom-control-label frm-lable-fntsz"
																	for="chkSelfSameAddrFlg">Mailing Address is
																	same as Residential Address</label>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<div class="custom-control custom-checkbox">
																<input type="checkbox"
																	class="custom-control-input checkdb"
																	name="dfSelfMailaddrflg" id="chkSelfNoSameAddrFlg"
																	onclick="NotsameAsResiAddr(this,'chkSelfSameAddrFlg')"
																	value="N" maxlength="20"> <label
																	class="custom-control-label frm-lable-fntsz"
																	for="chkSelfNoSameAddrFlg">If different from
																	Registered Residential Address. </label>
															</div>
														</div>
													</div>
													<div class="row pl-3 ">
														<div class="col- ">
															<img src="vendor/psgisubmit/img/infor.png" class="mt-2">
														</div>
														<div class="col-11">
															<div class="col-">
																<small
																	class="font-italic bold text-primaryy font-sz-level8">Please
																	submit a proof of address issued within the last 6
																	months(e.g.) Bank/Utility/Phone bill statement.</small>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<input name="dfSelfMailAddr" id="dfSelfMailAddr"
																type="text" class="form-control input-md checkdb"
																maxlength="300">
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingTwoR">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion_right" href="#collapseTwoR"
											aria-expanded="false" aria-controls="collapseTwoR"> <i
											class="fa fa-graduation-cap" style="color: #01579b;"></i>&nbsp;Education
											Details
										</a>
									</h4>
								</div>
								<div id="collapseTwoR" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingTwoR">
									<div class="panel-body">
										<div class="row">
											<div class="col-6">
												<div class="card">
													<div class="card-header" id="cardHeaderStyle">
														<span>Highest Education Qualification</span>
													</div>
													<div class="card-body">
														<span id="span_dfSelfEduLevel"></span>
														<ul class="list-group" id="ulLiStyle-01">
															<li class="list-group-item">
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input dfSelfEduLevelGrp checkdb"
																		name="dfSelfEduLevel" id="chkClntHPRIMARY"
																		value="PRIMARY"> <label
																		class="custom-control-label frm-lable-fntsz"
																		for="chkClntHPRIMARY"> Primary</label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input dfSelfEduLevelGrp checkdb"
																		name="dfSelfEduLevel" id="chkClntHSECONDARY"
																		value="SECONDARY"> <label
																		class="custom-control-label frm-lable-fntsz"
																		for="chkClntHSECONDARY"> Secondary</label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input dfSelfEduLevelGrp checkdb"
																		name="dfSelfEduLevel" id="chkClntHPRETER"
																		value="PRETER"> <label
																		class="custom-control-label frm-lable-fntsz"
																		for="chkClntHPRETER"> Pre-Tertiary</label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input dfSelfEduLevelGrp checkdb"
																		name="dfSelfEduLevel" id="chkClntHEQTERABV"
																		value="EQTERABV"> <label
																		class="custom-control-label frm-lable-fntsz"
																		for="chkClntHEQTERABV"> Tertiary and above</label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input dfSelfEduLevelGrp checkdb"
																		name="dfSelfEduLevel" id="chkClntHEQGCE" value="EQGCE">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="chkClntHEQGCE"> GCE 'N' or 'O Level or
																		Equivalent Academic Qualification</label>
																</div> <!-- <input type="hidden" name="dfSelfEdulevel" id="dfSelfEdulevel"> -->
															</li>
														</ul>
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="card">
													<div class="card-header" id="cardHeaderStyle">
														<span>Language Proficiency</span>
													</div>
													<div class="card-body">
														<span id="span_dfSelfEngSpoken"></span> <span
															id="span_dfSelfEngWritten"></span>
														<ul class="list-group" id="ulLiStyle-01">
															<li class="list-group-item">
																<div class="frm-lable-fntsz bold"
																	style="font-weight: 700;">Spoken English</div>
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input checkdb"
																		id="dfSelfEngSpokenY" name="dfSelfEngSpoken" value="Y">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="dfSelfEngSpokenY" style="text-align: left;">
																		Proficient </label>
																</div>
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input checkdb"
																		id="dfSelfEngSpokenN" name="dfSelfEngSpoken" value="N">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="dfSelfEngSpokenN" style="text-align: left;">
																		<span class="txt-urline" style="font-weight: 500;">
																			Not</span> Proficient
																	</label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="frm-lable-fntsz bold"
																	style="font-weight: 700;">Written English</div>
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input checkdb"
																		id="dfSelfEngWrittenY" name="dfSelfEngWritten"
																		value="Y"> <label
																		class="custom-control-label frm-lable-fntsz"
																		for="dfSelfEngWrittenY" style="text-align: left;">
																		Proficient </label>
																</div>
																<div class="custom-control custom-radio">
																	<input type="radio"
																		class="custom-control-input checkdb"
																		id="dfSelfEngWrittenN" name="dfSelfEngWritten"
																		value="N"> <label
																		class="custom-control-label frm-lable-fntsz"
																		for="dfSelfEngWrittenN" style="text-align: left;"><span
																		class="txt-urline" style="font-weight: 500;">
																			Not</span> Proficient </label>
																</div>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row" id="accordian-row">
					<div class="col-6">
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingThree">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion" href="#collapseThree"
											aria-expanded="false" aria-controls="collapseThree"> <i
											class="fa fa-phone-square" style="color: #028686;"></i>&nbsp;Contact
											Details
										</a>
									</h4>
								</div>
								<div id="collapseThree" class="panel-collapse collapse"
									role="tabpanel" aria-labelledby="headingThree">
									<div class="panel-body">
										<div class="card">
											<div class="card-body">
												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label class="frm-lable-fntsz" for="dfSelfMobile">Mobile</label>
															<input id="dfSelfMobile" name="dfSelfMobile" type="text"
																class="form-control input-md checkdb dfSelfMobilegrp"
																maxlength="60">
														</div>
														<div class="col-md-6">
															<label class="frm-lable-fntsz" for="dfSelfHome">Home</label>
															<input id="dfSelfHome" name="dfSelfHome" type="text"
																class="form-control input-md checkdb dfSelfMobilegrp"
																maxlength="60">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-6">
															<label class="frm-lable-fntsz" for="dfSelfOffice">Office</label>
															<input id="dfSelfOffice" name="dfSelfOffice" type="text"
																class="form-control input-md checkdb dfSelfMobilegrp"
																maxlength="60">
														</div>
														<div class="col-md-6">
															<label class="frm-lable-fntsz" for="dfSelfPersEmail">Email
																Address</label> <input id="dfSelfPersEmail"
																name="dfSelfPersEmail" type="email"
																class="form-control input-md"
																onblur="validEmailId(this,'dfSelfPersEmail')"
																maxlength="60">
															<div class="invalid-feedbacks hide"
																id="dfSelfPersEmailError"></div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-6">
						<div class="panel-group" id="accordion" role="tablist"
							aria-multiselectable="true">
							<div class="panel panel-default">
								<div class="panel-heading" role="tab" id="headingIntrPreTrust">
									<h4 class="panel-title">
										<a class="collapsed" role="button" data-toggle="collapse"
											data-parent="#accordion_right"
											href="#headingInterpreterDetls" aria-expanded="false"
											aria-controls="headingInterpreterDetls"> <i
											class="fa fa-user" style="color: #388e3c;"></i>&nbsp;Interpreter
											/ Trusted Individual Details
										</a>
									</h4>
								</div>
								<div id="headingInterpreterDetls"
									class="panel-collapse collapse" role="tabpanel"
									aria-labelledby="headingIntrPreTrust">
									<div class="panel-body">
										<div class="row">
											<div class="col-6">
												<div class="card ">
													<div class="card-header" id="cardHeaderStyle">
														<div class="row">
															<div class="col-md-9">
																<div class="custom-control custom-checkbox">
																	<input type="checkbox"
																		class="custom-control-input checkdb"
																		onclick="fnaAssgnFlg(this,'chk');enableIntrprt(this)"
																		id="cdIntrprtflg" name="cdIntrprtflg" value="N">
																	<label class="custom-control-label pt-1"
																		for="cdIntrprtflg">Any Interpreter</label>
																</div>
															</div>
															<div class="col-md-3">
																<span class="badge badge-pill badge-warning mt-1"
																	style="margin: 0px 0px 0px -3em;">Note :&nbsp;<i
																	class="fa fa-commenting" aria-hidden="true"
																	data-toggle="popoverPersDetls" data-trigger="hover"
																	data-original-title="" title=""></i></span>
															</div>
														</div>
													</div>
													<div class="card-body" id="tempIntprtDiv">
														<div class="row pl-1 ">
															<div class="col-1">
																<img src="vendor/psgisubmit/img/infor.png" class="mt-2">
															</div>
															<div class="col-10">
																<small
																	class="font-italic bold text-primaryy font-sz-55prcnt">
																	(If Any Interpreter / Trusted Individual presented then
																	key-in the following details)</small>
															</div>
														</div>
														<div class="">
															<form action="">
																<div class="form-group">
																	<label class="frm-lable-fntsz" for="intprtName"
																		style="margin-bottom: 0px;">Name</label> <input
																		type="text" class="form-control checkdb checkMand"
																		name="intprtName" id="intprtName" disabled="true"
																		maxlength="60" onchange="validateFld(this)">
																	<div class="invalid-feedbacks hide"
																		id="intprtNameError">Keyin Name</div>
																</div>
																<div class="form-group">
																	<label class="frm-lable-fntsz" for="intprtContact"
																		style="margin-bottom: 0px;">Contact Number</label> <input
																		type="text" class="form-control checkdb checkMand"
																		name="intprtContact" id="intprtContact"
																		disabled="true" maxlength="20"
																		onchange="validateFld(this)">
																	<div class="invalid-feedbacks hide"
																		id="intprtContactError">Keyin Contact Number</div>
																</div>
																<div class="form-group">
																	<label class="frm-lable-fntsz" for=""
																		style="margin-bottom: 0px;">Relationship</label> <select
																		class="form-control checkdb checkMand"
																		id="intprtRelat" name="intprtRelat" disabled="true"
																		maxlength="60" onchange="validateFld(this)">
																		<option value="" selected="selected">--Select--</option>
																		<%
																		for (String relationship : psgprop.getString("app.relationship").split("\\^")) {
																		%>
																		<option value="<%=relationship%>">
																			<%=relationship%>
																		</option>
																		<%
																		}
																		%>
																	</select>
																	<div class="invalid-feedbacks hide"
																		id="intprtRelatError">Select the Relationship</div>
																</div>
															</form>
														</div>
													</div>
												</div>
											</div>
											<div class="col-6">
												<div class="card ">
													<div class="card-header" id="cardHeaderStyle">
														<span>Language Use</span>
													</div>
													<div class="card-body">
														<ul class="list-group selfInptrLangSec" id="ulLiStyle-01">
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input class="custom-control-input kycLanguage checkdb"
																		type="checkbox" name="htflaneng" id="htflaneng"
																		value="Y" data="ENG"
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="htflaneng"> English </label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input class="custom-control-input kycLanguage checkdb"
																		type="checkbox" name="htflanman" id="htflanman"
																		value="Y" data="MAN"
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="htflanman">Mandarin </label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input class="custom-control-input kycLanguage checkdb"
																		type="checkbox" name="htflanmalay" id="htflanmalay"
																		value="Y" data="MAL"
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="htflanmalay">Malay </label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input class="custom-control-input kycLanguage checkdb"
																		type="checkbox" name="htflantamil" id="htflantamil"
																		value="Y" data="TAM"
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="htflantamil">Tamil </label>
																</div>
															</li>
															<li class="list-group-item">
																<div class="custom-control custom-checkbox">
																	<input class="custom-control-input kycLanguage checkdb"
																		type="checkbox" name="htflanoth" id="htflanoth"
																		value="Y" data="OTH"
																		onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'cdLanguages');chkSelfSpsOthrValid(this,'cdLanguageOth');">
																	<label class="custom-control-label frm-lable-fntsz"
																		for="htflanoth">Others </label>
																	<textarea id="cdLanguageOth" name="cdLanguageOth"
																		disabled class="form-control checkdb" rows="2"
																		maxlength="150" onkeydown="textCounter(this,150);"
																		onkeyup="textCounter(this,150);"></textarea>
																	<div
																		class="font-sz-level7 mt-2 text-primaryy char-count"></div>
																	<input type="hidden" name="cdLanguages"
																		id="cdLanguages" />
																</div>
															</li>
														</ul>
														<div class="invalid-feedbacks hide"
															id="selfInptrLangSecError">Atleast Select any one
															of the Language in Interpreter Language Section</div>
													</div>
												</div>
											</div>
										</div>
										<br>
										<div class="row">
											<div class="col-12">
												<div class="card">
													<div class="card-body">
														<div class="list-group" id="listgroupbg-01">
															<a href="#"
																class="list-group-item list-group-item-action flex-column align-items-start">
																<div class="custom-control custom-checkbox">
																	<input type="checkbox"
																		class="custom-control-input checkdb"
																		name="cdBenfownflg" id="cdBenfownflg"
																		onclick="openOthInptrSec(this,'BENOWNER')"
																		maxlength="1" value="N"> <label
																		class="custom-control-label bold font-sz-level6 pt-1"
																		for="cdBenfownflg">Beneficial Owner</label> <small
																		class="pl-4"><button type="button"
																			class="btn btn-link p-0 font-sz-level6"
																			data-toggle="modal" data-target="#txtAreaModal1">
																			<i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;View
																		</button></small>
																</div>
																<p class="mb-1 italic font-sz-level7">Is someone
																	else expected to participate in, make decisions about,
																	or benefit from this policy in any way? (This does not
																	include Account holder/Policyholder, Proposed Life
																	Insured, Payer, Beneficiary or signing officer.)</p>
															</a> <a href="#"
																class="list-group-item list-group-item-action flex-column align-items-start">
																<div class="d-flex w-100 ">
																	<div class="d-flex w-100 ">
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox"
																				class="custom-control-input checkdb" name="cdTppflg"
																				id="cdTppflg" onclick="openOthInptrSec(this,'TPP')"
																				maxlength="1" value="N"> <label
																				class="custom-control-label bold font-sz-level6 pt-1"
																				for="cdTppflg">Third Party Payer</label> <small
																				class="pl-3"><button type="button"
																					class="btn btn-link p-0 font-sz-level6"
																					data-toggle="modal" data-target="#txtAreaModal2">
																					<i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;View
																				</button></small>
																		</div>
																	</div>
																</div>
																<p class="mb-1 italic font-sz-level7">Is anyone
																	other than the Account holder/Policyholder be paying
																	for this policy / Investment account?</p>
															</a> <a href="#"
																class="list-group-item list-group-item-action flex-column align-items-start">
																<div class="d-flex w-100">
																	<div class="custom-control custom-checkbox">
																		<input type="checkbox"
																			class="custom-control-input checkdb" name="cdPepflg"
																			id="cdPepflg" onclick="openOthInptrSec(this,'PEP')"
																			maxlength="1" value="N"> <label
																			class="custom-control-label bold font-sz-level6 pt-1"
																			for="cdPepflg">PEP and RCA</label> <small
																			class="pl-5"><button type="button"
																				class="btn btn-link p-0 font-sz-level6"
																				data-toggle="modal" data-target="#txtAreaModal3">
																				<i class="fa fa-external-link" aria-hidden="true"></i>&nbsp;View
																			</button></small>
																	</div>
																</div>
																<p class="mb-1 italic font-sz-level7">Are you or any
																	close relative(s) currently or previously held a senior
																	position in a government, political party, military,
																	tribunal or government-owned corporation?.</p>
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--End Tab1  -->
<!--Tab2 Elements  -->
<div id="divSpouseDetsElemsParent">
	<div class="d-none" id="divSpouseDetsElems">
		<div class="row" id="accordian-row">
			<div class="col-6">
				<div class="panel-group" id="accordion" role="tablist"
					aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOne">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse"
									data-parent="#accordion" href="#collapseOneSps"
									aria-expanded="true" aria-controls="collapseOneSps"> <i
									class="fa fa-user" style="color: #4caf50;"></i>&nbsp;Personal
									Details
								</a>
							</h4>
						</div>
						<div id="collapseOneSps" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="headingOne">
							<div class="panel-body" style="">
								<div class="card">
									<div class="card-body">
										<div class="form-group">
											<div class="row">
												<div class="col-12">
													<label class="frm-lable-fntsz" for="dfSpsName">Name(as
														per NRIC)</label>
													<div class="input-group">
														<input id="dfSpsName" name="dfSpsName" type="text"
															class="form-control checkdb input-md spsTabChange"
															maxlength="75" onchange="changeSpsTabName(this)">
														<div class="input-group-prepend">
															<span class="input-group-text bg-white"
																style="border-top-right-radius: 6px; border-bottom-right-radius: 6px;"><i
																class="fa fa-pencil fa-1x text-primary px-1"
																title="Edit Client(2) Name" aria-hidden="true"
																onclick="enableEditSpsNameFld()"></i></span>
														</div>
													</div>
													<div class="invalid-feedbacks hide" id="dfSpsNameError">Keyin
														Spouse / Client(2) Name</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label class="frm-lable-fntsz" for="dfSpsNric">NRIC/Passport
														No.</label> <input type="text" id="dfSpsNric" name="dfSpsNric"
														class="form-control input-md checkdb" maxlength="15">
												</div>
												<div class="col-md-6">
													<!--   <div class="form-group">
																			       <button type="button" class="btn btn-toggle mt-4" id="btndfSpsGender" data-toggle="button" aria-pressed="false" autocomplete="off">
																						<div class="handle"></div>
																					</button>
																					<input type = "hidden" name="dfSpsGender" id="dfSpsGender">
																			</div> -->
													<div class="row">
														<div class="col-6">
															<div id="custTypeRadioBtn" class="mt-3">
																<input type="radio" name="dfSpsGender"
																	id="radBtnSpsmale" value="M" maxlength="10"
																	class="checkdb dfSpsGendergrp"> <label
																	for="radBtnSpsmale" class="pt-1" style="height: 5vh;">
																	<img src="vendor/psgisubmit/img/man.png">&nbsp;Male
																</label>
															</div>
														</div>
														<div class=" col-6">
															<div id="custTypeRadioBtn" class="mt-3">
																<input type="radio" name="dfSpsGender"
																	id="radBtnSpsfemale" value="F" maxlength="10"
																	class="checkdb dfSpsGendergrp"> <label
																	for="radBtnSpsfemale" class="pt-1" style="height: 5vh;">
																	<img src="vendor/psgisubmit/img/woman.png">&nbsp;Female
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="form-group" id="simple-date2">
														<label class="frm-lable-fntsz" for="dfSpsDob">Date
															of Birth</label>
														<div class="input-group date">
															<div class="input-group-prepend">
																<span class="input-group-text"><i
																	class="fas fa-calendar"></i></span>
															</div>
															<input type="text" name="dfSpsDob" id="dfSpsDob"
																class="form-control checkdb" style="" maxlength="10">
														</div>
													</div>
													<input type="hidden" name="dfSpsAge" id="dfSpsAge" />
												</div>
												<div class="col-md-6">
													<label class="frm-lable-fntsz" for="name">Marital
														Status</label> <select class="form-control checkdb"
														name="dfSpsMartsts" id="dfSpsMartsts">
														<option value="" selected>--Select--</option>
														<%
														for (String data : psgprop.getString("app.maritalstatus").split("\\^")) {
														%>
														<option value="<%=data%>">
															<%=data%>
														</option>
														<%
														}
														%>
													</select>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label class="frm-lable-fntsz" for="name">Country
														of Birth</label> <select class="form-control checkdb"
														name="dfSpsBirthCntry" id="dfSpsBirthCntry">
														<option value="" selected>--SELECT--</option>
														<%
														for (String country : psgprop.getString("app.country").split("\\^")) {
														%>
														<option value="<%=country%>">
															<%=country%>
														</option>
														<%
														}
														%>
													</select>
												</div>
												<div class="col-6">
													<span id="spanSpsNatlyDets"></span> <label
														class="frm-lable-fntsz" for="name">Nationality</label>
													<div class="custom-control custom-radio">
														<input type="radio"
															class="custom-control-input checkdb dfSpsNationalitygrp"
															type="radio" name="dfSpsNationality"
															id="dfSpsNationalitySign" value="SG"
															onclick="fnaNaltyFlg(this,'dfSpsNatyDets')"> <label
															class="custom-control-label  frm-lable-fntsz font-normal"
															for="dfSpsNationalitySign">Singaporean</label>
													</div>
													<div class="custom-control custom-radio">
														<input type="radio"
															class="custom-control-input checkdb dfSpsNationalitygrp"
															type="radio" name="dfSpsNationality"
															id="dfSpsNationalitySign-PR" value="SG-PR"
															onclick="fnaNaltyFlg(this,'dfSpsNatyDets')"> <label
															class="custom-control-label  frm-lable-fntsz font-normal"
															for="dfSpsNationalitySign-PR"> Singapore - PR</label>
													</div>
													<div class="custom-control custom-radio">
														<input type="radio"
															class="custom-control-input checkdb dfSpsNationalitygrp"
															type="radio" name="dfSpsNationality"
															id="dfSpsNationalityOth" value="Others"
															onclick="fnaNaltyFlg(this,'dfSpsNatyDets')"> <label
															class="custom-control-label  frm-lable-fntsz font-normal"
															for="dfSpsNationalityOth">Others</label>
													</div>
													<div class="form-check ">
														<select class="form-control invisible checkdb"
															id="dfSpsNatyDets" name="dfSpsNatyDets">
															<option value="" selected>--SELECT--</option>
															<%
															for (String data : psgprop.getString("app.nationalitywithcode").split("\\^")) {
															%>
															<option value="<%=data.split(":")[0]%>">
																<%=data.split(":")[1]%>
															</option>
															<%
															}
															%>
														</select>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="panel-group" id="accordion_right" role="tablist"
					aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingOneR">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse"
									data-parent="#accordion_right" href="#collapseOneSpsR"
									aria-expanded="true" aria-controls="collapseOneSpsR"> <i
									class="fa fa-money" style="color: #ff9800;"></i>&nbsp;Employment
									&amp; Financial Details
								</a>
							</h4>
						</div>
						<div id="collapseOneSpsR" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="headingOneR"
							style="margin-bottom: 15px;">
							<div class="panel-body">
								<div class="row">
									<div class="col-6">
										<div class="card ">
											<div class="card-header" id="cardHeaderStyle">
												<span>Employment Info</span>
											</div>
											<div class="card-body">
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<label class="frm-lable-fntsz" for="dfSelfCompname">Name
																of Employer</label> <input id="dfSpsCompname"
																name="dfSpsCompname" type="text"
																class="form-control input-md checkdb" maxlength="60">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<label class="frm-lable-fntsz" for="dfSpsOccpn">Occupation</label>
															<input id="dfSpsOccpn" name="dfSpsOccpn" type="text"
																class="form-control input-md checkdb" maxlength="60">
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<label class="frm-lable-fntsz" for="name">Estd.Annual
																Income</label>
															<div class="input-group ">
																<div class="input-group-prepend">
																	<span class="input-group-text">$</span>
																</div>
																<input type="text" name="dfSpsAnnlIncome"
																	id="dfSpsAnnlIncome" class="form-control checkdb"
																	aria-label="Amount (to the nearest dollar)"
																	maxlength="15">
																<div class="input-group-prepend">
																	<span class="input-group-text">.00</span>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-12">
															<label class="frm-lable-fntsz" for="dfSpsBusinatr">Nature
																of Business</label> <select class="form-control checkdb"
																name="dfSpsBusinatr" id="dfSpsBusinatr"
																onchange="valiBusNatOthers(this)" maxlength="150">
																<option value="" selected>--SELECT--</option>
																<%
																for (String data : psgprop.getString("app.natureofbusiness").split("\\^")) {
																%>
																<option value="<%=data%>">
																	<%=data%>
																</option>
																<%
																}
																%>
															</select>
														</div>
													</div>
												</div>
												<div class="form-group invisible" id="dfSpsBusNatOthSec">
													<div class="row">
														<div class="col-md-12">
															<label class="frm-lable-fntsz" for="">Other
																Detls.</label> <input id="dfSpsBusinatrDets"
																name="dfSpsBusinatrDets" type="text"
																class="form-control checkMand input-md checkdb"
																maxlength="150">
															<div class="invalid-feedbacks invisible"
																id="dfSpsBusinatrDetsError">keyin Other Details</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-6">
										<div class="card">
											<div class="card-header" id="cardHeaderStyle">
												<span>Source of Fund</span>
											</div>
											<div class="card-body">
												<ul class="list-group" id="ulLiStyle-01">
													<li class="list-group-item">
														<div class="custom-control custom-checkbox">
															<input type="checkbox"
																class="custom-control-input checkdb spsSrcFundgrp"
																name="chkCDSpsErndIncm" id="chkCDSpsErndIncm"
																data="CERN" value=""
																onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc')">
															<label class="custom-control-label frm-lable-fntsz"
																for="chkCDSpsErndIncm">&nbsp;&nbsp;Earned Income</label>
															<input type="hidden" name="htfcderndicn" />
														</div>
													</li>
													<li class="list-group-item">
														<div class="custom-control custom-checkbox">
															<input type="checkbox"
																class="custom-control-input checkdb spsSrcFundgrp"
																name="chkCDSpsInvIncm" id="chkCDSpsInvIncm" data="CINV"
																value=""
																onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc');">
															<label class="custom-control-label frm-lable-fntsz"
																for="chkCDSpsInvIncm">&nbsp;&nbsp;Investment</label> <input
																type="hidden" name="htfcdinvsticn" />
														</div>
													</li>
													<li class="list-group-item">
														<div class="custom-control custom-checkbox">
															<input type="checkbox"
																class="custom-control-input checkdb spsSrcFundgrp"
																name="chkCDSpsPrsnlIncm" id="chkCDSpsPrsnlIncm"
																data="CPRS" value=""
																onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc');">
															<label class="custom-control-label frm-lable-fntsz"
																for="chkCDSpsPrsnlIncm">&nbsp;&nbsp;Savings</label> <input
																type="hidden" name="htfcdpersicn" />
														</div>
													</li>
													<li class="list-group-item">
														<div class="custom-control custom-checkbox">
															<input type="checkbox"
																class="custom-control-input checkdb spsSrcFundgrp"
																name="chkCDSpsCPFSvngs" id="chkCDSpsCPFSvngs"
																data="CCPF" value=""
																onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc');">
															<label class="custom-control-label frm-lable-fntsz"
																for="chkCDSpsCPFSvngs">&nbsp;&nbsp;CPF Savings</label> <input
																type="hidden" name="htfcdcpficn" />
														</div>
													</li>
													<li class="list-group-item">
														<div class="custom-control custom-checkbox">
															<input type="checkbox"
																class="custom-control-input checkdb spsSrcFundgrp"
																name="chkCDSpsSrcIncmOthrs" id="chkCDSpsSrcIncmOthrs"
																data="COTH" value=""
																onclick="fnaAssgnFlg(this,'chk');chkJsnOptions(this,'dfSpsFundsrc');chkSelfSpsOthrValid(this,'dfSpsFundsrcDets');">
															<label class="custom-control-label frm-lable-fntsz"
																for="chkCDSpsSrcIncmOthrs">&nbsp;&nbsp;Others</label> <input
																type="hidden" name="htfcothicn" />
															<textarea name="dfSpsFundsrcDets" id="dfSpsFundsrcDets"
																class="form-control checkdb txtarea-hrlines" rows="2"
																maxlength="50" onkeydown="textCounter(this,50);"
																onkeyup="textCounter(this,50);" disabled></textarea>
															<div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
															<input type="hidden" name="txtFldCDSpsSrcIncmOthDet"
																id="txtFldCDSpsSrcIncmOthDet" /> <input type="hidden"
																name="dfSpsFundsrc" id="dfSpsFundsrc" />
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="accordian-row">
			<div class="col-6">
				<div class="panel-group" id="accordion" role="tablist"
					aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwo">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse"
									data-parent="#accordion" href="#collapseTwoSps"
									aria-expanded="false" aria-controls="collapseTwoSps"> <i
									class="fa fa-map-marker" style="color: #f33;"></i>&nbsp;Address
									Details
								</a>
							</h4>
						</div>
						<div id="collapseTwoSps" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="headingTwo">
							<div class="panel-body" style="">
								<div class="card">
									<div class="card-body">
										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<label class="frm-lable-fntsz" for="dfSpsHomeAddr">Registered
														Residential Address</label> <input id="dfSpsHomeAddr"
														name="dfSpsHomeAddr" type="text"
														class="form-control input-md checkdb"
														onblur="enableSelfSpsSameAddrFlg('dfSpsHomeAddr','spouse')">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<div class="custom-control custom-checkbox">
														<input type="checkbox"
															class="custom-control-input checkdb"
															name="chkSpsSameAddrFlg" id="chkSpsSameAddrFlg"
															onclick="sameAsResiAddr(this,'chkSpsNoSameAddrFlg')"
															value="" maxlength="20"> <label
															class="custom-control-label frm-lable-fntsz"
															for="chkSpsSameAddrFlg">Mailing Address is same
															as Residential Address</label>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<div class="custom-control custom-checkbox">
														<input type="checkbox"
															class="custom-control-input checkdb"
															name="dfSpsMailAddrFlg" id="chkSpsNoSameAddrFlg"
															onclick="NotsameAsResiAddr(this,'chkSpsSameAddrFlg')"
															value="" maxlength="20"> <label
															class="custom-control-label frm-lable-fntsz"
															for="chkSpsNoSameAddrFlg">If different from
															Registered Residential Address.</label>
													</div>
												</div>
											</div>
											<div class="row pl-3 ">
												<div class="col- ">
													<img src="vendor/psgisubmit/img/infor.png" class="mt-2">
												</div>
												<div class="col-11">
													<div class="col-">
														<small
															class="font-italic bold text-primaryy font-sz-level8">Please
															submit a proof of address issued within the last 6
															months(e.g.) Bank/Utility/Phone bill statement.</small>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-12">
													<input name="dfSpsMailAddr" id="dfSpsMailAddr" type="text"
														class="form-control input-md checkdb" maxlength="300">
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-6">
				<div class="panel-group" id="accordion" role="tablist"
					aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingTwoR">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse"
									data-parent="#accordion_right" href="#collapseTwoSpsR"
									aria-expanded="false" aria-controls="collapseTwoSpsR"> <i
									class="fa fa-graduation-cap" style="color: #01579b;"></i>&nbsp;Education
									Details
								</a>
							</h4>
						</div>
						<div id="collapseTwoSpsR" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="headingTwoR">
							<div class="panel-body">
								<div class="row">
									<div class="col-6">
										<div class="card" id="divSelfEduLevel">
											<div class="card-header" id="cardHeaderStyle">
												<span>Highest Education Qualification</span>
											</div>
											<div class="card-body">
												<ul class="list-group" id="ulLiStyle-01">
													<li class="list-group-item">
														<div class="custom-control custom-radio">
															<input type="radio"
																class="custom-control-input checkdb dfSpsEduLevelGrp"
																name="dfSpsEduLevel" id="chkSpsHPRIMARY" value="PRIMARY">
															<label class="custom-control-label frm-lable-fntsz"
																for="chkSpsHPRIMARY"> Primary</label>
														</div>
													</li>
													<li class="list-group-item">
														<div class="custom-control custom-radio">
															<input type="radio"
																class="custom-control-input checkdb dfSpsEduLevelGrp"
																name="dfSpsEduLevel" id="chkSpsHSECONDARY"
																value="SECONDARY"> <label
																class="custom-control-label frm-lable-fntsz"
																for="chkSpsHSECONDARY"> Secondary</label>
														</div>
													</li>
													<li class="list-group-item">
														<div class="custom-control custom-radio">
															<input type="radio"
																class="custom-control-input checkdb dfSpsEduLevelGrp"
																name="dfSpsEduLevel" id="chkSpsHPRETER" value="PRETER">
															<label class="custom-control-label frm-lable-fntsz"
																for="chkSpsHPRETER"> Pre-Tertiary</label>
														</div>
													</li>
													<li class="list-group-item">
														<div class="custom-control custom-radio">
															<input type="radio"
																class="custom-control-input checkdb dfSpsEduLevelGrp"
																name="dfSpsEduLevel" id="chkSpsHEQTERABV"
																value="EQTERABV"> <label
																class="custom-control-label frm-lable-fntsz"
																for="chkSpsHEQTERABV"> Tertiary and above</label>
														</div>
													</li>
													<li class="list-group-item">
														<div class="custom-control custom-radio">
															<input type="radio"
																class="custom-control-input checkdb dfSpsEduLevelGrp"
																name="dfSpsEduLevel" id="chkSpsHEQGCE" value="EQGCE">
															<label class="custom-control-label frm-lable-fntsz"
																for="chkSpsHEQGCE"> GCE 'N' or 'O Level or
																Equivalent Academic Qualification</label>
														</div> <input type="hidden" name="dfSpsEdulevel"
														id="dfSpsEdulevel">
													</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="col-6">
										<div class="card">
											<div class="card-header" id="cardHeaderStyle">
												<span>Language Proficiency</span>
											</div>
											<div class="card-body">
												<span id="spanDfSpsLangProf"></span> <span
													id="spanDfSpsLangProfWrite"></span>
												<ul class="list-group" id="ulLiStyle-01">
													<li class="list-group-item">
														<div class="frm-lable-fntsz bold"
															style="font-weight: 700;">Spoken English</div>
														<div class="custom-control custom-radio">
															<input type="radio" class="custom-control-input checkdb"
																id="dfSpsEngSpokenY" name="dfSpsEngSpoken" value="Y">
															<label class="custom-control-label frm-lable-fntsz"
																for="dfSpsEngSpokenY" style="text-align: left;">
																Proficient </label>
														</div>
														<div class="custom-control custom-radio">
															<input type="radio" class="custom-control-input checkdb"
																id="dfSpsEngSpokenN" name="dfSpsEngSpoken" value="N">
															<label class="custom-control-label frm-lable-fntsz"
																for="dfSpsEngSpokenN" style="text-align: left;">
																<span class="txt-urline" style="font-weight: 500;">
																	Not</span> Proficient
															</label>
														</div>
													</li>
													<li class="list-group-item">
														<div class="frm-lable-fntsz bold"
															style="font-weight: 700;">Written English</div>
														<div class="custom-control custom-radio">
															<input type="radio" class="custom-control-input checkdb"
																id="dfSpsEngWrittenY" name="dfSpsEngWritten" value="Y">
															<label class="custom-control-label frm-lable-fntsz"
																for="dfSpsEngWrittenY" style="text-align: left;">
																Proficient </label>
														</div>
														<div class="custom-control custom-radio">
															<input type="radio" class="custom-control-input checkdb"
																id="dfSpsEngWrittenN" name="dfSpsEngWritten" value="N">
															<label class="custom-control-label frm-lable-fntsz"
																for="dfSpsEngWrittenN" style="text-align: left;"><span
																class="txt-urline" style="font-weight: 500;"> Not</span>
																Proficient </label>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="accordian-row">
			<div class="col-6">
				<div class="panel-group" id="accordion" role="tablist"
					aria-multiselectable="true">
					<div class="panel panel-default">
						<div class="panel-heading" role="tab" id="headingThree">
							<h4 class="panel-title">
								<a class="collapsed" role="button" data-toggle="collapse"
									data-parent="#accordion" href="#collapseThree"
									aria-expanded="false" aria-controls="collapseThree"> <i
									class="fa fa-phone-square" style="color: #028686;"></i>&nbsp;Contact
									Details
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="panel-collapse collapse"
							role="tabpanel" aria-labelledby="headingThree">
							<div class="panel-body">
								<div class="card">
									<div class="card-body">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label class="frm-lable-fntsz" for="dfSelfMobile">Mobile</label>
													<input id="dfSpsHp" name="dfSpsHp" type="text"
														class="form-control input-md checkdb dfSpsHpgrp"
														maxlength="60">
												</div>
												<div class="col-md-6">
													<label class="frm-lable-fntsz" for="dfSelfHome">Home</label>
													<input id="dfSpsHome" name="dfSpsHome" type="text"
														class="form-control checkdb input-md dfSpsHpgrp"
														maxlength="60">
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label class="frm-lable-fntsz" for="dfSpsOffice">Office</label>
													<input id="dfSpsOffice" name="dfSpsOffice" type="text"
														class="form-control checkdb input-md dfSpsHpgrp"
														maxlength="60">
												</div>
												<div class="col-md-6">
													<label class="frm-lable-fntsz" for="dfSpsPersEmail">Email
														Address</label> <input id="dfSpsPersEmail" name="dfSpsPersEmail"
														type="email" class="form-control input-md checkdb"
														onblur="validEmailId(this,'dfSpsPersEmail')"
														maxlength="300">
													<div class="invalid-feedbacks hide"
														id="dfSpsPersEmailError"></div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>