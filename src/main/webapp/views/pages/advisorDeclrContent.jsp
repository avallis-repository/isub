<div class="row">
                      <div class="col-md-6">
                           <div class="card  card-content-fnsize">
                           
                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
										        <div class="row">
										            <div class="col-md-11">
												         <span> Read Below Details:- </span>
											       </div>
										          
										        </div>
										        
										       
							</div>
                              <div class="card-body">
                                    <div class="row">
                                         <div class="col-md-12">
                                         
                                         <div class="row pl-2  ">
		                                       <div class="col- "><img src="vendor/psgisubmit/img/infor.png" class="mt-2"></div>
		                                            <div class="col-11">
			                                             <div class="col-">
			                                                  <span class="font-sz-level6 ln-hei">I / We understand that the recommendation(s) made in this "Financial Needs Analysis" Form is / are based on the facts
furnished by me / us; any incomplete or inaccurate information provided by me / us may affect the suitability of the advice
given and product recommendation(s). If I/we choose not to disclose any requested information nor completely accept
the Representative's recommendation(s), I / we are responsible to ensure the suitability of the selected product(s) and the
arrangement made for me / us.</span>
			                                              </div>
		                                           </div>
		                                 </div>
		                                                                                                   
                                           
                                         </div>
                                       </div>  
                                         
                                         <div class="row mt-2">
                                         <div class="col-md-12">
                                         	
                                         	<div class="card-body">
                                         	<span id="span_cdackAgree"></span> 
                                         <ul class="list-group mt-2">
		                                     <li class="list-group-item bg-hlght">
		                                       <span class="italic bold text-primaryy font-sz-level6"> Select any <span class="txt-urline  bold">ONE</span> Option Below :-</span>
		                                         <div id="Sec2ContentFormId" style="padding:0px;">
							                          <div class="inputGroup">
													    <input name="cdackAgree" class="checkdb" id="radFldackAgree" type="radio" value="agree" onclick="chkAgreeDisagreeFld(this,'cdackAdvrecomm');" maxlength="20">
													    <label for="radFldackAgree" id="lblTxtSz-01"><span><span class="text-success bold">Accept</span> the proposed recommendation(s).</span></label>
													  </div>
													  
													  <div class="inputGroup">
													    <input name="cdackAgree" class="checkdb" id="radFldackPartial" type="radio" value="partial" onclick="chkAgreeDisagreeFld(this,'cdackAdvrecomm');" maxlength="20">
													    <label for="radFldackPartial" id="lblTxtSz-01"><span><span class="text-danger bold">Partially Accept </span> the representative recommendation(s) but <br> wish  to deviate with our amended specification.</span></label>
													  </div>
                                             </div> 
		                                      </li>
                                         </ul>	
                                         	</div>
                                         
                                        </div></div>
                                         
                                         <div class="row mt-2">
								             <div class="col-md-12">
								               <small><i> <strong>Note: </strong> (If <span class=" text-danger bold font-sz-level8">Partially Accept</span> , please state the reasons below)</i></small>
								            </div>
								         </div>
                                         
                                        <div class="row mt-2">
                                             <div class="col-md-12">
                                                    <div class="form-group">
								                          <label class="frm-lable-fntsz">Client's Choice / Remarks on Representative's Recommendation(s):</label>
									                      <textarea class="form-control txtarea-hrlines text-wrap checkdb" name="cdackAdvrecomm" id="cdackAdvrecomm" maxlength="300" onkeydown="textCounter(this,300);" onkeyup="textCounter(this,300);" rows="5"></textarea>
							                               <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
							                        </div> 
                                             </div>
                                        </div>
                                        
                                        <div class="row">
                                         <div class="col-md-12">
                                         <span id="span_cdConsentApprch"></span>
                                         <div class="vard-body">
                                         
                                              <ul class="list-group mt-2">
		                                          <li class="list-group-item bg-hlght"><span class="italic bold text-primaryy font-sz-level6 "> I / We acknowledge and consent to the approach used in conducting my/our financial advisory process :</span>
		                                               <div class="row">
		                                                   
				                                                 <div class="col-md-6">
						                                               <div id="Sec2ContentFormId" style="padding:0px;">
													                          <div class="inputGroup">
																			    <input id="cdConsentApprchF" class="checkdb" name="cdConsentApprch" type="radio" value="F2F" maxlength="20" onclick="consentFld();"/>
																			    <label for="cdConsentApprchF" id="lblTxtSz-01"><span class="bold text-custom-color-gp"><img src="vendor/psgisubmit/img/facetoface.png">&nbsp;Face to Face </span></label>
																			  </div>
																	  </div>    
				                                               </div> 
				                                           
				                                               <div class="col-md-6">
				                                                     <div id="Sec2ContentFormId" style="padding:0px;">
											                              <div class="inputGroup">
																	          <input id="cdConsentApprchNF" class="checkdb" name="cdConsentApprch" type="radio" value="NONF2F" maxlength="20" onclick="consentFld();"/>
																	           <label for="cdConsentApprchNF" id="lblTxtSz-01"><span class="bold text-custom-color-gp"><img src="vendor/psgisubmit/img/nonfacetoface.png">&nbsp;Non Face to Face  </span></label>
																	     </div>
				                                                    </div>    
				                                              </div> 
				                                            
                                           
                                                      </div>
		                                          
		                                          
		                                          </li>
		                                     </ul>
                                        
                                         </div>
                                                 
                                              
                                               
                                         </div></div>
                                         
                                        
                                             
                                             <div class="row">
                                             <div class="col-md-12">
                                             
                                             
                                             <ul class="list-group">
  <li class="list-group-item ln-hei">
        <ul>
            <li class="mt-1 "><span class="font-sz-level6">Where Investment-Linked policy / Collective Investment Schemes and participating plans are concerned, I / We understand and agree that:</span>

                  <ul class="mt-2">
                     <li class="mt-1"><span class="font-sz-level7">Life Insurance products are long-term commitments and any early termination may result in a cash value that is less than the total premiums paid.</span></li>
                     <li class="mt-1"><span class="font-sz-level7">The bonus rates used in the Benefit Illustrations of the participating plans of life insurance policies are non-guaranteed.The actual benefits payable vary accordingly based on the future performance of the Fund(s).</span></li>
                     <li class="mt-1"><span class="font-sz-level7">Past performance is not indicative of future performance. Investments are volatile and the value of underlying assets may fluctuate from time to time.</span></li>
                     <li class="mt-1"><span class="font-sz-level7">Association of Financial Advisers Pte Ltd and its Representative are not held responsible in any way whatsoever for the future performance of the Investment-Linked funds / Collective Investment Schemes and / or participating plans that I / We have chosen</span></li>
                  </ul>
            
            </li>
            
            <li class="mt-2"><span class="font-sz-level6">My / Our Representative has presented to me / us on the recommended product's sales documents and has fully explained to me / us on the listed features, benefits, risk factors, charges, warning, limitations and free-look period or cancellation period for unit trusts.</span></li>
            <li class="mt-1"><span class="font-sz-level6">I / We understand that my / our Representative has used his / her best effort to recommend products to suit my / our financial needs and / or Investment objectives. I / We hereby declare that any decision I / we make in relation to purchasing any product is that of my / our own judgment.</span></li>
            <li class="mt-1"><span class="font-sz-level6">I / We understand that my / our Representative do not provide any legal or tax related advice and my / our Representative has encouraged me / us to seek professionals with the appropriate field of expertise where necessary.</span></li>
            <li class="mt-1"><span class="font-sz-level6">I / We understand that a copy of the completed Financial Needs Analysis Form will be provided to me / us later.</span></li>
        </ul>
  </li>
  
</ul>
    
                                         
                                    </div>
                              </div>
                             
                                  
                               </div><!--row end  -->
                       </div><!-- card-body end -->
                  </div><!--card-end  -->
                 <!-- col-6 end -->
                  
                  <div class="col-md-6">
                           <div class="card  card-content-fnsize">
                           
                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
										        <div class="row">
										            <div class="col-md-11">
												         <span class="font-sz-level5"> Use of Personal Data &amp; Marketing Materials - Cons. by the Client(s) </span>
											       </div>
										          
										        </div>
										        
										       
							</div>
                              <div class="card-body">
                                    <div class="row">
                                      
                                </div>
                                
                                <div class="row">
                                     <div class="col-md-12">
                                     
                                      
                                     <ul class="list-group">
  <li class="list-group-item ln-hei">
     
      <span class="bold text-primaryy font-sz-level6"><i>I / We agree, authorise and give consent to Association of Financial Advisers Pte Ltd, its group of employees, representatives, business partners
                                            and service providers for the following:</i></span>
    
  <ul class="mt-1">
       <li class="mt-1"><span class="font-sz-level6 mt-1">To collect, use and/or disclose my / our personal data to issue and administer my / our existing and/or new policy(ies)/
account(s), including the processing of my / our personal data for account maintenance and transaction purposes; for
statistical, compliance, audit and regulatory purposes.</span></li> 
       <li class="mt-1"><span class="font-sz-level6 mt-1">To use and/or disclose my / our personal data to third party service providers, suppliers agents, distributors, fund
managers or intermediaries.</span></li>
       <li class="mt-1"><span class="font-sz-level6 mt-1">To provide marketing, advertising and promotional information, materials and documents relating to the Life and
Health Insurance product(s) / Investment product(s) and services that Association of Financial Advisers Pte Ltd will be marketing,
promoting or offering including the update of the same ("Marketing Materials") relevant to my /our needs via Phone Call
/ Voice Call and SMS / MMS (Text Messages). (collectively, the "Purposes").</span></li>
  </ul>
  
</li>


</ul>




 <ul class="list-group mt-2">
		  <li class="list-group-item bg-hlght">
		    <span class=" bold italic text-primaryy font-sz-level6">By ticking the following box(es), I / we wish to Opt out of receiving the Marketing Materials from Association of Financial Advisers Pte Ltd via :</span>
		          
		           <div class="row">
       
         <div class="col-md-6 ">
         <div id="Sec2ContentFormId" >
			<div class="inputGroup">
				         <input name="cdMrktmatPostalflg" id="cdMrktmatPostalflg" class="checkdb" type="checkbox" onclick="fnaAssgnFlg(this,'chk')" value="y">
						<label for="cdMrktmatPostalflg"><span><span> <img src="vendor/psgisubmit/img/postal.png"> Postal Mail </span> </span></label>
			 </div>
         </div>
         
         
         </div>
         <div class="col-md-6 ">
         <div id="Sec2ContentFormId" >
			  <div class="inputGroup">
					<input onclick="fnaAssgnFlg(this,'chk')" value="Y" class="checkdb" id="cdMrktmatEmailflg" name="cdMrktmatEmailflg" type="checkbox">
					 <label for="cdMrktmatEmailflg" id="lblTxtSz-01"><span> <span><img src="vendor/psgisubmit/img/mail.png"> Electronic Mail</span> </span></label>
			 </div>
         </div>
        
         </div>
          
    </div>
		  
		 
		 
		  </li>
 </ul>
     

                                         <div class="list-group list-group-root well mt-2">
  
 <ul class="list-group">
  <li class="list-group-item ln-hei"><span class=" bold font-sz-level6 text-primaryy">I / We acknowledge and agree that:</span>
     <ul class="mt-1">
         <li class="mt-1"><span class="font-sz-level6">The personal data may be withdrawn at any time by giving reasonable notice to Association of Financial Advisers Pte Ltd via AFAS
Hotline at 6220-5333 or e-mail to dpo@afas.org. I will stop receiving Marketing Materials via the selected modes of
communication after 30 days.</span></li>
         <li class="mt-1"><span class="font-sz-level6"> will continue to receive Marketing Materials via other modes of communication where my consent has been given
and information arising from my policy(ies).</span></li>
         <li class="mt-1"><span class="font-sz-level6">If I / we withdrawn consent for Association of Financial Advisers Pte Ltd to collect,use, process and disclose the personal data in relation
to my/ our insurance / investment policy(ies), it may affect the Company's ability or prevent the Company from keeping
my / our insurance / Investment polic(ies) inforce or supplying the services to me/ us. In such a case, I / we agree to
bear all losses resulting from the withdrawal of such consent.</span></li>
         <li class="mt-1"><span class="font-sz-level6">The withdrawal of consent for sending me/ us Marketing Materials will not impact Association of Financial Advisers Pte Ltd's ability or
prevent Association of Financial Advisers Pte Ltd from keeping my / our insurance policy(ies) inforce or supplying the services to me / us.
I / We will give reasonable notice to Association of Financial Advisers Pte Ltd if I / we wish to withdraw consent for Marketing Materials
or change communication mode to receive them.</span></li>
         <li class="mt-1"><span class="font-sz-level6">I / we have read, understood and agree to observe the clauses contained in this form and Association of Financial Advisers Pte Ltd's
Data Protection Policy which is available at www.afas.org.sg.</span></li>
     </ul>
     
     </li>
</ul>
  
  
  
</div>
                                     
                                     
                                     </div>
                                </div>
                        </div>
                    </div>
              
              </div>
             
                  
             </div>