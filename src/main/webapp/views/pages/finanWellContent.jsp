<%@ page language="java" import="java.util.*" %>
<%@ page language="java" import="com.afas.isubmit.util.PsgConst" %>

<%
	ResourceBundle psgprop =ResourceBundle.getBundle("properties/psgisubmit");
%>
<div id="frmDepntFinWell">
			<input type="hidden" id="expdId" name="expdId"/>
			<input type="hidden" id="hdnSessFnaId" name="hdnSessFnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
			
				<div class="row">
					<div class="col-md-12">
						<div class="card  card-frmLabel-fnsize">

							<div class="card-header" id="cardHeaderStyle-22"
								style="font-weight: normal;">
								<div class="row">
									<div class="col-11">
										<span> (i) Dependents</span>
									</div>
									<div class="col-1">
									     <div class="custom-control custom-checkbox">
											<input type="checkbox" id="fwrDepntflg"
												   name="fwrDepntflg" class="custom-control-input checkdb"
												   onclick="fnaAssgnFlg(this,'chk')"> <label
												   class="custom-control-label badge pt-4"
												   for="fwrDepntflg"><a data-toggle="collapse"
												  data-parent="#accordionDepend" href="#collapseDepend"></a>
										     </label>
										</div>
									</div>
								</div>


							</div>
							<div class="card-body">


								<div class="row">
									<div class="col-md-12">
										<form class="bs-example" action="">
											<div class="panel-group" id="accordionDepend">
												<div class="panel panel-default">
													<div class="panel-heading">
														<div class="row">
															<div class="col-11">
																<span class="panel-title font-sz-level5">Do you
																	want to include your <span class="text-info bold">Dependent(s)</span>
																	for your Needs Analysis and Product Recommendations? If<span
																	class="text-success bold"> Yes</span> 
																	 please fill in the details below :-
																</span>
															</div>

															<div class="col-1">
																 <span class="badge badge-pill badge-warning"
																		style="padding: .25em .4em;">Note :&nbsp;<i
																		class="fa fa-commenting" aria-hidden="true"
																		data-toggle="pg4DependNote" data-trigger="hover"
																		></i>
											                    </span>

															</div>

														</div>


													</div>
													<div id="collapseDepend"
														class="panel-collapse collapse in hide">
														<div class="panel-body">
															<div class="row mt-2">
																<div class="col-md-5">
																	<div class="card">
																		<div class="card-body">
                                                                             <div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="depName" class="frm-lable-fntsz">Name<sup style="color: red;"
																							class="mandatoryStar">*</sup></label> <input type="text"
																							class="form-control depnchkMand checkdb" placeholder="" id="depName"
																							name="depnName" maxlength="60">
																							
																							 <div
																							id="depnNameError" class="invalid-feedbacks hide"><small>Please
																								enter dependent name</small></div>
																					</div>
																				</div>
																			</div>

						


                                                                                 <div class="row">
																				<div class="col-md-7">
																					<div class="row">
																						    <div class="col-6">
																							      <div id="custTypeRadioBtn" class="mt-3">
																									  <input type="radio" name="depnGender" id="radBtnDepnmale" value="Male" class="checkdb">
																									  <label for="radBtnDepnmale" class="pt-1" style="height:5vh;">
																									  <img src="vendor/psgisubmit/img/man.png">&nbsp;Male
																									  </label>
																				                 </div>
																							</div>
																							
																							<div class=" col-6">
																							      <div id="custTypeRadioBtn" class="mt-3">
																									  <input type="radio" name="depnGender" id="radBtnDepnfemale" value="Female" class="checkdb">
																									  <label for="radBtnDepnfemale" class="pt-1" style="height:5vh;">
																									  <img src="vendor/psgisubmit/img/woman.png">&nbsp;Female
																									  </label>
																							     </div>
																							</div>
																				</div>
																					
																					
                                                                                </div>            
 
                                                                                       

																				<div class="col-md-2">
																					<div class="form-group">
																						<label for="pwd" class="frm-lable-fntsz">Age</label>
																						
																							<input type="text" class="form-control checkdb"
																								aria-label="" id="depnAge" name="depnAge"
																								onkeypress="return isNumber(event,'ageErrMsg')" maxlength="3">
																							<div id="ageErrMsg" class="invalid-feedbacks hide"><small>Please
																									enter valid number</small></div>

																					
																					</div>
																				</div>

																				<div class="col-md-3">
																					<div class="form-group">
																						<label for="pwd" class="frm-lable-fntsz">Yrs.Support</label>
																						

																							<input type="text" class="form-control checkdb"
																								aria-label="" id="depnYrsSupport"
																								name="depnYrsSupport" id="depnYrsSupport"
																								onkeypress="return isNumber(event,'yrsErrMsg')" maxlength="3">
																							<div id="yrsErrMsg" class="invalid-feedbacks hide"><small>Please
																									enter valid number</small></div>
																					
																					</div>
																				</div>

																			</div>



																			<div class="row">

																				<div class="col-md-6">
																					<div class="form-group">
																						<label for="sel1" class="frm-lable-fntsz">Relationship</label>
																						<select class="form-control custom-select checkdb"
																							id="depnRelationShip" name="depnRelationShip">
																							<option value="" selected>--Select--</option>
																							<%
																								for (String data : psgprop.getString("app.relationship").split("\\^")) {
																							%>
																							<option value="<%=data%>"><%=data%></option>
																							<%
																								}
																							%>

																						</select>
																					</div>
																				</div>

																				<div class="col-md-6">
																					<div class="form-group">
																						<label for="pwd" class="frm-lable-fntsz">Occupation</label>
																						<input type="text" class="form-control checkdb"
																							placeholder="" id="depnOccp" name="depnOccp" maxlength="60">
																						<input type="hidden" name="depnId" id="depnId" />
																						<input type="hidden" name="depnMonthcontr" id="depnMonthcontr"/>
																						<input type="hidden" name="createdBy"/>
																						<input type="hidden" name="createdDate"/>
																					    <input type="hidden" name="hdnMode" id="hdnMode" value="I"/>
																					</div>
																				</div>
																			</div>


																			<div class="row">
																				
																				<div class="col-12" id="">
																					<input type="button"
																						class="btn btn-sm btn-success btnpadfntsz"
																						onclick="saveDependantData();" value="Add Details" id="btnSaveDepnData" style="width: 7rem"/>
																					
																					
																					 <input type="button"
																						class="btn btn-sm btn-secondary btnpadfntsz"
																						onclick="clearData();" value="Clear Detls." />
																						
																					<!--  <input type="button"
																						class="btn btn-sm btn-primary btnpadfntsz hide" id="btnEdtDepnData"
																						 value="Edit Detls">	 -->


																				</div>
																			</div>


																		</div>

																	</div>

																</div>


																<div class="col-md-7">
																	<div class="card">
																		<div class="card-body" style="height: 65vh;overflow-y: scroll;overflow-x: hidden;">
																		
																		    <div class="row NoDepnDetlsRow hide" id="NoDepnDetlsRow">
													                                <div class="col-2"></div>
													                                <div class="col-8">
													                                 <img src="vendor/psgisubmit/img/dependent.gif" class="rounded mx-auto d-block img-fluid  prod-img" id="DepnImg" style="width: 80%;height: 27vh;" >
													                                 <p class="center noData" id="NoDepnInfo"><img src="vendor/psgisubmit/img/warning.png" class="mr-3">&nbsp;No Dependent Details Available&nbsp;</p>
													                                </div>
													                                
													                                <div class="col-2"></div>
													                        </div>
													                           
																			<div class="row">

																				<div class="list-group" id="addlist"></div>

																			</div>


																		</div>
																	</div>

																</div>




															</div>
														</div>
													</div>
												</div>

											</div>
										</form>


									</div>


								</div>







							</div>
						</div>

					</div>

				</div>



				<div class="row">
					<div class="col-md-12">
						<div class="card card-frmLabel-fnsize" style="margin-top: 15px;">

							<div class="card-header" id="cardHeaderStyle-22"
								style="font-weight: normal;">
								<div class="row">
									<div class="col-11">
										<span>(ii) Financial Situation and Commitments - Cash
											Flow &amp; Assets and liabilities</span>

									</div>
									<div class="col-1">
									     <div class="custom-control custom-checkbox">
											 <input type="checkbox" id="fwrFinassetflg"
													name='fwrFinassetflg' class="custom-control-input checkdb"
													onclick="fnaAssgnFlg(this,'chk')"> <label
													class="custom-control-label badge pt-3"
													for="fwrFinassetflg"><a data-toggle="collapse"
													data-parent="#accordioncashFlow"
													href="#collapsecashFlow"></a> </label>
										</div>
									</div>
								</div>
							</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-12">
										<form class="bs-example" action="">
											<div class="panel-group" id="accordioncashFlow">
												<div class="panel panel-default">
													<div class="panel-heading">

														<div class="row">
															<div class="col-11">
																<span class="panel-title font-sz-level5">Do you
																	want to include your <span class="text-info bold">Cash
																		Flow</span> for your Needs Analysis and Product
																	Recommendations? If<span class="text-success bold">
																		Yes</span> please fill in the details below :-
																</span>
															</div>

															<div class="col-1">
																<span class="badge badge-pill badge-warning"
											                         style="padding: .25em .4em;">Note :&nbsp;<i
											                         class="fa fa-commenting" aria-hidden="true"
											                         data-toggle="pg4CashFlwNote" data-trigger="hover"></i>
										                       </span>
															</div>
														</div>


													</div>
													<div id="collapsecashFlow"
														class="panel-collapse collapse in hide">
														<div class="panel-body">

															<div class="row mt-2">
																<div class="col-md-4">
																	<div class="card ">

																		<div class="card-header" id="cardHeaderStyle"
																			style="font-weight: normal;">
																			<div class="row">
																				<div class="col-md-12">
																					<span><img
																						src="vendor/psgisubmit/img/client1.png">&nbsp;Client
																						(1) </span>

																				</div>

																			</div>
																		</div>
																		<div class="card-body">
																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Total
																							Annual Income</label>

																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" id="cfSelfAnnlinc"
																								name="cfSelfAnnlinc"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this);calcCombined(this)"
																								class="form-control checkdb"
																								aria-label="Amount (to the nearest dollar)"
																								onkeypress="return isNumber(event,'cfSelfAnnlincErrMsg')"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfSelfAnnlincErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>

																					</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">CPF
																							Contribution(Employee)</label>
																						<div class="input-group ">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								name="cfSelfCpfcontib" id="cfSelfCpfcontib"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this);calcCombined(this)"
																								aria-label="Amount (to the nearest dollar)"
																								onkeypress="return isNumber(event,'cfSelfCpfcontibErrMsg')"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfSelfCpfcontibErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																					</div>
																				</div>

																			</div>

																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Total
																							Annual Estimated Expenses</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								name="cfSelfEstexpense" id="cfSelfEstexpense"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this);calcCombined(this)"
																								aria-label="Amount (to the nearest dollar)"
																								onkeypress="return isNumber(event,'cfSelfEstexpenseErrMsg')"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						
																						<div id="cfSelfEstexpenseErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																					</div>
																				</div>

																			</div>

																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Surplus
																							/ Deficit</label>
																						<div class="input-group ">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								name="cfSelfSurpdef" id="cfSelfSurpdef"
																								onpaste="PasteOnlyNum(this,4);"
																								aria-label="Amount (to the nearest dollar)"
																								onkeypress="return isNumber(event,'cfSelfSurpdefErrMsg')"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfSelfSurpdefErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																						
																					</div>
																				</div>

																			</div>

																		</div>

																	</div>
																</div>


																<!-- client 2 -->


																<div class="col-md-4">
																	<div class="card ">

																		<div class="card-header" id="cardHeaderStyle"
																			style="font-weight: normal;">
																			<div class="row">
																				<div class="col-md-12">
																					<span><img
																						src="vendor/psgisubmit/img/client2.png">&nbsp;Client
																						(2) </span>

																				</div>

																			</div>
																		</div>
																		<div class="card-body">


																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Total
																							Annual Income</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								name="cfSpsAnnlinc" id="cfSpsAnnlinc"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this);calcCombined(this)"
																								aria-label="Amount (to the nearest dollar)"
																								onkeypress="return isNumber(event,'cfSpsAnnlincErrMsg')"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfSpsAnnlincErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																					</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">CPF
																							Contribution(Employee)</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								name="cfSpsCpfcontib"
																								onkeypress="return isNumber(event,'cfSpsCpfcontibErrMsg')"
																								id="cfSpsCpfcontib"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this);calcCombined(this)"
																								aria-label="Amount (to the nearest dollar)"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfSpsCpfcontibErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																					</div>
																				</div>

																			</div>

																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Total
																							Annual Estimated Expenses</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								onkeypress="return isNumber(event,'cfSpsEstexpenseErrMsg')"
																								name="cfSpsEstexpense" id="cfSpsEstexpense"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this);calcCombined(this)"
																								aria-label="Amount (to the nearest dollar)"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfSpsEstexpenseErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																					</div>
																				</div>

																			</div>

																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Surplus
																							/ Deficit</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								onkeypress="return isNumber(event,'cfSpsSurpdefErrMsg')"
																								name="cfSpsSurpdef" id="cfSpsSurpdef"
																								onpaste="PasteOnlyNum(this,4);"
																								aria-label="Amount (to the nearest dollar)"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfSpsSurpdefErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																						
																					</div>
																				</div>

																			</div>

																		</div>

																	</div>
																</div>

																<!--combined  -->

																<div class="col-md-4">
																	<div class="card ">

																		<div class="card-header" id="cardHeaderStyle"
																			style="font-weight: normal;">
																			<div class="row">
																				<div class="col-md-12">
																					<span> <img
																						src="vendor/psgisubmit/img/joint.png">&nbsp;Combined
																					</span>

																				</div>

																			</div>
																		</div>
																		<div class="card-body">


																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Total
																							Annual Income</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								onkeypress="return isNumber(event,'cfFamilyAnnlincErrMsg')"
																								name="cfFamilyAnnlinc" id="cfFamilyAnnlinc"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this)"
																								aria-label="Amount (to the nearest dollar)"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						
																						<div id="cfFamilyAnnlincErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																						
																					</div>
																				</div>
																			</div>
																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">CPF
																							Contribution (Employee)</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								onkeypress="return isNumber(event,'cfFamilyCpfcontibErrMsg')"
																								name="cfFamilyCpfcontib" id="cfFamilyCpfcontib"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this)"
																								aria-label="Amount (to the nearest dollar)"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																						
																						</div>
																							<div id="cfFamilyCpfcontibErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																					</div>
																				</div>

																			</div>

																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Total
																							Annual Estimated Expenses</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								onkeypress="return isNumber(event,'cfFamilyEstexpenseErrMsg')"
																								name="cfFamilyEstexpense"
																								id="cfFamilyEstexpense"
																								onpaste="PasteOnlyNum(this,4);"
																								onchange="calcFinCommit(this)"
																								aria-label="Amount (to the nearest dollar)"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfFamilyEstexpenseErrMsg"
																								class="invalid-feedbacks hide">Please
																									enter valid number</div>
																					</div>
																				</div>

																			</div>

																			<div class="row">
																				<div class="col-md-12">
																					<div class="form-group">
																						<label for="email" class="frm-lable-fntsz">Surplus
																							/ Deficit</label>
																						<div class="input-group">
																							<div class="input-group-prepend">
																								<span class="input-group-text">$</span>
																							</div>
																							<input type="text" class="form-control checkdb"
																								onkeypress="return isNumber(event,'cfFamilySurpdefErrMsg')"
																								name="cfFamilySurpdef" id="cfFamilySurpdef"
																								onpaste="PasteOnlyNum(this,4);"
																								aria-label="Amount (to the nearest dollar)"
																								maxlength="15">
																							<div class="input-group-prepend">
																								<span class="input-group-text">.00</span>
																							</div>
																							
																						</div>
																						<div id="cfFamilySurpdefErrMsg" class="invalid-feedbacks hide">Please
																									enter valid number</div>
																					</div>
																				</div>

																			</div>

																		</div>

																	</div>
																</div>


															</div>
															
															<!--asset and Liability title content start  -->
															    <div class="row mt-2">
														<div class="col-11">
															<span class="panel-title font-sz-level5">Do you
																want to include your <span class="text-info bold">Assets
																	&amp; Liabilities</span> for your Needs Analysis and Product
																Recommendations? If<span class="text-success bold">
																	Yes</span> please fill in the details
																below :-
															</span>
														</div>
														
														<div class="col-1">
															<span class="badge badge-pill badge-warning"
																style="padding: .25em .4em;">Note :&nbsp;<i
																class="fa fa-commenting" aria-hidden="true"
																data-toggle="pg4AssetNote" data-trigger="hover"
																data-original-title="" title=""></i></span>
									                 </div>

														
													</div>
															    
															<!-- End -->
															
															<!--asset details row start  -->
															    <div class="row mt-2">
															<div class="col-md-4">
																<div class="card ">

																	<div class="card-header" id="cardHeaderStyle"
																		style="font-weight: normal;">
																		<div class="row">
																			<div class="col-md-12">
																				<span><img
																					src="vendor/psgisubmit/img/client1.png">&nbsp;Client
																					(1) </span>

																			</div>

																		</div>
																	</div>
																	<div class="card-body">
																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Estimated
																						Total Assets</label>&nbsp;<i class="fa fa-info-circle"
																						aria-hidden="true" style="color: #41aec0;"
																						data-toggle="Pg4assetETANote" data-trigger="hover"
																						data-original-title="" title=""></i>

																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alSelfTotassetErrMsg')"
																							name="alSelfTotasset" id="alSelfTotasset"
																							onpaste="PasteOnlyNum(this,4);"
																							onchange="calcCombined(this);calcAsset(this)"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					
																					<div id="alSelfTotassetErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Estimated
																						Total Liabilities</label>&nbsp;<i
																						class="fa fa-info-circle" aria-hidden="true"
																						style="color: #41aec0;"
																						data-toggle="Pg4assetETLNote" data-trigger="hover"
																						data-original-title="" title=""></i>

																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alSelfTotliabErrMsg')"
																							name="alSelfTotliab" id="alSelfTotliab"
																							onpaste="PasteOnlyNum(this,4);"
																							onchange="calcCombined(this);calcAsset(this)"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					<div id="alSelfTotliabErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>

																		</div>

																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Net
																						Assets</label>
																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alSelfNetassetErrMsg')"
																							name="alSelfNetasset" id="alSelfNetasset"
																							onpaste="PasteOnlyNum(this,4);"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					
																					<div id="alSelfNetassetErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>

																		</div>
																	</div>


																</div>
															</div>

															<!--client 2  -->
															<div class="col-md-4">
																<div class="card ">

																	<div class="card-header" id="cardHeaderStyle"
																		style="font-weight: normal;">
																		<div class="row">
																			<div class="col-md-12">
																				<span><img
																					src="vendor/psgisubmit/img/client2.png">&nbsp;Client
																					(2) </span>

																			</div>

																		</div>
																	</div>
																	<div class="card-body">
																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Estimated
																						Total Assets</label>&nbsp;<i class="fa fa-info-circle"
																						aria-hidden="true" data-toggle="Pg4assetETANote"
																						data-trigger="hover" style="color: #41aec0;"
																						data-original-title="" title=""></i>

																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alSpsTotassetErrMsg')"
																							name="alSpsTotasset" id="alSpsTotasset"
																							onpaste="PasteOnlyNum(this,4);"
																							onchange="calcCombined(this);calcAsset(this)"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					
																					<div id="alSpsTotassetErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Estimated
																						Total Liabilities</label>&nbsp;<i
																						class="fa fa-info-circle" aria-hidden="true"
																						data-toggle="Pg4assetETLNote" data-trigger="hover"
																						style="color: #41aec0;" data-original-title=""
																						title=""></i>

																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alSpsTotliabErrMsg')"
																							name="alSpsTotliab" id="alSpsTotliab"
																							onpaste="PasteOnlyNum(this,4);"
																							onchange="calcCombined(this);calcAsset(this)"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					
																					<div id="alSpsTotliabErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>

																		</div>

																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Net
																						Assets</label>
																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alSpsNetassetErrMsg')"
																							name="alSpsNetasset" id="alSpsNetasset"
																							onpaste="PasteOnlyNum(this,4);"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					
																					<div id="alSpsNetassetErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>

																		</div>
																	</div>


																</div>
															</div>
															<!--combined  -->

															<div class="col-md-4">
																<div class="card ">

																	<div class="card-header" id="cardHeaderStyle"
																		style="font-weight: normal;">
																		<div class="row">
																			<div class="col-md-12">
																				<span><img src="vendor/psgisubmit/img/joint.png">&nbsp;Combined</span>

																			</div>

																		</div>
																	</div>
																	<div class="card-body">
																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Estimated
																						Total Assets</label>&nbsp;<i class="fa fa-info-circle"
																						aria-hidden="true" style="color: #41aec0;"
																						data-toggle="Pg4assetETANote" data-trigger="hover"
																						data-original-title="" title=""></i>

																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alFamilyTotassetErrMsg')"
																							name="alFamilyTotasset" id="alFamilyTotasset"
																							onpaste="PasteOnlyNum(this,4);"
																							onchange="calcAsset(this)"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					<div id="alFamilyTotassetErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>
																		</div>
																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Estimated
																						Total Liabilities</label>&nbsp;<i
																						class="fa fa-info-circle" aria-hidden="true"
																						style="color: #41aec0;"
																						data-toggle="Pg4assetETLNote" data-trigger="hover"
																						data-original-title="" title=""></i>

																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alFamilyTotliabErrMsg')"
																							name="alFamilyTotliab" id="alFamilyTotliab"
																							onpaste="PasteOnlyNum(this,4);"
																							onchange="calcAsset(this)"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					
																					<div id="alFamilyTotliabErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>

																		</div>

																		<div class="row">
																			<div class="col-md-12">
																				<div class="form-group">
																					<label for="email" class="frm-lable-fntsz">Net
																						Assets</label>
																					<div class="input-group">
																						<div class="input-group-prepend">
																							<span class="input-group-text">$</span>
																						</div>
																						<input type="text" class="form-control checkdb"
																							onkeypress="return isNumber(event,'alFamilyNetassetErrMsg')"
																							name="alFamilyNetasset" id="alFamilyNetasset"
																							onpaste="PasteOnlyNum(this,4);"
																							aria-label="Amount (to the nearest dollar)"
																							maxlength="15">
																						<div class="input-group-prepend">
																							<span class="input-group-text">.00</span>
																						</div>
																						
																					</div>
																					
																					<div id="alFamilyNetassetErrMsg" class="invalid-feedbacks hide hide">Please
																									enter valid number</div>
																					
																				</div>
																			</div>

																		</div>
																	</div>


																</div>
															</div>
														</div>
															 
															<!--asset details row end  -->
														</div>
													</div>
												</div>

											</div>
										</form>
									</div>
								</div>

							</div>
						</div>

					</div>


				</div>

				

			</div>
			
			
<!--Popover Section  -->      

	<div id="popover-pg4DependNote" style="display: none">
		<ul class="list-group custom-popover">
			<li class="list-group-item"><i class="fa fa-sticky-note-o"
				aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Suggested no. of
				Years to support children: Boy - till 25 years old, Girl - till 22
				years old.</li>

		</ul>
	</div>

	<div id="popover-pg4CashFlwNote" style="display: none">
		<ul class="list-group custom-popover">
			<li class="list-group-item"><i class="fa fa-sticky-note-o"
				aria-hidden="true" id="i-icon-note-style"></i>&nbsp;For Regular
				premium plans, recommended premium amount should not be more than
				30% of client's annual earning/income..</li>


		</ul>
	</div>

	<div id="popover-pg4AssetNote" style="display: none">
		<ul class="list-group custom-popover">
			<li class="list-group-item"><i class="fa fa-sticky-note-o"
				aria-hidden="true" id="i-icon-note-style"></i>&nbsp;For Single
				premium plans, recommended premium amount should not form more than
				50% of client's assets.</li>

		</ul>
	</div>
	<!-- Financial Situation/Commitments(Asset & Liabilities) Notes -->
	<div id="popover-Pg4assetETANote" style="display: none">
		<ul class="list-group custom-popover">
			<li class="list-group-item"><i class="fa fa-sticky-note-o"
				aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Include Fixed
				Assets, Investments, Bank Deposit and Other.</li>
		</ul>
	</div>

	<div id="popover-Pg4assetETLNote" style="display: none">
		<ul class="list-group custom-popover">
			<li class="list-group-item"><i class="fa fa-sticky-note-o"
				aria-hidden="true" id="i-icon-note-style"></i>&nbsp;Include Loans,
				Mortgages and other debts.</li>
		</ul>
	</div>

<!-- end popover sections -->            