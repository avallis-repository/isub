<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

 
<style>




.card0 {
    background-color: #F5F5F5;
    border-radius: 8px;
    z-index: 0
}

.card00 {
    z-index: 0
}

.card1 {
    margin-left: 5rem;
    z-index: 0;
    border-right: 5px dotted #F5F5F5;
}

.card2 {
    display: none;
}

.card2.show {
    display: block
}

.social {
    border-radius: 50%;
    background-color: #FFCDD2;
    color: #E53935;
    height: 47px;
    width: 47px;
    padding-top: 16px;
    cursor: pointer
}


 



.next-button {
    width: 12%;
    height: 36px;
    background-color: #BDBDBD;
    color: #fff;
    border-radius: 6px;
    padding: 10px;
    cursor: pointer;
}

.next-button:hover {
    background-color: blue;
    color: #fff
}


.pic {
    width: 230px;
    height: 110px
}
 
#progressbar {
    position: absolute;
    left: 10px;
    overflow: hidden;
    color: #E53935;
}

#progressbar li {
    list-style-type: none;
    font-size: 8px;
    font-weight: 400;
    margin-bottom: 36px
}

#progressbar li:nth-child(3) {
    /* margin-bottom: 88px */
}

#progressbar .step0:before {
    content: "";
    color: #fff
}

#progressbar li:before {
    width: 30px;
    height: 30px;
    line-height: 30px;
    display: block;
    font-size: 20px;
    background: #fff;
    border: 2px solid #b5abab5e;
    border-radius: 50%;
    margin: auto
}

#progressbar li:last-child:before {
    width: 40px;
    height: 40px
}

#progressbar li:after {
    content: '';
    width: 3px;
    height: 66px;
    background: #35e56b;;
    position: absolute;
    left: 58px;
    top: 15px;
    z-index: -1
}



#progressbar li:last-child:after {
    top: 147px;
    height: 132px
}

#progressbar li:nth-child(3):after {
    top: 81px
}

#progressbar li:nth-child(2):after {
    top: 0px
}

#progressbar li:first-child:after {
    position: absolute;
    top: -81px
}

#progressbar li.active:after {
    background: #35e56b;
}

#progressbar li.active:before {
    background: #35e56b;;
    font-family: FontAwesome;
    content: "\f00c"; /* check icon content */
    
    /* content:"\f00d";  remove icon content */
}



.prevBtnStep {
    display: block;
    position: absolute;
    left: 40px;
    top: 20px;
    cursor: pointer
}

.prevBtnStep:hover {
    color: #D50000 !important
}

@media screen and (max-width: 912px) {
    .card00 {
        padding-top: 30px
    }

    .card1 {
        border: none;
        margin-left: 50px
    }

    .card2 {
        border-bottom: 1px solid #F5F5F5;
        margin-bottom: 25px
    }

    #progressbar {
        left: -25px
    }
}
</style>
</head>
<body>

 <!-- Modal -->
<div class="modal fade" id="signModal">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header" id="cardHeaderStyle-22">
          <h5 class="modal-title">Check List to Send for Approval</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
         <div class="modal-body">
            <div class="row">
                    <div class="col-12">
                        <div class="card card00 m-2 border-0">
                            <div class="row text-center justify-content-center px-3">
                                <h3 class="mt-4">eKYC</h3>
                            </div>
                            <div class="row">
                            <!-- Steps sec start -->
                                <div class="col-md-6">
                                    <div class="card1">
                                        <ul id="progressbar" class="text-center">
                                            <li class="step0" id="allPgeValSec"></li>
                                            <li class="step0" id="signSec"></li>
                                            <li class="step0" id="mgrApprSec"></li>
                                            <li class="step0" id="admnApprSec"></li>
                                            <li class="step0" id="compApprSec"></li>
                                        </ul>
                                        
                                        <h6 class="mb-5"  onclick="openContSec('allPgeValSec');">All Page Validations</h6>
                                        <h6 class="mb-5"  onclick="openContSec('signSec');">Signature Section</h6>
                                        <h6 class="mb-5"  onclick="openContSec('mgrApprSec');">Send Approval to Manager </h6>
                                        <h6 class="mb-5"  onclick="openContSec('admnApprSec');">Send Approval to Admin</h6>
                                        <h6 class="mb-5"  onclick="openContSec('compApprSec');">Send Approval to Compliance</h6>
                                    </div>
                                </div>
                                <!--step Sec End  -->
                                
                                <!--steps content Sec Start  -->
                                <div class="col-md-6" id="contentSec">
                                
                                    <div class="card2 ml-2 show" id="allPgeValSecCont">
                                        <div class="row text-center px-3 mr-2 pt-5">
                                           All page validations Completed..
                                        </div>
                                    </div>
                                    
                                    <div class="card2 ml-2 hide" id="signSecCont">
                                        <div class="row text-center px-3 mr-2">
                                          <div class="card">
                                              <div class="card-body">
                                                   <div class="row">
                                                      <div class="col-12">
                                                         <ul>
                                                          <li>Client Signature Done
                                                          <li>Adviser Signature Done
                                                          <li>Manager Signature Done                                                         
                                                         </ul>
                                                      </div>
                                                        
                                                   </div>
                                              </div>
                                          </div>
                                        </div>
                                        
                                    </div>
                                    
                                    
                                    <div class="card2 ml-2 hide" id ="mgrApprSecCont">
                                       <div class="row text-center px-3 mr-2">
                                          <div class="card">
                                              <div class="card-body">
                                                   <div class="row">
                                                      <div class="col-12">
                                                         Approval Link sent success
                                                      </div>
                                                       <div class="col-12">
                                                         manager not approve(Link to aprove manager)
                                                      </div>
                                                       
                                                   </div>
                                              </div>
                                          </div>
                                        </div>
                                       
                                    </div>
                                    
                                    
                                     <div class="card2 ml-2 hide" id ="admnApprSecCont">
                                       <div class="row text-center px-3 mr-2">
                                            <div class="card">
                                              <div class="card-body">
                                                   <div class="row">
                                                      <div class="col-12">
                                                          Approval Link sent success
                                                      </div>
                                                       <div class="col-12">
                                                         Admin not approve(Link to aprove Admin)
                                                      </div>
                                                       
                                                   </div>
                                              </div>
                                          </div>
                                        </div>
                                       
                                    </div>
                                    
                                     <div class="card2 ml-2 hide" id ="compApprSecCont"> 
                                        <div class="row text-center px-3 mr-2">
                                            <div class="card">
                                              <div class="card-body">
                                                   <div class="row">
                                                      <div class="col-12">
                                                          Approval Link sent success
                                                      </div>
                                                       <div class="col-12">
                                                         Comp. not approve(Link to aprove Comp.)
                                                      </div>
                                                       
                                                   </div>
                                              </div>
                                          </div>
                                        </div>
                                        
                                    </div>
                                    
                                    
                                   <!--  <div class="card2 ml-2">
                                        <div class="row px-3 mt-2 mb-4 text-center">
                                            <h2 class="col-12 text-success"><strong>Success !</strong></h2>
                                            <div class="col-12 text-center"><img class="tick" src="vendor/avallis/img/check.png"></div>
                                            <h6 class="col-12 mt-2"><i>...All Page Completed...</i></h6>
                                        </div>
                                    </div> -->
                                    
                                </div>
                                <!--Steps Content sec End  -->
                                
                            </div>
                        </div>
                       
                    </div>
                </div>
          </div>
       
        <div class="modal-footer">
          <button type="button" class="btn btn-bs3-prime btn-sm" data-dismiss="modal">Close</button>
        </div>
        
    </div>
  </div>
</div>

</body>
<script>
function openContSec(thisTab){
	
	
	var allPgeValSec= "allPgeValSec",signSec= "signSec",mgrApprSec= "mgrApprSec",
	admnApprSec= "admnApprSec",compApprSec= "compApprSec";
	
	//$("#"+thisTab).addClass("active");
	$("#"+thisTab+"Cont").removeClass("hide").addClass("show");
	
    if(thisTab == 'allPgeValSec'){
    	/* $("#"+signSec).removeClass("active");
    	$("#"+mgrApprSec).removeClass("active");
    	$("#"+admnApprSec).removeClass("active");
    	$("#"+compApprSec).removeClass("active"); */
    	
    	$("#"+signSec+"Cont").removeClass("show").addClass("hide");
    	$("#"+mgrApprSec+"Cont").removeClass("show").addClass("hide");
    	$("#"+admnApprSec+"Cont").removeClass("show").addClass("hide");
    	$("#"+compApprSec+"Cont").removeClass("show").addClass("hide");
	}
	
    if(thisTab == 'signSec'){
    	/* $("#"+allPgeValSec).removeClass("active");
    	$("#"+mgrApprSec).removeClass("active");
    	$("#"+admnApprSec).removeClass("active");
    	$("#"+compApprSec).removeClass("active"); */
    	
    	$("#"+allPgeValSec+"Cont").removeClass("show").addClass("hide");
    	$("#"+mgrApprSec+"Cont").removeClass("show").addClass("hide");
    	$("#"+admnApprSec+"Cont").removeClass("show").addClass("hide");
    	$("#"+compApprSec+"Cont").removeClass("show").addClass("hide");
	}
   
   if(thisTab == 'mgrApprSec'){
		/* $("#"+allPgeValSec).removeClass("active");
	   	$("#"+signSec).removeClass("active");
	   	$("#"+admnApprSec).removeClass("active");
	   	$("#"+compApprSec).removeClass("active"); */
	   	
	   	$("#"+allPgeValSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+signSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+admnApprSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+compApprSec+"Cont").removeClass("show").addClass("hide");
	}
   
   if(thisTab == 'admnApprSec'){
	  /*   $("#"+allPgeValSec).removeClass("active");
	   	$("#"+signSec).removeClass("active");
	   	$("#"+mgrApprSec).removeClass("active");
	   	$("#"+compApprSec).removeClass("active");
	   	 */
	   	$("#"+allPgeValSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+signSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+mgrApprSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+compApprSec+"Cont").removeClass("show").addClass("hide");
	}
   
   if(thisTab == 'compApprSec'){
	   /*  $("#"+allPgeValSec).removeClass("active");
	   	$("#"+signSec).removeClass("active");
	   	$("#"+mgrApprSec).removeClass("active");
	   	$("#"+admnApprSec).removeClass("active"); */
	   	
	   	$("#"+allPgeValSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+signSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+mgrApprSec+"Cont").removeClass("show").addClass("hide");
	   	$("#"+admnApprSec+"Cont").removeClass("show").addClass("hide");
		
	}
	
}
</script>
</html>

 
