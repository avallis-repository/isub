<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" import="com.afas.isubmit.util.PsgConst" %>
<!DOCTYPE html>
<html lang="en">
  
   <body id="page-top">
     
               
               <!-- Container Fluid-->
     <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
     <input type="hidden" id="hdnSessfnaId" name="hdnSessfnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
     <input type="hidden" id="hdnSessDataFormId" value="<%=session.getAttribute(PsgConst.SESS_DATAFORM_ID)%>"/>
	 <input type="hidden" id="recommPpId" name="recommPpId">			                                     
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
			  <div class="col-5">
			      <h5>Product Recommendations</h5>
			  </div>
			  
			  <div class="col-md-7">
			           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
			  </div>
        
       </div>
       
        <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>
          
          <div  class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh;" id="divProdRecomFrm">
			   <div class="card-body">
			      <jsp:include page="/views/pages/prodRecomContent.jsp"></jsp:include>
                 
                  </div><!--card-body end  -->
                  </div><!--card end  -->
                
                  
                  
            <!-- Add Prod Recomms Detls PopUp Form  modal-dialog-scrollable --> 
    <div class="modal fade" id="AddProdRecommDetlsMdl">
    <div class="modal-dialog modal-lg-sz" id="AddProdRecommdldlg" style="min-width: 42%;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
          <h5 class="modal-title bold" ><img src="vendor/psgisubmit/img/addins.png" id="mdlHdrImg">&nbsp; <span class="" id="prdMdlHdr">Add</span>&nbsp;Product Recommendation Details</h5>
          <button type="button" class="close white" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
             <div class="card">
                       <div class="card-body" id="prodFrom">
								<div id="prodDetlsFrm">	
								<div class="row">
								     <div class="col-12">
								     <ul class="navbar-nav ml-auto" style="margin-top:-24px;">
								             <li class="nav-item dropdown no-arrow">
							                        <a class="nav-link dropdown-toggle hide" href="#" id="addNewClnt" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="float: right;">
							                             <span><span class="text-primaryy font-sz-level5 btn btn-link" title="Add New Client" id="listStyle-02"><i class="fa fa-user-plus" aria-hidden="true" style="color:green;"></i>&nbsp;Add New Client</span></span>
							                         </a>
								                        <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="addNewClnt" id="dropdwnSec">
								                           <form class="navbar-search">
								                              <div class="input-group">
								                                 <input type="text" class="form-control border-1 small border-blue checkdb" placeholder="Type New client Name here..." aria-label="Search" aria-describedby="basic-addon2" style="" name="" id="txtFlddropAddNewClient" maxlength="60">
								                                 <div class="input-group-append">
								                                    <button class="btn btn-bs3-prime   btn-sm" type="button" title="Click to Add Client" id="btnAddNewDepntClnt" style="border-color: #3f51b5;">
								                                     <i class="fa fa-user-plus"></i>&nbsp;Add
								                                    </button>
								                                 </div>
								                              </div>
								                              <span class="hide" id="errorFld"><small class="text-danger">Keyin clientName.</small></span>
								                           </form>
								                        </div>
                                               </li>
							        </ul>
					      </div>
								    
					</div>
					
					<div class="row">
					       <div class='col-12'><span class="font-sz-level8 right"><span style="color: red;"><sup class="font-sz-level5">*</sup></span>Indicated Fields are Mandatory.</span></div>
					      
					</div>
					
					<div class="row">
					     <div class="col-12" id="alrtInfo" style="position: absolute;top: 0%;left: 85%;z-index: 200000;display:none">
					       
					       
					     
					     </div>
					</div>
					
					
					<div class="row">
					     <span class="font-sz-level7 noteInfo hide" id="bscRdrNotInfo">You Currently&nbsp;<span><span class="badge badge-pill badge-success font-sz-level9" id="taskNotiFlg" style="cursor: default;"> Adding </span></span>&nbsp;<span class="bold" id="hdntxtPlntype">&nbsp;&nbsp;</span> Details for client&nbsp;:&nbsp;<span class="text-customcolor-gp bold" id="hdnBscTextClnt">&nbsp;&nbsp;</span>&nbsp;<span> and </span>Principal&nbsp;:&nbsp;<span class="text-customcolor-gp bold" id="hdnBscTextComp">&nbsp;&nbsp;</span></span>
			       </div>
			
			    
							
								<div class="row">
								
									<div class="col-9">
										     <div class="form-group">
											    <label class="frm-lable-fntsz" for="recomPpName">Client Name<sup style="color: red;"><strong>*</strong></sup></label>
											    <select class="form-control form-control-sm checkdb" id="recomPpName" name="recomPpName">
												  <option value="" selected="">Select a Client Name</option>
												   <optgroup label="Search Client or Add New Client Details" class="font-sz-level4" id="dynaClientAddSec">
														<c:if test="${not empty FNA_SELF_SPOUSE_NAMES}">
														<c:forEach var="names" items="${FNA_SELF_SPOUSE_NAMES}">
														<option class="" value="${names}">${names}</option>
														</c:forEach>
														</c:if>  
									               </optgroup>
											     </select>
											    
											     
									             
									            <!--  <div class="input-group input-group-sm mb-3 d-none">
													  <input type="text" class="form-control typeahead border-primary "  name="recomPpNameText" id="recomPpNameText" placeholder="Start typing to search/add..." data-provide="typeahead" autocomplete="off">
													   <div class="input-group-prepend">
														<span class="input-group-text p-1 border-primary" id="btnClrCnlt" style="border-radius: 0px 5px 5px 0px;">
															<a class="btn btn-bs3-prime" style="padding:2px 2px" onclick="clrRecommPpName('recomPpName')"><i class="fa fa-user-plus"  title="Clear ClientName"  ></i>&nbsp;Add New Client</a>
														</span>
													  </div>
									            </div> -->
									             
									              <div class="invalid-feedbacks hide" id="txtFldClntNameError">
                                                          Please Select Client Name
                                                   </div>
				                            </div>
				                            
				                      </div>
	                                 
	                                 
	                                 <div class="col-3 mt-3">
	                                       <div class="mt-1" id="btnClrCnlt" style="">
												<a class=" btn btn-bs3-success px-2 py-2" style="padding:2px 2px;border-radius: 18px;" onclick="clrRecommPpName('recomPpName')"><i class="fa fa-user-plus" title="Clear ClientName"></i>&nbsp;Add New Client</a>
										   </div>
										   
	                                 </div>
                                </div>
								
								
								<div class="row">
								     <div class="col-12">
										   <div class="form-group">
												    <label class="frm-lable-fntsz" for="">Company / Principal  Name<sup style="color: red;"><strong>*</strong></sup></label>
												    
												    <select class="form-control checkdb" id="recomPpPrin" name="recomPpPrin" maxlength="60">
												       <option value="" selected="" data-select2-id="select2-data-2-d94v">Select an Insurance Company</option>
												           <c:forEach var="prin" items="${PRINCIPAL_LIST}">
																<option value="${prin.prinName}">${prin.prinName}</option>
															</c:forEach>
													 </select>
				                                     
				                                    <div class="invalid-feedbacks hide" id="recomPpPrinError">
                                                          Please Select a Company / Principal Name
                                                        </div>
                                                   </div>
                                            </div>
								</div>
								
										 
								<div class="row">								
									<div class="col-8">
									 <label class="frm-lable-fntsz" for="">Product LOB<sup style="color: red;"><strong>*</strong></sup></label><br>
										       <div class="custom-control custom-radio custom-control-inline">
												    <input type="radio" class="custom-control-input checkdb" name="recomPpProdtype" 
												    id="recomPpProdtypeLife" value="Life" maxlength="10">
												    <label class="custom-control-label font-sz-level6 pt-1" for="recomPpProdtypeLife" style="font-weight:normal;">Life</label>
												</div> 
                                       
										        <div class="custom-control custom-radio custom-control-inline">
												    <input type="radio" class="custom-control-input checkdb" name="recomPpProdtype" 
												    id="recomPpProdtypeA&amp;H" value="A&amp;H" maxlength="10">
												    <label class="custom-control-label font-sz-level6 pt-1" for="recomPpProdtypeA&amp;H" style="font-weight:normal;">A&amp;H</label>
												</div> 
												
										        <div class="custom-control custom-radio custom-control-inline">
												    <input type="radio" class="custom-control-input checkdb" name="recomPpProdtype"
												     id="recomPpProdtypePA" value="PA" maxlength="10">
												    <label class="custom-control-label font-sz-level6 pt-1" for="recomPpProdtypePA" style="font-weight:normal;">PA</label>
												</div> 
										
									            <div class="custom-control custom-radio custom-control-inline">
												    <input type="radio" class="custom-control-input checkdb" name="recomPpProdtype"
												     id="recomPpProdtypeILP" value="ILP" maxlength="10">
												    <label class="custom-control-label font-sz-level6 pt-1" for="recomPpProdtypeILP" style="font-weight:normal;">ILP</label>
												</div> 

                                               <div class="invalid-feedbacks hide" id="radBtnprodLobError">
                                                          Please Select  Product LOB.
                                              </div>
									</div>
									
									<div class="col-4">
									 <label class="frm-lable-fntsz" for="selFldProdType">Product Type<sup style="color: red;"><strong>*</strong></sup></label><br>
									     <div class="form-check form-check-inline" id="prodTypeBasicSec">
										  <input class="form-check-input checkdb" type="radio" name="recomPpBasrid" id="recomPpBasrid" value="BASIC" maxlength="3">
										  <label class="form-check-label font-sz-level8 bold pr-2" for="inlineRadioT1">Basic</label>
                                         </div>
                                     
										<div class="form-check form-check-inline hide" id="prodTypeRiderSec">
										  <input class="form-check-input checkdb" type="radio" name="recomPpBasrid" id="recomPpBasrid" value="RIDER" maxlength="3">
										  <label class="form-check-label font-sz-level8 bold pr-2" for="inlineRadioT2">Rider</label>
										</div>
										
										 <div class="invalid-feedbacks hide" id="radBtnProdTypeError">
                                              Please Select Prod.Type
                                         </div>
									 </div>
								</div>
								
								
								<div class="row">
								   <div class="col-12">
								  <div class="form-group hide"  id="selFldBscPlanComboSec">
                                        <label class="frm-lable-fntsz" for="selFldBscPlanCombo" style="color: #0f4ef3;"> Which Basic Plan You Want to Add Rider Now?</label>
                                            <select class="form-control checkdb" id="selFldBscPlanCombo">
                                               <option value="" selected="true" >--Select Basic Plans Below--</option>
                                               
    									   </select>
    									   
    									   <div class="invalid-feedbacks btnshake hide" id="selFldBscPlanComboError">
                                                  Select a Basic Plan to Proceed !
                                           </div>
                                  </div>
                                  <select class="form-control checkdb d-none" id="selFldBscProdLobCombo">
                                               <option value="" selected="true" >--Select Basic Plans Below--</option>
                                               
    									   </select>
                                      </div> 
								 </div>
								
								<div class="row">
								<div class="col-12">
									   <div class="form-group">
											    <label class="frm-lable-fntsz" for="recomPpProdname">Product Name<sup style="color: red;"><strong>*</strong></sup></label>
											    <select class="form-control checkdb" id="recomPpProdname" name="recomPpProdname" maxlength="20" >
												        <option value="" selected="true" data-select2-id="select2-data-4-j3jb">Keyin the Product Name</option>
					                           </select>
											
											 <div class="invalid-feedbacks hide" id="recomPpProdnameError">
                                                  Please Keyin the Product Name 
                                             </div>
                                             
                                             
                                             
                                             <div class="invalid-feedbacks hide" id="errInfoSameProd">
                                                   
                                             </div>
                                             
                                             
			                          </div>
                                </div>
								</div>
								
							<div class="row">								
									<div class="col-4">
									<label class="frm-lable-fntsz" for="">Premium</label>
									<div class="input-group input-group-sm mb-1">
									  <div class="input-group-prepend">
										<span class="input-group-text p-1" >$</span>
									  </div>
									  <input type="text" class="form-control numberClass checkdb" id="recomPpPremium" name="recomPpPremium"
									   aria-label="Amount (to the nearest dollar)" maxlength="17">
									 
									</div>

                                      <div class="invalid-feedbacks hide"  id="txtFldPremiumError">
                                               Keyin  Numbers Only!
                                      </div> 
                                      
									</div>
									
									<div class="col-4">
									
									<label class="frm-lable-fntsz" for="">Sum Assured</label>
									<div class="input-group input-group-sm mb-1">
									  <div class="input-group-prepend">
										<span class="input-group-text p-1" >$</span>
									  </div>
									  <input type="text" class="form-control numberClass checkdb" id="recomPpSumassr" name="recomPpSumassr"
									   aria-label="Amount (to the nearest dollar)" maxlength="17">
									 
									</div>
									
									<div class="invalid-feedbacks hide" id="txtFldSAError">
                                           Keyin  Numbers Only!
                                     </div>
									
									</div>
									
									<div class="col-4">
									<label class="frm-lable-fntsz" for="name">Pay. Method</label>
                                       <select class="form-control form-control-sm custom-select mr-sm-2 checkdb" 
                                       id="recomPpPaymentmode" name="recomPpPaymentmode" maxlength="20">
                                          <option value="" selected="true">--Select--</option>
                                          <option value="Cash">Cash</option>
                                          <option value="Credit-Card">Credit-Card</option>
                                          <option value="CPFMA">CPFMA</option>
                                          <option value="CPFOA">CPFOA</option>
                                          <option value="CPFSA">CPFSA</option>
                                          <option value="CPF">CPF</option>
                                          <option value="GIRO-Others">GIRO-Others</option>
                                          <option value="TT">TT</option>
                                          <option value="SRS">SRS</option>
                                       </select>
                                        <div id="selFldPayMethodError">
                                                 
                                         </div>
									</div>
								</div>
								
								
								<div class="row">								
									<div class="col-4">
									<label class="frm-lable-fntsz" for="">Plan Term</label>
									<div class="input-group input-group-sm mb-3">
									  
									  <input type="text" class="form-control checkdb" aria-label=""
									   id="recomPpPlanterm" name="recomPpPlanterm" maxlength="30">
									   <div class="input-group-prepend">
										<span class="input-group-text p-1" >yrs</span>
									  </div>
									</div>
									<div class="invalid-feedbacks" id="txtFldplnTermError">
                                              
                                     </div>
									</div>
									
									<div class="col-4">
									<label class="frm-lable-fntsz" for="">Prem. Term</label>
									<div class="input-group input-group-sm mb-3">
									  <input type="text" class="form-control checkdb" aria-label="" 
									  id="recomPpPayterm" name="recomPpPayterm" maxlength="30">
									   <div class="input-group-prepend">
										<span class="input-group-text p-1" >yrs</span>
									  </div>
									</div>
									
								   
                                    
									<div class="invalid-feedbacks" id="txtFldPrmTermError">
                                              
                                     </div>
									</div>
									
									<div class="col-4">
									 <label class="frm-lable-fntsz" for="prodRiskRate">Prod. Risk Rate</label>
									  <select class="form-control form-control-sm custom-select mr-sm-2 checkdb" id="prodRiskRate" name="prodRiskRate">
                                          <option value="" selected="true">--Select--</option>
                                          <option value="1">Conservative</option>
                                          <option value="2">Moderatively Conservative </option>
                                          <option value="3">Moderatively Aggressive </option>
                                          <option value="4">Aggressive</option>
                                       </select>
									</div>
								</div>
								<div class="row">
									<div class="col-12">
									<span class="badge badge-pill badge-success font-sz-level8 mt-2 py-2 hide" id="btnAddBscPlns"><i class="fa fa-shield" aria-hidden="true"></i><sub><i class="fa fa-plus" aria-hidden="true"></i></sub>&nbsp;Add Basic Plans</span>
									<span class="badge badge-pill badge-warning font-sz-level8 right mt-2 py-2 hide" id="btnAddRdrPlns"><i class="fa fa-shield" aria-hidden="true"></i><sub><i class="fa fa-plus" aria-hidden="true"></i></sub>&nbsp;Add Rider Plans</span>
									</div>
								</div>
								
								
								<input type="hidden" id="hdnFldPrinId"/>
								<input type="hidden" id="hdnFldPrinCrdId"/>
								<input type="hidden" id="hdnFldcurntBscPlnId"/>
								<input type="hidden" id="hdnFldrdrBscPrinIn"/>
								<input type="hidden" id="hdntisBtnId"/>
								<input type="hidden" id="hdnRdrCompId"/>
								<input type="hidden" id="hdntxtProdText"/>
								<input type="hidden" id="hdnRdrEdtSecId"/>
								<input type="hidden" id="hdnBscEdtSecId"/>
								
								<!--Edit Hdn DFields  -->
								<input type="hidden" id="hdnUnqId"/>
								<input type="hidden" id="hdnAction"/> 
								<input type="hidden" id="hEdtComp"/> 
								<input type="hidden" id="hEdtClntName"/> 
								<input type="hidden" id="hEtdPlnVal"/> 
								<input type="hidden" id="hEtdPln"/> 
								<input type="hidden" id="hEdtBscProdVal"/> 
								<input type="hidden" id="hEdtBscSetId"/> 
								<input type="hidden" id="hEdtRdrBasicId"/> 
								<input type="hidden" id="recomPpId" name="recomPpId"/>
								<input type="hidden" name="fnaDetails" id="fnaDetails" value="${CURRENT_FNAID}"/>
								<input type="hidden" name="dataformId" id="dataformId" value="${CURRENT_DATAFORM_ID}"/>
								<input type="hidden" id="recomPpPlan" name="recomPpPlan">
								<input type="hidden" id="recomPpBasicRef" name="recomPpBasicRef" maxlength="60">
								
								<input type="hidden" name="recomPpPerson"/>
								<input type="hidden" name="recomPpPersonname"/>
								<input type="hidden" name="recomPpCateg"/>
								<input type="hidden" name="createdBy"/>
								<input type="hidden" name="createdDate"/>
								<input type="hidden" name="recomPpOpt"/>
								<input type="hidden" id="htxtFlfProdselVal" name="htxtFlfProdselVal"/>
							</div>
							</div>
							
							
                           
                        </div>
        </div>
        
       
        <!-- Modal footer -->
        <div class="modal-footer">
        	<button class="btn btn-sm btn-success  font-sz-level8" id="btnsaveAddNewPrdDetls"><i class="fa fa-floppy-o" aria-hidden="true" id="saveBtnIcon"></i>&nbsp;Save &amp; Add New</button>
           <button class="btn btn-sm grey-bg font-sz-level8" id="btnClrFrmFdls"><i class="fa fa-refresh" aria-hidden="true" style="color: #058e8e;"></i>&nbsp;Clear</button>
           <button class="btn btn-sm btn-warning  font-sz-level8" data-dismiss="modal" id="btnClose"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Close</button>
        </div>
        
      </div>
    </div>
  </div>
 <!-- End of Prod Recomm Prod Detls -->        
                  
                  
                  
                  

                 
                  
                  
                   <div class="modal fade" id="genKYCModalScrollable" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
                     aria-labelledby="genKYCModalScrollableTitle" aria-hidden="true">
                     <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
                        <div class="modal-content">
                           <div class="modal-header">
                              <h5 class="modal-title" id="genKYCModalScrollableTitle">Loading...</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                              <span aria-hidden="true">&times;</span>
                              </button>
                           </div>
                           <div class="modal-body">
                              <div class="col-lg-12">
                                 <div class="loader">
                                    <p></p>
                                 </div>
                              </div>
                           </div>
                           <div class="modal-footer">
                              <button type="button" class="btn btn-outline-primary" data-dismiss="modal">OK</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!---Container Fluid-->
   	
            
             <!-- Profile Modal -->
               
         
        <!-- page 6 add new Client modal popup -->
        <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			  <div class="modal-dialog modal-sm">
			    <div class="modal-content">
			      ...
			    </div>
			  </div>
       </div>
       <!--End Here  -->
       
          <!-- page 6 prod detls consolidated popup details -->
            
              <!-- The Modal -->
  <div class="modal fade" id="prodConsDetls" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-xl  modal-dialog-scrollable" style="min-height: 90vh;min-width: 90vw;">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
          <h6 class="modal-title font-sz-level3"><img src="vendor/psgisubmit/img/preview.png"> Summary Of Product  Details</h6>
          <span class="font-sz-level3 hide d-none" style="margin-left: 60%;" title="click to Print Product Summary Details.">Print : <i class="fa fa-print prntIcn icncolr" aria-hidden="true" onclick=printProdSumDet();></i></span>
          <button type="button" class="close" data-dismiss="modal" style="color:#fff;">&times;</button> 
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
      <div class="row"> 
     <span class="pl-2"><img src="vendor/psgisubmit/img/eye.png"> <span class="bold text-primaryy font-sz-level4">Consolidated View of All product Details :- </span></span>
    
        <div class="card mx-2 mt-1">
    
                 <div id="srchClntLoader"> 
                       <img src="vendor/psgisubmit/img/DV9T.gif"  style="width:20%;" class="rounded mx-auto d-block">
                </div>
                
                 <div class="table-responsive p-1" id="prodSummayTbl" style="min-height: 65vh;">
                    <table class="table data-table align-items-center cell-border" id="prodSummTable" 
                    
                    style="position: relative; border-collapse: collapse;" >
                      <thead class="thead-light font-sz-level6">
                      <tr>
					    <th valign="top"></th>
                        <th valign="top">Proposed<br/>Name</th>
                        <th valign="top">Assurer<br/>Name</th>
                        <th  valign="top" width="150">Company</th>
                        <th valign="top">LOB</th>
                        <th  width="250" valign="top">Product</th>
                        <th valign="top">Type</th>
                        <th valign="top">SA<br/>($)</th>
                        <th valign="top">Payment</th>
                        <th valign="top">Plan.Term<br/>(Yrs.)</th>
                        <th valign="top">Pay.Term<br/>(Yrs.)</th>
                        <th valign="top">Prem.<br/>($)</th>
                        <th valign="top">Risk</th>
                      </tr>
                    </thead>
                  
                      <tbody id="prodSummTblBody"></tbody>
                   
                  </table>
                </div>
                </div> 
        </div>
     
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
       
     
	
        
        <script src="vendor/psgisubmit/js/session_timeout.js"></script>  
        <script type="text/javascript" src="vendor/psgisubmit/js/Common Script.js"></script>
          <script src="vendor/psgisubmit/js/fna_common.js"></script>
		  <script src="vendor/psgisubmit/js/product_recomm.js"></script>
		  <script>jsnDataProdRecomDetls = ${PRODUCT_RECOM_PLAN_DETS}</script>
		  

       
     
 
 

</body>
</html>