<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>FPMS | eKYC Approvals</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
  
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"/>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css"/>  
	
	<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

   
<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
   
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
 -->
 
 <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
 <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    
  <style>
  .fakeimg {
    height: 200px;
    background: #aaa;
  }
  .Notifig_icons{
 /*  margin:10px 0px;  */
  border-radius:50%;
  }
  .Notifigicons{
    border-radius:50%;
  
  }
  #right{
  float:right;
  }
  
  /*
  #Sections{
 background: linear-gradient(to left, rgb(173, 169, 150), rgb(242, 242, 242), rgb(219, 219, 219), rgb(234, 234, 234));
}

.card-header{
  background: linear-gradient(to left, rgb(17, 153, 142), rgb(56, 239, 125)); 
      color: white;
    font-weight: bold;
  }
  

  
  .navbar{
  background: linear-gradient(to left, rgb(17, 153, 142), rgb(56, 239, 125)); 
  }
    */
  .notification {
  color: white;
  text-decoration: none;
  padding: 10px 0px;
  position: relative;
  display: inline-block;
  border-radius: 50%;
}
.notification .badge {
    position: absolute;
    top: 5px;
    right: -20px;
    padding: 5px 10px;
    border-radius: 50%;
    background: #4caf50;
    color: white;
}
#right{
float:right;
}
#left{
float:left;
}
 #myBtn {
  display: none;
  position: fixed;
  bottom: 20px;
  right: 30px;
  z-index: 99;
  font-size: 18px;
  border: none;
  outline: none;
  background: linear-gradient(to right, rgb(17, 153, 142), rgb(56, 239, 125)); 
  color: white;
  cursor: pointer;
  padding: 15px;
  border-radius: 50%;
}

#myBtn:hover {
 background: linear-gradient(to right, rgb(0, 191, 143), rgb(0, 21, 16));  
} 
hr{
background: teal;
    height: 1px;
}

.modal-dialog,
.modal-content {
    height: 90%;
}

.modal-body {
    /* 100% = dialog height, 120px = header + footer */
    max-height: calc(100% - 40px);
    overflow-y: scroll;
     overflow-x: none;
}

.carousel-control-prev-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23f00' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
}

.carousel-control-next-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23f00' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
}

.carousel-control-prev,
.carousel-control-next{
    align-items: flex-start; /* Aligns it at the top */
}

.carousel-control-prev,
.carousel-control-next{
    align-items: flex-end; /* Aligns it at the bottom */
}

#srchDetls {
    top: -750px;
    right: 80px;
    left: 152px;
    position: absolute;
    border: 1px solid teal;
}
 
 .statsIcons{
 margin: 0px 5px;
 }

.vLine{
height:50px;
background:white;
}
.show{
display:none;
}
.hide{
display:block;
}
/* #ListofFNAIds{
height:800px;
display:flex;
align-content:center;
align-items:center;

} */
/* #clntSrchDetls{
    position: absolute;
    height: 400px;
    border: 1px solid red;
    z-index: 1000;
    background: #fff;
    overflow-y: scroll;
    top: 47px;
    width: 450px;
    overflow-x: hidden;
    right: 113px;
}}  */

.filter {
  width: 300px;
  /* margin: 20px auto; */
  font-family: 'Whitney', sans-serif;
}
.filter p {
  padding: 0 10px;
}
.filter p, .filter li {
  font-weight: bold;
  line-height: 35px;
  font-size: 12px;
}
.filter .title {
  color: #fff;
  background-color: #1ba0eb;
  -moz-border-radius: 3px 3px 0 0;
  -webkit-border-radius: 3px;
  border-radius: 3px 3px 0 0;
}
.filter .title_items {
  color: #e4effd;
  cursor: pointer;
  font-weight:bold;
  position: relative;
  /* background-color: #fff; */
  border-top: 1px solid #e4effd;
  border-bottom: 1px solid #e4effd;
  -moz-transition: linear 0.2s background-color;
  -o-transition: linear 0.2s background-color;
  -webkit-transition: linear 0.2s background-color;
  transition: linear 0.2s background-color;
}
.filter .title_items:hover {
  background-color: transparent;
}
.filter .title_items.active + ul + .title_items {
  border-top: 1px solid #e4effd;
}
.filter .title_items.active:after {
  height: 5px;
  width: 5px;
  right: 14px;
  margin-top: -2.5px;
}
.filter .title_items:before, .filter .title_items:after {
  content: "";
  position: absolute;
  -moz-border-radius: 3px;
  -webkit-border-radius: 3px;
  border-radius: 3px;
  background-color: #c3d0e8;
  -moz-transition: linear 0.3s all;
  -o-transition: linear 0.3s all;
  -webkit-transition: linear 0.3s all;
  transition: linear 0.3s all;
}
.filter .title_items:before {
  height: 5px;
  width: 13px;
  top: 50%;
  right: 10px;
  margin-top: -2.5px;
}
.filter .title_items:after {
  height: 13px;
  width: 5px;
  top: 50%;
  right: 14px;
  margin-top: -6.5px;
}
.filter ul {
  -moz-transition-property: all;
  -o-transition-property: all;
  -webkit-transition-property: all;
  transition-property: all;
  -moz-transition-timing-function: linear;
  -o-transition-timing-function: linear;
  -webkit-transition-timing-function: linear;
  transition-timing-function: linear;
  max-height: 250px;
  min-height:auto;
  overflow-y: auto;
  overflow-x:hidden;
   width: 90%;
  list-style: none;
}
.filter ul li {
  height: 0;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  color: #92a5bf;
  background-color: #f5f8fe;
  -moz-transition: linear 0.2s all;
  -o-transition: linear 0.2s all;
  -webkit-transition: linear 0.2s all;
  transition: linear 0.2s all;
}
.filter ul li:nth-child(odd) {
  -moz-transform: scale(0.5) translateX(-150px);
  -ms-transform: scale(0.5) translateX(-150px);
  -webkit-transform: scale(0.5) translateX(-150px);
  transform: scale(0.5) translateX(-150px);
}
.filter ul li:nth-child(even) {
  -moz-transform: scale(0.5) translateX(150px);
  -ms-transform: scale(0.5) translateX(150px);
  -webkit-transform: scale(0.5) translateX(150px);
  transform: scale(0.5) translateX(150px);
}
.filter ul li.visible {
  height: 36px;
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
  -moz-transform: scale(1) translateX(0);
  -ms-transform: scale(1) translateX(0);
  -webkit-transform: scale(1) translateX(0);
  transform: scale(1) translateX(0);
}
.filter ul li:last-child label {
  border-bottom: none;
}
.filter ul li:nth-child(odd) label:before, .filter ul li:nth-child(odd) label:after {
  border-color: #5db6e2;
}
.filter ul li:nth-child(even) label:before, .filter ul li:nth-child(even) label:after {
  border-color: #c0a2f1;
}
.filter input[type="radio"] {
  display: none;
}
.filter input[type="radio"]:checked + label:after {
  filter: progid:DXImageTransform.Microsoft.Alpha(enabled=false);
  opacity: 1;
}
.filter label {
  margin: 0 15px;
  display: block;
  cursor: pointer;
  position: relative;
  padding: 0 10px 0 25px;
  border-bottom: 1px solid #ddebfd;
}
.filter label:before {
  content: "";
  height: 10px;
  width: 10px;
  top: 50%;
  left: 0;
  margin-top: -9px;
  position: absolute;
  border-width: 3px;
  border-style: solid;
  -moz-border-radius: 50%;
  -webkit-border-radius: 50%;
  border-radius: 50%;
}
.filter label:after {
  content: "";
  top: 50%;
  left: 5px;
  margin-top: -4px;
  position: absolute;
  border-width: 3px;
  border-style: solid;
  filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  -moz-border-radius: 50%;
  -webkit-border-radius: 50%;
  border-radius: 50%;
  -moz-transition: linear 0.2s all;
  -o-transition: linear 0.2s all;
  -webkit-transition: linear 0.2s all;
  transition: linear 0.2s all;
}


#dLabel {
    width: 240px;
  height: 40px;
  border-radius: 4px;
  background-color: #fff;
  border: solid 1px #cccccc;
  text-align: left;
  padding: 7.5px 15px;
  color: #ccc;
  letter-spacing: 0.7px;
  margin-top: 25px;
  
 
}

 .caret {
    float: right;
    margin-top: 9px;
    display: block;
  }

.dropdown-menu {
  width: 240px;
  padding: 0;
  margin: 0;
  border-top-left-radius: 0;
  border-top-right-radius: 0;
}

.dropdown button:hover, .dropdown button:focus {
  border: none;
  outline: 0;
}

.dropdown.open button#dLabel {
  border-bottom-left-radius: 0;
  border-bottom-right-radius: 0;
 
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.23);
  border: solid 1px #666;
   border-bottom: none;
}

.dropdown.open ul {
   box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.23);
  border: solid 1px #666;
  border-top: none;
  height: 200px;
  overflow-y: scroll;
}

.dropdown-menu li {
 
     line-height: 1.5;
  letter-spacing: 0.7px;
  color: #666;
    font-size: 14px;
  cursor: pointer;
  padding: 7.5px 15px;
  border-top: solid 1px #f3f3f3;
  
 
 
}

.dropdown-menu li:hover {
  background-color: #ccc;
}

.bootstrap-select.show > .dropdown-menu > .dropdown-menu {
  display: block;
}
.bootstrap-select > .dropdown-menu > .dropdown-menu li.hidden {
  display: none;
}

.nav-tabs .nav-link.active {
    font-weight:bold;
    background-color: transparent;
    border-bottom:3px solid #dd0000;
    border-right: none;
    border-left: none;
    border-top: none;
}

</style>
</head>
<body >
<form id="frmTempLogin" method="POST" action="KycApprovalPre.action" autocomplete="off">

<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_DISTID")%>" name="hTxtFldDistId"/>
									<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_ADVID")%>" name="hTxtFldAdvId"/>
									<input type="hidden" value="<%=session.getAttribute("LOGGED_ADVSTFID")%>" name="txtFldLogUserAdvId"/>
									<input type="hidden"   name="hTxtFldUserId"/>
									<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_USERID")%>" name="txtFldCrtdUser" id="txtFldCrtdUser"/>
									<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_MGRACSFLG")%>" name="hTxtFldMgrAcsFlg"/>
									<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_MGRACSFLG")%>" name="mgrAcsFlg"/>
									<input type="hidden" value="<%=session.getAttribute("KYC_APPROVE_FNAID")%>" name="hTxtFldFnaId" id="hTxtFldFnaId"/>

</form>




<!-- Scroll overflow button -->
<!-- <button type="button"   id="myBtn" title="Go to top"><i class="fa fa-arrow-up" aria-hidden="true"></i></button> -->
<!--Scroll overflow button end  -->

<nav class="navbar fixed-top  navbar-light bg-light ">
	        <a href="#" class="navbar-brand bold" style="color:#000;font-weight:500">eKYC</a>  <strong>FNA ID:</strong>  <input type="text"  id="txtFldFnaId"  readonly style="border:none;background:transparent"/>
	        	<div class="form-inline ml-auto">
	        	<div class="row-fluid">
		        	<select id="dropDownClntNameDiv" class="checkdb">
		        		<option disabled="disabled" selected="true">Client Search.....</option>
		          	</select>
	 			</div>
	                   
	        	</div>
            
	       <ul class="nav navbar-nav ml-auto">
                <li class="nav-item dropdown" style="display:inline-block">
                    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown"><strong>Adviser : <span id="spanLoggedAdviser"></span></strong></a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a href="#" class="dropdown-item">Reports</a>
                        <a href="#" class="dropdown-item">Settings</a>
                        <div class="dropdown-divider"></div>
                        <a href="#"class="dropdown-item">Logout</a>
                    </div>
                </li>
            </ul>
	</nav>
	
	<form name="frmKycApprovalNew"  id="frmKycApprovalNew" >
<div class="container-fluid" style="margin-top:50px">
 



<div class="row">

<div class="col-md-1">
  <div class="card">
     <div class="card-header text-center"><i class="fa fa-bell" aria-hidden="true"> </i></div>
          <div class="card-body" id="Sections">
       <input type="hidden"  id="txtFldAdvStfName"  readonly style="border:none;background:transparent"/>
           <div class="ListDets_Icons" >
              
               <a href="#" class="notification">
               <button type="button" class="btn btn-info Notifigicons" id="btnApprovedHistory" onclick="getFNADetailsNew('APPROVE');" value="APPROVE" title="View Approved List"><i class="fa fa-check" aria-hidden="true"></i></button>
                <span class="badge">
                	<c:if test="${not empty FNA_MGR_STATUS_LIST}">
                    	${FNA_MGR_STATUS_LIST.APPROVE}
                     </c:if> 
                     
                     
                </span></a>
   
                 <a href="#" class="notification">
                 <button type="button" class="btn btn-primary Notifigicons" id="btnPendHistory" onclick="getFNADetailsNew('PENDING');" value="PENDING" title="View Pending List"><i class="fa fa-exclamation-circle" aria-hidden="true"></i></button>
                 <span class="badge"><c:if test="${not empty FNA_MGR_STATUS_LIST}">
                      	 ${FNA_MGR_STATUS_LIST.PENDING}
                      	 </c:if>
                      	 
                      	 </span></a>
                
                <a href="#" class="notification">
                <button type="button" class="btn btn-secondary Notifigicons" id="btnRejectHistory"  onclick="getFNADetailsNew('REJECT');" value ="REJECT" title="View Rejected List" ><i class="fa fa-ban" aria-hidden="true"></i></button>
                <span class="badge"><c:if test="${not empty FNA_MGR_STATUS_LIST}">
                      	 ${FNA_MGR_STATUS_LIST.REJECT}
                      	 </c:if></span></a>
                
           </div> 
      	</div>
      </div>
 </div>
 
 <div class="col-md-4">
 
 <div class="card">

<div class="card-header"> Approval Sections <i class="fa fa-eye"></i></div>

<div class="card-body" >

<nav class="nav-justified ">
                  <div class="nav nav-tabs " id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="pop1-tab" data-toggle="tab" href="#pop1" role="tab" aria-controls="pop1" aria-selected="true">Manager <i class="fa fa-check-circle"></i></a>
                    <a class="nav-item nav-link" id="pop2-tab" data-toggle="tab" href="#pop2" role="tab" aria-controls="pop2" aria-selected="false">Admin <i class="fa fa-times-circle"></i></a>
                    <a class="nav-item nav-link" id="pop3-tab" data-toggle="tab" href="#pop3" role="tab" aria-controls="pop3" aria-selected="false">Comp. <i class="fa fa-times-circle"></i></a>
                    
                  </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                  <div class="tab-pane fade show active" id="pop1" role="tabpanel" aria-labelledby="pop1-tab">
                        <div class="pt-3"></div>
                        
                        <div class="row">
						   <div class="col-md-6">
						   <label for="" class="text-primary">Status</label>
						    <select class="form-control checkdb" id="txtFldMgrApproveStatus"> 
						        <option value="APPROVE">Approved</option>
						        <option value="REJECT">Rejected</option>
						        <option value="">Pending</option>
						    </select>
						   </div>
						   
						   <div class="col-md-6">
						        <label class="text-primary" id="mngrapprLbl" ></label> 
						          
						             <div class="input-group form">
							               <input type="text" class="form-control checkdb" id="txtFldMgrApproveDate" maxlength="10" readonly>
							              
							                      <!-- <span class="input-group-btn">
							                             <button class="btn btn-info"  onmouseover="showMgrInfo(this);"title=""  id="txtFldMgrName"  type="button"><i class="fa fa-info" ></i></button>
							                      </span> -->	     
							         </div>
						    </div>
						</div>
						
						<div class="row">
							<div class="col-md-12">
							 <label for="" class="text-primary">Remarks:</label>
							  <textarea class="form-control checkdb" id="txtFldMgrApproveRemarks" maxlength="300" readonly>Enter Remarks here...</textarea>
							</div>
						</div>
						
		
                        
                  </div>
                  <div class="tab-pane fade" id="pop2" role="tabpanel" aria-labelledby="pop2-tab">
                       <div class="pt-3"></div>
                       
                       <div class="row">
		   <div class="col-md-6">
		   <label for="" class="text-primary">Status</label>
		    <select class="form-control checkdb" id="txtFldAdminApproveStatus"> 
		        <option value="APPROVE">Approved</option>
		        <option value="REJECT">Rejected</option>
		        <option value="">Pending</option>
		    </select>
		   </div>
		   
		   <div class="col-md-6">
		        <label class="text-primary" id="admnapprLbl" ></label> 
		             <div class="input-group form">
			               <input type="text" class="form-control checkdb" id="txtFldAdminApproveDate" maxlength="10" readonly>
			              
			                      <!-- <span class="input-group-btn">
			                             <button class="btn btn-info"  onmouseover="showMgrInfo(this);"title=""  id="txtFldMgrName"  type="button"><i class="fa fa-info" ></i></button>
			                      </span> -->	     
			         </div>
		    </div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
			 <label for="" class="text-primary">Remarks:</label>
			  <textarea class="form-control checkdb" id="txtFldAdminApproveRemarks" maxlength="300" readonly>Enter Remarks here...</textarea>
			</div>
		</div>
		
                  </div>
                  <div class="tab-pane fade" id="pop3" role="tabpanel" aria-labelledby="pop3-tab">
                       <div class="pt-3"></div>
                        <div class="row">
		   <div class="col-md-6">
		   <label for="" class="text-primary">Status</label>
		    <select class="form-control checkdb" id="txtFldCompApproveStatus"> 
		        <option value="APPROVE">Approved</option>
		        <option value="REJECT">Rejected</option>
		        <option value="">Pending</option>
		    </select>
		   </div>
		   
		   <div class="col-md-6">
		             <div class="input-group form">
			               <input type="text" class="form-control checkdb"  id="txtFldCompApproveDate" maxlength="10" readonly>
			              
			                      <!-- <span class="input-group-btn">
			                             <button class="btn btn-info"  onmouseover="showMgrInfo(this);"title=""  id="txtFldMgrName"  type="button"><i class="fa fa-info" ></i></button>
			                      </span> -->	     
			         </div>
		    </div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
			 <label for="" class="text-primary">Remarks:</label>
			  <textarea class="form-control checkdb" id="txtFldCompApproveRemarks" maxlength="300" readonly>Enter Remarks here...</textarea>
			</div>
		</div>
                  </div>
                  
                </div>


</div>

</div>

<hr/>

<div class="card">
<div class="card-header">Attachment Details
<i class="fa fa-file-archive-o pull-right"  title="Zip all Documents" onclick="downLoadAllFile();alert()"></i>
</div>
<div class="card-body">


<div class="list-group" id="dynaAttachList" style="max-height:60vh;overflow-y:auto">
  
</div>
  


</div>
</div>
 
 </div>
 
 <div class="col-md-7">


<div class="card">
<div class="card-header">View eKYC : <i title="Print FNA Form" class="fa fa-print pull-right" aria-hidden="true"></i>
 
</div>
<div class="card-body" style="overflow-x:auto">

<iframe  style="height:100vh;width:53vw;border:0px;" id="dynaFrame"></iframe>	
</div>
</div>
</div>
 
 
</div>










</div><!-- CONTAINER FLUID END -->
</form>


 <div class="container-fluid"> 
  <div class="row">
    
</div>
</div><!--  -->
 
<!-- The Modal for Approved list -->
 <div class="modal" id="viewApprovHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

   
      <div class="modal-header">
        <h4 class="modal-title">History : Approved List</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      
      <div class="modal-body">
      
        
    <table class="table  table-hover table-bordered " id="approveList">
     <thead  class="thead-light">
      <tr>
            <th>#</th>
             <th>FNA Id</th>
              <th>Client Name</th>
               <th>Advisor Name</th>
               
                 <th>Manager Status</th>
               <th>Admin Status</th>
                <th>Compliance Status</th>       
                          
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
      
     
      </div>
 
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">ok</button>
      </div>

    </div>
  </div>
</div> 


<!-- The Modal for Pending list -->
 <div class="modal" id="viewPendHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

   
      <div class="modal-header">
        <h4 class="modal-title">History : Pending List</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      
      <div class="modal-body">
      <table class="table  table-hover table-bordered " id="pendingList">
     <thead  class="thead-light">
      <tr>
              <th>#</th>
             <th>FNA Id</th>
              <th>Client Name</th>
               <th>Advisor Name</th>
               
                 <th>Manager Status</th>
               <th>Admin Status</th>
                <th>Compliance Status</th>               
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
      </div>
 
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">ok</button>
      </div>

    </div>
  </div>
</div>    


<!-- The Modal for Rejected list -->
 <div class="modal" id="viewRejectHistory">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

   
      <div class="modal-header">
        <h4 class="modal-title">History : Rejected List</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      
      <div class="modal-body">
      <table class="table  table-hover table-bordered " id="rejectList">
     <thead  class="thead-light">
      <tr>
           <th>#</th>
             <th>FNA Id</th>
              <th>Client Name</th>
               <th>Advisor Name</th>
               
                 <th>Manager Status</th>
               <th>Admin Status</th>
                <th>Compliance Status</th>               
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
      </div>
 
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">ok</button>
      </div>

    </div>
  </div>
</div> 


<div class="modal left fade" id="approvalDataModal" tabindex="" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                
                
                <div class="d-flex align-items-center d-none" id="dynaApprovalLoader">
				  <strong>Loading...</strong>
				  <div class="spinner-border ml-auto" role="status" aria-hidden="true"></div>
				</div>


<table id="dynaApprovalDataFNAID" class="table table-striped table-bordered" style="width:100%">
 <thead>
            <tr>
                <th>FNA ID</th>
                <th>Client</th>  
                <th>custid</th>   
                <th>advid</th>   
                      <th>email</th>           
            </tr>
        </thead>
        <tbody>
        </tbody>
</table>                

                    <!-- <div class="nav flex-sm-column flex-row" id="dynaApprovalDataFNAID">                     
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    

</body>

<script src="scripts/Constants.js"></script>
<script src="scripts/commonScript.js"></script>
<script src="scripts/kycApprovalScript.js"></script>
<script src="scripts/lookupJAXreq.js"></script>
<script src="scripts/typeahead.bundle.js"></script>

<script>

$(document).ready(function(){
	
	
  
    //poovathi add on 20-02-2020
    $("#myBtn").click(function(){
    	//alert("topfn");
     //   document.body.scrollTop = 0;
       // document.documentElement.scrollTop = 0;
    });
     
  
  //Get the button
  //var mybutton = document.getElementById("myBtn");

  // When the user scrolls down 20px from the top of the document, show the button
  window.onscroll = function() {
	  //alert("scroll");
	 // scrollFunction();
	  };

  function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
      mybutton.style.display = "block";
    } else {
      mybutton.style.display = "none";
    }
  }

  // When the user clicks on the button, scroll to the top of the document
 function topFunction() {
	  alert("topfn");
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  } 
});

strGlblUsrPrvlg="<%=session.getAttribute("GLBL_USR_PRVLG_LIST")%>";
strFnaMgrUnAppDets=<%=request.getAttribute("FNA_MGR_NOTUPDATED_DETS")%>;
baseUrl = "<%=request.getContextPath()%>";
strAdminFNADets=<%=request.getAttribute("ADMIN_FNA_VERSION_DETS")%>;
strComplianceFNADets=<%=request.getAttribute("COMPLIANCE_FNA_VERSION_DETS")%>;
strALLFNAIds=<%=request.getAttribute("ALL_FNA_IDS")%>;



strMngrFlg="<%= session.getAttribute("MANAGER_ACCESSFLG")%>";

<%-- BIRT_URL = "<%=com.kyc.Utils.KycConst.BIRT_PATH%>";
YUI_URL = "<%=com.kyc.Utils.KycConst.YUIPATH%>"; --%>
COMMON_SESS_VALS = <%=session.getAttribute("COMMON_SESS_OBJ")%>;
		    





  
 



callBodyInit();

$(function() {
	  
	  transition_timeout = 20;
	  
	  $('.title_items').click(function() {
	    
	    current = $(this).next().find('li');
	    
	    $(this).toggleClass('active');
	    current.toggleClass('visible');
	    
	    if ($(this).hasClass('active')) {
	      for( i = 0; i <= current.length; i++ ) {
	        $(current[i]).css('transition-delay', transition_timeout * i + 'ms');
	      }
	    }
	    else {
	      for( i = current.length, j = -1; i >= 0; i--, j++) {
	        $(current[i]).css('transition-delay', transition_timeout * j + 'ms');
	      }
	    }
	  
	  });


	  $('.dropdown-menu li').on('click', function() {
		  var getValue = $(this).text();
		  $('.dropdown-select').text(getValue);
		});
		
	});

</script>

</html>