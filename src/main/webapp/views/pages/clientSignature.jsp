<%@ page language="java" import="com.afas.isubmit.util.PsgConst" %>
<html lang="en">

     <body id="page-top">
         <!-- Container Fluid-->
          <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		 
		 <div class="col-5">
		 <h5>Client Signature Form</h5>
		 </div>
		 
		 <div class="col-md-7">
		           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
		  </div>
		 
</div>

          <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>

<!--Page 14 content area Start  -->
      <div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh">
			<jsp:include page="/views/pages/clientSignContent.jsp"></jsp:include>
     </div><!-- page 14 content Area End -->
      
      
  </div>
        
        <!-- container-fluid-end -->
  
  
  </body>
   
   <script src="vendor/psgisubmit/js/session_timeout.js"></script>
   <script type="text/javascript" src="vendor/psgisubmit/js/Common Script.js"></script>
   <script type="text/javascript" src="vendor/psgisubmit/js/client_signature.js"></script>
  </html>
