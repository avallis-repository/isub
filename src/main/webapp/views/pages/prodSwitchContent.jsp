                  <div class="row">
                      <div class="col-md-6">
                           <div class="card  card-content-fnsize">

						<div class="card-header" id="cardHeaderStyle-22"
							style="font-weight: normal;">
							<div class="row">
								<div class="col-md-11">
									<span> Switching / Replacement </span>
								</div>

							</div>


						</div>
						<div class="card-body">
                              <div class="row">
                               <div class="col-md-12">
									<div class="row">
										<div class="col-md-12">
											<span class=" bold font-sz-level5">What you Should
												Know about Switching / Replacement : </span>
										</div>
										<br>
										<br>
									</div>

									<div class="row">
							               <div class="col-md-12">
							                   <div class="card">
							                         <div class="card-body">
							                              <div class="row">
							                                   <div class="col-md-12">
										                            <span class="italic bold text-primaryy font-sz-level6">1. Fees / Charges </span>
										                            <ul>
																		  <li><span class="font-sz-level7">Disposal of, or reduction in interest on, an existing investment product; and</span></li>
																		  <li><span class="font-sz-level7">Acquisition of, or increase in interest on, a new investment product.</span></li>
																		  
																   </ul>
										                      </div>
							                              </div>
							                         </div>
							                   </div>
							               </div>
							               <div class="col-md-12">
							                    <div class="card">
							                         <div class="card-body">
							                               <div class="row">
							                                   <div class="col-md-12">
										                            <span class="italic bold text-primaryy font-sz-level6 mb-2">2. Possible Disadvantages of Switching or Replacement </span>
										                            <br>
										                            <span class="bold font-sz-level7 pl-3 ">Some of the disadvantages associated with switching / replacement includes:</span>
										                             <ul>
																		  <li><span class="font-sz-level7">You may incur transaction costs without gaining any benefit from the switch / replacement;</span></li>
																		  <li><span class="font-sz-level7">The new investment / insurance product may offer a lower level of benefit at a higher cost or same cost, or
                                                                           offer the same level of benefit at a higher cost;</span></li>
																		  <li><span class="font-sz-level7">You may incur penalties for terminating your existing investment / insurance policies; and</span></li>
																          <li><span class="font-sz-level7">The new investment / insurance product may be less suitable for you.</span></li>
																   </ul>
										                      </div>
							                              </div>
							                         </div>
							                   </div>
							               </div>
							               <div class="col-md-12">
							                    <div class="card">
							                         <div class="card-body">
							                              <div class="row">
							                                   <div class="col-md-12">
										                            <span class="italic bold text-primaryy font-sz-level6 mb-2">3. Fund Switching Facility / Free Switch </span>
										                             <br>
										                            <span class=" bold font-sz-level7 pl-3">Before you perform any switching/replacement of your existing investment / insurance policy:</span>
										                             <ul class="">
																		  <li><span class="font-sz-level7">You should find out whether you are entitled to free switching;</span></li>
																		  <li><span class="font-sz-level7">If there is any fee/charge incurred, you should consider carefully whether the cost and potential disadvantages
                                                                           (if any) would outweigh any potential benefits</span></li>
																		  
																   </ul>
										                      </div>
							                              </div>
							                         </div>
							                   </div>
							               </div>
							               
							               <div class="col-md-12">
							                    <div class="card">
							                         <div class="card-body">
							                              <div class="row">
							                                   <div class="col-md-12">
							                                        <span class="italic bold text-primaryy font-sz-level6">You should seek advice from your financial adviser when in doubt or if you require further clarification.</span>
										                            
										                             
										                            
										                             <ul style="list-style-type: none;" class="mt-2">
																		  <li><span class=" bold font-sz-level6">(a) Sales Charges</span>
																		       <ul>
																		            <li><span class="font-sz-level7">There may be a one-time charge which ranges from 1% to 5% of your investment amount for new purchases</span></li>
																		       </ul>
																		  </li>
																		  
																		  <li><span class=" bold font-sz-level6">(b) Administration / Transaction Charges</span>
																		        <ul>
																		            <li><span class="font-sz-level7">These charges are levied for administration which may be a one-time charge or may be charged periodically.</span></li>
																		       </ul>
																		  </li>
																		  
																		  <li><span class=" bold font-sz-level6">(c) Other Charges</span>
																		        <ul>
																		            <li><span class="font-sz-level7">These may be other charges such as policy fees in the case of insurance products.</span></li>
																		       </ul>
																		  </li>
																		  
																   </ul>
										                      </div>
							                              </div>
							                         </div>
							                   </div>
							               </div>
							       </div>
                              </div>
                              
                             
                              </div>
                             
                                  
                               </div>
                       </div>
                  </div>
                  
                     <div class="col-md-6">
                           <div class="card  card-content-fnsize">
                            
                            <div class="card-header" id="cardHeaderStyle-22" style="font-weight: normal;">
							        <div class="row">
							          <div class="col-md-11">
									 <span>Please complete the questions below. </span>
								
									  </div>
							          
							        </div>
							</div>
							
                              <div class="card-body">
                                 <div class="row">
                                   <div class="col-md-12">
                                        <br>
			                            
			                             <ul class="ln-hei" style="list-style-type: none;">
											  <li class="mb-4">
											  
																						  
												<div class="custom-control custom-checkbox custom-control-inline">
											    <input type="checkbox" class="custom-control-input checkdb swrepConflgC" id="swrepConflgY" name="swrepConflgY" 
											    onclick="chkforOthers(this,'swrepConfDets');setQ1Value(this)">
											    <label class="custom-control-label frm-lable-fntsz" for="swrepConflgY">Yes</label>
											  </div>
											  
											  <div class="custom-control custom-checkbox custom-control-inline">
											    <input type="checkbox" class="custom-control-input checkdb swrepConflgC" id="swrepConflgN" name="swrepConflgN" onclick="setQ1Value(this)">
											    <label class="custom-control-label frm-lable-fntsz" for="swrepConflgN">No</label>
											  </div>
											  
											 <div class="invalid-feedbacks" id="span_swrepConflg"></div>
											 
											 <div class="italic font-sz-level6">  
                                                                   <!-- <input type="checkbox" class="custom-control-input checkdb" name="swrepConflg" id="swrepConflg" onclick="chkforOthers(this,'swrepConfDets');setQValue(this)" 
                                                                   value="N" maxlength="1"/> -->
                                                                   
                                                                   <input type="hidden" id="swrepConflg" name="swrepConflg">
                                                                      <label class="" for=""> <span class="text-primaryy bold">Q1.</span> Are you intending to  <span class="bold">switch / replace</span> or  <span class="bold">have partially surrender</span> or  <span class="bold">terminate</span> any of your existing
                                                                     investment/life &amp; health Insurance policy(ies) for the purpose of this application in the  <span class="bold" style="text-decoration: underline;">next 12 months?</span>
                                                                     If <span class="text-primaryy">Yes </span>, please provide  <span class="bold">reasons /details</span> on Remarks section below:
                                                                     </label>
                                                                     </div>
											       <ul style="list-style-type: none;">
											            <li>
											               <div class="form-group">
													             <label class="frm-lable-fntsz"> Remarks</label>
														          <textarea class="form-control txtarea-hrlines text-wrap checkdb" rows="3" id="swrepConfDets" maxlength="150" name="swrepConfDets" onkeydown="textCounter(this,150);" onkeyup="textCounter(this,150);" disabled></textarea>
												                  <div class="font-sz-level7 mt-2 text-primaryy char-count"></div>
												                </div>
											            
											            </li>
											       </ul>
											  </li>
											  
											  <li class="mb-4">
											  
											  
											<div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input checkdb swrepAdvbyflgC" id="swrepAdvbyflgY" name="swrepAdvbyflgY" onclick="setQ2Value(this)">
    <label class="custom-control-label frm-lable-fntsz" for="swrepAdvbyflgY">Yes</label>
  </div>
  
  <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input checkdb swrepAdvbyflgC" id="swrepAdvbyflgN" name="swrepAdvbyflgN" onclick="setQ2Value(this)">
    <label class="custom-control-label frm-lable-fntsz" for="swrepAdvbyflgN">No</label>
  </div>
											  <div class="invalid-feedbacks" id="spanerrswrepAdvbyflg"></div>
											  
											  <div class="italic font-sz-level6"> 
												    <input type="hidden" class="" name="swrepAdvbyflg" id="swrepAdvbyflg"
												    value="" maxlength="1">
												    <label class="" for=""><span class="text-primaryy bold">Q2.</span> Were you  <span class="bold">advised by</span> a Association of Financial Advisers Representative to switch / replace your existing investment /
                                                                        life &amp; health insurance policy(ies) ?
                                                                        This includes any holdings or products previously bought from any companies, including Association of Financial Advisers
                                                                         Financial.</label></div>
											      
											  </li>
											  
											  <li class="mb-4">
											  
											<div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input checkdb swrepDisadvgflgC" id="swrepDisadvgflgY" name="swrepDisadvgflgY" onclick="setQ3Value(this)">
    <label class="custom-control-label frm-lable-fntsz" for="swrepDisadvgflgY">Yes</label>
  </div>
  
  <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input checkdb swrepDisadvgflgC" id="swrepDisadvgflgN" name="swrepDisadvgflgN" onclick="setQ3Value(this)">
    <label class="custom-control-label frm-lable-fntsz" for="swrepDisadvgflgN">No</label>
  </div>
											  
											  <div class="invalid-feedbacks" id="spanerrswrepDisadvgflg"></div>
											  
											  <div class="italic font-sz-level6">
                                                                       <input type="hidden" class="" name="swrepDisadvgflg" id="swrepDisadvgflg" 
                                                                       value="" maxlength="1"> 
                                                                          <label class="" for=""> <span class="text-primaryy bold">Q3.</span> Have you been advised by a Association of Financial Advisers Representative on the  <span class="bold">possible costs and disadvantages</span>
                                                                       associated with switching / replacement of your existing investment/insurance policy(ies) ?</label></div>
											        <!-- <ul style="list-style-type: none;">
											            <li><div class="form-group ">
													             <label class="frm-lable-fntsz"> Remarks</label>
														          <textarea class="form-control txtarea-hrlines text-wrap" rows="3" id="comment"></textarea>
												        </div></li>
											       </ul> -->
											  </li>
											  
											  <li class="mb-4">
											  
											  
											<div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input checkdb swrepProceedflgC" id="swrepProceedflgY" name="swrepProceedflgY" onclick="setQ4Value(this)">
    <label class="custom-control-label frm-lable-fntsz" for="swrepProceedflgY">Yes</label>
  </div>
  
  <div class="custom-control custom-checkbox custom-control-inline">
    <input type="checkbox" class="custom-control-input checkdb swrepProceedflgC" id="swrepProceedflgN" name="swrepProceedflgN" onclick="setQ4Value(this)">
    <label class="custom-control-label frm-lable-fntsz" for="swrepProceedflgN">No</label>
  </div>
											   <div class="invalid-feedbacks" id="spanerrswrepProceedflg"></div>
											  
											  <div class="italic font-sz-level6"> 
                                                                   <input type="hidden" id="swrepProceedflg" name="swrepProceedflg" 
                                                                   value="" maxlength="1">
                                                                              <label class="" for=""><span class="text-primaryy bold">Q4.</span> Do you still <span class="bold">wish to proceed </span> with the switching / replacement despite knowing the possible costs and
                                                                            disadvantages?</label></div>
											       <!--  <ul style="list-style-type: none;">
											            <li><div class="form-group">
													             <label class="frm-lable-fntsz"> Remarks</label>
														          <textarea class="form-control txtarea-hrlines text-wrap" rows="3" name="swrepRemarks" id="swrepRemarks" onkeydown="textCounter(this,300);"	onkeyup="textCounter(this,300);"></textarea>
												        </div></li>
											       </ul> -->
											  </li>
											  
									   </ul>
				                      </div>
							      </div> 
                               </div>
                       	</div>


		</div>

</div>