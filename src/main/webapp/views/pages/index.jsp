 <!DOCTYPE html>
<html lang="en">

<head>
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css"> 
 <link rel="stylesheet" href="vendor/psgisubmit/css/index.css"/>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/4.0.0/crypto-js.min.js"></script>
</head>

<body id="page-top">
 <div id="wrapper" >
    
    <div id="content-wrapper" class="d-flex flex-column">
     <!-- Container Fluid-->
        <div class="container" id="container-wrapper" style="min-height: 100vh;padding: 6rem 3rem; box-sizing: border-box;">
         
		<div class="card" style="border:1px solid #044CB3;">
                <div class="card-header d-flex flex-row align-items-center justify-content-between" id="cardHeaderStyle-22">
                  <h5 class="m-0" style="font-weight:normal;">Financial Need Analysis V4.1</h5>
                </div>
                <div class="card-body" >
                  <div class="row">
				  
				  <div class="col-xs-12 col-sm-2">
					&nbsp;
				  </div>
				  
				  <div class="col-xs-12 col-sm-4">
                        <div class="card ">
						
							<div class="card-content text-center">
                                <img src="vendor/psgisubmit/img/addUser.png" class="imgSize"/>
								<div></div>
								<h6 class="card-title p-3" style="font-size: 13px;">
                                    Add New Client details 
                                  </h6>
							</div>
							
							<div class="card-read-more text-center">
                                 <input type="button" class="btn btn-sm  btn-bs3-prime btnshake btnClntAddSrch" value="  Proceed " id="btnAddClnts"   onclick='window.location.href="addClientInfo";'/>
                            </div>
                           
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <div class="card ">
						
							<div class="card-content text-center">
                                <img src="vendor/psgisubmit/img/srchUser.png" class="imgSize"/>
								<div></div>
								<h6 class="card-title p-3" style="font-size: 13px;">
                                    Search for existing Client details
                                  </h6>
							</div>
							
							<div class="card-read-more text-center">
                                 <input type="button" class="btn btn-sm  btn-bs3-prime btnClntAddSrch" value="  Proceed " data-toggle="modal"    data-target="#srchClntModal" id="#btnSrchClint" />
                            </div>
                           
                        </div>
                    </div>
					<!-- <div class="col-xs-12 col-sm-2">
					&nbsp;
				  </div> -->
					</div>
				   
				 
				 
                </div>
              </div>
          
        </div>
        <!---Container Fluid-->
 		  <!-- Modal Scrollable -->
          <div class="modal fade" id="srchClntModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
            aria-labelledby="srchClntModalTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-scrollable modal-xl" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="srchClntModalTitle"><img src="vendor/psgisubmit/img/srch.png">&nbsp;&nbsp;Search Client</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                
                <div class="col-lg-12">
              <div class="card mb-4" >
              
              <div class="card-body" id="secOverlay">
              
              <div class="row">
                   <div class="co-12">
                   <small class="pl-3 noteInfoMsg" title="Note Information"><img src="vendor/psgisubmit/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;<span id="noteInfo">Keyin client Name and Click Search Button !</span></small>
                   </div>
              </div>
              
              <div class="row mt-2">
                   <div class="col-8">
                       <label class="frm-lable-fntsz pl-3" for="">CustName</label>
									<div class="input-group input-group-sm mb-3 pl-3">
									  <div class="input-group-prepend">
										<span class="input-group-text p-1"><img src="vendor/psgisubmit/img/srchClnt.png" class="pl-1" style="width: 80%;"></span>
									  </div>
									  <input type="text" class="form-control checkdb border-primary" maxlength="60" id="txtFldCustName" name="txtFldCustName" placeholder="Search Client Names here...">
									 
									</div>
									
									<div class="invalid-feedbacks pl-3 hide" id="txtFldCustNameError">
                                             Keyin CustName
                                     </div>
									
                   </div>
                   
                   <div class="col-4">
                       <button class="btn  btn-sm btn-bs3-prime mt-4 font-sz-level6" id="#btnSrchClint" onclick="getAllCustomerDetails()"><img src="vendor/psgisubmit/img/clntSrch.png" style="width: 26%;">&nbsp; Search</button>
                   </div>
              </div>
              
              <div class="row hide" id="srchClntTblSec">
                   <div class="col-12">
	                   <div id="srchClntLoader"> 
	                       <img src="vendor/psgisubmit/img/srchLoader.gif" style="width:15%;" class="rounded mx-auto d-block">
	                    </div>
	                    
	                     <div class="table-responsive p-2" >
			                  <table class="table align-items-center table-flush table-striped table-hover" id="srchClntTable" style="overflow-x: hidden; overflow-y: scroll;">
			                    <thead class="thead-light">
			                      <tr>					    
			                        <th><div style="width:200px">Name</div></th>
			                        <th><div style="width:90px">Initials</div></th>
			                        <th><div style="width:90px">NRIC</div></th>
			                       	<th><div style="width:90px">DOB</div></th>
			                        <th><div style="width:90px">Contact</div></th>
			                        <th><div style="width:90px">Cust. Type</div></th>
			                         
			                        <th></th>
			                      </tr>
			                    </thead>
			                   
			                    <tbody>
			                      
			                    </tbody>
			                  </table>
                        </div>
                   
                   
                   </div>
              </div>
              
              
                
               
                </div>
              </div>
            </div>
                  
                  
                </div>
                <div class="modal-footer float-left p-1">
				
                  <button type="button" class="btn btn-sm pl-3 pr-3 btn-bs3-prime" data-dismiss="modal">OK</button>
                  
                </div>
              </div>
            </div>
          </div>
    
    </div>
    
    </div>



       

<script src="vendor/psgisubmit/js/Common Script.js"></script>
<script src="vendor/psgisubmit/js/index.js"></script>
</body>
</html>