<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.afas.isubmit.util.PsgConst"%>
<html lang="en">

<head>
 
    <link rel="stylesheet" href="css/bs_style.css">

   <link rel="stylesheet" href="css/app/advisor-declaration.css">
 

<body id="page-top">
 
        <!-- Container Fluid-->
           <div class="container-fluid" id="container-wrapper" style="min-height:75vh">
		<div id="frmClientDeclForm">
		<input type="hidden" id="hdnSessFnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
		
		<div class="d-sm-flex align-items-center justify-content-between mb-1">
		 
		 <div class="col-6">
		      <h5>Client's Declaration&amp;&nbsp;Acknowledgement</h5>
		</div>
		
		<div class="col-md-6">
		           <jsp:include page="/views/pages/policyE-Submission.jsp"></jsp:include>
		  </div>
         
</div>

    <div class="row">
             <jsp:include page="/views/pages/signApproveStatus.jsp"></jsp:include>
         </div>

<!--Page 14 content area Start  -->
      <div class="card mb-4 " style="border:1px solid #044CB3;min-height:65vh;">
			<div class="card-body" style="font-size: 13px;">
                  <jsp:include page="/views/pages/advisorDeclrContent.jsp"></jsp:include>
   			</div>
   			<!-- page 14 content Area End -->
  </div>
   </div>
        <!-- End Container Fluid -->
    </div>  
 
  </body>
  
          <script src="vendor/jquery-easing/jquery.easing.min.js"></script> 
          <!-- <script src="vendor/psgisubmit/js/ekyc-layout.js"></script> -->
          <script src="js/jquery.steps.js"></script>
          <script src="js/select2.js"></script>
          <script src="vendor/psgisubmit/js/session_timeout.js"></script>
		  <script type="text/javascript" src="vendor/psgisubmit/js/Common Script.js"></script>
          <script type="text/javascript" src="vendor/psgisubmit/js/fna_common.js"></script>
          <script src="vendor/psgisubmit/js/advisor_declaration.js"></script>
          
           
  </html>