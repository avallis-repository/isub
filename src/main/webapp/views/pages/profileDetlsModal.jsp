<!-- Pofile Modal -->
	<div class="modal fade" id="profileModal">
		<div class="modal-dialog ">
			<div class="modal-content">

				<!-- Modal Header -->
				<div class="modal-header" id="cardHeaderStyle-22">
					<h4 class="modal-title">Profile Details</h4>
					<button type="button" class="close white" data-dismiss="modal">&times;</button>
				</div>

				<!-- Modal body -->
				<div class="modal-body">
					<ul class="list-group">
					
					<li class="list-group-item"><span
							class="text-secondary bold italic"><img
								src="vendor/psgisubmit/img/profile.png"> Manager Details</span>

							<div class="row">
								<div class="col-6">
									<small><span class="text-primaryy  italic">
											Manager Name : </span><span> Salim M Amin</span></small>
								</div>
								<div class="col-6">
									<small><span class="text-primaryy  italic">
											Manager Rep RNF : </span><span>Test</span></small>
								</div>
							</div>
							<div class="row">
								<div class="col-6">
									<small><span class="text-primaryy  italic">Manager
											POS:</span><span> Test</span></small>
								</div>
								<div class="col-">
									<small><span class="text-primaryy  italic">RNf
											of rep's Supervisor : </span><span>Test</span></small>
								</div>
							</div></li>
					
						<li class="list-group-item"><span
							class="text-secondary bold italic"><img
								src="vendor/psgisubmit/img/profile.png"> Advisor Details</span>

							<div class="row">
								<div class="col-6">
									<small><span class="text-primaryy  italic">Advisor
											RNF No. :</span><span> REPJG56D5</span></small>
								</div>
								<div class="col-6">
									<small><span class="text-primaryy  italic">Advisor
											POS : </span><span>Test</span></small>
								</div>
							</div></li>
						

					</ul>
				</div>

				<!-- Modal footer -->
				<div class="modal-footer">
					<button type="button" class="btn btn-sm btn-bs3-prime"
						data-dismiss="modal">Close</button>
				</div>

			</div>
		</div>
	</div>

	<!-- Profile Modal End-->