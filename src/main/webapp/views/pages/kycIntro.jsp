<%@ page language="java" import="com.afas.isubmit.util.PsgConst" %>
<html lang="en">

<head>

<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link href="vendor/psgisubmit/css/jquery.steps.css" rel="stylesheet">
<link href="vendor/psgisubmit/css/style_checkboxs.css" rel="stylesheet">
</head>

<body id="page-top">

	<!-- Container Fluid-->
	<div class="container " id="container-wrapper" style="
	">
	<input type="hidden" id="hdnSessfnaId" name="hdnSessfnaId" value="<%=session.getAttribute(PsgConst.SESS_FNA_ID)%>"/>
    <input type="hidden" id="hdnFnaDetails" value='${FNA_DETAILS}'>
		<!-- Card Content Start -->
		    
		    <div class="card" style="border:1px solid #044CB3;">
                <div class="card-header" id="cardHeaderStyle-22">
                <div class="row">
                      <div class="col-6">
                           <h5 class="m-0  text-custom-color">Financial Need Analysis V4.1</h5>
                      </div>
                      
                      <div class="col-6" id="kycIntroNote">
                             <input type="hidden" value="${CustomerId}" id="custIdInfo">
                             <div class="badge badge-pill badge-light  o_ellipsis p-1 "  title="${CustomerName}" onclick="showClientInfo();">Client Name&nbsp;:&nbsp;${CustomerName}</div>
                             <div class="badge badge-pill badge-light p-1 o_ellipsis hide" id="noteInfoFnaId"></div>
                      </div>
                </div>
                
                </div>
               
               
                <div class="card-body " >
		         <div id="KycIntroFrm">
		         
		        
		      
		         				<div id="wizard">
                <h2><i class="fa fa-bell" aria-hidden="true" ></i>&nbsp;Notice to Clients</h2>
                <section>
                     <div class="card my-1 p-3 bg-white rounded shadow-sm" >
      <div class="card-header" id="cardHeaderStyle"style="color: #2a4fb6;"><i class="fa fa-bell" aria-hidden="true" ></i>&nbsp;<span class="font-sz-level3" >&nbsp;Please read the below details Carefully</span></div>
    <div class="" style="padding: 0px 16px;">
    
    <div class="media text-muted pg1Contentstyle mt-2">
      <i class="fa fa-check-circle pt-1"></i>&nbsp;&nbsp;
      <label class="media-body  mb-0 font-sz-level6  lh-125  border-gray text-custom-color-gp ">
        
        Association of Financial Advisers Pte Ltd is registered as a Licensed Financial Adviser under the Financial Advisers Act (Cap. 110).
      </label>
    </div>
    <div class="media text-muted pg1Contentstyle mt-2">
      <i class="fa fa-check-circle pt-1"></i>&nbsp;&nbsp;
      <label class="media-body  mb-0 font-sz-level6 lh-125  border-gray text-custom-color-gp">
       
        Association of Financial Advisers Pte Ltd has adopted the use of the term <strong>" Independent "</strong> to ensure that it is able to provide objective and impartial advice to meet client financial needs.
      </label>
    </div>
    <div class="media text-muted pg1Contentstyle mt-2">
      <i class="fa fa-check-circle pt-1"></i>&nbsp;&nbsp;
      <label class="media-body  mb-0 font-sz-level6 lh-125  border-gray text-custom-color-gp">
        The information you have provided to our representative is strictly confidential and is only to be used for the purpose of fact finding in order to recommend suitable products and shall not be used for any other purpose(s).
      </label>
    </div>
	
	<div class="media text-muted pg1Contentstyle mt-2">
      <i class="fa fa-check-circle pt-1"></i>&nbsp;&nbsp;
      <label class="media-body  mb-0 font-sz-level6 lh-125  border-gray text-custom-color-gp">
        We emphasize on the importance of providing accurate and complete information to enable us in making appropriate advisory and recommendations.
      </label>
    </div>
	
	<div class="media text-muted pg1Contentstyle mt-2">
      <i class="fa fa-check-circle pt-1"></i>&nbsp;&nbsp;
      <label class="media-body  mb-0 font-sz-level6 lh-125  border-gray text-custom-color-gp">
        However, during the analysis and advisory of our financial needs,we may sometimes use subjective assumptions to perform appropriate recommendations at that point in time and may not reflect the actual occurrence in the future event.
      </label>
    </div>
	 
	 </div>
	
	</div>
   </section>

                <h2><i class="fa fa-file-text"  aria-hidden="true"></i>&nbsp;Declaration</h2>
                 <section>
				<div class="card my-1 p-3 bg-white rounded shadow-sm" id="repDecTbl">
			                 <div class="card-header" id="cardHeaderStyle" style="color: #2a4fb6;">
			                 <i class="fa fa-file-text"  aria-hidden="true"></i>&nbsp;
			                 <span class="font-sz-level3">As your Representative, I am licensed to perform the  financial advisory services&nbsp;</span></div>
					         
							 
							 <div class="card-content text-center" id="advDclSec">
							 
							 <div class="invalid-feedbacks hide" id="appClntChioceError">Select any one of the option below !</div>
							 
							 <div  id="Sec2ContentFormId">
							  
							   <div class="inputGroup">
							    <input type="checkbox" class="checkdb" onclick="chkJsnOptions(this,'advdecOptions');slctAllDeclTypes(this)" data="NO1ALL" name="htfrepdecno1all" id="htfrepdecno1all"/>
							    <label for="htfrepdecno1all" class="font-sz-level6">Below all Financial Advisory Services</label>
							  </div>
							  
							  
							  <div class="inputGroup">
							    <input type="checkbox" class="checkdb" onclick="chkJsnOptions(this,'advdecOptions');" data="NO1A" name="htfrepdecno1a" id="htfrepdecno1a"/>
							    <label for="htfrepdecno1a" class="font-sz-level6">Life Insurance Policies</label>
							  </div>
							  
							  <div class="inputGroup">
							    <input  type="checkbox" class="checkdb" onclick="chkJsnOptions(this,'advdecOptions');" data="NO1B" name="htfrepdecno1b" id="htfrepdecno1b"/>
							    <label for="htfrepdecno1b" class="font-sz-level6">Units in Collective Investment Schemes</label>
							  </div>
							  
							  <div class="inputGroup">
							    <input  type="checkbox" class="checkdb" onclick="chkJsnOptions(this,'advdecOptions');" data="NO2" name="htfrepdecno2" id="htfrepdecno2"/>
							    <label for="htfrepdecno2" class="font-sz-level6">Advising on Investment products(Unites in Collective Investment Schemes).</label>
							    <!-- <input type="checkbox" onchange="chkAdvDecOptions(this);slctDslectAll()" value="AHPROD" name="htfrepdecah" id="htfrepdecah" style="display:none"/> -->
							  </div>
							  
							  <div class="inputGroup">
							    <input  type="checkbox" class="checkdb" onclick="chkJsnOptions(this,'advdecOptions');" data="NO3" name="htfrepdecno3" id="htfrepdecno3"/>
							    <label for="htfrepdecno3" class="font-sz-level6">Advising and Arranging contracts of insurance in respect of Life Insurance policies</label>
							  </div>
							  
							  <div class="inputGroup">
							    <input  type="checkbox" class="checkdb" onclick="chkJsnOptions(this,'advdecOptions');" data="NO4" name="htfrepdecno4" id="htfrepdecno4"/>
							    <label for="htfrepdecno4" class="font-sz-level6" >"Accident & Health (A&H) product" includes any policy that
															 provides financial benefits in the event of <br> accident, illness or disability.</label>
							  </div>
							  
							  <div class="inputGroup">
							    <input  type="checkbox" class="checkdb" onclick="chkJsnOptions(this,'advdecOptions');" data="NO5" name="htfrepdecno5" id="htfrepdecno5"/>
							    <label for="htfrepdecno5" class="font-sz-level6">"Sales of General Insurance product"includes automobile and homeowners polices
															 that provides financial<br> benefits in the event of financial loss.</label>
							  </div>
							  
							  

                                <input type="hidden" name="advdecOptions" id="advdecOptions" maxlength="150" />
			                    
							</div>
							 </div>
              </div>
                </section>

                <h2><i class="fa fa-cubes" aria-hidden="true"></i>&nbsp;Application Types</h2>
                <section>
				
				<div class="card my-1 p-3 bg-white rounded shadow-sm" >
			                 <div class="card-header" id="cardHeaderStyle" >
			                     <div class="row">
			                       <div class="col-10">
			                              <i class="fa fa-cubes" aria-hidden="true" style="color: #2a4fb6;"></i>&nbsp;<span class="font-sz-level3 "style="color: #2a4fb6;">Types of Advice (Client Choice)&nbsp;</span>
			                       </div>
			                       <div  class="col-">
			                           <div class="custom-control custom-switch hide d-none">
											  <input type="checkbox" class="custom-control-input checkdb" id="chkSelectAll" onclick="slectAllAppType(this)">
											  <label class="custom-control-label" for="chkSelectAll" title="Select All Choices"  style="color: #2a4fb6;">Select All</label>
                                      </div>
			                       </div>
			                 </div>
			                 </div> 
					          
							 <div class="card-content" style="padding: 0px 16px;">
								<div class="row mt-2">
									<div class="col-12">
										<div class="card  shadow-sm card-btm-bor-style1">
											 <div class="card-body">
												<label class="media-body  mb-0 font-sz-level6 lh-20 text-custom-color-gp"> I / We understand that the product(s) recommended by the Representative without the completion of the fact find, or with any inaccurate information provided me / us may result in inaccurate recommendation being inappropriate to my / our needs.</label>
												</div>
										</div>
									</div>
									
									
								</div>
								
								<div class="row mt-1">
									<div class="col-8">
										<div class="invalid-feedbacks hide" id="life12Error">
											 select any one of the Options below
										</div>
									</div>
									
									<div class="col-4">
										<div class="invalid-feedbacks hide" id="repInvExtFlg">
											 select any one  Options below
										</div>
									</div>
								</div>
								
								
								<div class="row mt-2" id="AppTypeSec">
								
									<div class="col-4">
									<div class="card-deck  h-100">
											<div class="card  shadow-sm h-100 card-btm-bor-style2" id="life1Error">
											  
											  <div class="card-body">
												<label class="media-body  mb-0 font-sz-level6 lh-20 text-custom-color-gp"> 
												    <div class="custom-control custom-switch" class="spnAppchoicClsFull" id="spnOptnOneLandN">
                                                         <input type="checkbox" class="custom-control-input checkdb" onclick="fnaAssgnFlgs(this,'chk');validateApplcntType();chkAdvAppTypeOptions(this)" name="htfcclife1" id="htfcclife1"  data="LIF1"/>
                    									 <label class="custom-control-label" for="htfcclife1">&nbsp; I / We would like to complete the full needs analysis in this review before receiving any financial advice.</label>
                                                   
		                                                   <span class="spnAppchoicClsFull" id="spnOptnOneAandH" style="display:none"> 
									                              <input type="checkbox" class="checkdb" onclick="fnaAssgnFlgs(this,'chk');validateApplcntType();chkAdvAppTypeOptions(this)" name="htfccah1" id="htfccah1" Style="display:none;"/>
									                       </span>
                                                   </div>
												</label>
												 
											  </div>
											</div>
										</div>
									</div>
									<div class="col-4">
									     <div class="card-deck  h-100">
											<div class="card  shadow-sm h-100 card-btm-bor-style4" id="life2Error">
											   
											  <div class="card-body">
												  <label class="media-body  mb-0 font-sz-level6 lh-20 text-custom-color-gp"> 
												    <div class="custom-control custom-switch" class="spnAppchoicCls" id="spnOptnTwoLandN">
                                                         <input type="checkbox" class="custom-control-input checkdb" onclick="fnaAssgnFlgs(this,'chk');validateApplcntType();chkAdvAppTypeOptions(this)"	name="htfcclife2" id="htfcclife2" data="LIF2"/>
                    									 <label class="custom-control-label" for="htfcclife2">&nbsp; I / We do not wish to complete the full needs analysis in this review but would like to receive product advice on specific need(s) planning.
                    									 </label>
                    									 
                    									 <span class="spnAppchoicCls" id="spnOptnTwoLyfAandH" style="display:none"> 
							                                   <input type="checkbox" class="checkdb" onclick="fnaAssgnFlgs(this,'chk');validateApplcntType();chkAdvAppTypeOptions(this)" name="htfccah2" id="htfccah2" data="AH2" Style="display:none"/>
							                             </span>
                    									 
                    									 
                                                   </div> 
												 </label>
												 
												 <input type="hidden" name="apptypesClient" id="apptypesClient" maxlength="150" />
                                              </div>
											</div>
										</div>
									</div>
									
									
									
									<div class="col-4">
									 
										
										<div class="card-deck h-100">
											<div class="card shadow-sm h-100 card-btm-bor-style3" id="repInvExistFlgError">
											   
											  <div class="card-body">
												 <div class="media-body  mb-0 font-sz-level6 lh-20 text-custom-color-gp"> 
												      <div class="custom-control custom-checkbox custom-control-inline">
													    <input type="checkbox" class="custom-control-input checkdb" id="ccRepexistinvestflgY" name="ccRepexistinvestflgY" onclick="toggleExistInvest(this);">
													    <label class="custom-control-label frm-lable-fntsz" for="ccRepexistinvestflgY">Yes</label>
													  </div>
													  
													  <div class="custom-control custom-checkbox custom-control-inline">
													    <input type="checkbox" class="custom-control-input checkdb" id="ccRepexistinvestflgN" name="ccRepexistinvestflgN"  onclick="toggleExistInvest(this);">
													    <label class="custom-control-label frm-lable-fntsz" for="ccRepexistinvestflgN">No</label>
													  </div>
													  <div>
													      <small class=" mb-0 font-sz-level6 lh-20 text-custom-color-gp">Is any part of this application intended to switch / replace / surrender / terminate 
													  your existing Investment holdings or Life / Health Insurance polices?</small>
													  </div>
													  <input type="hidden"  name="ccRepexistinvestflg" id="ccRepexistinvestflg" value="" maxlength="1" Style="display:none;"/>
												    
												 </div>
											  </div>
											</div>
										</div>
									</div>
									
								</div>
								
								
								
							 </div>
							 
				</div>
				
				 
                  
	 
                </section>

                 
            </div>
				</div>
		</div>
        
		   
		  
		  
          <!--Row-->
		    
		   
		
		<!-- Card Content End  -->

	<div class="modal fade" id="genKYCModalScrollable"
			data-backdrop="static" data-keyboard="false" tabindex="-1"
			role="dialog" aria-labelledby="genKYCModalScrollableTitle"
			aria-hidden="true">
			<div class="modal-dialog modal-dialog-scrollable modal-lg"
				role="document">
				<div class="modal-content">
					<div class="modal-header" id="cardHeaderStyle-22">
						<h6 class="modal-title" id="genKYCModalScrollableTitle">Loading...</h6>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close" style="color: #fff;">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<div class="col-lg-12">

							<div class="loader">
								<p></p>
							</div>

						</div>


					</div>
					<div class="modal-">
						<button type="button" class="btn btn-sm btn-bs3-prime"
							data-dismiss="modal">OK</button>

					</div>
				</div>
			</div>
		</div>


	</div>

	</div>
	<script src="vendor/psgisubmit/js/jquery.steps.js"></script>
	<script src="vendor/psgisubmit/js/fna_common.js"></script>
    <script type="text/javascript" src="vendor/psgisubmit/js/kyc_intro.js"></script>
    <!-- <script>latestFNADetails =${LATEST_FNA_DETAILS};</script> --> 
    <script src="vendor/psgisubmit/js/Common Script.js"></script>

</body>

</html>