<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
 <title>AFAS|ISUBMIT|eSignature</title>
  <meta name="description" content="Let the user draw a Shape using the mouse or finger without any constraints." />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Copyright 1998-2019 by Northwoods Software Corporation. -->
  
 
  
  
  
   <link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
  <link href="vendor/psgisubmit/css/ekyc-layout.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="vendor/psgisubmit/css/stylesheet.css"/>
  
  <link href="vendor/SweetAlert2/css/sweetalert2.css">
  <link href="vendor/SweetAlert2/css/sweetalert2.min.css"> 
  
  <script src="vendor/jquery/jquery.js"></script>
  <script src="vendor/jquery/popper.min.js"></script>
  <script src="vendor/bootstrap453/js/bootstrap.js"></script>
  <script src="js/modernizr.custom.js"></script>  
  
          <script src="vendor/SweetAlert2/js/sweetalert2.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.js"></script> 
  
  
 <script src="vendor/psgisubmit/js/gojs/go.js"></script>
  <script src="vendor/psgisubmit/js/gojs/FreehandDrawingTool.js"></script>
  <script src="vendor/psgisubmit/js/gojs/GeometryReshapingTool.js"></script>
 
  <script src="vendor/psgisubmit/js/scan_sign.js"></script>
  
   <style>
  
  body{
  overflow-y: scroll;
  overflow-x: hidden;
  height:100%;
 }
  .lblSz{
    color: #2b8cd2;
    font-size: 14px;
  }
  
  .signSec{
    border: 1px dotted grey;
    background: #6da0de;
    padding: 10px 0rem;
    color: #fff;
    border-radius: 1px;
  }
  
  .spnTxt{
    font-size: 12px;
    padding-left: 1em;
  }
  
  #noteInfo{
    font-size: 16px;
    border-bottom: 3px solid #89d4d4;
  }
  
  
  .disabledsec{
    pointer-events: none;
    opacity: 0.4;
}
  
  
/*   ::-webkit-scrollbar {
    -webkit-appearance: none;
}

::-webkit-scrollbar:vertical {
    width: 12px;
}

::-webkit-scrollbar:horizontal {
    height: 12px;
}

::-webkit-scrollbar-thumb {
    background-color: rgba(0, 0, 0, .5);
    border-radius: 10px;
    border: 2px solid #ffffff;
}

::-webkit-scrollbar-track {
    border-radius: 10px;
    background-color: #ffffff;
} */



/*body spinner*/

#cover-spin {
    position:fixed;
    width:100%;
    left:0;right:0;top:0;bottom:0;
    background-color: rgba(255,255,255,0.7);
    z-index:9999;
    display:block;
    
    
}



@-webkit-keyframes spin {
	from {-webkit-transform:rotate(0deg);}
	to {-webkit-transform:rotate(360deg);}
}

@keyframes spin {
	from {transform:rotate(0deg);}
	to {transform:rotate(360deg);}
}

#cover-spin::after {
    content:'';
    display:block;
    position:absolute;
    left:48%;top:40%;
    width:40px;height:40px;
    border-style:solid;
    border-color:black;
    border-top-color:transparent;
    border-width: 4px;
    border-radius:50%;
    -webkit-animation: spin .8s linear infinite;
    animation: spin .8s linear infinite;
}

/*body spinner*/

  </style>
 
</head>
<body>

 <div class="container mt-5">
	   <div class="row">
       <div class="col-md-2"></div>
        <div class="col-sm-12 col-md-8">
            <div class="card p-2 text-center" style="border: 1px solid #c2dbfc;">
                <div class="row">
                    <div class="col-md-6">
                        <div class="row signSec ml-2 mr-2 p-1">
                            
                            <div class="col-4">
                                 <img class="mt-1" alt="AfaS_Logo"  src="vendor/psgisubmit/img/logo/logo.png" style="width: 100%;background: #fff;border-radius:30px;">
                            </div>
                            
                            <div class="col-3">
                                 <img src="vendor/psgisubmit/img/profileUser.png" width="" class="" style="width: 100%;">
                            </div>
                            
                             <div class="col-5">
                                <div class="mt-2 ml-n2"><small class="" title="Note Information"><span id="noteInfo">Client Details</span></small></div>
                             </div>
                           
                              </div>
                        
                             
                       
                            <div class="mt-1">
                                <table class="table table-borderless table-hover">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="d-flex flex-column"><label class="text-left lblSz "><img src="vendor/psgisubmit/img/client.png" class="">Client Name</label><span class="text-left spnTxt">${clientName}</span> </div>
                                            </td>
                                            <td>
                                                <div class="d-flex flex-column"><label class="text-left lblSz "><img src="vendor/psgisubmit/img/Nric.png"  class=""> NRIC</label><span class="text-left spnTxt">${nric}</span> </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="d-flex flex-column"><label class="text-left lblSz"><img src="vendor/psgisubmit/img/calendar.png"  class=""> DOB</label><span class="text-left spnTxt">${dateOfBirth}</span> </div>
                                            </td>
                                            <td>
                                                <div class="d-flex flex-column"><label class="text-left lblSz"><img src="vendor/psgisubmit/img/contact.png"  class=""> Phone No</label><span class="text-left spnTxt">${phoneNo}</span> </div>
                                                
                                                <input type="hidden" name="fnaid" id="fnaValue" value="${fnaId}"/>
		                                        <input type="hidden" name="perType" id="perType" value="${perType}"/>
		                                        <input type="hidden" name="curPage" id="curPage" value="${curPage}"/>
											    <input type="hidden" name="imgId" id="imgValue" />
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            
                            
                            
                        
                    </div>
                    
                    
                    <div class="col-md-6">
                    
                    <div class="row">
                            <div class="col-12">
                               <div class="signSec">
                                    <img src="vendor/psgisubmit/img/SignPad.png" width="10%" class=""> Signature Section (${signTitle})
                               </div>
                                
                            </div>
                      </div>
                        
                      <div class="row">
                               <div class="col-12" id="sample">
                                   <div id="myDiagramDiv" style="border:1px solid #6ca0de; width: 100%; height: 30vh;">
                                    </div>
                               </div>
                               <small class="p-1 ml-3 m-1" title="Note Information"><img src="vendor/psgisubmit/img/infor.png" alt="Note" class="img-rounded mr-2">&nbsp;<span id="spanSignHelpText">Draw your Sign and Click Save Button</span></small>
                            </div>
                            
                            <div class="row aftersignature">
                               <div class="col-1"></div>
                               
                               <div class="col-4">
                                    <button class="btn btn-sm btn-warning btn-block" onclick="clear11()"><i class="fa fa-times" aria-hidden="true"></i>&nbsp;Clear</button>
                               </div>
                               
                               <div class="col-6">
                                   <button class="btn btn-sm btn-success btn-block" onclick="personSave()"><i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save Signature</button>
                               </div>
                               
                               <div class="col-1"></div>
                           </div>
                           
                             <div class="row">
                               <div class="col-12">
                                    <form name="testform">
                                      <input type="hidden" name="esignId" id="esignId"/>
                                               <textarea id="mySavedDiagram" name="mySavedDiagram" style="width:100%;height:30vh;display:none">
		                                             { "position": "0 0",
													  "model": { "class": "GraphLinksModel",
													  "nodeDataArray": [],
													  "linkDataArray": []} }
											  </textarea>
									  </form>
                               
                               
                               </div>
                               
              
                        </div>
                    </div>
                </div>
            </div>
        </div>
          <div class="col-md-2"></div>
    </div>
</div>

</body>
<script type="text/javascript" src="vendor/psgisubmit/js/Common Script.js"></script>
<script src="vendor/psgisubmit/js/session_timeout.js"></script>
<script id="code">
  var myDiagram;
    function init() {
      // if (window.goSamples) goSamples();  // init for these samples -- you don't need to call this
      var $ = go.GraphObject.make;
      myDiagram = $(go.Diagram, "myDiagramDiv", {allowHorizontalScroll: true, allowVerticalScroll: true});
      myDiagram.toolManager.mouseDownTools.insertAt(3, new GeometryReshapingTool());
      myDiagram.nodeTemplateMap.add("FreehandDrawing",
        $(go.Part,
          { locationSpot: go.Spot.Center, isLayoutPositioned: false },
          new go.Binding("location", "loc", go.Point.parse).makeTwoWay(go.Point.stringify),
          {
            selectionAdorned: true, selectionObjectName: "SHAPE",
            selectionAdornmentTemplate:  // custom selection adornment: a blue rectangle
              $(go.Adornment, "Auto",
                $(go.Shape, { stroke: "dodgerblue", fill: null }),
                $(go.Placeholder, { margin: -1 }))
          },
          { resizable: true, resizeObjectName: "SHAPE" },
          { rotatable: true, rotateObjectName: "SHAPE" },
          { reshapable: true },  // GeometryReshapingTool assumes nonexistent Part.reshapeObjectName would be "SHAPE"
          $(go.Shape,
            { name: "SHAPE", fill: null, strokeWidth: 1.5 },
            new go.Binding("desiredSize", "size", go.Size.parse).makeTwoWay(go.Size.stringify),
            new go.Binding("angle").makeTwoWay(),
            new go.Binding("geometryString", "geo").makeTwoWay(),
            new go.Binding("fill"),
            new go.Binding("stroke"),
            new go.Binding("strokeWidth"))
        ));
      // create drawing tool for myDiagram, defined in FreehandDrawingTool.js
      var tool = new FreehandDrawingTool();
      // provide the default JavaScript object for a new polygon in the model
      tool.archetypePartData =
        { stroke: "blue", strokeWidth: 3, category: "FreehandDrawing" };
      // allow the tool to start on top of an existing Part
      tool.isBackgroundOnly = false;
      // install as first mouse-move-tool
      myDiagram.toolManager.mouseMoveTools.insertAt(0, tool);
      load("mySavedDiagram",myDiagram);  // load a simple diagram from the textarea
    
    }
    $( document ).ready(function() {
    	 var fnaId=$("#fnaValue").val();
    	    init();
    	    getAllPersonSign(fnaId);
    });
   
    function getAllPersonSign(fnaId){
  	   $.ajax({
  		    url: 'FnaSignature/getAllDataById/'+fnaId,
  		    type: 'GET',
  		    success: function(data) {
  		    	setPersonData(data);
  		    	
  		    },
  		    error: function(jqXHR, textStatus, errorThrown,data){
  		    	
  		    	console.log('Error: ' + textStatus + ' - ' + errorThrown);
  		       
  		    }
  		});
  		
     }
    
    function setPersonData(data){
	     var perType=$('#perType').val();
    	for(var i=0;i<data.length;i++){
	    	var signData=data[i];
			var signPerson=signData.signPerson;
			
			if(signPerson == perType){
				if(signData.esignId!=undefined){
					document.getElementById("esignId").value=signData.esignId;
				}
				try {
					var json = signData.eSign;
					// if (myDiagram) {
						myDiagram.initialPosition = go.Point.parse("-5 -5");
						myDiagram.model = go.Model.fromJson(json.model);
						myDiagram.model.undoManager.isEnabled = true;
					// }
					document.getElementById("mySavedDiagram").value=signData.eSign;
				} catch (ex) {
					alert(ex);
				}
				$(".aftersignature").hide();
				$("#myDiagramDiv").addClass("disabledsec");
				$("#spanSignHelpText").text("Signature Captured Successfully!");
			}
    	}
    }

    function mode(draw) {
      var tool = myDiagram.toolManager.findTool("FreehandDrawing");
      tool.isEnabled = draw;
    }
    function updateAllAdornments() {  // called after checkboxes change Diagram.allow...
      myDiagram.selection.each(function(p) { p.updateAdornments(); });
    }
    function clear11(){
		Swal.fire({
			title: 'Are you sure?',
			text: "want to Clear Your Signature",
			icon: 'question',
			showCancelButton: true,	
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Clear it!'
		}).then((result) => {
			if (result.isConfirmed) {
				var str1 = '{ "position": "-5 -5", "model": { "class": "GraphLinksModel", "nodeDataArray": [], "linkDataArray": []} }';
				document.getElementById("mySavedDiagram").value = str1;
				var str = document.getElementById("mySavedDiagram").value;
				try {
					var json = JSON.parse(str);
					myDiagram.initialPosition = go.Point.parse(json.position || "-5 -5");
					myDiagram.model = go.Model.fromJson(json.model);
					myDiagram.model.undoManager.isEnabled = true;
				} catch (ex) {
					alert(ex);
				}
				Swal.fire(
    		      'Cleared!',
    		      'Your Signature has been Cleared.',
    		      'success'
    		    );
			}
		});
	}
    // save a model to and load a model from Json text, displayed below the Diagram
    function personSave() {
    	signMakeBlob();
 	}
    function signMakeBlob() {
        var blob = myDiagram.makeImageData({ background: "white", returnType: "blob", callback: sendSignBlobData });
      }
    function sendSignBlobData(blob) {
    	// var newImg = document.getElementById("diagramId");
       	 var url = URL.createObjectURL(blob);

		 /* newImg.onload = function() {
		      // no longer need to read the blob so it's revoked
		      URL.revokeObjectURL(url);
		  }; */
		
		 /*  newImg.src = url;
		  document.body.appendChild(newImg);
		   */
		  
		  // var signImg=document.getElementById("diagramId").src;
		  
	    	
	    	var signByte;
	    	var request = new XMLHttpRequest();
	        request.open('GET', url, true);
	        request.responseType = 'blob';
	        request.onload = function() {
	            var reader = new FileReader();
	            reader.readAsDataURL(request.response);
	            reader.onload =  function(e){
	            	signByte=e.target.result;
	            	getSignImage(signByte);
	            	
	            };
	        };
	        request.send();
 }

   function getSignImage(signByte){
	   
	     var str = '{ "position": "' + go.Point.stringify(myDiagram.position) + '",\n  "model": ' + myDiagram.model.toJson() + ' }';
	      document.getElementById("mySavedDiagram").value = str;
	       //alert(str);
	    
	     var signImgString=signByte.split(",")[1];
	     
	     var fnaId=$("#fnaValue").val();
	     var perType=$('#perType').val();
	     var esignId=$("#esignId").val();
	     var curPage=$("#curPage").val();
	   
	     
	    	  $.ajax({
	    	  url: "FnaSignature/saveFnaSignature",
	    	  type: "POST",
	    	  
	    	  data:{fnaId:fnaId,
	    		  esign:str,
	    		  signDoc:signImgString,
	    		  perType:perType,
	    		  esignId:esignId,
	    		  curPage:curPage
	    	  },
	    	
	    		  
	    	  success : function(data){
	    		  
		    	  Swal.fire({
					icon: 'success',
					title: 'Signature captured successfully!',
					showConfirmButton: false,
					timer: 1500
				  });
			  	  $(".aftersignature").hide();
			  	  $("#myDiagramDiv").addClass("disabledsec");
			  	  $("#spanSignHelpText").text("Signature Captured Successfully!");
	    	   },
	    	  error : function(data){
	    		  alert("Error");
	    		 
	    	  }});
   }  
  
    
  </script>
</html>