<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <link href="img/logo/logo.png" rel="icon">
  
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <!--<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"  >
  <link href="css/ruang-admin.css" rel="stylesheet">
  
    <link href="css/jquery.steps.css" rel="stylesheet">
	<link href="css/style_checkboxs.css" rel="stylesheet">
	<link href="css/style_accord.css" rel="stylesheet">
<!--
<link href="css/style_declare.css" rel="stylesheet">
-->
   <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap" rel="stylesheet">  
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:400,700'>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css'>
    <!-- Select2 -->
  <link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="css/app/kyc-home.css">

<title><tiles:getAsString name="title" /></title>

</head>

<body>
 <div id="wrapper">
     <tiles:insertAttribute name="menu" />
      <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <tiles:insertAttribute name="header" />
        <tiles:insertAttribute name="body" />
        
        </div>
        </div>
   </div>
   <tiles:insertAttribute name="footer" />
   <!-- Pofile Modal -->
    <div class="modal fade" id="profileModal">
    <div class="modal-dialog ">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
          <h4 class="modal-title"> Profile Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <ul class="list-group">
             <li class="list-group-item">
             <span class="text-secondary bold italic" ><img src="img/profile.png"> Advisor Details</span>
             
             <div class="row">
                <div class="col-6">
                  <small><span class="text-primaryy  italic">Advisor RNF No. :</span><span> REPJG56D5</span></small>
                </div>
                <div class="col-6">
                <small><span class="text-primaryy  italic">Advisor  POS : </span><span>Test</span></small>
                </div>
             </div>
             </li>
             <li class="list-group-item">
             
             <span class="text-secondary bold italic"><img src="img/profile.png"> Manager Details</span>
             
                  <div class="row">
                <div class="col-6">
                  <small><span  class="text-primaryy  italic"> Manager Name : </span><span> Salim M Amin</span></small>
                </div>
                <div class="col-6">
                <small><span class="text-primaryy  italic"> Manager Rep RNF : </span><span>Test</span></small>
                </div>
             </div>
              <div class="row">
                <div class="col-6">
                  <small><span class="text-primaryy  italic" >Manager POS:</span><span> Test</span></small>
                </div>
                <div class="col-">
                <small><span class="text-primaryy  italic">RNf of rep's Supervisor : </span><span>Test</span></small>
                </div>
             </div>   
              </li>
             
           </ul>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
   
   <!-- Profile Modal End-->
   
   
   <!--view Archive List Button ArchiveList   -->
       <div id="btnArchiveList">
            <button class="btn btn-secondary btn-sm " data-toggle="modal" data-target="#archveListModal">View Archive&nbsp;<span class="badge badge-primary" style="padding: .25em .4em;">5</span></button>
       </div>
  <!--End   -->
  
  <!-- Archive List left side Modal -->
          <div class="modal left fade" id="archveListModal" tabindex="" role="dialog" aria-labelledby="archveListModal" aria-hidden="true">
             <div class="modal-dialog" role="document">
            <div class="modal-content">
                 
               
                <div class="modal-body" style="	padding: 10px 0px 0px 35px;" >
                  <div class="row">
                      <p><small><strong>You have 5 Archive List</strong></small></p>
                  </div>
                    
                    <div class="row">
                        <div class="list-group" style="width:93%;">
                        
							 <a href="#" class="list-group-item list-group-item-action flex-column align-items-start ">
								    <div class="d-flex w-100 justify-content-between">
								      <h6 class="mb-1">Archive 5</h6>
								      <small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-file-text" aria-hidden="true"></i></small>
								      
								    </div>
								    <p class="mb-1"><small><strong>Created Date: </strong>11-07-2020</small></p>
								     <p class="mb-1"><small><strong>Created By: </strong>Salim M Amin</small></p>
								     <small><strong>Archive Status:</strong> Open</small>
								      
								     <p class="mb-1"><small><strong>DSF Status: </strong>Finalised</small></p>
								      <p class="mb-1"><small><strong>Manager Status: </strong>Approved </small></p>
								       <p class="mb-1"><small><strong>Compliance Status: </strong>Pending </small></p>
								      
								       
								   
								     
								   
							 </a>
  
							  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
							    <div class="d-flex w-100 justify-content-between">
							     <h6 class="mb-1">Archive 4</h6>
							        <small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-file-text" aria-hidden="true"></i></small>
								    </div>
								     <p class="mb-1"><small><strong>Created Date: </strong>11-07-2020</small></p>
								     <p class="mb-1"><small><strong>Created By: </strong>Salim M Amin</small></p>
								     <small><strong>Archive Status:</strong> Open</small>
								      
								     <p class="mb-1"><small><strong>DSF Status: </strong>Finalised</small></p>
								      <p class="mb-1"><small><strong>Manager Status: </strong>Approved </small></p>
								       <p class="mb-1"><small><strong>Compliance Status: </strong>Pending </small></p>
							  </a>
  
							  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
							    <div class="d-flex w-100 justify-content-between">
							     <h6 class="mb-1">Archive 3</h6>
							       <small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-file-text" aria-hidden="true"></i></small>
								  
								    
								    </div>
								    <p class="mb-1"><small><strong>Created Date: </strong>11-07-2020</small></p>
								     <p class="mb-1"><small><strong>Created By: </strong>Salim M Amin</small></p>
								     <small><strong>Archive Status:</strong> Open</small>
								      
								     <p class="mb-1"><small><strong>DSF Status: </strong>Finalised</small></p>
								      <p class="mb-1"><small><strong>Manager Status: </strong>Approved </small></p>
								       <p class="mb-1"><small><strong>Compliance Status: </strong>Pending </small></p>
							  </a>
  
  
							  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
							    <div class="d-flex w-100 justify-content-between">
							      <h6 class="mb-1">Archive 2</h6>
							       <small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-file-text" aria-hidden="true"></i></small>
								    </div>
								   <p class="mb-1"><small><strong>Created Date: </strong>11-07-2020</small></p>
								     <p class="mb-1"><small><strong>Created By: </strong>Salim M Amin</small></p>
								     <small><strong>Archive Status:</strong> Open</small>
								      
								     <p class="mb-1"><small><strong>DSF Status: </strong>Finalised</small></p>
								      <p class="mb-1"><small><strong>Manager Status: </strong>Approved </small></p>
								       <p class="mb-1"><small><strong>Compliance Status: </strong>Pending </small></p>							  </a>
  
						  <a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						    <div class="d-flex w-100 justify-content-between">
						      <h6 class="mb-1">Archive 1</h6>
						       <small><i class="fa fa-info-circle" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;<i class="fa fa-file-text" aria-hidden="true"></i></small>
								    </div>
								   <p class="mb-1"><small><strong>Created Date: </strong>11-07-2020</small></p>
								     <p class="mb-1"><small><strong>Created By: </strong>Salim M Amin</small></p>
								     <small><strong>Archive Status:</strong> Open</small>
								      
								     <p class="mb-1"><small><strong>DSF Status: </strong>Finalised</small></p>
								      <p class="mb-1"><small><strong>Manager Status: </strong>Approved </small></p>
								       <p class="mb-1"><small><strong>Compliance Status: </strong>Pending </small></p>						  </a>
  
</div>


                     
                    </div>
                    
                    <!--  -->

 <!--  -->
                    
                   
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
        <!--Archive List Modal End...  -->
        
         <!--  Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top" style="display: inline;">
      <i class="fas fa-angle-up"></i>
      </a>
      
      <!-- Modal Logout -->
                  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
                     aria-hidden="true">
                     <div class="modal-dialog" role="document">
                        <div class="modal-content">
                           <div class="modal-header" id="cardHeaderStyle-22">
                              <h5 class="modal-title" id="exampleModalLabelLogout">eKYC</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                              <span aria-hidden="true">&times;</span>
                              </button>
                           </div>
                           <div class="modal-body">
                              <p>Are you sure you want to logout?</p>
                           </div>
                           <div class="modal-footer">
                              <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Cancel</button>
                              <a href="login" class="btn btn-sm btn-bs3-prime">Logout</a>
                           </div>
                        </div>
                     </div>
                  </div>
          <!-- Logout modal end -->
          <script src="vendor/psgisubmit/js/session_timeout.js"></script>
  
</body>
</html>