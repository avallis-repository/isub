<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>

<head>
<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

<link href="img/logo/logo.png" rel="icon">
<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link href="css/ruang-admin.css" rel="stylesheet">
<link href="css/style_radio_type1.css" rel="stylesheet">


<title><tiles:getAsString name="title" /></title>
</head>

<body>
    <div id="wrapper" style="background-color: #ecf0f1;">
    
    <div id="content-wrapper" class="d-flex flex-column">
      <div id="content">
        <tiles:insertAttribute name="header" />
       
		<tiles:insertAttribute name="body" />
      </div>
      </div>  
      </div>
    <tiles:insertAttribute name="footer" />
    
    
    <!-- Profile Modal -->
               <div class="modal fade" id="profileModal">
    <div class="modal-dialog ">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
          <h4 class="modal-title"> Profile Details</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          <ul class="list-group">
             <li class="list-group-item">
             <span class="text-secondary bold italic" ><img src="img/profile.png"> Advisor Details</span>
             
             <div class="row">
                <div class="col-6">
                  <small><span class="text-primaryy  italic">Advisor RNF No. :</span><span> REPJG56D5</span></small>
                </div>
                <div class="col-6">
                <small><span class="text-primaryy  italic">Advisor  POS : </span><span>Test</span></small>
                </div>
             </div>
             </li>
             <li class="list-group-item">
             
             <span class="text-secondary bold italic"><img src="img/profile.png"> Manager Details</span>
             
                  <div class="row">
                <div class="col-6">
                  <small><span  class="text-primaryy  italic"> Manager Name : </span><span> Salim M Amin</span></small>
                </div>
                <div class="col-6">
                <small><span class="text-primaryy  italic"> Manager Rep RNF : </span><span>Test</span></small>
                </div>
             </div>
              <div class="row">
                <div class="col-6">
                  <small><span class="text-primaryy  italic" >Manager POS:</span><span> Test</span></small>
                </div>
                <div class="col-">
                <small><span class="text-primaryy  italic">RNf of rep's Supervisor : </span><span>Test</span></small>
                </div>
             </div>   
              </li>
             
           </ul>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
          <!--Profile Modal End  -->
    
    
     <!-- Modal Logout -->
          <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelLogout"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" id="cardHeaderStyle-22">
                  <h6 class="modal-title" id="exampleModalLabelLogout">eKYC</h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:#fff;">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Are you sure you want to logout?</p>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-sm btn-bs3-prime" data-dismiss="modal">Cancel</button>
                  <a href="login" class="btn btn-sm btn-bs3-prime">Logout</a>
                </div>
              </div>
            </div>
          </div>
    
</body>
</html>