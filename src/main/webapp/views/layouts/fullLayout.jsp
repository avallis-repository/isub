<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link href="vendor/psgisubmit/img/logo/logo.png" rel="icon">
<link href="vendor/SweetAlert/css/sweetalert.css" rel="stylesheet" >
 <link href="vendor/SweetAlert2/css/sweetalert2.css">
<link href="vendor/SweetAlert2/css/sweetalert2.min.css">

<link href='https://fonts.googleapis.com/css?family=Roboto:400,700'
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap"
	rel="stylesheet">

<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

<!-- <link href="vendor/fontawesome-free/css/font-awesome463.min.css"	rel="stylesheet"> -->
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
<link href="vendor/psgisubmit/css/ekyc-layout.css" rel="stylesheet">
<link
	href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css"
	rel="stylesheet" />

<link href="css/jquery.steps.css" rel="stylesheet">
<link href="css/style_checkboxs.css" rel="stylesheet">
<link href="css/style_accord.css" rel="stylesheet">
<link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet"
	type="text/css">
<link href="vendor/psgisubmit/css/style_upload.css" rel="stylesheet">
<link href="vendor/psgisubmit/css/style_toggle.css" rel="stylesheet">
<link href="vendor/psgisubmit/css/stepper.css" rel="stylesheet">

<link rel="stylesheet" href="vendor/jquery/jquery-ui.min.css">
<link rel="stylesheet" href="vendor/GitUpToast/css/jquery.toast.css">
<!-- <link href="vendor/SweetAlert/css/sweetalert.css" rel="stylesheet" > -->
<link href="vendor/psgisubmit/css/sign_Status_Style.css"
	rel="stylesheet">

<script src="vendor/jquery/jquery.js"></script>
<script src="vendor/jquery/popper.min.js"></script>
<script src="vendor/bootstrap453/js/bootstrap.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>


<script
	src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script src="vendor/psgisubmit/js/ekyc-layout.js"></script>
<script src="vendor/psgisubmit/js/Common Script.js"></script>
<script src="vendor/psgisubmit/js/scan_sign.js"></script>
<script src="vendor/psgisubmit/js/Constants.js"></script>





<script src="vendor/select2/dist/js/select2.js"></script>

	      <script src="vendor/jquery/jquery-ui.min.js"></script>
	      <script src="vendor/GitUpToast/js/jquery.toast.js"></script>
	      
	      <script src="vendor/SweetAlert/js/sweetalert.js"></script>
		  <script src="vendor/SweetAlert/js/sweetalert.min.js"></script>
	      
		 <script src="vendor/SweetAlert2/js/sweetalert2.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.js"></script> 
<script src="vendor/psgisubmit/js/appconfig.js"></script>


<script src="vendor/bootstrap453/js/bs.typeahead.js"></script>
<script>
var baseUrl="",strPageValidMsgFld="";
baseUrl="${contextPath}";
LOG_USER_MGRACS='${MANAGER_ACCESSFLG}';
LOG_USER_ADVID='${LOGGED_ADVSTFID}';
var seesselfName = '${SESS_DF_SELF_NAME}';
var sessspsname = '${SESS_DF_SPS_NAME}';
<%-- BIRT_URL = "<%=com.avallis.ekyc.utils.KycConst.BIRT_PATH%>"; --%>
strPageValidMsgFld ="${SIGN_VALIDATION_MSG}"
var approvalScreen = "false";
</script>




<title><tiles:getAsString name="title" /></title>

</head>

<body>
	<div id="cover-spin"></div>
	<input type="hidden" name="sessspsname" id="sessspsname"
		value="${SESS_DF_SPS_NAME}" />
	<input type="hidden" name="hTxtFldCurrFNAId" id="hTxtFldCurrFNAId"
		value="${CURRENT_FNAID}" />
	<input type="hidden" name="hTxtFldCurrCustId" id="hTxtFldCurrCustId"
		value="${CURRENT_CUSTID}" />
	<input type="hidden" name="hTxtFldCurrCustName"
		id="hTxtFldCurrCustName" value="${CURRENT_CUSTNAME}" />
	<input type="hidden" name="hTxtFldLoggedAdvId" id="hTxtFldLoggedAdvId"
		value="${LOGGED_USER_INFO.LOGGED_USER_ADVSTFID}" />
	<input type="hidden" id="hTxtValidationMsg" name="hTxtValidationMsg">
	<input type="hidden" size="2" id="hTxtFldSignedOrNot"
		name="hTxtFldSignedOrNot" />
	<input type="hidden" name="hdnFnaDetails" id="hdnFnaDetails"
		value='${FNA_DETAILS}' />
	<input type="hidden" id="hTxtFldManagerApprovalHide" />

	<div id="wrapper">
		<tiles:insertAttribute name="menu" />
		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<tiles:insertAttribute name="header" />
				<tiles:insertAttribute name="body" />

			</div>
		</div>
	</div>
	<tiles:insertAttribute name="footer" />

	<jsp:include page="/views/pages/logoutModal.jsp"></jsp:include>
	<jsp:include page="/views/pages/profileDetlsModal.jsp"></jsp:include>
	<jsp:include page="/views/pages/scanToSignModal.jsp"></jsp:include>
	<jsp:include page="/views/pages/documentUploadModal.jsp"></jsp:include>
	<jsp:include page="/views/pages/archiveListModal.jsp"></jsp:include>


	<jsp:include page="/views/pages/SignModal.jsp"></jsp:include>
	<%-- <jsp:include page="/views/pages/ekycValidations.jsp"></jsp:include> --%>





	<!-- Scroll to top -->
	<a class="scroll-to-top" href="#page-top" title="Go to Top"
		style="display: none;"> <i class="fa fa-chevron-up"
		aria-hidden="true"></i>
	</a>


	<!-- Hidden Field Values Configuration poovathi add on 10-11-2020 -->
	<input type="hidden" name="chkanychange" id="chkanychange" />
	<!-- End of Hidden Fields -->





	<div id="ekycinvmodal" class="modal fade">
		<div class="modal-dialog modal-xl  modal-dialog-scrollable">
			<div class="modal-content" style="min-height: 85vh">
				<div class="modal-header" id="cardHeaderStyle-22">
					<h6 class="modal-title">Investment eKYC</h6>
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<!-- <iframe id="ifrInvEkyc" style="width:100%;height:70vh"></iframe>  -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<form id="hiddenForm" method="post" action="" target="TheWindowBIRT"></form>
	<form id="validationForm" method="post"></form>
 <form id="isubmitForm" method="post" target="TheWindowISUBMIT"></form>
	

	<div class="modal fade" id="loadermodal" role="dialog"
		data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header" id="cardHeaderStyle-22">
					<h5 class="modal-title" id="finishedModalLabelClnt">Loading</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close" style="color: #fff;">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body text-center">


					<div class="circle-loader">
						<div class="checkmark draw"></div>

					</div>
					
					 


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-sm  btn-bs3-prime"
						data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	
	
           <!--Poovathi add on 25-07-2021 policy esubmission modal  -->
          
  <div class="modal" id="policyEsubMdl">
    <div class="modal-dialog modal-dialog-scrollable modal-xl">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header" id="cardHeaderStyle-22">
          <h1 class="modal-title">Policy E-Submissions :-</h1>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        
        
        <div class="container mt-5 mb-5">
    <div class="d-flex justify-content-center row">
        <div class="col-md-10">
            <div class="row p-2 bg-white border rounded">
                <div class="col-md-3 mt-1 border-right">
                    <img class="img-fluid img-responsive rounded product-image" src="vendor/psgisubmit/img/prinImg/ntucincome.png">
                    <button class="btn btn-outline-primary btn-sm btn-sm mt-4"  onclick="policySubmission('ntuc_income')">Create Policy</button>
                </div>
                <div class="col-md-6 mt-1 border-right">
                   <div class="d-flex flex-column mt-4">
                     <h5>DSF - Submission</h5>
                   
                     <!-- <div class="mt-1 mb-1 spec-1"><span class="strike-text badge badge-primary" id="spanCaseId"></span></div>
                     <div class="mt-1 mb-1 spec-1"><span class="strike-text badge badge-info" id="spanCaseStatus"></span></div> -->
                    
                     <p class="frm-lable-fntsz p-0">Ntuc Case Id : <span class="frm-lable-fntsz font-normal" id="spanCaseId">--NIL--</span></p>
                     <p class="frm-lable-fntsz p-0">Ntuc Case Status : <span class="frm-lable-fntsz font-normal" id="spanCaseStatus">--NIL--</span></p>
                     <p class="frm-lable-fntsz p-0">Policy No : <span class="frm-lable-fntsz font-normal" id="spanPolNo">--NIL--</span></p>
                     <p class="frm-lable-fntsz p-0">Prod Id : <span class="frm-lable-fntsz font-normal" id="spanPolId">--NIL--</span></p>
                    
                     <button data-toggle="collapse" data-target="#demo" class="btn btn-bs3-prime hide" id="btnviewDoc"></button>
                          <div id="demo" class="collapse mt-2">
                                 <small class="text-secondary hide" id="noNtucDataMsg">---No NTUC Document(s) found---</small>
                                <ul class="list-group" id="dynaAttachList"></ul>
                          </div>
                    </div>
                </div>
                <div class="align-items-center align-content-center col-md-3 border-left mt-1" id="ntucPolsecId">
                  
                    <div class="d-flex flex-column mt-4">
                      
                         <button class="btn btn-outline-primary btn-sm mt-2" onclick="onNtucService('retrieveFileInfo');">Download Policy Docs.</button>
                         <button class="btn btn-outline-primary btn-sm mt-2" onclick="onNtucService('retrievePolicy');">Get Policy Dets.</button>
                         <div class="d-flex flex-column mt-4">
                        <div class="form-group">
                                                      <label  class="frm-lable-fntsz" for="sel1">Change Case Status:</label>
                                                      <select class="form-control" id="selFldNtucSts" onchange="onNtucService('dsfUpdateSts');">
                                                        <option value="">--Select--</option>
                                                        <option value="InProgress">InProgress</option>
                                                        <option value="Completed">Completed</option>
                                                        <option value="Submitted">Submitted</option>
                                                        <option value="Approved">Approved</option>
                                                        <option value="Rejected">Rejected</option>
                                                        <option value="Cancelled">Cancelled</option>
                                                      </select>
                                                </div>
                                                
                    	 
                    	</div>
                    	</div>
                </div>
                
            </div>
        </div>
    </div>
    
    
    <div class="d-flex justify-content-center mt-3 mb-3 row">
        <div class="col-md-10">
            <div class="row p-2 bg-white border rounded">
                <div class="col-md-3 mt-1 border-right">
                    <img class="img-fluid img-responsive rounded product-image" src="vendor/psgisubmit/img/prinImg/aviva.png">
                    <button class="btn btn-outline-primary btn-sm btn-sm mt-4" onclick="policySubmission('aviva')">Create Policy</button>
                </div>
                
            </div>
        </div>
    </div>
    
    
    <div class="d-flex justify-content-center mt-3 mb-3 row">
        <div class="col-md-10">
            <div class="row p-2 bg-white border rounded">
                <div class="col-md-3 mt-1 border-right">
                	<img class="img-fluid img-responsive rounded product-image" src="vendor/psgisubmit/img/prinImg/Manulife_logo.png">
                </div>
                
            </div>
        </div>
    </div>
</div>
        
        
              
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="button" class="btn btn-bs3-prime" data-dismiss="modal">Close</button>
        </div>
        
      </div>
    </div>
  </div>
  
  <!--end of policy esum modal  -->
</body>
<script>
$(function(){
	$('.navbar').css('width', '87%');
	
	 //set tooltip for all page  text ,textarea and select combo 
	    $('input[type=text],input[type=email],textarea,select').keyup(function(){
		   $(this).attr('title',$(this).val())
	    })
	    
	    $('input[type=text],input[type=email],textarea,select').mouseover(function(){
		   $(this).attr('title',$(this).val())
	    })
	     $('input[type=text],input[type=email],textarea,select').mouseout(function(){
		   $(this).attr('title',"")
	    })
	     $('input[type=text],input[type=email],textarea,select').keyup(function(){
	       $(this).attr('autocomplete',"nope")
    })
    
         $('input[type=text],input[type=email],textarea,select').keydown(function(){
		   $(this).attr('autocomplete',"nope")
	})
	
	
	    $('input[type=text],input[type=email],textarea,select').keypress(function(){
		   $(this).attr('autocomplete',"nope")
	})

    $('input[type=text],input[type=email],textarea,select').mouseover(function(){
	  $(this).attr('autocomplete',"nope")
    })
	   //end of tooltip function
});



$("#linkekycinvaddnew").on("click",function(e){
	e.preventDefault();
	
	var params = ['height='+screen.height,'width='+screen.width, 'fullscreen=yes' ].join(',');

	var popup = window.open('https://internal-ekyc-ias.avallis.com/EKYC_IAS/applicant/getApplicantDets', 'popup_window', params); 
	          popup.moveTo(0,0);
	
})

$("#linkekycinvsummary").on("click",function(e){
	e.preventDefault();
	var params = ['height='+screen.height,'width='+screen.width, 'fullscreen=yes' ].join(',');

	var popup = window.open('https://internal-ekyc-ias.avallis.com/EKYC_IAS/prodSummary/getProductSummaryDets', 'popup_window', params); 
	          popup.moveTo(0,0);
	          
})

var elementFloatingChat = $('.floating-chat');
 

setTimeout(function() {
	elementFloatingChat.addClass('enter');
}, 1000);

elementFloatingChat.click(openElement);

function openElement() {
	
	
    var messages = elementFloatingChat.find('.messages');
  //  var textInput = elementFloatingChat.find('.text-box');
    elementFloatingChat.find('>i').hide();
    elementFloatingChat.addClass('expand');
    elementFloatingChat.find('.chat').addClass('enter');
    //var strLength = textInput.val().length * 2;
    //textInput.keydown(onMetaAndEnter).prop("disabled", false).focus();
    elementFloatingChat.off('click', openElement);
    elementFloatingChat.find('.header button').click(closeElement);
    //elementFloatingChat.find('#sendMessage').click(sendNewMessage);
    messages.scrollTop(messages.prop("scrollHeight"));
    
    
	
}

function closeElement() {
	elementFloatingChat.find('.chat').removeClass('enter').hide();
	elementFloatingChat.find('>i').show();
	elementFloatingChat.removeClass('expand');
	elementFloatingChat.find('.header button').off('click', closeElement);
	//elementFloatingChat.find('#sendMessage').off('click', sendNewMessage);
	//elementFloatingChat.find('.text-box').off('keydown', onMetaAndEnter).prop("disabled", true).blur();
    setTimeout(function() {
    	elementFloatingChat.find('.chat').removeClass('enter').show()
        elementFloatingChat.click(openElement);
    }, 500);
}

 if(!isEmpty(strPageValidMsgFld)){
	//$("#loadermodal").modal("show")
	setTimeout(function() {
		$("#btntoolbarsign").trigger("click");
	}, 1200);
} 

 $(".checkdb").bind("change",function(){
	
	var curntObj = $(this);
	enableReSignIfRequired(curntObj);
	});


	
function enableReSignIfRequired(curntObj){
	
	var nodetype = $(curntObj).prop('type').toUpperCase();
	
 if(nodetype == "HIDDEN" || nodetype == "RADIO" || nodetype=="CHECKBOX"){}

	var isSigned = $("#hTxtFldSignedOrNot").val();
	
	if(isSigned == "Y") {
		
		var nodetype = $(curntObj).prop('nodeName').toLowerCase();
		
     var chkanychange = $("#chkanychange");
		var jsonvalues = JSON.parse(isEmpty(chkanychange.val()) ? "{}" : chkanychange.val());

		var inputtype = $(curntObj).prop("type").toLowerCase();
		
		if(inputtype ==  "text" || inputtype == "hidden" || inputtype == "select" || inputtype == "textarea" || nodetype == "select"){
         var newobj=jsonvalues;
			newobj[$(curntObj).prop("name")]=$(curntObj).val();
			chkanychange.val(JSON.stringify(newobj));

		}else if(inputtype == "radio" || inputtype == "checkbox"){
			var chkFlg = $(curntObj).is(":checked");
			   if(chkFlg == true || chkFlg == false){
				   var newobj=jsonvalues;
						newobj[$(curntObj).prop("name")]=$(curntObj).val();
						chkanychange.val(JSON.stringify(newobj));
				   }
		}
		
		if(!isEmpty(chkanychange.val())) {
			showConfrmAfterSignChng($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);

		}
	 }
}
//checkdb function end	
function enableReSignIfRequiredforEdtDeltAction(){
var isSigned = $("#hTxtFldSignedOrNot").val();
	if(isSigned == "Y") {
		showConfrmAfterSignChng($("#btnNextFormUniq"),$("#applNextScreen").val(),false,false);
	}
}

//defalut hide client(2) label when Spouse is not present
if ($("#MenuSpsName").text().length > 0 ) {
    $("#clnt2Text").addClass("show").removeClass("hide");
} else {
    $("#clnt2Text").addClass("hide").removeClass("show");
}
//End


function creatSubmtDynaIsubmit(href){
	
    var lex1 = href.split('?');
    var action= lex1[0];
    var qstr = lex1[1];
    var obj = " ";

    var formId = document.getElementById("isubmitForm");
    var divId = document.getElementById("dynamicFormDiv");

    if(formId){
        if(divId)
            formId.removeChild(divId);
    }

    var newdiv = document.createElement('div');
    newdiv.id = "dynamicFormDiv";

    if(qstr != null) {
        var params = qstr.split('&');
        for(var p=0;p< params.length;p++){
            var keyValue = params[p].split('=');
            var name = keyValue[0];
            var value = keyValue[1];

            if(value.indexOf("\'") != -1)
                value = value.replace("\'","\'");
                value = unescape(value);

            //obj += '<input type="hidden" name="'+name+'" value="'+value+'"/>';
            obj += "<input type='hidden' name='"+name+"' value='"+value+"'/>";

        }
    }
    newdiv.innerHTML = obj;   

    if(document.getElementById("isubmitForm"))
    	document.getElementById("isubmitForm").appendChild(newdiv);

    document.forms["isubmitForm"].action=action;
    document.forms["isubmitForm"].method="POST";

  window.open('', 'TheWindowISUBMIT',"channelMode,toolbar=no,scrollbars=no,location=no,resizable =yes");
  document.getElementById("isubmitForm").submit();

}



//poovathi add policy esubmission related validation on 12-08-2021
function validatePolicyEsubDetls(){
		
	//check key and sign existed or not
	var keyInfoFlg = $("#keyStsInfo").hasClass("btn-success");
	var sgnStsFlag = $("#sgnStsInfo").hasClass("btn-success");
	var signorNot = $("#hTxtFldSignedOrNot").val();
	
	if(keyInfoFlg == true && sgnStsFlag == true && signorNot == "Y"){
		//show policyE-sub mdl
		onCaseInfo();
		$("#policyEsubMdl").modal('show');
	}else{
		$("#policyEsubMdl").modal('hide');
		toastr.clear($('.toast'));
		toastr.options = {  timeOut: "5000", extendedTimeOut: "1000"};
		toastr["error"]("Key Infomation Details or Client (1) / Client (2) / Adviser Signature can't be empty");	
		
	}
	
}

//policy Esubmission NTUC Validation start here

$('#policyEsubMdl').on('shown.bs.modal', function () {
	let caseId = $("#spanCaseId").text().trim();
	if(isEmpty(caseId)) {
		$("#ntucPolsecId").addClass("disabledMngrSec");
	} else {
		$("#ntucPolsecId").removeClass("disabledMngrSec");
    }
});

//End of NTUC Policy Esubmission.
</script>

</html>