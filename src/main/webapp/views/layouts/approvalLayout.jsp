<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html>

<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link href="vendor/psgisubmit/img/logo/logo.png" rel="icon">


<link href='https://fonts.googleapis.com/css?family=Roboto:400,700'
	rel="stylesheet">
<link
	href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap"
	rel="stylesheet">

<link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">

<!-- <link href="vendor/fontawesome-free/css/font-awesome463.min.css"	rel="stylesheet"> -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
<link href="vendor/psgisubmit/css/ekyc-layout.css" rel="stylesheet">
<link href="vendor/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">

<link href="css/style_checkboxs.css" rel="stylesheet">
<link href="css/style_accord.css" rel="stylesheet">
<link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
<link href="vendor/psgisubmit/css/style_upload.css" rel="stylesheet">
<link href="vendor/psgisubmit/css/style_toggle.css" rel="stylesheet">

<script src="vendor/jquery/jquery.js"></script>
<script src="vendor/jquery/popper.min.js"></script>
<script src="vendor/bootstrap453/js/bootstrap.js"></script>
<script src="js/modernizr.custom.js"></script>
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="vendor/datatables/jquery.dataTables.min.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script src="vendor/psgisubmit/js/session_timeout.js"></script>
<script src="vendor/psgisubmit/js/ekyc-layout.js"></script>
<script src="js/gojs/go.js"></script>
<!--   <script src="assets/js/goSamples.js"></script>  --> <!-- this is only for the GoJS Samples framework -->
  <script src="js/gojs/FreehandDrawingTool.js"></script>
  <script src="js/gojs/GeometryReshapingTool.js"></script>
 <link rel="stylesheet" href="css/stylesheet.css"/>

<script src="vendor/psgisubmit/js/Common Script.js"></script>

<script src="vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<script src="vendor/jquery-easing/jquery.easing.min.js"></script>


<!-- <script src="vendor/select2/dist/js/select2.js"></script>


<link href="vendor/psgisubmit/css/approval.steps.css" rel="stylesheet">
  <link href="vendor/psgisubmit/css/kyc_approval.css" rel="stylesheet">
 
 <script src="js/jquery.steps.js"></script>

  App CSS
  <link rel="stylesheet" href="css/app/fin-well-review.css"> -->
 
   
   



<title><tiles:getAsString name="title" /></title>

</head>

<body>
	<div id="wrapper" style="background-color: #ecf0f1;">

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<tiles:insertAttribute name="header" />

				<tiles:insertAttribute name="body" />
			</div>
		</div>
	</div>

	
	<!-- Poovathi Add on 04-NOV-2020 -->
	 <jsp:include page="/views/pages/logoutModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/profileDetlsModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/scanToSignModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/documentUploadModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/archiveListModal.jsp"></jsp:include>
     
     <input type="hidden" name="hTxtFldCurrFNAId" id="hTxtFldCurrFNAId" value="${CURRENT_FNAID}"/>
		
 <!-- Scroll to top -->
  <a class="scroll-to-top" href="#page-top" title="Go to Top" style="display: none;">
   <i class="fa fa-chevron-up" aria-hidden="true"></i>
  </a>
  
  
 
       <input type="hidden" name="chkanychange" id="chkanychange"/>
  <!-- End of Hidden Fields -->
  
	
</body>
<script>
$(function(){
	$('.navbar').css('width', '100%');
	$('#wrapper #content-wrapper').css('overflow-y','auto');
});
</script>

 

</html>