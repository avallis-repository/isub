<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<script>
var baseUrl="${contextPath}";
</script>
  <link href="vendor/psgisubmit/img/logo/logo.png" rel="icon">
  <link href='https://fonts.googleapis.com/css?family=Roboto:400,700'  rel='stylesheet'>
  <link href="https://fonts.googleapis.com/css?family=Poppins:400,600&display=swap" rel="stylesheet">
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <!-- <link href="vendor/fontawesome-free/css/font-awesome463.min.css" rel="stylesheet"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
  <link rel="stylesheet" href="vendor/bootstrap453/css/bootstrap.css">
  <link href="vendor/psgisubmit/css/ekyc-layout.css" rel="stylesheet">
  
  
  <link href="vendor/select2/dist/css/select2.min.css" rel="stylesheet" type="text/css">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet" />
  <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
  
  <link href="vendor/psgisubmit/css/style_radio_type1.css" rel="stylesheet">
  <link href="vendor/psgisubmit/css/style_upload.css" rel="stylesheet">  
  <link href="vendor/psgisubmit/css/style_toggle.css" rel="stylesheet">
  
  <link href="vendor/SweetAlert2/css/sweetalert2.css">
  <link href="vendor/SweetAlert2/css/sweetalert2.min.css">
  

	<script src="vendor/jquery/jquery.js"></script>
	<script src="vendor/jquery/popper.min.js"></script>
	<script src="vendor/bootstrap453/js/bootstrap.js"></script>
	<script src="js/modernizr.custom.js"></script>
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
	
	<script src="vendor/datatables/jquery.dataTables.js"></script>
  	<script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>
  	
  	
  	
	<script src="vendor/psgisubmit/js/ekyc-layout.js"></script>
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="vendor/select2/dist/js/select2.min.js"></script>
	<script src="vendor/psgisubmit/js/Common Script.js"></script>
	<script src="vendor/psgisubmit/js/Constants.js"></script>
	
	<script src="vendor/SweetAlert2/js/sweetalert2.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.all.min.js"></script>
		  <script src="vendor/SweetAlert2/js/sweetalert2.js"></script> 
	
	

<title><tiles:getAsString name="title" /></title>
</head>

<body>
	<div id="cover-spin"></div>
	<div id="wrapper" style="background-color: #ecf0f1;">

		<div id="content-wrapper" class="d-flex flex-column">
			<div id="content">
				<tiles:insertAttribute name="header" />

				<tiles:insertAttribute name="body" />
			</div>
		</div>
	</div>
	<%-- <tiles:insertAttribute name="footer" /> --%><!-- Footer not Neded in MInLayout.jsp Add  Poovathi 0n 10-11-2020 -->
	 
	 <jsp:include page="/views/pages/logoutModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/profileDetlsModal.jsp"></jsp:include>
     <%-- <jsp:include page="/views/pages/scanToSignModal.jsp"></jsp:include>
     <jsp:include page="/views/pages/documentUploadModal.jsp"></jsp:include> 
     <jsp:include page="/views/pages/archiveListModal.jsp"></jsp:include> --%>
     
     <input type="hidden" name="hTxtFldLoggedAdvId" id="hTxtFldLoggedAdvId" value="${LOGGED_USER_INFO.LOGGED_USER_ADVSTFID}"/>
     <input type="hidden" name="hTxtFldCurrFNAId" id="hTxtFldCurrFNAId" value="${CURRENT_FNAID}"/>
		
	 <script src="vendor/psgisubmit/js/session_timeout.js"></script>
</body>
<script>
//poovathi add on 15-11-2020
$(function(){
	$('.navbar').css('width', '100%');
	$('#wrapper #content-wrapper').css('overflow-y','auto');
})
</script>
</html>