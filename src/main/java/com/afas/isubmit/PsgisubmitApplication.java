package com.afas.isubmit;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;

@SpringBootApplication
@EnableEncryptableProperties
public class PsgisubmitApplication {

	private static Logger logger = LogManager.getLogger(PsgisubmitApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(PsgisubmitApplication.class, args);
		logger.info("iSubmit is started!");
		logger.info("Testing on 17 11 2021!");
	}

}
