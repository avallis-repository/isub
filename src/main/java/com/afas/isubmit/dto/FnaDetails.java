package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "FNA_DETAILS")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FnaDetails {

	@Id
	@GenericGenerator(name = "fnadetails", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "fnaid_sequence"),
			@Parameter(name = "sequence_prefix", value = "FNA"), })
	@GeneratedValue(generator = "fnadetails", strategy = GenerationType.SEQUENCE)
	@Column(name = "FNA_ID", nullable = false)
	private String fnaId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CUST_ID", nullable = false)
	private CustomerDetails custDetails;

	@Column(name = "ADVSTF_ID", nullable = false)
	private String advstfId;

	@Column(name = "MGR_ID", nullable = false)
	private String mgrId;

	@Column(name = "FNA_TYPE", nullable = false)
	private String fnaType;

	@Column(name = "CC_REPEXISTINVESTFLG")
	private String ccRepexistinvestflg;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "CUST_SIGN_DATE")
	private Date custSignDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "SPS_SIGN_DATE")
	private Date spsSignDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "ADV_SIGN_DATE")
	private Date advSignDate;

	@Column(name = "COMMENTS")
	// @Type(type="TEXT")
	private String comments;

	@Column(name = "MGR_SIGN")
	private Date mgrSign;

	@Column(name = "MGR_SGIN_DATE")
	private Date mgrSignDate;

	@Column(name = "CR_INVSTOBJ")
	// @Type(type="TEXT")
	private String crInvstObj;

	@Column(name = "CR_INVSTAMT")
	private float crInvstAmt;

	@Column(name = "CR_INVSTTIMEHORIZON")
	private String crInvstTimeHorizon;

	@Column(name = "CR_RISKPREF")
	private String crRiskPref;

	@Column(name = "CR_RISKCLASS")
	private String crRiskClass;

	@Column(name = "CR_ROI")
	private float crRoi;

	@Column(name = "CR_ADEQFUND")
	private String crAdeqFund;

	@Column(name = "CR_OTHCONCERN")
	// @Type(type="TEXT")
	private String crOthConcern;

	@Column(name = "CD_MAILADDRFLG")
	private String cdMailAddrFlg;

	@Column(name = "CD_CUST_MAILADDR")
	// @Type(type="TEXT")
	private String cdCustMailAddr;

	@Column(name = "CD_SPS_MAILADDR")
	// @Type(type="TEXT")
	private String cdSpsMailAddr;

	@Column(name = "CD_MAILADDRRESFLG")
	private String cdMailAddrResFlg;

	@Column(name = "CD_MAILADDR_OTHDET")
	private String cdMailAddrOthDet;

	@Column(name = "CD_INTRPRTFLG")
	private String cdIntrprtFlg;

	@Column(name = "CD_BENFOWNFLG")
	private String cdBenfownFlg;

	@Column(name = "CD_TPPFLG")
	private String cdTppFlg;

	@Column(name = "CD_PEPFLG")
	private String cdPepFlg;

	@Column(name = "ADVREC_REMARKS")
	// @Type(type="TEXT")
	private String advRecRemarks;

	@Column(name = "SWREP_CONFLG")
	private String swrepConflg;

	@Column(name = "SWREP_CONF_DETS")
	private String swrepConfDets;

	@Column(name = "SWREP_ADVBYFLG")
	private String swrepAdvbyflg;

	@Column(name = "SWREP_DISADVGFLG")
	private String swrepDisadvgflg;

	@Column(name = "SWREP_PROCEEDFLG")
	private String swrepProceedflg;

	@Column(name = "SWREP_REMARKS")
	// @Type(type="TEXT")
	private String swrepRemarks;

	@Column(name = "SWREP_PP_FF_REMARKS")
	// @Type(type="TEXT")
	private String swrepPpFfRemarks;

	@Column(name = "ADVREC_REASON")
	// @Type(type="TEXT")
	private String advrecReason;

	@Column(name = "CDACK_AGREE")
	private String cdackAgree;

	@Column(name = "CDACK_ADVRECOMM")
	// @Type(type="TEXT")
	private String cdackAdvrecomm;

	@Column(name = "ADVDEC_REMARKS")
	// @Type(type="TEXT")
	private String advDecRemarks;

	@Column(name = "ADVREC_FNANUMBER")
	private String advRecFnaNumber;

	@Column(name = "ACKMGR_COMMENTS")
	// @Type(type="TEXT")
	private String ackMgrComments;

	@Column(name = "ACKMGR_FNANUMBER")
	private String ackMgrFnaNumber;

	@Column(name = "CREATED_BY", nullable = false)
	private String createdBy;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;

	@Column(name = "ARCH_STATUS")
	private String archStatus;

	@Column(name = "KYC_SENT_STATUS")
	private String kycSentStatus;

	@Column(name = "MGR_APPROVE_STATUS")
	private String mgrApproveStatus;

	@Column(name = "MGR_APPROVE_DATE")
	private Date mgrApproveDate;

	@Column(name = "CD_CUST_EMPLYR")
	private String cdCustEmplyr;

	@Column(name = "CD_SPS_EMPLYR")
	private String cdSpsEmplyr;

	@Column(name = "CD_CUST_ANNLINCOME")
	private String cdCustAnnlIncome;

	@Column(name = "CD_SPS_ANNLINCOME")
	private String cdSpsAnnlIncome;

	@Column(name = "CD_REMARKS")
	// @Type(type="TEXT")
	private String cdRemarks;

	@Column(name = "ADVREC_REMARKS1")
	// @Type(type="TEXT")
	private String advRecRemarks1;

	@Column(name = "ADVREC_FNANUMBER1")
	private String advRecFnaNumber1;

	@Column(name = "ADVREC_REMARKS2")
	// @Type(type="TEXT")
	private String advRecRemarks2;

	@Column(name = "ADVREC_FNANUMBER2")
	private String advRecFnaNumber2;

	@Column(name = "ACKMGR_COMMENTS1")
	// @Type(type="TEXT")
	private String ackMgrComments1;

	@Column(name = "ACKMGR_FNANUMBER1")
	private String ackMgrFnaNumber1;

	@Column(name = "FATCA_BIRTHPLACE")
	private String fatcaBirthPlace;

	@Column(name = "MGR_STATUS_REMARKS")
	// @Type(type="TEXT")
	private String mgrStatusRemarks;

	@Column(name = "CLIENT_CONSENT")
	private String clientConsent;

	@Column(name = "FATCA_NOCONTRY_FLG")
	private String fatcaNoContryFlg;

	@Column(name = "CUST_ATTACH_FLG")
	private String custAttachFlg;

	@Column(name = "SUPREV_MGR_FLG")
	private String suprevMgrFlg;

	@Column(name = "SUPREV_FOLLOW_REASON")
	// @Type(type="TEXT")
	private String suprevFollowReason;

	@Column(name = "CD_MRKTMAT_POSTALFLG")
	private String cdMrktmatPostalflg;

	@Column(name = "CD_MRKTMAT_EMAILFLG")
	private String cdMrktmatEmailflg;

	@Column(name = "NTUC_POLICY_ID")
	private String ntucPolicyId;

	@Column(name = "ADMIN_KYC_SENT_STATUS")
	private String adminKycSentStatus;

	@Column(name = "ADMIN_APPROVE_STATUS")
	private String adminApproveStatus;

	@Column(name = "ADMIN_APPROVE_DATE")
	private Date adminApproveDate;

	@Column(name = "COMP_KYC_SENT_STATUS")
	private String compKycSentStatus;

	@Column(name = "COMP_APPROVE_STATUS")
	private String compApproveStatus;

	@Column(name = "COMP_APPROVE_DATE")
	private Date compApproveDate;

	@Column(name = "ADMIN_REMARKS")
	// @Type(type="TEXT")
	private String adminRemarks;

	@Column(name = "COMP_REMARKS")
	// @Type(type="TEXT")
	private String compRemarks;

	@Column(name = "NTUC_POL_NUM")
	private String ntucPolNum;

	@Column(name = "MGRAPPRSTS_BYADVSTF")
	private String mgrApprStsByAdvStf;

	@Column(name = "ADMINAPPRSTS_BYADVSTF")
	private String adminApprStsByAdvStf;

	@Column(name = "COMPAPPRSTS_BYADVSTF")
	private String compApprStsByAdvStf;

	@Column(name = "MGR_EMAIL_SENT_FLG")
	private String mgrEmailSentFlg;

	@Column(name = "CLIENT_FEEDBACK_FLAG")
	private String clientFeedbackFlag;

	@Column(name = "DISCLOSURE_FOLLOWUPS")
	// @Type(type="TEXT")
	private String disclosureFollowUps;

	@Column(name = "CALLBACK_CLIENT")
	private String callbackClient;

	@Column(name = "CALLBACK_ADVISER")
	private String callbackAdvisor;

	@Column(name = "CALLBACK_APPTDATE")
	private Date callbackApptDate;

	@Column(name = "CALLBACK_CUSTPH")
	private String callbackCustph;

	@Column(name = "CALLBACK_BY")
	private String callbackBy;

	@Column(name = "CALLBAK_ON_DATE")
	private Date callbakOnDate;

	@Column(name = "CALLBACK_FEEDBACKS")
	// @Type(type="TEXT")
	private String callbackFeedBaccks;

	@Column(name = "INTPRT_CONTACT")
	private String intprtContact;

	@Column(name = "INTPRT_NAME")
	private String intprtName;

	@Column(name = "INTPRT_NRIC")
	private String intprtNric;

	@Column(name = "INTPRT_RELAT")
	private String intprtRelat;

	@Column(name = "CD_LANGUAGES")
	private String cdLanguages;

	@Column(name = "FWR_DEPNTFLG")
	private String fwrDepntFlg;

	@Column(name = "FWR_FINASSETFLG")
	private String fwrFinAssetFlg;

	@Column(name = "PRODREC_OBJECTIVES")
	private String prodrecObjectives;

	@Column(name = "ADVDEC_OPTIONS")
	private String advdecOptions;

	@Column(name = "APPTYPES_CLIENT")
	private String apptypesClient;

	@Column(name = "CD_LANGUAGEOTH")
	private String cdLanguageOth;

	@Column(name = "ADVREC_REASON1")
	// @Type(type="TEXT")
	private String advrecReason1;

	@Column(name = "ADVREC_REASON2")
	// @Type(type="TEXT")
	private String advrecReason2;

	@Column(name = "ADVREC_REASON3")
	// @Type(type="TEXT")
	private String advrecReason3;

	@Column(name = "MGR_NAME")
	private String mgrName;

	@Column(name = "NTUC_CASE_LOCK")
	private String ntucCaseLock;

	@Column(name = "NTUC_LOCKED_CASE")
	private String ntucLockedCase;

	@Column(name = "NTUC_CASE_ID")
	private String ntucCaseId;

	@Column(name = "NTUC_CASE_STATUS")
	private String ntucCaseStatus;

	@Column(name = "POLICY_CREATE_FLG")
	private String policyCreateFlg;

	@Column(name = "CR_RISKCLASS_ILP")
	private String crRiskClassIlp;

	@Column(name = "CD_CONSENT_APPRCH")
	private String cdConsentApprch;

	public String getFnaId() {
		return fnaId;
	}

	public void setFnaId(String fnaId) {
		this.fnaId = fnaId;
	}

	public CustomerDetails getCustDetails() {
		return custDetails;
	}

	public void setCustDetails(CustomerDetails custDetails) {
		this.custDetails = custDetails;
	}

	public String getAdvstfId() {
		return advstfId;
	}

	public void setAdvstfId(String advstfId) {
		this.advstfId = advstfId;
	}

	public String getMgrId() {
		return mgrId;
	}

	public void setMgrId(String mgrId) {
		this.mgrId = mgrId;
	}

	public String getFnaType() {
		return fnaType;
	}

	public void setFnaType(String fnaType) {
		this.fnaType = fnaType;
	}

	public String getCcRepexistinvestflg() {
		return ccRepexistinvestflg;
	}

	public void setCcRepexistinvestflg(String ccRepexistinvestflg) {
		this.ccRepexistinvestflg = ccRepexistinvestflg;
	}

	public Date getCustSignDate() {
		return custSignDate;
	}

	public void setCustSignDate(Date custSignDate) {
		this.custSignDate = custSignDate;
	}

	public Date getSpsSignDate() {
		return spsSignDate;
	}

	public void setSpsSignDate(Date spsSignDate) {
		this.spsSignDate = spsSignDate;
	}

	public Date getAdvSignDate() {
		return advSignDate;
	}

	public void setAdvSignDate(Date advSignDate) {
		this.advSignDate = advSignDate;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Date getMgrSign() {
		return mgrSign;
	}

	public void setMgrSign(Date mgrSign) {
		this.mgrSign = mgrSign;
	}

	public Date getMgrSignDate() {
		return mgrSignDate;
	}

	public void setMgrSignDate(Date mgrSignDate) {
		this.mgrSignDate = mgrSignDate;
	}

	public String getCrInvstObj() {
		return crInvstObj;
	}

	public void setCrInvstObj(String crInvstObj) {
		this.crInvstObj = crInvstObj;
	}

	public float getCrInvstAmt() {
		return crInvstAmt;
	}

	public void setCrInvstAmt(float crInvstAmt) {
		this.crInvstAmt = crInvstAmt;
	}

	public String getCrInvstTimeHorizon() {
		return crInvstTimeHorizon;
	}

	public void setCrInvstTimeHorizon(String crInvstTimeHorizon) {
		this.crInvstTimeHorizon = crInvstTimeHorizon;
	}

	public String getCrRiskPref() {
		return crRiskPref;
	}

	public void setCrRiskPref(String crRiskPref) {
		this.crRiskPref = crRiskPref;
	}

	public String getCrRiskClass() {
		return crRiskClass;
	}

	public void setCrRiskClass(String crRiskClass) {
		this.crRiskClass = crRiskClass;
	}

	public float getCrRoi() {
		return crRoi;
	}

	public void setCrRoi(float crRoi) {
		this.crRoi = crRoi;
	}

	public String getCrAdeqFund() {
		return crAdeqFund;
	}

	public void setCrAdeqFund(String crAdeqFund) {
		this.crAdeqFund = crAdeqFund;
	}

	public String getCrOthConcern() {
		return crOthConcern;
	}

	public void setCrOthConcern(String crOthConcern) {
		this.crOthConcern = crOthConcern;
	}

	public String getCdMailAddrFlg() {
		return cdMailAddrFlg;
	}

	public void setCdMailAddrFlg(String cdMailAddrFlg) {
		this.cdMailAddrFlg = cdMailAddrFlg;
	}

	public String getCdCustMailAddr() {
		return cdCustMailAddr;
	}

	public void setCdCustMailAddr(String cdCustMailAddr) {
		this.cdCustMailAddr = cdCustMailAddr;
	}

	public String getCdSpsMailAddr() {
		return cdSpsMailAddr;
	}

	public void setCdSpsMailAddr(String cdSpsMailAddr) {
		this.cdSpsMailAddr = cdSpsMailAddr;
	}

	public String getCdMailAddrResFlg() {
		return cdMailAddrResFlg;
	}

	public void setCdMailAddrResFlg(String cdMailAddrResFlg) {
		this.cdMailAddrResFlg = cdMailAddrResFlg;
	}

	public String getCdMailAddrOthDet() {
		return cdMailAddrOthDet;
	}

	public void setCdMailAddrOthDet(String cdMailAddrOthDet) {
		this.cdMailAddrOthDet = cdMailAddrOthDet;
	}

	public String getCdIntrprtFlg() {
		return cdIntrprtFlg;
	}

	public void setCdIntrprtFlg(String cdIntrprtFlg) {
		this.cdIntrprtFlg = cdIntrprtFlg;
	}

	public String getCdBenfownFlg() {
		return cdBenfownFlg;
	}

	public void setCdBenfownFlg(String cdBenfownFlg) {
		this.cdBenfownFlg = cdBenfownFlg;
	}

	public String getCdTppFlg() {
		return cdTppFlg;
	}

	public void setCdTppFlg(String cdTppFlg) {
		this.cdTppFlg = cdTppFlg;
	}

	public String getCdPepFlg() {
		return cdPepFlg;
	}

	public void setCdPepFlg(String cdPepFlg) {
		this.cdPepFlg = cdPepFlg;
	}

	public String getAdvRecRemarks() {
		return advRecRemarks;
	}

	public void setAdvRecRemarks(String advRecRemarks) {
		this.advRecRemarks = advRecRemarks;
	}

	public String getSwrepConfDets() {
		return swrepConfDets;
	}

	public void setSwrepConfDets(String swrepConfDets) {
		this.swrepConfDets = swrepConfDets;
	}

	public String getSwrepRemarks() {
		return swrepRemarks;
	}

	public void setSwrepRemarks(String swrepRemarks) {
		this.swrepRemarks = swrepRemarks;
	}

	public String getSwrepPpFfRemarks() {
		return swrepPpFfRemarks;
	}

	public void setSwrepPpFfRemarks(String swrepPpFfRemarks) {
		this.swrepPpFfRemarks = swrepPpFfRemarks;
	}

	public String getCdackAgree() {
		return cdackAgree;
	}

	public void setCdackAgree(String cdackAgree) {
		this.cdackAgree = cdackAgree;
	}

	public String getAdvDecRemarks() {
		return advDecRemarks;
	}

	public void setAdvDecRemarks(String advDecRemarks) {
		this.advDecRemarks = advDecRemarks;
	}

	public String getAdvRecFnaNumber() {
		return advRecFnaNumber;
	}

	public void setAdvRecFnaNumber(String advRecFnaNumber) {
		this.advRecFnaNumber = advRecFnaNumber;
	}

	public String getAckMgrComments() {
		return ackMgrComments;
	}

	public void setAckMgrComments(String ackMgrComments) {
		this.ackMgrComments = ackMgrComments;
	}

	public String getAckMgrFnaNumber() {
		return ackMgrFnaNumber;
	}

	public void setAckMgrFnaNumber(String ackMgrFnaNumber) {
		this.ackMgrFnaNumber = ackMgrFnaNumber;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getArchStatus() {
		return archStatus;
	}

	public void setArchStatus(String archStatus) {
		this.archStatus = archStatus;
	}

	public String getKycSentStatus() {
		return kycSentStatus;
	}

	public void setKycSentStatus(String kycSentStatus) {
		this.kycSentStatus = kycSentStatus;
	}

	public String getMgrApproveStatus() {
		return mgrApproveStatus;
	}

	public void setMgrApproveStatus(String mgrApproveStatus) {
		this.mgrApproveStatus = mgrApproveStatus;
	}

	public Date getMgrApproveDate() {
		return mgrApproveDate;
	}

	public void setMgrApproveDate(Date mgrApproveDate) {
		this.mgrApproveDate = mgrApproveDate;
	}

	public String getCdCustEmplyr() {
		return cdCustEmplyr;
	}

	public void setCdCustEmplyr(String cdCustEmplyr) {
		this.cdCustEmplyr = cdCustEmplyr;
	}

	public String getCdSpsEmplyr() {
		return cdSpsEmplyr;
	}

	public void setCdSpsEmplyr(String cdSpsEmplyr) {
		this.cdSpsEmplyr = cdSpsEmplyr;
	}

	public String getCdCustAnnlIncome() {
		return cdCustAnnlIncome;
	}

	public void setCdCustAnnlIncome(String cdCustAnnlIncome) {
		this.cdCustAnnlIncome = cdCustAnnlIncome;
	}

	public String getCdSpsAnnlIncome() {
		return cdSpsAnnlIncome;
	}

	public void setCdSpsAnnlIncome(String cdSpsAnnlIncome) {
		this.cdSpsAnnlIncome = cdSpsAnnlIncome;
	}

	public String getCdRemarks() {
		return cdRemarks;
	}

	public void setCdRemarks(String cdRemarks) {
		this.cdRemarks = cdRemarks;
	}

	public String getAdvRecRemarks1() {
		return advRecRemarks1;
	}

	public void setAdvRecRemarks1(String advRecRemarks1) {
		this.advRecRemarks1 = advRecRemarks1;
	}

	public String getAdvRecFnaNumber1() {
		return advRecFnaNumber1;
	}

	public void setAdvRecFnaNumber1(String advRecFnaNumber1) {
		this.advRecFnaNumber1 = advRecFnaNumber1;
	}

	public String getAdvRecRemarks2() {
		return advRecRemarks2;
	}

	public void setAdvRecRemarks2(String advRecRemarks2) {
		this.advRecRemarks2 = advRecRemarks2;
	}

	public String getAdvRecFnaNumber2() {
		return advRecFnaNumber2;
	}

	public void setAdvRecFnaNumber2(String advRecFnaNumber2) {
		this.advRecFnaNumber2 = advRecFnaNumber2;
	}

	public String getAckMgrComments1() {
		return ackMgrComments1;
	}

	public void setAckMgrComments1(String ackMgrComments1) {
		this.ackMgrComments1 = ackMgrComments1;
	}

	public String getAckMgrFnaNumber1() {
		return ackMgrFnaNumber1;
	}

	public void setAckMgrFnaNumber1(String ackMgrFnaNumber1) {
		this.ackMgrFnaNumber1 = ackMgrFnaNumber1;
	}

	public String getFatcaBirthPlace() {
		return fatcaBirthPlace;
	}

	public void setFatcaBirthPlace(String fatcaBirthPlace) {
		this.fatcaBirthPlace = fatcaBirthPlace;
	}

	public String getMgrStatusRemarks() {
		return mgrStatusRemarks;
	}

	public void setMgrStatusRemarks(String mgrStatusRemarks) {
		this.mgrStatusRemarks = mgrStatusRemarks;
	}

	public String getClientConsent() {
		return clientConsent;
	}

	public void setClientConsent(String clientConsent) {
		this.clientConsent = clientConsent;
	}

	public String getFatcaNoContryFlg() {
		return fatcaNoContryFlg;
	}

	public void setFatcaNoContryFlg(String fatcaNoContryFlg) {
		this.fatcaNoContryFlg = fatcaNoContryFlg;
	}

	public String getCustAttachFlg() {
		return custAttachFlg;
	}

	public void setCustAttachFlg(String custAttachFlg) {
		this.custAttachFlg = custAttachFlg;
	}

	public String getSuprevMgrFlg() {
		return suprevMgrFlg;
	}

	public void setSuprevMgrFlg(String suprevMgrFlg) {
		this.suprevMgrFlg = suprevMgrFlg;
	}

	public String getSuprevFollowReason() {
		return suprevFollowReason;
	}

	public void setSuprevFollowReason(String suprevFollowReason) {
		this.suprevFollowReason = suprevFollowReason;
	}

	public String getCdackAdvrecomm() {
		return cdackAdvrecomm;
	}

	public void setCdackAdvrecomm(String cdackAdvrecomm) {
		this.cdackAdvrecomm = cdackAdvrecomm;
	}

	public String getCdMrktmatPostalflg() {
		return cdMrktmatPostalflg;
	}

	public void setCdMrktmatPostalflg(String cdMrktmatPostalflg) {
		this.cdMrktmatPostalflg = cdMrktmatPostalflg;
	}

	public String getCdMrktmatEmailflg() {
		return cdMrktmatEmailflg;
	}

	public void setCdMrktmatEmailflg(String cdMrktmatEmailflg) {
		this.cdMrktmatEmailflg = cdMrktmatEmailflg;
	}

	public String getNtucPolicyId() {
		return ntucPolicyId;
	}

	public void setNtucPolicyId(String ntucPolicyId) {
		this.ntucPolicyId = ntucPolicyId;
	}

	public String getAdminKycSentStatus() {
		return adminKycSentStatus;
	}

	public void setAdminKycSentStatus(String adminKycSentStatus) {
		this.adminKycSentStatus = adminKycSentStatus;
	}

	public String getAdminApproveStatus() {
		return adminApproveStatus;
	}

	public void setAdminApproveStatus(String adminApproveStatus) {
		this.adminApproveStatus = adminApproveStatus;
	}

	public Date getAdminApproveDate() {
		return adminApproveDate;
	}

	public void setAdminApproveDate(Date adminApproveDate) {
		this.adminApproveDate = adminApproveDate;
	}

	public String getCompKycSentStatus() {
		return compKycSentStatus;
	}

	public void setCompKycSentStatus(String compKycSentStatus) {
		this.compKycSentStatus = compKycSentStatus;
	}

	public String getCompApproveStatus() {
		return compApproveStatus;
	}

	public void setCompApproveStatus(String compApproveStatus) {
		this.compApproveStatus = compApproveStatus;
	}

	public Date getCompApproveDate() {
		return compApproveDate;
	}

	public void setCompApproveDate(Date compApproveDate) {
		this.compApproveDate = compApproveDate;
	}

	public String getAdminRemarks() {
		return adminRemarks;
	}

	public void setAdminRemarks(String adminRemarks) {
		this.adminRemarks = adminRemarks;
	}

	public String getCompRemarks() {
		return compRemarks;
	}

	public void setCompRemarks(String compRemarks) {
		this.compRemarks = compRemarks;
	}

	public String getNtucPolNum() {
		return ntucPolNum;
	}

	public void setNtucPolNum(String ntucPolNum) {
		this.ntucPolNum = ntucPolNum;
	}

	public String getMgrApprStsByAdvStf() {
		return mgrApprStsByAdvStf;
	}

	public void setMgrApprStsByAdvStf(String mgrApprStsByAdvStf) {
		this.mgrApprStsByAdvStf = mgrApprStsByAdvStf;
	}

	public String getAdminApprStsByAdvStf() {
		return adminApprStsByAdvStf;
	}

	public void setAdminApprStsByAdvStf(String adminApprStsByAdvStf) {
		this.adminApprStsByAdvStf = adminApprStsByAdvStf;
	}

	public String getCompApprStsByAdvStf() {
		return compApprStsByAdvStf;
	}

	public void setCompApprStsByAdvStf(String compApprStsByAdvStf) {
		this.compApprStsByAdvStf = compApprStsByAdvStf;
	}

	public String getMgrEmailSentFlg() {
		return mgrEmailSentFlg;
	}

	public void setMgrEmailSentFlg(String mgrEmailSentFlg) {
		this.mgrEmailSentFlg = mgrEmailSentFlg;
	}

	public String getClientFeedbackFlag() {
		return clientFeedbackFlag;
	}

	public void setClientFeedbackFlag(String clientFeedbackFlag) {
		this.clientFeedbackFlag = clientFeedbackFlag;
	}

	public String getDisclosureFollowUps() {
		return disclosureFollowUps;
	}

	public void setDisclosureFollowUps(String disclosureFollowUps) {
		this.disclosureFollowUps = disclosureFollowUps;
	}

	public String getCallbackClient() {
		return callbackClient;
	}

	public void setCallbackClient(String callbackClient) {
		this.callbackClient = callbackClient;
	}

	public String getCallbackAdvisor() {
		return callbackAdvisor;
	}

	public void setCallbackAdvisor(String callbackAdvisor) {
		this.callbackAdvisor = callbackAdvisor;
	}

	public Date getCallbackApptDate() {
		return callbackApptDate;
	}

	public void setCallbackApptDate(Date callbackApptDate) {
		this.callbackApptDate = callbackApptDate;
	}

	public String getCallbackCustph() {
		return callbackCustph;
	}

	public void setCallbackCustph(String callbackCustph) {
		this.callbackCustph = callbackCustph;
	}

	public String getCallbackBy() {
		return callbackBy;
	}

	public void setCallbackBy(String callbackBy) {
		this.callbackBy = callbackBy;
	}

	public Date getCallbakOnDate() {
		return callbakOnDate;
	}

	public void setCallbakOnDate(Date callbakOnDate) {
		this.callbakOnDate = callbakOnDate;
	}

	public String getCallbackFeedBaccks() {
		return callbackFeedBaccks;
	}

	public void setCallbackFeedBaccks(String callbackFeedBaccks) {
		this.callbackFeedBaccks = callbackFeedBaccks;
	}

	public String getIntprtContact() {
		return intprtContact;
	}

	public void setIntprtContact(String intprtContact) {
		this.intprtContact = intprtContact;
	}

	public String getIntprtName() {
		return intprtName;
	}

	public void setIntprtName(String intprtName) {
		this.intprtName = intprtName;
	}

	public String getIntprtNric() {
		return intprtNric;
	}

	public void setIntprtNric(String intprtNric) {
		this.intprtNric = intprtNric;
	}

	public String getIntprtRelat() {
		return intprtRelat;
	}

	public void setIntprtRelat(String intprtRelat) {
		this.intprtRelat = intprtRelat;
	}

	public String getCdLanguages() {
		return cdLanguages;
	}

	public void setCdLanguages(String cdLanguages) {
		this.cdLanguages = cdLanguages;
	}

	public String getFwrDepntFlg() {
		return fwrDepntFlg;
	}

	public void setFwrDepntFlg(String fwrDepntFlg) {
		this.fwrDepntFlg = fwrDepntFlg;
	}

	public String getFwrFinAssetFlg() {
		return fwrFinAssetFlg;
	}

	public void setFwrFinAssetFlg(String fwrFinAssetFlg) {
		this.fwrFinAssetFlg = fwrFinAssetFlg;
	}

	public String getProdrecObjectives() {
		return prodrecObjectives;
	}

	public void setProdrecObjectives(String prodrecObjectives) {
		this.prodrecObjectives = prodrecObjectives;
	}

	public String getAdvdecOptions() {
		return advdecOptions;
	}

	public void setAdvdecOptions(String advdecOptions) {
		this.advdecOptions = advdecOptions;
	}

	public String getApptypesClient() {
		return apptypesClient;
	}

	public void setApptypesClient(String apptypesClient) {
		this.apptypesClient = apptypesClient;
	}

	public String getCdLanguageOth() {
		return cdLanguageOth;
	}

	public void setCdLanguageOth(String cdLanguageOth) {
		this.cdLanguageOth = cdLanguageOth;
	}

	public String getAdvrecReason() {
		return advrecReason;
	}

	public void setAdvrecReason(String advrecReason) {
		this.advrecReason = advrecReason;
	}

	public String getAdvrecReason1() {
		return advrecReason1;
	}

	public void setAdvrecReason1(String advrecReason1) {
		this.advrecReason1 = advrecReason1;
	}

	public String getAdvrecReason2() {
		return advrecReason2;
	}

	public void setAdvrecReason2(String advrecReason2) {
		this.advrecReason2 = advrecReason2;
	}

	public String getAdvrecReason3() {
		return advrecReason3;
	}

	public void setAdvrecReason3(String advrecReason3) {
		this.advrecReason3 = advrecReason3;
	}

	public String getMgrName() {
		return mgrName;
	}

	public void setMgrName(String mgrName) {
		this.mgrName = mgrName;
	}

	public String getNtucCaseLock() {
		return ntucCaseLock;
	}

	public void setNtucCaseLock(String ntucCaseLock) {
		this.ntucCaseLock = ntucCaseLock;
	}

	public String getNtucLockedCase() {
		return ntucLockedCase;
	}

	public void setNtucLockedCase(String ntucLockedCase) {
		this.ntucLockedCase = ntucLockedCase;
	}

	public String getNtucCaseId() {
		return ntucCaseId;
	}

	public void setNtucCaseId(String ntucCaseId) {
		this.ntucCaseId = ntucCaseId;
	}

	public String getNtucCaseStatus() {
		return ntucCaseStatus;
	}

	public void setNtucCaseStatus(String ntucCaseStatus) {
		this.ntucCaseStatus = ntucCaseStatus;
	}

	public String getPolicyCreateFlg() {
		return policyCreateFlg;
	}

	public void setPolicyCreateFlg(String policyCreateFlg) {
		this.policyCreateFlg = policyCreateFlg;
	}

	public String getCrRiskClassIlp() {
		return crRiskClassIlp;
	}

	public void setCrRiskClassIlp(String crRiskClassIlp) {
		this.crRiskClassIlp = crRiskClassIlp;
	}

	public String getCdConsentApprch() {
		return cdConsentApprch;
	}

	public void setCdConsentApprch(String cdConsentApprch) {
		this.cdConsentApprch = cdConsentApprch;
	}

	public String getSwrepConflg() {
		return swrepConflg;
	}

	public void setSwrepConflg(String swrepConflg) {
		this.swrepConflg = swrepConflg;
	}

	public String getSwrepAdvbyflg() {
		return swrepAdvbyflg;
	}

	public void setSwrepAdvbyflg(String swrepAdvbyflg) {
		this.swrepAdvbyflg = swrepAdvbyflg;
	}

	public String getSwrepDisadvgflg() {
		return swrepDisadvgflg;
	}

	public void setSwrepDisadvgflg(String swrepDisadvgflg) {
		this.swrepDisadvgflg = swrepDisadvgflg;
	}

	public String getSwrepProceedflg() {
		return swrepProceedflg;
	}

	public void setSwrepProceedflg(String swrepProceedflg) {
		this.swrepProceedflg = swrepProceedflg;
	}

}
