package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FNA_SWTCHREP_FUND_DET")
public class FnaSwtchRepFundDet {

	@Id
	@Column(name = "SWREP_FF_ID", nullable = false)
	private String swrepFfId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "SWREP_FF_PRODTYPE")
	private String swrepFfProdType;

	@Column(name = "SWREP_FF_PRODNAME")
	private String swrepFfProdName;

	@Column(name = "SWREP_FF_FUNDRISKRATE")
	private float swrepFfFundRiskRate;

	@Column(name = "SWREP_FF_PAYMENTMODE")
	private String swrepFfPaymentMode;

	@Column(name = "SWREP_FF_SOREDMUNITS")
	private float swrepFfSoRedMunIts;

	@Column(name = "SWREP_FF_SOREDPRCNT")
	private float swrepFfSoRedPrcnt;

	@Column(name = "SWREP_FF_SIREPSALESCHRG")
	private float swrepFfSiRepSalesChrg;

	@Column(name = "SWREP_FF_SIREPAMT")
	private float swrepFfSiRepAmt;

	@Column(name = "SWREP_FF_SIREPPRCNT")
	private float swrepFfSiRepPrcnt;

	@Column(name = "SWREP_FF_IAFFEE")
	private String swrepFfIaffee;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = " SWREP_PP_PRIN")
	private String swrepPpPrin;

	@Column(name = " SWREP_PP_PLAN")
	private String swrepPpPlan;

	@Column(name = "SWREP_FF_BSRID")
	private String swrepFfBsRid;

	@Column(name = "SWREP_FF_OPT")
	private String swrepFfOpt;

	public String getSwrepFfId() {
		return swrepFfId;
	}

	public void setSwrepFfId(String swrepFfId) {
		this.swrepFfId = swrepFfId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getSwrepFfProdType() {
		return swrepFfProdType;
	}

	public void setSwrepFfProdType(String swrepFfProdType) {
		this.swrepFfProdType = swrepFfProdType;
	}

	public String getSwrepFfProdName() {
		return swrepFfProdName;
	}

	public void setSwrepFfProdName(String swrepFfProdName) {
		this.swrepFfProdName = swrepFfProdName;
	}

	public float getSwrepFfFundRiskRate() {
		return swrepFfFundRiskRate;
	}

	public void setSwrepFfFundRiskRate(float swrepFfFundRiskRate) {
		this.swrepFfFundRiskRate = swrepFfFundRiskRate;
	}

	public String getSwrepFfPaymentMode() {
		return swrepFfPaymentMode;
	}

	public void setSwrepFfPaymentMode(String swrepFfPaymentMode) {
		this.swrepFfPaymentMode = swrepFfPaymentMode;
	}

	public float getSwrepFfSoRedMunIts() {
		return swrepFfSoRedMunIts;
	}

	public void setSwrepFfSoRedMunIts(float swrepFfSoRedMunIts) {
		this.swrepFfSoRedMunIts = swrepFfSoRedMunIts;
	}

	public float getSwrepFfSoRedPrcnt() {
		return swrepFfSoRedPrcnt;
	}

	public void setSwrepFfSoRedPrcnt(float swrepFfSoRedPrcnt) {
		this.swrepFfSoRedPrcnt = swrepFfSoRedPrcnt;
	}

	public float getSwrepFfSiRepSalesChrg() {
		return swrepFfSiRepSalesChrg;
	}

	public void setSwrepFfSiRepSalesChrg(float swrepFfSiRepSalesChrg) {
		this.swrepFfSiRepSalesChrg = swrepFfSiRepSalesChrg;
	}

	public float getSwrepFfSiRepAmt() {
		return swrepFfSiRepAmt;
	}

	public void setSwrepFfSiRepAmt(float swrepFfSiRepAmt) {
		this.swrepFfSiRepAmt = swrepFfSiRepAmt;
	}

	public float getSwrepFfSiRepPrcnt() {
		return swrepFfSiRepPrcnt;
	}

	public void setSwrepFfSiRepPrcnt(float swrepFfSiRepPrcnt) {
		this.swrepFfSiRepPrcnt = swrepFfSiRepPrcnt;
	}

	public String getSwrepFfIaffee() {
		return swrepFfIaffee;
	}

	public void setSwrepFfIaffee(String swrepFfIaffee) {
		this.swrepFfIaffee = swrepFfIaffee;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getSwrepPpPrin() {
		return swrepPpPrin;
	}

	public void setSwrepPpPrin(String swrepPpPrin) {
		this.swrepPpPrin = swrepPpPrin;
	}

	public String getSwrepPpPlan() {
		return swrepPpPlan;
	}

	public void setSwrepPpPlan(String swrepPpPlan) {
		this.swrepPpPlan = swrepPpPlan;
	}

	public String getSwrepFfBsRid() {
		return swrepFfBsRid;
	}

	public void setSwrepFfBsRid(String swrepFfBsRid) {
		this.swrepFfBsRid = swrepFfBsRid;
	}

	public String getSwrepFfOpt() {
		return swrepFfOpt;
	}

	public void setSwrepFfOpt(String swrepFfOpt) {
		this.swrepFfOpt = swrepFfOpt;
	}

}
