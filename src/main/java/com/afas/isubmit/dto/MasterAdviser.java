package com.afas.isubmit.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "MASTER_ADVISER")
public class MasterAdviser implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "masterAdviser", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + ".adv_sequence"),
			@Parameter(name = "sequence_prefix", value = "ADV") })
	@GeneratedValue(generator = "masterAdviser", strategy = GenerationType.SEQUENCE)
	@Column(name = "ADVSTF_ID")
	private String advStfId;

	@Column(name = "ADVSTF_NAME")
	private String advStfName;

	@Column(name = "STAFF_TYPE")
	private String staffType;

	@Column(name = "NRIC")
	private String nric;

	@Column(name = "HAND_PH")
	private String handPhone;

	@Column(name = "EMAIL_ID")
	private String emailId;

	@Column(name = "DOB")
	private Date dob;

	@Column(name = "SEX")
	private String sex;

	@Column(name = "REMARKS")
	private String remarks;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TENANT_ID", nullable = false)
	private MasterTenantDist tenantId;

	//@ManyToOne(fetch = FetchType.EAGER)
	//@JoinColumn(name = "MANAGER_ID")
	@Column(name = "MANAGER_ID")
	private String managerId;
	
	@Column(name = "MANAGER")
	private String manager;

	public String getAdvStfId() {
		return advStfId;
	}

	public void setAdvStfId(String advStfId) {
		this.advStfId = advStfId;
	}

	public String getAdvStfName() {
		return advStfName;
	}

	public void setAdvStfName(String advStfName) {
		this.advStfName = advStfName;
	}

	public String getStaffType() {
		return staffType;
	}

	public void setStaffType(String staffType) {
		this.staffType = staffType;
	}

	public String getNric() {
		return nric;
	}

	public void setNric(String nric) {
		this.nric = nric;
	}

	public String getHandPhone() {
		return handPhone;
	}

	public void setHandPhone(String handPhone) {
		this.handPhone = handPhone;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public MasterTenantDist getTenantId() {
		return tenantId;
	}

	public void setTenantId(MasterTenantDist tenantId) {
		this.tenantId = tenantId;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getManager() {
		return manager;
	}

	public void setManager(String manager) {
		this.manager = manager;
	}

}
