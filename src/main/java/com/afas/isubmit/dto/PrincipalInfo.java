package com.afas.isubmit.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "PRINCIPAL_INFO")
public class PrincipalInfo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GenericGenerator(name = "principalInfo", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + ".princinfo_sequence"),
			@Parameter(name = "sequence_prefix", value ="PRI")})
	@GeneratedValue(generator = "principalInfo", strategy = GenerationType.SEQUENCE)
	@Column(name = "princ_info_id")
	private String princeinfoid;
	
	@Column(name = "princ_name")
	private String princName;
	
	@Column(name= "princ_code")
	private String princCode;
	
	@Column(name = "adviser_code")
	private String adviserCode;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ADVSTF_ID")
	private MasterAdviser advstfId;
	
	//@Column(name = "ADVSTF_ID")
	//private String advstfId;
	
	@Column(name = "remarks")
	private String remarks;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "created_date")
	private Date createdDate;
	
	@Column(name = "modify_by")
	private String modifyBy;
	
	@Column(name = "modify_date")
	private Date modifyDate;

	public String getPrinceinfoid() {
		return princeinfoid;
	}

	public void setPrinceinfoid(String princeinfoid) {
		this.princeinfoid = princeinfoid;
	}

	public String getPrincName() {
		return princName;
	}

	public void setPrincName(String princName) {
		this.princName = princName;
	}

	public String getPrincCode() {
		return princCode;
	}

	public void setPrincCode(String princCode) {
		this.princCode = princCode;
	}

	public String getAdviserCode() {
		return adviserCode;
	}

	public void setAdviserCode(String adviserCode) {
		this.adviserCode = adviserCode;
	}

	 
	public MasterAdviser getAdvstfId() {
		return advstfId;
	}

	public void setAdvstfId(MasterAdviser advstfId) {
		this.advstfId = advstfId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}
	
	
}
