package com.afas.isubmit.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "USER_LOGIN")
public class UserLogin implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "userLogin", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + ".user_sequence"),
			@Parameter(name = "sequence_prefix", value = "user") })
	@GeneratedValue(generator = "userLogin", strategy = GenerationType.SEQUENCE)
	@Column(name = "user_id")
	private String userId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "TENANT_ID")
	private MasterTenantDist tenantId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ADVSTF_ID")
	private MasterAdviser advStfId;

	@Column(name = "EMAIL_ID")
	private String emailId;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;
	
	@Column(name = "USER_TYPE")
	private String userType;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public MasterTenantDist getTenantId() {
		return tenantId;
	}

	public void setTenantId(MasterTenantDist tenantId) {
		this.tenantId = tenantId;
	}

	public MasterAdviser getAdvStfId() {
		return advStfId;
	}

	public void setAdvStfId(MasterAdviser advStfId) {
		this.advStfId = advStfId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}
