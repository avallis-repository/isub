package com.afas.isubmit.dto;

public class Email {

	private String sendToMail;
	private String sendToCc;

	public String getSendToMail() {
		return sendToMail;
	}

	public void setSendToMail(String sendToMail) {
		this.sendToMail = sendToMail;
	}

	public String getSendToCc() {
		return sendToCc;
	}

	public void setSendToCc(String sendToCc) {
		this.sendToCc = sendToCc;
	}

}
