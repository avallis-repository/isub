package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "FNA_FATCA_TAXDET")
public class FnaFatcaTaxDet {

	@Id
	@GenericGenerator(name = "taxDets", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "fna_fatca_seq"),
			@Parameter(name = "sequence_prefix", value = "TAX"), })
	@GeneratedValue(generator = "taxDets", strategy = GenerationType.SEQUENCE)
	@Column(name = "TAX_ID", nullable = false)
	private String taxId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "TAX_COUNTRY")
	private String taxCountry;

	@Column(name = "TAX_REFNO")
	private String taxRefNo;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "REASON_TO_NOTAX")
	private String reasonToNoTax;

	@Column(name = "REASONB_DETAILS")
	// @Type(type="TEXT")
	private String reasonbDetails;

	@Column(name = "TAX_MOD_BY")
	private String taxModBy;

	@Column(name = "TAX_MOD_DATE")
	private Date taxModDate;

	@Column(name = "TAX_FOR")
	private String taxFor;

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getTaxCountry() {
		return taxCountry;
	}

	public void setTaxCountry(String taxCountry) {
		this.taxCountry = taxCountry;
	}

	public String getTaxRefNo() {
		return taxRefNo;
	}

	public void setTaxRefNo(String taxRefNo) {
		this.taxRefNo = taxRefNo;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getReasonToNoTax() {
		return reasonToNoTax;
	}

	public void setReasonToNoTax(String reasonToNoTax) {
		this.reasonToNoTax = reasonToNoTax;
	}

	public String getReasonbDetails() {
		return reasonbDetails;
	}

	public void setReasonbDetails(String reasonbDetails) {
		this.reasonbDetails = reasonbDetails;
	}

	public String getTaxModBy() {
		return taxModBy;
	}

	public void setTaxModBy(String taxModBy) {
		this.taxModBy = taxModBy;
	}

	public Date getTaxModDate() {
		return taxModDate;
	}

	public void setTaxModDate(Date taxModDate) {
		this.taxModDate = taxModDate;
	}

	public String getTaxFor() {
		return taxFor;
	}

	public void setTaxFor(String taxFor) {
		this.taxFor = taxFor;
	}

}
