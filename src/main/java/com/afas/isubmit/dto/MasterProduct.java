package com.afas.isubmit.dto;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the master_product database table.
 * 
 */
@Entity
@Table(name = "master_product")
public class MasterProduct {

	@Id
	@Column(name = "prod_code")
	private String prodCode;

	@Column(name = "benf_rel_prd_id")
	private String benfRelPrdId;

	@Column(name = "bid_offer_spread")
	private Double bidOfferSpread;

	@Column(name = "comm_calc_prd_flg")
	private String commCalcPrdFlg;

	@Column(name = "comm_entl")
	private String commEntl;

	@Column(name = "comm_or_incl_flag")
	private String commOrInclFlag;

	@Column(name = "comm_prcnt")
	private Double commPrcnt;

	@Column(name = "comm_term_flag")
	private String commTermFlag;

	@Column(name = "comm_type_flag")
	private String commTypeFlag;

	@Column(name = "component_code")
	private String componentCode;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "end_date")
	private Date endDate;

	@Column(name = "fund_mngr_name")
	private String fundMngrName;

	@Column(name = "govrn_prov")
	private String govrnProv;

	@Column(name = "gst_flg")
	private String gstFlg;

	@Column(name = "incen_qual")
	private String incenQual;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "plan_code")
	private String planCode;

	@Column(name = "plan_desc")
	private String planDesc;

	@Column(name = "plan_name")
	private String planName;

	@Column(name = "prem_type")
	private String premType;

	@Column(name = "prin_code")
	private String prinCode;

	@Column(name = "prod_qual")
	private String prodQual;

	@Column(name = "prod_type")
	private String prodType;

	@Column(name = "product_line_id")
	private String productLineId;

	@Column(name = "product_status")
	private String productStatus;

	@Column(name = "shld_cpf_prm_flg")
	private String shldCpfPrmFlg;

	@Column(name = "source_data")
	private String sourceData;

	@Column(name = "start_date")
	private Date startDate;

	@Column(name = "term_date")
	private Date termDate;

	// bi-directional many-to-one association to MasterPrincipal
	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "prin_id")
	@JsonIgnore
	private MasterPrincipal masterPrincipal;

	public MasterProduct() {
	}

	public String getProdCode() {
		return this.prodCode;
	}

	public void setProdCode(String prodCode) {
		this.prodCode = prodCode;
	}

	public String getBenfRelPrdId() {
		return this.benfRelPrdId;
	}

	public void setBenfRelPrdId(String benfRelPrdId) {
		this.benfRelPrdId = benfRelPrdId;
	}

	public Double getBidOfferSpread() {
		return this.bidOfferSpread;
	}

	public void setBidOfferSpread(double bidOfferSpread) {
		this.bidOfferSpread = bidOfferSpread;
	}

	public String getCommCalcPrdFlg() {
		return this.commCalcPrdFlg;
	}

	public void setCommCalcPrdFlg(String commCalcPrdFlg) {
		this.commCalcPrdFlg = commCalcPrdFlg;
	}

	public String getCommEntl() {
		return this.commEntl;
	}

	public void setCommEntl(String commEntl) {
		this.commEntl = commEntl;
	}

	public String getCommOrInclFlag() {
		return this.commOrInclFlag;
	}

	public void setCommOrInclFlag(String commOrInclFlag) {
		this.commOrInclFlag = commOrInclFlag;
	}

	public Double getCommPrcnt() {
		return this.commPrcnt;
	}

	public void setCommPrcnt(Double commPrcnt) {
		this.commPrcnt = commPrcnt;
	}

	public String getCommTermFlag() {
		return this.commTermFlag;
	}

	public void setCommTermFlag(String commTermFlag) {
		this.commTermFlag = commTermFlag;
	}

	public String getCommTypeFlag() {
		return this.commTypeFlag;
	}

	public void setCommTypeFlag(String commTypeFlag) {
		this.commTypeFlag = commTypeFlag;
	}

	public String getComponentCode() {
		return this.componentCode;
	}

	public void setComponentCode(String componentCode) {
		this.componentCode = componentCode;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFundMngrName() {
		return this.fundMngrName;
	}

	public void setFundMngrName(String fundMngrName) {
		this.fundMngrName = fundMngrName;
	}

	public String getGovrnProv() {
		return this.govrnProv;
	}

	public void setGovrnProv(String govrnProv) {
		this.govrnProv = govrnProv;
	}

	public String getGstFlg() {
		return this.gstFlg;
	}

	public void setGstFlg(String gstFlg) {
		this.gstFlg = gstFlg;
	}

	public String getIncenQual() {
		return this.incenQual;
	}

	public void setIncenQual(String incenQual) {
		this.incenQual = incenQual;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPlanCode() {
		return this.planCode;
	}

	public void setPlanCode(String planCode) {
		this.planCode = planCode;
	}

	public String getPlanDesc() {
		return this.planDesc;
	}

	public void setPlanDesc(String planDesc) {
		this.planDesc = planDesc;
	}

	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPremType() {
		return this.premType;
	}

	public void setPremType(String premType) {
		this.premType = premType;
	}

	public String getPrinCode() {
		return this.prinCode;
	}

	public void setPrinCode(String prinCode) {
		this.prinCode = prinCode;
	}

	public String getProdQual() {
		return this.prodQual;
	}

	public void setProdQual(String prodQual) {
		this.prodQual = prodQual;
	}

	public String getProdType() {
		return this.prodType;
	}

	public void setProdType(String prodType) {
		this.prodType = prodType;
	}

	public String getProductLineId() {
		return this.productLineId;
	}

	public void setProductLineId(String productLineId) {
		this.productLineId = productLineId;
	}

	public String getProductStatus() {
		return this.productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	public String getShldCpfPrmFlg() {
		return this.shldCpfPrmFlg;
	}

	public void setShldCpfPrmFlg(String shldCpfPrmFlg) {
		this.shldCpfPrmFlg = shldCpfPrmFlg;
	}

	public String getSourceData() {
		return this.sourceData;
	}

	public void setSourceData(String sourceData) {
		this.sourceData = sourceData;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getTermDate() {
		return this.termDate;
	}

	public void setTermDate(Date termDate) {
		this.termDate = termDate;
	}

	public MasterPrincipal getMasterPrincipal() {
		return this.masterPrincipal;
	}

	public void setMasterPrincipal(MasterPrincipal masterPrincipal) {
		this.masterPrincipal = masterPrincipal;
	}

}