package com.afas.isubmit.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the master_principal database table.
 * 
 */
@Entity
@Table(name = "master_principal")
public class MasterPrincipal {

	@Id
	@Column(name = "prin_id")
	private String prinId;

	private String addr1;

	private String addr2;

	private String addr3;

	private String city;

	private String country;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "email_id")
	private String emailId;

	@Column(name = "fax_no")
	private String faxNo;

	@Column(name = "life_ins_flag")
	private String lifeInsFlag;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "phone_no")
	private String phoneNo;

	@Column(name = "postal_code")
	private String postalCode;

	@Column(name = "prin_code")
	private String prinCode;

	@Column(name = "prin_name")
	private String prinName;

	private String remarks;

	private String state;

	@Column(name = "web_site")
	private String webSite;

	// bi-directional many-to-one association to MasterProduct
	@OneToMany(mappedBy = "masterPrincipal", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JsonIgnore
	private List<MasterProduct> masterProducts;

	public MasterPrincipal() {
	}

	public String getPrinId() {
		return this.prinId;
	}

	public void setPrinId(String prinId) {
		this.prinId = prinId;
	}

	public String getAddr1() {
		return this.addr1;
	}

	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}

	public String getAddr2() {
		return this.addr2;
	}

	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}

	public String getAddr3() {
		return this.addr3;
	}

	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFaxNo() {
		return this.faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getLifeInsFlag() {
		return this.lifeInsFlag;
	}

	public void setLifeInsFlag(String lifeInsFlag) {
		this.lifeInsFlag = lifeInsFlag;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getPhoneNo() {
		return this.phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getPostalCode() {
		return this.postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getPrinCode() {
		return this.prinCode;
	}

	public void setPrinCode(String prinCode) {
		this.prinCode = prinCode;
	}

	public String getPrinName() {
		return this.prinName;
	}

	public void setPrinName(String prinName) {
		this.prinName = prinName;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getWebSite() {
		return this.webSite;
	}

	public void setWebSite(String webSite) {
		this.webSite = webSite;
	}

	public List<MasterProduct> getMasterProducts() {
		return this.masterProducts;
	}

	public void setMasterProducts(List<MasterProduct> masterProducts) {
		this.masterProducts = masterProducts;
	}

	public MasterProduct addMasterProduct(MasterProduct masterProduct) {
		getMasterProducts().add(masterProduct);
		masterProduct.setMasterPrincipal(this);

		return masterProduct;
	}

	public MasterProduct removeMasterProduct(MasterProduct masterProduct) {
		getMasterProducts().remove(masterProduct);
		masterProduct.setMasterPrincipal(null);

		return masterProduct;
	}

}