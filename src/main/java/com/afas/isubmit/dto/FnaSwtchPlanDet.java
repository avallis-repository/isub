package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FNA_SWTCHREP_PLAN_DET")
public class FnaSwtchPlanDet {

	@Id
	@Column(name = "SWREP_PP_ID", nullable = false)
	private String swrepPpId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "SWREP_PP_PRODTYPE")
	private String swrepPpProdType;

	@Column(name = "SWREP_PP_PRODNAME")
	private String swrepPpProdName;

	@Column(name = "SWREP_PP_SUMASSR")
	private float swrepPpSumAssr;

	@Column(name = "SWREP_PP_TRANSIND")
	private String swrepPpTransInd;

	@Column(name = "SWREP_PP_PAYMENTMODE")
	private String swrepPpPaymentMode;

	@Column(name = "SWREP_PP_PLANTERM")
	private String swrepPpPlanTerm;

	@Column(name = "SWREP_PP_PAYTERM")
	private String swrepPpPayTerm;

	@Column(name = "SWREP_PP_PREMIUM")
	private float swrepPpPremium;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "SWREP_PP_PRIN")
	private String swrepPpPrin;

	@Column(name = "SWREP_PP_PLAN")
	private String swrepPpPlan;

	@Column(name = "SWREP_PP_BASRID")
	private String swrepPpBasRid;

	@Column(name = "SWREP_PP_OPT")
	private String swrepPpOpt;

	@Column(name = "SWREP_PP_NAME")
	private String swrepPpName;

	@Column(name = "PROD_RISK_RATE")
	private float prodRiskRate;

	public String getSwrepPpId() {
		return swrepPpId;
	}

	public void setSwrepPpId(String swrepPpId) {
		this.swrepPpId = swrepPpId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getSwrepPpProdType() {
		return swrepPpProdType;
	}

	public void setSwrepPpProdType(String swrepPpProdType) {
		this.swrepPpProdType = swrepPpProdType;
	}

	public String getSwrepPpProdName() {
		return swrepPpProdName;
	}

	public void setSwrepPpProdName(String swrepPpProdName) {
		this.swrepPpProdName = swrepPpProdName;
	}

	public float getSwrepPpSumAssr() {
		return swrepPpSumAssr;
	}

	public void setSwrepPpSumAssr(float swrepPpSumAssr) {
		this.swrepPpSumAssr = swrepPpSumAssr;
	}

	public String getSwrepPpTransInd() {
		return swrepPpTransInd;
	}

	public void setSwrepPpTransInd(String swrepPpTransInd) {
		this.swrepPpTransInd = swrepPpTransInd;
	}

	public String getSwrepPpPaymentMode() {
		return swrepPpPaymentMode;
	}

	public void setSwrepPpPaymentMode(String swrepPpPaymentMode) {
		this.swrepPpPaymentMode = swrepPpPaymentMode;
	}

	public String getSwrepPpPlanTerm() {
		return swrepPpPlanTerm;
	}

	public void setSwrepPpPlanTerm(String swrepPpPlanTerm) {
		this.swrepPpPlanTerm = swrepPpPlanTerm;
	}

	public String getSwrepPpPayTerm() {
		return swrepPpPayTerm;
	}

	public void setSwrepPpPayTerm(String swrepPpPayTerm) {
		this.swrepPpPayTerm = swrepPpPayTerm;
	}

	public float getSwrepPpPremium() {
		return swrepPpPremium;
	}

	public void setSwrepPpPremium(float swrepPpPremium) {
		this.swrepPpPremium = swrepPpPremium;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getSwrepPpPrin() {
		return swrepPpPrin;
	}

	public void setSwrepPpPrin(String swrepPpPrin) {
		this.swrepPpPrin = swrepPpPrin;
	}

	public String getSwrepPpPlan() {
		return swrepPpPlan;
	}

	public void setSwrepPpPlan(String swrepPpPlan) {
		this.swrepPpPlan = swrepPpPlan;
	}

	public String getSwrepPpBasRid() {
		return swrepPpBasRid;
	}

	public void setSwrepPpBasRid(String swrepPpBasRid) {
		this.swrepPpBasRid = swrepPpBasRid;
	}

	public String getSwrepPpOpt() {
		return swrepPpOpt;
	}

	public void setSwrepPpOpt(String swrepPpOpt) {
		this.swrepPpOpt = swrepPpOpt;
	}

	public String getSwrepPpName() {
		return swrepPpName;
	}

	public void setSwrepPpName(String swrepPpName) {
		this.swrepPpName = swrepPpName;
	}

	public float getProdRiskRate() {
		return prodRiskRate;
	}

	public void setProdRiskRate(float prodRiskRate) {
		this.prodRiskRate = prodRiskRate;
	}

}
