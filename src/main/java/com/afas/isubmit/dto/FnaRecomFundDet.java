package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = " FNA_RECOM_FUND_DET")
public class FnaRecomFundDet {
	@Id
	@Column(name = "RECOM_FF_ID", nullable = false)
	private String recomFfId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "RECOM_FF_PRODTYPE")
	private String recomFfProdType;

	@Column(name = "RECOM_FF_PRONAME")
	private String recomFfProName;

	@Column(name = "RECOM_FF_FUNDRISKRATE")
	private float recomFfFundRiskRate;

	@Column(name = "RECOM_FF_PAYMENTMODE")
	private String recomFfPaymentMode;

	@Column(name = "RECOM_FF_SALESCHRG")
	private float recomFfSaleChrg;

	@Column(name = "RECOM_FF_PURAMOUNT")
	private float recomFfPurAmount;

	@Column(name = "RECOM_FF_PURPRCNT")
	private float recomFfPurPrcnt;

	@Column(name = "RECOM_FF_IAFFEE")
	private String recomFfIaffee;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "RECOM_FF_PRIN")
	private String recomFfPrin;

	@Column(name = "RECOM_FF_PLAN")
	private String recomFfPlan;

	@Column(name = "RECOM_FF_BASRID")
	private String recomFfBasRid;

	@Column(name = "RECOM_FF_OPT")
	private String recomFfOpt;

	public String getRecomFfId() {
		return recomFfId;
	}

	public void setRecomFfId(String recomFfId) {
		this.recomFfId = recomFfId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getRecomFfProdType() {
		return recomFfProdType;
	}

	public void setRecomFfProdType(String recomFfProdType) {
		this.recomFfProdType = recomFfProdType;
	}

	public String getRecomFfProName() {
		return recomFfProName;
	}

	public void setRecomFfProName(String recomFfProName) {
		this.recomFfProName = recomFfProName;
	}

	public float getRecomFfFundRiskRate() {
		return recomFfFundRiskRate;
	}

	public void setRecomFfFundRiskRate(float recomFfFundRiskRate) {
		this.recomFfFundRiskRate = recomFfFundRiskRate;
	}

	public String getRecomFfPaymentMode() {
		return recomFfPaymentMode;
	}

	public void setRecomFfPaymentMode(String recomFfPaymentMode) {
		this.recomFfPaymentMode = recomFfPaymentMode;
	}

	public float getRecomFfSaleChrg() {
		return recomFfSaleChrg;
	}

	public void setRecomFfSaleChrg(float recomFfSaleChrg) {
		this.recomFfSaleChrg = recomFfSaleChrg;
	}

	public float getRecomFfPurAmount() {
		return recomFfPurAmount;
	}

	public void setRecomFfPurAmount(float recomFfPurAmount) {
		this.recomFfPurAmount = recomFfPurAmount;
	}

	public float getRecomFfPurPrcnt() {
		return recomFfPurPrcnt;
	}

	public void setRecomFfPurPrcnt(float recomFfPurPrcnt) {
		this.recomFfPurPrcnt = recomFfPurPrcnt;
	}

	public String getRecomFfIaffee() {
		return recomFfIaffee;
	}

	public void setRecomFfIaffee(String recomFfIaffee) {
		this.recomFfIaffee = recomFfIaffee;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRecomFfPrin() {
		return recomFfPrin;
	}

	public void setRecomFfPrin(String recomFfPrin) {
		this.recomFfPrin = recomFfPrin;
	}

	public String getRecomFfPlan() {
		return recomFfPlan;
	}

	public void setRecomFfPlan(String recomFfPlan) {
		this.recomFfPlan = recomFfPlan;
	}

	public String getRecomFfBasRid() {
		return recomFfBasRid;
	}

	public void setRecomFfBasRid(String recomFfBasRid) {
		this.recomFfBasRid = recomFfBasRid;
	}

	public String getRecomFfOpt() {
		return recomFfOpt;
	}

	public void setRecomFfOpt(String recomFfOpt) {
		this.recomFfOpt = recomFfOpt;
	}

}
