package com.afas.isubmit.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "master_attach_categ")
public class MasterAttachCategory {

	@Id
	@Column(name = "attach_categ_id")
	private String attacCategId;

	@Column(name = "attach_categ_desc")
	private String attachCategDesc;

	@Column(name = "attach_categ_name")
	private String attachCategName;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;

	@Column(name = "doc_title")
	private String docTitle;

	@Column(name = "module")
	private String module;

	public String getAttacCategId() {
		return attacCategId;
	}

	public void setAttacCategId(String attacCategId) {
		this.attacCategId = attacCategId;
	}

	public String getAttachCategDesc() {
		return attachCategDesc;
	}

	public void setAttachCategDesc(String attachCategDesc) {
		this.attachCategDesc = attachCategDesc;
	}

	public String getAttachCategName() {
		return attachCategName;
	}

	public void setAttachCategName(String attachCategName) {
		this.attachCategName = attachCategName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getDocTitle() {
		return docTitle;
	}

	public void setDocTitle(String docTitle) {
		this.docTitle = docTitle;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

}
