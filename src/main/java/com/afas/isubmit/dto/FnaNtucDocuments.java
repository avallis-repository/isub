package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//@Entity
//@Table(name = "FNA_NTUC_DOCUMENTS")
public class FnaNtucDocuments {

	@Id
	@Column(name = "ND_ID")
	private String ndId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "NTUC_CASE_ID")
	private String ntucCaseId;

	@Column(name = "DOC_DATA_KEY")
	private String docDataKey;

	@Column(name = "DOC_DESC")
//	@Type(type = "TEXT")
	private String docDesc;

	@Column(name = "DOC_SFTP_FILEPATH")
//	@Type(type = "TEXT")
	private String docSftpFilePath;

	@Column(name = "DOC_DECRYPT_KEY")
//	@Type(type = "TEXT")
	private String docDecryptKey;

	@Column(name = "DOC_FILETYPE")
	private String docFileType;

	@Column(name = "DOC_FILESIZE")
	private String docFileSize;

	@Column(name = "DOC_AVA_DOCTITILE")
//	@Type(type = "TEXT")
	private String docAvaDocTitile;

	@Column(name = "DOC_AVA_FILENAME")
	private String docAvaFileName;

	@Column(name = "DOC_AVA_FILETYPE")
	private String docAvaFileType;

	@Column(name = "DOC_AVA_FILESIZE")
	private String docAvaFileSize;

	@Column(name = "DOC_AVA_CATEGID")
	private String docAvaCategId;

	@Column(name = "DOC_CRTD_BY")
	private String docCreatedBy;

	@Column(name = "DOC_CRTD_DATE")
	private Date docCreatedDate;

	public String getNdId() {
		return ndId;
	}

	public void setNdId(String ndId) {
		this.ndId = ndId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getNtucCaseId() {
		return ntucCaseId;
	}

	public void setNtucCaseId(String ntucCaseId) {
		this.ntucCaseId = ntucCaseId;
	}

	public String getDocDataKey() {
		return docDataKey;
	}

	public void setDocDataKey(String docDataKey) {
		this.docDataKey = docDataKey;
	}

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public String getDocSftpFilePath() {
		return docSftpFilePath;
	}

	public void setDocSftpFilePath(String docSftpFilePath) {
		this.docSftpFilePath = docSftpFilePath;
	}

	public String getDocDecryptKey() {
		return docDecryptKey;
	}

	public void setDocDecryptKey(String docDecryptKey) {
		this.docDecryptKey = docDecryptKey;
	}

	public String getDocFileType() {
		return docFileType;
	}

	public void setDocFileType(String docFileType) {
		this.docFileType = docFileType;
	}

	public String getDocFileSize() {
		return docFileSize;
	}

	public void setDocFileSize(String docFileSize) {
		this.docFileSize = docFileSize;
	}

	public String getDocAvaDocTitile() {
		return docAvaDocTitile;
	}

	public void setDocAvaDocTitile(String docAvaDocTitile) {
		this.docAvaDocTitile = docAvaDocTitile;
	}

	public String getDocAvaFileName() {
		return docAvaFileName;
	}

	public void setDocAvaFileName(String docAvaFileName) {
		this.docAvaFileName = docAvaFileName;
	}

	public String getDocAvaFileType() {
		return docAvaFileType;
	}

	public void setDocAvaFileType(String docAvaFileType) {
		this.docAvaFileType = docAvaFileType;
	}

	public String getDocAvaFileSize() {
		return docAvaFileSize;
	}

	public void setDocAvaFileSize(String docAvaFileSize) {
		this.docAvaFileSize = docAvaFileSize;
	}

	public String getDocAvaCategId() {
		return docAvaCategId;
	}

	public void setDocAvaCategId(String docAvaCategId) {
		this.docAvaCategId = docAvaCategId;
	}

	public String getDocCreatedBy() {
		return docCreatedBy;
	}

	public void setDocCreatedBy(String docCreatedBy) {
		this.docCreatedBy = docCreatedBy;
	}

	public Date getDocCreatedDate() {
		return docCreatedDate;
	}

	public void setDocCreatedDate(Date docCreatedDate) {
		this.docCreatedDate = docCreatedDate;
	}

}
