package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "FNA_RECOM_PRDTPLAN_DET")
public class FnaRecomPrdtPlanDet {

	@Id
	@GenericGenerator(name = "recommDets", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "fna_recom_prdtplan_seq"),
			@Parameter(name = "sequence_prefix", value = "PRO"), })
	@GeneratedValue(generator = "recommDets", strategy = GenerationType.SEQUENCE)
	@Column(name = "RECOM_PP_ID", nullable = false)
	private String recomPpId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "RECOM_PP_PRODTYPE")
	private String recomPpProdtype;

	@Column(name = "RECOM_PP_PRODNAME")
	private String recomPpProdname;

	@Column(name = "RECOM_PP_SUMASSR")
	private float recomPpSumassr;

	@Column(name = "RECOM_PP_PAYMENTMODE")
	private String recomPpPaymentmode;

	@Column(name = "RECOM_PP_PLANTERM")
	private String recomPpPlanterm;

	@Column(name = "RECOM_PP_PAYTERM")
	private String recomPpPayterm;

	@Column(name = "RECOM_PP_PREMIUM")
	private float recomPpPremium;

	@Column(name = "RECOM_PP_PERSON")
	private String recomPpPerson;

	@Column(name = "RECOM_PP_PERSONNAME")
	private String recomPpPersonName;

	@Column(name = "RECOM_PP_CATEG")
	private String recomPpCateg;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "RECOM_PP_PRIN")
	private String recomPpPrin;

	@Column(name = "RECOM_PP_PLAN")
	private String recomPpPlan;

	@Column(name = "RECOM_PP_BASRID")
	private String recomPpBasrid;

	@Column(name = "RECOM_PP_OPT")
	private String recomPpOpt;

	@Column(name = "RECOM_PP_NAME")
	private String recomPpName;

	@Column(name = "CLIENT_OPTION")
	private String clientOption;

	@Column(name = "PROD_RISK_RATE")
	private float prodRiskRate;

	@Column(name = "RECOM_BASIC_REF")
	private String recomPpBasicRef;

	public String getRecomPpId() {
		return recomPpId;
	}

	public void setRecomPpId(String recomPpId) {
		this.recomPpId = recomPpId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getRecomPpProdtype() {
		return recomPpProdtype;
	}

	public void setRecomPpProdtype(String recomPpProdtype) {
		this.recomPpProdtype = recomPpProdtype;
	}

	public String getRecomPpProdname() {
		return recomPpProdname;
	}

	public void setRecomPpProdname(String recomPpProdname) {
		this.recomPpProdname = recomPpProdname;
	}

	public float getRecomPpSumassr() {
		return recomPpSumassr;
	}

	public void setRecomPpSumassr(float recomPpSumassr) {
		this.recomPpSumassr = recomPpSumassr;
	}

	public String getRecomPpPaymentmode() {
		return recomPpPaymentmode;
	}

	public void setRecomPpPaymentmode(String recomPpPaymentmode) {
		this.recomPpPaymentmode = recomPpPaymentmode;
	}

	public String getRecomPpPlanterm() {
		return recomPpPlanterm;
	}

	public void setRecomPpPlanterm(String recomPpPlanterm) {
		this.recomPpPlanterm = recomPpPlanterm;
	}

	public String getRecomPpPayterm() {
		return recomPpPayterm;
	}

	public void setRecomPpPayterm(String recomPpPayterm) {
		this.recomPpPayterm = recomPpPayterm;
	}

	public float getRecomPpPremium() {
		return recomPpPremium;
	}

	public void setRecomPpPremium(float recomPpPremium) {
		this.recomPpPremium = recomPpPremium;
	}

	public String getRecomPpPerson() {
		return recomPpPerson;
	}

	public void setRecomPpPerson(String recomPpPerson) {
		this.recomPpPerson = recomPpPerson;
	}

	public String getRecomPpPersonName() {
		return recomPpPersonName;
	}

	public void setRecomPpPersonName(String recomPpPersonName) {
		this.recomPpPersonName = recomPpPersonName;
	}

	public String getRecomPpCateg() {
		return recomPpCateg;
	}

	public void setRecomPpCateg(String recomPpCateg) {
		this.recomPpCateg = recomPpCateg;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getRecomPpPrin() {
		return recomPpPrin;
	}

	public void setRecomPpPrin(String recomPpPrin) {
		this.recomPpPrin = recomPpPrin;
	}

	public String getRecomPpPlan() {
		return recomPpPlan;
	}

	public void setRecomPpPlan(String recomPpPlan) {
		this.recomPpPlan = recomPpPlan;
	}

	public String getRecomPpBasrid() {
		return recomPpBasrid;
	}

	public void setRecomPpBasrid(String recomPpBasrid) {
		this.recomPpBasrid = recomPpBasrid;
	}

	public String getRecomPpOpt() {
		return recomPpOpt;
	}

	public void setRecomPpOpt(String recomPpOpt) {
		this.recomPpOpt = recomPpOpt;
	}

	public String getRecomPpName() {
		return recomPpName;
	}

	public void setRecomPpName(String recomPpName) {
		this.recomPpName = recomPpName;
	}

	public String getClientOption() {
		return clientOption;
	}

	public void setClientOption(String clientOption) {
		this.clientOption = clientOption;
	}

	public float getProdRiskRate() {
		return prodRiskRate;
	}

	public void setProdRiskRate(float prodRiskRate) {
		this.prodRiskRate = prodRiskRate;
	}

	public String getRecomPpBasicRef() {
		return recomPpBasicRef;
	}

	public void setRecomPpBasicRef(String recomPpBasicRef) {
		this.recomPpBasicRef = recomPpBasicRef;
	}

}
