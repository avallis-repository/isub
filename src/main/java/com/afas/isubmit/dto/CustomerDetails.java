package com.afas.isubmit.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "CUSTOMER_DETAILS")
public class CustomerDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "customerdetails", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "custid_sequence"),
			@Parameter(name = "sequence_prefix", value = "CUST"), })
	@GeneratedValue(generator = "customerdetails", strategy = GenerationType.SEQUENCE)
	@Column(name = "CUST_ID", nullable = false)
	private String custId;

	@Column(name = "CUST_CATEG", nullable = false)
	private String custCateg;

	@Column(name = "CUST_NAME")
	private String custName;

	@Column(name = "CUST_INITIALS", nullable = false)
	private String custInitials;

	@Column(name = "NRIC")
	private String nric;

	@Column(name = "NRIC_TYPE")
	private String nricType;

	@Column(name = "BDAY_CALL_OPTION")
	private String bdayCallOption;

	@Column(name = "BDAY_CALL_DAYS")
	private double bdayCallDays;

	@Column(name = "CUST_STATUS")
	private String custStatus;

	@Column(name = "AGENT_ID_INITIAL", nullable = false)
	private String agentIdInitial;

	@Column(name = "AGENT_ID_CURRENT", nullable = false)
	private String agentIdCurrent;

	@Column(name = "HOUSE_HLD_HEAD_FLG")
	private String houseHldHeadFlg;

	@Column(name = "COMPANY_NAME")
	private String companyName;

	@Column(name = "OCCPN_TITLE")
	private String occpnTitle;

	@Column(name = "OCCPN_DESC")
	private String occpnDesc;

	@Column(name = "ADDRESS_PREF")
	private String addressPref;

	@Column(name = "RES_ADDR1")
	private String resAddr1;

	@Column(name = "RES_ADDR2")
	private String resAddr2;

	@Column(name = "RES_ADDR3")
	private String resAddr3;

	@Column(name = "RES_CITY")
	private String resCity;

	@Column(name = "RES_STATE")
	private String resState;

	@Column(name = "RES_COUNTRY")
	private String resCountry;

	@Column(name = "RES_POSTALCODE")
	private String resPostalCode;

	@Column(name = "RES_FAX")
	private String resFax;

	@Column(name = "RES_PH")
	private String resPh;

	@Column(name = "RES_HAND_PHONE")
	private String resHandPhone;

	@Column(name = "OFF_ADDR1")
	private String offAddr1;

	@Column(name = "OFF_ADDR2")
	private String offAddr2;

	@Column(name = "OFF_ADDR3")
	private String offAddr3;

	@Column(name = "OFF_CITY")
	private String offCity;

	@Column(name = "OFF_STATE")
	private String offState;

	@Column(name = "OFF_COUNTRY")
	private String offCountry;

	@Column(name = "OFF_POSTALCODE")
	private String offPostalCode;

	@Column(name = "OFF_PH")
	private String offPh;

	@Column(name = "OFF_HAND_PHONE")
	private String offHandPhone;

	@Column(name = "OFF_FAX")
	private String offFax;

	@Column(name = "COR_ADDR1")
	private String corAddr1;

	@Column(name = "COR_ADDR2")
	private String corAddr2;

	@Column(name = "COR_ADDR3")
	private String corAddr3;

	@Column(name = "COR_CITY")
	private String corCity;

	@Column(name = "COR_STATE")
	private String corState;

	@Column(name = "COR_COUNTRY")
	private String corCountry;

	@Column(name = "COR_POSTALCODE")
	private String corPostalCode;

	@Column(name = "OTH_PH")
	private String othPh;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	@Column(name = "DOB")
	private Date dob;

	@Column(name = "MARITAL_STATUS")
	private String maritalStatus;

	@Column(name = "SEX")
	private String sex;

	@Column(name = "EMAIL_ID")
	private String emailId;

	@Column(name = "WEBSITE")
	private String website;

	@Column(name = "REFERRAL_FLG")
	private String referralFlg;

	@Column(name = "REFERRAL_SOURCE")
	private String referralSource;

	@Column(name = "REFERRAL_CUST_ID")
	private String referralCustId;

	@Column(name = "REFERRAL_CUST_NAME")
	private String referralCustName;

	@Column(name = "REFERRAL_CUST_NRIC")
	private String referralCustNric;

	@Column(name = "SMOKER_FLG")
	private String smokerFlg;

	@Column(name = "RACE")
	private String race;

	@Column(name = "NATIONALITY")
	private String nationality;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "WEIGHT")
	private String weight;

	@Column(name = "HEIGHT")
	private String height;

	@Column(name = "ACADEMIC_TITLE")
	private String academicTitle;

	@Column(name = "INCOME")
	private double income;

	@Column(name = "REMARKS")
	private String remarks;

//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;

//	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	@Column(name = "CREATED_BY", nullable = false)
	private String createdBy;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "STATUS_FLG")
	private String statusFlg;

	@Column(name = "DISTRIBUTOR_ID", nullable = false)
	private String distributorId;

	@Column(name = "DISTRIBUTOR_NAME")
	private String distributorName;

	@Column(name = "CUSTOMER_STATUS_ID", nullable = false)
	private String customerStatusId;

	@Column(name = "REFERRAL_ADV_ID")
	private String referralAdvId;

	@Column(name = "REFERRAL_ADV_NAME")
	private String referralAdvName;

	@Column(name = "REFERRAL_ADV_NRIC")
	private String referralAdvNric;

	@Column(name = "CONTACT_PREF")
	private String contactPref;

	@Column(name = "OTH_HAND_PHONE")
	private String othHandPhone;

	@Column(name = "OTH_FAX")
	private String othFax;

	@Column(name = "BLCK_LST_FLG")
	private String blckLstFlg;

	@Column(name = "GST_TYPE")
	private String gstType;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "ROC_DATE")
	private Date rocDate;

	@Column(name = "ROC_NO")
	private String rocNo;

	@Column(name = "CUST_CORE_ID")
	private String custCoreId;

	@Column(name = "CUST_FIN")
	private String custFin;

	@Column(name = "CUST_PASSPORT_NUM")
	private String custPassportNum;

	@Column(name = "TEMP_CUSTID_FPMS06_DM")
	private String tempCustIdFpmso6Dm;

	@Column(name = "RELIGION")
	private String religion;

	@Column(name = "SOURCE_DATA")
	private String sourceData;

	@Column(name = "DM_REMARKS")
	private String dmRemarks;

	@Column(name = "GI_CUST_FLG")
	private String giCustFlg;

	@Column(name = "TYPEOFPDT")
	private String typeOfPdt;

	@Column(name = "FNATYPE")
	private String fnaType;

	@Column(name = "RISK_LEVEL")
	private String riskLevel;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "RISK_START_DATE")
	private Date riskStartDate;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "RISK_END_DATE")
	private Date riskEndDate;

	@Column(name = "RISK_PRCNT")
	private double riskPrcnt;

	@Column(name = "RISK_REMARKS")
	private String riskRemarks;

	@Column(name = "DO_NOT_CALL")
	private String doNotCall;

	@Column(name = "DO_NOT_COMMUNICATE")
	private String doNotCommunicate;

	@Column(name = "BUSINESS_NATR")
	private String businessNatr;

	@Column(name = "BUSINESS_NATR_OTH")
	private String businessNatrDets;

	@Column(name = "CITIZENSHIP")
	private String citizenShip;

	@Column(name = "TYPES_OF_PROSPECT")
	private String typesOfProspect;

	@OneToMany(mappedBy = "cusDetails")
	@JsonIgnore
	private List<CustomerAttachments> attachmentList;

	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustCateg() {
		return custCateg;
	}

	public void setCustCateg(String custCateg) {
		this.custCateg = custCateg;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustInitials() {
		return custInitials;
	}

	public void setCustInitials(String custInitials) {
		this.custInitials = custInitials;
	}

	public String getNric() {
		return nric;
	}

	public void setNric(String nric) {
		this.nric = nric;
	}

	public String getBdayCallOption() {
		return bdayCallOption;
	}

	public void setBdayCallOption(String bdayCallOption) {
		this.bdayCallOption = bdayCallOption;
	}

	public double getBdayCallDays() {
		return bdayCallDays;
	}

	public void setBdayCallDays(double bdayCallDays) {
		this.bdayCallDays = bdayCallDays;
	}

	public String getCustStatus() {
		return custStatus;
	}

	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}

	public String getAgentIdInitial() {
		return agentIdInitial;
	}

	public void setAgentIdInitial(String agentIdInitial) {
		this.agentIdInitial = agentIdInitial;

	}

	public String getAgentIdCurrent() {
		return agentIdCurrent;
	}

	public void setAgentIdCurrent(String agentIdCurrent) {
		this.agentIdCurrent = agentIdCurrent;

	}

	public String getHouseHldHeadFlg() {
		return houseHldHeadFlg;
	}

	public void setHouseHldHeadFlg(String houseHldHeadFlg) {
		this.houseHldHeadFlg = houseHldHeadFlg;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getOccpnTitle() {
		return occpnTitle;
	}

	public void setOccpnTitle(String occpnTitle) {
		this.occpnTitle = occpnTitle;
	}

	public String getOccpnDesc() {
		return occpnDesc;
	}

	public void setOccpnDesc(String occpnDesc) {
		this.occpnDesc = occpnDesc;
	}

	public String getAddressPref() {
		return addressPref;
	}

	public void setAddressPref(String addressPref) {
		this.addressPref = addressPref;
	}

	public String getResAddr1() {
		return resAddr1;
	}

	public void setResAddr1(String resAddr1) {
		this.resAddr1 = resAddr1;
	}

	public String getResAddr2() {
		return resAddr2;
	}

	public void setResAddr2(String resAddr2) {
		this.resAddr2 = resAddr2;
	}

	public String getResAddr3() {
		return resAddr3;
	}

	public void setResAddr3(String resAddr3) {
		this.resAddr3 = resAddr3;
	}

	public String getResCity() {
		return resCity;
	}

	public void setResCity(String resCity) {
		this.resCity = resCity;
	}

	public String getResState() {
		return resState;
	}

	public void setResState(String resState) {
		this.resState = resState;
	}

	public String getResCountry() {
		return resCountry;
	}

	public void setResCountry(String resCountry) {
		this.resCountry = resCountry;
	}

	public String getResPostalCode() {
		return resPostalCode;
	}

	public void setResPostalCode(String resPostalCode) {
		this.resPostalCode = resPostalCode;
	}

	public String getResFax() {
		return resFax;
	}

	public void setResFax(String resFax) {
		this.resFax = resFax;
	}

	public String getResPh() {
		return resPh;
	}

	public void setResPh(String resPh) {
		this.resPh = resPh;
	}

	public String getResHandPhone() {
		return resHandPhone;
	}

	public void setResHandPhone(String resHandPhone) {
		this.resHandPhone = resHandPhone;
	}

	public String getOffAddr1() {
		return offAddr1;
	}

	public void setOffAddr1(String offAddr1) {
		this.offAddr1 = offAddr1;
	}

	public String getOffAddr2() {
		return offAddr2;
	}

	public void setOffAddr2(String offAddr2) {
		this.offAddr2 = offAddr2;
	}

	public String getOffAddr3() {
		return offAddr3;
	}

	public void setOffAddr3(String offAddr3) {
		this.offAddr3 = offAddr3;
	}

	public String getOffCity() {
		return offCity;
	}

	public void setOffCity(String offCity) {
		this.offCity = offCity;
	}

	public String getOffState() {
		return offState;
	}

	public void setOffState(String offState) {
		this.offState = offState;
	}

	public String getOffCountry() {
		return offCountry;
	}

	public void setOffCountry(String offCountry) {
		this.offCountry = offCountry;
	}

	public String getOffPostalCode() {
		return offPostalCode;
	}

	public void setOffPostalCode(String offPostalCode) {
		this.offPostalCode = offPostalCode;
	}

	public String getOffPh() {
		return offPh;
	}

	public void setOffPh(String offPh) {
		this.offPh = offPh;
	}

	public String getOffHandPhone() {
		return offHandPhone;
	}

	public void setOffHandPhone(String offHandPhone) {
		this.offHandPhone = offHandPhone;
	}

	public String getOffFax() {
		return offFax;
	}

	public void setOffFax(String offFax) {
		this.offFax = offFax;
	}

	public String getCorAddr1() {
		return corAddr1;
	}

	public void setCorAddr1(String corAddr1) {
		this.corAddr1 = corAddr1;
	}

	public String getCorAddr2() {
		return corAddr2;
	}

	public void setCorAddr2(String corAddr2) {
		this.corAddr2 = corAddr2;
	}

	public String getCorAddr3() {
		return corAddr3;
	}

	public void setCorAddr3(String corAddr3) {
		this.corAddr3 = corAddr3;
	}

	public String getCorCity() {
		return corCity;
	}

	public void setCorCity(String corCity) {
		this.corCity = corCity;
	}

	public String getCorState() {
		return corState;
	}

	public void setCorState(String corState) {
		this.corState = corState;
	}

	public String getCorCountry() {
		return corCountry;
	}

	public void setCorCountry(String corCountry) {
		this.corCountry = corCountry;
	}

	public String getCorPostalCode() {
		return corPostalCode;
	}

	public void setCorPostalCode(String corPostalCode) {
		this.corPostalCode = corPostalCode;
	}

	public String getOthPh() {
		return othPh;
	}

	public void setOthPh(String othPh) {
		this.othPh = othPh;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getReferralFlg() {
		return referralFlg;
	}

	public void setReferralFlg(String referralFlg) {
		this.referralFlg = referralFlg;
	}

	public String getReferralSource() {
		return referralSource;
	}

	public void setReferralSource(String referralSource) {
		this.referralSource = referralSource;
	}

	public String getReferralCustId() {
		return referralCustId;
	}

	public void setReferralCustId(String referralCustId) {
		this.referralCustId = referralCustId;
	}

	public String getReferralCustName() {
		return referralCustName;
	}

	public void setReferralCustName(String referralCustName) {
		this.referralCustName = referralCustName;
	}

	public String getReferralCustNric() {
		return referralCustNric;
	}

	public void setReferralCustNric(String referralCustNric) {
		this.referralCustNric = referralCustNric;
	}

	public String getSmokerFlg() {
		return smokerFlg;
	}

	public void setSmokerFlg(String smokerFlg) {
		this.smokerFlg = smokerFlg;
	}

	public String getRace() {
		return race;
	}

	public void setRace(String race) {
		this.race = race;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getHeight() {
		return height;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public String getAcademicTitle() {
		return academicTitle;
	}

	public void setAcademicTitle(String academicTitle) {
		this.academicTitle = academicTitle;
	}

	public double getIncome() {
		return income;
	}

	public void setIncome(double income) {
		this.income = income;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getStatusFlg() {
		return statusFlg;
	}

	public void setStatusFlg(String statusFlg) {
		this.statusFlg = statusFlg;
	}

	public String getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;

	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public String getCustomerStatusId() {
		return customerStatusId;
	}

	public void setCustomerStatusId(String customerStatusId) {
		this.customerStatusId = customerStatusId;
	}

	public String getReferralAdvId() {
		return referralAdvId;
	}

	public void setReferralAdvId(String referralAdvId) {
		this.referralAdvId = referralAdvId;
	}

	public String getReferralAdvName() {
		return referralAdvName;
	}

	public void setReferralAdvName(String referralAdvName) {
		this.referralAdvName = referralAdvName;
	}

	public String getReferralAdvNric() {
		return referralAdvNric;
	}

	public void setReferralAdvNric(String referralAdvNric) {
		this.referralAdvNric = referralAdvNric;
	}

	public String getContactPref() {
		return contactPref;
	}

	public void setContactPref(String contactPref) {
		this.contactPref = contactPref;
	}

	public String getOthHandPhone() {
		return othHandPhone;
	}

	public void setOthHandPhone(String othHandPhone) {
		this.othHandPhone = othHandPhone;
	}

	public String getOthFax() {
		return othFax;
	}

	public void setOthFax(String othFax) {
		this.othFax = othFax;
	}

	public String getBlckLstFlg() {
		return blckLstFlg;
	}

	public void setBlckLstFlg(String blckLstFlg) {
		this.blckLstFlg = blckLstFlg;
	}

	public String getGstType() {
		return gstType;
	}

	public void setGstType(String gstType) {
		this.gstType = gstType;
	}

	public Date getRocDate() {
		return rocDate;
	}

	public void setRocDate(Date rocDate) {
		this.rocDate = rocDate;
	}

	public String getRocNo() {
		return rocNo;
	}

	public void setRocNo(String rocNo) {
		this.rocNo = rocNo;
	}

	public String getCustCoreId() {
		return custCoreId;
	}

	public void setCustCoreId(String custCoreId) {
		this.custCoreId = custCoreId;
	}

	public String getCustFin() {
		return custFin;
	}

	public void setCustFin(String custFin) {
		this.custFin = custFin;
	}

	public String getCustPassportNum() {
		return custPassportNum;
	}

	public void setCustPassportNum(String custPassportNum) {
		this.custPassportNum = custPassportNum;
	}

	public String getTempCustIdFpmso6Dm() {
		return tempCustIdFpmso6Dm;
	}

	public void setTempCustIdFpmso6Dm(String tempCustIdFpmso6Dm) {
		this.tempCustIdFpmso6Dm = tempCustIdFpmso6Dm;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getSourceData() {
		return sourceData;
	}

	public void setSourceData(String sourceData) {
		this.sourceData = sourceData;
	}

	public String getDmRemarks() {
		return dmRemarks;
	}

	public void setDmRemarks(String dmRemarks) {
		this.dmRemarks = dmRemarks;
	}

	public String getGiCustFlg() {
		return giCustFlg;
	}

	public void setGiCustFlg(String giCustFlg) {
		this.giCustFlg = giCustFlg;
	}

	public String getTypeOfPdt() {
		return typeOfPdt;
	}

	public void setTypeOfPdt(String typeOfPdt) {
		this.typeOfPdt = typeOfPdt;
	}

	public String getFnaType() {
		return fnaType;
	}

	public void setFnaType(String fnaType) {
		this.fnaType = fnaType;
	}

	public String getRiskLevel() {
		return riskLevel;
	}

	public void setRiskLevel(String riskLevel) {
		this.riskLevel = riskLevel;
	}

	public Date getRiskStartDate() {
		return riskStartDate;
	}

	public void setRiskStartDate(Date riskStartDate) {
		this.riskStartDate = riskStartDate;
	}

	public Date getRiskEndDate() {
		return riskEndDate;
	}

	public void setRiskEndDate(Date riskEndDate) {
		this.riskEndDate = riskEndDate;
	}

	public double getRiskPrcnt() {
		return riskPrcnt;
	}

	public void setRiskPrcnt(double riskPrcnt) {
		this.riskPrcnt = riskPrcnt;
	}

	public String getRiskRemarks() {
		return riskRemarks;
	}

	public void setRiskRemarks(String riskRemarks) {
		this.riskRemarks = riskRemarks;
	}

	public String getDoNotCall() {
		return doNotCall;
	}

	public void setDoNotCall(String doNotCall) {
		this.doNotCall = doNotCall;
	}

	public String getDoNotCommunicate() {
		return doNotCommunicate;
	}

	public void setDoNotCommunicate(String doNotCommunicate) {
		this.doNotCommunicate = doNotCommunicate;
	}

	public String getBusinessNatr() {
		return businessNatr;
	}

	public void setBusinessNatr(String businessNatr) {
		this.businessNatr = businessNatr;
	}

	public String getBusinessNatrDets() {
		return businessNatrDets;
	}

	public void setBusinessNatrDets(String businessNatrDets) {
		this.businessNatrDets = businessNatrDets;
	}

	public String getCitizenShip() {
		return citizenShip;
	}

	public void setCitizenShip(String citizenShip) {
		this.citizenShip = citizenShip;
	}

	public String getTypesOfProspect() {
		return typesOfProspect;
	}

	public void setTypesOfProspect(String typesOfProspect) {
		this.typesOfProspect = typesOfProspect;
	}

	public List<CustomerAttachments> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<CustomerAttachments> attachmentList) {
		this.attachmentList = attachmentList;
	}

	public String getNricType() {
		return nricType;
	}

	public void setNricType(String nricType) {
		this.nricType = nricType;
	}

}
