package com.afas.isubmit.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "master_customer_status")
public class MasterCustomerStatus {

	@Id
	@Column(name = "customer_status_id")
	private String customerStatusId;

	@Column(name = "customer_status_desc")
	private String customerStatusDesc;

	@Column(name = "customer_status_name")
	private String customerStatusName;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modified_date")
	private Date modifiedDate;

	public String getCustomerStatusId() {
		return customerStatusId;
	}

	public void setCustomerStatusId(String customerStatusId) {
		this.customerStatusId = customerStatusId;
	}

	public String getCustomerStatusDesc() {
		return customerStatusDesc;
	}

	public void setCustomerStatusDesc(String customerStatusDesc) {
		this.customerStatusDesc = customerStatusDesc;
	}

	public String getCustomerStatusName() {
		return customerStatusName;
	}

	public void setCustomerStatusName(String customerStatusName) {
		this.customerStatusName = customerStatusName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}
