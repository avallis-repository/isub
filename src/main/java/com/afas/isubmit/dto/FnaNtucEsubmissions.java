package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//@Entity
//@Table(name = "FNA_NTUC_ESUBMISSIONS")
public class FnaNtucEsubmissions {
	@Id
	@Column(name = "ESUBID", nullable = false)
	private String esubId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "ADVSTF_CODE")
	private String advStfCode;

	@Column(name = "ACCESS_TOKEN")
	private String accessToken;

	@Column(name = "E_SUMBIT_DT")
	private String eSubmitDt;

	@Column(name = "EXPIRES_SECONDS")
	private String expriesSeconds;

	@Column(name = "CASE_ID")
	private String caseId;

	@Column(name = "CASE_NO")
	private String caseNo;

	@Column(name = "DSF_SERVER_DT")
	private String dsfServerDt;

	@Column(name = "SAML_RESP_ID")
	private String samlRespId;

	@Column(name = "SAML_ASSR_ID")
	private String samlAssrId;

	@Column(name = "SAML_NOTBEFORE")
	private String samlNotBefore;

	@Column(name = "SAML_NOTAFTER")
	private String samlNotAfter;

	@Column(name = "SAML_AUTHINSTANT")
	private String samlAuthInstant;

	@Column(name = "AUTHNQRY_ID")
	private String authNqryId;

	@Column(name = "AUTHN_ISSUEINSTANT")
	private String authnIssueInstant;

	@Column(name = "IDPRESP_RESP_ID")
	private String idPrespRespId;

	@Column(name = "IDPRESP_INRESPONSETO")
	private String idPrespInresponseEto;

	@Column(name = "IDPRESP_INSTANT")
	private String idPrespInstant;

	@Column(name = "IDPRESP_ASSR_ID")
	private String idPrespAssrId;

	@Column(name = "IDPRESP_NOTBEFORE")
	private String idPrespNotBefore;

	@Column(name = "IDPRESP_NOTAFTER")
	private String idPrespNotAfter;

	@Column(name = "IDPRESP_AUTHN_INSTANT")
	private String idPrespAuthnInstant;

	@Column(name = "ESUB_CRTD_BY")
	private String esubCreatedBy;

	@Column(name = "ESUB_CRTD_DATE")
	private Date esubCrtdDate;

	@Column(name = "IDPRESP_STATUS")
	private String idPrespStatus;

	public String getEsubId() {
		return esubId;
	}

	public void setEsubId(String esubId) {
		this.esubId = esubId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getAdvStfCode() {
		return advStfCode;
	}

	public void setAdvStfCode(String advStfCode) {
		this.advStfCode = advStfCode;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String geteSubmitDt() {
		return eSubmitDt;
	}

	public void seteSubmitDt(String eSubmitDt) {
		this.eSubmitDt = eSubmitDt;
	}

	public String getExpriesSeconds() {
		return expriesSeconds;
	}

	public void setExpriesSeconds(String expriesSeconds) {
		this.expriesSeconds = expriesSeconds;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getCaseNo() {
		return caseNo;
	}

	public void setCaseNo(String caseNo) {
		this.caseNo = caseNo;
	}

	public String getDsfServerDt() {
		return dsfServerDt;
	}

	public void setDsfServerDt(String dsfServerDt) {
		this.dsfServerDt = dsfServerDt;
	}

	public String getSamlRespId() {
		return samlRespId;
	}

	public void setSamlRespId(String samlRespId) {
		this.samlRespId = samlRespId;
	}

	public String getSamlAssrId() {
		return samlAssrId;
	}

	public void setSamlAssrId(String samlAssrId) {
		this.samlAssrId = samlAssrId;
	}

	public String getSamlNotBefore() {
		return samlNotBefore;
	}

	public void setSamlNotBefore(String samlNotBefore) {
		this.samlNotBefore = samlNotBefore;
	}

	public String getSamlNotAfter() {
		return samlNotAfter;
	}

	public void setSamlNotAfter(String samlNotAfter) {
		this.samlNotAfter = samlNotAfter;
	}

	public String getSamlAuthInstant() {
		return samlAuthInstant;
	}

	public void setSamlAuthInstant(String samlAuthInstant) {
		this.samlAuthInstant = samlAuthInstant;
	}

	public String getAuthNqryId() {
		return authNqryId;
	}

	public void setAuthNqryId(String authNqryId) {
		this.authNqryId = authNqryId;
	}

	public String getAuthnIssueInstant() {
		return authnIssueInstant;
	}

	public void setAuthnIssueInstant(String authnIssueInstant) {
		this.authnIssueInstant = authnIssueInstant;
	}

	public String getIdPrespRespId() {
		return idPrespRespId;
	}

	public void setIdPrespRespId(String idPrespRespId) {
		this.idPrespRespId = idPrespRespId;
	}

	public String getIdPrespInresponseEto() {
		return idPrespInresponseEto;
	}

	public void setIdPrespInresponseEto(String idPrespInresponseEto) {
		this.idPrespInresponseEto = idPrespInresponseEto;
	}

	public String getIdPrespInstant() {
		return idPrespInstant;
	}

	public void setIdPrespInstant(String idPrespInstant) {
		this.idPrespInstant = idPrespInstant;
	}

	public String getIdPrespAssrId() {
		return idPrespAssrId;
	}

	public void setIdPrespAssrId(String idPrespAssrId) {
		this.idPrespAssrId = idPrespAssrId;
	}

	public String getIdPrespNotBefore() {
		return idPrespNotBefore;
	}

	public void setIdPrespNotBefore(String idPrespNotBefore) {
		this.idPrespNotBefore = idPrespNotBefore;
	}

	public String getIdPrespNotAfter() {
		return idPrespNotAfter;
	}

	public void setIdPrespNotAfter(String idPrespNotAfter) {
		this.idPrespNotAfter = idPrespNotAfter;
	}

	public String getIdPrespAuthnInstant() {
		return idPrespAuthnInstant;
	}

	public void setIdPrespAuthnInstant(String idPrespAuthnInstant) {
		this.idPrespAuthnInstant = idPrespAuthnInstant;
	}

	public String getEsubCreatedBy() {
		return esubCreatedBy;
	}

	public void setEsubCreatedBy(String esubCreatedBy) {
		this.esubCreatedBy = esubCreatedBy;
	}

	public Date getEsubCrtdDate() {
		return esubCrtdDate;
	}

	public void setEsubCrtdDate(Date esubCrtdDate) {
		this.esubCrtdDate = esubCrtdDate;
	}

	public String getIdPrespStatus() {
		return idPrespStatus;
	}

	public void setIdPrespStatus(String idPrespStatus) {
		this.idPrespStatus = idPrespStatus;
	}

}
