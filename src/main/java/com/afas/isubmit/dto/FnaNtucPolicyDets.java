package com.afas.isubmit.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "FNA_NTUC_POLICYDETS")
public class FnaNtucPolicyDets {

	@Id
	@GenericGenerator(name = "ntucPolicyDetails", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + ".fna_ntuc_policy_seq"),
			@Parameter(name = "sequence_prefix", value = "POL") })
	@GeneratedValue(generator = "ntucPolicyDetails", strategy = GenerationType.SEQUENCE)
	@Column(name = "NP_ID")
	private String npId;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;
	
	@Column(name = "NTUC_CASE_ID")
	private String ntucCaseId;
	
	@Column(name = "POLICY_NO")
	private String policyNo;
	
	@Column(name = "PRODUCT_ID")
	private String productId;
	
	@Column(name = "CREATED_BY")
	private String createdBy;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "MODIFIED_BY")
	private String modifiedBy;
	
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	public String getNpId() {
		return npId;
	}

	public void setNpId(String npId) {
		this.npId = npId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getNtucCaseId() {
		return ntucCaseId;
	}

	public void setNtucCaseId(String ntucCaseId) {
		this.ntucCaseId = ntucCaseId;
	}

	public String getPolicyNo() {
		return policyNo;
	}

	public void setPolicyNo(String policyNo) {
		this.policyNo = policyNo;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
}
