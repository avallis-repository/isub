package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FNA_MASTER_CHKLIST")
public class FnaMasterChkList {

	@Id
	@Column(name = "CL_ID", nullable = false)
	private String clId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "CHKLIST_GROUP", nullable = false)
	private String chkListGroup;

	@Column(name = "CHKLIST_DESC", nullable = false)
//	@Type(type = "TEXT")
	private String chkListDesc;

	@Column(name = "CHKLIST_SUBITEMS", nullable = false)
//	@Type(type = "TEXT")
	private String chkListSubItems;

	@Column(name = "CHKLIST_STATUS")
	private String chkListStatus;

	@Column(name = "CHKLIST_CRTD_BY", nullable = false)
	private String chkListCreatedBy;

	@Column(name = "CHKLIST_CRTD_DATE", nullable = false)
	private Date chkListCreatedDate;

	public String getClId() {
		return clId;
	}

	public void setClId(String clId) {
		this.clId = clId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getChkListGroup() {
		return chkListGroup;
	}

	public void setChkListGroup(String chkListGroup) {
		this.chkListGroup = chkListGroup;
	}

	public String getChkListDesc() {
		return chkListDesc;
	}

	public void setChkListDesc(String chkListDesc) {
		this.chkListDesc = chkListDesc;
	}

	public String getChkListSubItems() {
		return chkListSubItems;
	}

	public void setChkListSubItems(String chkListSubItems) {
		this.chkListSubItems = chkListSubItems;
	}

	public String getChkListStatus() {
		return chkListStatus;
	}

	public void setChkListStatus(String chkListStatus) {
		this.chkListStatus = chkListStatus;
	}

	public String getChkListCreatedBy() {
		return chkListCreatedBy;
	}

	public void setChkListCreatedBy(String chkListCreatedBy) {
		this.chkListCreatedBy = chkListCreatedBy;
	}

	public Date getChkListCreatedDate() {
		return chkListCreatedDate;
	}

	public void setChkListCreatedDate(Date chkListCreatedDate) {
		this.chkListCreatedDate = chkListCreatedDate;
	}

}
