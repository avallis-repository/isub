package com.afas.isubmit.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FNA_NTUC_DOCS")
public class FnaNtucDocs {
	
	@Id
	@Column(name = "ND_ID")
	private String ndId;
	
	@Column(name = "NTUC_CASE_ID")
	private String ntucCaseId;
	
	@Column(name = "DOC_DATA_KEY")
	private String docDataKey;
	
	@Column(name = "DOC_DESC")
	private String docDesc;
	
	@Column(name = "DOC_FILE_TYPE")
	private String docFileType;
	
	@Column(name = "DOC_FILE_PATH")
	private String docFilePath;
	
	@Column(name = "DOC_FILE_SIZE")
	private String docFileSize;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;
	
	@Column(name = "DOC_FILE_NAME")
	private String docFileName;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	public String getNdId() {
		return ndId;
	}

	public void setNdId(String ndId) {
		this.ndId = ndId;
	}

	public String getNtucCaseId() {
		return ntucCaseId;
	}

	public void setNtucCaseId(String ntucCaseId) {
		this.ntucCaseId = ntucCaseId;
	}

	public String getDocDataKey() {
		return docDataKey;
	}

	public void setDocDataKey(String docDataKey) {
		this.docDataKey = docDataKey;
	}

	public String getDocDesc() {
		return docDesc;
	}

	public void setDocDesc(String docDesc) {
		this.docDesc = docDesc;
	}

	public String getDocFileType() {
		return docFileType;
	}

	public void setDocFileType(String docFileType) {
		this.docFileType = docFileType;
	}

	public String getDocFilePath() {
		return docFilePath;
	}

	public void setDocFilePath(String docFilePath) {
		this.docFilePath = docFilePath;
	}

	public String getDocFileSize() {
		return docFileSize;
	}

	public void setDocFileSize(String docFileSize) {
		this.docFileSize = docFileSize;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getDocFileName() {
		return docFileName;
	}

	public void setDocFileName(String docFileName) {
		this.docFileName = docFileName;
	}
	
}
