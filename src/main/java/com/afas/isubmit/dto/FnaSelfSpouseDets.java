package com.afas.isubmit.dto;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "FNA_SELFSPOUSE_DETS")
public class FnaSelfSpouseDets {

	@Id
	@GenericGenerator(name = "fnaselfspousedets", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "dataformid_sequence"),
			@Parameter(name = "sequence_prefix", value = "DAT"), })
	@GeneratedValue(generator = "fnaselfspousedets", strategy = GenerationType.SEQUENCE)
	@Column(name = "DATAFORM_ID", nullable = false)
	private String dataFormId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "DF_SELF_NAME")
	private String dfSelfName;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "DF_SELF_DOB")
	private Date dfSelfDob;

	@Column(name = "DF_SELF_NATIONALITY")
	private String dfSelfNationality;

	@Column(name = "DF_SELF_AGE")
	private String dfSelfAge;

	@Column(name = "DF_SELF_NRIC")
	private String dfSelfNric;

	@Column(name = "DF_SELF_MARTSTS")
	private String dfSelfMartsts;

	@Column(name = "DF_SELF_OCCPN")
	private String dfSelfOccpn;

	@Column(name = "DF_SELF_GENDER")
	private String dfSelfGender;

	@Column(name = "DF_SELF_EMPSTS")
	private String dfSelfEmpsts;

	@Column(name = "DF_SELF_SMOKING")
	private String dfSelfSmoking;

	@Column(name = "DF_SELF_HOMEADDR")
	private String dfSelfHomeAddr;

	@Column(name = "DF_SELF_OFFADDR")
	private String dfSelfOffAddr;

	@Column(name = "DF_SELF_PERSEMAIL")
	private String dfSelfPersEmail;

	@Column(name = "DF_SELF_OFFEMAIL")
	private String dfSelfOffEmail;

	@Column(name = "DF_SELF_MOBILE")
	private String dfSelfMobile;

	@Column(name = "DF_SELF_HOME")
	private String dfSelfHome;

	@Column(name = "DF_SELF_OFFICE")
	private String dfSelfOffice;

	@Column(name = "DF_SELF_FAX")
	private String dfSelfFax;

	@Column(name = "DF_SPS_NAME")
	private String dfSpsName;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "DF_SPS_DOB")
	private Date dfSpsDob;

	@Column(name = "DF_SPS_NATIONALITY")
	private String dfSpsNationality;

	@Column(name = "DF_SPS_AGE")
	private String dfSpsAge;

	@Column(name = "DF_SPS_NRIC")
	private String dfSpsNric;

	@Column(name = "DF_SPS_MARTSTS")
	private String dfSpsMartsts;

	@Column(name = "DF_SPS_OCCPN")
	private String dfSpsOccpn;

	@Column(name = "DF_SPS_GENDER")
	private String dfSpsGender;

	@Column(name = "DF_SPS_EMPSTS")
	private String dfSpsEmpsts;

	@Column(name = "DF_SPS_SMOKING")
	private String dfSpsSmoking;

	@Column(name = "DF_SPS_PERSEMAIL")
	private String dfSpsPersEmail;

	@Column(name = "DF_SPS_HP")
	private String dfSpsHp;

	@Column(name = "DF_SPS_OFFICE")
	private String dfSpsOffice;

	@Column(name = "DF_FINGOALS")
	private String dfFingoals;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", timezone = "Asia/Singapore")
	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "DF_SPS_HOME")
	private String dfSpsHome;

	@Column(name = "DF_SPS_HOMEADDR")
	private String dfSpsHomeAddr;

	@Column(name = "DF_SELF_COMPNAME")
	private String dfSelfCompname;

	@Column(name = "DF_SPS_COMPNAME")
	private String dfSpsCompname;

	@Column(name = "DF_SELF_BUSINATR")
	private String dfSelfBusinatr;

	@Column(name = "DF_SPS_BUSINATR")
	private String dfSpsBusinatr;

	@Column(name = "DF_SELF_BUSINATR_DETS")
	private String dfSelfBusinatrDets;

	@Column(name = "DF_SPS_BUSINATR_DETS")
	private String dfSpsBusinatrDets;

	@Column(name = "DF_SELF_USNOTIFYFLG")
	private String dfSelfUsnotifyflg;

	@Column(name = "DF_SPS_USNOTIFYFLG")
	private String dfSpsUsnotifyflg;

	@Column(name = "DF_SELF_USPERSONFLG")
	private String dfSelfUspersonflg;

	@Column(name = "DF_SPS_USPERSONFLG")
	private String dfSpsUspersonflg;

	@Column(name = "DF_SELF_USTAXNO")
	private String dfSelfUstaxno;

	@Column(name = "DF_SPS_USTAXNO")
	private String dfSpsUstaxno;

	@Column(name = "DF_SELF_RESCOUNTRY")
	private String dfSelfResCountry;

	@Column(name = "DF_SPS_RESCOUNTRY")
	private String dfSpsResCountry;

	@Column(name = "RES_ADDR1")
	private String resAddr1;

	@Column(name = "RES_ADDR2")
	private String resAddr2;

	@Column(name = "RES_ADDR3")
	private String resAddr3;

	@Column(name = "RES_CITY")
	private String resCity;

	@Column(name = "RES_STATE")
	private String resState;

	@Column(name = "RES_COUNTRY")

	private String resCountry;

	@Column(name = "RES_POSTALCODE")

	private String resPostalCode;

	@Column(name = "DF_SELF_AGENEXT")

	private String dfSelfAgeNext;

	@Column(name = "DF_SPS_AGENEXT")

	private String dfSpsAgeNext;

	@Column(name = "DF_SELF_NATY_DETS")

	private String dfSelfNatyDets;

	@Column(name = "DF_SPS_NATY_DETS")

	private String dfSpsNatyDets;

	@Column(name = "DF_SELF_ENG_SPOKEN")
	private String dfSelfEngSpoken;

	@Column(name = "DF_SELF_ENG_WRITTEN")
	private String dfSelfEngWritten;

	@Column(name = "DF_SPS_ENG_SPOKEN")
	private String dfSpsEngSpoken;

	@Column(name = "DF_SPS_ENG_WRITTEN")
	private String dfSpsEngWritten;

	@Column(name = "DF_SELF_EDULEVEL")

	private String dfSelfEduLevel;

	@Column(name = "DF_SPS_EDULEVEL")

	private String dfSpsEduLevel;

	@Column(name = "DF_SELF_REPL_EXISTPOL")
	private String dfSelfReplExistPol;

	@Column(name = "DF_SPS_REPL_EXISTPOL")
	private String dfSpsReplExistPol;

	@Column(name = "DF_SELF_BIRTHCNTRY")

	private String dfSelfBirthCntry;

	@Column(name = "DF_SPS_BIRTHCNTRY")

	private String dfSpsBirthCntry;

	@Column(name = "DF_SELF_ONECONTFLG")

	private String dfSelfOneContFlg;

	@Column(name = "DF_SELF_SAMEASREGADD")

	private String dfSelfSameAsregadd;

	@Column(name = "DF_SELF_MAILADDRFLG")

	private String dfSelfMailAddrFlg;

	@Column(name = "DF_SPS_MAILADDRFLG")

	private String dfSpsMailAddrFlg;

	@Column(name = "DF_SELF_MAILADDR")

	private String dfSelfMailAddr;

	@Column(name = "DF_SPS_MAILADDR")

	private String dfSpsMailAddr;

	@Column(name = "DF_SELF_ANNLINCOME")
	private String dfSelfAnnlIncome;

	@Column(name = "DF_SPS_ANNLINCOME")
	private String dfSpsAnnlIncome;

	@Column(name = "DF_SELF_FUNDSRC")
	private String dfSelfFundsrc;

	@Column(name = "DF_SPS_FUNDSRC")
	private String dfSpsFundsrc;

	@Column(name = "DF_SELF_FUNDSRC_DETS")
	private String dfSelfFundsrcDets;

	@Column(name = "DF_SPS_FUNDSRC_DETS")
	private String dfSpsFundsrcDets;

	@Column(name = "DF_SELF_HEIGHT")
	private String dfSelfHeight;

	@Column(name = "DF_SPS_HEIGHT")
	private String dfSpsHeight;

	@Column(name = "DF_SELF_WEIGHT")
	private String dfSelfWeight;

	@Column(name = "DF_SPS_WEIGHT")
	private String dfSpsWeight;

	@Column(name = "DF_SELF_TAXRES")
	private String dfSelfTaxres;

	@Column(name = "DF_SELF_TAXRES_OTH")
	private String dfSelfTaxresOth;

	@Column(name = "DF_SPS_TAXRES")
	private String dfSpsTaxres;

	@Column(name = "DF_SPS_TAXRES_OTH")
	private String dfSpsTaxresOth;

	@OneToMany(mappedBy = "fnaDependant", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	private List<FnaDependantDets> fnaDependantList;

	public String getDataFormId() {
		return dataFormId;
	}

	public void setDataFormId(String dataFormId) {
		this.dataFormId = dataFormId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getDfSelfName() {
		return dfSelfName;
	}

	public void setDfSelfName(String dfSelfName) {
		this.dfSelfName = dfSelfName;
	}

	public Date getDfSelfDob() {
		return dfSelfDob;
	}

	public void setDfSelfDob(Date dfSelfDob) {
		this.dfSelfDob = dfSelfDob;
	}

	public String getDfSelfNationality() {
		return dfSelfNationality;
	}

	public void setDfSelfNationality(String dfSelfNationality) {
		this.dfSelfNationality = dfSelfNationality;
	}

	public String getDfSelfAge() {
		return dfSelfAge;
	}

	public void setDfSelfAge(String dfSelfAge) {
		this.dfSelfAge = dfSelfAge;
	}

	public String getDfSelfNric() {
		return dfSelfNric;
	}

	public void setDfSelfNric(String dfSelfNric) {
		this.dfSelfNric = dfSelfNric;
	}

	public String getDfSelfMartsts() {
		return dfSelfMartsts;
	}

	public void setDfSelfMartsts(String dfSelfMartsts) {
		this.dfSelfMartsts = dfSelfMartsts;
	}

	public String getDfSelfOccpn() {
		return dfSelfOccpn;
	}

	public void setDfSelfOccpn(String dfSelfOccpn) {
		this.dfSelfOccpn = dfSelfOccpn;
	}

	public String getDfSelfGender() {
		return dfSelfGender;
	}

	public void setDfSelfGender(String dfSelfGender) {
		this.dfSelfGender = dfSelfGender;
	}

	public String getDfSelfEmpsts() {
		return dfSelfEmpsts;
	}

	public void setDfSelfEmpsts(String dfSelfEmpsts) {
		this.dfSelfEmpsts = dfSelfEmpsts;
	}

	public String getDfSelfSmoking() {
		return dfSelfSmoking;
	}

	public void setDfSelfSmoking(String dfSelfSmoking) {
		this.dfSelfSmoking = dfSelfSmoking;
	}

	public String getDfSelfHomeAddr() {
		return dfSelfHomeAddr;
	}

	public void setDfSelfHomeAddr(String dfSelfHomeAddr) {
		this.dfSelfHomeAddr = dfSelfHomeAddr;
	}

	public String getDfSelfOffAddr() {
		return dfSelfOffAddr;
	}

	public void setDfSelfOffAddr(String dfSelfOffAddr) {
		this.dfSelfOffAddr = dfSelfOffAddr;
	}

	public String getDfSelfPersEmail() {
		return dfSelfPersEmail;
	}

	public void setDfSelfPersEmail(String dfSelfPersEmail) {
		this.dfSelfPersEmail = dfSelfPersEmail;
	}

	public String getDfSelfOffEmail() {
		return dfSelfOffEmail;
	}

	public void setDfSelfOffEmail(String dfSelfOffEmail) {
		this.dfSelfOffEmail = dfSelfOffEmail;
	}

	public String getDfSelfMobile() {
		return dfSelfMobile;
	}

	public void setDfSelfMobile(String dfSelfMobile) {
		this.dfSelfMobile = dfSelfMobile;
	}

	public String getDfSelfHome() {
		return dfSelfHome;
	}

	public void setDfSelfHome(String dfSelfHome) {
		this.dfSelfHome = dfSelfHome;
	}

	public String getDfSelfOffice() {
		return dfSelfOffice;
	}

	public void setDfSelfOffice(String dfSelfOffice) {
		this.dfSelfOffice = dfSelfOffice;
	}

	public String getDfSelfFax() {
		return dfSelfFax;
	}

	public void setDfSelfFax(String dfSelfFax) {
		this.dfSelfFax = dfSelfFax;
	}

	public String getDfSpsName() {
		return dfSpsName;
	}

	public void setDfSpsName(String dfSpsName) {
		this.dfSpsName = dfSpsName;
	}

	public Date getDfSpsDob() {
		return dfSpsDob;
	}

	public void setDfSpsDob(Date dfSpsDob) {
		this.dfSpsDob = dfSpsDob;
	}

	public String getDfSpsNationality() {
		return dfSpsNationality;
	}

	public void setDfSpsNationality(String dfSpsNationality) {
		this.dfSpsNationality = dfSpsNationality;
	}

	public String getDfSpsAge() {
		return dfSpsAge;
	}

	public void setDfSpsAge(String dfSpsAge) {
		this.dfSpsAge = dfSpsAge;
	}

	public String getDfSpsNric() {
		return dfSpsNric;
	}

	public void setDfSpsNric(String dfSpsNric) {
		this.dfSpsNric = dfSpsNric;
	}

	public String getDfSpsMartsts() {
		return dfSpsMartsts;
	}

	public void setDfSpsMartsts(String dfSpsMartsts) {
		this.dfSpsMartsts = dfSpsMartsts;
	}

	public String getDfSpsOccpn() {
		return dfSpsOccpn;
	}

	public void setDfSpsOccpn(String dfSpsOccpn) {
		this.dfSpsOccpn = dfSpsOccpn;
	}

	public String getDfSpsGender() {
		return dfSpsGender;
	}

	public void setDfSpsGender(String dfSpsGender) {
		this.dfSpsGender = dfSpsGender;
	}

	public String getDfSpsEmpsts() {
		return dfSpsEmpsts;
	}

	public void setDfSpsEmpsts(String dfSpsEmpsts) {
		this.dfSpsEmpsts = dfSpsEmpsts;
	}

	public String getDfSpsSmoking() {
		return dfSpsSmoking;
	}

	public void setDfSpsSmoking(String dfSpsSmoking) {
		this.dfSpsSmoking = dfSpsSmoking;
	}

	public String getDfSpsPersEmail() {
		return dfSpsPersEmail;
	}

	public void setDfSpsPersEmail(String dfSpsPersEmail) {
		this.dfSpsPersEmail = dfSpsPersEmail;
	}

	public String getDfSpsHp() {
		return dfSpsHp;
	}

	public void setDfSpsHp(String dfSpsHp) {
		this.dfSpsHp = dfSpsHp;
	}

	public String getDfSpsOffice() {
		return dfSpsOffice;
	}

	public void setDfSpsOffice(String dfSpsOffice) {
		this.dfSpsOffice = dfSpsOffice;
	}

	public String getDfFingoals() {
		return dfFingoals;
	}

	public void setDfFingoals(String dfFingoals) {
		this.dfFingoals = dfFingoals;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedData(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDfSpsHome() {
		return dfSpsHome;
	}

	public void setDfSpsHome(String dfSpsHome) {
		this.dfSpsHome = dfSpsHome;
	}

	public String getDfSpsHomeAddr() {
		return dfSpsHomeAddr;
	}

	public void setDfSpsHomeAddr(String dfSpsHomeAddr) {
		this.dfSpsHomeAddr = dfSpsHomeAddr;
	}

	public String getDfSelfCompname() {
		return dfSelfCompname;
	}

	public void setDfSelfCompname(String dfSelfCompname) {
		this.dfSelfCompname = dfSelfCompname;
	}

	public String getDfSpsCompname() {
		return dfSpsCompname;
	}

	public void setDfSpsCompname(String dfSpsCompname) {
		this.dfSpsCompname = dfSpsCompname;
	}

	public String getDfSelfBusinatr() {
		return dfSelfBusinatr;
	}

	public void setDfSelfBusinatr(String dfSelfBusinatr) {
		this.dfSelfBusinatr = dfSelfBusinatr;
	}

	public String getDfSpsBusinatr() {
		return dfSpsBusinatr;
	}

	public void setDfSpsBusinatr(String dfSpsBusinatr) {
		this.dfSpsBusinatr = dfSpsBusinatr;
	}

	public String getDfSelfBusinatrDets() {
		return dfSelfBusinatrDets;
	}

	public void setDfSelfBusinatrDets(String dfSelfBusinatrDets) {
		this.dfSelfBusinatrDets = dfSelfBusinatrDets;
	}

	public String getDfSpsBusinatrDets() {
		return dfSpsBusinatrDets;
	}

	public void setDfSpsBusinatrDets(String dfSpsBusinatrDets) {
		this.dfSpsBusinatrDets = dfSpsBusinatrDets;
	}

	public String getDfSelfUsnotifyflg() {
		return dfSelfUsnotifyflg;
	}

	public void setDfSelfUsnotifyflg(String dfSelfUsnotifyflg) {
		this.dfSelfUsnotifyflg = dfSelfUsnotifyflg;
	}

	public String getDfSpsUsnotifyflg() {
		return dfSpsUsnotifyflg;
	}

	public void setDfSpsUsnotifyflg(String dfSpsUsnotifyflg) {
		this.dfSpsUsnotifyflg = dfSpsUsnotifyflg;
	}

	public String getDfSelfUspersonflg() {
		return dfSelfUspersonflg;
	}

	public void setDfSelfUspersonflg(String dfSelfUspersonflg) {
		this.dfSelfUspersonflg = dfSelfUspersonflg;
	}

	public String getDfSpsUspersonflg() {
		return dfSpsUspersonflg;
	}

	public void setDfSpsUspersonflg(String dfSpsUspersonflg) {
		this.dfSpsUspersonflg = dfSpsUspersonflg;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDfSelfUstaxno() {
		return dfSelfUstaxno;
	}

	public void setDfSelfUstaxno(String dfSelfUstaxno) {
		this.dfSelfUstaxno = dfSelfUstaxno;
	}

	public String getDfSpsUstaxno() {
		return dfSpsUstaxno;
	}

	public void setDfSpsUstaxno(String dfSpsUstaxno) {
		this.dfSpsUstaxno = dfSpsUstaxno;
	}

	public String getDfSelfResCountry() {
		return dfSelfResCountry;
	}

	public void setDfSelfResCountry(String dfSelfResCountry) {
		this.dfSelfResCountry = dfSelfResCountry;
	}

	public String getDfSpsResCountry() {
		return dfSpsResCountry;
	}

	public void setDfSpsResCountry(String dfSpsResCountry) {
		this.dfSpsResCountry = dfSpsResCountry;
	}

	public String getResAddr1() {
		return resAddr1;
	}

	public void setResAddr1(String resAddr1) {
		this.resAddr1 = resAddr1;
	}

	public String getResAddr2() {
		return resAddr2;
	}

	public void setResAddr2(String resAddr2) {
		this.resAddr2 = resAddr2;
	}

	public String getResAddr3() {
		return resAddr3;
	}

	public void setResAddr3(String resAddr3) {
		this.resAddr3 = resAddr3;
	}

	public String getResCity() {
		return resCity;
	}

	public void setResCity(String resCity) {
		this.resCity = resCity;
	}

	public String getResState() {
		return resState;
	}

	public void setResState(String resState) {
		this.resState = resState;
	}

	public String getResCountry() {
		return resCountry;
	}

	public void setResCountry(String resCountry) {
		this.resCountry = resCountry;
	}

	public String getResPostalCode() {
		return resPostalCode;
	}

	public void setResPostalCode(String resPostalCode) {
		this.resPostalCode = resPostalCode;
	}

	public String getDfSelfAgeNext() {
		return dfSelfAgeNext;
	}

	public void setDfSelfAgeNext(String dfSelfAgeNext) {
		this.dfSelfAgeNext = dfSelfAgeNext;
	}

	public String getDfSpsAgeNext() {
		return dfSpsAgeNext;
	}

	public void setDfSpsAgeNext(String dfSpsAgeNext) {
		this.dfSpsAgeNext = dfSpsAgeNext;
	}

	public String getDfSelfNatyDets() {
		return dfSelfNatyDets;
	}

	public void setDfSelfNatyDets(String dfSelfNatyDets) {
		this.dfSelfNatyDets = dfSelfNatyDets;
	}

	public String getDfSpsNatyDets() {
		return dfSpsNatyDets;
	}

	public void setDfSpsNatyDets(String dfSpsNatyDets) {
		this.dfSpsNatyDets = dfSpsNatyDets;
	}

	public String getDfSelfEngSpoken() {
		return dfSelfEngSpoken;
	}

	public void setDfSelfEngSpoken(String dfSelfEngSpoken) {
		this.dfSelfEngSpoken = dfSelfEngSpoken;
	}

	public String getDfSelfEngWritten() {
		return dfSelfEngWritten;
	}

	public void setDfSelfEngWritten(String dfSelfEngWritten) {
		this.dfSelfEngWritten = dfSelfEngWritten;
	}

	public String getDfSpsEngSpoken() {
		return dfSpsEngSpoken;
	}

	public void setDfSpsEngSpoken(String dfSpsEngSpoken) {
		this.dfSpsEngSpoken = dfSpsEngSpoken;
	}

	public String getDfSpsEngWritten() {
		return dfSpsEngWritten;
	}

	public void setDfSpsEngWritten(String dfSpsEngWritten) {
		this.dfSpsEngWritten = dfSpsEngWritten;
	}

	public String getDfSelfEduLevel() {
		return dfSelfEduLevel;
	}

	public void setDfSelfEduLevel(String dfSelfEduLevel) {
		this.dfSelfEduLevel = dfSelfEduLevel;
	}

	public String getDfSpsEduLevel() {
		return dfSpsEduLevel;
	}

	public void setDfSpsEduLevel(String dfSpsEduLevel) {
		this.dfSpsEduLevel = dfSpsEduLevel;
	}

	public String getDfSelfReplExistPol() {
		return dfSelfReplExistPol;
	}

	public void setDfSelfReplExistPol(String dfSelfReplExistPol) {
		this.dfSelfReplExistPol = dfSelfReplExistPol;
	}

	public String getDfSpsReplExistPol() {
		return dfSpsReplExistPol;
	}

	public void setDfSpsReplExistPol(String dfSpsReplExistPol) {
		this.dfSpsReplExistPol = dfSpsReplExistPol;
	}

	public String getDfSelfBirthCntry() {
		return dfSelfBirthCntry;
	}

	public void setDfSelfBirthCntry(String dfSelfBirthCntry) {
		this.dfSelfBirthCntry = dfSelfBirthCntry;
	}

	public String getDfSpsBirthCntry() {
		return dfSpsBirthCntry;
	}

	public void setDfSpsBirthCntry(String dfSpsBirthCntry) {
		this.dfSpsBirthCntry = dfSpsBirthCntry;
	}

	public String getDfSelfOneContFlg() {
		return dfSelfOneContFlg;
	}

	public void setDfSelfOneContFlg(String dfSelfOneContFlg) {
		this.dfSelfOneContFlg = dfSelfOneContFlg;
	}

	public String getDfSelfSameAsregadd() {
		return dfSelfSameAsregadd;
	}

	public void setDfSelfSameAsregadd(String dfSelfSameAsregadd) {
		this.dfSelfSameAsregadd = dfSelfSameAsregadd;
	}

	public String getDfSelfMailAddrFlg() {
		return dfSelfMailAddrFlg;
	}

	public void setDfSelfMailAddrFlg(String dfSelfMailAddrFlg) {
		this.dfSelfMailAddrFlg = dfSelfMailAddrFlg;
	}

	public String getDfSpsMailAddrFlg() {
		return dfSpsMailAddrFlg;
	}

	public void setDfSpsMailAddrFlg(String dfSpsMailAddrFlg) {
		this.dfSpsMailAddrFlg = dfSpsMailAddrFlg;
	}

	public String getDfSelfMailAddr() {
		return dfSelfMailAddr;
	}

	public void setDfSelfMailAddr(String dfSelfMailAddr) {
		this.dfSelfMailAddr = dfSelfMailAddr;
	}

	public String getDfSpsMailAddr() {
		return dfSpsMailAddr;
	}

	public void setDfSpsMailAddr(String dfSpsMailAddr) {
		this.dfSpsMailAddr = dfSpsMailAddr;
	}

	public String getDfSelfAnnlIncome() {
		return dfSelfAnnlIncome;
	}

	public void setDfSelfAnnlIncome(String dfSelfAnnlIncome) {
		this.dfSelfAnnlIncome = dfSelfAnnlIncome;
	}

	public String getDfSpsAnnlIncome() {
		return dfSpsAnnlIncome;
	}

	public void setDfSpsAnnlIncome(String dfSpsAnnlIncome) {
		this.dfSpsAnnlIncome = dfSpsAnnlIncome;
	}

	public String getDfSelfFundsrc() {
		return dfSelfFundsrc;
	}

	public void setDfSelfFundsrc(String dfSelfFundsrc) {
		this.dfSelfFundsrc = dfSelfFundsrc;
	}

	public String getDfSpsFundsrc() {
		return dfSpsFundsrc;
	}

	public void setDfSpsFundsrc(String dfSpsFundsrc) {
		this.dfSpsFundsrc = dfSpsFundsrc;
	}

	public String getDfSelfFundsrcDets() {
		return dfSelfFundsrcDets;
	}

	public void setDfSelfFundsrcDets(String dfSelfFundsrcDets) {
		this.dfSelfFundsrcDets = dfSelfFundsrcDets;
	}

	public String getDfSpsFundsrcDets() {
		return dfSpsFundsrcDets;
	}

	public void setDfSpsFundsrcDets(String dfSpsFundsrcDets) {
		this.dfSpsFundsrcDets = dfSpsFundsrcDets;
	}

	public String getDfSelfHeight() {
		return dfSelfHeight;
	}

	public void setDfSelfHeight(String dfSelfHeight) {
		this.dfSelfHeight = dfSelfHeight;
	}

	public String getDfSpsHeight() {
		return dfSpsHeight;
	}

	public void setDfSpsHeight(String dfSpsHeight) {
		this.dfSpsHeight = dfSpsHeight;
	}

	public String getDfSelfWeight() {
		return dfSelfWeight;
	}

	public void setDfSelfWeight(String dfSelfWeight) {
		this.dfSelfWeight = dfSelfWeight;
	}

	public String getDfSpsWeight() {
		return dfSpsWeight;
	}

	public void setDfSpsWeight(String dfSpsWeight) {
		this.dfSpsWeight = dfSpsWeight;
	}

	public String getDfSelfTaxres() {
		return dfSelfTaxres;
	}

	public void setDfSelfTaxres(String dfSelfTaxres) {
		this.dfSelfTaxres = dfSelfTaxres;
	}

	public String getDfSelfTaxresOth() {
		return dfSelfTaxresOth;
	}

	public void setDfSelfTaxresOth(String dfSelfTaxresOth) {
		this.dfSelfTaxresOth = dfSelfTaxresOth;
	}

	public String getDfSpsTaxres() {
		return dfSpsTaxres;
	}

	public void setDfSpsTaxres(String dfSpsTaxres) {
		this.dfSpsTaxres = dfSpsTaxres;
	}

	public String getDfSpsTaxresOth() {
		return dfSpsTaxresOth;
	}

	public void setDfSpsTaxresOth(String dfSpsTaxresOth) {
		this.dfSpsTaxresOth = dfSpsTaxresOth;
	}

}
