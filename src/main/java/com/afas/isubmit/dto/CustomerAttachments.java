package com.afas.isubmit.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "CUSTOMER_ATTACHMENTS")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerAttachments {

	@Id
	@GenericGenerator(name = "custAttachdetails", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "custattach_seq"),
			@Parameter(name = "sequence_prefix", value = "CUSTATTCH"), })
	@GeneratedValue(generator = "custAttachdetails", strategy = GenerationType.SEQUENCE)
	@Column(name = "CUST_ATTACH_ID", nullable = false)
	private String custAttachId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CUST_ID", nullable = false)
	private CustomerDetails cusDetails;

	@Column(name = "TITLE", nullable = false)
	private String title;

	@Column(name = "PAGE_NUM")
	private String pageNum;

	@Column(name = "REMARKS")
	private String remarks;

	@Column(name = "FILENAME")
	private String fileName;

	@Column(name = "FILESIZE")
	private String fileSize;

	@Column(name = "FILETYPE")
	private String fileType;

	@Column(name = "ATTACH_CATEG_ID")
	private String attachCategId;

	@Column(name = "ATTACH_CATEG_NAME")
	private String attachCategName;

	@Column(name = "CREATED_BY", nullable = false)
	private String createdBy;

	@Column(name = "CREATED_DATE", nullable = false)
	private Date createdDate;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	@Column(name = "DISTRIBUTOR_ID", nullable = false)
	private String distributorId;

	@Column(name = "DISTRIBUTOR_NAME")
	private String distributorName;

	@Column(name = "DOCUMENT")
	private byte[] documentData;

	@Column(name = "NTUC_POLICY_ID")
	private String ntucPolicyId;

	@Column(name = "ATTACH_FOR")
	private String attachFor;

	@Column(name = "ATTACH_APPLICANT_NAME")
	private String attachApplicantName;

	@Column(name = "decryptkey")
	private String decryptKey;

	@Column(name = "attachment_location")
	private String attachmentLocation;

	@Column(name = "INSURER_NAME")
	private String insurerName;

	public String getCustAttachId() {
		return custAttachId;
	}

	public void setCustAttachId(String custAttachId) {
		this.custAttachId = custAttachId;
	}

	public CustomerDetails getCusDetails() {
		return cusDetails;
	}

	public void setCusDetails(CustomerDetails cusDetails) {
		this.cusDetails = cusDetails;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPageNum() {
		return pageNum;
	}

	public void setPageNum(String pageNum) {
		this.pageNum = pageNum;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileSize() {
		return fileSize;
	}

	public void setFileSize(String fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getAttachCategId() {
		return attachCategId;
	}

	public void setAttachCategId(String attachCategId) {
		this.attachCategId = attachCategId;
	}

	public String getAttachCategName() {
		return attachCategName;
	}

	public void setAttachCategName(String attachCategName) {
		this.attachCategName = attachCategName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getDistributorId() {
		return distributorId;
	}

	public void setDistributorId(String distributorId) {
		this.distributorId = distributorId;
	}

	public String getDistributorName() {
		return distributorName;
	}

	public void setDistributorName(String distributorName) {
		this.distributorName = distributorName;
	}

	public byte[] getDocumentData() {
		return documentData;
	}

	public void setDocumentData(byte[] documentData) {
		this.documentData = documentData;
	}

	public String getNtucPolicyId() {
		return ntucPolicyId;
	}

	public void setNtucPolicyId(String ntucPolicyId) {
		this.ntucPolicyId = ntucPolicyId;
	}

	public String getAttachFor() {
		return attachFor;
	}

	public void setAttachFor(String attachFor) {
		this.attachFor = attachFor;
	}

	public String getAttachApplicantName() {
		return attachApplicantName;
	}

	public void setAttachApplicantName(String attachApplicantName) {
		this.attachApplicantName = attachApplicantName;
	}

	public String getDecryptKey() {
		return decryptKey;
	}

	public void setDecryptKey(String decryptKey) {
		this.decryptKey = decryptKey;
	}

	public String getAttachmentLocation() {
		return attachmentLocation;
	}

	public void setAttachmentLocation(String attachmentLocation) {
		this.attachmentLocation = attachmentLocation;
	}

	public String getInsurerName() {
		return insurerName;
	}

	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}

}
