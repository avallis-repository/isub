package com.afas.isubmit.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "fna_otherperson_dets")
public class FnaOtherPersonDets implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "othPerDetails", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "fna_otherpers_seq"),
			@Parameter(name = "sequence_prefix", value = "OTH"), })
	@GeneratedValue(generator = "othPerDetails", strategy = GenerationType.SEQUENCE)
	@Column(name = "othper_id")
	private String othperId;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@ManyToOne
	@JoinColumn(name = "fna_id")
	private FnaDetails fnaDetails;

	@Column(name = "othper_bankname")
	private String othperBankname;

	@Column(name = "othper_categ")
	private String othperCateg;

	@Column(name = "othper_chq_orderno")
	private String othperChqOrderno;

	@Column(name = "othper_contactno")
	private String othperContactno;

	@Column(name = "othper_incorpno")
	private String othperIncorpno;

	@Column(name = "othper_name")
	private String othperName;

	@Column(name = "othper_nric")
	private String othperNric;

	@Column(name = "othper_paymentmode")
	private String othperPaymentmode;

	@Column(name = "othper_regaddr")
	private String othperRegaddr;

	@Column(name = "othper_relation")
	private String othperRelation;

	@Column(name = "othper_work")
	private String othperWork;

	@Column(name = "othper_work_country")
	private String othperWorkCountry;

	public FnaOtherPersonDets() {
	}

	public String getOthperId() {
		return this.othperId;
	}

	public void setOthperId(String othperId) {
		this.othperId = othperId;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getOthperBankname() {
		return this.othperBankname;
	}

	public void setOthperBankname(String othperBankname) {
		this.othperBankname = othperBankname;
	}

	public String getOthperCateg() {
		return this.othperCateg;
	}

	public void setOthperCateg(String othperCateg) {
		this.othperCateg = othperCateg;
	}

	public String getOthperChqOrderno() {
		return this.othperChqOrderno;
	}

	public void setOthperChqOrderno(String othperChqOrderno) {
		this.othperChqOrderno = othperChqOrderno;
	}

	public String getOthperContactno() {
		return this.othperContactno;
	}

	public void setOthperContactno(String othperContactno) {
		this.othperContactno = othperContactno;
	}

	public String getOthperIncorpno() {
		return this.othperIncorpno;
	}

	public void setOthperIncorpno(String othperIncorpno) {
		this.othperIncorpno = othperIncorpno;
	}

	public String getOthperName() {
		return this.othperName;
	}

	public void setOthperName(String othperName) {
		this.othperName = othperName;
	}

	public String getOthperNric() {
		return this.othperNric;
	}

	public void setOthperNric(String othperNric) {
		this.othperNric = othperNric;
	}

	public String getOthperPaymentmode() {
		return this.othperPaymentmode;
	}

	public void setOthperPaymentmode(String othperPaymentmode) {
		this.othperPaymentmode = othperPaymentmode;
	}

	public String getOthperRegaddr() {
		return this.othperRegaddr;
	}

	public void setOthperRegaddr(String othperRegaddr) {
		this.othperRegaddr = othperRegaddr;
	}

	public String getOthperRelation() {
		return this.othperRelation;
	}

	public void setOthperRelation(String othperRelation) {
		this.othperRelation = othperRelation;
	}

	public String getOthperWork() {
		return this.othperWork;
	}

	public void setOthperWork(String othperWork) {
		this.othperWork = othperWork;
	}

	public String getOthperWorkCountry() {
		return this.othperWorkCountry;
	}

	public void setOthperWorkCountry(String othperWorkCountry) {
		this.othperWorkCountry = othperWorkCountry;
	}

}