package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.annotation.JsonInclude;

@Entity
@Table(name = "FNA_DEPENDANT_DETS")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FnaDependantDets {
	@Id
	@GenericGenerator(name = "depDetails", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "fna_dependant_seq"),
			@Parameter(name = "sequence_prefix", value = "DEP"), })
	@GeneratedValue(generator = "depDetails", strategy = GenerationType.SEQUENCE)
	@Column(name = "DEPN_ID", nullable = false)
	private String depnId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "DEPN_NAME")
	private String depnName;

	@Column(name = "DEPN_RELATIONSHIP")
	private String depnRelationShip;

	@Column(name = "DEPN_AGE")
	private String depnAge;

	@Column(name = "DEPN_GENDER")
	private String depnGender;

	@Column(name = "DEPN_YRSSUPPORT")
	private String depnYrsSupport;

	@Column(name = "DEPN_MONTHCONTR")
	private float depnMonthContr;

	@Column(name = "DEPN_PROV_SELF")
	private float depnProvSelf;

	@Column(name = "DEPN_PROV_SPOUSE")
	private float depnProvSpouse;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "DEPN_OCCP")
	private String depnOccp;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "dataFormId")
	private FnaSelfSpouseDets fnaDependant;

	public String getDepnId() {
		return depnId;
	}

	public void setDepnId(String depnId) {
		this.depnId = depnId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getDepnName() {
		return depnName;
	}

	public void setDepnName(String depnName) {
		this.depnName = depnName;
	}

	public String getDepnRelationShip() {
		return depnRelationShip;
	}

	public void setDepnRelationShip(String depnRelationShip) {
		this.depnRelationShip = depnRelationShip;
	}

	public String getDepnAge() {
		return depnAge;
	}

	public void setDepnAge(String depnAge) {
		this.depnAge = depnAge;
	}

	public String getDepnGender() {
		return depnGender;
	}

	public void setDepnGender(String depnGender) {
		this.depnGender = depnGender;
	}

	public String getDepnYrsSupport() {
		return depnYrsSupport;
	}

	public void setDepnYrsSupport(String depnYrsSupport) {
		this.depnYrsSupport = depnYrsSupport;
	}

	public float getDepnMonthContr() {
		return depnMonthContr;
	}

	public void setDepnMonthContr(float depnMonthContr) {
		this.depnMonthContr = depnMonthContr;
	}

	public float getDepnProvSelf() {
		return depnProvSelf;
	}

	public void setDepnProvSelf(float depnProvSelf) {
		this.depnProvSelf = depnProvSelf;
	}

	public float getDepnProvSpouse() {
		return depnProvSpouse;
	}

	public void setDepnProvSpouse(float depnProvSpouse) {
		this.depnProvSpouse = depnProvSpouse;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getDepnOccp() {
		return depnOccp;
	}

	public void setDepnOccp(String depnOccp) {
		this.depnOccp = depnOccp;
	}

	public FnaSelfSpouseDets getFnaDependant() {
		return fnaDependant;
	}

	public void setFnaDependant(FnaSelfSpouseDets fnaDependant) {
		this.fnaDependant = fnaDependant;
	}

}
