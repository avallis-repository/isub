package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FNA_RISKPREF_INVOBJ")
public class FnaRiskPrefInvobj {

	@Id
	@Column(name = "CR_ID", nullable = false)
	private String crId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "CR_FOR")
	private String crFor;

	@Column(name = "CR_INVSTOBJ")
	private String crInvstObj;

	@Column(name = "CR_INVSTAMT")
	private float crInvstaMT;

	@Column(name = "CR_INVSTTIMEHORIZON")
	private String crInvstTimeHorizon;

	@Column(name = "CR_RISKPREF")
	private String crRiskPref;

	@Column(name = "CR_RISKCLASS")
	private String crRiskClass;

	@Column(name = "CR_ROI")
	private float crRoi;

	@Column(name = "CR_ADEQFUND")
	private String crAdeqFund;

	@Column(name = "CR_OTHCONCERN")
	private String crOthConcern;

	@Column(name = "CR_CRTDBY")
	private String crCrtdBy;

	@Column(name = "CR_CRTDDATE")
	private Date crCrtdDate;

	@Column(name = "CR_MODBY")
	private String crModBy;

	@Column(name = "CR_MODDATE")
	private Date crModDate;

	public String getCrId() {
		return crId;
	}

	public void setCrId(String crId) {
		this.crId = crId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getCrFor() {
		return crFor;
	}

	public void setCrFor(String crFor) {
		this.crFor = crFor;
	}

	public String getCrInvstObj() {
		return crInvstObj;
	}

	public void setCrInvstObj(String crInvstObj) {
		this.crInvstObj = crInvstObj;
	}

	public float getCrInvstaMT() {
		return crInvstaMT;
	}

	public void setCrInvstaMT(float crInvstaMT) {
		this.crInvstaMT = crInvstaMT;
	}

	public String getCrInvstTimeHorizon() {
		return crInvstTimeHorizon;
	}

	public void setCrInvstTimeHorizon(String crInvstTimeHorizon) {
		this.crInvstTimeHorizon = crInvstTimeHorizon;
	}

	public String getCrRiskPref() {
		return crRiskPref;
	}

	public void setCrRiskPref(String crRiskPref) {
		this.crRiskPref = crRiskPref;
	}

	public String getCrRiskClass() {
		return crRiskClass;
	}

	public void setCrRiskClass(String crRiskClass) {
		this.crRiskClass = crRiskClass;
	}

	public float getCrRoi() {
		return crRoi;
	}

	public void setCrRoi(float crRoi) {
		this.crRoi = crRoi;
	}

	public String getCrAdeqFund() {
		return crAdeqFund;
	}

	public void setCrAdeqFund(String crAdeqFund) {
		this.crAdeqFund = crAdeqFund;
	}

	public String getCrOthConcern() {
		return crOthConcern;
	}

	public void setCrOthConcern(String crOthConcern) {
		this.crOthConcern = crOthConcern;
	}

	public String getCrCrtdBy() {
		return crCrtdBy;
	}

	public void setCrCrtdBy(String crCrtdBy) {
		this.crCrtdBy = crCrtdBy;
	}

	public Date getCrCrtdDate() {
		return crCrtdDate;
	}

	public void setCrCrtdDate(Date crCrtdDate) {
		this.crCrtdDate = crCrtdDate;
	}

	public String getCrModBy() {
		return crModBy;
	}

	public void setCrModBy(String crModBy) {
		this.crModBy = crModBy;
	}

	public Date getCrModDate() {
		return crModDate;
	}

	public void setCrModDate(Date crModDate) {
		this.crModDate = crModDate;
	}

}
