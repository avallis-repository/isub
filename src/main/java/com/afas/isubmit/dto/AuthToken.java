package com.afas.isubmit.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "AUTH_TOKEN")
public class AuthToken implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GenericGenerator(name = "authToken", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + ".auth_sequence"),
			@Parameter(name = "sequence_prefix", value = "AUTH") })
	@GeneratedValue(generator = "authToken", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private String id;
	
	@Column(name = "ACCESS_TOKEN")
	private String accessToken;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss a", timezone = "Asia/Singapore")
	@Column(name = "ACCESS_TOKEN_EXP_DATE")
	private Date accessTokenExpiredDate;
	
	@Column(name = "REFRESH_TOKEN")
	private String refreshToken;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss a", timezone = "Asia/Singapore")
	@Column(name = "REFRESH_TOKEN_EXP_DATE")
	private Date refreshTokenExpiredDate;
	
	@Column(name = "TOKEN_TYPE")
	private String tokenType;
	
	@Column(name = "NOT_BEFORE_POLICY")
	private Long notBeforePolicy;
	
	@Column(name = "SESSION_STATE")
	private String sessionState;
	
	@Column(name = "SCOPE")
	private String scope;
	
	@Column(name = "ACTIVE")
	private String active;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss a",timezone = "Asia/Singapore")
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy hh:mm:ss a",timezone = "Asia/Singapore")
	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Date getAccessTokenExpiredDate() {
		return accessTokenExpiredDate;
	}

	public void setAccessTokenExpiredDate(Date accessTokenExpiredDate) {
		this.accessTokenExpiredDate = accessTokenExpiredDate;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public Date getRefreshTokenExpiredDate() {
		return refreshTokenExpiredDate;
	}

	public void setRefreshTokenExpiredDate(Date refreshTokenExpiredDate) {
		this.refreshTokenExpiredDate = refreshTokenExpiredDate;
	}

	public String getTokenType() {
		return tokenType;
	}

	public void setTokenType(String tokenType) {
		this.tokenType = tokenType;
	}

	public Long getNotBeforePolicy() {
		return notBeforePolicy;
	}

	public void setNotBeforePolicy(Long notBeforePolicy) {
		this.notBeforePolicy = notBeforePolicy;
	}

	public String getSessionState() {
		return sessionState;
	}

	public void setSessionState(String sessionState) {
		this.sessionState = sessionState;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String isActive() {
		return active;
	}

	public void setActive(String active) {
		this.active = active;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
}
