package com.afas.isubmit.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "fna_expenditure_dets")
public class FnaExpenditureDets {

	@Id
	@GenericGenerator(name = "fnaExpendDets", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + "." + "fna_expend_seq"),
			@Parameter(name = "sequence_prefix", value = "EXP"), })
	@GeneratedValue(generator = "fnaExpendDets", strategy = GenerationType.SEQUENCE)
	@Column(name = "expd_id")
	private String expdId;

	@Column(name = "al_family_netasset")
	private double alFamilyNetasset;

	@Column(name = "al_family_totasset")
	private double alFamilyTotasset;

	@Column(name = "al_family_totliab")
	private double alFamilyTotliab;

	@Column(name = "al_self_netasset")
	private double alSelfNetasset;

	@Column(name = "al_self_totasset")
	private double alSelfTotasset;

	@Column(name = "al_self_totliab")
	private double alSelfTotliab;

	@Column(name = "al_sps_netasset")
	private double alSpsNetasset;

	@Column(name = "al_sps_totasset")
	private double alSpsTotasset;

	@Column(name = "al_sps_totliab")
	private double alSpsTotliab;

	@Column(name = "cf_family_annlinc")
	private double cfFamilyAnnlinc;

	@Column(name = "cf_family_cpfcontib")
	private double cfFamilyCpfcontib;

	@Column(name = "cf_family_estexpense")
	private double cfFamilyEstexpense;

	@Column(name = "cf_family_surpdef")
	private double cfFamilySurpdef;

	@Column(name = "cf_self_annlinc")
	private double cfSelfAnnlinc;

	@Column(name = "cf_self_cpfcontib")
	private double cfSelfCpfcontib;

	@Column(name = "cf_self_estexpense")
	private double cfSelfEstexpense;

	@Column(name = "cf_self_surpdef")
	private double cfSelfSurpdef;

	@Column(name = "cf_sps_annlinc")
	private double cfSpsAnnlinc;

	@Column(name = "cf_sps_cpfcontib")
	private double cfSpsCpfcontib;

	@Column(name = "cf_sps_estexpense")
	private double cfSpsEstexpense;

	@Column(name = "cf_sps_surpdef")
	private double cfSpsSurpdef;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Date createdDate;

	@Column(name = "expd_family_charity")
	private double expdFamilyCharity;

	@Column(name = "expd_family_clothing")
	private double expdFamilyClothing;

	@Column(name = "expd_family_domestic")
	private double expdFamilyDomestic;

	@Column(name = "expd_family_eatingout")
	private double expdFamilyEatingout;

	@Column(name = "expd_family_enhance")
	private double expdFamilyEnhance;

	@Column(name = "expd_family_entertain")
	private double expdFamilyEntertain;

	@Column(name = "expd_family_festiv")
	private double expdFamilyFestiv;

	@Column(name = "expd_family_grocery")
	private double expdFamilyGrocery;

	@Column(name = "expd_family_household")
	private double expdFamilyHousehold;

	@Column(name = "expd_family_insurance")
	private double expdFamilyInsurance;

	@Column(name = "expd_family_medical")
	private double expdFamilyMedical;

	@Column(name = "expd_family_others")
	private double expdFamilyOthers;

	@Column(name = "expd_family_personal")
	private double expdFamilyPersonal;

	@Column(name = "expd_family_proploan")
	private double expdFamilyProploan;

	@Column(name = "expd_family_rent")
	private double expdFamilyRent;

	@Column(name = "expd_family_taxes")
	private double expdFamilyTaxes;

	@Column(name = "expd_family_transport")
	private double expdFamilyTransport;

	@Column(name = "expd_family_util")
	private double expdFamilyUtil;

	@Column(name = "expd_family_vacations")
	private double expdFamilyVacations;

	@Column(name = "expd_family_vehiloan")
	private double expdFamilyVehiloan;

	@Column(name = "expd_remarks")
	private String expdRemarks;

	@Column(name = "expd_self_charity")
	private double expdSelfCharity;

	@Column(name = "expd_self_clothing")
	private double expdSelfClothing;

	@Column(name = "expd_self_depntcontr")
	private double expdSelfDepntcontr;

	@Column(name = "expd_self_eatingout")
	private double expdSelfEatingout;

	@Column(name = "expd_self_entertain")
	private double expdSelfEntertain;

	@Column(name = "expd_self_festiv")
	private double expdSelfFestiv;

	@Column(name = "expd_self_grocery")
	private double expdSelfGrocery;

	@Column(name = "expd_self_insurance")
	private double expdSelfInsurance;

	@Column(name = "expd_self_medical")
	private double expdSelfMedical;

	@Column(name = "expd_self_others")
	private double expdSelfOthers;

	@Column(name = "expd_self_personal")
	private double expdSelfPersonal;

	@Column(name = "expd_self_proploan")
	private double expdSelfProploan;

	@Column(name = "expd_self_rent")
	private double expdSelfRent;

	@Column(name = "expd_self_taxes")
	private double expdSelfTaxes;

	@Column(name = "expd_self_transport")
	private double expdSelfTransport;

	@Column(name = "expd_self_util")
	private double expdSelfUtil;

	@Column(name = "expd_self_vacations")
	private double expdSelfVacations;

	@Column(name = "expd_self_vehiloan")
	private double expdSelfVehiloan;

	@Column(name = "expd_sps_charity")
	private double expdSpsCharity;

	@Column(name = "expd_sps_clothing")
	private double expdSpsClothing;

	@Column(name = "expd_sps_depntcontr")
	private double expdSpsDepntcontr;

	@Column(name = "expd_sps_eatingout")
	private double expdSpsEatingout;

	@Column(name = "expd_sps_entertain")
	private double expdSpsEntertain;

	@Column(name = "expd_sps_festiv")
	private double expdSpsFestiv;

	@Column(name = "expd_sps_grocery")
	private double expdSpsGrocery;

	@Column(name = "expd_sps_insurance")
	private double expdSpsInsurance;

	@Column(name = "expd_sps_medical")
	private double expdSpsMedical;

	@Column(name = "expd_sps_others")
	private double expdSpsOthers;

	@Column(name = "expd_sps_personal")
	private double expdSpsPersonal;

	@Column(name = "expd_sps_proploan")
	private double expdSpsProploan;

	@Column(name = "expd_sps_rent")
	private double expdSpsRent;

	@Column(name = "expd_sps_taxes")
	private double expdSpsTaxes;

	@Column(name = "expd_sps_transport")
	private double expdSpsTransport;

	@Column(name = "expd_sps_util")
	private double expdSpsUtil;

	@Column(name = "expd_sps_vacations")
	private double expdSpsVacations;

	@Column(name = "expd_sps_vehiloan")
	private double expdSpsVehiloan;

	// bi-directional many-to-one association to FnaDetail
	@ManyToOne
	@JoinColumn(name = "fna_id")
	private FnaDetails fnaDetails;

	public String getExpdId() {
		return expdId;
	}

	public void setExpdId(String expdId) {
		this.expdId = expdId;
	}

	public double getAlFamilyNetasset() {
		return alFamilyNetasset;
	}

	public void setAlFamilyNetasset(double alFamilyNetasset) {
		this.alFamilyNetasset = alFamilyNetasset;
	}

	public double getAlFamilyTotasset() {
		return alFamilyTotasset;
	}

	public void setAlFamilyTotasset(double alFamilyTotasset) {
		this.alFamilyTotasset = alFamilyTotasset;
	}

	public double getAlFamilyTotliab() {
		return alFamilyTotliab;
	}

	public void setAlFamilyTotliab(double alFamilyTotliab) {
		this.alFamilyTotliab = alFamilyTotliab;
	}

	public double getAlSelfNetasset() {
		return alSelfNetasset;
	}

	public void setAlSelfNetasset(double alSelfNetasset) {
		this.alSelfNetasset = alSelfNetasset;
	}

	public double getAlSelfTotasset() {
		return alSelfTotasset;
	}

	public void setAlSelfTotasset(double alSelfTotasset) {
		this.alSelfTotasset = alSelfTotasset;
	}

	public double getAlSelfTotliab() {
		return alSelfTotliab;
	}

	public void setAlSelfTotliab(double alSelfTotliab) {
		this.alSelfTotliab = alSelfTotliab;
	}

	public double getAlSpsNetasset() {
		return alSpsNetasset;
	}

	public void setAlSpsNetasset(double alSpsNetasset) {
		this.alSpsNetasset = alSpsNetasset;
	}

	public double getAlSpsTotasset() {
		return alSpsTotasset;
	}

	public void setAlSpsTotasset(double alSpsTotasset) {
		this.alSpsTotasset = alSpsTotasset;
	}

	public double getAlSpsTotliab() {
		return alSpsTotliab;
	}

	public void setAlSpsTotliab(double alSpsTotliab) {
		this.alSpsTotliab = alSpsTotliab;
	}

	public double getCfFamilyAnnlinc() {
		return cfFamilyAnnlinc;
	}

	public void setCfFamilyAnnlinc(double cfFamilyAnnlinc) {
		this.cfFamilyAnnlinc = cfFamilyAnnlinc;
	}

	public double getCfFamilyCpfcontib() {
		return cfFamilyCpfcontib;
	}

	public void setCfFamilyCpfcontib(double cfFamilyCpfcontib) {
		this.cfFamilyCpfcontib = cfFamilyCpfcontib;
	}

	public double getCfFamilyEstexpense() {
		return cfFamilyEstexpense;
	}

	public void setCfFamilyEstexpense(double cfFamilyEstexpense) {
		this.cfFamilyEstexpense = cfFamilyEstexpense;
	}

	public double getCfFamilySurpdef() {
		return cfFamilySurpdef;
	}

	public void setCfFamilySurpdef(double cfFamilySurpdef) {
		this.cfFamilySurpdef = cfFamilySurpdef;
	}

	public double getCfSelfAnnlinc() {
		return cfSelfAnnlinc;
	}

	public void setCfSelfAnnlinc(double cfSelfAnnlinc) {
		this.cfSelfAnnlinc = cfSelfAnnlinc;
	}

	public double getCfSelfCpfcontib() {
		return cfSelfCpfcontib;
	}

	public void setCfSelfCpfcontib(double cfSelfCpfcontib) {
		this.cfSelfCpfcontib = cfSelfCpfcontib;
	}

	public double getCfSelfEstexpense() {
		return cfSelfEstexpense;
	}

	public void setCfSelfEstexpense(double cfSelfEstexpense) {
		this.cfSelfEstexpense = cfSelfEstexpense;
	}

	public double getCfSelfSurpdef() {
		return cfSelfSurpdef;
	}

	public void setCfSelfSurpdef(double cfSelfSurpdef) {
		this.cfSelfSurpdef = cfSelfSurpdef;
	}

	public double getCfSpsAnnlinc() {
		return cfSpsAnnlinc;
	}

	public void setCfSpsAnnlinc(double cfSpsAnnlinc) {
		this.cfSpsAnnlinc = cfSpsAnnlinc;
	}

	public double getCfSpsCpfcontib() {
		return cfSpsCpfcontib;
	}

	public void setCfSpsCpfcontib(double cfSpsCpfcontib) {
		this.cfSpsCpfcontib = cfSpsCpfcontib;
	}

	public double getCfSpsEstexpense() {
		return cfSpsEstexpense;
	}

	public void setCfSpsEstexpense(double cfSpsEstexpense) {
		this.cfSpsEstexpense = cfSpsEstexpense;
	}

	public double getCfSpsSurpdef() {
		return cfSpsSurpdef;
	}

	public void setCfSpsSurpdef(double cfSpsSurpdef) {
		this.cfSpsSurpdef = cfSpsSurpdef;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public double getExpdFamilyCharity() {
		return expdFamilyCharity;
	}

	public void setExpdFamilyCharity(double expdFamilyCharity) {
		this.expdFamilyCharity = expdFamilyCharity;
	}

	public double getExpdFamilyClothing() {
		return expdFamilyClothing;
	}

	public void setExpdFamilyClothing(double expdFamilyClothing) {
		this.expdFamilyClothing = expdFamilyClothing;
	}

	public double getExpdFamilyDomestic() {
		return expdFamilyDomestic;
	}

	public void setExpdFamilyDomestic(double expdFamilyDomestic) {
		this.expdFamilyDomestic = expdFamilyDomestic;
	}

	public double getExpdFamilyEatingout() {
		return expdFamilyEatingout;
	}

	public void setExpdFamilyEatingout(double expdFamilyEatingout) {
		this.expdFamilyEatingout = expdFamilyEatingout;
	}

	public double getExpdFamilyEnhance() {
		return expdFamilyEnhance;
	}

	public void setExpdFamilyEnhance(double expdFamilyEnhance) {
		this.expdFamilyEnhance = expdFamilyEnhance;
	}

	public double getExpdFamilyEntertain() {
		return expdFamilyEntertain;
	}

	public void setExpdFamilyEntertain(double expdFamilyEntertain) {
		this.expdFamilyEntertain = expdFamilyEntertain;
	}

	public double getExpdFamilyFestiv() {
		return expdFamilyFestiv;
	}

	public void setExpdFamilyFestiv(double expdFamilyFestiv) {
		this.expdFamilyFestiv = expdFamilyFestiv;
	}

	public double getExpdFamilyGrocery() {
		return expdFamilyGrocery;
	}

	public void setExpdFamilyGrocery(double expdFamilyGrocery) {
		this.expdFamilyGrocery = expdFamilyGrocery;
	}

	public double getExpdFamilyHousehold() {
		return expdFamilyHousehold;
	}

	public void setExpdFamilyHousehold(double expdFamilyHousehold) {
		this.expdFamilyHousehold = expdFamilyHousehold;
	}

	public double getExpdFamilyInsurance() {
		return expdFamilyInsurance;
	}

	public void setExpdFamilyInsurance(double expdFamilyInsurance) {
		this.expdFamilyInsurance = expdFamilyInsurance;
	}

	public double getExpdFamilyMedical() {
		return expdFamilyMedical;
	}

	public void setExpdFamilyMedical(double expdFamilyMedical) {
		this.expdFamilyMedical = expdFamilyMedical;
	}

	public double getExpdFamilyOthers() {
		return expdFamilyOthers;
	}

	public void setExpdFamilyOthers(double expdFamilyOthers) {
		this.expdFamilyOthers = expdFamilyOthers;
	}

	public double getExpdFamilyPersonal() {
		return expdFamilyPersonal;
	}

	public void setExpdFamilyPersonal(double expdFamilyPersonal) {
		this.expdFamilyPersonal = expdFamilyPersonal;
	}

	public double getExpdFamilyProploan() {
		return expdFamilyProploan;
	}

	public void setExpdFamilyProploan(double expdFamilyProploan) {
		this.expdFamilyProploan = expdFamilyProploan;
	}

	public double getExpdFamilyRent() {
		return expdFamilyRent;
	}

	public void setExpdFamilyRent(double expdFamilyRent) {
		this.expdFamilyRent = expdFamilyRent;
	}

	public double getExpdFamilyTaxes() {
		return expdFamilyTaxes;
	}

	public void setExpdFamilyTaxes(double expdFamilyTaxes) {
		this.expdFamilyTaxes = expdFamilyTaxes;
	}

	public double getExpdFamilyTransport() {
		return expdFamilyTransport;
	}

	public void setExpdFamilyTransport(double expdFamilyTransport) {
		this.expdFamilyTransport = expdFamilyTransport;
	}

	public double getExpdFamilyUtil() {
		return expdFamilyUtil;
	}

	public void setExpdFamilyUtil(double expdFamilyUtil) {
		this.expdFamilyUtil = expdFamilyUtil;
	}

	public double getExpdFamilyVacations() {
		return expdFamilyVacations;
	}

	public void setExpdFamilyVacations(double expdFamilyVacations) {
		this.expdFamilyVacations = expdFamilyVacations;
	}

	public double getExpdFamilyVehiloan() {
		return expdFamilyVehiloan;
	}

	public void setExpdFamilyVehiloan(double expdFamilyVehiloan) {
		this.expdFamilyVehiloan = expdFamilyVehiloan;
	}

	public String getExpdRemarks() {
		return expdRemarks;
	}

	public void setExpdRemarks(String expdRemarks) {
		this.expdRemarks = expdRemarks;
	}

	public double getExpdSelfCharity() {
		return expdSelfCharity;
	}

	public void setExpdSelfCharity(double expdSelfCharity) {
		this.expdSelfCharity = expdSelfCharity;
	}

	public double getExpdSelfClothing() {
		return expdSelfClothing;
	}

	public void setExpdSelfClothing(double expdSelfClothing) {
		this.expdSelfClothing = expdSelfClothing;
	}

	public double getExpdSelfDepntcontr() {
		return expdSelfDepntcontr;
	}

	public void setExpdSelfDepntcontr(double expdSelfDepntcontr) {
		this.expdSelfDepntcontr = expdSelfDepntcontr;
	}

	public double getExpdSelfEatingout() {
		return expdSelfEatingout;
	}

	public void setExpdSelfEatingout(double expdSelfEatingout) {
		this.expdSelfEatingout = expdSelfEatingout;
	}

	public double getExpdSelfEntertain() {
		return expdSelfEntertain;
	}

	public void setExpdSelfEntertain(double expdSelfEntertain) {
		this.expdSelfEntertain = expdSelfEntertain;
	}

	public double getExpdSelfFestiv() {
		return expdSelfFestiv;
	}

	public void setExpdSelfFestiv(double expdSelfFestiv) {
		this.expdSelfFestiv = expdSelfFestiv;
	}

	public double getExpdSelfGrocery() {
		return expdSelfGrocery;
	}

	public void setExpdSelfGrocery(double expdSelfGrocery) {
		this.expdSelfGrocery = expdSelfGrocery;
	}

	public double getExpdSelfInsurance() {
		return expdSelfInsurance;
	}

	public void setExpdSelfInsurance(double expdSelfInsurance) {
		this.expdSelfInsurance = expdSelfInsurance;
	}

	public double getExpdSelfMedical() {
		return expdSelfMedical;
	}

	public void setExpdSelfMedical(double expdSelfMedical) {
		this.expdSelfMedical = expdSelfMedical;
	}

	public double getExpdSelfOthers() {
		return expdSelfOthers;
	}

	public void setExpdSelfOthers(double expdSelfOthers) {
		this.expdSelfOthers = expdSelfOthers;
	}

	public double getExpdSelfPersonal() {
		return expdSelfPersonal;
	}

	public void setExpdSelfPersonal(double expdSelfPersonal) {
		this.expdSelfPersonal = expdSelfPersonal;
	}

	public double getExpdSelfProploan() {
		return expdSelfProploan;
	}

	public void setExpdSelfProploan(double expdSelfProploan) {
		this.expdSelfProploan = expdSelfProploan;
	}

	public double getExpdSelfRent() {
		return expdSelfRent;
	}

	public void setExpdSelfRent(double expdSelfRent) {
		this.expdSelfRent = expdSelfRent;
	}

	public double getExpdSelfTaxes() {
		return expdSelfTaxes;
	}

	public void setExpdSelfTaxes(double expdSelfTaxes) {
		this.expdSelfTaxes = expdSelfTaxes;
	}

	public double getExpdSelfTransport() {
		return expdSelfTransport;
	}

	public void setExpdSelfTransport(double expdSelfTransport) {
		this.expdSelfTransport = expdSelfTransport;
	}

	public double getExpdSelfUtil() {
		return expdSelfUtil;
	}

	public void setExpdSelfUtil(double expdSelfUtil) {
		this.expdSelfUtil = expdSelfUtil;
	}

	public double getExpdSelfVacations() {
		return expdSelfVacations;
	}

	public void setExpdSelfVacations(double expdSelfVacations) {
		this.expdSelfVacations = expdSelfVacations;
	}

	public double getExpdSelfVehiloan() {
		return expdSelfVehiloan;
	}

	public void setExpdSelfVehiloan(double expdSelfVehiloan) {
		this.expdSelfVehiloan = expdSelfVehiloan;
	}

	public double getExpdSpsCharity() {
		return expdSpsCharity;
	}

	public void setExpdSpsCharity(double expdSpsCharity) {
		this.expdSpsCharity = expdSpsCharity;
	}

	public double getExpdSpsClothing() {
		return expdSpsClothing;
	}

	public void setExpdSpsClothing(double expdSpsClothing) {
		this.expdSpsClothing = expdSpsClothing;
	}

	public double getExpdSpsDepntcontr() {
		return expdSpsDepntcontr;
	}

	public void setExpdSpsDepntcontr(double expdSpsDepntcontr) {
		this.expdSpsDepntcontr = expdSpsDepntcontr;
	}

	public double getExpdSpsEatingout() {
		return expdSpsEatingout;
	}

	public void setExpdSpsEatingout(double expdSpsEatingout) {
		this.expdSpsEatingout = expdSpsEatingout;
	}

	public double getExpdSpsEntertain() {
		return expdSpsEntertain;
	}

	public void setExpdSpsEntertain(double expdSpsEntertain) {
		this.expdSpsEntertain = expdSpsEntertain;
	}

	public double getExpdSpsFestiv() {
		return expdSpsFestiv;
	}

	public void setExpdSpsFestiv(double expdSpsFestiv) {
		this.expdSpsFestiv = expdSpsFestiv;
	}

	public double getExpdSpsGrocery() {
		return expdSpsGrocery;
	}

	public void setExpdSpsGrocery(double expdSpsGrocery) {
		this.expdSpsGrocery = expdSpsGrocery;
	}

	public double getExpdSpsInsurance() {
		return expdSpsInsurance;
	}

	public void setExpdSpsInsurance(double expdSpsInsurance) {
		this.expdSpsInsurance = expdSpsInsurance;
	}

	public double getExpdSpsMedical() {
		return expdSpsMedical;
	}

	public void setExpdSpsMedical(double expdSpsMedical) {
		this.expdSpsMedical = expdSpsMedical;
	}

	public double getExpdSpsOthers() {
		return expdSpsOthers;
	}

	public void setExpdSpsOthers(double expdSpsOthers) {
		this.expdSpsOthers = expdSpsOthers;
	}

	public double getExpdSpsPersonal() {
		return expdSpsPersonal;
	}

	public void setExpdSpsPersonal(double expdSpsPersonal) {
		this.expdSpsPersonal = expdSpsPersonal;
	}

	public double getExpdSpsProploan() {
		return expdSpsProploan;
	}

	public void setExpdSpsProploan(double expdSpsProploan) {
		this.expdSpsProploan = expdSpsProploan;
	}

	public double getExpdSpsRent() {
		return expdSpsRent;
	}

	public void setExpdSpsRent(double expdSpsRent) {
		this.expdSpsRent = expdSpsRent;
	}

	public double getExpdSpsTaxes() {
		return expdSpsTaxes;
	}

	public void setExpdSpsTaxes(double expdSpsTaxes) {
		this.expdSpsTaxes = expdSpsTaxes;
	}

	public double getExpdSpsTransport() {
		return expdSpsTransport;
	}

	public void setExpdSpsTransport(double expdSpsTransport) {
		this.expdSpsTransport = expdSpsTransport;
	}

	public double getExpdSpsUtil() {
		return expdSpsUtil;
	}

	public void setExpdSpsUtil(double expdSpsUtil) {
		this.expdSpsUtil = expdSpsUtil;
	}

	public double getExpdSpsVacations() {
		return expdSpsVacations;
	}

	public void setExpdSpsVacations(double expdSpsVacations) {
		this.expdSpsVacations = expdSpsVacations;
	}

	public double getExpdSpsVehiloan() {
		return expdSpsVehiloan;
	}

	public void setExpdSpsVehiloan(double expdSpsVehiloan) {
		this.expdSpsVehiloan = expdSpsVehiloan;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

}
