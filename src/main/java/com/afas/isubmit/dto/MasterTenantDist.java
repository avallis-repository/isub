package com.afas.isubmit.dto;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import com.afas.isubmit.util.PsgConst;

@Entity
@Table(name = "MASTER_TENANT_DIST")
public class MasterTenantDist implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "masterTenantDist", strategy = "com.afas.isubmit.util.StringSequenceIdentifier", parameters = {
			@Parameter(name = "sequence_name", value = PsgConst.SCHEMA_NAME + ".tnt_sequence"),
			@Parameter(name = "sequence_prefix", value = "TNT") })
	@GeneratedValue(generator = "masterTenantDist", strategy = GenerationType.SEQUENCE)
	@Column(name = "TENANT_ID")
	private String tenantId;

	@Column(name = "MNEMONIC")
	private String mnemonic;

	@Column(name = "TENANT_NAME")
	private String tenantName;

	@Column(name = "ADDR1")
	private String address1;

	@Column(name = "ADDR2")
	private String address2;

	@Column(name = "ADDR3")
	private String address3;

	@Column(name = "CITY")
	private String city;

	@Column(name = "STATE")
	private String state;

	@Column(name = "COUNTRY")
	private String country;

	@Column(name = "PINCODE")
	private String pinCode;

	@Column(name = "TELEPHONE_OFF")
	private String telephoneOff;

	@Column(name = "TELEPHONE_RES")
	private String telephoneRes;

	@Column(name = "FAX_NO")
	private String faxNo;

	@Column(name = "EMAIL_ID")
	private String emailId;

	@Column(name = "CONTACT_REF")
	private String contactRef;

	@Column(name = "REGISTRATION_NO")
	private String registrationNo;

	@Column(name = "TYPE")
	private String type;

	@Column(name = "PARENT_YN")
	private Boolean parent;

	@Column(name = "PARENT_NAME")
	private String parentName;

	@Column(name = "PREVIOUS_NAME")
	private String previousName;

	@Column(name = "NAME_PERIOD_FROM")
	private Date namePeriodFrom;

	@Column(name = "NAME_PERIOD_TO")
	private Date namePeriodTo;

	@Column(name = "LOGIN_APPEAR")
	private Boolean loginAppear;

	@Column(name = "GST_FLG")
	private Boolean GST;

	@Column(name = "WEBSITE")
	private String website;

	@Column(name = "CREATED_DATE")
	private Date createdDate;

	@Column(name = "MODIFIED_DATE")
	private Date modifiedDate;

	@Column(name = "CREATED_BY")
	private String createdBy;

	@Column(name = "MODIFIED_BY")
	private String modifiedBy;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getMnemonic() {
		return mnemonic;
	}

	public void setMnemonic(String mnemonic) {
		this.mnemonic = mnemonic;
	}

	public String getTenantName() {
		return tenantName;
	}

	public void setTenantName(String tenantName) {
		this.tenantName = tenantName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPinCode() {
		return pinCode;
	}

	public void setPinCode(String pinCode) {
		this.pinCode = pinCode;
	}

	public String getTelephoneOff() {
		return telephoneOff;
	}

	public void setTelephoneOff(String telephoneOff) {
		this.telephoneOff = telephoneOff;
	}

	public String getTelephoneRes() {
		return telephoneRes;
	}

	public void setTelephoneRes(String telephoneRes) {
		this.telephoneRes = telephoneRes;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getContactRef() {
		return contactRef;
	}

	public void setContactRef(String contactRef) {
		this.contactRef = contactRef;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Boolean getParent() {
		return parent;
	}

	public void setParent(Boolean parent) {
		this.parent = parent;
	}

	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getPreviousName() {
		return previousName;
	}

	public void setPreviousName(String previousName) {
		this.previousName = previousName;
	}

	public Date getNamePeriodFrom() {
		return namePeriodFrom;
	}

	public void setNamePeriodFrom(Date namePeriodFrom) {
		this.namePeriodFrom = namePeriodFrom;
	}

	public Date getNamePeriodTo() {
		return namePeriodTo;
	}

	public void setNamePeriodTo(Date namePeriodTo) {
		this.namePeriodTo = namePeriodTo;
	}

	public Boolean getLoginAppear() {
		return loginAppear;
	}

	public void setLoginAppear(Boolean loginAppear) {
		this.loginAppear = loginAppear;
	}

	public Boolean getGST() {
		return GST;
	}

	public void setGST(Boolean gST) {
		GST = gST;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

}
