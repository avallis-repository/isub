package com.afas.isubmit.dto;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "FNA_DISCLOSURE_CHKLIST")
public class FnaDisclosureChklist {
	@Id
	@Column(name = "DISCL_ID", nullable = false)
	private String disclId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "CL_ID")
	private String clId;

	@Column(name = "CL_FLAG")
	private String clFlag;

	@Column(name = "DISCL_CRTD_BY")
	private String disclCreatedBy;

	@Column(name = "DISCL_CRTD_DATE")
	private Date disclCreatedDate;

	@Column(name = "DISCL_MOD_BY")
	private String disclModBy;

	@Column(name = "DISCL_MOD_DATE")
	private Date disclModDate;

	public String getDisclId() {
		return disclId;
	}

	public void setDisclId(String disclId) {
		this.disclId = disclId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getClId() {
		return clId;
	}

	public void setClId(String clId) {
		this.clId = clId;
	}

	public String getClFlag() {
		return clFlag;
	}

	public void setClFlag(String clFlag) {
		this.clFlag = clFlag;
	}

	public String getDisclCreatedBy() {
		return disclCreatedBy;
	}

	public void setDisclCreatedBy(String disclCreatedBy) {
		this.disclCreatedBy = disclCreatedBy;
	}

	public Date getDisclCreatedDate() {
		return disclCreatedDate;
	}

	public void setDisclCreatedDate(Date disclCreatedDate) {
		this.disclCreatedDate = disclCreatedDate;
	}

	public String getDisclModBy() {
		return disclModBy;
	}

	public void setDisclModBy(String disclModBy) {
		this.disclModBy = disclModBy;
	}

	public Date getDisclModDate() {
		return disclModDate;
	}

	public void setDisclModDate(Date disclModDate) {
		this.disclModDate = disclModDate;
	}

}
