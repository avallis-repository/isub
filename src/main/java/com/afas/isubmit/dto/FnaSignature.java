package com.afas.isubmit.dto;

import java.sql.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.json.simple.JSONObject;

import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

@Entity
@Table(name = "FNA_ESIGNATURE")
@TypeDef(name = "JsonType", typeClass = JsonBinaryType.class)
public class FnaSignature {

	@Id
	@GeneratedValue
	@Column(name = "ESIGN_ID")
	private UUID esignId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "FNA_ID")
	private FnaDetails fnaDetails;

	@Column(name = "SIGN_PERSON")
	private String signPerson;

	@Type(type = "JsonType")
	@Column(columnDefinition = "json", name = "ESIGN")
	private JSONObject eSign;

	@Column(name = "SIGN_DOC_BLOB")
	private byte[] signDocBlob;

	@Column(name = "SIGN_DATE")
	private Date signDate;

	@Column(name = "PAGE_SEC_REF")
	private String pageSecRef;

	public UUID getEsignId() {
		return esignId;
	}

	public void setEsignId(UUID esignId) {
		this.esignId = esignId;
	}

	public FnaDetails getFnaDetails() {
		return fnaDetails;
	}

	public void setFnaDetails(FnaDetails fnaDetails) {
		this.fnaDetails = fnaDetails;
	}

	public String getSignPerson() {
		return signPerson;
	}

	public void setSignPerson(String signPerson) {
		this.signPerson = signPerson;
	}

	public JSONObject geteSign() {
		return eSign;
	}

	public void seteSign(JSONObject eSign) {
		this.eSign = eSign;
	}

	public byte[] getSignDocBlob() {
		return signDocBlob;
	}

	public void setSignDocBlob(byte[] signDocBlob) {
		this.signDocBlob = signDocBlob;
	}

	public Date getSignDate() {
		return signDate;
	}

	public void setSignDate(Date signDate) {
		this.signDate = signDate;
	}

	public String getPageSecRef() {
		return pageSecRef;
	}

	public void setPageSecRef(String pageSecRef) {
		this.pageSecRef = pageSecRef;
	}

}
