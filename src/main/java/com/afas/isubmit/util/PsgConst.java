package com.afas.isubmit.util;

public class PsgConst {
	public final static String SCHEMA_NAME = "psgisub_schema";

	// where condition Column name
	public final static String FNA_ID = "fna_id";
	public final static String ESIGN_ID = "esign_id";
	public final static String DATAFORM_ID = "dataform_id";
	public final static String DEPN_ID = "depn_id";
	public final static String CUST_ID = "cust_id";
	public final static String EXPD_ID = "expd_id";
	public final static String TAX_ID = "tax_id";
	public final static String PRIN_NAME = "prin_name";
	public final static String PRIN_ID = "prin_id";
	public final static String SIGN_PERSON = "sign_person";
	public final static String RECOM_PP_PRODNAME = "recomPpProdname";
	public final static String RECOM_BASIC_REF = "recomPpBasicRef";
	public final static String RECOM_PP_ID = "recomPpId";
	public final static String RECOM_PP_NAME = "recomPpName";
	public final static String RECOM_PP_PRIN = "recomPpPrin";
	public final static String MGR_APPROVE_STATUS = "MGR_APPROVE_STATUS";
	public final static String ADMIN_APPROVE_STATUS = "ADMIN_APPROVE_STATUS";
	public final static String COMP_APPROVE_STATUS = "COMP_APPROVE_STATUS";
	public final static String NRIC = "NRIC";
	public final static String TAX_FOR = "TAX_FOR";

	// Hard coded value
	public final static String FNA_TYPE = "SIMPLIFIED";
	public final static String NO_VALUE = "N";
	public final static String YES_VALUE = "Y";
	public final static String SIGN_CLIENT = "CLIENT";
	public final static String SIGN_SPS = "SPS";
	public final static String SIGN_ADVISER = "ADVISER";
	public final static String SIGN_MANAGER = "MANAGER";
	public final static String ACCESS_ROLE = "ACCESSROLE";
	public final static String ROLE_MANAGER = "MANAGER";
	public final static String ROLE_ADMIN = "ADMIN";
	public final static String ROLE_COMP = "COMP";
	public final static String CLIENT_UPLOADS = "Client_Uploads";
	public final static String FIRM_CODE = "FIRM001";

	// Sequence Id Prefix
	public final static String FNAID_PREFIX = "FNA";
	public final static String CUSTID_PREFIX = "CUST";
	public final static String DATID_PREFIX = "DAT";
	public final static int idLen = 20;

	// Session Id's
	public final static String SESS_CUST_ID = "CustomerId";
	public final static String SESS_FNA_ID = "FnaId";
	public final static String SESS_DATAFORM_ID = "DataFormId";
	public final static String CLIENT_STATUS = "ClientStatus";
	public final static String CLIENT_ADD = "Add";
	public final static String CLIENT_EDIT = "Edit";
	public final static String SESS_EXPD_ID = "ExpenditureId";
	public final static String SESS_FNA_DATA = "SessFnaData";
	public final static String SESS_CLIENT_NAME = "ClientName";
	public final static String SESS_SPOUSE_NAME = "SpouseName";
	public final static String SESS_ADVISER_NAME = "AdviserName";
	public final static String SESS_MANAGER_NAME = "ManagerName";

	// Class name
	public final static String CUSTOMER_DETAILS = "CustomerDetails";
	public final static String CUSTOMER_ATTACHMENTS = "CustomerAttachments";
	public final static String FNA_DETAILS = "FnaDetails";
	public final static String FNA_SELF_SPOUSE_DETS = "FnaSelfSpouseDets";
	public final static String FNA_DEPENDANT_DETS = "FnaDependantDets";
	public final static String FNA_EXPENDITURE_DETS = "FnaExpenditureDets";
	public final static String FNA_OTHPER_DETS = "FnaOtherPersonDets";
	public final static String FNA_FATCA_TAX_DETS = "FnaFatcaTaxDet";
	public final static String MASTER_PRINCIPAL = "MasterPrincipal";
	public final static String MASTER_PRODUCT = "MasterProduct";
	public final static String FNA_RECOMM_PRDT_DETS = "FnaRecomPrdtPlanDet";
	public final static String FNA_SIGNATURE = "FnaSignature";
	public final static String MASTER_ATTACH_CATEG = "MasterAttachCategory";
	public final static String MASTER_CUST_STATUS = "MasterCustomerStatus";

	// Url name
	public final static String BACK_TO_URL = "BackToUrl";
	public final static String CLIENT_INFO = "clientInfo";
	public final static String KYC_INTRO = "kycIntro";
	public final static String KYC_HOME = "kycHome";
	public final static String PRODUCT_RECOMMEND = "productRecommend";
	public final static String FIN_WELL_REVIEW = "finanWellReview";
	public final static String ADVICE_BASIS_RECOMM = "adviceBasisRecomm";
	public final static String PRODUCT_SWITCH_RECOMM = "productSwitchRecomm";
	public static final String CLIENT_SIGNATURE = "clientSignature";
	public static final String MANAGER_DECLARATION = "managerDeclaration";
	public static final String ADVISOR_DECLARATION = "advisorDeclaration";
	public static final String CLIENT_DECLARATION = "clientDeclaration";
	public static final String TAX_RESIDENCY = "taxResidency";
	public static final String CUST_NAME = "CustomerName";
	public static final String CURRENT_SCREEN = "CURRENT_SCREEN";
	public static final String NEXT_SCREEN = "NEXT_SCREEN";
	public static final String PREV_SCREEN = "PREV_SCREEN";

	// Common variable
	public final static String STR_NULL_STRING = "";
	public final static String FINISHED = "FINISHED";

	public final static String GEN_FEMALE = "F";
	public final static String GEN_MALE = "M";

	public final static String CUST_STS_NAME = "customerStatusName";
	public final static String ASC_ORDER = "asc";
	
	public final static String TENANT_ID = "tenant_id";
	public final static String MASTER_TENANT_DIST = "MasterTenantDist";
	
	public final static String ADVSTF_ID = "advstf_id";
	public final static String MASTER_ADVISER = "MasterAdviser";
	
	public final static String MANAGER_FLG = "manager";
	
	public final static String PRINCIPAL_INFO = "PrincipalInfo";
	public final static String REGISTRATION_NO = "REGISTRATION_NO"; 
	public final static String USER_LOGIN = "UserLogin";
	public final static String PRINC_INFO_ID = "princ_info_id";
}
