package com.afas.isubmit.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.afas.isubmit.restservice.FnaOtherPersonDetRestService;
import com.google.api.gax.paging.Page;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.ReadChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class AESAlgorithm {

	private static Logger logger = LogManager.getLogger(FnaOtherPersonDetRestService.class);

	private static SecretKeySpec secretKey;
	private static byte[] key;

	public static void setKey(String myKey) {
		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			logger.info("key: " + key);
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");
			logger.info("secretKey: " + secretKey);
			byte[] byteKey = secretKey.getEncoded();
			String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
			StringBuilder result = new StringBuilder();
			for (byte b : byteKey) {
				result.append(String.format("%02x", b));
			}

			logger.info("key--" + encodedKey);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public static void encrypt(String secret, String inFileName, InputStream inFile) {
		String secKey = "L3MFnOprYd+JU8HEwrpEyw==";
		try {
			setKey(secret);

			byte[] decodedKey = Base64.getDecoder().decode(secKey);
			// rebuild key using SecretKeySpec
			SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
			// MultipartFile inFileMM = (MultipartFile) new File("D:\\sample123.pdf");
			// InputStream inFile=inFileMM.getInputStream();
			// FileInputStream inFile = new FileInputStream("D:\\"+inFileName);
			// ByteArrayInputStream inFile =(ByteArrayInputStream) inStream ;
			FileOutputStream outFile = new FileOutputStream("D:\\encryptedfile" + inFileName + ".tmp");
			// file encryption
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, originalKey);

			byte[] input = new byte[64];
			int bytesRead;
			ByteArrayOutputStream bOutput = new ByteArrayOutputStream();
			while ((bytesRead = inFile.read(input)) != -1) {
				byte[] output = cipher.update(input, 0, bytesRead);
				if (output != null)
					bOutput.write(output);
			}

			byte[] output = cipher.doFinal();
			if (output != null)
				bOutput.write(output);

			outFile.write(bOutput.toByteArray());
			inFile.close();
			outFile.flush();
			outFile.close();

			logger.info("File Encrypted.");

			/*
			 * Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			 * cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			 */
			// return
			// Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes("UTF-8")));
		} catch (Exception e) {
			logger.error("Error while encrypting: " + e.toString());
		}
		// return null;
		decrypt(secKey, inFileName);
	}

	public static void decrypt(String secret, String inFileName) {
		try {
			String secKey = "L3MFnOprYd+JU8HEwrpEyw==";
			byte[] decodedKey = Base64.getDecoder().decode(secKey);
			// rebuild key using SecretKeySpec
			SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");

			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, originalKey);
			FileInputStream fis = new FileInputStream("D:\\encryptedfile" + inFileName + ".tmp");
			FileOutputStream fos = new FileOutputStream("D:\\encryptedfile" + inFileName + ".pdf");
			byte[] in = new byte[64];
			int read;
			while ((read = fis.read(in)) != -1) {
				byte[] output = cipher.update(in, 0, read);
				if (output != null)
					fos.write(output);
			}

			byte[] output = cipher.doFinal();
			if (output != null)
				fos.write(output);
			fis.close();
			fos.flush();
			fos.close();
			logger.info("File Decrypted.");

		} catch (Exception e) {
			logger.error("Error while decrypting: " + e.toString());
		}
	}

	public static void decrypt(byte[] bytes) {
		try {
			String secKey = "JNNysozYSn6Qqn081he1xg==";
			byte[] decodedKey = Base64.getDecoder().decode(secKey);
			// rebuild key using SecretKeySpec
			SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");

			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, originalKey);
			// FileInputStream fis = new FileInputStream("encryptedfile.tmp");
			ByteArrayInputStream fis = new ByteArrayInputStream(bytes);
			FileOutputStream fos = new FileOutputStream("d://download//plainfile_decrypted123.pdf");
			byte[] in = new byte[64];
			int read;
			while ((read = fis.read(in)) != -1) {
				byte[] output = cipher.update(in, 0, read);
				if (output != null)
					fos.write(output);
			}

			byte[] output = cipher.doFinal();
			if (output != null)
				fos.write(output);
			fis.close();
			fos.flush();
			fos.close();
			logger.info("File Decrypted.");

		} catch (Exception e) {
			logger.error("Error while decrypting: " + e.toString());
		}
	}

	public static void downloadGcp() throws FileNotFoundException, IOException {
		// Download all list
		String bucketName = "psg-sg";
		String objectName = "Client_Uploads/FIRM001/CUST0000000000000191/sample123.tmp";
		Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("D:\\psg-sg-pure-storage.json"));
		Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
		// Storage storage =
		// StorageOptions.newBuilder().setProjectId(projectId).build().getService();
		Bucket bucket = storage.get(bucketName);
		Page<Blob> blobs = bucket.list();

		for (Blob blob : blobs.iterateAll()) {
			logger.info(blob.getName());
		}

		// Download object
		// Storage storage =
		// StorageOptions.newBuilder().setProjectId(projectId).build().getService();
		String destFilePath = "d://download//";
		Blob blob = storage.get(BlobId.of(bucketName, objectName));
		// blob.downloadTo(Paths.get(destFilePath));

		// FileOutputStream outStream=new FileOutputStream("d://outfile.tmp");
		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		try (ReadChannel reader = blob.reader()) {
			ByteBuffer bytes = ByteBuffer.allocate(blob.getSize().intValue());
			while (reader.read(bytes) > 0) {
				outStream.write(bytes.array());
			}
		}
		outStream.flush();
		outStream.close();

		decrypt(outStream.toByteArray());
		logger.info("Downloaded object " + objectName + " from bucket name " + bucketName + " to " + destFilePath);
	}

//    public static void main(String[] args) throws FileNotFoundException, IOException {
//    	final String secretKey = "ssshhhhhhhhhhh!!!!";
//    	downloadGcp();
	// String originalString = "howtodoinjava.com";
	// AESAlgorithm.encrypt(secretKey) ;
	// AESAlgorithm.decrypt(secretKey) ;

	// System.out.println(originalString);
	// System.out.println(encryptedString);
	// System.out.println(decryptedString);
//    }
}
