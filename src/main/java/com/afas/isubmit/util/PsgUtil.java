package com.afas.isubmit.util;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.afas.isubmit.dto.FnaSelfSpouseDets;

public class PsgUtil {
	
	private static Logger logger = LogManager.getLogger(PsgUtil.class);

	@Autowired
	private static Environment env;

//	static Logger log=Logger.getLogger(PsgUtil.class);
	public static List<String> compareTwoObj(Object newObj, Object oldObj)
			throws IllegalAccessException, InvocationTargetException, NoSuchMethodException {
		if (newObj.getClass() != oldObj.getClass()) {
			throw new IllegalArgumentException("The beans must be from the same Class!");
		}
		List<String> changesFieldValue = new ArrayList<>();
		Iterator<String> keysIt = BeanUtils.describe(newObj).keySet().iterator();
		while (keysIt.hasNext()) {
			String key = (String) keysIt.next();
			Object oldValue = PropertyUtils.getProperty(oldObj, key);
			Object newValue = PropertyUtils.getProperty(newObj, key);
			logger.info("Old value---" + oldValue + "--New value---" + newValue);
			if (oldValue != newValue) {
				if (((oldValue != null) && !oldValue.equals(newValue))
						|| ((newValue != null) && !newValue.equals(oldValue))) {
					changesFieldValue.add(key);
				}
			}
		}
		return changesFieldValue;
	}

	public static void setUserSessData(FnaSelfSpouseDets selfSpouse, HttpServletRequest request) {

		request.getSession(false).setAttribute(PsgConst.SESS_CLIENT_NAME, selfSpouse.getDfSelfName());
		String spsName = selfSpouse.getDfSpsName();
		if (spsName != null && !spsName.isEmpty()) {
			request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, spsName);
		} else {
			request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, null);
		}

//		request.getSession(false).setAttribute(PsgConst.SESS_ADVISER_NAME, "Test");
//		request.getSession(false).setAttribute(PsgConst.SESS_MANAGER_NAME, "Salim");
	}

	public static String getParamValue(HttpServletRequest request, String param) {
		return ((request.getParameter(param) != PsgConst.STR_NULL_STRING) ? request.getParameter(param)
				: PsgConst.STR_NULL_STRING);
	}

	public static boolean isValidUSession(HttpServletRequest requestObj) {

		HttpSession sessObj = requestObj.getSession(false);
		if (nullObj(sessObj)) {
//		    log.info("---------->SESSSION HAS BEEN EXPIRED");
			return false;
		} else {
			return true;
		}
	}

	public static boolean nullObj(Object obj) {
		return ((obj == null));
	}

	public static boolean emptyOrNull(String value) {
		if (value != null && !value.isEmpty()) {
			return true;
		}
		return false;
	}

	public static boolean dateEmptyOrNull(Date value) {
		if (value != null) {
			return true;
		}
		return false;
	}

	public static String getSchemaName() {
		Properties prop = new Properties();

		prop.setProperty("schema", env.getProperty("spring.jpa.properties.hibernate.default_schema"));
		String schema = prop.getProperty("schema");
		return schema;
	}

	public static Date getDateForamt(Date dob) throws ParseException {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy 21:30:ss");
		String dateFormat = dateFormatter.format(dob);

		SimpleDateFormat dateParser = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		java.util.Date date = (java.util.Date) dateParser.parse(dateFormat);
		return date;
	}

	public static String emptyValue(Object value) {
		return value == null ? "" : value.toString();
	}

}
