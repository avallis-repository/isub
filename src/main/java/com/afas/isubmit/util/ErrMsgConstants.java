package com.afas.isubmit.util;

public class ErrMsgConstants {
	public static final String DLT_MSG = "Successfully deleted your data";
	public static final String SUCCESS_MSG = "Successfully saved your data";
	public static final String UPDATE_MSG = "Successfully updated your data";
}
