package com.afas.isubmit.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

//import com.afas.isubmit.util.SessionUtil;

public abstract class AbstractGenericDao<T> {

	@Autowired
	SessionFactory sessionFactory;
//	SessionUtil sessionUtil = new SessionUtil()

	protected Session getCurrentSession() {
		return sessionFactory.openSession();
	}

//	private Class<T> entityClass;
//	private String className;

	public AbstractGenericDao() {

//		  this.entityClass = (Class<T>) ((ParameterizedType)
//		  this.getClass().getGenericSuperclass()) .getActualTypeArguments()[0];
//		  className = this.entityClass.getSimpleName();
//		  System.out.println("Class name---"+className);

	}

	public Object saveData(T entity) {
		Session session = null;
		Transaction tx = null;
		Object idObj =null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
			idObj = session.save(entity);
		} catch (Exception ex) {
//			if(tx!=null) {
//				tx.rollback();
//			}
			throw ex;
		} finally {
			session.flush();
			session.clear();
			tx.commit();
			session.close();
		}
		return idObj;
	}

	@SuppressWarnings("unchecked")
	public List<T> getAllData(String className) {
		Session session = getCurrentSession();
		List<T> data = session.createQuery("from " + className).list();
		session.close();
		return data;
	}

	@SuppressWarnings("unchecked")
	public List<T> getAllDataById(String colName, String id, String className) {
		Session session = getCurrentSession();
		Query<T> query = session.createQuery("from " + className + " where " + colName + "= :id");
		query.setParameter("id", id);

		List<T> data = query.list();

		session.close();
		return data;
	}
	
	@SuppressWarnings("unchecked")
	public List<T> getAllDataByIdnotequal(String colName, String id, String className) {
		Session session = getCurrentSession();
		Query<T> query = session.createQuery("from " + className + " where " + colName + "!= :id");
		query.setParameter("id", id);

		List<T> data = query.list();

		session.close();
		return data;
	}

	public void update(T entity) {

		Session session = null;
		Transaction tx = null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
			session.saveOrUpdate(entity);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw ex;
		} finally {
			tx.commit();
			session.close();
		}

	}

	public void delete(String id, String colName, String className) {
		delete(id, colName, className, null, null);
	}

	@SuppressWarnings("unchecked")
	public void delete(String id, String colName, String className, String id2, String colName2) {

		Session session = null;
		Transaction tx = null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
			String strQuery = "DELETE FROM " + className + " WHERE " + colName + "=:id";
			if (colName2 != null && !colName2.isEmpty() && id2 != null && !id2.isEmpty()) {
				strQuery += " and " + colName2 + "=:id2";
			}
			Query<T> query = session.createQuery(strQuery);
			query.setParameter("id", id);
			if (colName2 != null && !colName2.isEmpty() && id2 != null && !id2.isEmpty()) {
				query.setParameter("id2", id2);
			}
			query.executeUpdate();
//			tx = session.getTransaction();

		} catch (Exception ex) {
//			if(tx!=null) {
//				tx.rollback();
//			}
			throw ex;
		} finally {
			tx.commit();
//			session.clear();
//			session.flush();
			session.close();
		}

	}

	@SuppressWarnings("unchecked")
	public T getDataById(String id, String colName, String className) {
		Session session = getCurrentSession();
		Query<T> query = session.createQuery("from " + className + " where " + colName + "= :id");
		query.setParameter("id", id);
		T fnaData = query.uniqueResult();
		session.close();
		return fnaData;
	}

	@SuppressWarnings("unchecked")
	public T getDataByCol(String val1, String colName1, String val2, String colName2, String className) {
		Session session = getCurrentSession();
		Query<T> query = session
				.createQuery("from " + className + " where " + colName1 + "= :col1 and " + colName2 + "= :col2");
		query.setParameter("col1", val1);
		query.setParameter("col2", val2);
		T fnaData = query.uniqueResult();
		session.close();
		return fnaData;
	}

	@SuppressWarnings("unchecked")
	public List<T> getAllDataByCol(String val1, String colName1, String val2, String colName2, String className) {
		Session session = getCurrentSession();
		Query<T> query = session
				.createQuery("from " + className + " where " + colName1 + "= :col1 and " + colName2 + "= :col2");
		query.setParameter("col1", val1);
		query.setParameter("col2", val2);
		List<T> data = query.list();
		session.close();
		return data;
	}

	@SuppressWarnings("unchecked")
	public Long getAllDataCount(String className) {
		Session session = getCurrentSession();
		Query<T> query = session.createQuery("SELECT COUNT(*) FROM " + className);
		Long count = (Long) query.uniqueResult();
		session.close();
		return count;
	}

	@SuppressWarnings("unchecked")
	public List<Object> getParticularColDataByOrderByCol(String className, String colName, String orderColName,
			String orderType, int limit) {
		Session session = getCurrentSession();
		Query<T> query = session.createQuery(
				"SELECT " + colName + " FROM " + className + " ORDER BY " + orderColName + " " + orderType);
		if (limit > 0) {
			query.setMaxResults(limit);
		}
		List<Object> data = (List<Object>) query.list();
		session.close();
		return data;
	}

	@SuppressWarnings("unchecked")
	public void deleteByOrderByCol(String className, String colName, String orderColName, String orderType, int limit) {
		Session session = null;
		Transaction tx = null;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
			List<Object> objects = getParticularColDataByOrderByCol(className, colName, orderColName, orderType, limit);
			if (objects != null && objects.size() > 0) {
				for (Object object : objects) {
					Query<T> query = session.createQuery("DELETE FROM " + className + " WHERE " + colName + "=:id");
					query.setParameter("id", object);
					if (limit > 0) {
						query.setMaxResults(limit);
					}
					query.executeUpdate();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			tx.commit();
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> getAllDataByOrderByCol(String className, String orderColName, String orderType) {
		Session session = getCurrentSession();
		Query<T> query = session.createQuery("FROM " + className + " ORDER BY " + orderColName + " " + orderType);
		List<T> data = (List<T>) query.list();
		session.close();
		return data;
	}

	@SuppressWarnings("unchecked")
	public boolean deleteAllDetailsByFnaId(String fnaId) {
		Session session = null;
		Transaction tx = null;
		boolean isDeleted = false;
		try {
			session = getCurrentSession();
			tx = session.beginTransaction();
//			StoredProcedureQuery spQuery = session.createStoredProcedureCall("psgisub_schema.deleteByFnaId");
//			spQuery.registerStoredProcedureParameter(1, String.class, ParameterMode.IN);
//			spQuery.setParameter(1, fnaId);
//			spQuery.executeUpdate();

			Query<T> query = session.createSQLQuery("CALL psgisub_schema.deleteByFnaId(:fnaId)");
			query.setParameter("fnaId", fnaId);
			query.executeUpdate();
			isDeleted = true;
		} catch (Exception e) {
			e.printStackTrace();
			return isDeleted;
		} finally {
			tx.commit();
			session.close();
		}
		return isDeleted;
	}

	public List<T> getAllDataBySrchStr(Class<T> clazz, String colName, String searchStr) {
		Session session = getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<T> cQuery = builder.createQuery(clazz);
		Root<T> root = cQuery.from(clazz);
		cQuery.select(root).where(builder.like(builder.lower(root.get(colName)), "%" + searchStr.toLowerCase() + "%"));

		Query<T> query = session.createQuery(cQuery);
		List<T> data = query.getResultList();

		session.close();
		return data;
	}
	
	public List<Object> getParticularColData(Class<T> rootClass, String colName, String classType, String condition1, String value1) {
		Session session = getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Object> cQuery = builder.createQuery(Object.class);
		Root<T> root = cQuery.from(rootClass);
		Path<Object> path = null;
		if (classType != null && !classType.isEmpty()) {
			path = root.get(classType).get(condition1);
		} else {
			path = root.get(condition1);
		}
		cQuery.select(root.get(colName)).where(builder.equal(path, value1));
		Query<Object> query = session.createQuery(cQuery);
		if (query != null && query.getResultList() != null && query.getResultList().size() > 0) {
			return query.getResultList();
		}
		return null;
	}
}
