package com.afas.isubmit.fileupload;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class GcpUpload {

	private static Logger logger = LogManager.getLogger(GcpUpload.class);

//	@Value("${gcp.json.file.path}")
//	static String gcpPath;

//	@Value("${gcp.bucketName}")
//	static String bucketName;

//	@Value("${gcp.projectId}")
//	static String projId;

//	private static Resource fileResource = new ClassPathResource("properties/psg-sg-pure-storage.json");
//	private static Resource fileResource = new ClassPathResource("properties/psg-tst-storage-admin.json");
//	Resource fileResource = new ClassPathResource(gcpPath);

//	private static ResourceBundle gcpProp = ResourceBundle.getBundle("properties/gcp");

//	private static String projId = gcpProp.getString(gcpProjectId);
//	private static String bucketName = gcpProp.getString(gcpBucketName);
	// static String password=gcpProp.getString("encryptPwd");

	private static SecretKeySpec secretKey;
	private static byte[] key;

	public static String generateKey(String password) {
		MessageDigest sha = null;
		String encodedKey = null;
		try {
			key = password.getBytes("UTF-8");
			logger.info("key: " + key);
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");

			encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return encodedKey;
	}

	public static void encryptData(String secret, InputStream inFile, String objectName, String inFileName,
			String projId, String bucketName, String gcpPath) {
		try {
			byte[] decodedKey = Base64.getDecoder().decode(secret);
			SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
			// FileOutputStream outFile = new
			// FileOutputStream("D:\\encryptedfile"+inFileName+".tmp");
			// file encryption
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, originalKey);

			byte[] input = new byte[64];
			int bytesRead;
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

			while ((bytesRead = inFile.read(input)) != -1) {
				byte[] output = cipher.update(input, 0, bytesRead);
				if (output != null)
					outputStream.write(output);
			}

			byte[] output = cipher.doFinal();
			if (output != null)
				outputStream.write(output);

			// outFile.write(outputStream.toByteArray());
			inFile.close();
			outputStream.flush();
			outputStream.close();

			// inFile.close();
			// outFile.flush();
			// outFile.close();

			logger.info("File Encrypted.");
			uploadGcp(projId, bucketName, objectName, outputStream.toByteArray(), gcpPath);

			// test decrypt
			// decrypt(inFileName);

		} catch (Exception e) {
			logger.error("Error while encrypting: " + e.toString());
			e.printStackTrace();
		}

	}

	public static void uploadGcp(String projectId, String bucketName, String objectName, byte[] fileData,
			String gcpPath) throws IOException {

		Resource fileResource = new ClassPathResource(gcpPath);

		Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(fileResource.getFile()));
		Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

		// byte[] fileData=getFileData(file);
//			Storage storage = StorageOptions.newBuilder().setProjectId(projectId)
//					.build().getService();
		BlobId blobId = BlobId.of(bucketName, objectName);
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
		storage.create(blobInfo, fileData);

		logger.info("File byte array " + fileData + " uploaded to bucket " + bucketName + " as " + objectName);
	}

}
