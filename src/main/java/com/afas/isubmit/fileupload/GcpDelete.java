package com.afas.isubmit.fileupload;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

@Service
public class GcpDelete {

//	private static Logger logger = LogManager.getLogger(GcpDownload.class);

//	@Value("${gcp.json.file.path}")
//	String gcpPath;

//	@Value("${gcp.bucketName}")
//	String bucketName;

//	private static Resource fileResource = new ClassPathResource("properties/psg-sg-pure-storage.json");
//	private static Resource fileResource = new ClassPathResource("properties/psg-tst-storage-admin.json");
//	Resource fileResource = new ClassPathResource(gcpPath);

//	static ResourceBundle gcpProp = ResourceBundle.getBundle("properties/gcp");

	public boolean deleteGcpFile(String objectName, String gcpPath, String bucketName)
			throws FileNotFoundException, IOException {
		Resource fileResource = new ClassPathResource(gcpPath);

		Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(fileResource.getFile()));
		Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

//		String bucketName = gcpProp.getString("bucketName");

		return storage.delete(bucketName, objectName);
	}

}
