package com.afas.isubmit.fileupload;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.afas.isubmit.util.PsgConst;
import com.google.api.gax.paging.Page;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.ReadChannel;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BucketInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

public class GcpDownload {

	private static Logger logger = LogManager.getLogger(GcpDownload.class);

//	@Value("${gcp.json.file.path}")
//	private static String gcpPath;

//	@Value("${gcp.bucketName}")
//	private static String bucketName;

//	private static Resource fileResource = new ClassPathResource("properties/psg-sg-pure-storage.json");
//	private static Resource fileResource = new ClassPathResource("properties/psg-tst-storage-admin.json");
//	private static Resource fileResource = new ClassPathResource(gcpPath);

//	private static ResourceBundle gcpProp = ResourceBundle.getBundle("properties/gcp");

	public static byte[] decrypt(String decryptKey, byte[] bytes) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		try {
			// String secKey="JNNysozYSn6Qqn081he1xg==";
			byte[] decodedKey = Base64.getDecoder().decode(decryptKey);
			// rebuild key using SecretKeySpec
			SecretKeySpec originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");

			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, originalKey);
			// FileInputStream fis = new FileInputStream("encryptedfile.tmp");
			ByteArrayInputStream fis = new ByteArrayInputStream(bytes);
			// FileOutputStream fos = new
			// FileOutputStream("d://download//plainfile123.pdf");

			byte[] in = new byte[64];
			int read;
			while ((read = fis.read(in)) != -1) {
				byte[] output = cipher.update(in, 0, read);
				if (output != null) {
					bos.write(output);
				}
			}

			byte[] output = cipher.doFinal();
			if (output != null) {
				bos.write(output);
			}
			fis.close();
			bos.flush();
			bos.close();
//    		fos.write(bos.toByteArray());
//    		fos.flush();
//    		fos.close();
			logger.info("File Decrypted.");

		} catch (Exception e) {
			logger.error("Error while decrypting: " + e.toString());
			e.printStackTrace();
		}

		return bos.toByteArray();
	}

	public static void listGCPObjects(String bucketName, Storage storage) {
		Page<Blob> blobs = storage.list(bucketName,
				Storage.BlobListOption.prefix(PsgConst.CLIENT_UPLOADS + "/" + PsgConst.FIRM_CODE + "/"));

		for (Blob blob : blobs.iterateAll()) {
			logger.info("bucket name: " + blob.getBucket());
			logger.info("blob name: " + blob.getName());
		}
	}

	/**
	 * One time use
	 * 
	 * @param bucketName
	 * @param storage
	 */
	public static void createNewBucket(String bucketName, Storage storage) {
		storage.create(BucketInfo.of(bucketName));
	}

	public static byte[] downloadGcp(String objectName, String decryptKey, String gcpPath, String bucketName)
			throws FileNotFoundException, IOException {

		Resource fileResource = new ClassPathResource(gcpPath);

		// Download all list
//		String bucketName = gcpProp.getString(gcpBucketName);

		Credentials credentials = GoogleCredentials.fromStream(new FileInputStream(fileResource.getFile()));
		Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();

//		try {
//			createNewBucket(bucketName, storage);
//		} catch (Exception e) {
//			logger.error("Error on creating bucket!");
//			e.printStackTrace();
//		}

//	    listGCPObjects(bucketName, storage);

		Blob blob = storage.get(BlobId.of(bucketName, objectName));

		ByteArrayOutputStream outStream = new ByteArrayOutputStream();
		try (ReadChannel reader = blob.reader()) {
			ByteBuffer bytes = ByteBuffer.allocate(blob.getSize().intValue());
			while (reader.read(bytes) > 0) {
				outStream.write(bytes.array());
			}
		}
		outStream.flush();
		outStream.close();

		byte[] decryptByte = decrypt(decryptKey, outStream.toByteArray());
		logger.info("Downloaded object " + objectName + " from bucket name " + bucketName + " to ");

		return decryptByte;
	}

}
