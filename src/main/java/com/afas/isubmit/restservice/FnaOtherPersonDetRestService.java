package com.afas.isubmit.restservice;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaOtherPersonDets;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaOtherPersonDetsService;
import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/FnaOtherPerDets")
public class FnaOtherPersonDetRestService {

	private static Logger logger = LogManager.getLogger(FnaOtherPersonDetRestService.class);

	@Autowired
	FnaOtherPersonDetsService othPerService;

	@Autowired
	FnaDetailsService fnaDetailsService;

	ObjectMapper mapper = new ObjectMapper();

	@PostMapping(value = "/saveData")
	public void saveOtherPerData(@RequestParam("benfDets") Object benfDets, @RequestParam("tppDets") Object tppDets,
			@RequestParam("pepRceDets") Object pepRceDets, HttpServletRequest request) throws JsonProcessingException {
		logger.info("coming saveOtherPerData");
		List<Object> arrObj = new ArrayList<Object>();
		arrObj.add(benfDets);
		arrObj.add(tppDets);
		arrObj.add(pepRceDets);
		FnaDetails fnaDtls = null;
		boolean benFlg = false, tppFlg = false, pepFlg = false;

		for (Object obj : arrObj) {
			try {
				FnaOtherPersonDets fnaOtherDets = mapper.readValue(obj.toString(), FnaOtherPersonDets.class);
				String othPerId = fnaOtherDets.getOthperId();
				if (fnaOtherDets.getOthperCateg() != null) {
					if (othPerId == null || (othPerId != null && othPerId.isEmpty())) {
						othPerService.saveData(fnaOtherDets);
					} else {
						othPerService.update(fnaOtherDets);
					}
					fnaDtls = fnaOtherDets.getFnaDetails();

					if ("BENF".equals(fnaOtherDets.getOthperCateg())) {
						benFlg = true;

					} else if ("TPP".equals(fnaOtherDets.getOthperCateg())) {
						tppFlg = true;

					} else if ("PEP".equals(fnaOtherDets.getOthperCateg())) {
						pepFlg = true;
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		if (fnaDtls != null) {
			if (benFlg) {
				fnaDtls.setCdBenfownFlg(PsgConst.YES_VALUE);
			}
			if (tppFlg) {
				fnaDtls.setCdTppFlg(PsgConst.YES_VALUE);
			}
			if (pepFlg) {
				fnaDtls.setCdPepFlg(PsgConst.YES_VALUE);
			}
			fnaDetailsService.update(fnaDtls);
		}
		// othPerService.saveData(fnaOthPer);
	}

	@GetMapping(value = "/getAllDataById/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaOtherPersonDets> getAllDataById(@PathVariable String fnaId, HttpServletRequest request) {
		return othPerService.getAllDataById(PsgConst.FNA_ID, fnaId, PsgConst.FNA_OTHPER_DETS);
	}

	public void save(List<Object> arrObj) {

	}

	@DeleteMapping("/deleteOthPerData/{fnaId}/{categ}")
	public String deleteOtherPersonData(@PathVariable String fnaId, @PathVariable String categ,
			HttpServletRequest request) {
		logger.info("fnaId: " + fnaId);
		logger.info("categ: " + categ);
		String isDeleted = "false";
		if (fnaId != null && !fnaId.isEmpty() && categ != null && !categ.isEmpty()) {
			othPerService.delete(fnaId, PsgConst.FNA_ID, PsgConst.FNA_OTHPER_DETS, categ, "othper_categ");
			FnaDetails fnaDtls = fnaDetailsService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
			if (fnaDtls != null) {
				if ("BENF".equals(categ)) {
					fnaDtls.setCdBenfownFlg(PsgConst.NO_VALUE);
				} else if ("TPP".equals(categ)) {
					fnaDtls.setCdTppFlg(PsgConst.NO_VALUE);
				} else if ("PEP".equals(categ)) {
					fnaDtls.setCdPepFlg(PsgConst.NO_VALUE);
				}
				fnaDetailsService.update(fnaDtls);
				isDeleted = "true";
			}
		}
		return isDeleted;
	}

}
