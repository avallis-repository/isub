package com.afas.isubmit.restservice;

import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
//import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.Email;
import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaSignature;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaSignatureService;
import com.afas.isubmit.service.MailService;
import com.afas.isubmit.util.PsgConst;
import com.afas.isubmit.util.QrCode;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.afas.isubmit.websocket.AutoRefreshHandler;

@RestController
@RequestMapping("/FnaSignature")
public class FnaSignatureRestService {
	
	private static Logger logger = LogManager.getLogger(FnaSignatureRestService.class);

	@Autowired
	FnaSignatureService signService;

	@Autowired
	FnaDetailsService fnaDetsService;

	@Autowired
	MailService mailService;
	
	ObjectMapper mapper = new ObjectMapper();

//	Logger log=Logger.getLogger(FnaSignatureRestService.class);

	@GetMapping(value = "/getAllData", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaSignature> getAllFnaSignature() {
		return signService.getAllData(PsgConst.FNA_SIGNATURE);
	}

	@GetMapping(value = "/getAllDataById/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaSignature> getDataByFnaId(@PathVariable("fnaId") String fnaId) {
		return signService.getAllDataById(PsgConst.FNA_ID, fnaId, PsgConst.FNA_SIGNATURE);
	}

	@PutMapping(value = "/updateData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateFnaData(@RequestBody FnaSignature fna) {
		signService.update(fna);
	}

	@DeleteMapping(value = "/deleteData/{primaryId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void deleteFnaData(@PathVariable("primaryId") String primaryId) {
		signService.delete(primaryId, PsgConst.ESIGN_ID, PsgConst.FNA_SIGNATURE);
	}

	@GetMapping("/getQrCodeById/{signPerson}")
	public byte[] getQrCodeById(@PathVariable(value = "signPerson") String signPerson, HttpServletRequest request)
			throws JsonProcessingException, UnsupportedEncodingException {

		String strDelFlag = request.getParameter("isDelete");

		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
		String dataFormId = (String) request.getSession(false).getAttribute(PsgConst.SESS_DATAFORM_ID);
		String encodedString = DatatypeConverter.printBase64Binary((signPerson + "=" + dataFormId).getBytes("UTF-8"));
		String url = request.getRequestURL().toString().replaceAll("FnaSignature/getQrCodeById/" + signPerson, "")
				+ "signInfo?" + encodedString;
		logger.info("URL----" + url);

		if ("true".equalsIgnoreCase(strDelFlag)) {
			clearFnaFlagStatus(fnaId, request);
			signService.delete(fnaId, PsgConst.FNA_ID, PsgConst.FNA_SIGNATURE, signPerson, PsgConst.SIGN_PERSON);
		}

		QrCode qrCode = new QrCode();
		return qrCode.getQRCodeImage(url, fnaId, 220, 220);
	}

	@PostMapping(value = "/saveFnaSignature")
	public FnaSignature saveFnaSignature(@RequestParam("fnaId") String fnaId, @RequestParam("esign") String esign,
			@RequestParam("signDoc") String signDoc, @RequestParam("perType") String perType,
			@RequestParam("curPage") String curPage, @RequestParam("esignId") String esignId,
			HttpServletRequest request) throws ParseException {

		// Save fnasignature
		FnaSignature fnaSign = new FnaSignature();
		FnaDetails fnaDets = fnaDetsService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
		fnaSign.setFnaDetails(fnaDets);

		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(esign);
		fnaSign.seteSign(json);
		fnaSign.setSignPerson(perType);
		fnaSign.setSignDate(new Date(System.currentTimeMillis()));
		fnaSign.setPageSecRef(curPage);
		byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(signDoc);
		fnaSign.setSignDocBlob(imageBytes);

		if (!esignId.isEmpty() && esignId != null) {
			fnaSign.setEsignId(UUID.fromString(esignId));
			signService.update(fnaSign);
		} else {
			signService.saveData(fnaSign);
		}
		// new AutoRefreshHandler();
		// autoRefresh.ha
		return fnaSign;
	}

	@GetMapping("/checkPopupMail/{fnaId}")
	public List<FnaSignature> checkPopupMail(@PathVariable("fnaId") String fnaId) {

		FnaDetails fnaDets = fnaDetsService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);

		List<FnaSignature> clntAdvList = new ArrayList<FnaSignature>();
		if (fnaDets.getMgrEmailSentFlg() == null) {
			List<FnaSignature> signList = signService.getAllDataById(PsgConst.FNA_ID, fnaId, PsgConst.FNA_SIGNATURE);
			for (FnaSignature sign : signList) {
				String signPerson = sign.getSignPerson();
				if (signPerson.equals(PsgConst.SIGN_CLIENT)) {
					clntAdvList.add(sign);
				}
				if (signPerson.equals(PsgConst.SIGN_ADVISER)) {
					clntAdvList.add(sign);
				}
				if (signPerson.equals(PsgConst.SIGN_SPS)) {
					clntAdvList.add(sign);
				}
			}
		}
		return clntAdvList;
	}

	@GetMapping("/sentMail/{toMailAddr}")
	public String sendMail(@PathVariable("toMailAddr") String toMailAddr, HttpServletRequest request)
			throws JsonProcessingException {
		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
		FnaDetails fnaDets = fnaDetsService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
		Email mail = new Email();
		mail.setSendToMail(toMailAddr);
		boolean isSent = mailService.sendEmail(mail, request);
		Map<String, Object> resultMap = new HashMap<>();
		if (isSent) {
			// Update mail sent flag
			fnaDets.setMgrEmailSentFlg(PsgConst.YES_VALUE);
			request.setAttribute("FNA_DETAILS", mapper.writeValueAsString(fnaDets));
			fnaDetsService.update(fnaDets);
			
			resultMap.put("status", "success");
			resultMap.put("data", fnaDets);
		} else {
			resultMap.put("status", "failed");
			resultMap.put("reason", "Please Try again Later or else Contact your System Administrator");
		}
		
		return mapper.writeValueAsString(resultMap);
	}
	
	@PostMapping(value = "/sentAdvMail")
	public String sentAdvMail(@RequestParam("email") String toMailAddr, @RequestParam("reason") String reason,
			@RequestParam("approveStatus") String approveStatus, @RequestParam("role") String role,
			HttpServletRequest request) {

		// String status,content;
		Email mail = new Email();
		mail.setSendToMail(toMailAddr);
		/*
		 * if(supreFlg.equals(Constants.YES_VALUE)) { status="Agree";
		 * content="Your application is verified"; }else { status="Disagree";
		 * content=reason; }
		 */
		mailService.sendAdvEmail(mail, approveStatus, reason, role, request);

		return "mail was sent";
	}

	@GetMapping("/getDataByCol/{signPerson}")
	public FnaSignature getDataByCol(@PathVariable("signPerson") String signPerson, HttpServletRequest request) {
		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
		return signService.getDataByCol(fnaId, PsgConst.FNA_ID, signPerson, PsgConst.SIGN_PERSON,
				PsgConst.FNA_SIGNATURE);
	}

	@DeleteMapping("/clearByFnaId/{fnaId}")
	public String clearByFnaId(@PathVariable("fnaId") String fnaId, HttpServletRequest request)
			throws JsonProcessingException {
		FnaDetails fnaDetails = clearFnaFlagStatus(fnaId, request);
		signService.delete(fnaId, PsgConst.FNA_ID, PsgConst.FNA_SIGNATURE);
		return mapper.writeValueAsString(fnaDetails);
	}

	public FnaDetails clearFnaFlagStatus(String fnaId, HttpServletRequest request) throws JsonProcessingException {
		FnaDetails fnaDetails = fnaDetsService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
		fnaDetails.setSuprevMgrFlg(null);
		fnaDetails.setSuprevFollowReason(null);
		fnaDetails.setKycSentStatus(null);
		fnaDetails.setMgrApproveStatus(null);
		fnaDetails.setMgrApproveDate(null);
		fnaDetails.setMgrEmailSentFlg(null);
		fnaDetails.setAdminKycSentStatus(null);
		fnaDetails.setAdminApproveStatus(null);
		fnaDetails.setAdminApproveDate(null);
		fnaDetails.setCompKycSentStatus(null);
		fnaDetails.setCompApproveStatus(null);
		fnaDetails.setCompApproveDate(null);
		fnaDetails.setMgrApprStsByAdvStf(null);
		fnaDetails.setAdminApprStsByAdvStf(null);
		fnaDetails.setCompApprStsByAdvStf(null);
		fnaDetsService.update(fnaDetails);
		request.setAttribute("FNA_DETAILS", mapper.writeValueAsString(fnaDetails));
		return fnaDetails;
	}

}
