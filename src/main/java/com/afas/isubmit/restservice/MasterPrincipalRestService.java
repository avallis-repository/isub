package com.afas.isubmit.restservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.MasterPrincipal;
import com.afas.isubmit.dto.MasterProduct;
import com.afas.isubmit.service.MasterPrincipalService;
import com.afas.isubmit.service.MasterProductService;
import com.afas.isubmit.util.PsgConst;

@RestController
@RequestMapping("/MasterPrincipal")
public class MasterPrincipalRestService {

	@Autowired
	MasterPrincipalService prinService;

	@Autowired
	MasterProductService prodService;

	@GetMapping(value = "/getAllData", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MasterPrincipal> getAllDatas() {
		return prinService.getAllData(PsgConst.MASTER_PRINCIPAL);
	}

	@GetMapping(value = "/getDataById/{prinId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public MasterPrincipal getDataById(@PathVariable("prinId") String prinId) {
		return prinService.getDataById(prinId, PsgConst.PRIN_ID, PsgConst.MASTER_PRINCIPAL);
	}

	@GetMapping(value = "/getDataByName/{prinName}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MasterProduct> getDataByPrinName(@PathVariable("prinName") String prinName) {
		MasterPrincipal prinObj = prinService.getDataById(prinName, PsgConst.PRIN_NAME, PsgConst.MASTER_PRINCIPAL);
		return prodService.getAllDataById(PsgConst.PRIN_ID, prinObj.getPrinId(), PsgConst.MASTER_PRODUCT);
	}

}
