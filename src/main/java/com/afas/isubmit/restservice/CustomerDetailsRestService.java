package com.afas.isubmit.restservice;

import java.sql.Date;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.CustomerDetails;
import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaSelfSpouseDets;
import com.afas.isubmit.dto.MasterAdviser;
import com.afas.isubmit.dto.UserLogin;
import com.afas.isubmit.service.CustomerDetailsService;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaSelfSpouseDetsService;
import com.afas.isubmit.service.MasterAdviserService;
import com.afas.isubmit.service.UserLoginService;
import com.afas.isubmit.util.ErrorMsg;
import com.afas.isubmit.util.PsgConst;
import com.afas.isubmit.util.PsgUtil;

@RestController
@RequestMapping("/CustomerDetails")
public class CustomerDetailsRestService {
	
	@Autowired
	CustomerDetailsService custDetService;

	@Autowired
	FnaDetailsService fnaDetService;

	@Autowired
	FnaSelfSpouseDetsService selfSpsService;

	@Autowired
	UserLoginService loginService;
	
	@Autowired
	MasterAdviserService masterAdviserService;

	String className = PsgConst.CUSTOMER_DETAILS;

//	Logger log=Logger.getLogger(CustomerDetailsRestService.class);
	ResourceBundle resource = ResourceBundle.getBundle("properties/psgisubmit", Locale.ENGLISH);

	@PostMapping(value = "/saveData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public CustomerDetails saveCustomerDets(@RequestBody CustomerDetails custDets, HttpServletRequest request,
			HttpServletResponse response) {
//		log.info("Customer details save---");
		try {
			String custId = custDets.getCustId();
			String sessCustId = (String) request.getSession(false).getAttribute(PsgConst.SESS_CUST_ID);
			String clientStatus = (String) request.getSession(false).getAttribute(PsgConst.CLIENT_STATUS);

			String loggedInUserId = (String) request.getSession(false).getAttribute("loggedInUserId");

			UserLogin userLogin = null;
			if (loggedInUserId != null) {
				userLogin = loginService.getDataById(loggedInUserId, "user_id", "UserLogin");
			}
			// Add client data
			if (clientStatus.equals(PsgConst.CLIENT_ADD)) {
				if (sessCustId != null) {
					setCustomerDetails(custDets, userLogin, clientStatus);
					custDets.setCustId(sessCustId);

					custDetService.update(custDets);

					/* Update Fna Details */

					FnaDetails fnaDet = fnaDetService.getDataById(sessCustId, PsgConst.CUST_ID, PsgConst.FNA_DETAILS);

					String fnaId = fnaDet.getFnaId();
					setSessionData(custDets.getCustName(), sessCustId, fnaId, request);
					// return ResponseEntity.ok().body("updated your data");
					response.setStatus(200);
					// response.getWriter().write("updated your data");

					// }
				} else {
					saveData(custDets, request, userLogin, clientStatus);
					response.setStatus(200);
				}
			} else {
				// Edit client data
				if (custId == null || custId.isEmpty()) {
					saveData(custDets, request, userLogin, clientStatus);
					response.setStatus(200);
				} else {
					updateData(custDets, custId, request, userLogin, clientStatus);
					response.setStatus(200);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(400);
			// response.getWriter().write(e.getMessage());
			// return response.getWriter().write(e.getMessage());
		}
		return custDets;
	}

	@GetMapping(value = "/getAllNricList/{nric}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDetails> getDataByNric(@PathVariable String nric) {
		return custDetService.getAllDataById(PsgConst.NRIC, nric, PsgConst.CUSTOMER_DETAILS);
	}

	@GetMapping(value = "/getAllData", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDetails> getAllData() {
		return custDetService.getAllData(className);
	}
	
	@GetMapping(value = "/getAllDataBySrchStr", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerDetails> getAllDataBySrchStr(@RequestParam("clientName") String searchStr) {
		return custDetService.getAllDataBySrchStr(CustomerDetails.class, "custName", searchStr);
	}

	@GetMapping(value = "/getDataById/{custId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public CustomerDetails getDataById(@PathVariable String custId) throws ParseException {
		CustomerDetails custDets = custDetService.getDataById(custId, PsgConst.CUST_ID, className);
		// Format for dateofbirth
		if (PsgUtil.dateEmptyOrNull(custDets.getDob()))
			custDets.setDob(PsgUtil.getDateForamt(custDets.getDob()));
		// ROC Date
		if (PsgUtil.dateEmptyOrNull(custDets.getRocDate()))
			custDets.setRocDate(PsgUtil.getDateForamt(custDets.getRocDate()));
		return custDets;
	}

	@GetMapping(value = "/findDataById/{nric}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ErrorMsg> findDataById(@PathVariable String nric) {
		ErrorMsg errMsg = new ErrorMsg();
		try {
			CustomerDetails custDets = custDetService.getDataById(nric, PsgConst.NRIC, className);
			if (custDets == null) {
				errMsg.setErrMsg("TRUE");
			}
		} catch (Exception e) {
			errMsg.setErrMsg(e.getMessage());
			return ResponseEntity.ok().body(errMsg);
		}
		return ResponseEntity.ok().body(errMsg);
	}

	private CustomerDetails setCustomerDetails(CustomerDetails custDets, UserLogin userLogin, String clientStatus) {
		custDets.setFnaType(PsgConst.FNA_TYPE);
		if (userLogin != null) {
			custDets.setAgentIdInitial(userLogin.getAdvStfId().getAdvStfId());
			custDets.setAgentIdCurrent(userLogin.getAdvStfId().getAdvStfId());
			if (clientStatus.equals(PsgConst.CLIENT_ADD)) {
				custDets.setCreatedBy(userLogin.getAdvStfId().getAdvStfName());
			} else {
				custDets.setModifiedBy(userLogin.getAdvStfId().getAdvStfName());
			}
			custDets.setDistributorId(userLogin.getTenantId().getTenantId());
			custDets.setDistributorName(userLogin.getTenantId().getTenantName());
		}
		if (clientStatus.equals(PsgConst.CLIENT_ADD)) {
			custDets.setCreatedDate(new Date(System.currentTimeMillis()));
		} else {
			custDets.setModifiedDate(new Date(System.currentTimeMillis()));
		}
		return custDets;

	}

	private FnaDetails setFnaDetails(CustomerDetails custDets, UserLogin userLogin, String clientStatus) {
		FnaDetails fnaDet = new FnaDetails();
		fnaDet.setFnaType(PsgConst.FNA_TYPE);
		fnaDet.setCustDetails(custDets);
		if (userLogin != null) {
			fnaDet.setAdvstfId(userLogin.getAdvStfId().getAdvStfId());
			fnaDet.setMgrId(userLogin.getAdvStfId().getManagerId());

			String managerId = userLogin.getAdvStfId().getManagerId();
			if (managerId != null && !managerId.isEmpty()) {
				MasterAdviser managerStaff = masterAdviserService.getDataById(managerId, PsgConst.ADVSTF_ID,
						PsgConst.MASTER_ADVISER);
	
				if (managerStaff != null) {
					fnaDet.setMgrName(managerStaff.getAdvStfName());
				}
			}

			fnaDet.setCreatedBy(userLogin.getAdvStfId().getAdvStfName());
		}
		fnaDet.setCreatedDate(new Date(System.currentTimeMillis()));
		return fnaDet;
	}

	private void saveData(CustomerDetails custDets, HttpServletRequest request, UserLogin userLogin,
			String clientStatus) {
		String custId, fnaId;
//		String dataFormId;
		// Setting hardcoded customer details
		setCustomerDetails(custDets, userLogin, clientStatus);
		// Save Customer Details
		custDetService.saveData(custDets);
		custId = custDets.getCustId();

		// Save Fna Details
		FnaDetails fnaDet = setFnaDetails(custDets, userLogin, clientStatus);
		fnaDetService.saveData(fnaDet);
		fnaId = fnaDet.getFnaId();

		// Save self details
		FnaSelfSpouseDets selfDets = setSelfSpouseDets(custDets);
		selfDets.setFnaDetails(fnaDet);
		selfSpsService.saveData(selfDets);

		// setUserSessData(fnaSpouse,request);
		setSessionData(custDets.getCustName(), custId, fnaId, request);
	}

	private FnaSelfSpouseDets setSelfSpouseDets(CustomerDetails custDets) {
		FnaSelfSpouseDets sp = new FnaSelfSpouseDets();

		sp.setDfSelfName(custDets.getCustName());
		sp.setDfSelfDob(custDets.getDob());
		sp.setDfSelfMartsts(custDets.getMaritalStatus());
		if (PsgUtil.emptyOrNull(custDets.getSex())) {
			if (custDets.getSex().equals(PsgConst.GEN_FEMALE)) {
				sp.setDfSelfGender(custDets.getSex());
			} else {
				sp.setDfSelfGender(PsgConst.GEN_MALE);
			}
		}
		sp.setDfSelfNric(custDets.getNric());
		sp.setDfSelfPersEmail(custDets.getEmailId());
		sp.setDfSelfMobile(custDets.getResHandPhone());
		if (!"SG".equals(custDets.getNationality()) && !"SG-PR".equals(custDets.getNationality())) {
			sp.setDfSelfNationality("Others");
			sp.setDfSelfNatyDets(custDets.getNationality());
		} else {
			sp.setDfSelfNationality(custDets.getNationality());
		}
		sp.setDfSelfBusinatr(custDets.getBusinessNatr());
		sp.setDfSelfBusinatrDets(custDets.getBusinessNatrDets());
		sp.setDfSelfCompname(custDets.getCompanyName());
		sp.setDfSelfHeight(custDets.getHeight());
		sp.setDfSelfWeight(custDets.getWeight());
		String income = String.valueOf(custDets.getIncome() > 0 ? custDets.getIncome() : "");
		sp.setDfSelfAnnlIncome(income);
		sp.setDfSelfOccpn(custDets.getOccpnDesc());

		if (custDets.getCustCateg().equalsIgnoreCase("COMPANY")) {
			String offAddr = custDets.getOffAddr1();

			if (PsgUtil.emptyOrNull(custDets.getOffAddr2()))
				offAddr += "," + custDets.getOffAddr2();
			if (PsgUtil.emptyOrNull(custDets.getOffAddr3()))
				offAddr += "," + custDets.getOffAddr3();
			if (PsgUtil.emptyOrNull(custDets.getOffCountry()))
				offAddr += "," + custDets.getOffCountry();
			if (PsgUtil.emptyOrNull(custDets.getOffCity()))
				offAddr += "," + custDets.getOffCity();
			if (PsgUtil.emptyOrNull(custDets.getOffState()))
				offAddr += "," + custDets.getOffState();

			offAddr += "," + custDets.getOffPostalCode();

			sp.setDfSelfOffAddr(offAddr);
		} else {
			String addr = custDets.getResAddr1();

			if (PsgUtil.emptyOrNull(custDets.getResAddr2()))
				addr += "," + custDets.getResAddr2();
			if (PsgUtil.emptyOrNull(custDets.getResAddr3()))
				addr += "," + custDets.getResAddr3();
			if (PsgUtil.emptyOrNull(custDets.getResCountry()))
				addr += "," + custDets.getResCountry();
			if (PsgUtil.emptyOrNull(custDets.getResCity()))
				addr += "," + custDets.getResCity();
			if (PsgUtil.emptyOrNull(custDets.getResState()))
				addr += "," + custDets.getResState();

			addr += "," + custDets.getResPostalCode();

			sp.setDfSelfHomeAddr(addr);
		}

		sp.setResAddr1(custDets.getResAddr1());
		sp.setResAddr2(custDets.getResAddr2());
		sp.setResAddr3(custDets.getResAddr3());
		sp.setResCountry(custDets.getResCountry());
		sp.setResCity(custDets.getResCity());
		sp.setResState(custDets.getResState());
		sp.setResPostalCode(custDets.getResPostalCode());

		return sp;
	}

	private void setSessionData(String custName, String custId, String fnaId, HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.CUST_NAME, custName);
		request.getSession(false).setAttribute(PsgConst.SESS_CUST_ID, custId);
		request.getSession(false).setAttribute(PsgConst.SESS_FNA_ID, fnaId);
	}

	private void updateData(CustomerDetails custDets, String custId, HttpServletRequest request, UserLogin userLogin,
			String clientStatus) {
//		String dataFormId;
		String fnaId = null;
		/* Update Customer Details */
		setCustomerDetails(custDets, userLogin, clientStatus);
		custDetService.update(custDets);

		/* Update Self spouse Details */

		FnaDetails fnaDet = fnaDetService.getDataById(custDets.getCustId(), PsgConst.CUST_ID, PsgConst.FNA_DETAILS);
		if (fnaDet == null) {
			// Save Fna Details
			fnaDet = setFnaDetails(custDets, userLogin, clientStatus);
			fnaDetService.saveData(fnaDet);
			fnaId = fnaDet.getFnaId();

			// Save self details
			FnaSelfSpouseDets selfDets = setSelfSpouseDets(custDets);
			selfDets.setFnaDetails(fnaDet);
			selfSpsService.saveData(selfDets);
		}
		
		if (fnaDet != null) {
			if (fnaId == null) {
				fnaId = fnaDet.getFnaId();
			}
			
			FnaSelfSpouseDets selfDets = updateSelfSpsDets(fnaId, custDets);
			selfSpsService.update(selfDets);
			
			// setUserSessData(selfSpouseDets,request);
			setSessionData(custDets.getCustName(), custId, fnaId, request);
		}
	}
	
	public FnaSelfSpouseDets updateSelfSpsDets(String fnaId, CustomerDetails custDets) {
		FnaSelfSpouseDets selfDets = selfSpsService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_SELF_SPOUSE_DETS);
		selfDets.setDfSelfName(custDets.getCustName());
		selfDets.setDfSelfDob(custDets.getDob());
		selfDets.setDfSelfNric(custDets.getNric());
		if (!"SG".equals(custDets.getNationality()) && !"SG-PR".equals(custDets.getNationality())) {
			selfDets.setDfSelfNationality("Others");
			selfDets.setDfSelfNatyDets(custDets.getNationality());
		} else {
			selfDets.setDfSelfNationality(custDets.getNationality());
		}
		
		if (custDets.getCustCateg().equalsIgnoreCase("COMPANY")) {
			String offAddr = custDets.getOffAddr1();

			if (PsgUtil.emptyOrNull(custDets.getOffAddr2()))
				offAddr += "," + custDets.getOffAddr2();
			if (PsgUtil.emptyOrNull(custDets.getOffAddr3()))
				offAddr += "," + custDets.getOffAddr3();
			if (PsgUtil.emptyOrNull(custDets.getOffCountry()))
				offAddr += "," + custDets.getOffCountry();
			if (PsgUtil.emptyOrNull(custDets.getOffCity()))
				offAddr += "," + custDets.getOffCity();
			if (PsgUtil.emptyOrNull(custDets.getOffState()))
				offAddr += "," + custDets.getOffState();

			offAddr += "," + custDets.getOffPostalCode();

			selfDets.setDfSelfOffAddr(offAddr);
		} else {
			String addr = custDets.getResAddr1();

			if (PsgUtil.emptyOrNull(custDets.getResAddr2()))
				addr += "," + custDets.getResAddr2();
			if (PsgUtil.emptyOrNull(custDets.getResAddr3()))
				addr += "," + custDets.getResAddr3();
			if (PsgUtil.emptyOrNull(custDets.getResCountry()))
				addr += "," + custDets.getResCountry();
			if (PsgUtil.emptyOrNull(custDets.getResCity()))
				addr += "," + custDets.getResCity();
			if (PsgUtil.emptyOrNull(custDets.getResState()))
				addr += "," + custDets.getResState();

			addr += "," + custDets.getResPostalCode();

			selfDets.setDfSelfHomeAddr(addr);
		}

		selfDets.setResAddr1(custDets.getResAddr1());
		selfDets.setResAddr2(custDets.getResAddr2());
		selfDets.setResAddr3(custDets.getResAddr3());
		selfDets.setResCountry(custDets.getResCountry());
		selfDets.setResCity(custDets.getResCity());
		selfDets.setResState(custDets.getResState());
		selfDets.setResPostalCode(custDets.getResPostalCode());
		
		return selfDets;
	}
}
