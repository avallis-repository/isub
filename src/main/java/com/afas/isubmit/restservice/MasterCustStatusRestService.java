package com.afas.isubmit.restservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.MasterCustomerStatus;
import com.afas.isubmit.service.MasterCustStatusService;
import com.afas.isubmit.util.PsgConst;

@RestController
@RequestMapping("/MasterCustStatus")
public class MasterCustStatusRestService {

	@Autowired
	MasterCustStatusService custService;

	@GetMapping("/getAllData")
	public List<MasterCustomerStatus> getAllData() {
		List<MasterCustomerStatus> custList = custService.getAllData(PsgConst.MASTER_CUST_STATUS);
		return custList;
	}

}
