package com.afas.isubmit.restservice;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.FnaDependantDets;
import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaSelfSpouseDets;
import com.afas.isubmit.service.FnaDependantDetsService;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaSelfSpouseDetsService;
import com.afas.isubmit.util.ErrMsgConstants;
import com.afas.isubmit.util.ErrorMsg;
import com.afas.isubmit.util.PsgConst;

@RestController
@RequestMapping("/FnaDependantDets")
public class FnaDependantDetsRestService {

	private static Logger logger = LogManager.getLogger(FnaDependantDetsRestService.class);

	@Autowired
	FnaDependantDetsService service;

	@Autowired
	FnaSelfSpouseDetsService selfSpouseService;

	@Autowired
	FnaDetailsService fnaDetsService;

	@GetMapping(value = "/getAllData", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaDependantDets> getAllData(HttpServletRequest request) {
		String dataFormId = (String) request.getSession(false).getAttribute(PsgConst.SESS_DATAFORM_ID);
		logger.info("Data formid----" + dataFormId);
		return service.getAllDataById("dataformid", dataFormId, PsgConst.FNA_DEPENDANT_DETS);
	}

	@GetMapping(value = "/getDataById/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public FnaDependantDets getDataById(@PathVariable("fnaId") String fnaId) {
		return service.getDataById(fnaId, PsgConst.DEPN_ID, PsgConst.FNA_DEPENDANT_DETS);
	}

	@PostMapping(value = "/saveData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaDependantDets saveFnaData(@RequestBody FnaDependantDets depDets, HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		try {

			String dataFormId = (String) request.getSession(false).getAttribute(PsgConst.SESS_DATAFORM_ID);

			String depnId = depDets.getDepnId();

			FnaSelfSpouseDets fnaSelf = selfSpouseService.getDataById(dataFormId, PsgConst.DATAFORM_ID,
					PsgConst.FNA_SELF_SPOUSE_DETS);
//		fnaSelf.setDataFormId(dataFormId);
			FnaDetails fnaDets = fnaSelf.getFnaDetails();
			fnaDets.setFwrDepntFlg(PsgConst.YES_VALUE);
			depDets.setFnaDetails(fnaDets);
			fnaDetsService.update(fnaDets);
			depDets.setFnaDependant(fnaSelf);
			depDets.setCreatedDate(new Date(System.currentTimeMillis()));
			if (depnId == null || depnId.isEmpty()) {
				service.saveData(depDets);
				return depDets;
			} else {
				service.update(depDets);
				return depDets;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return depDets;
	}

	@PutMapping(value = "/updateData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void updateFnaData(@RequestBody FnaDependantDets fna) {
		service.update(fna);
	}

	@DeleteMapping(value = "/deleteData/{primaryId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ErrorMsg> deleteFnaData(@PathVariable("primaryId") String primaryId,
			HttpServletRequest request) {
		ErrorMsg errMsg = new ErrorMsg();
		try {
//			String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
//			FnaDetails fnaDets = fnaDetsService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
			/*
			 * if(fwrFlg.equalsIgnoreCase(PsgConst.NO_VALUE)) {
			 * fnaDets.setFwrDepntFlg(PsgConst.NO_VALUE); fnaDetsService.update(fnaDets); }
			 */
			service.delete(primaryId, PsgConst.DEPN_ID, PsgConst.FNA_DEPENDANT_DETS);
			errMsg.setErrMsg(ErrMsgConstants.DLT_MSG);
		} catch (Exception e) {
			errMsg.setErrMsg(e.getMessage());
			return ResponseEntity.ok().body(errMsg);
		}
		return ResponseEntity.ok().body(errMsg);
	}

}
