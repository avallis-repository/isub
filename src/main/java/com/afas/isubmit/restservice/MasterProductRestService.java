package com.afas.isubmit.restservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.MasterProduct;
import com.afas.isubmit.service.MasterProductService;
import com.afas.isubmit.util.PsgConst;

@RestController
@RequestMapping("/MasterProduct")
public class MasterProductRestService {
	
	@Autowired
	MasterProductService prodService;

	@GetMapping(value = "/getAllDataById/{prinId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<MasterProduct> getAllDataById(@PathVariable("prinId") String prinId) {
		return prodService.getAllDataById(PsgConst.PRIN_ID, prinId, PsgConst.MASTER_PRODUCT);
	}

}
