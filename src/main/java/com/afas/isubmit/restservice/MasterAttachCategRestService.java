package com.afas.isubmit.restservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.MasterAttachCategory;
import com.afas.isubmit.service.MasterAttachCategService;
import com.afas.isubmit.util.PsgConst;

@RestController
@RequestMapping("/MasterAttachCateg")
public class MasterAttachCategRestService {

	@Autowired
	MasterAttachCategService attachService;

	@GetMapping("/getAllData")
	public List<MasterAttachCategory> getAllData() {
		List<MasterAttachCategory> categList = attachService.getAllData(PsgConst.MASTER_ATTACH_CATEG);
//		Set<MasterAttachCategory> setList = new HashSet<>(categList);
//		categList.clear();
//		categList.addAll(setList);
		return categList;
	}
}
