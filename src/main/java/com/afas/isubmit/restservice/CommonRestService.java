package com.afas.isubmit.restservice;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.UnknownHttpStatusCodeException;
import org.springframework.web.util.UriComponentsBuilder;

import com.afas.isubmit.dto.CustomerDetails;
import com.afas.isubmit.dto.FnaDependantDets;
import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaExpenditureDets;
import com.afas.isubmit.dto.FnaFatcaTaxDet;
import com.afas.isubmit.dto.FnaNtucDocs;
import com.afas.isubmit.dto.FnaNtucPolicyDets;
import com.afas.isubmit.dto.FnaRecomPrdtPlanDet;
import com.afas.isubmit.dto.FnaSelfSpouseDets;
import com.afas.isubmit.dto.MasterAdviser;
import com.afas.isubmit.dto.UserLogin;
import com.afas.isubmit.service.AuthTokenService;
import com.afas.isubmit.service.FnaDependantDetsService;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaExpenditureDetsService;
import com.afas.isubmit.service.FnaFatcaTaxDetService;
import com.afas.isubmit.service.FnaNtucDocsService;
import com.afas.isubmit.service.FnaNtucPolicyDetsService;
import com.afas.isubmit.service.FnaOtherPersonDetsService;
import com.afas.isubmit.service.FnaRecommPrdtPlanDetService;
import com.afas.isubmit.service.FnaSelfSpouseDetsService;
import com.afas.isubmit.service.MasterAdviserService;
import com.afas.isubmit.service.UserLoginService;
import com.afas.isubmit.util.PsgConst;
import com.afas.isubmit.util.PsgUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/Common")
public class CommonRestService {

	private static Logger logger = LogManager.getLogger(CommonRestService.class);

	@Autowired
	FnaSelfSpouseDetsService selfSpouse;

	@Autowired
	FnaOtherPersonDetsService otherPerson;

	@Autowired
	FnaDetailsService fnaDets;

	@Autowired
	FnaFatcaTaxDetService taxDets;

	@Autowired
	FnaDependantDetsService depDets;

	@Autowired
	FnaExpenditureDetsService expendDets;

	@Autowired
	FnaRecommPrdtPlanDetService prdtPlanDets;

	@Autowired
	UserLoginService loginService;

	@Autowired
	AuthTokenService tokenService;
	
	@Autowired
	FnaNtucDocsService ntucDocsService;
	
	@Autowired
	MasterAdviserService masterAdviserService;
	
	@Autowired
	FnaNtucPolicyDetsService ntucPolicyService;

	JSONParser parser = new JSONParser();

	ObjectMapper mapper = new ObjectMapper();

	RestTemplate restTemplate = new RestTemplate();
	
	@Value("${afa.psg.url}")
	String STR_AFA_PSG_URL;
	
	@Value("${afa.psg.index.path}")
	String STR_AFA_PSG_INDEX;
	
	@Value("${afa.psg.authtoken.path}")
	String STR_AFA_PSG_TOKEN;

//	@Value("${policy.submit.url}")
//	String submissionUrl;
//	
	@Value("${afa.psg.client_id}")
	String clientId;
	
	@Value("${afa.psg.client_secret}")
	String clientSecret;
	
	@Value("${afas.psg.retrieve.policy.url}")
	String ntucRetrievePolicyUrl;
	
	@Value("${afas.psg.retrieve.fileinfo.url}")
	String ntucRetrieveFileInfoUrl;
	
	@Value("${afas.psg.dsf.updatests.url}")
	String ntucDsfUpdateStsUrl;
	
	@Value("${afas.psg.dsf.approval.updatests.url}")
	String ntucDsfApprovalUpdateStsUrl;
	
	@Value("${afas.psg.case.dets.url}")
	String caseDetsUrl;
	
	@Value("${afas.psg.docs.local.file.path}")
	String ntucDocslocalFilePath;

	@GetMapping(value = "/getAllPageData/{fnaId}")
	public Map<String, String> getAllPageData(@PathVariable("fnaId") String fnaId) throws JsonProcessingException {
		Map<String, String> obj = new HashMap<>();

		try {
			FnaSelfSpouseDets selfSps = selfSpouse.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_SELF_SPOUSE_DETS);
			if (selfSps != null) {
				obj.put("FNA_SELF_SPOUSE_DATA", mapper.writeValueAsString(selfSps));
			}

			// FnaOtherPersonDets
			// otherPer=otherPerson.getDataById(fnaId,PsgConst.FNA_ID,PsgConst.FNA_OTHPER_DETS);
			// obj.put("FNA_OTH_PERSON_DATA",mapper.writeValueAsString(otherPer));

			FnaDetails fnaDet = fnaDets.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
			if (fnaDet != null) {
				obj.put("FNA_DETAILS", mapper.writeValueAsString(fnaDet));
			}

			List<FnaFatcaTaxDet> fatcaTax = taxDets.getAllDataByCol(fnaId, PsgConst.FNA_ID, "SELF", PsgConst.TAX_FOR,
					PsgConst.FNA_FATCA_TAX_DETS);
			if (fatcaTax != null) {
				obj.put("FATCATAXDET_DATA", mapper.writeValueAsString(fatcaTax));
			}

			List<FnaFatcaTaxDet> fatcaSpsTax = taxDets.getAllDataByCol(fnaId, PsgConst.FNA_ID, PsgConst.SIGN_SPS,
					PsgConst.TAX_FOR, PsgConst.FNA_FATCA_TAX_DETS);
			if (fatcaSpsTax != null) {
				obj.put("FATCATAXDETSPS_DATA", mapper.writeValueAsString(fatcaSpsTax));
			}

			FnaExpenditureDets expend = expendDets.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_EXPENDITURE_DETS);
			if (expend != null) {
				obj.put("EXPENDITURE_DETAILS", mapper.writeValueAsString(expend));
			}

			List<FnaDependantDets> deps = depDets.getAllDataById(PsgConst.FNA_ID, fnaId, PsgConst.FNA_DEPENDANT_DETS);
			if (deps != null) {
				obj.put("FNADEPNT_TABLE_DATA", mapper.writeValueAsString(deps));
			}

			List<FnaRecomPrdtPlanDet> prodRecommList = prdtPlanDets.getAllDataById(PsgConst.FNA_ID, fnaId,
					PsgConst.FNA_RECOMM_PRDT_DETS);
			if (prodRecommList != null) {
				obj.put("FNAPROD_RECOM_DATA", mapper.writeValueAsString(prodRecommList));
			}
			// FnaRecommPrdtPlanDet
		} catch (Exception e) {
			logger.error("Exception on getAllPageData!!!");
			e.printStackTrace();
		}

		return obj;
	}
	
//	Comment - AB
//	@GetMapping("/getAFAToekn")
//	public Map<String, String>  getAFAToken() {
//		
//		
//		HttpHeaders tokenHeaders = new HttpHeaders();
//		tokenHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//		logger.info("token url: " + tokenUrl);
//
//		MultiValueMap<String, String> tokenParamMap = new LinkedMultiValueMap<String, String>();
//		tokenParamMap.add("client_id", clientId);
//		tokenParamMap.add("client_secret", clientSecret);
//
//		HttpEntity<MultiValueMap<String, String>> tokenRequestEntity = new HttpEntity<MultiValueMap<String, String>>(
//				tokenParamMap, tokenHeaders);
//
//		Map<String, String> resultMap = new HashMap<String, String>();
//		try {
//			RestTemplate restTemplate = new RestTemplate();
//			ResponseEntity<String> tokenResp = restTemplate.postForEntity(tokenUrl, tokenRequestEntity, String.class);
//			if (tokenResp != null) {
//				if (tokenResp.getBody() != null) {
//					logger.info("response body: " + tokenResp.getBody());
//					
//					resultMap.put("status", "success");					
//					resultMap.put("body", tokenResp.getBody());
//				}
//			}
//
//		} catch (Exception ex) {
////			ex.printStackTrace();
//			resultMap.put("status", "failed");
//		}
//		return resultMap;
//	
//	}
	

	@GetMapping("/policySubmit")
	public String policySubmit(@RequestParam String fnaId, @RequestParam String principals, HttpServletRequest request,
			HttpServletResponse response) throws java.text.ParseException, ParseException {

		Map<String, Object> resultMap = new HashMap<>();
		ObjectMapper mapper = new ObjectMapper();

		logger.info("fnaId: " + fnaId);
		logger.info("principals: " + principals);
		
		List<String> principalList = new ArrayList<>();
		if (principals != null && !principals.isEmpty()) {
			String[] principalArray = principals.split(",");
			principalList = Arrays.asList(principalArray);
		}
		logger.info("principalList: " + principalList);
		FnaSelfSpouseDets fnaSelfSpouseDetails = selfSpouse.getDataById(fnaId, PsgConst.FNA_ID,
				PsgConst.FNA_SELF_SPOUSE_DETS);
		
		logger.info("fnaSelfSpouseDetails: " +fnaSelfSpouseDetails);

		if (fnaSelfSpouseDetails != null) {
			
			String fnaDetailsJson = formFNADetails(fnaSelfSpouseDetails, principalList, request);
			
			logger.info("fnaDetailsJson: " +fnaDetailsJson);
			
			String encodedString = Base64.getEncoder().encodeToString(fnaDetailsJson.getBytes());
			logger.info("encodedString: " +encodedString);
			String strCred = clientId+":"+clientSecret;
			logger.info("strCred: " +strCred);
			String strCredDec = Base64.getEncoder().encodeToString(strCred.getBytes());
			logger.info("strCredDec: " +strCredDec);
			
			resultMap.put("status", "success");
			resultMap.put("afaJson", encodedString);
			resultMap.put("afaPsgUrl", STR_AFA_PSG_URL);
			resultMap.put("afaPsgIndexPath", STR_AFA_PSG_INDEX);
			resultMap.put("afaPsgMyCred", strCredDec);

//			AuthToken activeToken = tokenService.getDataById("true", "active", "AuthToken");
//			logger.info("auth token: " + activeToken);
//			String token = null;
//
//			if (activeToken != null) {
//				if (activeToken.getAccessTokenExpiredDate().after(new Date())) {
//					token = activeToken.getAccessToken();
//					logger.info("access token is taken!");
////				} else if (activeToken.getRefreshTokenExpiredDate().after(new Date())) {
////					token = activeToken.getRefreshToken();
////					logger.info("refresh token is taken!");
//				} else {
//					activeToken.setActive("false");
//					activeToken.setModifiedDate(new Date());
//					tokenService.update(activeToken);
//					token = null;
//					activeToken = null;
//				}
//			}
//
//			if (activeToken == null) {
//				try {
//					activeToken = createAuthToken();
//				} catch (ParseException parseEx) {
//					parseEx.printStackTrace();
//					logger.error("Parsing error when creating auth token: " + parseEx.getMessage());
//				}
//				logger.info("active token: " + activeToken);
//				if (activeToken != null) {
//					try {
//						logger.info("active token in string: " + mapper.writeValueAsString(activeToken));
//					} catch (JsonProcessingException e) {
//						e.printStackTrace();
//					}
//					token = activeToken.getAccessToken();
//					tokenService.saveData(activeToken);
//				}
//			}
//			logger.info("token: " + token);
//			if (token != null) {
//				HttpHeaders headers = new HttpHeaders();
//				headers.add("Authorization", "Bearer " + token);
//				headers.setContentType(MediaType.APPLICATION_JSON);
//
			if (principalList.size() > 0 && "aviva".equals(principalList.get(0))) {
				
				UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(STR_AFA_PSG_URL + STR_AFA_PSG_INDEX)
				        .queryParam("afaClientInfo", encodedString)
				        .queryParam("afaCred", strCredDec);
				System.out.println("strCredDec: " + strCredDec);
				
				logger.info("builder: " +builder.toUriString());
				
//				HttpEntity<String> entityRequest = new HttpEntity<>(fnaDetailsJson);
//				logger.info("Body: " + entityRequest.getBody());
//
//				ResponseEntity<String> result = restTemplate.exchange(STR_AFA_PSG_URL + STR_AFA_PSG_INDEX, HttpMethod.POST, entityRequest,
//						String.class);
				HttpEntity<String> result = restTemplate.exchange(
				        builder.toUriString(), 
				        HttpMethod.POST,
				        null, 
				        String.class);
				logger.info("result: " + result);
				if (result != null) {
					logger.info("result body: " + result.getBody());
					if (result.getBody() != null) {
						if (result.getBody() != null) {
							JSONObject json = (JSONObject) parser.parse(result.getBody());
							if (json != null && json.containsKey("encryptedPayload")) {
								resultMap.put("encryptedPayload", json.get("encryptedPayload"));
							}
							logger.info("result body: " + result.getBody());
						}
					}
				}
			}
		} else {
			resultMap.put("status", "fail");
			
		}
		
		
		
		try {
			return mapper.writeValueAsString(resultMap);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return "failed";
		}
	}


	@SuppressWarnings("unchecked")
	public String formFNADetails(FnaSelfSpouseDets fnaSelfSpouse, List<String> principalList,
			HttpServletRequest request) {
		FnaDetails fnaDetails = fnaSelfSpouse.getFnaDetails();
		CustomerDetails customerDetails = fnaDetails.getCustDetails();
		
		SimpleDateFormat yMdHmsS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        SimpleDateFormat dMySdf = new SimpleDateFormat("dd/MM/yyyy");

		JSONObject kycDetails = new JSONObject();
		if (fnaDetails != null) {
			kycDetails.put("kycId", fnaDetails.getFnaId());
		}

		UserLogin userLogin = null;
		if (request.getSession(false) != null) {
			String loggedInUserId = (String) request.getSession(false).getAttribute("loggedInUserId");

			if (loggedInUserId != null) {
				userLogin = loginService.getDataById(loggedInUserId, "user_id", "UserLogin");
			}
			if (userLogin != null) {
				kycDetails.put("faCode", userLogin.getTenantId().getMnemonic());
				kycDetails.put("faUrl", userLogin.getTenantId().getWebsite());
			}
		}

		JSONArray principalNames = new JSONArray();
		principalNames.addAll(principalList);
		kycDetails.put("insurerToSubmit", principalNames);

		JSONObject agentIntroForm = new JSONObject();
		if (customerDetails != null) {
			JSONObject agentCode = new JSONObject();
			if (principalList != null && principalList.size() > 0) {
				for (String principal : principalList) {
//					agentCode.put(principal, PsgUtil.emptyValue(customerDetails.getAgentIdCurrent()));
					agentCode.put(principal, "586043");
				}
			}
			agentIntroForm.put("agentCode", agentCode);
//			agentIntroForm.put("createDate", PsgUtil.emptyValue(customerDetails.getCreatedDate()));
			agentIntroForm.put("lifeLicense", "yes"); // yes or no
			if (userLogin != null) {
				agentIntroForm.put("emailId", userLogin.getAdvStfId().getEmailId());
				
				String managerId = userLogin.getAdvStfId().getManagerId();
				if (managerId != null && !managerId.isEmpty()) {
					MasterAdviser manager = masterAdviserService.getDataById(managerId, PsgConst.ADVSTF_ID,
							PsgConst.MASTER_ADVISER);

					if (manager != null) {
						agentIntroForm.put("managerEmailId", manager.getEmailId());
					}
				}
				
			}
		}
		kycDetails.put("adviserInfoForm", agentIntroForm);

		JSONObject clientInfoForm = new JSONObject();

		JSONObject personalGeneralForm = new JSONObject();
		personalGeneralForm.put("name", fnaSelfSpouse.getDfSelfName());
		personalGeneralForm.put("nationalityCode", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfNationality()));
		personalGeneralForm.put("nationalityOthers", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfNatyDets()));
		personalGeneralForm.put("nricPassport", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfNric()));
		
		if (fnaSelfSpouse.getDfSelfDob() != null) {
			String strDate = fnaSelfSpouse.getDfSelfDob().toString();
	        String selfDob = null;
			try {
				selfDob = dMySdf.format(yMdHmsS.parse(strDate));
			} catch (java.text.ParseException e1) {
				e1.printStackTrace();
			}
	       
			if (selfDob != null) {
				personalGeneralForm.put("dateOfBirth", selfDob);
			}
		}
		
//		personalGeneralForm.put("dateOfBirth", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfDob()));
		personalGeneralForm.put("genderCode", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfGender()));
		personalGeneralForm.put("emailAddress", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfPersEmail()));
//		personalGeneralForm.put("weight", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfWeight()));
//		personalGeneralForm.put("height", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfHeight()));
//		personalGeneralForm.put("smoker", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfSmoking()));
		personalGeneralForm.put("weight", "58");
		personalGeneralForm.put("height", "164");
		personalGeneralForm.put("smoker", "N");
		personalGeneralForm.put("maritalStatus", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfMartsts()));
		personalGeneralForm.put("educationLevel", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfEduLevel()));
		personalGeneralForm.put("languageWritten", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfEngWritten()));
		personalGeneralForm.put("languageSpoken", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfEngSpoken()));
		personalGeneralForm.put("nameOfCompanySchool", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfCompname()));
		personalGeneralForm.put("natureOfWork", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfBusinatr()));
		personalGeneralForm.put("occupation", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfOccpn()));
		personalGeneralForm.put("countryOfBirth", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfBirthCntry()));
		personalGeneralForm.put("annualIncome", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfAnnlIncome()));
//		personalGeneralForm.put("race", PsgUtil.emptyValue(customerDetails.getRace()));
		personalGeneralForm.put("race", "I");

		String selfFundSrc = fnaSelfSpouse.getDfSelfFundsrc();
		if (selfFundSrc != null && !selfFundSrc.isEmpty()) {
			JSONObject sourceOfFund = null;
			try {
				sourceOfFund = (JSONObject) parser.parse(selfFundSrc);
				personalGeneralForm.put("sourceOfFund", sourceOfFund);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

		JSONObject contactNoPerson = new JSONObject();
		contactNoPerson.put("office", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfOffice()));
		contactNoPerson.put("mobile", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfMobile()));
		contactNoPerson.put("home", PsgUtil.emptyValue(fnaSelfSpouse.getDfSelfHome()));
		personalGeneralForm.put("contactNo", contactNoPerson);

		JSONObject address = new JSONObject();
		address.put("addressType", "Singapore");
		address.put("address1", PsgUtil.emptyValue(customerDetails.getResAddr1()));
		address.put("address2", PsgUtil.emptyValue(customerDetails.getResAddr2()));
		address.put("address3", PsgUtil.emptyValue(customerDetails.getResAddr3()));
		address.put("address4", "Singapore");
		address.put("postalCode", PsgUtil.emptyValue(customerDetails.getResPostalCode()));
		address.put("city", PsgUtil.emptyValue(customerDetails.getResCity()));
		address.put("state", PsgUtil.emptyValue(customerDetails.getResState()));
		address.put("country", PsgUtil.emptyValue(customerDetails.getResCountry()));
		address.put("unit", "9-12");
		personalGeneralForm.put("address", address);

		clientInfoForm.put("personalForm", personalGeneralForm);

		JSONObject dependentsForm = new JSONObject();
		JSONArray dependents = new JSONArray();
		JSONObject dependent = new JSONObject();
		if (fnaSelfSpouse.getDfSpsName() != null && !fnaSelfSpouse.getDfSpsName().isEmpty()) {
			dependent.put("name", fnaSelfSpouse.getDfSpsName());
			dependent.put("relationship", "SPOUSE"); // we need to amend relationship field in fnaselfspouse class
			dependent.put("gender", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsGender()));
			
			if (fnaSelfSpouse.getDfSpsDob() != null) {
				String strSpsDate = fnaSelfSpouse.getDfSpsDob().toString();
		        String spsDob = null;
				try {
					spsDob = dMySdf.format(yMdHmsS.parse(strSpsDate));
				} catch (java.text.ParseException e1) {
					e1.printStackTrace();
				}
		       
				if (spsDob != null) {
					dependent.put("bday", spsDob);
				}
			}
			
//			dependent.put("bday", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsDob()));
			dependent.put("occupation", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsOccpn()));
			dependent.put("nationalityCode", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsNationality()));
			dependent.put("nationalityOthers", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsNatyDets()));
			dependent.put("languageWritten", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsEngWritten()));
			dependent.put("languageSpoken", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsEngSpoken()));
//			dependent.put("smoker", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsSmoking()));
			dependent.put("smoker", "N");
			dependent.put("emailAddress", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsPersEmail()));
			dependent.put("maritalStatus", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsMartsts()));
			dependent.put("nricPassport", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsNric()));

//			dependent.put("race", PsgUtil.emptyValue(customerDetails.getRace()));
//			dependent.put("weight", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsWeight()));
//			dependent.put("height", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsHeight()));
			
			dependent.put("annualIncome", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsAnnlIncome()));
			
			dependent.put("race", "I");
			dependent.put("weight", "58");
			dependent.put("height", "164");
			dependent.put("natureOfWork", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsBusinatr()));
			dependent.put("nameOfCompanySchool", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsCompname()));

			JSONObject contactNoDependent = new JSONObject();
			contactNoDependent.put("office", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsOffice()));
			contactNoDependent.put("mobile", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsHp()));
			contactNoDependent.put("home", PsgUtil.emptyValue(fnaSelfSpouse.getDfSpsHome()));
			dependent.put("contactNo", contactNoDependent);

			JSONObject addressDependent = new JSONObject();
			addressDependent.put("addressType", "Singapore");
			addressDependent.put("address1", PsgUtil.emptyValue(customerDetails.getResAddr1()));
			addressDependent.put("address2", PsgUtil.emptyValue(customerDetails.getResAddr2()));
			addressDependent.put("address3", PsgUtil.emptyValue(customerDetails.getResAddr3()));
			addressDependent.put("address4", "Singapore");
			addressDependent.put("postalCode", PsgUtil.emptyValue(customerDetails.getResPostalCode()));
			addressDependent.put("unit", "10");
			dependent.put("address", addressDependent);

			dependents.add(dependent);
		}
		dependentsForm.put("dependents", dependents);

		clientInfoForm.put("dependentsForm", dependentsForm);

		kycDetails.put("clientInfoForm", clientInfoForm);

		return kycDetails.toJSONString();
	}

	// Sample
	@GetMapping("/getAccessToken")
	public String getAccessToken() {
		String resultStr = "Not found!";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		String client_id = "l7xx11701044d2ac4578a42101a74ee7b25e";
		String client_secret = "0be1fb33dd43419ba5160e3f99141d49";
		String grant_type = "Client_credentials";

		String strPathParam = "client_id=" + client_id + "&client_secret=" + client_secret + "&grant_type="
				+ grant_type;
		String url = "https://api-sandbox.income.com.sg/auth/oauth/v2/token?" + strPathParam;

		ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.POST, null, String.class);
		logger.info("result: " + result);
		if (result != null) {
			logger.info("result body: " + result.getBody());
			if (result.getBody() != null) {
				resultStr = result.getBody();
				try {
					logger.info("result body in string: " + mapper.writeValueAsString(result.getBody()));
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
			}
		}
		return resultStr;
	}

	// Sample
	@SuppressWarnings("unchecked")
	@GetMapping("/getAccessToken2")
	public Map<String, String> getAccessToken2() {
		Map<String, String> resultMap = new HashMap<>();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		String client_id = "l7xx11701044d2ac4578a42101a74ee7b25e";
		String client_secret = "0be1fb33dd43419ba5160e3f99141d49";
		String grant_type = "Client_credentials";

//		String strPathParam = "client_id=" + client_id + "&client_secret="+ client_secret + "&grant_type=" + grant_type;
		String tokenurl = "https://api-sandbox.income.com.sg/auth/oauth/v2/token";

		MultiValueMap<String, String> paramMap = new LinkedMultiValueMap<String, String>();
		paramMap.add("client_id", client_id);
		paramMap.add("client_secret", client_secret);
		paramMap.add("grant_type", grant_type);

		HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<MultiValueMap<String, String>>(
				paramMap, headers);

		try {
			ResponseEntity<String> resp = restTemplate.postForEntity(tokenurl, requestEntity, String.class);
			if (resp != null) {
				if (resp.getBody() != null) {
					resultMap.put("status", "success");
					Map<String, Object> bodyMap = mapper.readValue(resp.getBody(), Map.class);
					logger.info("access token: " + bodyMap.get("access_token"));
					logger.info("body: " + resp.getBody());
					logger.info("status code value: " + resp.getStatusCodeValue());
					logger.info("status code: " + resp.getStatusCode());
					resultMap.put("access token", (String) bodyMap.get("access_token"));
				}
			}
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException httpEx) {
			httpEx.printStackTrace();
			resultMap.put("status", "failed");
			resultMap.put("reason", httpEx.getResponseBodyAsString());
			logger.info("message: " + httpEx.getResponseBodyAsString());
			logger.info("status text: " + httpEx.getStatusText());
			logger.info("status code: " + httpEx.getRawStatusCode());
		} catch (Exception ex) {
			ex.printStackTrace();
			resultMap.put("status", "failed");
			resultMap.put("reason", ex.getMessage());
		}
		return resultMap;
	}

	// Sample
	@GetMapping("/getAccessToken3")
	public Map<String, String> getAccessToken3() {
		Map<String, String> resultMap = new HashMap<>();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		String client_id = "psg-client-demo";
		String client_secret = "ea6ad78b-2e07-49cd-80e6-45ba7797eecf";

		String tokenUrl = "https://secure-ekyc-uatcloud.avallis.com/PSGXS/psgauth/token";

		MultiValueMap<String, String> tokenParamMap = new LinkedMultiValueMap<String, String>();
		tokenParamMap.add("client_id", client_id);
		tokenParamMap.add("client_secret", client_secret);

		HttpEntity<MultiValueMap<String, String>> tokenRequestEntity = new HttpEntity<MultiValueMap<String, String>>(
				tokenParamMap, headers);

		try {
			ResponseEntity<String> tokenResp = restTemplate.postForEntity(tokenUrl, tokenRequestEntity, String.class);
			if (tokenResp != null) {
				if (tokenResp.getBody() != null) {
					String submitUrl = "https://secure-ekyc-uatcloud.avallis.com/PSGXS/api/v1/esubmit/test";

					headers.setBearerAuth(tokenResp.getBody());

					HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(headers);

					ResponseEntity<String> resp = restTemplate.exchange(submitUrl, HttpMethod.GET, requestEntity,
							String.class);

					if (resp != null) {
						if (resp.getBody() != null) {
							resultMap.put("status", "success");
							resultMap.put("result", resp.getBody());
						}
					}
				}
			}
		} catch (HttpClientErrorException | HttpServerErrorException | UnknownHttpStatusCodeException httpEx) {
			httpEx.printStackTrace();
			resultMap.put("status", "failed");
			resultMap.put("reason", httpEx.getResponseBodyAsString());
		} catch (Exception ex) {
			ex.printStackTrace();
			resultMap.put("status", "failed");
			resultMap.put("reason", ex.getMessage());
		}
		return resultMap;
	}
	
	@GetMapping("/ntucRetrievePolicy/{fnaId}")
	public Map<String, Object> ntucRetrievePolicy(@PathVariable String fnaId, HttpServletRequest request) {
		return ntucCommonService(fnaId, ntucRetrievePolicyUrl, request, HttpMethod.GET);
	}
	
	@GetMapping("/ntucRetrieveFileInfo/{fnaId}")
	public Map<String, Object> ntucRetrieveFileInfo(@PathVariable String fnaId, HttpServletRequest request) {
		return ntucCommonService(fnaId, ntucRetrieveFileInfoUrl, request, HttpMethod.GET);
	}
	
	@GetMapping("/ntucDsfUpdateSts/{fnaId}")
	public Map<String, Object> ntucDsfUpdateSts(@PathVariable String fnaId, HttpServletRequest request) {
		logger.info("fnaId: " + fnaId);
		return ntucCommonService(fnaId, ntucDsfUpdateStsUrl, request, HttpMethod.PUT);
	}
	
	@GetMapping("/ntucDsfApprovalUpdateSts/{fnaId}")
	public Map<String, Object> ntucDsfApprovalUpdateSts(@PathVariable String fnaId, HttpServletRequest request) {
		logger.info("fnaId: " + fnaId);
		return ntucCommonService(fnaId, ntucDsfApprovalUpdateStsUrl, request, HttpMethod.POST);
	}
	
	public Map<String, Object> ntucCommonService(String fnaId, String ntucUrl, HttpServletRequest request,
			HttpMethod httpMethod) {
		Map<String, Object> resultMap = new HashMap<>();
		
		FnaSelfSpouseDets fnaSelfSpouseDetails = selfSpouse.getDataById(fnaId, PsgConst.FNA_ID,
				PsgConst.FNA_SELF_SPOUSE_DETS);
		
		if (fnaSelfSpouseDetails != null) {
			
			try {
				List<String> principalList = new ArrayList<>();
				principalList.add("ntuc_income");
				
				String fnaDetailsJson = formFNADetails(fnaSelfSpouseDetails, principalList, request);
				
				HttpHeaders headers = new HttpHeaders();
				
//				HttpEntity<String> reqEntity = new HttpEntity<>(headers);
				
//				String caseId = request.getParameter("caseId");
//				logger.info("Case Id: " + caseId);
//				if (caseId == null || caseId.isEmpty()) {
////					String caseDetsUrl = "http://localhost:7206/PSGXS/ui/getCaseInfoByFnaId/" + fnaId;
//					String caseInfoUrl = caseDetsUrl + fnaId;
//					logger.info("caseInfoUrl: " + caseInfoUrl);
//					
//					ResponseEntity<String> resEntity = restTemplate.exchange(caseInfoUrl, HttpMethod.GET,
//							reqEntity, String.class);
//					
//					if (resEntity != null && resEntity.getBody() != null) {
//						logger.info("res body: " + resEntity.getBody());
//						if ("No submission!".equals(resEntity.getBody())) {
//							resultMap.put("reason", resEntity.getBody());
//							resultMap.put("status", "failed");
//						} else {
//							Map<String, String> submissionMap = mapper.readValue(resEntity.getBody(), Map.class);
//							if (submissionMap != null) {
//								if (submissionMap.containsKey("caseId") && submissionMap.get("caseId") != null) {
//									caseId = submissionMap.get("caseId");
//								}
//							}
//						}
//					}
//				}
				
//				if (caseId != null) {
//					MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
					
//					paramMap.add("fa_payload", fnaDetailsJson);
//					paramMap.add("ntuc_case_id", caseId);
//					paramMap.add("fa_code", "AFA");
					
//					request.getSession(false).setAttribute("caseId", fnaId + "_" + caseId);
					
					String dsfStsName = request.getParameter("dsfStsName") != null ? request.getParameter("dsfStsName") : ""; ;
					logger.info("dsfStsName: " + dsfStsName);
					
					
					
//					MultiValueMap<String, Object> paramMap = new LinkedMultiValueMap<>();
					
					
					
					UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(ntucUrl)
					        .queryParam("fnaId",fnaId)
					        .queryParam("principalId", "ntuc_income")
					        .queryParam("faCode", "AFA");
				
//					
//					paramMap.add("fnaId", fnaId);
//					paramMap.add("principalId", "ntuc_income");
//					paramMap.add("faCode", "AFA");
					
					if (dsfStsName != null) {
//						paramMap.add("dsfStatus", dsfStsName);
//						paramMap.add("pdfFile", ""); // base64 format value
						
						builder.queryParam("dsfStatus", dsfStsName)
					        .queryParam("pdfFile", "");
					}
					
					HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(headers);
					
					
					System.out.println(ntucUrl );
					System.out.println(requestEntity);
//					System.out.println(paramMap);
					
					ResponseEntity<String> responseEntity = restTemplate.exchange(builder.toUriString(), httpMethod, requestEntity, String.class);
					
					if (responseEntity != null && responseEntity.getBody() != null) {
						logger.info("ntuc body: " + responseEntity.getBody());
						Map<String, Object> docMap = mapper.readValue(responseEntity.getBody(), Map.class);
						if (docMap != null && docMap.containsKey("documents") && docMap.containsKey("return")) {
							if ("fileExist".equals(docMap.get("return"))) {
								try {
									List<Map<String, Object>> docList = (List<Map<String, Object>>) docMap.get("documents");
									List<Object> ntucDocIdList = ntucDocIdList(fnaId);
									String caseId = (String) docMap.get("caseId");
									for (Map<String, Object> doc : docList) {
										if (ntucDocIdList != null && ntucDocIdList.size() > 0 && ntucDocIdList.contains(doc.get("docId"))) {
											continue;
										}
										saveNtucDocs(doc, fnaId, fnaSelfSpouseDetails.getFnaDetails(), caseId);
									}
									resultMap.put("status", "success");
									resultMap.put("docList", docList);
								} catch (IOException ioEx) {
									ioEx.printStackTrace();
									resultMap.put("result", ioEx.getMessage());
									resultMap.put("status", "failed");
									return resultMap;
								} catch (Exception ex) {
									ex.printStackTrace();
									resultMap.put("result", ex.getMessage());
									resultMap.put("status", "failed");
									return resultMap;
								}
							} else if ("fileNotExist".equals(docMap.get("return"))) {
								resultMap.put("status", "failed");
								resultMap.put("reason", "Documents are not available for your NTUC Case ID!");
							} else {
								resultMap = docMap;
							}
						} else if (docMap != null && docMap.containsKey("result")) {
							JSONObject jsonObject = (JSONObject) parser.parse(responseEntity.getBody());
							System.out.println("jsonObject from retrieve policy: " + jsonObject);
							resultMap = docMap;
							if (jsonObject != null) {
								if (jsonObject.containsKey("return") && "success".equals(jsonObject.get("return"))
										&& jsonObject.containsKey("result")) {
									JSONObject resultObj = (JSONObject) jsonObject.get("result");
									JSONArray dataArray = (JSONArray) resultObj.get("data");
									System.out.println("1");
									if (dataArray != null && jsonObject.containsKey("caseId")) {
										System.out.println("2");
										if (dataArray.size() > 0) {
											System.out.println("3");
//										if (dataArray != null && jsonObject.containsKey("caseId")) { // testing
											String ntucCaseId = (String) jsonObject.get("caseId");
											for(int dataIndex = 0; dataIndex < dataArray.size(); dataIndex++) {
												JSONObject dataJson = (JSONObject) dataArray.get(dataIndex);
//												JSONObject dataJson = new JSONObject(); // testing
//												dataJson.put("productId", "Enhanced Incomeshield"); // testing
												if (dataJson != null) {
													saveNtucPolicyDets(dataJson, fnaSelfSpouseDetails.getFnaDetails(),
															request, ntucCaseId);
												}
											}
											resultMap.put("message", "NTUC Policy Details saved successfully");
										} else if (dataArray.size() == 0) {
											System.out.println("4");
											resultMap.put("message", "Policy details empty!");
										}
										System.out.println("5");
									}
								}
							}
							resultMap.put("status", "info");
						} else if (docMap != null && docMap.containsKey("return")) {
							resultMap = docMap;
							if ("failed".equals(docMap.get("return"))) {
								resultMap.put("status", "failed");
								resultMap.put("reason", docMap.get("message"));
							} else if ("success".equals(docMap.get("return"))) {
								resultMap.put("status", "success");
								resultMap.put("message", "Case status is changed to " + docMap.get("caseStatus") + "!");
							}
						} else {
							resultMap = docMap;
						}
					}
//				}
			} catch (Exception ex) {
				ex.printStackTrace();
				resultMap.put("result", ex.getMessage());
				resultMap.put("status", "failed");
				return resultMap;
			}
		} else {
			resultMap.put("result", "Invalid FNA id!");
			resultMap.put("status", "failed");
		}
		return resultMap;		
	}
	
	public void saveNtucPolicyDets(JSONObject policyJson, FnaDetails fnaDetails, HttpServletRequest request,
			String ntucCaseId) {
		FnaNtucPolicyDets ntucPolicy = new FnaNtucPolicyDets();
		ntucPolicy.setFnaDetails(fnaDetails);
		if (policyJson.containsKey("productId")) {
			ntucPolicy.setProductId((String) policyJson.get("productId"));
		}
		if (policyJson.containsKey("policyNo")) {
			ntucPolicy.setPolicyNo((String) policyJson.get("policyNo"));
		}
		if (ntucCaseId != null && !ntucCaseId.isEmpty()) {
			ntucPolicy.setNtucCaseId(ntucCaseId);
			ntucPolicy.setCreatedDate(new Date());
			if (request != null && request.getSession(false) != null) {
				String loggedInUserId = (String) request.getSession(false).getAttribute("loggedInUserId");
	
				if (loggedInUserId != null) {
					UserLogin userLogin = loginService.getDataById(loggedInUserId, "user_id", "UserLogin");
					if (userLogin != null && userLogin.getAdvStfId() != null) {
						ntucPolicy.setCreatedBy(userLogin.getAdvStfId().getAdvStfName());
					}
				}
			}
			ntucPolicyService.saveData(ntucPolicy);
		}
	}
	
	public List<Object> ntucDocIdList(String fnaId) {
		return ntucDocsService.getParticularColData(FnaNtucDocs.class, "ndId", "fnaDetails", "fnaId", fnaId);
	}
	
//	public void saveNtucDocs(List<Map<String, Object>> docList, String fnaId, FnaDetails fnaDetails, String caseId) throws Exception {
	public void saveNtucDocs(Map<String, Object> doc, String fnaId, FnaDetails fnaDetails, String caseId) throws Exception {
//		List<Object> ntucDocIdList = ntucDocIdList(fnaId);
//		logger.info("ntucDocIdList: " + ntucDocIdList);
//		List<Map<String, Object>> docList = (List<Map<String, Object>>) docMap.get("docList");
//		if (docList != null && docList.size() > 0) {
//			FnaNtucDocs ntucDoc = null;
//			for (Map<String, Object> doc : docList) {
//				if (ntucDocIdList != null && ntucDocIdList.size() > 0 && ntucDocIdList.contains(doc.get("docId"))) {
//					continue;
//				}
				System.out.println("doc: " + mapper.writeValueAsString(doc));
				FnaNtucDocs ntucDoc = new FnaNtucDocs();
				if (doc.containsKey("docId")) {
					ntucDoc.setNdId((String) doc.get("docId"));
				}
				if (caseId != null && !caseId.isEmpty()) {
					ntucDoc.setNtucCaseId(caseId);
				}
				if (doc.containsKey("fileSecret")) {
					ntucDoc.setDocDataKey((String) doc.get("fileSecret"));
				}
				if (doc.containsKey("fileDesc")) {
					ntucDoc.setDocDesc((String) doc.get("fileDesc"));
				}
				if (doc.containsKey("fileType")) {
					ntucDoc.setDocFileType((String) doc.get("fileType"));
				}
				if (doc.containsKey("fileSize")) {
					ntucDoc.setDocFileSize(String.valueOf(doc.get("fileSize")));
				}
				if (doc.containsKey("fileName")) {
					ntucDoc.setDocFileName(String.valueOf(doc.get("fileName")));
				}
				if (ntucDoc.getNdId() != null && doc.containsKey("fileContent") && ntucDoc.getDocFileType() != null) {
					
					ntucDoc.setDocFilePath(ntucDocslocalFilePath + fnaId + "/" + ntucDoc.getNdId() + "." + ntucDoc.getDocFileType());
					
					byte[] docBytes = Base64.getDecoder().decode(String.valueOf(doc.get("fileContent")));
					FileUtils.writeByteArrayToFile(new File(ntucDoc.getDocFilePath()), docBytes);
					
					ntucDoc.setFnaDetails(fnaDetails);
					ntucDoc.setCreatedDate(new Date());
					
					try {
						ntucDocsService.saveData(ntucDoc);
					} catch (Exception ex) {
						ex.printStackTrace();
						throw new Exception(ex.getMessage());
					}
				}
//			}
//		}
	}
	
	@GetMapping("/getCaseInfoByFnaId/{fnaId}")
	public String getCaseInfoByFnaId(@PathVariable String fnaId) throws JsonProcessingException {
		Map<String, Object> resultMap = new HashMap<>();
		
		String caseInfoUrl = caseDetsUrl;
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(caseInfoUrl)
		        .queryParam("fnaId", fnaId)
		        .queryParam("principalId", "ntuc_income")
		        .queryParam("faCode", "AFA");
		
		try {
			ResponseEntity<String> resEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, null, String.class);
			
			if (resEntity != null && resEntity.getBody() != null) {
				if ("No submission!".equals(resEntity.getBody())) {
					resultMap.put("reason", resEntity.getBody());
					resultMap.put("status", "failed");
				} else {
					
					Map<String, String> caseInfo = mapper.readValue(resEntity.getBody(), Map.class);
					if (caseInfo != null) {
						FnaDetails fnaDetails = fnaDets.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
//						if (fnaDetails.getNtucCaseId() == null || fnaDetails.getNtucCaseStatus() == null) {
							fnaDetails.setNtucCaseId(caseInfo.get("caseId"));
							fnaDetails.setNtucCaseStatus(caseInfo.get("caseStatus"));
							fnaDets.update(fnaDetails);
//						}
					}
					
					resultMap.put("result", resEntity.getBody());
					resultMap.put("status", "success");
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			resultMap.put("reason", ex.getMessage());
			resultMap.put("status", "failed");
			return mapper.writeValueAsString(resultMap);
		}
		return mapper.writeValueAsString(resultMap);
	}
	
	@GetMapping("/openSafari")
	public String openSafariBrowser() {
		try {
			String url = "ezsub://?";
//			String url = "https://google.com";
			Runtime.getRuntime().exec(new String[]{"cmd", "/c", "start safari " + url});
			return "success";
		} catch (IOException ioEx) {
			ioEx.printStackTrace();
			return "failed";
		}
	}
	
//	Testing
//	Working
	@PostMapping("/encrypt")
	public static String encrypt(@RequestBody String payload) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {

//		String payload = "{\"productCode\": null,\"module\": \"QT\",\"quotationId\": null,\"efnaTransNo\": \"AFA41801783190814175025\",\"prospects\": [{\"givenName\": \"Testing \",\"familyName\": \"Testing \",\"dob\": \"23071970\",\"occupationName\": \"A419\",\"smoker\": \"N\",\"gender\": \"M\",\"type\": \"CLT\",\"nationality\": \"SG\",\"id\": \"S1234567D\",\"efnaProfileId\": \"C47009d4d61fe1052\"}],\"efnaQuotationId\": \"AFA41801783190814175025_190814175033\",\"adviser\": {\"agentCode\": \"41801783\"}}";
//		String payload = "\"payload\": {      \"productCode\": null,      \"module\": \"QT\",\r\n"
//				+ "      \"quotationId\": null,      \"efnaTransNo\": \"AFA41801783190814175025\",      \"prospects\": [\r\n"
//				+ "        {          \"givenName\": \"Testing \",          \"familyName\": \"Testing \",\r\n"
//				+ "          \"dob\": \"23071970\",          \"occupationName\": \"A419\",\r\n"
//				+ "          \"smoker\": \"N\",          \"gender\": \"M\",\r\n"
//				+ "          \"type\": \"CLT\",          \"nationality\": \"SG\",          \"id\": \"S1234567D\",\r\n"
//				+ "          \"efnaProfileId\": \"C47009d4d61fe1052\"        }\r\n"
//				+ "      ],\r\n"
//				+ "      \"efnaQuotationId\": \"AFA41801783190814175025_190814175033\",\r\n"
//				+ "      \"adviser\": {        \"agentCode\": \"41801783\"      }    }";
		String secretKey = "WyKFwP2RSlKfRU+uQJOuB4Z6kSnBjJ/kuZHBP3JT4zI=";

		byte[] keyBytes = Base64.getDecoder().decode(secretKey);

		Random rd = new Random();

		byte[] arr = new byte[16];
		rd.nextBytes(arr);

		SecretKey key = new SecretKeySpec(keyBytes, "AES");

		IvParameterSpec iv = new IvParameterSpec(arr);

		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, iv);

		byte[] enc = cipher.doFinal(payload.getBytes("UTF-8"));

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bos.write(iv.getIV());
		bos.write(enc);

		byte[] combined = bos.toByteArray();

		String enccc = Base64.getEncoder().encodeToString(combined);
		System.out.println("enccc: " + enccc);
//		decrypt(combined);
		return enccc;
	}

//	Testing
//	Working
	@PostMapping("/decrypt")
	public static String decrypt(@RequestBody String encData) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, IOException {
		String secretKey = "WyKFwP2RSlKfRU+uQJOuB4Z6kSnBjJ/kuZHBP3JT4zI=";

		byte[] keyBytes = Base64.getDecoder().decode(secretKey);

		byte[] raw = Base64.getDecoder().decode(encData);
		byte[] ivByte = new byte[16];

		byte[] encrypted = new byte[raw.length - 16];
		System.arraycopy(raw, 0, ivByte, 0, 16);
		System.arraycopy(raw, 16, encrypted, 0, raw.length - 16);

		IvParameterSpec iv = new IvParameterSpec(ivByte);

		SecretKey originalKey = new SecretKeySpec(keyBytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
		cipher.init(Cipher.DECRYPT_MODE, originalKey, iv);

		byte[] dec = cipher.doFinal(encrypted);

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		bos.write(dec);
		byte[] output = bos.toByteArray();

		System.out.println("dec: " + new String(output, "UTF-8"));
		return new String(output);
	}
	
}
