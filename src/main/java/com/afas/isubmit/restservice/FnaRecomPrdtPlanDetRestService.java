package com.afas.isubmit.restservice;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.FnaRecomPrdtPlanDet;
import com.afas.isubmit.service.FnaRecommPrdtPlanDetService;
import com.afas.isubmit.service.MasterPrincipalService;
import com.afas.isubmit.util.ErrMsgConstants;
import com.afas.isubmit.util.ErrorMsg;
import com.afas.isubmit.util.PsgConst;

@RestController
@RequestMapping("/RecommPrdtPlan")
public class FnaRecomPrdtPlanDetRestService {

	@Autowired
	FnaRecommPrdtPlanDetService prdtService;

	@Autowired
	MasterPrincipalService prinService;

	@PostMapping(value = "/saveData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaRecomPrdtPlanDet saveProductDetails(@RequestBody FnaRecomPrdtPlanDet fnaPrdtDet) {
		ErrorMsg err = new ErrorMsg();
		try {
			String recomPpId = fnaPrdtDet.getRecomPpId();
			if (recomPpId == null || recomPpId.isEmpty()) {
				prdtService.saveData(fnaPrdtDet);
				// MasterPrincipal prin=prinService.getDataById(fnaPrdtDet.getRecomPpPrin(),
				// Constants.PRIN_ID, Constants.MASTER_PRINCIPAL);
				// fnaPrdtDet.setRecomPpPrin(prin.getPrinName());
				err.setErrMsg(ErrMsgConstants.SUCCESS_MSG);
			} else {
				prdtService.update(fnaPrdtDet);
				err.setErrMsg(ErrMsgConstants.UPDATE_MSG);
			}
		} catch (Exception e) {
			e.printStackTrace();
			err.setErrMsg(e.getMessage());
			// return ResponseEntity.ok().body(err);
		}
		return fnaPrdtDet;
	}

	@PostMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaRecomPrdtPlanDet updateProductDetails(@RequestBody FnaRecomPrdtPlanDet fnaPrdtDet) {
		ErrorMsg err = new ErrorMsg();
		try {
			prdtService.update(fnaPrdtDet);
			err.setErrMsg(ErrMsgConstants.UPDATE_MSG);
		} catch (Exception e) {
			e.printStackTrace();
			err.setErrMsg(e.getMessage());
		}
		return fnaPrdtDet;
	}

	@GetMapping(value = "/getAllProdRecomDets/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaRecomPrdtPlanDet> getAllProdDetsById(@PathVariable("fnaId") String fnaId) {
		List<FnaRecomPrdtPlanDet> prdtDetails = prdtService.getAllDataById(PsgConst.FNA_ID, fnaId,
				PsgConst.FNA_RECOMM_PRDT_DETS);
//		List<FnaRecomPrdtPlanDet> prdtRecomDets=new ArrayList<FnaRecomPrdtPlanDet>();
//		for(FnaRecomPrdtPlanDet prdt:prdtDetails) {
//			MasterPrincipal prin=prinService.getDataById(prdt.getRecomPpPrin(), Constants.PRIN_ID, Constants.MASTER_PRINCIPAL);
//			prdt.setRecomPpPrin(prin.getPrinName());
//			prdtRecomDets.add(prdt);
//		}
		return prdtDetails;
	}

	@DeleteMapping(value = "/deleteBasicPlnSet/{prodVal}")
	public ResponseEntity<ErrorMsg> deleteBasicPlanSet(@PathVariable("prodVal") String prodVal) {
		ErrorMsg errMsg = new ErrorMsg();
		try {
			prdtService.delete(prodVal, PsgConst.RECOM_PP_PRODNAME, PsgConst.FNA_RECOMM_PRDT_DETS);
			prdtService.delete(prodVal, PsgConst.RECOM_BASIC_REF, PsgConst.FNA_RECOMM_PRDT_DETS);
			errMsg.setErrMsg(ErrMsgConstants.DLT_MSG);
		} catch (Exception e) {
			errMsg.setErrMsg(e.getMessage());
			return ResponseEntity.ok().body(errMsg);
		}
		return ResponseEntity.ok().body(errMsg);
	}

	@DeleteMapping(value = "/deleteBscRidPln/{recomPpId}")
	public ResponseEntity<ErrorMsg> deleteBasicPlan(@PathVariable("recomPpId") String recomPpId) {
		ErrorMsg errMsg = new ErrorMsg();
		try {
			prdtService.delete(recomPpId, PsgConst.RECOM_PP_ID, PsgConst.FNA_RECOMM_PRDT_DETS);
			errMsg.setErrMsg(ErrMsgConstants.DLT_MSG);
		} catch (Exception e) {
			errMsg.setErrMsg(e.getMessage());
			return ResponseEntity.ok().body(errMsg);
		}
		return ResponseEntity.ok().body(errMsg);
	}

	@DeleteMapping(value = "/deleteClient/{clientName}")
	public ResponseEntity<ErrorMsg> deleteClient(@PathVariable("clientName") String clientName) {
		ErrorMsg errMsg = new ErrorMsg();
		try {
			prdtService.delete(clientName, PsgConst.RECOM_PP_NAME, PsgConst.FNA_RECOMM_PRDT_DETS);
			errMsg.setErrMsg(ErrMsgConstants.DLT_MSG);
		} catch (Exception e) {
			errMsg.setErrMsg(e.getMessage());
			return ResponseEntity.ok().body(errMsg);
		}
		return ResponseEntity.ok().body(errMsg);
	}

	@DeleteMapping(value = "/deleteByPrinName/{prinName}")
	public ResponseEntity<ErrorMsg> deleteByPrinName(@PathVariable("prinName") String prinName) {
		ErrorMsg errMsg = new ErrorMsg();
		try {
			prdtService.delete(prinName, PsgConst.RECOM_PP_PRIN, PsgConst.FNA_RECOMM_PRDT_DETS);
			errMsg.setErrMsg(ErrMsgConstants.DLT_MSG);
		} catch (Exception e) {
			errMsg.setErrMsg(e.getMessage());
			return ResponseEntity.ok().body(errMsg);
		}
		return ResponseEntity.ok().body(errMsg);
	}

}
