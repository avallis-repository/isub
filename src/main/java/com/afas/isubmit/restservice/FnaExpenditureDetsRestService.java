package com.afas.isubmit.restservice;

import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaExpenditureDets;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaExpenditureDetsService;
import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/ExpendDets")
public class FnaExpenditureDetsRestService {
	
	private static Logger logger = LogManager.getLogger(FnaExpenditureDetsRestService.class);

	@Autowired
	FnaExpenditureDetsService expendService;

	@Autowired
	FnaDetailsService fnaDetsService;
	
	ObjectMapper mapper = new ObjectMapper();

	@PostMapping(value = "/saveData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaExpenditureDets saveData(@RequestBody FnaExpenditureDets fnaExp, HttpServletRequest request) {
		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
		FnaDetails fnaDets = fnaDetsService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
		fnaDets.setFwrFinAssetFlg(PsgConst.YES_VALUE);
		fnaDetsService.update(fnaDets);

		fnaExp.setFnaDetails(fnaDets);
		fnaExp.setCreatedDate(new Date(System.currentTimeMillis()));
		String expdId = fnaExp.getExpdId();
		logger.info("Expd id-------" + expdId);
		if (expdId.equals("null") || expdId.isEmpty()) {
			expendService.saveData(fnaExp);
		} else {
			expendService.update(fnaExp);
		}

		try {
			logger.info("fnaExp: " + mapper.writeValueAsString(fnaExp));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return fnaExp;
	}

	@GetMapping(value = "/getDataById/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public FnaExpenditureDets getDataById(@PathVariable String fnaId, HttpServletRequest request) {
		FnaExpenditureDets expdDets = expendService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_EXPENDITURE_DETS);
//		if(expdDets !=null ) {
//			String expdId=expdDets.getExpdId();
//			if(!expdId.isEmpty() && expdId !=null) {
//				request.getSession().setAttribute(PsgConst.SESS_EXPD_ID,expdId);
//			}
//		}
		return expdDets;
	}

}
