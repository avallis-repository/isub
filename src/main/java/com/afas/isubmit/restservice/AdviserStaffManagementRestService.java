package com.afas.isubmit.restservice;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.afas.isubmit.dto.MasterAdviser;
import com.afas.isubmit.dto.MasterPrincipal;
import com.afas.isubmit.dto.MasterTenantDist;
import com.afas.isubmit.dto.PrincipalInfo;
import com.afas.isubmit.dto.UserLogin;
import com.afas.isubmit.service.MasterAdviserService;
import com.afas.isubmit.service.MasterPrincipalService;
import com.afas.isubmit.service.MasterTenantDistService;
import com.afas.isubmit.service.PrincipalInfoService;
import com.afas.isubmit.service.UserLoginService;
import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/AdviserStaffManagementDetails")
public class AdviserStaffManagementRestService {
	
	@Autowired
	MasterAdviserService  masterAdviserManagementService;
	
	@Autowired
	PrincipalInfoService  princinfoService;
	
	@Autowired
	MasterPrincipalService princService;
	
	@Autowired
	MasterTenantDistService masterTenantDistService;
	
	@Autowired
	UserLoginService userService;
	
    ObjectMapper mapper = new ObjectMapper();
	
	ResourceBundle resource = ResourceBundle.getBundle("properties/psgisubmit", Locale.ENGLISH);
 
	ModelAndView mv = new ModelAndView();

	@GetMapping("/masterAdviserManagementDistList")
	@ResponseBody
	public List<MasterAdviser> adviserManagementDistList(HttpServletRequest request){
		//List<MasterTenantDist> tenantDisList = masterTenantDistService.getAllDataByIdnotequal("MasterTenantDist");
		//return  masterAdviserManagementService.getAllData("MasterAdviser");
		String stradminAdviserId = null;
		if (request != null && request.getSession(false) != null) {
			String adminAdviserId = (String) request.getSession(false).getAttribute("adminAdviserId");
			if (adminAdviserId != null && !adminAdviserId.isEmpty()) {
				stradminAdviserId = adminAdviserId;
			}
		}
		return  masterAdviserManagementService.getAllDataByIdnotequal(PsgConst.ADVSTF_ID, stradminAdviserId, PsgConst.MASTER_ADVISER);
	 
	}

	@GetMapping(value = "/getAllNriadviserStffList/{nric}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<MasterAdviser> getDataByNric(@PathVariable String nric) {
		return masterAdviserManagementService.getAllDataById(PsgConst.NRIC, nric, PsgConst.MASTER_ADVISER);
	}

	@PostMapping("/masterAdviserStaffsaveData")
	@ResponseBody
	public String saveAdviserStaffDets(@RequestBody String masterAdviserStaffDets, HttpServletRequest request,
			HttpServletResponse response) throws JsonMappingException, JsonProcessingException, ParseException {
 
		Map<String, String> resultMap = new HashMap<>();
		MasterAdviser masterAdviserDets = mapper.readValue(masterAdviserStaffDets, MasterAdviser.class);
		try {
			
			if (masterAdviserDets != null) {
				if (request != null && request.getSession(false) != null) {
					String strAdviserName = (String) request.getSession(false).getAttribute("AdviserName");
					if (strAdviserName != null && !strAdviserName.isEmpty()) {
						masterAdviserDets.setCreatedBy(strAdviserName);
					}
				}
				
				 
				if (request != null && request.getSession(false) != null) {
					String strTenantId = (String) request.getSession(false).getAttribute("sessionTenantId");
					System.out.println(strTenantId);
					MasterTenantDist masterTenant = masterTenantDistService.getDataById(strTenantId, PsgConst.TENANT_ID, PsgConst.MASTER_TENANT_DIST);
					if (masterTenant != null ) {
						//masterTenant.setTenantId(strTenantId);
						masterAdviserDets.setTenantId(masterTenant); 
					}
				}
				
				masterAdviserDets.setCreatedDate(new Date());
				String advid =   (String) masterAdviserManagementService.saveData(masterAdviserDets);
				resultMap.put("advid", advid);
				resultMap.put("status", "success");
			}
			 
			 
		} catch (HttpClientErrorException hEx) {
			resultMap.put("status", "failed");
			resultMap.put("response", hEx.getResponseBodyAsString());
			hEx.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return masterAdviserStaffDets;
		return mapper.writeValueAsString(resultMap);
	}
	 
	
	@PostMapping("/SaveDataAdviserCodes")
	@ResponseBody
	public String SaveDataAdviserCodes(@RequestBody String advisercodesDets, HttpServletRequest request,
			HttpServletResponse response) throws JsonMappingException, JsonProcessingException, ParseException {
 
		Map<String, Object> resultMap = new HashMap<>();
		PrincipalInfo advisercodes = mapper.readValue(advisercodesDets, PrincipalInfo.class);
		try {
			
			if (advisercodes != null) {
				if (request != null && request.getSession(false) != null) {
					String strAdviserName = (String) request.getSession(false).getAttribute("AdviserName");
					if (strAdviserName != null && !strAdviserName.isEmpty()) {
						advisercodes.setCreatedBy(strAdviserName);
					}
				}
				advisercodes.setCreatedDate(new Date());
				princinfoService.saveData(advisercodes);
				resultMap.put("advisercodes", advisercodes);
				resultMap.put("status", "success");
			}
			 
			 
		} catch (HttpClientErrorException hEx) {
			resultMap.put("status", "failed");
			resultMap.put("response", hEx.getResponseBodyAsString());
			hEx.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// return masterAdviserStaffDets;
		return mapper.writeValueAsString(resultMap);
	}

 
	@PostMapping("/masterAdviserStaffUpdateData")
	@ResponseBody
	public String masterAdviserStaffUpdateData(@RequestBody String masterAdviserStaffDets, HttpServletRequest request,
			HttpServletResponse response) throws JsonMappingException, JsonProcessingException {

		Map<String, String> resultMap = new HashMap<>();
		MasterAdviser masterAdviserUpdate = mapper.readValue(masterAdviserStaffDets, MasterAdviser.class);
		 
		try {
			
			if (masterAdviserUpdate != null) {
				if (request != null && request.getSession(false) != null) {
					String strAdviserName = (String) request.getSession(false).getAttribute("AdviserName");
					if (strAdviserName != null && !strAdviserName.isEmpty()) {
						masterAdviserUpdate.setModifiedBy(strAdviserName);
					}
				}
				
				 
				if (request != null && request.getSession(false) != null) {
					String strTenantId = (String) request.getSession(false).getAttribute("sessionTenantId");
					MasterTenantDist masterTenant =  masterTenantDistService.getDataById(strTenantId, PsgConst.TENANT_ID, PsgConst.MASTER_TENANT_DIST);
					if (masterTenant != null ) {
						//masterTenant.setTenantId(strTenantId);
						masterAdviserUpdate.setTenantId(masterTenant); 
					}
				}
				
				
				masterAdviserUpdate.setModifiedDate(new Date());
				masterAdviserManagementService.update(masterAdviserUpdate);
				resultMap.put("status", "success");
			}
			
		} catch (HttpClientErrorException hEx) {
			resultMap.put("status", "failed");
			resultMap.put("response", hEx.getResponseBodyAsString());
			hEx.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	//	return masterAdviserStaffDets;
		return mapper.writeValueAsString(resultMap);
	}
	
	
	
	@PostMapping("/UpdateDataAdviserCodes")
	@ResponseBody
	public String UpdateDataAdviserCodes(@RequestBody String advisercodesDets, HttpServletRequest request,
			HttpServletResponse response) throws JsonMappingException, JsonProcessingException {

		Map<String, Object> resultMap = new HashMap<>();
		PrincipalInfo AdviserCodesUpdate = mapper.readValue(advisercodesDets, PrincipalInfo.class);
		 
		try {
			
			if (AdviserCodesUpdate != null) {
				if (request != null && request.getSession(false) != null) {
					String strAdviserName = (String) request.getSession(false).getAttribute("AdviserName");
					if (strAdviserName != null && !strAdviserName.isEmpty()) {
						AdviserCodesUpdate.setModifyBy(strAdviserName);
					}
				}
				AdviserCodesUpdate.setModifyDate(new Date());
				princinfoService.update(AdviserCodesUpdate);
				resultMap.put("CodesUpdate", AdviserCodesUpdate);
				resultMap.put("status", "success");
			}
			
		} catch (HttpClientErrorException hEx) {
			resultMap.put("status", "failed");
			resultMap.put("response", hEx.getResponseBodyAsString());
			hEx.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	//	return masterAdviserStaffDets;
		return mapper.writeValueAsString(resultMap);
	}
	
 	
	@GetMapping("/getMasterAdvMagDets/{advStfId}")
	@ResponseBody
	public MasterAdviser masterAdvMagId(@PathVariable String advStfId, HttpServletRequest request)
			throws JsonProcessingException {
		MasterAdviser editmasteradviserMagList = masterAdviserManagementService.getDataById(advStfId,
				PsgConst.ADVSTF_ID, PsgConst.MASTER_ADVISER);
		return editmasteradviserMagList;
		 
	}

	 @GetMapping("/getPrincInfoDets/{princeinfoid}") 
	 @ResponseBody
	 public PrincipalInfo PrincInfoDets(@PathVariable String princeinfoid,HttpServletRequest request) 
			 throws JsonProcessingException{
		 PrincipalInfo editprincinfo = princinfoService.getDataById(princeinfoid, PsgConst.PRINC_INFO_ID, PsgConst.PRINCIPAL_INFO);
		 return editprincinfo;
	 }
	
	 @GetMapping("/getPrincInfoAdvStfIdDetails/{advStfId}") 
	 @ResponseBody
	 public List<PrincipalInfo> PrincInfoAdvStfId(@PathVariable String advStfId,HttpServletRequest request) 
			 throws JsonProcessingException{
		 List<PrincipalInfo> editprincinfo = princinfoService.getAllDataById(PsgConst.ADVSTF_ID, advStfId,  PsgConst.PRINCIPAL_INFO);
		 return editprincinfo;
	 }

	@DeleteMapping(value = "/deleteMasterAdviserStaff/{advStfId}")
	@ResponseBody
	public Map<String, String> deleteMasterAdviserStaff(@PathVariable("advStfId") String advStfId, HttpServletRequest request)
			throws IOException {
		Map<String, String> resultMap = new HashMap<>();
		try {
		MasterAdviser deleteasteradviserMagList = masterAdviserManagementService.getDataById(advStfId,
				PsgConst.ADVSTF_ID, PsgConst.MASTER_ADVISER);
		
 		
       UserLogin userinfo = userService.getDataById(advStfId, PsgConst.ADVSTF_ID, PsgConst.USER_LOGIN);
		
		if(userinfo !=null) {
			userService.delete(advStfId, PsgConst.ADVSTF_ID, PsgConst.USER_LOGIN);
		}
		
		
		if (deleteasteradviserMagList != null) {
			princinfoService.delete(deleteasteradviserMagList.getAdvStfId(), PsgConst.ADVSTF_ID, PsgConst.PRINCIPAL_INFO);
			
			masterAdviserManagementService.delete(advStfId, PsgConst.ADVSTF_ID, PsgConst.MASTER_ADVISER);
		}
		
		resultMap.put("status", "success");
		
		} catch (Exception e) {
			resultMap.put("status", "failed");
			e.printStackTrace();
		//	return resultMap;
			//errMsg.setErrMsg(e.getMessage());
			//return ResponseEntity.ok().body(errMsg);
		}
		return resultMap;
		
	}
	
	@DeleteMapping(value = "/deleteAdviserCodes/{princeinfoid}")
	@ResponseBody
	public void deleteAdviserCodes(@PathVariable("princeinfoid") String princeinfoid, HttpServletRequest request)
			throws IOException {
		PrincipalInfo principalInfoDets = princinfoService.getDataById(princeinfoid,
				PsgConst.PRINC_INFO_ID, PsgConst.PRINCIPAL_INFO);
		
		if (principalInfoDets != null) {
			princinfoService.delete(princeinfoid, PsgConst.PRINC_INFO_ID, PsgConst.PRINCIPAL_INFO);
			
		}
		
	}
	

	@PostMapping(path = "/adviserStaffFileUpload")
	@ResponseBody
	public String fileUpload(@RequestParam MultipartFile file, HttpServletRequest request) {
		
		String strtenantId = null;
		if (request != null && request.getSession(false) != null) {
			String strTenantId = (String) request.getSession(false).getAttribute("sessionTenantId");
			if (strTenantId != null && !strTenantId.isEmpty()) {
				strtenantId = strTenantId;
			}
		}
		
		String strAdvisername = null;
				
		if (request != null && request.getSession(false) != null) {
			String strAdviserName = (String) request.getSession(false).getAttribute("AdviserName");
			if (strAdviserName != null && !strAdviserName.isEmpty()) {
				strAdvisername = strAdviserName;
			}
		}
		
		//String strtenantId = tenantId;
		adviserstafffileUpload(file, strtenantId,strAdvisername);
		return "masterAdviserManagement";
	}

	public void adviserstafffileUpload(MultipartFile file, String strtenantId, String strAdvisername) {
		try {
			Workbook workbook = new XSSFWorkbook(file.getInputStream());
			saveDataFromExcel(workbook, 1, strtenantId, strAdvisername);
		} catch (IOException e) {
			System.out.println("File is not found");
			e.printStackTrace();
		}
	}

	public void saveDataFromExcel(Workbook workbook, int sheetIndex, String strtenantId, String strAdvisername) {
		Sheet sheet = workbook.getSheet("Master Adviser Staff");

		int totalNumOfRows = sheet.getLastRowNum();

		if (totalNumOfRows > 0) {
			boolean isHeader = true;
			Map<Integer, String> headerPosition = new HashMap<>();
			String headerValue;
			int emptyCell = 0;
			for (int rowIndex = 0; rowIndex <= totalNumOfRows; rowIndex++) {
				boolean  isDuplicate  =	false;
				Row currentRow = sheet.getRow(rowIndex);
				Cell currentCell;
				MasterAdviser adviserstff = null;

				if (!isHeader) {
					adviserstff = new MasterAdviser();
				}
				int totalNumOfCellsInRow = currentRow.getLastCellNum();

				emptyCell = 0;
				if (totalNumOfCellsInRow > 0) {
					for (int cellIndex = 0; cellIndex < totalNumOfCellsInRow; cellIndex++) {
						currentCell = currentRow.getCell(cellIndex);
						if (currentCell == null || currentCell.toString().trim().equals("")) {
							emptyCell++;
						}
						if (isHeader) {
							headerPosition.put(cellIndex, currentCell.getStringCellValue());
						} else {
							headerValue = headerPosition.get(cellIndex);
							try {
							isDuplicate  =	setValuesInAdviserFields(adviserstff, headerValue, currentCell, strtenantId);
							if(isDuplicate) {
								break;
								 
							}
							} catch (ParseException e) {
								e.printStackTrace();
							}
						}
					}
					if(isDuplicate) {
						continue;
					}
				}
				isHeader = false;

				if (totalNumOfCellsInRow > emptyCell) {
					if (adviserstff != null) {
						adviserstff.setCreatedDate(new Date());
						adviserstff.setCreatedBy(strAdvisername);
						masterAdviserManagementService.saveData(adviserstff);
					}
				}
			}
		}
	}

	public boolean setValuesInAdviserFields(MasterAdviser masterStaff, String header, Cell currentCell, String strtenantId)
			throws ParseException {

		MasterTenantDist tenantDisInfo = new MasterTenantDist();
		if (currentCell == null || currentCell.getCellType() == CellType.BLANK) {
			// current cell is empty
		} else if (currentCell.getCellType() == CellType.NUMERIC) {
			 if ("DOB".equalsIgnoreCase(header)) {
					if (DateUtil.isCellDateFormatted(currentCell)) {
						masterStaff.setDob(currentCell.getDateCellValue());
					}
				} else if ("Mobile No".equalsIgnoreCase(header)) {
					masterStaff.setHandPhone(String.valueOf(Double.valueOf(currentCell.getNumericCellValue()).longValue()));
				}
		}else if (currentCell.getCellType() == CellType.STRING) {
			if ("Adviser Name".equalsIgnoreCase(header)) {
				masterStaff.setAdvStfName(currentCell.getStringCellValue());
			}  else if ("NRIC".equalsIgnoreCase(header)) {
			MasterAdviser checkNRIC = masterAdviserManagementService.getDataById(currentCell.getStringCellValue(), PsgConst.NRIC,  PsgConst.MASTER_ADVISER);
			if(checkNRIC != null) {
					return true;
				}else {
				masterStaff.setNric(currentCell.getStringCellValue());
				}
			} else if ("Email Id".equalsIgnoreCase(header)) {
				masterStaff.setEmailId(currentCell.getStringCellValue());
				System.out.println( "email" + currentCell.getStringCellValue());
			}  else if ("Gender".equalsIgnoreCase(header)) {
				masterStaff.setSex(currentCell.getStringCellValue());
			} else if ("Manager".equalsIgnoreCase(header)) {
				masterStaff.setManager(currentCell.getStringCellValue());
			} else if ("Staff Type".equalsIgnoreCase(header)) {
				masterStaff.setStaffType(currentCell.getStringCellValue());
//			} else if ("Tenant Name".equalsIgnoreCase(header)) {
//				tenantDisInfo.setTenantId(strtenantId);
//				masterStaff.setTenantId(tenantDisInfo);
			}
			tenantDisInfo.setTenantId(strtenantId);
			masterStaff.setTenantId(tenantDisInfo);
//			 else if ("Manager Name".equalsIgnoreCase(header)) {
//				 masterStaff.setManagerId(currentCell.getStringCellValue()); }
		}
		return false;
	}

	
	@GetMapping("/getprincNameDataByName/{prinId}")
	@ResponseBody
	public MasterPrincipal princNameDataByName(@PathVariable String prinId, HttpServletRequest request)
			throws JsonProcessingException {
		MasterPrincipal getprincList = princService.getDataById(prinId,
				PsgConst.PRIN_ID, PsgConst.MASTER_PRINCIPAL);
		return getprincList;
		 
	}
	
}
