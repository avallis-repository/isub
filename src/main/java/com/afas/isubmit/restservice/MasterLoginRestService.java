package com.afas.isubmit.restservice;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.MasterAdviser;
import com.afas.isubmit.dto.UserLogin;
import com.afas.isubmit.service.MasterAdviserService;
import com.afas.isubmit.service.UserLoginService;
import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/masterLogin")
public class MasterLoginRestService {
	
	@Autowired
	MasterAdviserService masterAdviserManagementService;

	@Autowired
	UserLoginService userLoginService;
	
	ObjectMapper mapper = new ObjectMapper();

	@GetMapping("/listAdvisers")
	public String listAdvisers(HttpServletRequest request) throws JsonProcessingException {
		List<UserLogin> usersList = userLoginService.getAllDataById("userType", "User", "UserLogin");
		
		String stradminAdviserId = null;
		if (request != null && request.getSession(false) != null) {
			String adminAdviserId = (String) request.getSession(false).getAttribute("adminAdviserId");
			if (adminAdviserId != null && !adminAdviserId.isEmpty()) {
				stradminAdviserId = adminAdviserId;
			}
		}
		
		List<MasterAdviser> advisersList = masterAdviserManagementService.getAllDataByIdnotequal(PsgConst.ADVSTF_ID, stradminAdviserId, PsgConst.MASTER_ADVISER);
		Map<String, Object> resultMap = null;
		List<Map<String, Object>> resultList = new ArrayList<>();
		if (advisersList != null && advisersList.size() > 0) {
			for (MasterAdviser adviser : advisersList) {
				resultMap = new HashMap<>();
				resultMap.put("adviserId", adviser.getAdvStfId());
				resultMap.put("adviserName", adviser.getAdvStfName());
				resultMap.put("adviserEmailId", adviser.getEmailId());
				resultMap.put("tenantId", adviser.getTenantId().getTenantId());
				if (usersList != null && usersList.size() > 0) {
					for (UserLogin userLogin : usersList) {
						if (userLogin.getAdvStfId() != null
								&& adviser.getAdvStfId().equals(userLogin.getAdvStfId().getAdvStfId())) {
							resultMap.put("userLogin", mapper.writeValueAsString(userLogin));
						}
					}
				}
				resultList.add(resultMap);
			}
		}
		return mapper.writeValueAsString(resultList);
	}

	@PostMapping("/createAdviserInfo")
	public String createAdviserInfo(@RequestBody String loginInfo) {
		String result = "Not created";
		System.out.println("loginInfo in create: " + loginInfo);
		try {
			UserLogin userLoginInfo = mapper.readValue(loginInfo, UserLogin.class);
			if (userLoginInfo != null && userLoginInfo.getUserId() != null) {
				userLoginInfo.setCreatedDate(new Date());
				userLoginInfo.setUserType("User");
				userLoginService.saveData(userLoginInfo);
				result = "Created";
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return result;
	}

	@PutMapping("/updateAdviserInfo")
	public String updateAdviserInfo(@RequestBody String loginInfo) {
		String result = "Not updated";
		System.out.println("loginInfo in update: " + loginInfo);
		try {
			UserLogin userLoginInfo = mapper.readValue(loginInfo, UserLogin.class);
			if (userLoginInfo != null) {
				userLoginInfo.setModifiedDate(new Date());
				userLoginService.update(userLoginInfo);
				result = "Updated";
			}
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return result;
		}
		return result;
	}

	@DeleteMapping("/deleteAdviserInfo/{userLoginId}")
	public String deleteAdviserInfo(@PathVariable String userLoginId) {
		String result = "Not deleted";
		System.out.println("userLoginId: " + userLoginId);
		try {
			userLoginService.delete(userLoginId, "userId", "UserLogin");
			result = "Deleted";
		} catch (Exception ex) {
			ex.printStackTrace();
			return result;
		}
		return result;
	}
	
}
