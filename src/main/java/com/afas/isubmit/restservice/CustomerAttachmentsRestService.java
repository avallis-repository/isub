package com.afas.isubmit.restservice;

import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
//import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.afas.isubmit.dto.CustomerAttachments;
import com.afas.isubmit.dto.CustomerDetails;
import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaSelfSpouseDets;
import com.afas.isubmit.fileupload.GcpDelete;
import com.afas.isubmit.fileupload.GcpDownload;
import com.afas.isubmit.fileupload.GcpUpload;
import com.afas.isubmit.service.CustomerAttachmentService;
import com.afas.isubmit.service.CustomerDetailsService;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaSelfSpouseDetsService;
import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;

@RestController
@RequestMapping("/CustomerAttachments")
public class CustomerAttachmentsRestService {

	private static Logger logger = LogManager.getLogger(CustomerAttachmentsRestService.class);
	
	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	CustomerAttachmentService custAttachService;

	@Autowired
	CustomerDetailsService custDetService;

	@Autowired
	FnaDetailsService fnaDetService;

	@Autowired
	FnaSelfSpouseDetsService selfSpouseService;

	@Autowired
	GcpDelete gcpDelete;

	ResourceBundle resource = ResourceBundle.getBundle("properties/psgisubmit", Locale.ENGLISH);

	@Value("${gcp.json.file.path}")
	String gcpPath;

	@Value("${gcp.bucketName}")
	String bucketName;

	@Value("${gcp.projectId}")
	String projId;

//	Logger log=Logger.getLogger(CustomerAttachmentsRestService.class);

	@PostMapping(value = "/saveCustomerAttachment")
	public void saveData(@RequestParam("fileUpload") MultipartFile[] multipleFiles,
			@RequestParam("custAttachments") Object[] custAttachments, HttpServletRequest request) {

		String sessCustId = (String) request.getSession(false).getAttribute(PsgConst.SESS_CUST_ID);
		CustomerDetails custDets = custDetService.getDataById(sessCustId, PsgConst.CUST_ID, PsgConst.CUSTOMER_DETAILS);
		try {
			saveCustomerAttachments(multipleFiles, custDets, custAttachments);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GetMapping(value = "/getAllUploadFile", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerAttachments> getAllUploadFile(HttpServletRequest request) {

		String custId = (String) request.getSession(false).getAttribute(PsgConst.SESS_CUST_ID);

		// FnaDetails fnaDets=fnaDetService.getDataById(fnaId,
		// Constants.FNA_ID,Constants.FNA_DETAILS);
		// CustomerDetails custDets=fnaDets.getCustDetails();
		List<CustomerAttachments> attachList = custAttachService.getAllDataById(PsgConst.CUST_ID, custId,
				PsgConst.CUSTOMER_ATTACHMENTS);
		List<CustomerAttachments> resultList = new ArrayList<>();
		try {
			for (CustomerAttachments custAttach : attachList) {
				byte[] decryptFileData = GcpDownload.downloadGcp(custAttach.getAttachmentLocation(),
						custAttach.getDecryptKey(), gcpPath, bucketName);
				custAttach.setDocumentData(decryptFileData);
//				FileOutputStream fos = new FileOutputStream("d://download//newplain.pdf");
//				fos.write(decryptFileData);
//	    		fos.flush();
//	    		fos.close();
				resultList.add(custAttach);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultList;
		// return ResponseEntity.ok().body(resultList);
	}

	@GetMapping(value = "/getAllUploadFileById/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<CustomerAttachments> getAllUploadFileById(@PathVariable("fnaId") String fnaId,
			HttpServletRequest request) {
		// String
		// custId=(String)request.getSession().getAttribute(PsgConst.SESS_CUST_ID);

		FnaDetails fnaDets = fnaDetService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
		CustomerDetails custDets = fnaDets.getCustDetails();

		List<CustomerAttachments> attachList = custAttachService.getAllDataById(PsgConst.CUST_ID, custDets.getCustId(),
				PsgConst.CUSTOMER_ATTACHMENTS);
		List<CustomerAttachments> resultList = new ArrayList<CustomerAttachments>();
		try {
			for (CustomerAttachments custAttach : attachList) {
				byte[] decryptFileData = GcpDownload.downloadGcp(custAttach.getAttachmentLocation(),
						custAttach.getDecryptKey(), gcpPath, bucketName);
				custAttach.setDocumentData(decryptFileData);
//				FileOutputStream fos = new FileOutputStream("d://download//newplain.pdf");
//				fos.write(decryptFileData);
//	    		fos.flush();
//	    		fos.close();
				resultList.add(custAttach);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultList;
	}

	@PostMapping(value = "/saveData")
	public void saveData(@RequestParam("custDetails") Object objCustDets,
			@RequestParam("fnaSelfSpouseDets") Object fnaSelfSpouseDets, HttpServletRequest request,
			HttpServletResponse response) throws IOException {

//		log.info("Save customer data");

		try {
			CustomerDetails custDets = mapper.readValue(objCustDets.toString(), CustomerDetails.class);
			FnaSelfSpouseDets fnaSpouse = mapper.readValue(fnaSelfSpouseDets.toString(), FnaSelfSpouseDets.class);

			String custId = custDets.getCustId();
			String sessCustId = (String) request.getSession(false).getAttribute(PsgConst.SESS_CUST_ID);
			String clientStatus = (String) request.getSession(false).getAttribute(PsgConst.CLIENT_STATUS);

			// Add client data
			if (clientStatus.equals(PsgConst.CLIENT_ADD)) {
				if (sessCustId != null) {
					setCustomerDetails(custDets);
					custDets.setCustId(sessCustId);
					// updateData(custDets,fnaSpouse,sessCustId,request);

					custDetService.update(custDets);
					// custAttachService.delete(sessCustId,
					// Constants.CUST_ID,"CustomerAttachments");

					/* Update Fna Details */
					String dataFormId = fnaSpouse.getDataFormId();
					FnaDetails fnaDet = selfSpouseService
							.getDataById(dataFormId, PsgConst.DATAFORM_ID, "FnaSelfSpouseDets").getFnaDetails();

					fnaSpouse.setFnaDetails(fnaDet);
					String fnaId = fnaDet.getFnaId();
					selfSpouseService.update(fnaSpouse);
					setUserSessData(fnaSpouse, request);
					setSessionData(custDets.getCustName(), sessCustId, fnaId, dataFormId, request);
					// return ResponseEntity.ok().body("updated your data");
					response.setStatus(200);
					response.getWriter().write("updated your data");

					// }
				} else {
					saveData(custDets, fnaSpouse, request);
					response.setStatus(200);
				}
			} else {
				// Edit client data
				if (custId == null || custId.isEmpty()) {
					saveData(custDets, fnaSpouse, request);
					response.setStatus(200);
				} else {
					updateData(custDets, fnaSpouse, custId, request);
					response.setStatus(200);
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
			response.setStatus(400);
			response.getWriter().write(e.getMessage());
			// return response.getWriter().write(e.getMessage());
		}
		// return ResponseEntity.ok().body("Successfully saved data");
//		 response.setStatus(200);
//         response.getWriter().write("Successfully saved data");
	}

	// Upload file to GCP
	public void uploadFile(byte[] bytes, String fileName, int no) {

		Storage storage = StorageOptions.getDefaultInstance().getService();
		String bucketName = "psg-taspro-test";
		String objectName = "avallis/ntuc_attach/FNA00000000" + no + "/" + fileName;
		// String filePath = "D:\\testdownloadfeatures.png";

		BlobId blobId = BlobId.of(bucketName, objectName);
		BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();

		try {
			storage.create(blobInfo, bytes);
		} catch (Exception e) {
			e.printStackTrace();
		}

		logger.info("File " + fileName + " uploaded to bucket " + bucketName + " as " + objectName);
	}

	private CustomerDetails setCustomerDetails(CustomerDetails custDets) {
		custDets.setFnaType(PsgConst.FNA_TYPE);
		custDets.setAgentIdInitial(resource.getString("AGENT_ID_INITIAL"));
		custDets.setAgentIdCurrent(resource.getString("AGENT_ID_INITIAL"));
		custDets.setCreatedDate(new Date(System.currentTimeMillis()));
		custDets.setCreatedBy(PsgConst.SIGN_CLIENT);
		custDets.setDistributorId(resource.getString("DISTRIBUTOR_ID"));
		return custDets;

	}

	private FnaDetails setFnaDetails(CustomerDetails custDets) {
		FnaDetails fnaDet = new FnaDetails();
		fnaDet.setCustDetails(custDets);
		fnaDet.setAdvstfId(resource.getString("ADVISOR_ID"));
		fnaDet.setMgrId(resource.getString("MGR_ID"));
		fnaDet.setFnaType(PsgConst.FNA_TYPE);
		fnaDet.setCreatedBy(PsgConst.SIGN_CLIENT);
		fnaDet.setCreatedDate(new Date(System.currentTimeMillis()));
		return fnaDet;
	}

	private void saveCustomerAttachments(MultipartFile[] multipleFiles, CustomerDetails custDets,
			Object[] customerAttachments) throws IOException {
		Map<String, CustomerAttachments> attachMap = new HashMap<>();

		try {
			for (Object obj : customerAttachments) {
				CustomerAttachments custAttach = mapper.readValue(obj.toString(), CustomerAttachments.class);
				attachMap.put(custAttach.getFileName(), custAttach);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<CustomerAttachments> attachList = new ArrayList<>();
		for (MultipartFile file : multipleFiles) {

			CustomerAttachments custAttach = new CustomerAttachments();

			String fileName = file.getOriginalFilename();
			String decryptKey = GcpUpload.generateKey(fileName);

			custAttach = attachMap.get(fileName);
			custAttach.setCusDetails(custDets);

			custAttach.setFileSize(String.valueOf(file.getSize()));
			custAttach.setFileType(fileName.substring(fileName.lastIndexOf(".") + 1));
			byte[] encodedBytes = Base64.getEncoder().encode(file.getBytes());
			custAttach.setDocumentData(encodedBytes);

			custAttach.setCreatedBy(custDets.getCustName());
			custAttach.setCreatedDate(new Date(System.currentTimeMillis()));
			custAttach.setDistributorId("DIS");

			custAttach.setDecryptKey(decryptKey);
			String fileNameWOExtension = fileName.replace(fileName.substring(fileName.lastIndexOf(".")), "");
			fileNameWOExtension += "_" + new SimpleDateFormat("ddMMyyyyHHmmssSSS").format(new java.util.Date());
			String uploadFileName = fileNameWOExtension + "." + fileName.substring(fileName.lastIndexOf(".") + 1);
			uploadFileName = uploadFileName.replace(uploadFileName.substring(uploadFileName.lastIndexOf(".")), ".tmp");
//			String uploadFileName = fileName.replace(".pdf", ".tmp");
			// Encrypt and Upload data to GCP
			String objectName = PsgConst.CLIENT_UPLOADS + "/" + PsgConst.FIRM_CODE + "/" + custDets.getCustId() + "/"
					+ uploadFileName;

			custAttach.setAttachmentLocation(objectName);
			/*
			 * FileOutputStream outFile=new FileOutputStream("D://"+fileName);
			 * outFile.write(file.getBytes()); outFile.flush(); outFile.close();
			 */
			GcpUpload.encryptData(decryptKey, file.getInputStream(), objectName, fileName, projId, bucketName, gcpPath);
			// AESAlgorithm.encrypt(decryptKey,fileName,file.getInputStream());
			attachList.add(custAttach);
			custAttachService.saveData(custAttach);
			// downloadGcp();
		}
	}

	private void saveData(CustomerDetails custDets, FnaSelfSpouseDets fnaSpouse, HttpServletRequest request) {
		String custId, fnaId, dataFormId;
		// Setting hardcoded customer details
		setCustomerDetails(custDets);
		// Save Customer Details
		custDetService.saveData(custDets);
		custId = custDets.getCustId();

		// Save Fna Details
		FnaDetails fnaDet = setFnaDetails(custDets);
		fnaDetService.saveData(fnaDet);
		fnaId = fnaDet.getFnaId();

		// Save FnaSelfSpouse
		fnaSpouse.setFnaDetails(fnaDet);
		selfSpouseService.saveData(fnaSpouse);
		dataFormId = fnaSpouse.getDataFormId();

		setUserSessData(fnaSpouse, request);
		setSessionData(custDets.getCustName(), custId, fnaId, dataFormId, request);
	}

	private void updateData(CustomerDetails custDets, FnaSelfSpouseDets fnaSpouse, String custId,
			HttpServletRequest request) {
		String dataFormId, fnaId;
		/* Update Customer Details */
		setCustomerDetails(custDets);
		custDetService.update(custDets);
		// custAttachService.delete(custId, Constants.CUST_ID,"CustomerAttachments");

		/* Update Self spouse Details */

		FnaDetails fnaDet = fnaDetService.getDataById(custDets.getCustId(), PsgConst.CUST_ID, PsgConst.FNA_DETAILS);

		fnaSpouse.setFnaDetails(fnaDet);
		fnaId = fnaDet.getFnaId();
		FnaSelfSpouseDets selfSpouseDets = selfSpouseService.getDataById(fnaId, PsgConst.FNA_ID,
				PsgConst.FNA_SELF_SPOUSE_DETS);
		dataFormId = selfSpouseDets.getDataFormId();
		// fnaSpouse.setDataFormId(dataFormId);
		// selfSpouseService.update(fnaSpouse);
		setUserSessData(selfSpouseDets, request);
		setSessionData(custDets.getCustName(), custId, fnaId, dataFormId, request);
	}

	private void setSessionData(String custName, String custId, String fnaId, String dataFormId,
			HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.CUST_NAME, custName);
		request.getSession(false).setAttribute(PsgConst.SESS_CUST_ID, custId);
		request.getSession(false).setAttribute(PsgConst.SESS_DATAFORM_ID, dataFormId);
		request.getSession(false).setAttribute(PsgConst.SESS_FNA_ID, fnaId);
	}

	private void setUserSessData(FnaSelfSpouseDets selfSpouse, HttpServletRequest request) {

		request.getSession(false).setAttribute(PsgConst.SESS_CLIENT_NAME, selfSpouse.getDfSelfName());
		String spsName = selfSpouse.getDfSpsName();
		if (spsName != null && !spsName.isEmpty()) {
			request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, spsName);
		} else {
			request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, null);
		}

//		request.getSession(false).setAttribute(PsgConst.SESS_ADVISER_NAME, "Test");
//		request.getSession(false).setAttribute(PsgConst.SESS_MANAGER_NAME, "Salim");
	}

	@DeleteMapping(value = "/delete/{custAttachId}")
	public void deleteDepnDetls(@PathVariable("custAttachId") String custAttachId, HttpServletRequest request) {
		CustomerAttachments custAttach = custAttachService.getDataById(custAttachId, "CUST_ATTACH_ID",
				PsgConst.CUSTOMER_ATTACHMENTS);
		if (custAttach != null) {
			try {
				boolean isDeleted = gcpDelete.deleteGcpFile(custAttach.getAttachmentLocation(), gcpPath, bucketName);
				if (isDeleted) {
					custAttachService.delete(custAttachId, "CUST_ATTACH_ID", PsgConst.CUSTOMER_ATTACHMENTS);
				}
			} catch (IOException e) {
				logger.error("Error in delete gcp file!");
				e.printStackTrace();
			}
		}
	}
}
