package com.afas.isubmit.restservice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/fnadetails")
public class FnaDetailsRestService {

	@Autowired
	FnaDetailsService fnaService;

	@GetMapping(value = "/getAllFnaDetails", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaDetails> getAllFnaDetails() {
		return fnaService.getAllData("FnaDetails");
	}

	@GetMapping(value = "/getDataById/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public FnaDetails getDataById(@PathVariable("fnaId") String fnaId) {
		return fnaService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
	}

	@PostMapping(value = "/saveFnaData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void saveFnaData(@RequestBody FnaDetails fna) {
		fnaService.saveData(fna);
	}

	@PutMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaDetails updateFnaData(@RequestBody FnaDetails fna, HttpServletRequest request) {
		fnaService.update(fna);
		request.getSession(false).setAttribute(PsgConst.SESS_FNA_DATA, fna);
		ObjectMapper mapper = new ObjectMapper();
		try {
			request.setAttribute("FNA_DETAILS", mapper.writeValueAsString(fna));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return fna;
	}

	@DeleteMapping(value = "/deleteFnaData/{fnaId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void deleteFnaData(@PathVariable("fnaId") String fnaId) {
		fnaService.delete(fnaId, PsgConst.FNA_ID, "FnaDetails");
	}

	@GetMapping(value = "/getAllFnaDetsByStatus/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaDetails> getAllFnaDetsByStatus(@PathVariable("status") String status, HttpServletRequest request) {
		String role = (String) request.getSession(false).getAttribute(PsgConst.ACCESS_ROLE);

		if (role.equals(PsgConst.ROLE_MANAGER)) {
			return fnaService.getAllDataById(PsgConst.MGR_APPROVE_STATUS, status, PsgConst.FNA_DETAILS);
		} else if (role.equals(PsgConst.ROLE_ADMIN)) {
			return fnaService.getAllDataById(PsgConst.ADMIN_APPROVE_STATUS, status, PsgConst.FNA_DETAILS);
		}
		return fnaService.getAllDataById(PsgConst.COMP_APPROVE_STATUS, status, PsgConst.FNA_DETAILS);
	}

	@DeleteMapping("/deleteAllDetailsByFnaId/{fnaId}")
	public String deleteAllDetailsByFnaId(@PathVariable String fnaId) {
		boolean isDeleted = fnaService.deleteAllDetailsByFnaId(fnaId);
		String result = "false";
		if (isDeleted) {
			result = "true";
		}
		return result;
	}

}
