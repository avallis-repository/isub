package com.afas.isubmit.restservice;
 
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import com.afas.isubmit.dto.MasterAdviser;
import com.afas.isubmit.dto.MasterTenantDist;
import com.afas.isubmit.dto.UserLogin;
import com.afas.isubmit.service.MasterAdviserService;
import com.afas.isubmit.service.MasterTenantDistService;
import com.afas.isubmit.service.PrincipalInfoService;
import com.afas.isubmit.service.UserLoginService;
import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/MasterTenantAdviserDetails")
public class MasterTenantRestService {
	
	@Autowired
	MasterTenantDistService masterTenantDistService;
	
	@Autowired
	MasterAdviserService  masterAdviserManagementService;
	
	@Autowired
	PrincipalInfoService  princinfoService;
	
	@Autowired
	UserLoginService userService;
 
	ObjectMapper mapper = new ObjectMapper();
	
	ResourceBundle resource = ResourceBundle.getBundle("properties/psgisubmit", Locale.ENGLISH);
 
	ModelAndView mv = new ModelAndView();
	

	@GetMapping("/masterTenantDistList")
	@ResponseBody
	public List<MasterTenantDist> masterTenantDistList(HttpServletRequest request) throws JsonProcessingException {
		//List<MasterTenantDist> tenantDisList = masterTenantDistService.getAllData("MasterTenantDist");
		//return mapper.writeValueAsString(tenantDisList);
		
		String strtenantId = null;
		if (request != null && request.getSession(false) != null) {
			String strTenantId = (String) request.getSession(false).getAttribute("sessionTenantId");
			if (strTenantId != null && !strTenantId.isEmpty()) {
				strtenantId = strTenantId;
			}
		}
		
		 
		return  masterTenantDistService.getAllDataByIdnotequal(PsgConst.TENANT_ID, strtenantId, PsgConst.MASTER_TENANT_DIST);
	 
	}

	@PostMapping("/masterTenantsaveData")
	@ResponseBody
	public MasterTenantDist saveMasterTenantDets(@RequestBody MasterTenantDist masterTenantDets, HttpServletRequest request,
			HttpServletResponse response) throws JsonMappingException, JsonProcessingException {

		Map<String, String> resultMap = new HashMap<>();
		//MasterTenantDist masterTenantDetslist = mapper.readValue(masterTenantDets, MasterTenantDist.class);

		try {
			if (masterTenantDets != null) {
				if (request != null && request.getSession(false) != null) {
					String strAdviserName = (String) request.getSession(false).getAttribute("AdviserName");
					if (strAdviserName != null && !strAdviserName.isEmpty()) {
						masterTenantDets.setCreatedBy(strAdviserName);
					}
				}
				masterTenantDets.setCreatedDate(new Date());
				masterTenantDistService.saveData(masterTenantDets);
				resultMap.put("status", "success");
			}
		} catch (HttpClientErrorException hEx) {
			resultMap.put("status", "failed");
			resultMap.put("response", hEx.getResponseBodyAsString());
			hEx.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	//	mv.setViewName("masterTenantDist");
		return masterTenantDets;
//		 return masterTenantDets;
	//	return mapper.writeValueAsString(resultMap);
	}


	@PostMapping("/masterTenantupdateData")
	@ResponseBody
	public MasterTenantDist updateMasterTenantDets(@RequestBody MasterTenantDist masterTenantDets, HttpServletRequest request,
			HttpServletResponse response) throws JsonMappingException, JsonProcessingException {

		Map<String, String> resultMap = new HashMap<>();
		//MasterTenantDist masterTenantDetslist = mapper.readValue(masterTenantDets, MasterTenantDist.class);

		try {
			if (masterTenantDets != null) {
				if (request != null && request.getSession(false) != null) {
					String strAdviserName = (String) request.getSession(false).getAttribute("AdviserName");
					if (strAdviserName != null && !strAdviserName.isEmpty()) {
						masterTenantDets.setModifiedBy(strAdviserName);
					}
				}
				masterTenantDets.setModifiedDate(new Date());
				masterTenantDistService.update(masterTenantDets);
				resultMap.put("status", "success");
			}
		} catch (HttpClientErrorException hEx) {
			resultMap.put("status", "failed");
			resultMap.put("response", hEx.getResponseBodyAsString());
			hEx.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	//	mv.setViewName("masterTenantDist");
		return masterTenantDets;
		// return "masterTenantDist";
		//		return mapper.writeValueAsString(resultMap);
	}

	@DeleteMapping(value = "/deleteMasterTenantDis/{tenantId}")
	@ResponseBody
	public Map<String, String> deleteMasterTenantDetls(@PathVariable("tenantId") String tenantId, HttpServletRequest request) {
		
		Map<String, String> resultMap = new HashMap<>();
		try {
		UserLogin userinfo = userService.getDataById(tenantId, PsgConst.TENANT_ID, PsgConst.USER_LOGIN);
		
		if(userinfo !=null) {
			userService.delete(tenantId, PsgConst.TENANT_ID, PsgConst.USER_LOGIN);
		}

				
				List<MasterAdviser> deleteasteradviser = masterAdviserManagementService.getAllDataById(PsgConst.TENANT_ID,tenantId,
						 PsgConst.MASTER_ADVISER);
				for (MasterAdviser masterAdviser : deleteasteradviser) {
				    princinfoService.delete(masterAdviser.getAdvStfId(), PsgConst.ADVSTF_ID, PsgConst.PRINCIPAL_INFO);
					
				}
				 
				if(deleteasteradviser != null) {
					//PrincipalInfo deleteprincinfo = princinfoService.getDataById(deleteasteradviser.getAdvStfId(), PsgConst.ADVSTF_ID, PsgConst.PRINCIPAL_INFO);
					           
					               masterAdviserManagementService.delete(tenantId, PsgConst.TENANT_ID, PsgConst.MASTER_ADVISER);
					}
				
		MasterTenantDist deletemasterTenant = masterTenantDistService.getDataById(tenantId, PsgConst.TENANT_ID,
				PsgConst.MASTER_TENANT_DIST);
  
		
		if (deletemasterTenant != null) {
			masterTenantDistService.delete(tenantId, PsgConst.TENANT_ID, PsgConst.MASTER_TENANT_DIST);
			resultMap.put("status", "success");
		}
		} catch (Exception e) {
			resultMap.put("status", "failed");
			e.printStackTrace();
		//	return resultMap;
			//errMsg.setErrMsg(e.getMessage());
			//return ResponseEntity.ok().body(errMsg);
		}
		return resultMap;
		
	}

	@GetMapping(value = "/getAllTenantRegistreNoList/{registrationNo}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<MasterTenantDist> getDataByNric(@PathVariable String registrationNo) {
		return masterTenantDistService.getAllDataById(PsgConst.REGISTRATION_NO, registrationNo, PsgConst.MASTER_TENANT_DIST);
	}
	
	
}




