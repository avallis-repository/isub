package com.afas.isubmit.restservice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaSelfSpouseDets;
import com.afas.isubmit.service.CustomerDetailsService;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaSelfSpouseDetsService;
import com.afas.isubmit.util.ErrorMsg;
import com.afas.isubmit.util.PsgConst;

@RestController
@RequestMapping("/FnaSelfSpouse")
public class FnaSelfSpouseDetsRestService {

	@Autowired
	FnaSelfSpouseDetsService selfSpouseService;

	@Autowired
	FnaDetailsService fnaDetService;

	@Autowired
	CustomerDetailsService custDetsService;

	@GetMapping(value = "/getAllData", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaSelfSpouseDets> getAllFnaSpouseData() {
		return selfSpouseService.getAllData("FnaSelfSpouseDets");
	}

	@GetMapping(value = "/getDataById/{fnaId}", produces = MediaType.APPLICATION_JSON_VALUE)
	public FnaSelfSpouseDets getDataById(@PathVariable("fnaId") String fnaId, HttpServletRequest request) {
		FnaSelfSpouseDets fna;
		if (fnaId.contains("DAT")) {
			fna = selfSpouseService.getDataById(fnaId, PsgConst.DATAFORM_ID, PsgConst.FNA_SELF_SPOUSE_DETS);
		} else {
			fna = selfSpouseService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_SELF_SPOUSE_DETS);
		}

		if (fna != null) {
			setUserSessData(fna, request);
		}
		return fna;
	}

	@PostMapping(value = "/saveData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void saveFnaData(@RequestBody FnaSelfSpouseDets fna, HttpServletRequest request) {

		String dataFormId = fna.getDataFormId();

		if (dataFormId.isEmpty() || dataFormId == null) {
			selfSpouseService.saveData(fna);
		} else {
			selfSpouseService.update(fna);
		}
	}

	@PutMapping(value = "/updateData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ErrorMsg> updateFnaData(@RequestBody FnaSelfSpouseDets fnaSpouseDets,
			HttpServletRequest request) {
		ErrorMsg err = new ErrorMsg();
		try {
			// String fnaId = (String) request.getSession().getAttribute("FnaId");
			// FnaDetails fnaDet = fnaDetService.getDataById(fnaId,
			// Constants.FNA_ID,"FnaDetails");
			FnaDetails fnaDet = fnaSpouseDets.getFnaDetails();
			fnaDetService.update(fnaDet);

			fnaSpouseDets.setFnaDetails(fnaDet);
			selfSpouseService.update(fnaSpouseDets);
			String spsName = fnaSpouseDets.getDfSpsName();
			if (spsName != null && !spsName.isEmpty()) {
				request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, spsName);
			}
			err.setErrMsg("Update data");
			return ResponseEntity.ok().body(err);
		} catch (Exception e) {
			err.setErrMsg(e.getMessage());
			return ResponseEntity.ok().body(err);
		}
	}

	@DeleteMapping(value = "/deleteData/{primaryId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void deleteFnaData(@PathVariable("primaryId") String primaryId) {
		selfSpouseService.delete(primaryId, PsgConst.DATAFORM_ID, "FnaSelfSpouseDets");
	}

	private void setUserSessData(FnaSelfSpouseDets selfSpouse, HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.SESS_DATAFORM_ID, selfSpouse.getDataFormId());
		request.getSession(false).setAttribute(PsgConst.SESS_FNA_ID, selfSpouse.getFnaDetails().getFnaId());
		request.getSession(false).setAttribute(PsgConst.SESS_CLIENT_NAME, selfSpouse.getDfSelfName());
		request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, null);
		String spsName = selfSpouse.getDfSpsName();
		if (spsName != null && !spsName.isEmpty()) {
			request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, spsName);
		} else {
			request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, null);
		}

//		request.getSession(false).setAttribute(PsgConst.SESS_ADVISER_NAME, "Test");
//		request.getSession(false).setAttribute(PsgConst.SESS_MANAGER_NAME, "Salim");
	}

}
