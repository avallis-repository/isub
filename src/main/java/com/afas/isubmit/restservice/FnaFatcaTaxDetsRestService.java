package com.afas.isubmit.restservice;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.afas.isubmit.dto.FnaFatcaTaxDet;
import com.afas.isubmit.service.FnaFatcaTaxDetService;
import com.afas.isubmit.util.ErrMsgConstants;
import com.afas.isubmit.util.ErrorMsg;
import com.afas.isubmit.util.PsgConst;

@RestController
@RequestMapping("/FnaFatcaTax")
public class FnaFatcaTaxDetsRestService {
	
	private static Logger logger = LogManager.getLogger(FnaFatcaTaxDetsRestService.class);

	@Autowired
	FnaFatcaTaxDetService fnaTaxService;

	@PostMapping(value = "/saveData", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaFatcaTaxDet saveFnaData(@RequestBody FnaFatcaTaxDet fnaTax, HttpServletRequest request) {
		String taxId = fnaTax.getTaxId();
		logger.info("Tax id-----" + taxId);
		if (taxId == null || taxId.isEmpty()) {
			fnaTaxService.saveData(fnaTax);
		} else {
			fnaTaxService.update(fnaTax);
		}

		return fnaTax;
	}

	@GetMapping(value = "/getAllDataById/{fnaId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public List<FnaFatcaTaxDet> getAllDataByFnaId(@PathVariable("fnaId") String fnaId, HttpServletRequest request) {
		return fnaTaxService.getAllDataById(PsgConst.FNA_ID, fnaId, PsgConst.FNA_FATCA_TAX_DETS);
	}

	@GetMapping(value = "/getDataById/{taxId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public FnaFatcaTaxDet getDataById(@PathVariable("taxId") String taxId, HttpServletRequest request) {
		return fnaTaxService.getDataById(taxId, PsgConst.TAX_ID, PsgConst.FNA_FATCA_TAX_DETS);
	}

	@DeleteMapping(value = "/deleteData/{taxId}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ErrorMsg> delete(@PathVariable("taxId") String taxId, HttpServletRequest request) {
		ErrorMsg errMsg = new ErrorMsg();
		try {
			fnaTaxService.delete(taxId, PsgConst.TAX_ID, PsgConst.FNA_FATCA_TAX_DETS);
			errMsg.setErrMsg(ErrMsgConstants.DLT_MSG);
		} catch (Exception e) {
			errMsg.setErrMsg(e.getMessage());
			return ResponseEntity.ok().body(errMsg);
		}
		return ResponseEntity.ok().body(errMsg);
	}

}
