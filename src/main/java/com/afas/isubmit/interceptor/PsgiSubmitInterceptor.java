package com.afas.isubmit.interceptor;

import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;

import com.afas.isubmit.dto.UserSession;
import com.afas.isubmit.service.UserSessionService;
import com.fasterxml.jackson.databind.ObjectMapper;

//@Component // - uncomment this, when running iSubmit via STS
public class PsgiSubmitInterceptor implements HandlerInterceptor {

	private static final Logger logger = LogManager.getLogger(PsgiSubmitInterceptor.class);

	@Autowired
	UserSessionService sessionService;

	ResourceBundle resource = ResourceBundle.getBundle("application", Locale.ENGLISH);

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		if (request != null) {
			ObjectMapper mapper = new ObjectMapper();

			String apiName = request.getServletPath();
//			logger.info("apiName: " + apiName);

			if (apiName != null) {
				HttpSession currentSession = request.getSession(false);
				if (currentSession != null) {
//					logger.info("Session ID: " + currentSession.getId());
					String userSessionStr = (String) currentSession.getAttribute("userSession");
					UserSession userSession = null;
					if (userSessionStr != null && !userSessionStr.isEmpty()) {
						userSession = mapper.readValue(userSessionStr, UserSession.class);
					}
					String currentUserSessionStr;
					if ("/".equals(apiName) || "/login".equals(apiName) || "/logout".equals(apiName)
					/*
					 * || apiName.startsWith("/managerLogin") || apiName.startsWith("/adminLogin")
					 * || apiName.startsWith("/compLogin")
					 */) {
//						logger.info("session gets invalidated!");
						if (userSession != null) {
							userSession.setModifiedDate(new Date());
							userSession.setUserLoggedIn(false);
							sessionService.update(userSession);

							currentUserSessionStr = mapper.writeValueAsString(userSession);
							currentSession.setAttribute("userSession", currentUserSessionStr);
						}
						currentSession.invalidate();
					} else if ("/loginValidate".equals(apiName)) {
						userSession = sessionService.getDataById(currentSession.getId(), "session_id", "UserSession");
						currentUserSessionStr = mapper.writeValueAsString(userSession);
						currentSession.setAttribute("userSession", currentUserSessionStr);
						String emailId = request.getParameter("email");
						String password = request.getParameter("password");

						if (emailId == null || emailId.isEmpty() || password == null || password.isEmpty()) {
							if (userSession != null) {
								userSession.setModifiedDate(new Date());
								userSession.setUserLoggedIn(false);
								sessionService.update(userSession);

								currentUserSessionStr = mapper.writeValueAsString(userSession);
								currentSession.setAttribute("userSession", currentUserSessionStr);
							}
							currentSession.invalidate();
						} else {
							if (userSession != null) {
								userSession.setModifiedDate(new Date());
								userSession.setUserLoggedIn(false);
								sessionService.update(userSession);

								currentUserSessionStr = mapper.writeValueAsString(userSession);
								currentSession.setAttribute("userSession", currentUserSessionStr);
							}
							currentSession.invalidate();

							HttpSession newSession = request.getSession(true);
							String timeout = resource.getString("server.servlet.session.timeout");
							String maxInactiveIntervalStr = timeout.substring(0, timeout.length() - 1);
							int maxInactiveInterval = Integer.valueOf(maxInactiveIntervalStr);

							newSession.setMaxInactiveInterval(maxInactiveInterval * 60);
							newSession.setAttribute("sessionTimeout", newSession.getMaxInactiveInterval());

							userSession = new UserSession();
							userSession.setUserName(emailId);
							userSession.setSessionId(newSession.getId());
							userSession.setUserLoggedIn(true);
							userSession.setCreatedDate(new Date());
							userSession.setModifiedDate(new Date());
							sessionService.saveData(userSession);

							currentUserSessionStr = mapper.writeValueAsString(userSession);
							newSession.setAttribute("userSession", currentUserSessionStr);

							// Need to handle in separate thread - start
							Long totalSessions = sessionService.getAllDataCount("UserSession");
//							logger.info("totalSessions: " + totalSessions);
							if (totalSessions != null && totalSessions > 1000) {
								// List<UserSession> sessions =
								// sessionService.getAllDataByOrderByCol("UserSession", "CREATED_DATE", "ASC",
								// 10);
								// if (sessions != null) {
								// logger.info("Session count: " + sessions.size());
								// ObjectMapper mapper = new ObjectMapper();
								// logger.info("Sessions: " + mapper.writeValueAsString(sessions));
								// }
								sessionService.deleteByOrderByCol("UserSession", "id", "created_date", "asc", 100);
							}
							// Need to handle in separate thread - end
						}
					} else if ("/sessionUpdate".equals(apiName) || apiName.startsWith("/managerLogin")
							|| apiName.startsWith("/adminLogin") || apiName.startsWith("/compLogin")
							|| apiName.startsWith("/sign")) {
						String currentUserName = null;
						if (userSession != null) {
							userSession.setModifiedDate(new Date());
							userSession.setUserLoggedIn(false);
							sessionService.update(userSession);
							currentUserName = userSession.getUserName();
						}
						currentSession.invalidate();
						HttpSession newSession = request.getSession(true);

						UserSession userNewSession = new UserSession();
						userNewSession.setUserName(currentUserName);
						userNewSession.setSessionId(newSession.getId());
						userNewSession.setUserLoggedIn(true);
						userNewSession.setCreatedDate(new Date());
						userNewSession.setModifiedDate(new Date());
						sessionService.saveData(userNewSession);

						currentUserSessionStr = mapper.writeValueAsString(userNewSession);
						newSession.setAttribute("userSession", currentUserSessionStr);
//						logger.info("invalidated existing session and created new session");
					} else {
						if (userSession == null || (userSession != null && (!userSession.isUserLoggedIn()
								|| !currentSession.getId().equals(userSession.getSessionId())))) {
							if (userSession != null) {
								userSession.setModifiedDate(new Date());
								userSession.setUserLoggedIn(false);
								sessionService.update(userSession);
								currentUserSessionStr = mapper.writeValueAsString(userSession);
								currentSession.setAttribute("userSession", currentUserSessionStr);
							}
							currentSession.invalidate();
//							logger.info("invalid session");
							if (!"/".equals(apiName) && !"/login".equals(apiName) && !"/logout".equals(apiName)) {
								logger.info("existing session is null with redirecting to login page");
								response.sendRedirect("login");
								return false;
							}
						} else {
//							logger.info("valid session");
						}
					}
				} else {
					// current session is null
//					logger.info("current session is null!");
					if ("/sessionUpdate".equals(apiName) || apiName.startsWith("/managerLogin")
							|| apiName.startsWith("/adminLogin") || apiName.startsWith("/compLogin")
							|| apiName.startsWith("/sign")) {
						HttpSession newSession = request.getSession(true);

						UserSession userSession = new UserSession();
						userSession.setUserName(request.getParameter("adviserName") != null
								? request.getParameter("adviserName")
								: "Expired User");
						userSession.setSessionId(newSession.getId());
						userSession.setUserLoggedIn(true);
						userSession.setCreatedDate(new Date());
						userSession.setModifiedDate(new Date());
						sessionService.saveData(userSession);

						String currentUserSessionStr = mapper.writeValueAsString(userSession);
						newSession.setAttribute("userSession", currentUserSessionStr);
//						logger.info("created new session");
					} else if (!"/".equals(apiName) && !"/login".equals(apiName) && !"/logout".equals(apiName)) {
//						logger.info("current session is null with redirecting to login page");
						response.sendRedirect("login");
						return false;
					}
				}
			}
		}

		return true;
	}

}
