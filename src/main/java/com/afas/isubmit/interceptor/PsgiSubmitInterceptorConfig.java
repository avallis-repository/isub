package com.afas.isubmit.interceptor;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

//@Component // - uncomment this, when running iSubmit via STS
public class PsgiSubmitInterceptorConfig implements WebMvcConfigurer {

	private static Logger logger = LogManager.getLogger(PsgiSubmitInterceptorConfig.class);

	@Autowired
	PsgiSubmitInterceptor iSubmitInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		logger.info("inside interceptor config");
		List<String> excludePatterns = new ArrayList<>();
		excludePatterns.add("/*.jsp");
		excludePatterns.add("/*.js");
		excludePatterns.add("/*.css");
		registry.addInterceptor(iSubmitInterceptor).addPathPatterns("/**").excludePathPatterns(excludePatterns);
	}

}
