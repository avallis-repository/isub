package com.afas.isubmit.controller;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.afas.isubmit.dto.FnaSelfSpouseDets;
import com.afas.isubmit.dto.MasterAdviser;
import com.afas.isubmit.dto.UserLogin;
import com.afas.isubmit.service.FnaSelfSpouseDetsService;
import com.afas.isubmit.service.MasterAdviserService;
import com.afas.isubmit.service.UserLoginService;
import com.afas.isubmit.util.PsgConst;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class LoginController {

	@Autowired
	FnaSelfSpouseDetsService selfSpouseService;

	@Autowired
	UserLoginService loginService;
	
	@Autowired
	MasterAdviserService adviserService;

	@GetMapping("/managerLogin")
	public String login(HttpServletRequest request) throws UnsupportedEncodingException {
		String encodedFnaDataFormId = request.getQueryString();
		if (encodedFnaDataFormId != null && !encodedFnaDataFormId.isEmpty()) {
			String fnaDataFormId = new String(DatatypeConverter.parseBase64Binary(encodedFnaDataFormId), "UTF-8");
			getLoginDetails(fnaDataFormId, PsgConst.ROLE_MANAGER, request);
		}
		return "managerLogin";
	}

	@GetMapping("/adminLogin{fnaDataFormId}")
	public String adminLogin(@PathVariable String fnaDataFormId, HttpServletRequest request) {
		getLoginDetails(fnaDataFormId, PsgConst.ROLE_ADMIN, request);
		return "adminLogin";
	}

	@GetMapping("/compLogin{fnaDataFormId}")
	public String compLogin(@PathVariable String fnaDataFormId, HttpServletRequest request) {
		getLoginDetails(fnaDataFormId, PsgConst.ROLE_COMP, request);
		return "compLogin";
	}

	private void setUserSessData(FnaSelfSpouseDets selfSpouse, HttpServletRequest request) {

		request.getSession(false).setAttribute(PsgConst.SESS_CLIENT_NAME, selfSpouse.getDfSelfName());
		String spsName = selfSpouse.getDfSpsName();
		if (spsName != null && !spsName.isEmpty()) {
			request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, spsName);
		} else {
			request.getSession(false).setAttribute(PsgConst.SESS_SPOUSE_NAME, null);
		}

//		request.getSession(false).setAttribute(PsgConst.SESS_ADVISOR_NAME, "Test");
//		request.getSession(false).setAttribute(PsgConst.SESS_MANAGER_NAME, "Salim");
	}

	private void getLoginDetails(String fnaDataFormId, String roleAccess, HttpServletRequest request) {
		String[] fnaDataId = fnaDataFormId.split("=");
		String fnaId = fnaDataId[0];
		String dataFormId = fnaDataId[1];
		request.getSession(false).setAttribute(PsgConst.SESS_FNA_ID, fnaId);
		request.getSession(false).setAttribute(PsgConst.SESS_DATAFORM_ID, dataFormId);
		request.getSession(false).setAttribute(PsgConst.ACCESS_ROLE, roleAccess);
		FnaSelfSpouseDets fna = selfSpouseService.getDataById(dataFormId, PsgConst.DATAFORM_ID,
				PsgConst.FNA_SELF_SPOUSE_DETS);
		setUserSessData(fna, request);
	}

	@PostMapping(value = { "/loginValidate", "/approval/loginValidate", "/masterLogin/loginValidate" })
	@ResponseBody
	public String getAllClients(HttpServletRequest request) throws JsonProcessingException {

		String emailId = request.getParameter("email");
		String password = request.getParameter("password");
//		String adminId = request.getParameter("adminId");

		String userId = "UserId";
//		String userType = "User";
//		if (adminId != null) {
//			userId = "AdminId";
//			userType = "Admin";
//			emailId = adminId;
//		}
		
		UserLogin userLogin = loginService.getDataByCol(emailId, "EMAIL_ID", password, "PASSWORD", "UserLogin");

		ObjectMapper mapper = new ObjectMapper();

		Map<String, String> resultMap = new HashMap<>();

		if (userLogin == null) {
			resultMap.put("status", "failed");
			resultMap.put("reason", userId + " or Password is wrong!");
		} else {
			resultMap.put("status", "success");
			request.getSession(false).setAttribute("loggedInUserId", userLogin.getUserId());
//			String adviserName = null;
//			if ("Admin".equals(userLogin.getUserType())) {
//				adviserName = "Admin";
//			} else {
//				adviserName = userLogin.getAdvStfId().getAdvStfName();
//			}
			if (userLogin.getAdvStfId() != null) {
				request.getSession(false).setAttribute(PsgConst.SESS_ADVISER_NAME, userLogin.getAdvStfId().getAdvStfName());
			}
			request.getSession(false).setAttribute("userType", userLogin.getUserType());
			if(userLogin.getTenantId() != null && userLogin.getTenantId().getTenantId() !=null) {
			request.getSession(false).setAttribute("sessionTenantId", userLogin.getTenantId().getTenantId());
			}
			if(userLogin.getAdvStfId() != null && userLogin.getAdvStfId().getAdvStfId() !=null) {
				request.getSession(false).setAttribute("adminAdviserId", userLogin.getAdvStfId().getAdvStfId());
				}
			if (userLogin.getAdvStfId() != null && userLogin.getAdvStfId().getManagerId() != null) {
				MasterAdviser manager = adviserService.getDataById(userLogin.getAdvStfId().getManagerId(), "advStf_id", "MasterAdviser");
				if (manager != null) {
					request.getSession(false).setAttribute("managerEmailId", manager.getEmailId());
					request.getSession(false).setAttribute(PsgConst.SESS_MANAGER_NAME, manager.getAdvStfName());
				}
			}
		}

		return mapper.writeValueAsString(resultMap);
	}
	
	@GetMapping("/masterLogin")
	public ModelAndView masterLogin() {
		return new ModelAndView("masterLogin");
	}

}
