package com.afas.isubmit.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.afas.isubmit.dto.CustomerDetails;
import com.afas.isubmit.dto.FnaDetails;
import com.afas.isubmit.dto.FnaRecomPrdtPlanDet;
import com.afas.isubmit.dto.FnaSelfSpouseDets;
import com.afas.isubmit.dto.MasterAdviser;
import com.afas.isubmit.dto.MasterAttachCategory;
import com.afas.isubmit.dto.MasterPrincipal;
import com.afas.isubmit.dto.MasterTenantDist;
import com.afas.isubmit.service.CustomerDetailsService;
import com.afas.isubmit.service.FnaDetailsService;
import com.afas.isubmit.service.FnaRecommPrdtPlanDetService;
import com.afas.isubmit.service.FnaSelfSpouseDetsService;
import com.afas.isubmit.service.FnaSignatureService;
import com.afas.isubmit.service.MasterAdviserService;
import com.afas.isubmit.service.MasterAttachCategService;
import com.afas.isubmit.service.MasterCustStatusService;
import com.afas.isubmit.service.MasterPrincipalService;
import com.afas.isubmit.service.MasterTenantDistService;
import com.afas.isubmit.util.PsgConst;
import com.afas.isubmit.util.PsgUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class IndexController implements ErrorController {

	private static Logger logger = LogManager.getLogger(IndexController.class);

	ResourceBundle psgProp = ResourceBundle.getBundle("properties/psgisubmit");
	
	RestTemplate restTemp = new RestTemplate();

	ModelAndView mv = new ModelAndView();

	@Autowired
	CustomerDetailsService custService;

	@Autowired
	MasterPrincipalService prinService;

	@Autowired
	FnaDetailsService fnaService;

	@Autowired
	FnaSelfSpouseDetsService selfService;

	@Autowired
	FnaSignatureService signService;

	@Autowired
	FnaRecommPrdtPlanDetService prdtService;

	@Autowired
	MasterAttachCategService attachService;

	@Autowired
	MasterCustStatusService custStatusService;

	@Autowired
	MasterTenantDistService masterTenantDistService;

	@Autowired
	MasterAdviserService masterAdviserManagementService;

	@Autowired
	MasterPrincipalService masterPrincService;

	ObjectMapper mapper = new ObjectMapper();
	
	private final static Map<String, String> errorCodeWithReason = new HashMap<>();
	
	static {
		errorCodeWithReason.put("400", "Bad Request - Please check the request parameters!");
		errorCodeWithReason.put("401", "Unauthorized - Server requires authentication!");
		errorCodeWithReason.put("403", "Forbidden - Server refuses to authorize the request!");
		errorCodeWithReason.put("404", "Not Found - Required page does not exist!");
		errorCodeWithReason.put("405", "Method Not Allowed - Please contact the Administrator!");
		
		errorCodeWithReason.put("500", "Internal Server Error - Please contact the Administrator!");
		errorCodeWithReason.put("502", "Bad Gateway - Invalid response from the upstream server!");
		errorCodeWithReason.put("503", "Service Unavailable - Server is temporarily down!");
		errorCodeWithReason.put("504", "Gateway Timeout - Did not get a response in time from the upstream server!");
	}

	@GetMapping("/socket")
	public String webSocket() {
		return "socketIndex";
	}

	@GetMapping({ "/", "/login", "/logout" })
	public String loginPage(HttpServletRequest request) {
		return "login";
	}

	@GetMapping("/index")
	public String index() {
		return "index";
	}

	@GetMapping("/addClientInfo")
	public String stepper(HttpServletRequest request) throws JsonProcessingException {
		logger.info("ClientInfo: Setting constants to the client!");
		request.getSession(false).setAttribute(PsgConst.CLIENT_STATUS, PsgConst.CLIENT_ADD);
		request.getSession(false).setAttribute(PsgConst.SESS_CUST_ID, null);
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, null);
		List<CustomerDetails> customersList = custService.getAllDataByOrderByCol(PsgConst.MASTER_CUST_STATUS,
				PsgConst.CUST_STS_NAME, PsgConst.ASC_ORDER);
		request.setAttribute("MASTER_CUSTSTS", customersList);
		return "clientInfo";
	}

	@GetMapping("/editClientInfo")
	public ModelAndView stepperId(HttpServletRequest request)
			throws JsonProcessingException, UnsupportedEncodingException {

		String encodedCustId = request.getQueryString();
		if (encodedCustId != null && !encodedCustId.isEmpty()) {
			String custId = new String(DatatypeConverter.parseBase64Binary(encodedCustId), "UTF-8");

			request.getSession(false).setAttribute(PsgConst.CLIENT_STATUS, PsgConst.CLIENT_EDIT);

			request.setAttribute("MASTER_CUSTSTS", custService.getAllDataByOrderByCol(PsgConst.MASTER_CUST_STATUS,
					PsgConst.CUST_STS_NAME, PsgConst.ASC_ORDER));
			String custJson = mapper
					.writeValueAsString(custService.getDataById(custId, PsgConst.CUST_ID, PsgConst.CUSTOMER_DETAILS));
			request.setAttribute("CUSTOMER_DETAILS", custJson);

			mv.addObject("custId", custId);
		}
		mv.setViewName("clientInfo");
		return mv;
	}

	@GetMapping("/kycIntro")
	public ModelAndView kycIntro(HttpServletRequest request) {
		setFNADetailsInRequest(request);
		mv.setViewName(PsgConst.KYC_INTRO);
		return mv;
	}

	@GetMapping("/kycHome")
	public ModelAndView kycHome(HttpServletRequest request) throws JsonProcessingException {
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.KYC_HOME);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.KYC_HOME);

		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.TAX_RESIDENCY);
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.KYC_INTRO);

		HttpSession session = request.getSession(false);
		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
		FnaSelfSpouseDets ss = selfService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_SELF_SPOUSE_DETS);
		session.setAttribute("SESS_DF_SELF_NAME", ss.getDfSelfName());
		session.setAttribute("SESS_ADVISER_NAME", session.getAttribute("AdviserName"));
		session.setAttribute("SESS_DF_SPS_NAME", null);
		String spsName = ss.getDfSpsName();
		if (spsName != "" && spsName != null) {
			session.setAttribute("SESS_DF_SPS_NAME", spsName);
		}
		request.setAttribute("FNA_DETAILS", mapper.writeValueAsString(ss.getFnaDetails()));
		setErrParam(request);
		mv.setViewName(PsgConst.KYC_HOME);
		return mv;
	}

	@GetMapping("/kycHome{fnaDataFormId}")
	public ModelAndView managerKyc(@PathVariable String fnaDataFormId, HttpServletRequest request) {
		String[] id = fnaDataFormId.split("=");
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.KYC_HOME);

		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.FIN_WELL_REVIEW);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.KYC_HOME);
		request.getSession(false).setAttribute(PsgConst.SESS_FNA_ID, id[0]);
		request.getSession(false).setAttribute(PsgConst.SESS_DATAFORM_ID, id[1]);
		mv.setViewName(PsgConst.KYC_HOME);
		return mv;
	}

	@GetMapping("/productRecommend")
	public ModelAndView productRecommend(HttpServletRequest request) throws JsonProcessingException {
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.PRODUCT_RECOMMEND);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.PRODUCT_RECOMMEND);

		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);

		mv.addObject("PRINCIPAL_LIST", prinService.getAllData(PsgConst.MASTER_PRINCIPAL));
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.FIN_WELL_REVIEW);
		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.PRODUCT_SWITCH_RECOMM);

		getProductList(fnaId, request);

		addFnaSelfSpouseName(fnaId, request);

		setErrParam(request);

		mv.setViewName(PsgConst.PRODUCT_RECOMMEND);

		setFNADetailsInRequest(request);

		return mv;
	}

	private void addFnaSelfSpouseName(String fnaId, HttpServletRequest request) {
		List<String> names = new ArrayList<>();
		FnaSelfSpouseDets ss = selfService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_SELF_SPOUSE_DETS);
		if (PsgUtil.emptyOrNull(ss.getDfSelfName())) {
			names.add(ss.getDfSelfName().toUpperCase());
		}
		if (PsgUtil.emptyOrNull(ss.getDfSpsName())) {
			names.add(ss.getDfSpsName().toUpperCase());
		}

		request.setAttribute("FNA_SELF_SPOUSE_NAMES", names);
	}

	// Not in use
	@SuppressWarnings("unused")
	private void getCategoryList(HttpServletRequest request) throws JsonProcessingException {
		List<MasterAttachCategory> cateList = attachService.getAllData(PsgConst.MASTER_ATTACH_CATEG);
		if (cateList.size() > 0) {
			String categoryList = mapper.writeValueAsString(cateList);
			request.getSession(false).setAttribute("CATEGORY_LIST", categoryList);
		} else {
			request.setAttribute("CATEGORY_LIST", "{}");
		}

	}

	@GetMapping("/finanWellReview")
	public ModelAndView finWellReview(HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.FIN_WELL_REVIEW);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.FIN_WELL_REVIEW);

		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.PRODUCT_RECOMMEND);
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.TAX_RESIDENCY);

		setErrParam(request);

		mv.setViewName(PsgConst.FIN_WELL_REVIEW);

		setFNADetailsInRequest(request);

		return mv;
	}

	@GetMapping("/adviceBasisRecomm")
	public ModelAndView adviceBasisRecomm(HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.ADVICE_BASIS_RECOMM);
		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.CLIENT_SIGNATURE);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.ADVICE_BASIS_RECOMM);
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.PRODUCT_SWITCH_RECOMM);

		setErrParam(request);

		mv.setViewName(PsgConst.ADVICE_BASIS_RECOMM);

		setFNADetailsInRequest(request);

		return mv;
	}

	@GetMapping("/productSwitchRecomm")
	public ModelAndView productSwitchRecomm(HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.PRODUCT_SWITCH_RECOMM);
		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.ADVICE_BASIS_RECOMM);
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.PRODUCT_RECOMMEND);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.PRODUCT_SWITCH_RECOMM);

		setErrParam(request);

		mv.setViewName(PsgConst.PRODUCT_SWITCH_RECOMM);

		setFNADetailsInRequest(request);

		return mv;
	}

	@GetMapping("/taxResidency")
	public ModelAndView taxResidency(HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.TAX_RESIDENCY);
		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.FIN_WELL_REVIEW);
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.KYC_HOME);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.TAX_RESIDENCY);

		setErrParam(request);

		mv.setViewName(PsgConst.TAX_RESIDENCY);

		setFNADetailsInRequest(request);

		return mv;
	}

	@GetMapping("/clientSignature")
	public ModelAndView clientSignature(HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.CLIENT_SIGNATURE);
		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.CLIENT_DECLARATION);
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.ADVICE_BASIS_RECOMM);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.CLIENT_SIGNATURE);

		setErrParam(request);

		mv.setViewName(PsgConst.CLIENT_SIGNATURE);

		setFNADetailsInRequest(request);

		return mv;
	}

	@GetMapping("/clientDeclaration")
	public ModelAndView advisorDeclaration(HttpServletRequest request) throws JsonProcessingException {

		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.CLIENT_DECLARATION);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.CLIENT_DECLARATION);

		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.MANAGER_DECLARATION);
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.CLIENT_SIGNATURE);

		setErrParam(request);

		mv.setViewName(PsgConst.CLIENT_DECLARATION);

		setFNADetailsInRequest(request);

		return mv;
	}

	@GetMapping("/managerDeclaration")
	public ModelAndView managerDeclartion(HttpServletRequest request) {
		request.getSession(false).setAttribute(PsgConst.BACK_TO_URL, PsgConst.MANAGER_DECLARATION);
		request.getSession(false).setAttribute(PsgConst.CURRENT_SCREEN, PsgConst.MANAGER_DECLARATION);
		mv.addObject(PsgConst.NEXT_SCREEN, PsgConst.FINISHED);
		mv.addObject(PsgConst.PREV_SCREEN, PsgConst.CLIENT_DECLARATION);
		mv.setViewName(PsgConst.MANAGER_DECLARATION);

		setErrParam(request);

		setFNADetailsInRequest(request);

		return mv;
	}

	@GetMapping("/FileUpload")
	public String fileUpload() {
		return "dragDrop";
	}

	@GetMapping("/signInfo")
	public ModelAndView getSignPage(HttpServletRequest request) throws UnsupportedEncodingException {
		logger.info("sign page!");

		String encodedSignPerson = request.getQueryString();
		if (encodedSignPerson != null && !encodedSignPerson.isEmpty()) {
			String signPerson = new String(DatatypeConverter.parseBase64Binary(encodedSignPerson), "UTF-8");
			String[] personId = signPerson.split("=");
			String perType = personId[0];
			String dataFormId = personId[1];
			FnaSelfSpouseDets selfSpouse = selfService.getDataById(dataFormId, PsgConst.DATAFORM_ID,
					PsgConst.FNA_SELF_SPOUSE_DETS);
			SimpleDateFormat sm = new SimpleDateFormat("dd-MM-yyyy");

			if (perType.equals(PsgConst.SIGN_CLIENT)) {
				mv = getSelfData(selfSpouse, mv, sm);
			} else if (perType.equals(PsgConst.SIGN_SPS)) {
				mv = getSpouseData(selfSpouse, mv, sm);
			} else if (perType.equals(PsgConst.SIGN_ADVISER)) {
				mv = getAdviserData(selfSpouse, mv, sm);
			} else {
				mv = getManagerData(selfSpouse, mv, sm);
			}

			if (request.getSession(false) != null) {
				String curPage = (String) request.getSession(false).getAttribute(PsgConst.CURRENT_SCREEN);
				mv.addObject("curPage", curPage);
			}
			mv.addObject("perType", perType);
			logger.info("sign before");
		}
		logger.info("sign end");
		mv.setViewName("signature");
		return mv;
	}

	private ModelAndView getManagerData(FnaSelfSpouseDets selfSpouse, ModelAndView mv, SimpleDateFormat sm) {
		mv.addObject("clientName", selfSpouse.getDfSelfName());
		mv.addObject("nric", selfSpouse.getDfSelfNric());
		if (selfSpouse.getDfSelfDob() != null) {
			String dob = sm.format(selfSpouse.getDfSelfDob());
			mv.addObject("dateOfBirth", dob);
		}

		mv.addObject("phoneNo", selfSpouse.getDfSelfMobile());
		mv.addObject("fnaId", selfSpouse.getFnaDetails().getFnaId());
		mv.addObject("signTitle", "Manager");
		return mv;
	}

	private ModelAndView getAdviserData(FnaSelfSpouseDets selfSpouse, ModelAndView mv, SimpleDateFormat sm) {
		mv.addObject("clientName", selfSpouse.getDfSelfName());
		mv.addObject("nric", selfSpouse.getDfSelfNric());
		if (selfSpouse.getDfSelfDob() != null) {
			String dob = sm.format(selfSpouse.getDfSelfDob());
			mv.addObject("dateOfBirth", dob);
		}

		mv.addObject("phoneNo", selfSpouse.getDfSelfMobile());
		mv.addObject("fnaId", selfSpouse.getFnaDetails().getFnaId());
		mv.addObject("signTitle", "Adviser");
		return mv;
	}

	private ModelAndView getSpouseData(FnaSelfSpouseDets selfSpouse, ModelAndView mv, SimpleDateFormat sm) {
		mv.addObject("clientName", selfSpouse.getDfSpsName());
		mv.addObject("nric", selfSpouse.getDfSpsNric());

		if (selfSpouse.getDfSpsDob() != null) {
			String dob = sm.format(selfSpouse.getDfSpsDob());
			mv.addObject("dateOfBirth", dob);
		}

		mv.addObject("phoneNo", selfSpouse.getDfSelfMobile());
		mv.addObject("fnaId", selfSpouse.getFnaDetails().getFnaId());
		mv.addObject("signTitle", "Spouse");
		return mv;

	}

	private ModelAndView getSelfData(FnaSelfSpouseDets selfSpouse, ModelAndView mv, SimpleDateFormat sm) {
		mv.addObject("clientName", selfSpouse.getDfSelfName());
		mv.addObject("nric", selfSpouse.getDfSelfNric());

		String dob = sm.format(selfSpouse.getDfSelfDob());
		mv.addObject("dateOfBirth", dob);

		mv.addObject("phoneNo", selfSpouse.getDfSelfMobile());
		mv.addObject("fnaId", selfSpouse.getFnaDetails().getFnaId());
		mv.addObject("signTitle", "Client");
		return mv;
	}

	private void getProductList(String fnaId, HttpServletRequest request) throws JsonProcessingException {
		List<FnaRecomPrdtPlanDet> prodRecommList = prdtService.getAllDataById(PsgConst.FNA_ID, fnaId,
				PsgConst.FNA_RECOMM_PRDT_DETS);
		if (prodRecommList != null && prodRecommList.size() > 0) {
			String strProdRecommList = mapper.writeValueAsString(prodRecommList);
			request.setAttribute("PRODUCT_RECOM_PLAN_DETS", strProdRecommList);
			logger.info("prodRecommList-->>" + strProdRecommList);
		} else {
			request.setAttribute("PRODUCT_RECOM_PLAN_DETS", "{}");
		}
	}

	@GetMapping("/kycApproval")
	public String kycApproval(HttpServletRequest request) throws JsonProcessingException {
		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
		FnaSelfSpouseDets ss = selfService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_SELF_SPOUSE_DETS);

		if (ss != null) {
			if (ss.getFnaDetails() != null) {
				request.setAttribute("FNA_DETAILS", mapper.writeValueAsString(ss.getFnaDetails()));
			}
			String spsName = ss.getDfSpsName();
			if (spsName != "" && spsName != null) {
				request.getSession(false).setAttribute("SESS_DF_SPS_NAME", spsName);
			}
		}

		// sample map start
		Map<String, Integer> stsMap = new HashMap<>();
		stsMap.put("PENDING", 15);
		stsMap.put("APPROVE", 10);
		stsMap.put("REJECT", 5);
		request.setAttribute("FNA_MGR_STATUS_LIST", stsMap);
		// sample map end

		getProductList(fnaId, request);
		return "kycApproval";
	}

	@GetMapping("/copyKycApp")
	public String CopykycApproval() {
		return "copyKycApp";
	}

	@GetMapping("/newKycApproval")
	public String newKycApproval(HttpServletRequest request) {
		return "newKycApproval";
	}

	@GetMapping("/viewFnaDetails")
	public String viewFnaDetails(HttpServletRequest request) {
		return "viewFnaDetails";
	}

	public void setErrParam(HttpServletRequest request) {
		String encodedParam1 = request.getQueryString();
		if (encodedParam1 != null && !encodedParam1.isEmpty()) {
			String decodedParam1 = null;
			try {
				decodedParam1 = new String(DatatypeConverter.parseBase64Binary(encodedParam1), "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			if (decodedParam1 != null) {
				request.setAttribute("SIGN_VALIDATION_MSG", decodedParam1.split("=")[1]);
			}
		}
	}

	@PostMapping("/saveSessionAttributes")
	@ResponseBody
	public String saveSessionAttributes(HttpServletRequest request) throws JsonProcessingException {
		Map<String, String> attributeMap = new HashMap<>();
		try {
			Map<String, Object> currentSessionAttributeMap = getCurrentSessionAttributes(request.getSession(false));
			if (currentSessionAttributeMap != null && currentSessionAttributeMap.size() > 0) {
				attributeMap.put("currentSessionAttributes", mapper.writeValueAsString(currentSessionAttributeMap));
			}
//			Map<String, Object> currentRequestAttributeMap = getCurrentRequestAttributes(request);
//			if (currentRequestAttributeMap != null && currentRequestAttributeMap.size() > 0) {
//				attributeMap.put("currentRequestAttributes", mapper.writeValueAsString(currentRequestAttributeMap));
//			}
			logger.info(mapper.writeValueAsString(attributeMap));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return mapper.writeValueAsString(attributeMap);
	}

	public Map<String, Object> getCurrentSessionAttributes(HttpSession currentSession) {
		Map<String, Object> sessionAttributeMap = new HashMap<>();

		Enumeration<String> attributeNames = currentSession.getAttributeNames();
		String attributeName = null;
		Object attributeValue = null;
		while (attributeNames.hasMoreElements()) {
			attributeName = null;
			attributeValue = null;
			attributeName = attributeNames.nextElement();
			if (attributeName != null && !attributeName.equals("userSession")) {
				try {
					attributeValue = currentSession.getAttribute(attributeName);
					sessionAttributeMap.put(attributeName, attributeValue);
				} catch (NoSuchElementException nse) {
					nse.printStackTrace();
				}
			}
		}
		return sessionAttributeMap;
	}

	// Testing is going on
	@SuppressWarnings("unused")
	private Map<String, Object> getCurrentRequestAttributes(HttpServletRequest request) {
		Map<String, Object> requestAttributeMap = new HashMap<>();
		Enumeration<String> attributeNames = request.getAttributeNames();
		String attributeName = null;
		Object attributeValue = null;
		while (attributeNames.hasMoreElements()) {
			attributeName = null;
			attributeValue = null;
			attributeName = attributeNames.nextElement();
			try {
				attributeValue = request.getAttribute(attributeName);
				requestAttributeMap.put(attributeName, attributeValue);
			} catch (NoSuchElementException nse) {
				nse.printStackTrace();
			}
		}
		return requestAttributeMap;
	}

	@SuppressWarnings("unchecked")
	@PostMapping("/sessionUpdate")
	@ResponseBody
	public void sessionUpdate(@RequestBody String sessionAttributeStr, HttpServletRequest request)
			throws JsonMappingException, JsonProcessingException, UnsupportedEncodingException {
		String decodedStr = URLDecoder.decode(sessionAttributeStr, "UTF-8");
		decodedStr = decodedStr.substring(0, decodedStr.length() - 1);
		logger.info("decodedStr: " + decodedStr);
		try {
			Map<String, String> attributeMap = mapper.readValue(decodedStr, Map.class);
			if (attributeMap.containsKey("currentSessionAttributes")) {
				Map<String, String> sessionAttributeMap = mapper.readValue(attributeMap.get("currentSessionAttributes"),
						Map.class);
				setNewSessionAttributes(sessionAttributeMap, request.getSession(false));
			}
			if (attributeMap.containsKey("currentRequestAttributes")) {
				Map<String, String> requestAttributeMap = mapper.readValue(attributeMap.get("currentRequestAttributes"),
						Map.class);
				setNewRequestAttributes(requestAttributeMap, request);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setNewSessionAttributes(Map<String, String> sessionAttributeMap, HttpSession newSession) {
		if (sessionAttributeMap != null && sessionAttributeMap.size() > 0) {
			Object sessionAttributeValue = null;
			for (String sessionAttributeName : sessionAttributeMap.keySet()) {
				if (sessionAttributeName != null && !sessionAttributeName.isEmpty()
						&& sessionAttributeMap.containsKey(sessionAttributeName)) {
					sessionAttributeValue = sessionAttributeMap.get(sessionAttributeName);
					newSession.setAttribute(sessionAttributeName, sessionAttributeValue);
				}
			}
		}
	}

	private void setNewRequestAttributes(Map<String, String> requestAttributeMap, HttpServletRequest request) {
		if (requestAttributeMap != null && requestAttributeMap.size() > 0) {
			Object requestAttributeValue = null;
			for (String requestAttributeName : requestAttributeMap.keySet()) {
				if (requestAttributeName != null && !requestAttributeName.isEmpty()
						&& requestAttributeMap.containsKey(requestAttributeName)) {
					requestAttributeValue = requestAttributeMap.get(requestAttributeName);
					request.setAttribute(requestAttributeName, requestAttributeValue);
				}
			}
		}
	}

	public void setFNADetailsInRequest(HttpServletRequest request) {
		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
		FnaDetails fnaDetails = fnaService.getDataById(fnaId, PsgConst.FNA_ID, PsgConst.FNA_DETAILS);
		try {
			request.setAttribute("FNA_DETAILS", mapper.writeValueAsString(fnaDetails));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

	@GetMapping("/getMyInfo/{nricCode}")
	@ResponseBody
	public String getMyInfo(@PathVariable String nricCode) throws JsonProcessingException {
		Map<String, String> resultMap = new HashMap<>();
		String url = psgProp.getString("my_info.url") + nricCode;
		try {
			ResponseEntity<String> result = restTemp.exchange(url, HttpMethod.GET, null, String.class);
			if (result != null) {
				resultMap.put("status", "success");
				resultMap.put("response", result.getBody());
			}
		} catch (HttpClientErrorException hEx) {
			resultMap.put("status", "failed");
			resultMap.put("response", hEx.getResponseBodyAsString());
			hEx.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return mapper.writeValueAsString(resultMap);
	}

	@SuppressWarnings("unchecked")
	@PostMapping("/getSgLocate")
	@ResponseBody
	public String getSgLocate(@RequestBody String locate)
			throws UnsupportedEncodingException, JsonMappingException, JsonProcessingException {
		String locateStr = URLDecoder.decode(locate, "UTF-8");
		locateStr = locateStr.substring(0, locateStr.length() - 1);
		Map<String, Object> locateMap = mapper.readValue(locateStr, Map.class);
		Map<String, String> resultMap = new HashMap<>();

		String url = "";
		if (locateMap.containsKey("Postcode") && locateMap.get("Postcode") != null && locateMap.get("Postcode") != "") {
			url = psgProp.getString("sglocate.search_with_postcode");
		} else if (locateMap.containsKey("StreetName") && locateMap.get("StreetName") != null
				&& locateMap.get("StreetName") != "" && locateMap.containsKey("Block") && locateMap.get("Block") != null
				&& locateMap.get("Block") != "") {
			url = psgProp.getString("sglocate.search_with_block_street");
		}

		if (url != null && !url.isEmpty()) {
			String apiKey = psgProp.getString("sglocate.api.key");
			String apiSecret = psgProp.getString("sglocate.api.secret");

			MultiValueMap<String, String> reqBody = new LinkedMultiValueMap<>();

			reqBody.add("APIKey", apiKey);
			reqBody.add("APISecret", apiSecret);
			reqBody.add("Postcode", (String) locateMap.get("Postcode"));
			reqBody.add("Block", (String) locateMap.get("Block"));
			reqBody.add("StreetName", (String) locateMap.get("StreetName"));

			HttpHeaders headers = new HttpHeaders();

			HttpEntity<?> entityRequest = new HttpEntity<>(reqBody, headers);
			try {
				ResponseEntity<String> result = restTemp.exchange(url, HttpMethod.POST, entityRequest, String.class);
				if (result != null) {
					resultMap.put("status", "success");
					resultMap.put("response", result.getBody());
				}
			} catch (HttpClientErrorException hEx) {
				resultMap.put("status", "failed");
				resultMap.put("response", hEx.getResponseBodyAsString());
				hEx.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mapper.writeValueAsString(resultMap);
	}

	@Override
	public String getErrorPath() {
		return "/error";
	}
	
	@GetMapping("/error")
	public ModelAndView handleError(HttpServletRequest request) {
//		logger.error("Error: " + request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE));
//		logger.error("Error: " + request.getAttribute(RequestDispatcher.ERROR_MESSAGE));
		
		String errorCode = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE).toString();
		mv.addObject("errorTitle", errorCode);
		mv.addObject("errorReason", errorCodeWithReason.get(errorCode).split(" - ")[0]);
		mv.addObject("errorDesc", errorCodeWithReason.get(errorCode).split(" - ")[1]);
		mv.setViewName("error");
		
		return mv;
	}
	
	@GetMapping("/masterTenantDist")
	public String masterTenantDist(HttpServletRequest request) throws JsonProcessingException {
		return "masterTenantDist";
	}

	@GetMapping("/editMasterTenantDis")
	public ModelAndView editMasterTenantDis(HttpServletRequest request)
			throws JsonProcessingException, UnsupportedEncodingException {

		String encodedtenantId = request.getQueryString();
		if (encodedtenantId != null && !encodedtenantId.isEmpty()) {
			String tenantId = new String(DatatypeConverter.parseBase64Binary(encodedtenantId), "UTF-8");

			String tenantIdJson = mapper
					.writeValueAsString(masterTenantDistService.getDataById(tenantId, PsgConst.TENANT_ID, PsgConst.MASTER_TENANT_DIST));
			//request.setAttribute("tenantIdJson", tenantIdJson);

			mv.addObject("tenantJson", tenantIdJson);
		}
		  
		mv.setViewName("masterTenantDist");
		return mv;
	}

	@GetMapping("/masterAdviserManagement")
	public ModelAndView adviserManagement(HttpServletRequest request) throws JsonProcessingException {
 
		List<MasterAdviser> advStffmagFlgList = masterAdviserManagementService.getAllDataById(PsgConst.MANAGER_FLG,
				PsgConst.YES_VALUE, PsgConst.MASTER_ADVISER);

		if (advStffmagFlgList != null && advStffmagFlgList.size() > 0) {

			String advStffFlgData = "";
			for (MasterAdviser AdvstffFlgDist : advStffmagFlgList) {
				advStffFlgData = advStffFlgData + AdvstffFlgDist.getAdvStfId() + ":" + AdvstffFlgDist.getAdvStfName()
						+ ",";
			}
			String finalAdvstffFlgDistList = advStffFlgData.substring(0, advStffFlgData.length() - 1);

			mv.addObject("advStffFlgList", finalAdvstffFlgDistList);
		}

		List<MasterPrincipal> prinfinfolist = masterPrincService.getAllData("MasterPrincipal");
		if (prinfinfolist != null && prinfinfolist.size() > 0) {
			String strprincinfoDate = "";
			for (MasterPrincipal masterPrincipal : prinfinfolist) {
				strprincinfoDate = strprincinfoDate + masterPrincipal.getPrinId() + ":" + masterPrincipal.getPrinName()
						+ ",";
			}
			String finalprincipalinfoList = strprincinfoDate.substring(0, strprincinfoDate.length() - 1);
			mv.addObject("princinfo", finalprincipalinfoList);
		}

		mv.setViewName("masterAdviserManagement");
		return mv;
	}

	@GetMapping("/editmasterAdvMagDets")
	public ModelAndView masterAdvMagId(HttpServletRequest request) throws UnsupportedEncodingException, JsonProcessingException
		 {
		
		String encodedadvStfId = request.getQueryString();
		if (encodedadvStfId != null && !encodedadvStfId.isEmpty()) {
			String advStfId = new String(DatatypeConverter.parseBase64Binary(encodedadvStfId), "UTF-8");

			String advStfIdJson = mapper
					.writeValueAsString(masterAdviserManagementService.getDataById(advStfId, PsgConst.ADVSTF_ID, PsgConst.MASTER_ADVISER));
			//request.setAttribute("MASTER_ADVISER", advStfIdJson);

			mv.addObject("advStfIdJson", advStfIdJson);
		}
		  
		mv.setViewName("masterAdviserManagement");
		return mv;
	}

	@GetMapping("/adviserInfo")
	public String adviserInfo() {
		return "adviserInfo";
	}
	
}
