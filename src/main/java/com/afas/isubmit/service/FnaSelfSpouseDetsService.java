package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.FnaSelfSpouseDets;

@Service
public class FnaSelfSpouseDetsService extends AbstractGenericDao<FnaSelfSpouseDets> {

}
