package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.FnaDetails;

@Service
public class FnaDetailsService extends AbstractGenericDao<FnaDetails> {

}
