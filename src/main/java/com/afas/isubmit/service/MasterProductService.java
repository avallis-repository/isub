package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.MasterProduct;

@Service
public class MasterProductService extends AbstractGenericDao<MasterProduct>{

}
