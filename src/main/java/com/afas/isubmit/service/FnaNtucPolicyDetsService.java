package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.FnaNtucPolicyDets;

@Service
public class FnaNtucPolicyDetsService extends AbstractGenericDao<FnaNtucPolicyDets> {

}
