package com.afas.isubmit.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

//import com.afas.isubmit.util.SessionUtil;

@Service
public class PsgService {

	@Autowired
	SessionFactory sessionFactory;
//	SessionUtil sessionUtil;

	@SuppressWarnings("rawtypes")
	public boolean chkValidDB() {
//		Date dateObj = null;
		try {
			Session session = sessionFactory.openSession();
			Query query = session.createQuery("show timezone");
			/* dateObj = (Date) */query.uniqueResult();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
