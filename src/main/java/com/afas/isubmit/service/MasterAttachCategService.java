package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.MasterAttachCategory;

@Service
public class MasterAttachCategService extends AbstractGenericDao<MasterAttachCategory>{

}
