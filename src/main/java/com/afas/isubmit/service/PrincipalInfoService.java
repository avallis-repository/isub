package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.PrincipalInfo;

@Service
public class PrincipalInfoService extends AbstractGenericDao<PrincipalInfo> {

}
