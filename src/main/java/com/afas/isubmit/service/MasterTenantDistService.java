package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.MasterTenantDist;

@Service
public class MasterTenantDistService extends AbstractGenericDao<MasterTenantDist> {

}
