package com.afas.isubmit.service;

import java.util.ResourceBundle;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.afas.isubmit.dto.Email;
import com.afas.isubmit.util.PsgConst;

@Service
public class MailService {
	
	private static Logger logger = LogManager.getLogger(MailService.class);

	@Autowired
	private JavaMailSender javaMailSender;
	
	@Value("${advmail.link}")
	String advMailLink;

	ResourceBundle resource = ResourceBundle.getBundle("properties/psgisubmit");

//	@Autowired
//	public MailService(JavaMailSender javaMailSender) {
//		this.javaMailSender = javaMailSender;
//	}
	public boolean sendEmail(Email user, HttpServletRequest request) {
		boolean isSent = false;
//		SimpleMailMessage mail = new SimpleMailMessage();
//		
//		mail.setTo(user.getSendToMail());
//		mail.setSubject(resource.getString("advmail.subject"));
//		mail.setText("<html>"
//	            + "<body>"
//	             + "<div><strong>Dear Manager,<strong></div>"
//	            +"<div>"+resource.getString("advmail.content")+"</div>"
//	            +"<a href="+resource.getString("advmail.link")+">"
//	            +"</body>"
//	            +"</html>",true);
		try {
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);

			String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
			String dataFormId = (String) request.getSession(false).getAttribute(PsgConst.SESS_DATAFORM_ID);
			String currentUrl = request.getRequestURL().toString()
					.replaceAll("FnaSignature/sentMail/" + user.getSendToMail(), "");
			logger.info("Current MGR URL--" + currentUrl);

			helper.setTo(user.getSendToMail());
			
			String adviserName = (String) request.getSession(false).getAttribute("AdviserName");
			String clientName = (String) request.getSession(false).getAttribute("SESS_DF_SELF_NAME");
			
			String mailSubject = resource.getString("appln.nonntuc.advtomgr.subject")
					.replace("~ADVISER_NAME~", adviserName)
					.replace("~CLIENT_NAME~", clientName)
					.replace("~FNA_ID~", fnaId);

			helper.setSubject(mailSubject);
			String encodedString = DatatypeConverter.printBase64Binary((fnaId + "=" + dataFormId).getBytes("UTF-8"));
			
			StringBuilder mailContent = new StringBuilder();
			mailContent.append(resource.getString("appln.nonntuc.advtomgr.content"))
					.append(resource.getString("appln.nonntuc.advtomgr.content.start"))
					.append(resource.getString("appln.nonntuc.advtomgr.content.advisername"))
					.append(resource.getString("appln.nonntuc.advtomgr.content.clientname"))
					.append(resource.getString("appln.nonntuc.advtomgr.content.fnaid"))
					.append(resource.getString("appln.nonntuc.advtomgr.content.end"))
					.append(resource.getString("appln.nonntuc.advtomgr.content.message"))
					.append(resource.getString("appln.nonntuc.advtomgr.content.appendix"))
					.append(resource.getString("donotreplyemail"));
			
			String mailCont = mailContent.toString().replace("~ADVISER_NAME~", adviserName)
					.replace("~CLIENT_NAME~", clientName)
					.replace("~FNA_ID~", fnaId)
					.replace("~APPROVAL_LINK~", "<a href=" + advMailLink + "managerLogin?" + encodedString + "> KYC-APPROVAL </a>");
			
			helper.setText(mailCont, true);
//			helper.setText("<html>" + "<body>" + "<div>Dear Manager,</div>" + "<div><p>"
//					+ resource.getString("advmail.content") + "</p></div>" + "<a href="
//					+ advMailLink + "managerLogin?" + encodedString + ">Click here</a>"
//					+ "</body>" + "</html>", true);
//
			javaMailSender.send(mimeMessage);
			isSent = true;
		} catch (Exception e) {
			e.printStackTrace();
			return isSent;
		}
		return isSent;
	}

	public void sendAdvEmail(Email user, String status, String reason, String role, HttpServletRequest request) {

		String fnaId = (String) request.getSession(false).getAttribute(PsgConst.SESS_FNA_ID);
		String dataFormId = (String) request.getSession(false).getAttribute(PsgConst.SESS_DATAFORM_ID);

		String currentUrl = request.getRequestURL().toString().replaceAll("FnaSignature/sentAdvMail", "");
		logger.info("Current URL--" + currentUrl);
		String mailContent = "<html>" + "<body>"

		;
		if (role.equals(PsgConst.ROLE_MANAGER)) {
			role = "admin";
			mailContent += "<div>Dear " + role + ",</div>" + "<div><p>" + resource.getString("mgrmail.content")
					+ "</p></div>" + "<div><p>Status :" + status + "</p></div>" + "<div><p>Reason :" + reason
					+ "</p></div>" + "<a href=" + currentUrl + role + "Login" + fnaId + "=" + dataFormId
					+ ">Click here</a>" + "</body>" + "</html>";
		} else if (role.equals(PsgConst.ROLE_ADMIN)) {
			role = "comp";
			mailContent += "<div>Dear " + role + ",</div>" + "<div><p>" + resource.getString("mgrmail.content")
					+ "</p></div>" + "<div><p>Status :" + status + "</p></div>" + "<div><p>Reason :" + reason
					+ "</p></div>" + "<a href=" + currentUrl + role + "Login" + fnaId + "=" + dataFormId
					+ ">Click here</a>" + "</body>" + "</html>";
		} else if (role.equals(PsgConst.ROLE_COMP)) {
			role = "adviser";
			mailContent += "<div>Dear " + role + ",</div>" + "<div><p>" + resource.getString("mgrmail.content")
					+ "</p></div>" + "<div><p>Status :" + status + "</p></div>" + "<div><p>Reason :" + reason
					+ "</p></div>" + "</body></html>";
		}
		try {
			MimeMessage mimeMessage = javaMailSender.createMimeMessage();

			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
			
		   
	        
			helper.setTo(user.getSendToMail());
			helper.setSubject(resource.getString("mgrmail.subject"));
			
			helper.setText(mailContent,true);
//			
			

			javaMailSender.send(mimeMessage);
			}catch(Exception e) {
				e.printStackTrace();
			}
	}
//	public void sendEmailWithAttachment(Customer user,File file) throws MailException, MessagingException {
//
//		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
//		
//		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
//
//		helper.setTo(user.getEmailId());
//		helper.setSubject("Testing Mail API with Attachment");
//		helper.setText("<html>"
//	            + "<body>"
//	             + "<div><strong>Dear customer,<strong></div>"
//	                + "<div>We are pleased to serve you.Happy shopping!</div>"
//	                +"<div>Please find the invoice attachment below.</div>"
//	                +"<br>"
//	                +"<br>"
//	                +"<br>"
//	                +"<br>"
//	                + "<div>Thanks and Regards,</div>"
//	                + "<strong>Team NK Spices<strong>"
//	              + "</body>"
//	            + "</html>", true);
//		
//		
//		//ClassPathResource classPathResource = new ClassPathResource("invoice.pdf");
//		//helper.addAttachment(classPathResource.getFilename(), classPathResource);
//		helper.addAttachment("invoice", file);
//		
//	javaMailSender.send(mimeMessage);
//	}

}


