package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.FnaSignature;

@Service
public class FnaSignatureService extends AbstractGenericDao<FnaSignature> {

}
