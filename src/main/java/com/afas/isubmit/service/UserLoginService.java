package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.UserLogin;

@Service
public class UserLoginService extends AbstractGenericDao<UserLogin> {

}
