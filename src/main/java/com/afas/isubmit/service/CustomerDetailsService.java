package com.afas.isubmit.service;

import org.springframework.stereotype.Service;

import com.afas.isubmit.dao.AbstractGenericDao;
import com.afas.isubmit.dto.CustomerDetails;

@Service
public class CustomerDetailsService extends AbstractGenericDao<CustomerDetails> {

}
